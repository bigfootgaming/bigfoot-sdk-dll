﻿#if UNITY_EDITOR
using System.Collections.Generic;
using BigfootSdk;
using BigfootSdk.Backend;
using BigfootSdk.Panels;
using Sirenix.OdinInspector;
using UnityEngine;

/// <summary>
/// Class only for showing how the reveal panel works
/// </summary>
public class RevealPanelTest : MonoBehaviour
{
    private readonly List<string> _itemsToGrant = new List<string> {"skin_01", "basicItem_01"};

    [Button("Simple")]
    public void RunSimple()
    {
        TransactionResultModel completeTransactionResultModel = new TransactionResultModel();
        CurrencyTransaction currencyTransaction = new CurrencyTransaction
        {
            CurrencyKey = "points",
            Amount = 10
        };

        completeTransactionResultModel.Union(currencyTransaction.ApplyTransaction());


        foreach (string itemName in _itemsToGrant)
        {
            BaseItemModel baseItemModel = ItemsManager.Instance.GetItem(itemName);
            ItemTransaction itemTransaction = new ItemTransaction(baseItemModel, 1);
            completeTransactionResultModel.Union(itemTransaction.ApplyTransaction());
        }

        ShowPanel(completeTransactionResultModel);
    }

    [Button("Composite")]
    public void RunComposite()
    {
        // List of transactions for the composite transaction
        List<ITransaction> transactions = new List<ITransaction>();

        CurrencyTransaction currencyTransaction = new CurrencyTransaction
        {
            CurrencyKey = "points",
            Amount = 20
        };


        transactions.Add(currencyTransaction);

        foreach (string itemName in _itemsToGrant)
        {
            BaseItemModel baseItemModel = ItemsManager.Instance.GetItem(itemName);
            ItemTransaction itemTransaction = new ItemTransaction(baseItemModel, 2);
            transactions.Add(itemTransaction);
        }

        CompositeTransaction compositeTransaction = new CompositeTransaction()
        {
            Transactions = transactions
        };

        ShowPanel(compositeTransaction.ApplyTransaction());
    }

    [Button("Composite + Composite")]
    public void RunMultipleComposite()
    {
        List<ITransaction> transactions = new List<ITransaction>();
        CurrencyTransaction currencyTransaction = new CurrencyTransaction
        {
            CurrencyKey = "points",
            Amount = 30
        };

        transactions.Add(currencyTransaction);

        foreach (string itemName in _itemsToGrant)
        {
            BaseItemModel baseItemModel = ItemsManager.Instance.GetItem(itemName);
            ItemTransaction itemTransaction = new ItemTransaction(baseItemModel, 3);
            transactions.Add(itemTransaction);
        }

        int amountOfCompositeTransactions = 2;

        List<ITransaction> compositeTransactions = new List<ITransaction>();
        for (int i = 0; i < amountOfCompositeTransactions; i++)
        {
            CompositeTransaction transaction = new CompositeTransaction()
            {
                Transactions = transactions
            };
            compositeTransactions.Add(transaction);
        }

        CompositeTransaction finalCompositeTransaction = new CompositeTransaction()
        {
            Transactions = compositeTransactions
        };

        ShowPanel(finalCompositeTransaction.ApplyTransaction());
    }

    private void ShowPanel(TransactionResultModel completeTransactionResultModel)
    {
        PanelsEvents.ShowPanel("RevealPanel", PanelBehavior.KeepInStack, panel =>
        {
            RevealPanel revealDialog = panel.GetComponent<RevealPanel>();
            if (revealDialog != null)
            {
                revealDialog.Populate(completeTransactionResultModel);
            }
        });
    }
}

#endif