﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditor.PackageManager.Requests;
using UnityEditor.PackageManager;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using BigfootSdk;
using BigfootSdk.Helpers;
using System;
using BigfootSdk.Backend;

/// <summary>
/// Update sdk version in config editor.
/// </summary>
public static class UpdateSdkVersionInConfigEditor
{
    /// <summary>
    /// The List of requests
    /// </summary>
    static ListRequest _request;
    
    /// <summary>
    /// Updates the sdk version in config on load.
    /// </summary>
    [InitializeOnLoadMethod]
    public static void UpdateSdkVersionInConfigOnLoad()
    {
        UpdateSdkVersionInConfig("");
    }

    /// <summary>
    /// Updates the sdk version in config.
    /// </summary>
    /// <param name="pathToBuildProject">Path to build project.</param>
    public static void UpdateSdkVersionInConfig(string pathToBuildProject)
    {
        // Read the config
        _request = Client.List();
        EditorApplication.update += Progress;
    }

    /// <summary>
    /// Check Packages used
    /// </summary>
    static void Progress()
    {
        if (_request.IsCompleted)
        {
            if (_request.Status == StatusCode.Success)
                foreach (var package in _request.Result)
                {
                    if (package.name == "com.bigfoot.sdk")
                    {
                        var configs = ScriptableObjectsHelper.GetAllInstances<SdkConfiguration>();
                        if (configs != null)
                        {
                            foreach (var config in configs)
                            {
                                config.SdkVersion = package.version;
                            }
                        }
                    } 
                }
            EditorApplication.update -= Progress;
        }
    }
}
