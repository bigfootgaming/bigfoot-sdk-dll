﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace BigfootSdk.Backend
{

	/// <summary>
	/// Manager to handle different environment configurations
	/// </summary>
	public static class EnvironmentManager
	{
#if UNITY_CLOUD_BUILD
		/// <summary>
		/// Used in Ucb to copy dev
		/// </summary>
		public static void CopyDevEnvironment(UnityEngine.CloudBuild.BuildManifestObject manifest)
		{
			UseDev();
	    }
		    
		/// <summary>
		/// Used in Ucb to copy staging
		/// </summary>
		public static void CopyStagingEnvironment(UnityEngine.CloudBuild.BuildManifestObject manifest) 
		{
		   UseStaging();
	    }
	    
		/// <summary>
		/// Used in Ucb to copy prod
		/// </summary>
		public static void CopyProdEnvironment(UnityEngine.CloudBuild.BuildManifestObject manifest) 
		{
			EnableSymbols();
		    UseProd();
			RemoveTitleData("prod");
	    }
#endif

		
		
		private const string ENVIRONMENT_DEV = "ENVIRONMENT_DEV";
		private const string ENVIRONMENT_STAGING = "ENVIRONMENT_STAGING";
		private const string ENVIRONMENT_PROD = "ENVIRONMENT_PROD";

		/// <summary>
		/// Menu item to switch to dev
		/// </summary>
		[MenuItem("BigfootSDK/Environments/Use Dev")]
		public static void UseDev()
		{
			SetDefineSymbol("ENVIRONMENT_DEV", true);
			SetDefineSymbol("ENVIRONMENT_STAGING", false);
			SetDefineSymbol("ENVIRONMENT_PROD", false);

			CopyEnvironmentFiles("dev");
		}
		

		/// <summary>
		/// Menu item to switch to staging
		/// </summary>
		[MenuItem("BigfootSDK/Environments/Use Staging")]
		public static void UseStaging()
		{
			SetDefineSymbol("ENVIRONMENT_DEV", false);
			SetDefineSymbol("ENVIRONMENT_STAGING", true);
			SetDefineSymbol("ENVIRONMENT_PROD", false);

			CopyEnvironmentFiles("staging");
		}

		/// <summary>
		/// Menu item to switch to prod
		/// </summary>
		[MenuItem("BigfootSDK/Environments/Use Prod")]
		public static void UseProd()
		{
			SetDefineSymbol("ENVIRONMENT_DEV", false);
			SetDefineSymbol("ENVIRONMENT_STAGING", false);
			SetDefineSymbol("ENVIRONMENT_PROD", true);

			CopyEnvironmentFiles("prod");
		}
		

		public static void CopyEnvironmentFiles(string environmentName)
		{
			Debug.Log("Copying firebase files for environment " + environmentName);

			// Copy the google-services.json file
			try
			{
				string sourceFilePathJson = "Assets/Environments/google-services." + environmentName + ".json";
				string destinationFilePathJson = "Assets/google-services.json";
				File.Copy(sourceFilePathJson, destinationFilePathJson, true);
				AssetDatabase.ImportAsset(destinationFilePathJson);
				ReadFile(destinationFilePathJson);
				
				string destinationFilePathXml = "Assets/Plugins/Android/FirebaseApp.androidlib/res/values/google-services.xml";
				if (File.Exists(destinationFilePathXml))
				{
					File.Delete(destinationFilePathXml);
				}
				
				string destinationFilePathDesktop = "Assets/StreamingAssets/google-services-desktop.json";
				if (File.Exists(destinationFilePathDesktop))
				{
					File.Delete(destinationFilePathDesktop);
				}
			}
			catch (Exception e)
			{
				Debug.Log("Error copying google-services.json: " + e.Message);
			}
			
			try
			{
				string sourceFilePath = "Assets/Environments/GoogleService-Info." + environmentName + ".plist";
				string destinationFilePath = "Assets/GoogleService-Info.plist";
				File.Copy(sourceFilePath, destinationFilePath, true);
				AssetDatabase.ImportAsset(destinationFilePath);
				ReadFile(destinationFilePath);
			}
			catch (Exception e)
			{
				Debug.Log("Error copying GoogleService-Info.plist: " + e.Message);
			}
		}
		
		static void ReadFile (string path)
		{
			Debug.Log("reading file " + path);
			StreamReader sr = new StreamReader(path);
			string text = sr.ReadToEnd();
			Debug.Log(text);
		}


		/// <summary>
		/// Enable the creation of symbols
		/// </summary>
		static void EnableSymbols()
		{
#if UNITY_ANDROID && UNITY_2019_3_OR_NEWER
			EditorUserBuildSettings.androidCreateSymbols = AndroidCreateSymbols.Debugging;
#endif
		}

		static void RemoveTitleData(string environment)
		{
			SdkConfiguration config = ScriptableObjectsHelper.GetAllInstances<SdkConfiguration>()
				.Find(x => x.Environment == environment);
			if (!config.BackendConfiguration.AllowOfflineLoad)
			{
				string assetPath = "Assets/Resources/TitleData.json";
				AssetDatabase.DeleteAsset(assetPath);
			}
		}

		/// <summary>
		/// Set the appropriate compiler flag is the implementation is enabled
		/// </summary>
		public static void SetDefineSymbol(string symbol, bool enable)
		{
#if UNITY_EDITOR
			List<BuildTargetGroup> targetGroups = new List<BuildTargetGroup>(){BuildTargetGroup.Android, BuildTargetGroup.iOS};
			foreach (var target in targetGroups)
			{
				// Grab the current compiler flags
				string definesString =
					PlayerSettings.GetScriptingDefineSymbolsForGroup(target);
				List<string> allDefines = definesString.Split(';').ToList();

				Debug.Log(target + " old: " + definesString);

				// Add it to the list
				if (enable)
				{
					if (!allDefines.Contains(symbol))
						allDefines.Add(symbol);
				}
				else
				{
					if (allDefines.Contains(symbol))
						allDefines.Remove(symbol);
				}

				string newDefinesString = string.Join(";", allDefines.ToArray());

				// Set the new list
				PlayerSettings.SetScriptingDefineSymbolsForGroup(
					target, newDefinesString);
				Debug.Log(target + " new: " + newDefinesString);
			}
#endif
		}
	}
}
