﻿using UnityEditor;
using UnityEngine.UI;

namespace BigfootSdk.Sprites
{
    [CustomEditor(typeof(DynamicSpriteLoaderUI))]
    public class DynamicSpriteLoaderUIEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DynamicSpriteLoaderUI loader = (DynamicSpriteLoaderUI)target;

            loader.Target = EditorGUILayout.ObjectField("Target", loader.Target, typeof(Image), true) as Image;

            loader.LoaderType = (SpriteLoaderType)EditorGUILayout.EnumPopup("Loader Type", loader.LoaderType);

            if (loader.LoaderType == SpriteLoaderType.Sprite)
            {
                loader.Key = EditorGUILayout.TextField("Sprite Key", loader.Key);
            }
            else
            {
                loader.Key = EditorGUILayout.TextField("Sprite Atlas Key", loader.Key);
                loader.SpriteElement = EditorGUILayout.TextField("Sprite Element", loader.SpriteElement);
            }

            loader.LoadOnEnable = EditorGUILayout.Toggle("Load On Enable", loader.LoadOnEnable);
        }
    }
}
