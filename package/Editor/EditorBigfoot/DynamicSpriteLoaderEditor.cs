﻿using UnityEngine;
using UnityEditor;

namespace BigfootSdk.Sprites
{
    [CustomEditor(typeof(DynamicSpriteLoader))]
    public class DynamicSpriteLoaderEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DynamicSpriteLoader loader = (DynamicSpriteLoader)target;


            loader.Target = EditorGUILayout.ObjectField("Target", loader.Target, typeof(SpriteRenderer), true) as SpriteRenderer;

            loader.LoaderType = (SpriteLoaderType) EditorGUILayout.EnumPopup("Loader Type", loader.LoaderType);

            if (loader.LoaderType == SpriteLoaderType.Sprite)
            {
                loader.Key = EditorGUILayout.TextField("Sprite Key", loader.Key);
            }
            else
            {
                loader.Key = EditorGUILayout.TextField("Sprite Atlas Key", loader.Key);
                loader.SpriteElement = EditorGUILayout.TextField("Sprite Element", loader.SpriteElement);
            }

            loader.LoadOnEnable = EditorGUILayout.Toggle("Load On Enable", loader.LoadOnEnable);
        }
    }
}
