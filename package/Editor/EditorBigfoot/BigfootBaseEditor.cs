﻿using BigfootSdk;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

/// <summary>
/// Bigfoot base editor.
/// </summary>
public abstract class BigfootBaseManagerEditor<T> : Editor where T : ILoadable
{
    /// <summary>
    /// The root of the editor
    /// </summary>
    protected VisualElement Root;

    /// <summary>
    /// The manager.
    /// </summary>
    protected T Manager;

    /// <summary>
    /// The properties to remove.
    /// </summary>
    string[] _propertiesToRemove;

    protected virtual void OnEnable()
    {
        // Cache the manager
        Manager = (T)target;

        Manager.OnFinishedLoading += PopulateCustomInspector;

        // Create the root
        Root = new VisualElement();

        // Grab the default BigfootStyle
        StyleSheet styleSheet;
        styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Bigfoot-sdk/Editor/EditorBigfoot/BigfootStyles.uss");
        if (styleSheet == null)
        {
            styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Packages/com.bigfoot.sdk/Editor/EditorBigfoot/BigfootStyles.uss");
        }

        // Apply it to the root
        Root.styleSheets.Add(styleSheet);
    }

    protected virtual void OnDisable()
    {
        Manager.OnFinishedLoading -= PopulateCustomInspector;
    }

    /// <summary>
    /// Populates the custom inspector.
    /// </summary>
    /// <param name="result">Result.</param>
    protected abstract void PopulateCustomInspector(string result);

    /// <summary>
    /// Removes the properties from the default edtior.
    /// </summary>
    /// <param name="properties">Properties.</param>
    protected void RemovePropertiesFromEdtior(string[] properties)
    {
        _propertiesToRemove = properties;
    }

    public override VisualElement CreateInspectorGUI()
    {
        // Clear the root
        Root.Clear();

        IMGUIContainer defaultInspector = new IMGUIContainer(() => {
            DrawPropertiesExcluding(serializedObject, _propertiesToRemove);
            serializedObject.ApplyModifiedProperties();
        });

        Root.Add(defaultInspector);

        if (Application.isPlaying)
        {
            PopulateCustomInspector("");
        }

        return Root;
    }
}
