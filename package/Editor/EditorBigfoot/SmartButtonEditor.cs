﻿using System;
using UnityEditor;
using UnityEditor.Events;
using UnityEditor.UIElements;
using BigfootSdk.Panels;
using UnityEngine.Events;
using UnityEngine.UIElements;
using UnityEngine;
using System.Collections.Generic;

namespace BigfootSdk
{
    [CustomEditor(typeof(SmartButton))]
    public class SmartButtonEditor : Editor
    {
        /// <summary>
        /// The root of the editor
        /// </summary>
        private VisualElement Root;

        /// <summary>
        /// The button.
        /// </summary>
        private SmartButton _button;

        /// <summary>
        /// The properties to remove.
        /// </summary>
        List<string> _propertiesToRemove = new List<string>();

        IMGUIContainer _defaultInspector;

        SerializedProperty _useAsProperty;
        SerializedProperty _valueProperty;

        EnumField _usage;
        TextField _eventName;

        protected virtual void OnEnable()
        {
            // Cache the smart button
            _button = (SmartButton)target;

            // Refresh UI.Button on click to keep it synced
            SetButtonOnClick();

            // Initialize the different uses
            _propertiesToRemove.AddRange(CreateGenericProperties());
            _propertiesToRemove.AddRange(CreatePanelManagerProperties());
            _propertiesToRemove.AddRange(CreateSendAnalyticsUiEventProperties());

            // Create the root
            Root = new VisualElement();

            // Grab the default BigfootStyle
            StyleSheet styleSheet;
            styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Bigfoot-sdk/Editor/EditorBigfoot/BigfootStyles.uss");
            if (styleSheet == null)
            {
                styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Packages/com.bigfoot.sdk/Editor/EditorBigfoot/BigfootStyles.uss");
            }

            // Apply it to the root
            Root.styleSheets.Add(styleSheet);
        }

        /// <summary>
        /// Sets the button on click.
        /// </summary>
        void SetButtonOnClick()
        {
            _button.Button = _button.GetComponent<UnityEngine.UI.Button>();
            int eventCount = _button.Button.onClick.GetPersistentEventCount();
            bool added = false;
            for (int i = 0; i < eventCount; i++)
            {
                if (_button.Button.onClick.GetPersistentTarget(i) == _button)
                {
                    added = true;
                }
            }

            if (!added)
            {
                UnityAction uAction = new UnityAction(_button.OnClick);
                UnityEventTools.AddPersistentListener(_button.Button.onClick, uAction);
                _button.Button.onClick.SetPersistentListenerState(0, UnityEventCallState.RuntimeOnly);
            }
        }

        /// <summary>
        /// Creates the inspector GUI.
        /// </summary>
        /// <returns>The inspector GUI.</returns>
        public override VisualElement CreateInspectorGUI()
        {
             _defaultInspector = new IMGUIContainer(GuiChangedEvent);

            ResetRoot(_button.UseAs);
            return Root;
        }

        /// <summary>
        /// Resets the root
        /// </summary>
        /// <param name="useAs">Use as.</param>
        void ResetRoot (SmartButton.Usage useAs)
        {
            // Clear the root
            Root.Clear();
            Root.Add(_defaultInspector);
            Root.Add(_usage);

            switch (useAs)
            {
                case SmartButton.Usage.ShowPanel:
                {
                    Root.Add(_panelName);
                    Root.Add(_previousPanelBehavior);
                    Root.Add(_panelsManagerId);
                    break;
                }
                case SmartButton.Usage.Back:
                {
                    Root.Add(_panelsManagerId);
                    break;
                }
                case SmartButton.Usage.SendAnalyticsCustomEvent:
                {
                    Root.Add(_eventName);
                    break;
                }
                case SmartButton.Usage.SendAnalyticsUiEvent:
                {
                    Root.Add(_userAction);
                    Root.Add(_uiLocation);
                    Root.Add(_uiElement);
                    Root.Add(_uiElementType);
                    break;
                }
                default: break;
            }
        }

        /// <summary>
        /// GUIs was changed
        /// </summary>
        void GuiChangedEvent ()
        {
            if (_useAsProperty.enumValueIndex != Convert.ToInt32(_usage.value))
            {
                _useAsProperty.enumValueIndex = Convert.ToInt32(_usage.value);
                ResetRoot((SmartButton.Usage)_usage.value);
            }

            // Update source properties
            switch (_button.UseAs)
            {
                case SmartButton.Usage.ShowPanel:
                {
                    _valueProperty.stringValue = _panelName.value;
                    _pbehaviorProperty.enumValueIndex = Convert.ToInt32(_previousPanelBehavior.value);
                    _pmidProperty.intValue = _panelsManagerId.value;
                    break;
                }
                case SmartButton.Usage.Back:
                {
                    _pmidProperty.intValue = _panelsManagerId.value;
                    break;
                }
                case SmartButton.Usage.SendAnalyticsCustomEvent:
                {
                    _valueProperty.stringValue = _eventName.value;
                    break;
                }
                case SmartButton.Usage.SendAnalyticsUiEvent:
                {
                    _userActionProperty.stringValue = _userAction.value;
                    _uiLocationProperty.stringValue = _uiLocation.value;
                    _uiElementProperty.stringValue = _uiElement.value;
                    _uiElementTypeProperty.stringValue = _uiElementType.value;
                    break;
                }
            }

            DrawPropertiesExcluding(serializedObject, _propertiesToRemove.ToArray());
            serializedObject.ApplyModifiedProperties();
        }

        /// <summary>
        /// Creates the generic properties.
        /// </summary>
        /// <returns>Properties to ignore.</returns>
        List<string> CreateGenericProperties()
        {
            List<string> propertiesToIgnore = new List<string>();

            _useAsProperty = serializedObject.FindProperty("UseAs");
            propertiesToIgnore.Add("UseAs");

            _valueProperty = serializedObject.FindProperty("Value");
            propertiesToIgnore.Add("Value");

            _usage = new EnumField("Use As", _button.UseAs);

            _eventName = new TextField("Event Name")
            {
                value = _valueProperty.stringValue
            };

            return propertiesToIgnore;
        }

        #region Panel Manager Event

        SerializedProperty _pbehaviorProperty;
        SerializedProperty _pmidProperty;

        TextField _panelName;
        EnumField _previousPanelBehavior;
        IntegerField _panelsManagerId;

        /// <summary>
        /// Creates the panel manager properties.
        /// </summary>
        /// <returns>Properties to ignore.</returns>
        List<string> CreatePanelManagerProperties()
        {
            List<string> propertiesToIgnore = new List<string>();

            _pbehaviorProperty = serializedObject.FindProperty("PreviousPanelBehavior");
            propertiesToIgnore.Add("PreviousPanelBehavior");

            _pmidProperty = serializedObject.FindProperty("PanelsManagerId");
            propertiesToIgnore.Add("PanelsManagerId");

            _previousPanelBehavior = new EnumField("Previous Panel Behavior", _button.PreviousPanelBehavior);

            _panelName = new TextField("Panel Name")
            {
                value = _valueProperty.stringValue
            };

            _panelsManagerId = new IntegerField("Panels Manager Id")
            {
                value = _pmidProperty.intValue
            };

            return propertiesToIgnore;
        }

        #endregion

        #region Analytics UI Event

        SerializedProperty _userActionProperty;
        SerializedProperty _uiLocationProperty;
        SerializedProperty _uiElementProperty;
        SerializedProperty _uiElementTypeProperty;

        TextField _userAction;
        TextField _uiLocation;
        TextField _uiElement;
        TextField _uiElementType;

        /// <summary>
        /// Creates the send analytics user interface event properties.
        /// </summary>
        /// <returns>Properties to ignore.</returns>
        List<string> CreateSendAnalyticsUiEventProperties()
        {
            List<string> propertiesToIgnore = new List<string>();

            _userActionProperty = serializedObject.FindProperty("UserAction");
            propertiesToIgnore.Add("UserAction");

            _uiLocationProperty = serializedObject.FindProperty("UiLocation");
            propertiesToIgnore.Add("UiLocation");

            _uiElementProperty = serializedObject.FindProperty("UiElement");
            propertiesToIgnore.Add("UiElement");

            _uiElementTypeProperty = serializedObject.FindProperty("UiElementType");
            propertiesToIgnore.Add("UiElementType");

            _userAction = new TextField("User Action")
            {
                value = _userActionProperty.stringValue
            };

            _uiLocation = new TextField("UI Location")
            {
                value = _uiLocationProperty.stringValue
            };

            _uiElement = new TextField("UI Element")
            {
                value = _uiElementProperty.stringValue
            };

            _uiElementType = new TextField("UI Element Type")
            {
                value = _uiElementTypeProperty.stringValue
            };

            return propertiesToIgnore;
        }

        #endregion
    }
}

