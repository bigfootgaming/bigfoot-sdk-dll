﻿using Amazon.S3;
using Amazon.S3.Transfer;
using Amazon;
using Amazon.Runtime;

using System;
using System.IO;
using System.Threading.Tasks;
using BigfootSdk;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using UnityEngine;
using UnityEditor;


    /// <summary>
    /// This script will help you uploading your Unity Addressables content to your S3 Bucket.
    /// You'll need to set up some tokens in the IAM console: https://console.aws.amazon.com/iam/home?#/home
    /// There's a quick tutorial on that here: https://aws.amazon.com/premiumsupport/knowledge-center/create-access-key/
    /// lastly, to find your bucket region name, visit the following table: https://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region
    /// Questions? E-mail me at ruben@thegamedev.guru
    /// </summary>
    public class UploadToS3 : MonoBehaviour
    {
        private static AddressablesS3Configuration _config;
        
        /// <summary>
        /// Method to be called from the Editor in the top menu, if you want to upload the bundles
        /// </summary>
        [MenuItem("BigfootSDK/Upload Addressables to S3")]
        public static async void UploadAddressables()
        {
            _config = ScriptableObjectsHelper.GetInstance<AddressablesS3Configuration>();
            
            await UploadAsync(RegionEndpoint.GetBySystemName(_config.BucketRegion), _config.BucketName, _config.IamAccessKeyId, _config.IamSecretKey,_config.AddressablesPath);
        }

        /// <summary>
        /// Method to upload the bundles from UCB. Add 'UploadAddressables' Post Build method list
        /// </summary>
        /// <param name="buildPath"></param>
        public static void UploadAddressables(string buildPath)
        {
            UploadAddressables();
        }
        
        /// <summary>
        /// Uploads to S3
        /// </summary>
        /// <param name="bucketRegion">The region</param>
        /// <param name="bucketName">The bucket name</param>
        /// <param name="iamAccessKeyId">The access key</param>
        /// <param name="iamSecretKey">The secret</param>
        /// <param name="path">The path to the built addressables</param>
        /// <returns></returns>
        private static async Task UploadAsync(RegionEndpoint bucketRegion, string bucketName, string iamAccessKeyId, string iamSecretKey, string path)
        {
            try
            {
                LogHelper.LogSdk(string.Format("Uploading from {0} to {1} in S3", path, bucketName));
                
                var credentials = new BasicAWSCredentials(iamAccessKeyId, iamSecretKey);
                var s3Client = new AmazonS3Client(credentials,  bucketRegion);

                var transferUtility = new TransferUtility(s3Client);

                var transferUtilityRequest = new TransferUtilityUploadDirectoryRequest
                {
                    BucketName = bucketName,
                    Directory = path,
                    StorageClass = S3StorageClass.StandardInfrequentAccess,
                    CannedACL = S3CannedACL.PublicRead,
                    SearchOption = SearchOption.AllDirectories
                };
                
                await transferUtility.UploadDirectoryAsync(transferUtilityRequest);
                LogHelper.LogSdk("Upload completed");
            }
            catch (AmazonS3Exception e)
            {
                LogHelper.LogError("Error encountered on server when writing an object: " + e.Message, Environment.StackTrace);
            }
            catch (Exception e)
            {
                LogHelper.LogError("Unknown encountered on server when writing an object: " + e.Message, Environment.StackTrace);
            }

        }
 
    }

