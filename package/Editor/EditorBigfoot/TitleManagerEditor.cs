﻿#if UNITY_EDITOR
using System.Collections;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Title manager editor.
	/// </summary>
	[CustomEditor (typeof(TitleManager))]
	public class TitleManagerEditor : Editor
	{
		bool showTitleData = true;

		public override void OnInspectorGUI ()
		{
            //Draw the normal UI
            DrawDefaultInspector();

            // Grab references to the manager & model
            TitleManager manager = (TitleManager)target;
			TitleModel title = manager.TitleData;

			// Title Data Foldout
			showTitleData = EditorGUILayout.Foldout (showTitleData, "Title Data");
			if (showTitleData) {
				EditorGUI.indentLevel++;
				foreach (KeyValuePair<string, ObscuredString> entry in title.Data) {
					EditorGUILayout.TextField (entry.Key, entry.Value);
				}
			}
		}
	}
}
#endif
