﻿#if UNITY_EDITOR
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEditor;
using UnityEngine;

namespace BigfootSdk.Audio
{

    [CustomEditor(typeof(AudioElement))]
    public class AudioElementEditor : Editor
    {
        AudioElement myScript;

        public override void OnInspectorGUI()
        {
            myScript = (AudioElement)target;



            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Name");
            EditorGUILayout.LabelField(myScript.Name);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Bus");
            EditorGUILayout.LabelField(myScript.Bus.ToString());
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Volume");
            float newVol = EditorGUILayout.Slider(myScript.Volume, 0f, 1f);
            if (newVol != myScript.Volume)
            {
                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                myScript.Volume = newVol;
                myScript.Source.volume = newVol;
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Voices");
            var newVoiceValue = EditorGUILayout.Slider(myScript.Voices, 1, 5);
            if (newVoiceValue != myScript.Voices)
            {
                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                myScript.Voices = Mathf.CeilToInt(newVoiceValue);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Trim Start Percentage");
            float newTSP = EditorGUILayout.Slider(myScript.TrimStartPercentage, 0f, 100f);
            if (myScript.TrimStartPercentage != newTSP)
            {
                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                myScript.TrimStartPercentage = newTSP;
            }

            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Trim End Percentage");
            float newTEP =  EditorGUILayout.Slider(myScript.TrimEndPercentage, 0f, 100f);
            if (newTEP != myScript.TrimEndPercentage)
            {
                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                myScript.TrimEndPercentage = newTEP;
            }

            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Original Duration");
            EditorGUILayout.LabelField(myScript.OriginalDuration.ToString());
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Current Duration");
            EditorGUILayout.LabelField(myScript.Duration.ToString());
            GUILayout.EndHorizontal();

            // Reference size
            float iconSize = 100f;
            float space = 15f;

            GUILayout.Space(10);
            GUILayout.Label("Test Audio (fully working on runtime)", EditorStyles.boldLabel);
            GUILayout.Space(10);

            GUILayout.BeginHorizontal();
            float btnWidth = (iconSize * 4) / 3;
            float btnHeight = 30f;

            if (GUILayout.Button("Play", "flow node 4", GUILayout.Width(btnWidth), GUILayout.Height(btnHeight)))
            {
                myScript.PlayAudio(false);
            }
            GUILayout.Space(space);
            if (GUILayout.Button("Play Looped", "flow node 0", GUILayout.Width(btnWidth), GUILayout.Height(btnHeight)))
            {
                myScript.PlayAudio(true);
            }
            GUILayout.Space(space);
            if (GUILayout.Button("Stop", "flow node 6", GUILayout.Width(btnWidth), GUILayout.Height(btnHeight)))
            {
                myScript.StopAudio();
            }
            GUILayout.EndHorizontal();

            GUILayout.Space(10);

            DrawWaveform();

        }

        public void DrawWaveform()
        {
            GUIStyle currentStyle = new GUIStyle(GUI.skin.box);
            currentStyle.normal.background = PaintWaveformSpectrum(myScript, 500, 50);

            Rect drop_area = GUILayoutUtility.GetRect(0.0f, 50.0f, GUILayout.ExpandWidth(true));

            GUI.Box(drop_area, "", currentStyle);
        }

        public Texture2D PaintWaveformSpectrum(AudioElement am, int width, int height)
        {
            var audio = am.Source.clip;

            Texture2D tex = new Texture2D(width, height, TextureFormat.RGBA32, false);
            float[] samples = new float[audio.samples];
            float[] waveform = new float[width];
            audio.GetData(samples, 0);
            int packSize = (audio.samples / width) + 1;
            int s = 0;
            for (int i = 0; i < audio.samples; i += packSize)
            {
                waveform[s] = Mathf.Abs(samples[i]);
                s++;
            }

            float trimStart = (am.TrimStartPercentage / 100) * width;
            float trimEnd = width - (width - ((am.TrimEndPercentage / 100) * width));


            int redlineWidth = 2;
            Color col = Color.black;

            // Create a full black block
            for (int x = 0; x < width; x++)
            {
                col = Color.black;

                if (x < trimStart)
                    col = Color.red;
                if (x < trimStart - redlineWidth)
                    col = Color.gray;

                if (x > trimEnd)
                    col = Color.red;
                if (x > trimEnd + redlineWidth)
                    col = Color.gray;


                for (int y = 0; y < height; y++)
                {
                    tex.SetPixel(x, y, col);
                }
            }

            myScript.UpdateDuration();

            // Change colors of blackBlock to show waveform and trimmed 
            for (int x = 0; x < waveform.Length; x++)
            {

                for (int y = 0; y <= waveform[x] * ((float)height * .75f); y++)
                {
                    col = Color.green;
                    if (x < trimStart)
                        col = Color.black;
                    if (x > trimEnd)
                        col = Color.black;

                    tex.SetPixel(x, (height / 2) + y, col);
                    tex.SetPixel(x, (height / 2) - y, col);
                }
            }
            tex.Apply();

            return tex;
        }

    }
}
#endif