﻿using System;
using UnityEditor;
using UnityEditor.Events;
using UnityEditor.UIElements;
using UnityEngine.Events;
using UnityEngine.UIElements;
using BigfootSdk.Helpers;

namespace BigfootSdk.Audio
{
    [CustomEditor(typeof(AudioAction))]
    public class AudioActionEditor : Editor
    {
        private VisualElement Root;

        private AudioAction _action;

        IMGUIContainer _defaultInspector;


        protected virtual void OnEnable()
        {
            // Cache the action
            _action = (AudioAction)target;

            // Create the root
            Root = new VisualElement();


            // Grab the default BigfootStyle
            StyleSheet styleSheet;
            styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Bigfoot-sdk/Editor/EditorBigfoot/BigfootStyles.uss");
            if (styleSheet == null)
            {
                styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Packages/com.bigfoot.sdk/Editor/EditorBigfoot/BigfootStyles.uss");
            }

            // Apply it to the root
            Root.styleSheets.Add(styleSheet);
        }

        public override VisualElement CreateInspectorGUI()
        {
            // Clear the root
            Root.Clear();



            IMGUIContainer defaultInspector = new IMGUIContainer(() => {
                //assing the UI.Button
                UnityEngine.UI.Button button = _action.GetComponent<UnityEngine.UI.Button>();

                if (_action.TriggerOnClick)
                {
                    if (button != null)
                    {
                        int eventCount = button.onClick.GetPersistentEventCount();
                        bool added = false;
                        for (int i = 0; i < eventCount; i++)
                        {
                            if (button.onClick.GetPersistentTarget(i) == _action)
                            {
                                added = true;
                            }
                        }

                        if (!added)
                        {
                            UnityAction uAction = new UnityAction(_action.SendEvent);
                            UnityEventTools.AddPersistentListener(button.onClick, uAction);
                            button.onClick.SetPersistentListenerState(eventCount, UnityEventCallState.RuntimeOnly);
                        }
                    }
                    else
                    {
                        LogHelper.LogWarning(string.Format("{0} can't trigger audio event on click because it hasn't a Button component", _action.name));
                        _action.TriggerOnClick = false;
                    }
                }
                else
                {
                    if (button != null)
                    {
                        int eventCount = button.onClick.GetPersistentEventCount();

                        int index = -1;
                        for (int i = 0; i < eventCount; i++)
                        {
                            if (button.onClick.GetPersistentTarget(i) == _action)
                            {
                                index = i;
                            }
                        }
                        if (index != -1)
                        {
                            UnityEventTools.RemovePersistentListener(button.onClick, index);
                        }
                    }
                }

                DrawPropertiesExcluding(serializedObject);
                serializedObject.ApplyModifiedProperties();
            });

            Root.Add(defaultInspector);

            return Root;
        }
    }

}
