﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace BigfootSdk.Audio
{
    [CustomEditor(typeof(AudioGroup))]
    public class AudioGroupEditor : Editor
    {
        AudioGroup myScript;

        public override void OnInspectorGUI()
        {
            myScript = (AudioGroup)target;

            bool dontDestroy = EditorGUILayout.Toggle("Dont Destroy", myScript.DontDestroy);
            if (dontDestroy != myScript.DontDestroy)
            {
                myScript.DontDestroy = dontDestroy;
                EditorUtility.SetDirty(target);
            }

            int id = EditorGUILayout.IntField("Id", myScript.Id);
            if (id != myScript.Id)
            {
                myScript.Id = id;
                EditorUtility.SetDirty(target);
            }

            myScript.UpdateLists();

            GUILayout.Label("Playlist", EditorStyles.boldLabel);

            bool oneMusicAtATime = EditorGUILayout.Toggle("One Music at a Time", myScript.OneMusicAtATime);
            if (oneMusicAtATime != myScript.OneMusicAtATime)
            {
                myScript.OneMusicAtATime = oneMusicAtATime;
                EditorUtility.SetDirty(target);
            }

            myScript.GroupPlaylist.Name = EditorGUILayout.TextField("Name", myScript.GroupPlaylist.Name.ToLower());

            for (int i = 0; i < myScript.GroupPlaylist.Elements.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(myScript.GroupPlaylist.Elements[i].Name);

                if (GUILayout.Button("Remove", GUILayout.Width(70)))
                {
                    myScript.RemoveFromPlaylist(myScript.GroupPlaylist.Elements[i]);
                }
                EditorGUILayout.EndHorizontal();
            }

            //Music
            GUILayout.Label("Music", EditorStyles.boldLabel);

            EditorGUI.indentLevel++;
            if (myScript.Sfxs != null)
            {
                for (int i = 0; i < myScript.Music.Count; i++)
                {
                    if (myScript.Music[i] != null)
                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(myScript.Music[i].Name);

                        if (GUILayout.Button("Add", GUILayout.Width(50)))
                        {
                            myScript.AddToPlaylist(myScript.Music[i]);
                        }

                        if (GUILayout.Button("Play", GUILayout.Width(50)))
                        {
                            myScript.Play(myScript.Music[i].Name, false, myScript.Id);
                        }

                        if (GUILayout.Button("Stop", GUILayout.Width(50)))
                        {
                            myScript.Stop(myScript.Music[i].Name, myScript.Id);
                        }

                        if (GUILayout.Button("Del", GUILayout.Width(50)))
                        {
                            myScript.DeleteAudio(myScript.Music[i].Name, true);
                        }
                        if (GUILayout.Button("Copy", GUILayout.Width(50)))
                        {
                            EditorGUIUtility.systemCopyBuffer = myScript.Music[i].Name;
                        }
                        EditorGUILayout.EndHorizontal();
                    }

                }
            }
            EditorGUI.indentLevel--;

            GUILayout.Space(10);
            GUILayout.Label("Add Music", EditorStyles.boldLabel);
            GUILayout.Space(10);

            DropAreaGUI(true);

            GUILayout.Space(10);

            //SFXs
            GUILayout.Label("Sfxs", EditorStyles.boldLabel);

            EditorGUI.indentLevel++;
            if (myScript.Sfxs != null)
            {
                for (int i = 0; i< myScript.Sfxs.Count; i++)
                {
                    if (myScript.Sfxs[i] != null)
                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(myScript.Sfxs[i].Name);

                        if (GUILayout.Button("Play", GUILayout.Width(50)))
                        {
                            myScript.Play(myScript.Sfxs[i].Name, false, myScript.Id);
                        }

                        if (GUILayout.Button("Stop", GUILayout.Width(50)))
                        {
                            myScript.Stop(myScript.Sfxs[i].Name, myScript.Id);
                        }                    
                       
                        if (GUILayout.Button("Del", GUILayout.Width(50)))
                        {
                            myScript.DeleteAudio(myScript.Sfxs[i].Name, false);
                        }

                        if (GUILayout.Button("Copy", GUILayout.Width(50)))
                        {
                            EditorGUIUtility.systemCopyBuffer = myScript.Sfxs[i].Name;
                        }
                        EditorGUILayout.EndHorizontal();
                    }

                }
            }
            EditorGUI.indentLevel--;

            GUILayout.Space(10);
            GUILayout.Label("Add Sfx", EditorStyles.boldLabel);
            GUILayout.Space(10);

            DropAreaGUI(false);

            GUILayout.Space(10);

            // Reference size
            float iconSize = 100;
            float space = 15f;

            GUILayout.BeginHorizontal();
            float btnWidth = (iconSize * 4) / 4;
            float btnHeight = 30f;
            GUILayout.Space(space);
            if (GUILayout.Button("Rnd Sfx", "flow node 4", GUILayout.Width(btnWidth), GUILayout.Height(btnHeight)))
            {
                myScript.PlayRandom(AudioBusEnum.SFX);
            }
            GUILayout.Space(space);
            if (GUILayout.Button("Rnd Music", "flow node 4", GUILayout.Width(btnWidth), GUILayout.Height(btnHeight)))
            {
                myScript.PlayRandom(AudioBusEnum.MUSIC);
            }
            GUILayout.Space(space);
            if (GUILayout.Button("Stop All", "flow node 6", GUILayout.Width(btnWidth), GUILayout.Height(btnHeight)))
            {
                myScript.StopAll();
            }
            GUILayout.EndHorizontal();

            //GUILayout.Space(10);
            //GUILayout.Label("Busses", EditorStyles.boldLabel);
            //GUILayout.Space(10);

            //GUILayout.BeginHorizontal();
            //btnWidth = (iconSize * 4) / 4;
            //btnHeight = 30f;

            //if (GUILayout.Button("Mute SFX", "flow node 0", GUILayout.Width(btnWidth), GUILayout.Height(btnHeight)))
            //{
            //    myScript.MuteBus(AudioBusEnum.SFX);
            //}
            //GUILayout.Space(space);
            //if (GUILayout.Button("Unmute SFX", "flow node 0", GUILayout.Width(btnWidth), GUILayout.Height(btnHeight)))
            //{
            //    myScript.UnmuteBus(AudioBusEnum.SFX);
            //}
            //GUILayout.Space(space);
            //if (GUILayout.Button("Mute Music", "flow node 0", GUILayout.Width(btnWidth), GUILayout.Height(btnHeight)))
            //{
            //    myScript.MuteBus(AudioBusEnum.MUSIC);
            //}
            //GUILayout.Space(space);
            //if (GUILayout.Button("Unmute Music", "flow node 0", GUILayout.Width(btnWidth), GUILayout.Height(btnHeight)))
            //{
            //    myScript.UnmuteBus(AudioBusEnum.MUSIC);
            //}
            //GUILayout.Space(space);
            //GUILayout.EndHorizontal();
        }

        public void DropAreaGUI(bool music)
        {
            GUIStyle currentStyle = new GUIStyle(GUI.skin.box);
            currentStyle.normal.background = MakeTex(2, 2, new Color(0f, 1f, 0f, 0.5f));


            Event evt = Event.current;
            Rect drop_area = GUILayoutUtility.GetRect(0.0f, 80.0f, GUILayout.ExpandWidth(true));

            if (music)
                GUI.Box(drop_area, "Drag music file here...", currentStyle);
            else
                GUI.Box(drop_area, "Drag sfx file here...", currentStyle);

            switch (evt.type)
            {
                case EventType.DragUpdated:
                case EventType.DragPerform:
                    if (!drop_area.Contains(evt.mousePosition))
                        return;

                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                    if (evt.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();

                        foreach (Object draggedFile in DragAndDrop.objectReferences)
                        {
                            if (draggedFile.GetType() == typeof(AudioClip))
                            {
                                var audioClip = (AudioClip)draggedFile;
                                myScript.CreateAudio(audioClip, music);
                            }
                        }
                    }
                    break;
            }
        }

        private Texture2D MakeTex(int width, int height, Color col)
        {
            Color[] pix = new Color[width * height];
            for (int i = 0; i < pix.Length; ++i)
            {
                pix[i] = col;
            }
            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();
            return result;
        }

    }
}
#endif