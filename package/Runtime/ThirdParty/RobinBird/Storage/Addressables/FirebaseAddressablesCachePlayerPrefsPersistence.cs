using UnityEngine;

namespace RobinBird.FirebaseTools.Storage.Addressables
{
    /// <summary>
    /// Use with caution. This will clutter your PlayerPrefs when you change asset paths on the server regularly
    /// There is no easy way to clear all Firebase Addressable keys from the PlayerPres. It is recommended to implement
    /// you own serialization on JSON or other formats
    /// </summary>
    public class FirebaseAddressablesCachePlayerPrefsPersistence : IFirebaseAddressablesCachePersistence
    {
        private const string PlayerPrefsPrependKey = "FAC_";

        public string GetCacheUrl(string key)
        {
            return PlayerPrefs.GetString(GetPlayerPrefKey(key));
        }

        public void SetCacheUrl(string key, string url)
        {
            PlayerPrefs.SetString(GetPlayerPrefKey(key), url);
        }

        public void RemoveCacheUrl(string key)
        {
            PlayerPrefs.DeleteKey(GetPlayerPrefKey(key));
        }

        private static string GetPlayerPrefKey(string key)
        {
            return $"{PlayerPrefsPrependKey}{key}";
        }
    }
}