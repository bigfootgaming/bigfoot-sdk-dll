namespace RobinBird.FirebaseTools.Storage.Addressables
{
    public interface IFirebaseAddressablesCachePersistence
    {
        string GetCacheUrl(string key);

        void SetCacheUrl(string key, string url);

        void RemoveCacheUrl(string key);
    }
}