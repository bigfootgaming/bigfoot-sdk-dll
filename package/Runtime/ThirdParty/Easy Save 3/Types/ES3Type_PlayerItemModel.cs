using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute("Id", "Name", "Type", "Class", "AmountRemaining", "Data")]
	public class ES3Type_PlayerItemModel : ES3ObjectType
	{
		public static ES3Type Instance = null;

		public ES3Type_PlayerItemModel() : base(typeof(BigfootSdk.Backend.PlayerItemModel)){ Instance = this; }

		protected override void WriteObject(object obj, ES3Writer writer)
		{
			var instance = (BigfootSdk.Backend.PlayerItemModel)obj;
			
			writer.WriteProperty("Id", instance.Id, ES3Type_ObscuredString.Instance);
			writer.WriteProperty("Name", instance.Name, ES3Type_ObscuredString.Instance);
			writer.WriteProperty("Type", instance.Type, ES3Type_ObscuredString.Instance);
			writer.WriteProperty("Class", instance.Class, ES3Type_ObscuredString.Instance);
			writer.WriteProperty("AmountRemaining", instance.AmountRemaining, ES3Type_ObscuredInt.Instance);
			writer.WriteProperty("Data", instance.Data);
		}

		protected override void ReadObject<T>(ES3Reader reader, object obj)
		{
			var instance = (BigfootSdk.Backend.PlayerItemModel)obj;
			foreach(string propertyName in reader.Properties)
			{
				switch(propertyName)
				{
					
					case "Id":
						instance.Id = reader.Read<CodeStage.AntiCheat.ObscuredTypes.ObscuredString>(ES3Type_ObscuredString.Instance);
						break;
					case "Name":
						instance.Name = reader.Read<CodeStage.AntiCheat.ObscuredTypes.ObscuredString>(ES3Type_ObscuredString.Instance);
						break;
					case "Type":
						instance.Type = reader.Read<CodeStage.AntiCheat.ObscuredTypes.ObscuredString>(ES3Type_ObscuredString.Instance);
						break;
					case "Class":
						instance.Class = reader.Read<CodeStage.AntiCheat.ObscuredTypes.ObscuredString>(ES3Type_ObscuredString.Instance);
						break;
					case "AmountRemaining":
						instance.AmountRemaining = reader.Read<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>(ES3Type_ObscuredInt.Instance);
						break;
					case "Data":
						instance.Data = reader.Read<System.Collections.Generic.Dictionary<string, System.Object>>();
						break;
					default:
						reader.Skip();
						break;
				}
			}
		}

		protected override object ReadObject<T>(ES3Reader reader)
		{
			var instance = new BigfootSdk.Backend.PlayerItemModel();
			ReadObject<T>(reader, instance);
			return instance;
		}
	}

	public class ES3Type_PlayerItemModelArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3Type_PlayerItemModelArray() : base(typeof(BigfootSdk.Backend.PlayerItemModel[]), ES3Type_PlayerItemModel.Instance)
		{
			Instance = this;
		}
	}
}