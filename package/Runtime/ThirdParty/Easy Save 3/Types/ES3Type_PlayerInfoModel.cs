using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute("Data", "Currencies", "Counters", "Flags", "Timers", "Items")]
	public class ES3Type_PlayerInfoModel : ES3ObjectType
	{
		public static ES3Type Instance = null;

		public ES3Type_PlayerInfoModel() : base(typeof(BigfootSdk.Backend.PlayerInfoModel)){ Instance = this; }

		protected override void WriteObject(object obj, ES3Writer writer)
		{
			var instance = (BigfootSdk.Backend.PlayerInfoModel)obj;
			
			writer.WriteProperty("Data", instance.Data);
			writer.WriteProperty("Currencies", instance.Currencies);
			writer.WriteProperty("Counters", instance.Counters);
			writer.WriteProperty("Flags", instance.Flags);
			writer.WriteProperty("Timers", instance.Timers);
			writer.WriteProperty("Items", instance.Items);
		}

		protected override void ReadObject<T>(ES3Reader reader, object obj)
		{
			var instance = (BigfootSdk.Backend.PlayerInfoModel)obj;
			foreach(string propertyName in reader.Properties)
			{
				switch(propertyName)
				{
					
					case "Data":
						instance.Data = reader.Read<System.Collections.Generic.Dictionary<System.String, CodeStage.AntiCheat.ObscuredTypes.ObscuredString>>();
						break;
					case "Currencies":
						instance.Currencies = reader.Read<System.Collections.Generic.Dictionary<System.String, CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>>();
						break;
					case "Counters":
						instance.Counters = reader.Read<System.Collections.Generic.Dictionary<System.String, CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>>();
						break;
					case "Flags":
						instance.Flags = reader.Read<System.Collections.Generic.Dictionary<System.String, CodeStage.AntiCheat.ObscuredTypes.ObscuredBool>>();
						break;
					case "Timers":
						instance.Timers = reader.Read<System.Collections.Generic.Dictionary<System.String, CodeStage.AntiCheat.ObscuredTypes.ObscuredLong>>();
						break;
					case "Items":
						instance.Items = reader.Read<System.Collections.Generic.List<BigfootSdk.Backend.PlayerItemModel>>();
						break;
					default:
						reader.Skip();
						break;
				}
			}
		}

		protected override object ReadObject<T>(ES3Reader reader)
		{
			var instance = new BigfootSdk.Backend.PlayerInfoModel();
			ReadObject<T>(reader, instance);
			return instance;
		}
	}

	public class ES3Type_PlayerInfoModelArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3Type_PlayerInfoModelArray() : base(typeof(BigfootSdk.Backend.PlayerInfoModel[]), ES3Type_PlayerInfoModel.Instance)
		{
			Instance = this;
		}
	}
}