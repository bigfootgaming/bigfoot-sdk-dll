using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute("hiddenValue")]
	public class ES3Type_ObscuredFloat : ES3Type
	{
		public static ES3Type Instance = null;

		public ES3Type_ObscuredFloat() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat))
		{
			Instance = this;
		}

		public override void Write(object obj, ES3Writer writer)
		{
			var instance = (CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat)obj;
			
			writer.WritePrivateField("hiddenValue", instance);
		}

		public override object Read<T>(ES3Reader reader)
		{
			var instance = new CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat();
			string propertyName;
			while((propertyName = reader.ReadPropertyName()) != null)
			{
				switch(propertyName)
				{
					
					case "hiddenValue":
						Int32 hiddenValue = reader.Read<Int32>();
					instance.SetEncrypted(hiddenValue);
					break;
					default:
						reader.Skip();
						break;
				}
			}
			return instance;
		}
	}

	public class ES3Type_ObscuredFloatArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3Type_ObscuredFloatArray() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat[]), ES3Type_ObscuredFloat.Instance)
		{
			Instance = this;
		}
	}
}