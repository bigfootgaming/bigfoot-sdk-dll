using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute("hiddenValue")]
	public class ES3Type_ObscuredLong : ES3Type
	{
		public static ES3Type Instance = null;

		public ES3Type_ObscuredLong() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredLong))
		{
			Instance = this;
		}

		public override void Write(object obj, ES3Writer writer)
		{
			var instance = (CodeStage.AntiCheat.ObscuredTypes.ObscuredLong)obj;
			
			writer.WritePrivateField("hiddenValue", instance);
		}

		public override object Read<T>(ES3Reader reader)
		{
			var instance = new CodeStage.AntiCheat.ObscuredTypes.ObscuredLong();
			string propertyName;
			while((propertyName = reader.ReadPropertyName()) != null)
			{
				switch(propertyName)
				{
					
					case "hiddenValue":
						Int64 hiddenValue = reader.Read<Int64>();
					instance.SetEncrypted(hiddenValue);
					break;
					default:
						reader.Skip();
						break;
				}
			}
			return instance;
		}
	}

	public class ES3Type_ObscuredLongArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3Type_ObscuredLongArray() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredLong[]), ES3Type_ObscuredLong.Instance)
		{
			Instance = this;
		}
	}
}