using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute("hiddenValue")]
	public class ES3Type_ObscuredDouble : ES3Type
	{
		public static ES3Type Instance = null;

		public ES3Type_ObscuredDouble() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble))
		{
			Instance = this;
		}

		public override void Write(object obj, ES3Writer writer)
		{
			var instance = (CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble)obj;
			
			writer.WritePrivateField("hiddenValue", instance);
		}

		public override object Read<T>(ES3Reader reader)
		{
			var instance = new CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble();
			string propertyName;
			while((propertyName = reader.ReadPropertyName()) != null)
			{
				switch(propertyName)
				{
					
					case "hiddenValue":
						Int64 hiddenValue = reader.Read<Int64>();
					instance.SetEncrypted(hiddenValue);
					break;
					default:
						reader.Skip();
						break;
				}
			}
			return instance;
		}
	}

	public class ES3Type_ObscuredDoubleArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3Type_ObscuredDoubleArray() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble[]), ES3Type_ObscuredDouble.Instance)
		{
			Instance = this;
		}
	}
}