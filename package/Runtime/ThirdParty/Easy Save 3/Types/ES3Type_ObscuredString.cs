using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute("hiddenValue")]
	public class ES3Type_ObscuredString : ES3ObjectType
	{
		public static ES3Type Instance = null;

		public ES3Type_ObscuredString() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredString)){ Instance = this; }

		protected override void WriteObject(object obj, ES3Writer writer)
		{
			var instance = (CodeStage.AntiCheat.ObscuredTypes.ObscuredString)obj;
			
			writer.WritePrivateField("hiddenValue", instance);
		}

		protected override void ReadObject<T>(ES3Reader reader, object obj)
		{
			var instance = (CodeStage.AntiCheat.ObscuredTypes.ObscuredString)obj;
			
			foreach(string propertyName in reader.Properties)
			{
				switch(propertyName)
				{
					
					case "hiddenValue":
					reader.SetPrivateField("hiddenValue", reader.Read<System.Byte[]>(), instance);
					break;
					default:
						reader.Skip();
						break;
				}
			}
		}

		protected override object ReadObject<T>(ES3Reader reader)
		{
			var instance = (CodeStage.AntiCheat.ObscuredTypes.ObscuredString)"";
			ReadObject<T>(reader, instance);
			return instance;
		}
	}

	public class ES3Type_ObscuredStringArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3Type_ObscuredStringArray() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredString[]), ES3Type_ObscuredString.Instance)
		{
			Instance = this;
		}
	}
}