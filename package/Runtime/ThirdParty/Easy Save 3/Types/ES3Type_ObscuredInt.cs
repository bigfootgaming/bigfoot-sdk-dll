using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute("hiddenValue")]
	public class ES3Type_ObscuredInt : ES3Type
	{
		public static ES3Type Instance = null;

		public ES3Type_ObscuredInt() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredInt))
		{
			Instance = this;
		}

		public override void Write(object obj, ES3Writer writer)
		{
			var instance = (CodeStage.AntiCheat.ObscuredTypes.ObscuredInt)obj;
			
			writer.WritePrivateField("hiddenValue", instance);
		}

		public override object Read<T>(ES3Reader reader)
		{
			var instance = new CodeStage.AntiCheat.ObscuredTypes.ObscuredInt();
			string propertyName;
			while((propertyName = reader.ReadPropertyName()) != null)
			{
				switch(propertyName)
				{
					
					case "hiddenValue":
						Int32 hiddenValue = reader.Read<Int32>();
						instance.SetEncrypted(hiddenValue);
					break;
					default:
						reader.Skip();
						break;
				}
			}
			return instance;
		}
	}

	public class ES3Type_ObscuredIntArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3Type_ObscuredIntArray() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredInt[]), ES3Type_ObscuredInt.Instance)
		{
			Instance = this;
		}
	}
}