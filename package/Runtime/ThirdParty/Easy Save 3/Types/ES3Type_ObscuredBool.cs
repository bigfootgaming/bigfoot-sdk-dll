using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute("hiddenValue")]
	public class ES3Type_ObscuredBool : ES3Type
	{
		public static ES3Type Instance = null;

		public ES3Type_ObscuredBool() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredBool))
		{
			Instance = this;
		}

		public override void Write(object obj, ES3Writer writer)
		{
			var instance = (CodeStage.AntiCheat.ObscuredTypes.ObscuredBool)obj;
			
			writer.WritePrivateField("hiddenValue", instance);
		}

		public override object Read<T>(ES3Reader reader)
		{
			var instance = new CodeStage.AntiCheat.ObscuredTypes.ObscuredBool();
			string propertyName;
			while((propertyName = reader.ReadPropertyName()) != null)
			{
				switch(propertyName)
				{
					
					case "hiddenValue":
						Int32 hiddenValue = reader.Read<Int32>();
						instance.SetEncrypted(hiddenValue);
					break;
					default:
						reader.Skip();
						break;
				}
			}
			return instance;
		}
	}

	public class ES3Type_ObscuredBoolArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3Type_ObscuredBoolArray() : base(typeof(CodeStage.AntiCheat.ObscuredTypes.ObscuredBool[]), ES3Type_ObscuredBool.Instance)
		{
			Instance = this;
		}
	}
}