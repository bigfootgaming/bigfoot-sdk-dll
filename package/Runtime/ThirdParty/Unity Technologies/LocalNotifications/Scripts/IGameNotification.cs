// Decompiled with JetBrains decompiler
// Type: BigfootSdk.Notifications.IGameNotification
// Assembly: BigfootSdk, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CF121039-145B-427B-A07A-2235E6421D4D
// Assembly location: /Users/martinamat/Documents/Bigfoot/Trie/Library/ScriptAssemblies/BigfootSdk.dll

using System;

namespace BigfootSdk.Notifications
{
    public interface IGameNotification
    {
        int? Id { get; set; }

        string LNId { get; set; }

        string Title { get; set; }

        string Body { get; set; }

        string Subtitle { get; set; }

        string Group { get; set; }

        int? BadgeNumber { get; set; }

        DateTime? DeliveryTime { get; set; }

        bool Scheduled { get; }

        string SmallIcon { get; set; }

        string LargeIcon { get; set; }

        string BigPicture { get; set; }
        
        bool ShouldAutoCancel { get; set; }
    }
}