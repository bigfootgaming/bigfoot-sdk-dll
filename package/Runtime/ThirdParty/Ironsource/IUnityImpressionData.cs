#if USE_IRONSOURCE
﻿using System;
public interface IUnityImpressionData
{
    event Action<IronSourceImpressionData> OnImpressionDataReady;

    event Action<IronSourceImpressionData> OnImpressionSuccess;
}

#endif