#if USE_IRONSOURCE
﻿using System;
    
public interface IUnitySegment
{
    event Action<String> OnSegmentRecieved;
}

#endif