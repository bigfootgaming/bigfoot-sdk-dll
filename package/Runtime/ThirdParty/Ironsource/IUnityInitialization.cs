#if USE_IRONSOURCE
﻿using System;
public interface IUnityInitialization
{
     event Action OnSdkInitializationCompletedEvent;
}

#endif