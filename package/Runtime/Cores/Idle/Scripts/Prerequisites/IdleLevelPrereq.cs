﻿using BigfootSdk.Backend;
using BigfootSdk.Core.Idle;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk
{
	/// <summary>
	/// This prerequisite can be used to check if a user has reached a certain level in the idle stack
	/// </summary>
	[System.Serializable]
	public class IdleLevelPrereq : IPrerequisite
	{
		/// <summary>
		/// The level identifier.
		/// </summary>
		public int Level;

		/// <summary>
		/// The comparison.
		/// </summary>
		public string Comparison = "eq";
		
		/// <summary>
		/// Should we check the max ever achieved, or the current one
		/// </summary>
		public bool CheckMax = true;
		
		/// <summary>
		/// Checks if the prerequisite is valid
		/// </summary>
		public override bool Check ()
		{
			if(CheckMax)
				return ComparisonHelper.Compare((int)IdleUserManager.Instance.GetSavedStat(IdleUserManager.LEVEL_SAVED_STAT).LifeMax, Level, Comparison);
			else
				return ComparisonHelper.Compare((int)IdleUserManager.Instance.GetSavedStat(IdleUserManager.LEVEL_SAVED_STAT).Game, Level, Comparison);

		}
	}
}
