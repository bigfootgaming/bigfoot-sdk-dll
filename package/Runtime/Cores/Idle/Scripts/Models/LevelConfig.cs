﻿using System.Collections.Generic;
using BigfootSdk.Backend;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// Level model.
    /// </summary>
    public class LevelModel
    {
        /// <summary>
        /// The base for the level progression
        /// </summary>
		public double LevelBase;

        /// <summary>
        /// The growth factor for the level progression
        /// </summary>
		public double LevelGrowth;

        /// <summary>
        /// The base for the level progression after the user did a hard reset
        /// </summary>
		public double HardResetLevelBase;

        /// <summary>
        /// The growth factor for the level progression after the user did a hard reset
        /// </summary>
		public double HardResetLevelGrowth;

        /// <summary>
        /// The level rewards.
        /// </summary>
        public List<ITransaction> LevelRewards;
    }
}
