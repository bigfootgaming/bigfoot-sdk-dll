﻿using System.Collections.Generic;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// Generators model.
    /// </summary>
    public class GeneratorsModel
    {
        /// <summary>
        /// The max amount of each generator a player can have
        /// </summary>
        public int MaxAmountOfGenerators;

        /// <summary>
        /// The generators.
        /// </summary>
        public List<GeneratorItemModel> Generators;
    }
}
