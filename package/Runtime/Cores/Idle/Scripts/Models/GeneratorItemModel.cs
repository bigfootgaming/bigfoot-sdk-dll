﻿using System.Collections.Generic;
using BigfootSdk.Backend;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// Generator item model.
    /// </summary>
    public class GeneratorItemModel
    {
        /// <summary>
        /// The name.
        /// </summary>
        public string Name;

        /// <summary>
        /// The unlock cost.
        /// </summary>
        public ICost UnlockCost;

        /// <summary>
        /// The upgrade base cost.
        /// </summary>
        public ResourceCost UpgradeCost;

        /// <summary>
        /// The resource it generates
        /// </summary>
        public string ResourceGenerated = "points";
        
        /// <summary>
        /// The multiplier cost
        /// </summary>
        public float CostMultiplier;
        
        /// <summary>
        /// the base amount of points it generates
        /// </summary>
        public double BaseBonus;

        /// <summary>
        /// The increment of points it generates per level.
        /// </summary>
        public double LevelUpBonus;

        /// <summary>
        /// The time it takes to generate (in seconds)
        /// </summary>
        public float Time;

        /// <summary>
        /// The ranks for this generator
        /// </summary>
        public List<GeneratorRankModel> Ranks;

        /// <summary>
        /// The generator custom data.
        /// </summary>
        public Dictionary<string, object> CustomData;

        /// <summary>
        /// The generator Tag
        /// </summary>
        public string Tag;

        /// <summary>
        /// Boolean value that indicates if the generator constantly generates points
        /// or need for an order to start generating
        /// </summary>
        public bool WaitForCollection;

        /// <summary>
        /// Boolean value that indicates if the generators awaits for an order to
        /// account generated resources or waits until collected
        /// </summary>
        public bool WaitForGeneration;
    }
}
