﻿using System;
using System.Collections.Generic;

namespace BigfootSdk.Core.Idle
{
    public class GeneratorModifiersModel : Singleton<GeneratorModifiersModel>, ICloneable
    {
        /// <summary>
        /// Bonus multiplier that applies to all generators
        /// </summary>
		public Dictionary <string, double> AllGeneratorBonusMultiplierByGroup = new Dictionary <string, double> 
			{ 
				{GameplayModifierConstants.NON_GROUPED, 1},
				{GameplayModifierConstants.RESULT, 1}
			};

        /// <summary>
        /// Bonus multipliers, for individual generators
        /// </summary>
		public Dictionary <string, Dictionary <string,double>> SpecificGeneratorsBonusMultipliersByGroup = new Dictionary <string, Dictionary <string, double>>();

        /// <summary>
        /// Time multiplier that applies to all generators
        /// </summary>
        public Dictionary<string, double> AllGeneratorTimeMultiplierByGroup = new Dictionary<string, double>
            {
                {GameplayModifierConstants.NON_GROUPED, 1},
                {GameplayModifierConstants.RESULT, 1}
            };

        /// <summary>
        /// Time multipliers, for individual generators
        /// </summary>
        public Dictionary<string, Dictionary<string, double>> SpecificGeneratorsTimeMultipliersByGroup = new Dictionary<string, Dictionary<string, double>>();
        
        /// <summary>
        /// Cost multipliers that applies to all generators
        /// </summary>
        public Dictionary<string, double> AllGeneratorCostMultiplierByGroup = new Dictionary<string, double>
        {
            {GameplayModifierConstants.NON_GROUPED, 1},
            {GameplayModifierConstants.RESULT, 1}
        };

        /// <summary>
        /// Cost multipliers, for individual generators
        /// </summary>
        public Dictionary<string, Dictionary<string, double>> SpecificGeneratorsCostMultipliersByGroup = new Dictionary<string, Dictionary<string, double>>();

        /// <summary>
        /// Creates a deep clone of the object
        /// </summary>
        /// <returns>Clone</returns>
        public object Clone()
        {
            GeneratorModifiersModel clone = new GeneratorModifiersModel();

            clone.AllGeneratorBonusMultiplierByGroup = new Dictionary<string, double>();
            foreach (var kvp in AllGeneratorBonusMultiplierByGroup)
            {
                clone.AllGeneratorBonusMultiplierByGroup.Add(kvp.Key, kvp.Value);
            }

            clone.SpecificGeneratorsBonusMultipliersByGroup = new Dictionary<string, Dictionary<string, double>>();
            foreach (var kvp in SpecificGeneratorsBonusMultipliersByGroup)
            {
                var clonedDict = new Dictionary<string, double>();
                foreach (var kvp2 in kvp.Value)
                {
                    clonedDict.Add(kvp2.Key, kvp2.Value);
                }
                
                clone.SpecificGeneratorsBonusMultipliersByGroup.Add(kvp.Key, clonedDict);
            }
            
            clone.AllGeneratorTimeMultiplierByGroup = new Dictionary<string, double>();
            foreach (var kvp in AllGeneratorTimeMultiplierByGroup)
            {
                clone.AllGeneratorTimeMultiplierByGroup.Add(kvp.Key, kvp.Value);
            }

            clone.SpecificGeneratorsTimeMultipliersByGroup = new Dictionary<string, Dictionary<string, double>>();
            foreach (var kvp in SpecificGeneratorsTimeMultipliersByGroup)
            {
                var clonedDict = new Dictionary<string, double>();
                foreach (var kvp2 in kvp.Value)
                {
                    clonedDict.Add(kvp2.Key, kvp2.Value);
                }
                
                clone.SpecificGeneratorsTimeMultipliersByGroup.Add(kvp.Key, clonedDict);
            }
            
            clone.AllGeneratorCostMultiplierByGroup = new Dictionary<string, double>();
            foreach (var kvp in AllGeneratorCostMultiplierByGroup)
            {
                clone.AllGeneratorCostMultiplierByGroup.Add(kvp.Key, kvp.Value);
            }

            clone.SpecificGeneratorsCostMultipliersByGroup = new Dictionary<string, Dictionary<string, double>>();
            foreach (var kvp in SpecificGeneratorsCostMultipliersByGroup)
            {
                var clonedDict = new Dictionary<string, double>();
                foreach (var kvp2 in kvp.Value)
                {
                    clonedDict.Add(kvp2.Key, kvp2.Value);
                }
                
                clone.SpecificGeneratorsCostMultipliersByGroup.Add(kvp.Key, clonedDict);
            }
            
            return clone;
        }
    }
}
