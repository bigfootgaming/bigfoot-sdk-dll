﻿using System.Collections.Generic;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// Ads model.
    /// </summary>
    public class AdsModel
    {
        /// <summary>
        /// The max amount of ads a player can stack
        /// </summary>
        public int MaxAdsStacked;

        /// <summary>
        /// The duration of each stack of bonus (in seconds)
        /// </summary>
        public int AdBonusDuration;

        /// <summary>
        /// The ad bonus multiplier.
        /// </summary>
        public float AdBonusMultiplier;
        
        /// <summary>
        /// Custom Data
        /// </summary>
        public Dictionary<string, object> CustomData;
    }
}
