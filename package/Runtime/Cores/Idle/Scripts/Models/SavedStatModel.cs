﻿
namespace BigfootSdk.Core.Idle
{
    public class SavedStatModel
    {
        // saved stats for current game, resets on soft reset
        public double Game;

        // saved stats since last hard reset, resets on hard reset
        public double Life;
        public double LifeMax;

        // all time saved stats, never resets
        public double All;
        public double AllMax;
    }
}
