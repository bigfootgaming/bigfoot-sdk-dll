﻿using System.Collections.Generic;

namespace BigfootSdk.Core.Idle
{
    public class GeneratorUserModel
    {
        /// <summary>
        /// How many of this generator the user has
        /// </summary>
        public int Amount;

        /// <summary>
        /// What was the last production time we have for this generator
        /// </summary>
        public double ProductionTime;

        /// <summary>
        /// The amount generated but not collected yet.
        /// </summary>
        public double Generated;

        /// <summary>
        /// Has to wait to start to generate
        /// </summary>
        public bool WaitForGeneration;

        /// <summary>
        /// Has to wait to collect
        /// </summary>
        public bool WaitForCollection;
        
        /// <summary>
        /// Resources generating states 
        /// </summary>
        public GeneratorsStates.StatesGeneration GenerationState;

        /// <summary>
        /// Resources collection state
        /// </summary>
        public GeneratorsStates.StatesCollection CollectingState;
        
        /// <summary>
        /// The Generator's custom data.
        /// </summary>
        public Dictionary<string, object> Data;
    }
}
