﻿using System.Collections.Generic;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// Generator rank model.
    /// </summary>
    public class GeneratorRankModel
    {
        /// <summary>
        /// How many generators it needs to be in this rank
        /// </summary>
        public int RequiredAmount = 0;

        /// <summary>
        /// The modifier to apply.
        /// </summary>
        public GameplayModifierModel Modifier;
        
        /// <summary>
        /// The rank's custom data.
        /// </summary>
        public Dictionary<string, object> CustomData;
    }
}
