﻿using System.Collections.Generic;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// Game model.
    /// </summary>
    public class IdleGameModel
    {
        /// <summary>
        /// How much money the player starts with
        /// </summary>
        public int StartingBalance;

        /// <summary>
        /// The minimum offline time for the offline bonus to kick in
        /// </summary>
        public int MinOfflineTime;

        /// <summary>
        /// Custom Data.
        /// </summary>
        public Dictionary<string, object> CustomData;
    }
}
