﻿using System.Collections.Generic;

namespace BigfootSdk.Core.Idle
{
    public class IdleUserModel
    {
        /// <summary>
        /// Generators
        /// </summary>
        public Dictionary<string, GeneratorUserModel> Generators = new Dictionary<string, GeneratorUserModel>();

        /// <summary>
        /// Resources
        /// </summary>
        public Dictionary<string, double> Resources = new Dictionary<string, double>();

        /// <summary>
        /// Saved stats for the user
        /// </summary>
        public Dictionary<string, SavedStatModel> SavedStats = new Dictionary<string, SavedStatModel>();
    }
}
