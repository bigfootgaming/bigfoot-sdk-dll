﻿using System.Collections.Generic;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// Soft reset model.
    /// </summary>
    public class SoftResetModel
    {
        /// <summary>
        /// The soft reset effects.
        /// </summary>
        public List<GameplayModifierModel> SoftResetEffects;

        /// <summary>
        /// The custom data.
        /// </summary>
        public Dictionary<string, object> CustomData;

        /// <summary>
        /// The name of the scene to load after the soft reset
        /// </summary>
        public string SceneName;
    }
}
