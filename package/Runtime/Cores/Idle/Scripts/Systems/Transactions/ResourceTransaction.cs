﻿using System;
using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// This transaction can be used to add or substract a resource
    /// </summary>
    /// <code>
    /// <br/><b>Examples:</b>
    /// 
    /// { "$type":"ResourceTransaction", "ResourceKey":"points", "Amount":10000000 }
    /// </code>
    public class ResourceTransaction : ITransaction
    {
        /// <summary>
        /// The resource key.
        /// </summary>
        public string ResourceKey;

        /// <summary>
        /// The amount.
        /// </summary>
        public double Amount;

        /// <summary>
        /// Applies the transaction.
        /// </summary>
        /// <param name="origin">Where this came from</param>
        /// /// <param name="sendTransactionEvent">send transaction event flag</param>
        /// <returns>The result model</returns>
        public override TransactionResultModel ApplyTransaction(string origin = "", bool sendTransactionEvent = true)
        {
            // Apply the resource
            ResourceManager.Instance.ModifyResource(ResourceKey, Amount);

            if (sendTransactionEvent)
                SendTransactionEvent(null, origin);

            // Persist it
            IdleUserManager.Instance.PersistUser();

            TransactionResultModel result = new TransactionResultModel();
            result.Resources.Add(ResourceKey, Amount);

            // Send the success callback
            return result;
        }

        /// <summary>
        /// Sends the transaction event.
        /// </summary>
        /// <param name="result">The result model</param>
        /// <param name="origin">Where this came from</param>
        protected override void SendTransactionEvent(TransactionResultModel result, string origin)
        {
            TransactionEventModel transactionEventModel = new TransactionEventModel()
            {
                TransactionType = "ResourceTransaction",
                Sinks = new List<TransactionEventParamenterModel>(),
                Sources = new List<TransactionEventParamenterModel>(),
                Origin = origin
            };

            if (Amount > 0)
                transactionEventModel.Sources.Add(new TransactionEventParamenterModel() { Key = ResourceKey, Amount = Amount, Type = "Resource" });
            else
                transactionEventModel.Sinks.Add(new TransactionEventParamenterModel() { Key = ResourceKey, Amount = Amount, Type = "Resource" });

            AnalyticsManager.Instance.TrackTransactionEvent(transactionEventModel);
        }
    }
}
