﻿using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// Resource Cost
    /// </summary>
    [System.Serializable]
    public class ResourceCost : ICost
	{
        /// <summary>
        /// The resource key.
        /// </summary>
        public string ResourceKey;

        /// <summary>
        /// The amount.
        /// </summary>
        public double Amount;

        /// <summary>
        /// Deducts the cost.
        /// </summary>
        public override void DeductCost(string origin, bool sendEvent)
        {
            ResourceManager.Instance.ModifyResource(ResourceKey, Amount * -1);

            // Persist it
            IdleUserManager.Instance.PersistUser();
            
            if (sendEvent)
	            TrackCostEvent(origin);
        }

        /// <summary>
        /// Returns if has enough to cover this cost
        /// </summary>
        /// <returns><c>true</c>, if the player has enough to cover the cost, <c>false</c> otherwise.</returns>
        public override bool HasEnough()
        {
            return ResourceManager.Instance.HasEnough(ResourceKey, Amount);
        }

        /// <summary>
        /// Is this an IAP
        /// </summary>
        /// <returns><c>true</c>, if this is an IAP, <c>false</c> otherwise.</returns>
        public override bool IsIAP()
        {
            return false;
        }

        /// <summary>
        /// Populates the transaction event.
        /// </summary>
        /// <param name="transactionEventModel">Transaction event model.</param>
        public override void PopulateTransactionEvent(TransactionEventModel transactionEventModel)
        {
            transactionEventModel.Sinks.Add(new TransactionEventParamenterModel() { Key = ResourceKey, Amount = Amount, Type = "Resource" });
        }
        
        /// <summary>
        /// Tracks Cost event
        /// </summary>
        /// <param name="origin"></param>
        public override void TrackCostEvent(string origin)
        {
	        TransactionEventModel transactionEventModel = new TransactionEventModel()
	        {
		        TransactionType = "ResourceCost",
		        Sinks = new List<TransactionEventParamenterModel>(),
		        Sources = new List<TransactionEventParamenterModel>(),
		        Origin = origin
	        };
	        PopulateTransactionEvent(transactionEventModel);
	        AnalyticsManager.Instance.TrackTransactionEvent(transactionEventModel);
        }

		/// <summary>
		/// Returns the amount of this cost type as an object
		/// </summary>
		/// <returns>Object with the cost amount</returns>
	    public override object GetCost()
	    {
	        return Amount;
	    }
	    
		/// <summary>
	    /// Returns the type of this cost
	    /// </summary>
	    /// <returns></returns>
	    public override string GetCostType()
	    {
	        return CostType.Resource;
	    }

	    /// <summary>
        /// Reverts the deduct cost.
        /// </summary>
        public override void RevertDeductCost()
        {
            ResourceManager.Instance.ModifyResource(ResourceKey, Amount);

            // Persist it
            IdleUserManager.Instance.PersistUser();
        }
    }
}
