﻿using System;
using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.LTE;
using BigfootSdk.SceneManagement;

namespace BigfootSdk.Core.Idle
{
    public class SoftResetManager : LoadableSingleton<SoftResetManager>
    {
        /// <summary>
        /// Event for when the player reaches a new level
        /// </summary>
        public static Action OnSoftReset;
        
        /// <summary>
        /// Config for the soft reset
        /// </summary>
        protected SoftResetModel _softResetModel;

        /// <summary>
        /// Reference to the PlayerManager
        /// </summary>
        protected PlayerManager _playerManager;
        
        public override void StartLoading()
        {
            // Grab the config
            _softResetModel = ConfigHelper.LoadConfig<SoftResetModel>(LimitedTimeEventsManager.Instance.GetConfigurationMapping("soft_reset"));

            // Cache the player manager
            _playerManager = PlayerManager.Instance;
            
            // Apply the current soft effects
            ApplySoftResetEffects();

            FinishedLoading(true);
        }

        /// <summary>
        /// Applies the soft reset effects
        /// </summary>
        public virtual void ApplySoftResetEffects()
        {
            // Get how many times we did a soft reset
            int amountSoftResets = _playerManager.GetCounter(CountersConstants.SOFT_RESET_AMOUNT, GameModeManager.CurrentGameMode);

            GameplayModifiersManager modifiersManager = GameplayModifiersManager.Instance;

            if (_softResetModel.SoftResetEffects != null)
            {
                // Iterate through all of the modifiers and apply them
                for (int i = 0; i < amountSoftResets; i++)
                {
                    if (i < _softResetModel.SoftResetEffects.Count)
                    {
                        modifiersManager.ApplyGameplayModifier(_softResetModel.SoftResetEffects[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Do the actual soft reset
        /// </summary>
        public virtual void SoftReset(bool changeScene = true)
        {
            IdleUserManager userManager = IdleUserManager.Instance;

            var eventData = new Dictionary<string, object>();
            eventData.Add("CurrentLevel", IdleUserManager.Instance.GetSavedStat(IdleUserManager.LEVEL_SAVED_STAT).Game);

            // Reset all of the saved stats
            userManager.SoftResetStats();

            // Reset the resources
            ResourceManager.Instance.SoftResetResources();

            // Reset the generators
            userManager.SoftResetGenerators();
            
            // Remove all of the modifiers from Ranks
            GameplayModifiersManager.Instance.RemoveGameplayModifiersWithTag("Rank");
            GameplayModifiersManager.Instance.RemoveGameplayModifiersWithTag("SoftReset");
            
            //after all resets, save the progress
            Dictionary<string, DataUpdateModel> dataToUpdate = new Dictionary<string, DataUpdateModel>();
            SetDataToUpdate(dataToUpdate);
            _playerManager.SetPlayerData(dataToUpdate, GameModeManager.CurrentGameMode);
            
            // Trigger the event
            OnSoftReset?.Invoke();
            
            TrackEvent(eventData);

            if (!string.IsNullOrEmpty(_softResetModel.SceneName) && changeScene)
            {
                // Reload the game
                BigfootSceneManager.LoadScene(_softResetModel.SceneName);
            }
        }

        protected virtual void TrackEvent(Dictionary<string, object> data)
        {
            data.Add("AmountPrestigesDone", _playerManager.GetCounter(CountersConstants.SOFT_RESET_AMOUNT, GameModeManager.CurrentGameMode));
            AnalyticsManager.Instance.TrackCustomEvent("NewPrestige", data);
        }

        /// <summary>
        /// Sets the data to update.
        /// </summary>
        /// <param name="dataToUpdate">Data to update.</param>
        public virtual void SetDataToUpdate (Dictionary<string, DataUpdateModel> dataToUpdate)
        {
            _playerManager.SetTimer(TimersConstants.LAST_IDLE, TimeHelper.GetTimeInServer(), GameModeManager.CurrentGameMode);
            
            _playerManager.SetCounter(CountersConstants.SOFT_RESET_AMOUNT, 1, GameModeManager.CurrentGameMode,true);
            
            dataToUpdate.Add("idle_user", new DataUpdateModel(JsonHelper.SerializeObject(IdleUserManager.Instance.GetUser())));
        }

        /// <summary>
        /// Get Reset Modifiers
        /// </summary>
        /// <param name="modifierIndex"> GameplayMofidier index </param>
        /// <returns> Returns GameplayModifierConfig for specified index</returns>
        public GameplayModifierModel GetResetRewards(int modifierIndex)
        {
            return _softResetModel.SoftResetEffects[modifierIndex];
        }

        /// <summary>
        /// Get the custom data for the soft reset
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetSoftResetCustomData()
        {
            return _softResetModel.CustomData;
        }

    }
}
