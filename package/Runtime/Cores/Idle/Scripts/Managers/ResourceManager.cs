﻿using System;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.LTE;

namespace BigfootSdk.Core.Idle
{
    public class ResourceManager : LoadableSingleton<ResourceManager>
    {
        /// <summary>
        /// Event for when the balance of a resource changes
        /// </summary>
        public static Action<string, double> OnResourceBalanceChanged;

        /// <summary>
        /// Main resource of Points
        /// </summary>
        public const string POINTS = "points";

        public override void StartLoading()
        {
            // Broadcast that we finished loading
            FinishedLoading(true);
        }

        /// <summary>
        /// Gets the amount of a specific resource
        /// </summary>
        /// <returns>The resource amount.</returns>
        /// <param name="resourceName">Resource name.</param>
        public double GetResourceAmount(string resourceName)
        {
            IdleUserModel userModel = IdleUserManager.Instance.GetUser();

            if (userModel.Resources.ContainsKey(resourceName))
                return userModel.Resources[resourceName];
            else
                return 0;
        }

        /// <summary>
        /// Checks if the user has enough of a resource
        /// </summary>
        /// <returns><c>true</c>, if enough was hased, <c>false</c> otherwise.</returns>
        /// <param name="resourceName">Resource name.</param>
        /// <param name="amount">Amount.</param>
        public bool HasEnough(string resourceName, double amount)
        {
            return GetResourceAmount(resourceName) >= amount;
        }

        /// <summary>
        /// Modifies the amount of a resource the user has
        /// </summary>
        /// <param name="resourceName">Resource name.</param>
        /// <param name="amount">Amount.</param>
        public void ModifyResource(string resourceName, double amount, bool triggerEvent = true)
        {
            IdleUserModel user = IdleUserManager.Instance.GetUser();
            if (user.Resources.ContainsKey(resourceName))
            {
                user.Resources[resourceName] += amount;
            }
            else
            {
                user.Resources[resourceName] = amount;
            }

            // Sometimes we want to trigger the event ourselves
            if (triggerEvent)
            {
                OnResourceBalanceChanged?.Invoke(resourceName, amount);
            }
        }

        /// <summary>
        /// Resets all of the resources
        /// </summary>
        public virtual void SoftResetResources()
        {
            IdleUserModel user = IdleUserManager.Instance.GetUser();

            // Grab the game config
            IdleGameModel gameModel = ConfigHelper.LoadConfig<IdleGameModel>(LimitedTimeEventsManager.Instance.GetConfigurationMapping("game"));

            // Set the starting points balance
            user.Resources[POINTS] = gameModel.StartingBalance;
        }

        /// <summary>
        /// Check if a given resource name is valid in this game instance
        /// </summary>
        /// <param name="resourceName">Resource name to validate</param>
        public bool IsValidResource(string resourceName)
        {
            IdleUserModel user = IdleUserManager.Instance.GetUser();
            return user.Resources.ContainsKey(resourceName);
        }
    }
}