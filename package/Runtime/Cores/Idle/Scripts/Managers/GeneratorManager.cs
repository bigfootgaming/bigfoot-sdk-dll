﻿using System;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.LTE;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    public class GeneratorManager : LoadableSingleton<GeneratorManager>
    {
        /// <summary>
        /// Event for when a generator is purchased
        /// </summary>
        public static Action<string, int, ICost> OnPurchasedGenerator;
        
        /// <summary>
        /// Event for when a generator is unlocked
        /// </summary>
        public static Action <string> OnGeneratorFirstPurchase;

        /// <summary>
        /// Event for when the generation of a generator is automated
        /// </summary>
        public static Action<string> OnAutomateGeneration;
        
        /// <summary>
        /// Event for when the generation of a generator is deautomated
        /// </summary>
        public static Action<string> OnDeautomateGeneration;
        
        /// <summary>
        /// Event for when the collection of a generator is automated
        /// </summary>
        public static Action<string> OnAutomateCollection;
        
        // <summary>
        /// Event for when the collection of a generator is deautomated
        /// </summary>
        public static Action<string> OnDeautomateCollection;
        
        /// <summary>
        /// Event for when a Generator ranks up
        /// </summary>
        public static Action<string> OnGeneratorRankUp;
    
        /// <summary>
        /// The generators config.
        /// </summary>
        protected GeneratorsModel _generatorsConfig;
        
        /// <summary>
        /// Cached reference to the IdleUserManager
        /// </summary>
        protected IdleUserManager _userManager;

        /// <summary>
        /// Cached refernce to the ResourceManager
        /// </summary>
        protected ResourceManager _resourceManager;

        /// <summary>
        /// The ad bonus manager.
        /// </summary>
        protected AdBonusManager _adBonusManager;

        /// <summary>
        /// The gameplay modifiers manager.
        /// </summary>
        protected GameplayModifiersManager _gameplayModifiersManager;
        
        /// <summary>
        /// Reference to the GeneratorModifiersModel
        /// </summary>
        protected GeneratorModifiersModel _generatorModifiers;
        
        /// <summary>
        /// Property to access the GeneratorModifiersModel
        /// </summary>
        public GeneratorModifiersModel GeneratorModifiersModel
        {
            get { return _generatorModifiers; }
        }
        
        public override void StartLoading()
        {
            // Cache the managers
            _userManager = IdleUserManager.Instance;
            _resourceManager = ResourceManager.Instance;
            _adBonusManager = AdBonusManager.Instance;
            _gameplayModifiersManager = GameplayModifiersManager.Instance;
            _generatorModifiers = GeneratorModifiersModel.Instance;
            
            // Grab the config
            _generatorsConfig = ConfigHelper.LoadConfig<GeneratorsModel>(LimitedTimeEventsManager.Instance.GetConfigurationMapping("generators"));

            // Apply the appropiate rank bonuses
            ApplyUsersRankModifiers();

            // Broadcast that we finished loading
            FinishedLoading(true);
        }

        /// <summary>
        /// Gets the generators.
        /// </summary>
        /// <returns>The generators.</returns>
        public virtual List<GeneratorItemModel> GetGenerators()
        {
            return _generatorsConfig.Generators;
        }

        /// <summary>
        /// Gets the generators with a specific tag
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public virtual List<GeneratorItemModel> GetGeneratorsWithTag(string tag)
        {
            return _generatorsConfig.Generators.Where(gen => gen.Tag == tag).ToList();
        }

        /// <summary>
        /// Gets the generator by name
        /// </summary>
        /// <returns>The generator by name.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual GeneratorItemModel GetGeneratorByName(string generatorName)
        {
            return _generatorsConfig.Generators.FirstOrDefault(generator => generator.Name == generatorName);
        }

        /// <summary>
        /// Modifies the generator production time.
        /// </summary>
        /// <returns>The generator production time.</returns>
        /// <param name="generatorName">Generator name.</param>
        /// <param name="delta">Delta.</param>
        public virtual double ModifyGeneratorProductionTime(string generatorName, double delta)
        {
            double result = 0;
            
            // Grab the generator for the user
            GeneratorUserModel generator = _userManager.GetGeneratorFromUser(generatorName);

            if (generator != null)
            {
                generator.ProductionTime += delta;
                result = generator.ProductionTime;
            }

            return result;
        }
        
        /// <summary>
        /// Sets the generator production time.
        /// </summary>
        /// <returns>The generator production time.</returns>
        /// <param name="generatorName">Generator name.</param>
        /// <param name="value">The value.</param>
        public virtual double SetGeneratorProductionTime(string generatorName, double value)
        {
            double result = 0;
            // Grab the generator for the user
            GeneratorUserModel generator = _userManager.GetGeneratorFromUser(generatorName);
            if (generator != null)
            {
                generator.ProductionTime = value;
                result = generator.ProductionTime;
            }
            return result;
        }

        /// <summary>
        /// Gets the generator production time.
        /// </summary>
        /// <returns>The generator production time.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual double GetGeneratorProductionTime(string generatorName)
        {
            double result = 0;

            GeneratorUserModel generator = _userManager.GetGeneratorFromUser(generatorName);

            if (generator != null)
            {
                result = generator.ProductionTime;
            }

            return result;
        }
        
        /// <summary>
        /// Gets the production's interval of a specific Generator
        /// </summary>
        /// <returns>The production interval.</returns>
        /// <param name="generatorConfig">Generator config.</param>
        /// <param name="ignoreAllMultiplier">If true, ignores the gameplay modifiers that affect all generators .</param>
        public virtual double GetProductionInterval(GeneratorItemModel generatorConfig, bool ignoreAllMultiplier = false) 
        {
            // Base time from the GeneratorItemModel
            double result = generatorConfig.Time;

            // If we have any Modifier that decrements our time production
            if (_generatorModifiers.SpecificGeneratorsTimeMultipliersByGroup.ContainsKey(generatorConfig.Name))
            {
                result /= _generatorModifiers.SpecificGeneratorsTimeMultipliersByGroup[generatorConfig.Name][GameplayModifierConstants.RESULT];
            }

            // Add the bonus for all of the generators
            if (_generatorModifiers.AllGeneratorTimeMultiplierByGroup[GameplayModifierConstants.RESULT] > 0 && !ignoreAllMultiplier)
            {
                result /= _generatorModifiers.AllGeneratorTimeMultiplierByGroup[GameplayModifierConstants.RESULT];
            }

            return result;
        }

        /// <summary>
        /// Returns the generator production per interval.
        /// </summary>
        /// <returns>The generator production per interval.</returns>
        /// <param name="generatorConfig">Generator config.</param>
        /// <param name="amountOwned">Amount owned.</param>
        /// <param name="overrideAdCheck">Overrides if we need to check the ad bonus</param>
        /// <param name="overrideAdValue">Overrides the result of the ad bonus, in case you want to force it to true/false</param>
        /// <param name="ignoreAllMultiplier">If true, ignores the gameplay modifiers that affect all generators .</param>
        public virtual double GetGeneratorProductionPerInterval(GeneratorItemModel generatorConfig, int amountOwned, bool ignoreAllMultiplier = false)
        {
            double result = 0;

            if (amountOwned > 0)
                // basic formula
                result = generatorConfig.BaseBonus + generatorConfig.LevelUpBonus * (amountOwned - 1);
            

            // If we have any Modifier that increments our production amount
            if (_generatorModifiers.SpecificGeneratorsBonusMultipliersByGroup.ContainsKey(generatorConfig.Name)) {
                result *= _generatorModifiers.SpecificGeneratorsBonusMultipliersByGroup[generatorConfig.Name][GameplayModifierConstants.RESULT];
            }

            // Add the bonus for all of the generators
            if (_generatorModifiers.AllGeneratorBonusMultiplierByGroup[GameplayModifierConstants.RESULT] > 0 && !ignoreAllMultiplier) {
                result *= _generatorModifiers.AllGeneratorBonusMultiplierByGroup[GameplayModifierConstants.RESULT];
            }

            return result;
        }

        /// <summary>
        /// Purchases a generator.
        /// </summary>
        /// <returns><c>true</c>, if generator was purchased, <c>false</c> otherwise.</returns>
        /// <param name="generatorName">Generator name.</param>
        /// <param name="purchaseQuantity">Purchase quantity.</param>
        /// <param name="ignoreCost">Ignores the cost.</param>
        public virtual bool PurchaseGenerator (string generatorName, int purchaseQuantity, bool ignoreCost = false)
        {
            GeneratorUserModel generatorUserModel = _userManager.GetGeneratorFromUser(generatorName);
            GeneratorItemModel generatorItemModel = GetGeneratorByName(generatorName);

            if ((generatorUserModel == null || generatorUserModel.Amount == 0) && generatorItemModel.UnlockCost != null)
            {
                return UnlockGenerator(generatorName, ignoreCost); 
            }
            else
            {
                return UpgradeGenerator(generatorName, purchaseQuantity, ignoreCost); 
            }
        }

        /// <summary>
        /// Automates the generation of one generator
        /// </summary>
        /// <param name="generatorName">Generator name.</param>
        public virtual void AutomateGeneration(string generatorName)
        {
            GeneratorUserModel generatorUserModel = _userManager.GetGeneratorFromUser(generatorName);

            if (generatorUserModel.WaitForGeneration)
            {
                generatorUserModel.WaitForGeneration = false;
                generatorUserModel.GenerationState = GeneratorsStates.StatesGeneration.GENERATING;
                OnAutomateGeneration?.Invoke(generatorName);
            }
        }
        
        /// <summary>
        /// Deautomates the generation of one generator
        /// </summary>
        /// <param name="generatorName">Generator name.</param>
        public virtual void DeautomateGeneration(string generatorName)
        {
            GeneratorUserModel generatorUserModel = _userManager.GetGeneratorFromUser(generatorName);

            if (!generatorUserModel.WaitForGeneration)
            {
                generatorUserModel.WaitForGeneration = true;
                OnDeautomateGeneration?.Invoke(generatorName);
            }
        }

        /// <summary>
        /// Automates the collection of one generator
        /// </summary>
        /// <param name="generatorName">Generator name.</param>
        public virtual void AutomateCollection(string generatorName)
        {
            GeneratorUserModel generatorUserModel = _userManager.GetGeneratorFromUser(generatorName);

            if (generatorUserModel.WaitForCollection)
            {
                generatorUserModel.WaitForCollection = false;
                generatorUserModel.CollectingState = GeneratorsStates.StatesCollection.COLLECTING;
                OnAutomateCollection?.Invoke(generatorName);
            }
        }
        
        /// <summary>
        /// Deautomates the collection of one generator
        /// </summary>
        /// <param name="generatorName">Generator name.</param>
        public virtual void DeautomateCollection(string generatorName)
        {
            GeneratorUserModel generatorUserModel = _userManager.GetGeneratorFromUser(generatorName);

            if (!generatorUserModel.WaitForCollection)
            {
                generatorUserModel.WaitForCollection = true;
                OnDeautomateCollection?.Invoke(generatorName);
            }
        }

        /// <summary>
        /// Sets the generation state to generating
        /// </summary>
        /// <param name="generatorName">Generator name.</param>
        public virtual void Generate (string generatorName)
        {
            GeneratorUserModel generatorUserModel = _userManager.GetGeneratorFromUser(generatorName);
            generatorUserModel.GenerationState = GeneratorsStates.StatesGeneration.GENERATING;
        }
        
        /// <summary>
        /// Sets the collection state to collecting
        /// </summary>
        /// <param name="generatorName">Generator name.</param>
        public virtual void Collect (string generatorName)
        {
            GeneratorUserModel generatorUserModel = _userManager.GetGeneratorFromUser(generatorName);
            generatorUserModel.CollectingState = GeneratorsStates.StatesCollection.COLLECTING;
        }

        /// <summary>
        /// Unlocks the generator.
        /// </summary>
        /// <returns><c>true</c>, if generator was unlocked, <c>false</c> otherwise.</returns>
        /// <param name="generatorName">Generator name.</param>
        /// <param name="ignoreCost">Ignores the cost.</param>
        public virtual bool UnlockGenerator(string generatorName, bool ignoreCost = false)
        {
            // Check for a Valid generator Name
            if (string.IsNullOrEmpty(generatorName))
            {
                LogHelper.LogSdk("Generator name is not a valid string");
                return false;   
            }
            
            var generatorItemModel = GetGeneratorByName(generatorName);

            if (generatorItemModel == null)
            {
                LogHelper.LogSdk(string.Format("Generator with name $0 is not a valid Generator", generatorName));
                return false;
            }
            
            //get purchase cost
            ICost unlockCost = GetGeneratorUnlockCost(generatorName);
            
            if (unlockCost != null && unlockCost.HasEnough() || ignoreCost)
            {
                if (!ignoreCost)
                {
                    unlockCost.DeductCost("UnlockGenerator", true);
                }

                // Modify the user model
                GeneratorUserModel generator = _userManager.GetGeneratorFromUser(generatorName);
                generator.Amount = 1;

                OnPurchasedGenerator?.Invoke(generatorName, 1, unlockCost);
                OnGeneratorFirstPurchase?.Invoke(generatorName);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the generator unlock cost.
        /// </summary>
        /// <returns>The generator unlock cost.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual ICost GetGeneratorUnlockCost(string generatorName)
        {
            ICost unlockCost = GetGeneratorByName(generatorName).UnlockCost;
            
            if (unlockCost is ResourceCost resourceCost)
            {
                double amount = resourceCost.Amount;
                // If we have any Modifier that decrements our time production
                if (_generatorModifiers.SpecificGeneratorsCostMultipliersByGroup.ContainsKey(generatorName))
                {
                    amount *= _generatorModifiers.SpecificGeneratorsCostMultipliersByGroup[generatorName][GameplayModifierConstants.RESULT];    
                }

                // Add the bonus for all of the generators
                if (_generatorModifiers.AllGeneratorCostMultiplierByGroup[GameplayModifierConstants.RESULT] > 0)
                {
                    amount *= _generatorModifiers.AllGeneratorCostMultiplierByGroup[GameplayModifierConstants.RESULT];
                }

                return new ResourceCost()
                {
                    Amount = amount,
                    ResourceKey = resourceCost.ResourceKey
                };
            }
            else if (unlockCost is CurrencyCost currencyCost)
            {
                int amount = currencyCost.Amount;
                // If we have any Modifier that decrements our time production
                if (_generatorModifiers.SpecificGeneratorsCostMultipliersByGroup.ContainsKey(generatorName))
                {
                    amount *= (int)_generatorModifiers.SpecificGeneratorsCostMultipliersByGroup[generatorName][GameplayModifierConstants.RESULT];
                }

                // Add the bonus for all of the generators
                if (_generatorModifiers.AllGeneratorCostMultiplierByGroup[GameplayModifierConstants.RESULT] > 0)
                {
                    amount *= (int)_generatorModifiers.AllGeneratorCostMultiplierByGroup[GameplayModifierConstants.RESULT];
                }
                return new CurrencyCost()
                {
                    Amount = amount,
                    CurrencyKey = currencyCost.CurrencyKey
                };
            }

            return unlockCost;
        }

        /// <summary>
        /// Upgrades the generator.
        /// </summary>
        /// <returns><c>true</c>, if generator was upgraded, <c>false</c> otherwise.</returns>
        /// <param name="generatorName">Generator name.</param>
        /// <param name="purchaseQuantity">Purchase quantity.</param>
        /// <param name="ignoreCost">Ignores the cost.</param>
        public virtual bool UpgradeGenerator (string generatorName, int purchaseQuantity, bool ignoreCost = false)
        {
            //get purchase cost
            ResourceCost upgradeCost = GetGeneratorUpgradeCost(generatorName, purchaseQuantity);
            
            if (upgradeCost != null && upgradeCost.HasEnough () || ignoreCost)
            {
                if (!ignoreCost)
                {
                    // Take out the resources
                    upgradeCost.DeductCost("UpgradeGenerator", true);
                }
                
                int amountGeneratorsUntilRankUp = GetAmountOfGeneratorsUntilNextRank(generatorName);

                // Grab the user model
                GeneratorUserModel generator = _userManager.GetGeneratorFromUser(generatorName);

                // Check if we ranked up
                CheckIfGeneratorRankedUp(generatorName, generator, purchaseQuantity);

                // Modify the amount
                generator.Amount += purchaseQuantity;

                OnPurchasedGenerator?.Invoke(generatorName, purchaseQuantity, upgradeCost);

                if (generator.Amount - purchaseQuantity == 0)
                    OnGeneratorFirstPurchase?.Invoke(generatorName);
                
                // Check if we will rank up
                if (amountGeneratorsUntilRankUp <= purchaseQuantity)
                {
                    OnGeneratorRankUp?.Invoke(generatorName);
                }
                
                return true;
            }
            return false;
        }

        /// <summary>
        /// Calculates the cost to buy the next n number of generators
        /// </summary>
        /// <returns>The generator purchase cost.</returns>
        /// <param name="generatorName">Generator name.</param>
        /// <param name="purchaseQuantity">Purchase quantity.</param>
        public virtual ResourceCost GetGeneratorUpgradeCost(string generatorName, int purchaseQuantity)
        {
            int amountOwned = GetGeneratorAmount(generatorName);

            // Make sure we cap to the max amount possible
            if ((amountOwned + purchaseQuantity) > _generatorsConfig.MaxAmountOfGenerators)
            {
                purchaseQuantity = _generatorsConfig.MaxAmountOfGenerators - amountOwned;
            }

            GeneratorItemModel generatorItemModel = GetGeneratorByName(generatorName);
            
            // The math for this can be found in the following post
            // https://gameanalytics.com/blog/idle-game-mathematics.html
            ResourceCost baseCost = generatorItemModel.UpgradeCost;

            float r = generatorItemModel.CostMultiplier;

            double firstTopCoefficient = Math.Pow(r, amountOwned);

            double secondTopCoefficient = Math.Pow(r, purchaseQuantity) - 1;

            double bottomCoefficient = r - 1;

            ResourceCost currentCost = new ResourceCost()
            {
                ResourceKey = baseCost.ResourceKey,
                Amount = baseCost.Amount * ((firstTopCoefficient * secondTopCoefficient) / bottomCoefficient)
            };
            
            // If we have any Modifier that decrements our time production
            if (_generatorModifiers.SpecificGeneratorsCostMultipliersByGroup.ContainsKey(generatorName))
            {
                currentCost.Amount *= _generatorModifiers.SpecificGeneratorsCostMultipliersByGroup[generatorName][GameplayModifierConstants.RESULT];
            }

            // Add the bonus for all of the generators
            if (_generatorModifiers.AllGeneratorCostMultiplierByGroup[GameplayModifierConstants.RESULT] > 0)
            {
                currentCost.Amount *= _generatorModifiers.AllGeneratorCostMultiplierByGroup[GameplayModifierConstants.RESULT];
            }

            return currentCost;
        }
        
        public virtual ResourceCost GetGeneratorUpgradeCost(string generatorName)
        {
            int amountOwned = GetGeneratorAmount(generatorName);

            // Make sure we cap to the max amount possible
            if (amountOwned + 1 > _generatorsConfig.MaxAmountOfGenerators)
            {
                return null;
            }

            GeneratorItemModel generatorItemModel = GetGeneratorByName(generatorName);
            
            // The math for this can be found in the following post
            // https://gameanalytics.com/blog/idle-game-mathematics.html
            ResourceCost baseCost = generatorItemModel.UpgradeCost;

            float r = generatorItemModel.CostMultiplier;

            double firstTopCoefficient = Math.Pow(r, amountOwned);

            ResourceCost currentCost = new ResourceCost()
            {
                ResourceKey = baseCost.ResourceKey,
                Amount = baseCost.Amount * firstTopCoefficient 
            };
            
            // If we have any Modifier that decrements our time production
            if (_generatorModifiers.SpecificGeneratorsCostMultipliersByGroup.ContainsKey(generatorName))
            {
                currentCost.Amount *= _generatorModifiers.SpecificGeneratorsCostMultipliersByGroup[generatorName][GameplayModifierConstants.RESULT];
            }

            // Add the bonus for all of the generators
            if (_generatorModifiers.AllGeneratorCostMultiplierByGroup[GameplayModifierConstants.RESULT] > 0)
            {
                currentCost.Amount *= _generatorModifiers.AllGeneratorCostMultiplierByGroup[GameplayModifierConstants.RESULT];
            }

            return currentCost;
        }

        /// <summary>
        /// Returns the amount of a single generator that the user has
        /// </summary>
        /// <returns>The generator amount.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual int GetGeneratorAmount(string generatorName)
        {
            int result = 0;

            GeneratorUserModel generator = _userManager.GetGeneratorFromUser(generatorName);
            if(generator != null)
            {
                result = generator.Amount;
            }

            return result;
        }

        /// <summary>
        /// Returns the amount of generators needed to be bought to reach next rank.
        /// </summary>
        /// <returns>The amount of generators until next rank.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual int GetAmountOfGeneratorsUntilNextRank(string generatorName)
        {
            int result = 0;

            // Grab the config
            GeneratorItemModel generatorConfig = GetGeneratorByName(generatorName);

            if(generatorConfig != null)
            {
                // Get the rank
                int currentRank = GetCurrentRank(generatorConfig);

                if (currentRank < GetMaxRank(generatorConfig))
                {
                    // Calculate the difference between the required amount and the amount we currently have
                    int requiredAmount = generatorConfig.Ranks[currentRank].RequiredAmount;
                    result = requiredAmount - GetGeneratorAmount(generatorName);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the amount of generators in this rank.
        /// </summary>
        /// <returns>The amount of generators in this rank.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual int GetAmountOfGeneratorsInThisRank(string generatorName)
        {
            // Grab the config
            GeneratorItemModel generatorConfig = GetGeneratorByName(generatorName);

            if (generatorConfig != null)
            {
                // Get the rank
                int currentRank = GetCurrentRank(generatorConfig);

                // Gets the max rank
                int maxRank = GetMaxRank(generatorConfig);

                // Get how many of this generator the user has
                int amountOwned = GetGeneratorAmount(generatorConfig.Name);

                //// If we want to buy until rank up, but we don't own any, just buy 1
                if (amountOwned == 0)
                    return 0;

                int prevRank = 0;
                if(currentRank == 0)
                {
                    prevRank = 0;
                }
                else if(currentRank < maxRank)
                {
                    prevRank = generatorConfig.Ranks[currentRank - 1].RequiredAmount;
                }
                else
                {
                    prevRank = generatorConfig.Ranks[maxRank - 1].RequiredAmount;
                }
                
                return amountOwned - prevRank;
            }

            return 0;
        }

        /// <summary>
        /// Returns the current rank of a generator
        /// </summary>
        /// <returns>The current rank.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual int GetCurrentRank(string generatorName)
        {
            return GetCurrentRank(GetGeneratorByName(generatorName));
        }

        /// <summary>
        /// Returns the current rank of a generator
        /// </summary>
        /// <returns>The current rank.</returns>
        /// <param name="generatorConfig">Generator config.</param>
        public virtual int GetCurrentRank(GeneratorItemModel generatorConfig)
        {
            int result = 0;

            if(generatorConfig.Ranks != null)
            {
                // Get how many of this generator the user has
                int amountOwned = GetGeneratorAmount(generatorConfig.Name);

                // Iterate through the ranks to see where are fall into
                for (int i = 0; i < generatorConfig.Ranks.Count; ++i)
                {
                    GeneratorRankModel rankModel = generatorConfig.Ranks[i];
                    if (amountOwned < rankModel.RequiredAmount)
                    {
                        break;
                    }

                    result++;
                }
            }

            return result;
        }
       
        
        /// <summary>
        /// Is the generator at max rank
        /// </summary>
        /// <returns><c>true</c>, if generator is at max rank, <c>false</c> otherwise.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual bool IsGeneratorAtMax(string generatorName)
        {
            // Get amount of generators owned
            int amountOwned = GetGeneratorAmount(generatorName);

            // Compare against the config's max amount
            return amountOwned >= _generatorsConfig.MaxAmountOfGenerators;
        }

        /// <summary>
        /// Returns the max rank of a generator
        /// </summary>
        /// <returns>The max rank.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual int GetMaxRank(string generatorName)
        {
            return GetMaxRank(GetGeneratorByName(generatorName));
        }

        /// <summary>
        /// Returns the max rank of a generator
        /// </summary>
        /// <returns>The max rank.</returns>
        /// <param name="generatorConfig">Generator config.</param>
        public virtual int GetMaxRank(GeneratorItemModel generatorConfig)
        {
            int result = 0;

            if (generatorConfig.Ranks != null)
            {
                result = generatorConfig.Ranks.Count;
            }

            return result;
        }

        /// <summary>
        /// The math for this can be found in the following post
        /// https://gameanalytics.com/blog/idle-game-mathematics.html        
        /// </summary>
        /// <returns>The max generators purchasable.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual int GetMaxGeneratorsPurchasable(string generatorName, int maxCap = 0)
        {
            GeneratorItemModel generatorItemModel = GetGeneratorByName(generatorName);

            ResourceCost upgradeCost = GetGeneratorUpgradeCost(generatorName);
            
            if (generatorItemModel != null && upgradeCost != null)
            {
                // The base cost (b)
                ResourceCost baseCost = generatorItemModel.UpgradeCost;
            
                // Amount of points (c)
                double amountResources = _resourceManager.GetResourceAmount(baseCost.ResourceKey);

                // The multiplier for growth (r)
                float growthMultiplier = generatorItemModel.CostMultiplier;

                double topCoefficient = amountResources * (growthMultiplier - 1);

                double bottomCoefficient = upgradeCost.Amount;

                double resultCoefficient = (topCoefficient / bottomCoefficient) + 1;

                int amountAvailableToPurchase = (int)Math.Floor(Math.Log(resultCoefficient, growthMultiplier));

                if(amountAvailableToPurchase == 0)
                    amountAvailableToPurchase = 1;

                if (maxCap != 0 && amountAvailableToPurchase > maxCap)
                    amountAvailableToPurchase = maxCap;
            
                return amountAvailableToPurchase;
            }

            return 0;
        }

        /// <summary>
        /// Get the max amount of generators you can have
        /// </summary>
        /// <returns></returns>
        public virtual int GetMaxAmountOfGenerators()
        {
            return _generatorsConfig.MaxAmountOfGenerators;
        }

        /// <summary>
        /// Applies the users rank modifiers.
        /// </summary>
        protected virtual void ApplyUsersRankModifiers()
        {
            GameplayModifiersManager.Instance.RemoveGameplayModifiersWithTag("Rank");
            
            // Grab all of the generators the user has
            Dictionary<string, GeneratorUserModel> generators = _userManager.GetAllGeneratorsFromUser();

            // Iterate through them
            foreach(KeyValuePair<string, GeneratorUserModel> userGenerator in generators)
            {
                // Grab it's config
                GeneratorItemModel generatorConfig = GetGeneratorByName(userGenerator.Key);

                if (generatorConfig != null)
                {
                    int amountOwned = userGenerator.Value.Amount;

                    for (int i = 0; i < generatorConfig.Ranks.Count; ++i)
                    {
                        GeneratorRankModel rankModel = generatorConfig.Ranks[i];

                        // If we dont have this rank, we won't have any of the next ones either
                        if (amountOwned < rankModel.RequiredAmount)
                        {
                            break;
                        }

                        ApplyGeneratorModifier(userGenerator.Key, rankModel.Modifier);
                    }   
                }
            }
        }

        /// <summary>
        /// Checks if a generator ranked up.
        /// </summary>
        /// <param name="generatorName">Generator name.</param>
        /// <param name="generatorModel">Generator model.</param>
        /// <param name="purchasedAmount">Purchased amount.</param>
        protected virtual void CheckIfGeneratorRankedUp(string generatorName, GeneratorUserModel generatorModel, int purchasedAmount)
        {
            // Grab the generator config config
            GeneratorItemModel generatorConfig = GetGeneratorByName(generatorName);

            // Get how many we own
            int amountOwned = generatorModel.Amount;

            // Iterate through the ranks
            for (int i = 0; i < generatorConfig.Ranks.Count; ++i)
            {
                // Get the GeneratorRankModel
                GeneratorRankModel rankModel = generatorConfig.Ranks[i];

                // If we dont have this rank
                if (amountOwned < rankModel.RequiredAmount)
                {
                    // Check if we would now own it with what we purchased
                    if (amountOwned + purchasedAmount >= rankModel.RequiredAmount)
                    {
                        ApplyGeneratorModifier(generatorName, rankModel.Modifier);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Applies the generator modifier.
        /// </summary>
        /// <param name="generatorName">Generator name.</param>
        /// <param name="gameplayModifier">Gameplay modifier.</param>
        protected virtual void ApplyGeneratorModifier(string generatorName, GameplayModifierModel gameplayModifier)
        {
            if (gameplayModifier != null)
            {
                // Clone the modifier
                GameplayModifierModel clonedModifier = (GameplayModifierModel)gameplayModifier.Clone();
                // Make sure it has set this generator as the specific generator
                ApplyModifierToSpecificGenerator(clonedModifier, generatorName);
                // Apply the modifier
                _gameplayModifiersManager.ApplyGameplayModifier(clonedModifier);
            }
        }

        /// <summary>
        /// Applies the modifier to a specific generator.
        /// </summary>
        /// <param name="gameplayModifier">Gameplay modifier.</param>
        /// <param name="generatorName">Generator name.</param>
        protected virtual void ApplyModifierToSpecificGenerator(GameplayModifierModel gameplayModifier, string generatorName)
        {
            if (gameplayModifier is SpecificGeneratorTimeMultiplier generatorTimeMultiplier)
            {
                generatorTimeMultiplier.GeneratorNames = new string[1] { generatorName };
                return;
            }
            else if (gameplayModifier is SpecificGeneratorBonusMultiplier generatorBonusMultiplier)
            {
                generatorBonusMultiplier.GeneratorNames = new string[1] { generatorName };
                return;
            }
            else if (gameplayModifier is SpecificGeneratorCostMultiplier generatorCostMultiplier)
            {
                generatorCostMultiplier.GeneratorNames = new string[1] { generatorName };
                return;
            }
            else
                return;
        }
    }
}
