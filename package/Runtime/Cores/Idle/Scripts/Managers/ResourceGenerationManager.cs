﻿using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.LTE;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    public class ResourceGenerationManager : LoadableSingleton<ResourceGenerationManager>
    {
        /// <summary>
        /// Event fired when a non Automated Generator That Waits for Collection is ready to be collected
        /// </summary>
        public static Action<string> OnResourcesReady;

        /// <summary>
        ///  Event fired when a Generator Started Idling (Waiting to generate resources)
        /// </summary>
        public static Action<string> OnGeneratorIdling;

        /// <summary>
        ///  Event fired when a generator is collected indicating which generator and the amount collected.
        /// </summary>
        public static Action<string, double> OnGeneratorCollected;
        
        /// <summary>
        /// Flag to control if we want to stop the resource generation
        /// </summary>
        public bool StopResourceGeneration;
        
        /// <summary>
        /// Tag of the generators you want to use
        /// </summary>
        public string GeneratorsTag = "";
        
        /// <summary>
        /// Event for when we finished processing offline resources
        /// </summary>
        public static Action<Dictionary<string, double>, long> OnProcessOfflineFinished;
        
        public static Action<Dictionary<string, double>, long> OnTimeWarpApplied;

        /// <summary>
        /// All of the generator configs
        /// </summary>
        protected List<GeneratorItemModel> _generatorModels;

        /// <summary>
        /// The last time we generated some points
        /// </summary>
        protected float _lastGenerationProcessTime;

        /// <summary>
        /// Cached version of GeneratorManager
        /// </summary>
        protected GeneratorManager _generatorManager;

        /// <summary>
        /// Cached version of ResourceManager
        /// </summary>
        protected ResourceManager _resourceManager;

        /// <summary>
        /// Cached version of AdBonusManager
        /// </summary>
        protected AdBonusManager _adBonusManager;

        /// <summary>
        /// Cached version of IdleUserManager
        /// </summary>
        protected IdleUserManager _idleUserManager;

        // Performance wise, this is used to throttle the max frequency of ticks
        protected const float MIN_TICK_THRESHOLD = 0.2f;

        // Min time to process offline (in seconds)
        protected long _minOfflineTime = 60;

        /// <summary>
        /// Resources generated per second
        /// </summary>
        protected double _generationPerSecond;

        /// <summary>
        /// Used to calculate the current generationPerSecond. Will go into _generationPerSecond once finished
        /// </summary>
        protected double _newGenerationPerSecond;

        /// <summary>
        /// Dictionary containing the resources generated
        /// </summary>
        protected Dictionary<string, double> _resourcesGenerated = new Dictionary<string, double>();
        
        public override void StartLoading()
        {
            // Cache the managers
            _generatorManager = GeneratorManager.Instance;
            _resourceManager = ResourceManager.Instance;
            _adBonusManager = AdBonusManager.Instance;
            _idleUserManager = IdleUserManager.Instance;

            LoadConfig();

            // Process offline
            ProcessOffline();

            // Broadcast that we finished loading
            FinishedLoading(true);
        }

        protected virtual void LoadConfig()
        {
            // Grab the game config
            IdleGameModel gameModel = ConfigHelper.LoadConfig<IdleGameModel>(LimitedTimeEventsManager.Instance.GetConfigurationMapping("game"));

            // Get the minOfflineTime from the gameConfig
            _minOfflineTime = gameModel.MinOfflineTime;

            // Set when we opened the game as the last time we processed the generation of resources
            _lastGenerationProcessTime = Time.realtimeSinceStartup;

            // Fetch all of the configs from the GeneratorManager
            if (string.IsNullOrEmpty(GeneratorsTag))
            {
                _generatorModels = _generatorManager.GetGenerators();
            }
            else
            {
                _generatorModels = _generatorManager.GetGeneratorsWithTag(GeneratorsTag);
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            PlayerManager.OnTimeTravelApplied += TimeTravelApplied;
        }

        void OnDisable()
        {
            PlayerManager.OnTimeTravelApplied -= TimeTravelApplied;
        }

        void TimeTravelApplied(int delta)
        {
            // Apply that time as a time warp
            ApplyTimewarp(delta);
        }

        /// <summary>
        /// Gets the generation per second
        /// </summary>
        /// <returns>The generation per second.</returns>
        public double GetGenerationPerSecond()
        {
            return _generationPerSecond;
        }

        /// <summary>
        /// Applies a timewarp.
        /// </summary>
        /// <param name="duration">Duration.</param>
        /// <param name="displayPopup">If set to <c>true</c> display popup.</param>
        public virtual void ApplyTimewarp(int duration, bool displayPopup = true)
        {
            if (duration > 0)
            {
                // Calculate the money earned during the timewarp
                Dictionary<string, double> generatedInTimewarp = CalculateProduction(duration);

                // Save the user after doing a time warp
                IdleUserManager.Instance.PersistUser();

                if (displayPopup)
                {
                    foreach (var resourceGenerated in generatedInTimewarp)
                    {
                        LogHelper.LogSdk(string.Format("Generated {0} of {1} with a timewarp", FormatHelper.Instance.FormatNumber(resourceGenerated.Value), resourceGenerated.Key));
                    }
                }
                
                OnTimeWarpApplied?.Invoke(generatedInTimewarp, duration);
            }
        }

        /// <summary>
        /// Check how much we've generated
        /// </summary>
        void Update()
        {
            if (IsLoaded)
            {
                if (!StopResourceGeneration)
                {
                    // Calculate the time that passed since we last generated some resources
                    float deltaTime = Time.realtimeSinceStartup - _lastGenerationProcessTime;

                    // Calculate the production
                    CalculateProduction(deltaTime);
                }

                // Set that the last time we generated resources was now
                _lastGenerationProcessTime = Time.realtimeSinceStartup;
            }
        }

        /// <summary>
        /// Calculates the production for the time elapsed
        /// </summary>
        /// <returns>The production.</returns>
        /// <param name="deltaTime">Delta time.</param>
        /// <param name="overrideAdCheck">If set to <c>true</c> override ad check.</param>
        /// <param name="overrideAdValue">If set to <c>true</c> override ad value.</param>
        protected virtual Dictionary<string, double> CalculateProduction(float deltaTime)
        {
            _newGenerationPerSecond = 0;
            _resourcesGenerated.Clear();
            
            // Check how much each generator produced
            foreach (GeneratorItemModel generator in _generatorModels)
            {
                CalculateProductionForGenerator(generator, deltaTime);
            }

            _generationPerSecond = _newGenerationPerSecond;

            foreach (var resourceGenerated in _resourcesGenerated)
            {
                // Trigger the event that we generated resources
                if(resourceGenerated.Value > 0)
                {
                    // Modify the resource
                    _resourceManager.ModifyResource(resourceGenerated.Key, resourceGenerated.Value, true);
                }            
            }
            

            return _resourcesGenerated;
        }

        /// <summary>
        /// Calculates the production for a given generator.
        /// </summary>
        /// <returns>The production for generator.</returns>
        /// <param name="generatorModel">Generator model.</param>
        /// <param name="deltaTime">Delta time elapsed.</param>
        /// <param name="overrideAdCheck">If set to <c>true</c>, it overrides the ad check, to whatever value is set in overrideAdValue</param>
        /// <param name="overrideAdValue">If set to <c>true</c>, it forces the ad bonus to be active.</param>
        protected virtual double CalculateProductionForGenerator(GeneratorItemModel generatorModel, float deltaTime)
        {
            double result = 0;

            // Grab the generatorUserModel
            GeneratorUserModel generatorUserModel = _idleUserManager.GetGeneratorFromUser(generatorModel.Name);

            // Get how many of this generator we have
            int amount = _generatorManager.GetGeneratorAmount(generatorModel.Name);
            if (amount > 0)
            {
                double productionTime = generatorUserModel.ProductionTime;
                
                if (generatorUserModel.GenerationState == GeneratorsStates.StatesGeneration.GENERATING)
                    // Calculate how much time passed since we last generated for this generator
                    productionTime = _generatorManager.ModifyGeneratorProductionTime(generatorModel.Name, deltaTime);

                // Get how long the production interval is
                double productionInterval = _generatorManager.GetProductionInterval(generatorModel);

                // Get how much we produce per interval
                double productionPerInterval =
                    _generatorManager.GetGeneratorProductionPerInterval(generatorModel, amount);

                _newGenerationPerSecond += productionInterval > 0
                    ? productionPerInterval / productionInterval
                    : productionPerInterval * productionInterval;
                
                double intervalCount = Math.Floor(productionTime / productionInterval);
                
                double timeConsumed = intervalCount * productionInterval;
                
                // If the generator needs an order to start generating and its not given nor is automated, just return 0
                if (generatorUserModel.WaitForGeneration &&
                    generatorUserModel.GenerationState != GeneratorsStates.StatesGeneration.GENERATING &&
                    generatorUserModel.CollectingState != GeneratorsStates.StatesCollection.COLLECTING)
                    return result;
                
                // If we already waited enough
                if (productionTime >= productionInterval && productionTime >= MIN_TICK_THRESHOLD)
                {
                    // Finished generating, evaluate if needs an order to collect and is not automated
                    if (generatorUserModel.WaitForCollection)
                    {
                        if (generatorUserModel.GenerationState == GeneratorsStates.StatesGeneration.GENERATING &&
                            generatorUserModel.CollectingState != GeneratorsStates.StatesCollection.COLLECTING)
                        {
                            generatorUserModel.CollectingState = GeneratorsStates.StatesCollection.WAITING_COLLECTION;
                            if (generatorUserModel.WaitForGeneration)
                            {
                                intervalCount = 1;
                                generatorUserModel.Generated += intervalCount * productionPerInterval;
                                generatorUserModel.GenerationState = GeneratorsStates.StatesGeneration.IDLE;
                                OnGeneratorIdling?.Invoke(generatorModel.Name);
                            }
                            else
                            {
                                generatorUserModel.Generated += intervalCount * productionPerInterval;
                                _generatorManager.ModifyGeneratorProductionTime(generatorModel.Name, -timeConsumed);
                            }
                            OnResourcesReady?.Invoke(generatorModel.Name);
                        }
                    }
                    else
                    {
                        if (generatorUserModel.WaitForGeneration)
                        {
                            intervalCount = 1;
                            timeConsumed = productionTime;
                            generatorUserModel.GenerationState = GeneratorsStates.StatesGeneration.IDLE;
                            OnGeneratorIdling?.Invoke(generatorModel.Name);
                        }
                        else
                        {
                            generatorUserModel.GenerationState = GeneratorsStates.StatesGeneration.GENERATING;
                        }

                        // award points
                        result = intervalCount * productionPerInterval;

                        // Modify the resources for the player
                        AddToResourcesCollected(generatorModel.ResourceGenerated, result);
                        
                        OnGeneratorCollected?.Invoke(generatorModel.Name, result);

                        // Update the timer
                        _generatorManager.ModifyGeneratorProductionTime(generatorModel.Name, -timeConsumed);
                    }
                }
                
                if (generatorUserModel.CollectingState == GeneratorsStates.StatesCollection.COLLECTING)
                {
                    if (generatorUserModel.WaitForGeneration)
                    {
                        _generatorManager.ModifyGeneratorProductionTime(generatorModel.Name, -productionTime);
                    }

                    // award points
                    result = generatorUserModel.Generated;
                    generatorUserModel.Generated = 0;
                    generatorUserModel.CollectingState = GeneratorsStates.StatesCollection.EMPTY;

                    // Modify the resources for the player
                    AddToResourcesCollected(generatorModel.ResourceGenerated, result);
                    
                    OnGeneratorCollected?.Invoke(generatorModel.Name, result);
                    
                }
            }

            return result;
        }

        protected void AddToResourcesCollected(string resourceKey, double amount)
        {
            if (_resourcesGenerated.ContainsKey(resourceKey))
            {
                _resourcesGenerated[resourceKey] += amount;
            }
            else
            {
                _resourcesGenerated.Add(resourceKey, amount);
            }
        }

        protected virtual void ProcessOffline()
        {
            // Get when was the last time we saved the user
            long lastIdleTick = PlayerManager.Instance.GetTimer(TimersConstants.LAST_IDLE, GameModeManager.CurrentGameMode);

            // Get how many seconds passed since that time
            long offlineDuration = TimeHelper.GetTimeInServer() - lastIdleTick;

            Dictionary<string, double> totalGenerated = new Dictionary<string, double>();

            // If enough time passed
            if (lastIdleTick != 0 && offlineDuration > _minOfflineTime)
            {
                // If we still have the bonus, we've had the bonus all of the offline duration
                if (_adBonusManager != null && _adBonusManager.IsAdBonusActive())
                {
                    totalGenerated = CalculateProduction(offlineDuration);
                    foreach (var resourceGenerated in totalGenerated)
                    {
                        totalGenerated[resourceGenerated.Key] *= _adBonusManager.GetAdBonusMultiplier();
                    }
                }
                // If we didn't have the bonus active
                else
                {
                    if (_adBonusManager != null && _adBonusManager.GetAmountStackedAds() > 0)
                    {
                        // Get when the ad bonus expired
                        long bonusExpirationTime = _adBonusManager.GetBonusExpirationTime();

                        long timeWithBonus = bonusExpirationTime - lastIdleTick;

                        long timeWithoutBonus = TimeHelper.GetTimeInServer() - bonusExpirationTime;

                        // Calculate the production generated with the ad bonus active
                        var generatedWithBonus = CalculateProduction(timeWithBonus);
                        foreach (var resourceGenerated in totalGenerated)
                        {
                            generatedWithBonus[resourceGenerated.Key] *= _adBonusManager.GetAdBonusMultiplier();
                        }
                        
                        // Add the production generated without ad bonus active
                        var generatedWithoutBonus = CalculateProduction(timeWithoutBonus);

                        foreach (var resourceGenerated in totalGenerated)
                        {
                            totalGenerated[resourceGenerated.Key] = generatedWithBonus[resourceGenerated.Key] + generatedWithoutBonus[resourceGenerated.Key];
                        }
                    }
                    else
                    {
                        // If we didnt have bonus, nor have any stacks, it means no bonus was used, so just calculate normaly 
                        totalGenerated = CalculateProduction(offlineDuration);
                    }
                }
            }
            
            // Trigger the event that we finished processing offline bonus
            OnProcessOfflineFinished?.Invoke(totalGenerated, offlineDuration);
        }
    }
}
