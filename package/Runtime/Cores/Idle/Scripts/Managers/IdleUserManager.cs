using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.LTE;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// Idle user manager.
    /// </summary>
    public class IdleUserManager : LoadableSingleton<IdleUserManager>
    {
        public const string PROGRESS_SAVED_STAT = "progress";
        public const string LEVEL_SAVED_STAT = "level";

        /// <summary>
        /// Event for when the user is saved
        /// </summary>
        public static Action OnIdleUserSaved;
        
        /// <summary>
        /// Model containing all of the information about the User
        /// </summary>
        protected IdleUserModel _userModel;

        /// <summary>
        /// Reference to the PlayerManager
        /// </summary>
        protected PlayerManager _playerManager;

        /// <summary>
        /// Reference to the LimitedTimeEventsManager
        /// </summary>
        protected LimitedTimeEventsManager _eventsManager;
        
        /// <summary>
        /// Reference to the IdleGameModel
        /// </summary>
        protected IdleGameModel _gameModel;

        /// <summary>
        /// The key that holds the idle user data
        /// </summary>
        protected string _idleUserKey;
        
        public override void StartLoading()
        {
            // Cache the Managers
            _playerManager = PlayerManager.Instance;
            _eventsManager = LimitedTimeEventsManager.Instance;
            
            LoadIdleUserData();

            // Broadcast that we finished loading
            FinishedLoading(true);
        }

        /// <summary>
        /// Load data
        /// </summary>
        protected virtual void LoadIdleUserData()
        {
            // Grab the game config
            _gameModel = ConfigHelper.LoadConfig<IdleGameModel>(_eventsManager.GetConfigurationMapping("game"));
            
            // Set the key to use for the idle user
            _idleUserKey = "idle_user";
            
            // Try to read the data from the PlayerData. If it's empty, it means it's a new user
            string userJson = _playerManager.GetFromPlayerData(_idleUserKey, GameModeManager.CurrentGameMode);
            
            if (!string.IsNullOrEmpty(userJson))
            {
                _userModel = JsonHelper.DesarializeObject<IdleUserModel>(userJson);
            }
            else
            {
                CreateInitialIdleUser();
            }
        }

        /// <summary>
        /// Creates the initial idle user.
        /// </summary>
        protected virtual void CreateInitialIdleUser ()
        {
            _userModel = new IdleUserModel();

            // Add the starting balance to the user
            _userModel.Resources.Add(ResourceManager.POINTS, _gameModel.StartingBalance);
        }

        /// <summary>
        /// Returns the user model
        /// </summary>
        /// <returns>The user.</returns>
        public IdleUserModel GetUser()
        {
            return _userModel;
        }

        /// <summary>
        /// Persists the user in the backend
        /// </summary>
        public void PersistUser(bool sendEvent = true)
        {
            CancelInvoke();
            
            DoPersist(sendEvent);
        }

        /// <summary>
        /// Populates the data for the user, and set's it in the backend
        /// </summary>
        void DoPersist(bool sendEvent)
        {
            Dictionary<string, DataUpdateModel> data = new Dictionary<string, DataUpdateModel>();
            SetData(data);

            // Save all of the information in playerData
            _playerManager.SetPlayerData(data, GameModeManager.CurrentGameMode);
            if (sendEvent)
                // Send the event that the user is saved
                OnIdleUserSaved?.Invoke();
        }

        /// <summary>
        /// Sets the data to persist.
        /// </summary>
        /// <param name="data">Data.</param>
        protected virtual void SetData (Dictionary<string, DataUpdateModel> data)
        {
           _playerManager.SetTimer(TimersConstants.LAST_IDLE, TimeHelper.GetTimeInServer(), GameModeManager.CurrentGameMode);
           data.Add(_idleUserKey, new DataUpdateModel(JsonHelper.SerializeObject(_userModel)));
        }

        #region generators

        /// <summary>
        /// Return the GeneratorUserModel for a specific generator
        /// </summary>
        /// <returns>The generator from user.</returns>
        /// <param name="generatorName">Generator name.</param>
        public virtual GeneratorUserModel GetGeneratorFromUser(string generatorName)
        {
            GeneratorManager generatorManager = GeneratorManager.Instance;
            // If the generator doesn't exist, create it and add it to the user
            //to create it the generator manager has to be loaded
            if (!_userModel.Generators.ContainsKey(generatorName) && generatorManager != null && generatorManager.IsLoaded)
            {
                GeneratorItemModel generatorModel = generatorManager.GetGeneratorByName(generatorName);
                GeneratorsStates.StatesGeneration genState = GeneratorsStates.StatesGeneration.IDLE;
                if(generatorModel != null)
                {
                    if (!generatorModel.WaitForGeneration)
                        genState = GeneratorsStates.StatesGeneration.GENERATING;
                    //create the intial user generator
                    GeneratorUserModel newGenerator = new GeneratorUserModel()
                    {
                        WaitForCollection = generatorModel.WaitForCollection,
                        WaitForGeneration = generatorModel.WaitForGeneration,
                        CollectingState = GeneratorsStates.StatesCollection.EMPTY,
                        GenerationState = genState
                    };
                    _userModel.Generators.Add(generatorName, newGenerator);
                }
                else
                {
                    return null;
                }
            }
            // Return the generator
            return _userModel.Generators[generatorName];
        }

        /// <summary>
        /// Gets all generators from user.
        /// </summary>
        /// <returns>The all generators from user.</returns>
        public Dictionary<string, GeneratorUserModel> GetAllGeneratorsFromUser()
        {
            // Return the generators
            return _userModel.Generators;
        }

        /// <summary>
        /// Resets all of the generators
        /// </summary>
        public virtual void SoftResetGenerators()
        {
            // Iterate through the generators
            foreach(KeyValuePair<string, GeneratorUserModel> generatorUserModel in _userModel.Generators)
            {
                GeneratorItemModel generatorItemModel =
                    GeneratorManager.Instance.GetGeneratorByName(generatorUserModel.Key);
                GeneratorsStates.StatesGeneration genState = GeneratorsStates.StatesGeneration.IDLE;
                if (!generatorItemModel.WaitForGeneration)
                    genState = GeneratorsStates.StatesGeneration.GENERATING;
                
                generatorUserModel.Value.Amount = 0;
                generatorUserModel.Value.ProductionTime = 0;
                generatorUserModel.Value.Generated = 0;
                generatorUserModel.Value.WaitForGeneration = generatorItemModel.WaitForGeneration;
                generatorUserModel.Value.WaitForCollection = generatorItemModel.WaitForCollection;
                generatorUserModel.Value.CollectingState = GeneratorsStates.StatesCollection.EMPTY;
                generatorUserModel.Value.GenerationState = genState;
            }
        }

        #endregion

        #region savedStats

        /// <summary>
        /// Gets the saved stat.
        /// </summary>
        /// <param name="savedStatName">Saved stat name.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <returns>The saved stat.</returns>
        public SavedStatModel GetSavedStat(string savedStatName, string gameMode = GameModeConstants.NONE)
        {
            if (gameMode == GameModeConstants.NONE)
                gameMode = GameModeManager.CurrentGameMode;
            
            IdleUserModel userModel = _userModel;
            
            if (gameMode != GameModeManager.CurrentGameMode)
            {
                userModel = JsonHelper.DesarializeObject<IdleUserModel>(
                    _playerManager.GetFromPlayerData(_idleUserKey, gameMode));
            }

            // If the saved stat doesn't exist, create it and add it to the user
            if (!userModel.SavedStats.ContainsKey(savedStatName))
            {
                SavedStatModel newStat = new SavedStatModel();
                userModel.SavedStats.Add(savedStatName, newStat);
            }

            // Return the saved stat
            return userModel.SavedStats[savedStatName];
        }

        /// <summary>
        /// Resets all saved stats.
        /// </summary>
        public void SoftResetStats()
        {
            // Iterate through all of the saved stats
            foreach(KeyValuePair<string, SavedStatModel> savedStat in _userModel.SavedStats)
            {
                ResetGameSavedStat(savedStat.Key);
            }
        }

        /// <summary>
        /// Updates the saved stat.
        /// </summary>
        /// <param name="savedStatName">Saved stat name.</param>
        /// <param name="delta">Delta.</param>
        /// <param name="increment">Should we always increment this value?</param>
        public void UpdateSavedStat(string savedStatName, double delta, bool increment = true)
        {
            SavedStatModel savedStat = GetSavedStat(savedStatName);
            
            savedStat.Game += delta;
            
            if(increment)
            {
                savedStat.All += delta;
                savedStat.Life += delta;
            }
            else
            {
                if (savedStat.Game > savedStat.All)
                    savedStat.All = savedStat.Game;
                if (savedStat.Game > savedStat.Life)
                    savedStat.Life = savedStat.Game;
            }

            if (savedStat.All > savedStat.AllMax)
                savedStat.AllMax = savedStat.All;

            if (savedStat.Life > savedStat.LifeMax)
                savedStat.LifeMax = savedStat.Life;
        }

        /// <summary>
        /// Resets the saved stat for the game
        /// </summary>
        /// <param name="savedStatName">Saved stat name.</param>
        public void ResetGameSavedStat(string savedStatName)
        {
            SavedStatModel savedStat = GetSavedStat(savedStatName);

            savedStat.Game = 0;
        }

        #endregion
        
        /// <summary>
        /// Returns the game custom data value.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <returns></returns>
        public object GetCustomDataValue (string key)
        {
            if (_gameModel.CustomData.ContainsKey(key))
                return _gameModel.CustomData[key];
            return null;
        }
    }
    
    
}
