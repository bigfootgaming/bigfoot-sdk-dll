﻿using System;
using System.Collections;
using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.LTE;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    public class LevelManager : LoadableSingleton<LevelManager>
    {
        /// <summary>
        /// Event for when the player reaches a new level
        /// </summary>
        public static Action OnNewLevelReached;

        /// <summary>
        /// Cached LevelConfig
        /// </summary>
        LevelModel _levelModel;
        
        /// <summary>
        /// Public Getter for LevelModels
        /// </summary>
        public LevelModel LevelModel
        {
            get { return _levelModel; }
        }

        /// <summary>
        /// Cached IdleUserManager
        /// </summary>
        IdleUserManager _userManager;

        /// <summary>
        /// Cached saved stat for the progress of the player
        /// </summary>
        readonly SavedStatModel _progressSavedStat;

        void OnDestroy()
        {
            // Unsubscribe from the resource balance changed when we are destroyed
            ResourceManager.OnResourceBalanceChanged -= EarningsChanged;
        }

        public override void StartLoading()
        {
            // Cache the user manager
            _userManager = IdleUserManager.Instance;

            // Get the config
            _levelModel = ConfigHelper.LoadConfig<LevelModel>(LimitedTimeEventsManager.Instance.GetConfigurationMapping("levels"));

            // Subscribe to the event so we know when to update the player's progress
            ResourceManager.OnResourceBalanceChanged += EarningsChanged;

            // Broadcast that we finished loading
            FinishedLoading(true);
        }

        /// <summary>
        /// Earnings changed, so we have to update our saved stat
        /// </summary>
        /// <param name="resourceName">Resource name.</param>
        /// <param name="delta">Delta.</param>
        void EarningsChanged(string resourceName, double delta)
        {
            // Only change it if the resource that changed are the points and we are adding
            if(resourceName == ResourceManager.POINTS && delta > 0)
            {
                _userManager.UpdateSavedStat(IdleUserManager.PROGRESS_SAVED_STAT, delta);
            }
        }

        /// <summary>
        /// Returns the goal for a level
        /// </summary>
        /// <returns>The level goal.</returns>
        public double GetLevelGoal(int forLevel = -1)
        {
            int level;

            if(forLevel == -1)
            {
                // Grab the current level from the saved stat
                level = (int)_userManager.GetSavedStat(IdleUserManager.LEVEL_SAVED_STAT).Game;
            }
            else
            {
                level = forLevel;
            }

            // TODO: Take this into consideration when we have the hard reset
            bool didHardReset = false;

            // Pick the appropiate base and growth taking into consideration if we did a hard reset or not
            double baseToUse = (didHardReset && _levelModel.HardResetLevelBase > 0) ? _levelModel.HardResetLevelBase : _levelModel.LevelBase;
            double growthToUse = (didHardReset && _levelModel.HardResetLevelGrowth > 0) ? _levelModel.HardResetLevelGrowth : _levelModel.LevelGrowth;

            // Math is base * (growth^level)
            double growth = Math.Pow(growthToUse, level);
            return (baseToUse * growth);
        }

        /// <summary>
        /// Claims the level
        /// </summary>
        /// <param name="grantedReward">TransactionResultModel with Granted Reward</param>
        public void ClaimLevel(Action<TransactionResultModel> grantedReward = null, int forLevel = -1)
        {
            SavedStatModel levelSavedStat = _userManager.GetSavedStat(IdleUserManager.LEVEL_SAVED_STAT);
            double maxPreviousLevel = levelSavedStat.AllMax;
            
            if (levelSavedStat.Game == levelSavedStat.AllMax)
            {
                // Grab the current level goal
                double currentLevelGoal = GetLevelGoal();

                // Reward the user with that points
                ResourceManager.Instance.ModifyResource(ResourceManager.POINTS, currentLevelGoal);

                ITransaction reward; 
                
                // Grab the current reward for the level
                if (forLevel == -1)
                    reward = GetRewardForCurrentLevel();
                else
                    reward = GetRewardForLevel(forLevel);
            
                if(reward != null)
                {
                    TransactionResultModel model = reward.ApplyTransaction("LevelWon");
                    grantedReward?.Invoke(model);
                }
            }
            
            // Increment the level we are at
            _userManager.UpdateSavedStat(IdleUserManager.LEVEL_SAVED_STAT, 1, false);
            
            // If we reached a new max level
            if (levelSavedStat.AllMax > maxPreviousLevel)
            {
                var data = new Dictionary<string, object>()
                {
                    {"newMaxLevel", levelSavedStat.AllMax}
                };
                AnalyticsManager.Instance.TrackCustomEvent("levelUp", data);

                // If we are on an event, save the max level reached in the counters
                if (GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                {
                    var activeEvent = LimitedTimeEventsManager.Instance.ActiveEvent;
                    if (activeEvent != null)
                    {
                        PlayerManager.Instance.SetCounter(CountersConstants.LTE_MAX_LEVEL_REACHED, (int)levelSavedStat.AllMax, GameModeManager.CurrentGameMode);
                    }
                }
            }

            // Reset the accumulated points
            _userManager.ResetGameSavedStat(IdleUserManager.PROGRESS_SAVED_STAT);

            // Let everyone know we reached a new level
            OnNewLevelReached?.Invoke();

            // Force save the user
            _userManager.PersistUser();
        }

        public ITransaction GetRewardForCurrentLevel()
        {
            int currentLevel = (int)_userManager.GetSavedStat(IdleUserManager.LEVEL_SAVED_STAT).Game;
            if (_levelModel.LevelRewards != null && _levelModel.LevelRewards.Count > currentLevel)
            {
                return _levelModel.LevelRewards[currentLevel];
            }

            return null;
        }

        public ITransaction GetRewardForLevel(int level)
        {
            if (_levelModel.LevelRewards != null && _levelModel.LevelRewards.Count > level)
            {
                return _levelModel.LevelRewards[level];
            }

            return null;
        }
    }
}
