using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.Notifications;
using BigfootSdk.Tutorials;
using UnityEngine;
using BigfootSdk.LTE;

namespace BigfootSdk.Core.Idle
{
    public class AdBonusManager : LoadableSingleton<AdBonusManager>
    {
        /// <summary>
        /// Event for when an ad is unstacked
        /// </summary>
        public static Action OnAdUnstacked;

        /// <summary>
        /// Event for when an ad is stacked
        /// </summary>
        public static Action OnAdStacked;

        /// <summary>
        /// Cached reference to the AdsConfig
        /// </summary>
        protected AdsModel _adsModel;

        /// <summary>
        /// Cached reference to the PlayerManager
        /// </summary>
        protected PlayerManager _playerManager;

        /// <summary>
        /// Cached reference to the LocalNotificationsManager
        /// </summary>
        protected LocalNotificationsManager _localNotificationsManager;
        
        protected override void OnEnable()
        {
            base.OnEnable();

            PlayerManager.OnTimeTravelApplied += TimeTravelApplied;
            ResourceGenerationManager.OnProcessOfflineFinished += OfflineFinished;
            IdleUserManager.OnIdleUserSaved += ScheduleNoAdsNotification;
        }

        protected virtual void OnDisable()
        {
            PlayerManager.OnTimeTravelApplied -= TimeTravelApplied;
            ResourceGenerationManager.OnProcessOfflineFinished -= OfflineFinished;
            IdleUserManager.OnIdleUserSaved -= ScheduleNoAdsNotification;
        }

        public override void StartLoading()
        {
            // Cache the managers
            _playerManager = PlayerManager.Instance;
            _localNotificationsManager = LocalNotificationsManager.Instance;

            // Grab the config
            _adsModel = ConfigHelper.LoadConfig<AdsModel>(LimitedTimeEventsManager.Instance.GetConfigurationMapping("ads"));

            // If we have any stacked ads, set a notification for when they will run out
            if (_localNotificationsManager != null)
            {
                if (GetAmountStackedAds() > 0)
                {
                    // Schedule the notification
                    _localNotificationsManager.SendNotification("AdBonusExpires", GetBonusTimeRemaining());
                }
                else
                {
                    _localNotificationsManager.SendNotification("NoAdBonus");
                }
            }

            // Broadcast that we finished loading
            FinishedLoading(true);
        }
        
        /// <summary>
        /// Time Travel was done
        /// </summary>
        /// <param name="delta">Delta of time traveled.</param>
        protected virtual void TimeTravelApplied(int delta)
        {
            // Cancel all previous checks
            CancelInvoke();

            // Check again if we should unstack
            CheckIfShouldUnstack();
        }

        /// <summary>
        /// Checks if should unstack any ad counter
        /// </summary>
        protected virtual void CheckIfShouldUnstack()
        {
            if (_adsModel != null)
            {
                // How long each individual duration lasts
                int bonusDuration = _adsModel.AdBonusDuration;

                // How many ads the player has stacked currently
                int stackedAds = GetAmountStackedAds();
                if(stackedAds > 0)
                {
                    // Whats the bonus time remaining for the bonus
                    long bonusTimeRemaining = GetBonusTimeRemaining();

                    // If it's < 0, it means all bonus is off
                    if(bonusTimeRemaining < 0)
                    {
                        // Unstack all
                        _playerManager.SetCounter(CountersConstants.ADS_STACKED, 0, GameModeManager.CurrentGameMode);

                        RemoveAdBonusModifier();
                        
                        // Send the event
                        OnAdUnstacked?.Invoke();
                    }
                    else
                    {
                        // Calculate how many "Stacks" we curently have
                        float division = (float)bonusTimeRemaining / (float)bonusDuration;

                        // Calculate the whole part of the division above
                        int wholePart = (int)System.Math.Truncate(division);

                        // Calculate the ceiling of the whole part
                        int ceiling = wholePart + 1;

                        // Calculate the remainder of the current stack
                        float remainder = division - wholePart;

                        // Calculate the difference between have many we have stacked in reality, and how many we should have based on the bonusTimeRemaining
                        int difference = stackedAds - ceiling;
                        if (difference > 0)
                        {
                            // If we have a difference, we need to subtract them from the counter
                            _playerManager.SetCounter(CountersConstants.ADS_STACKED, difference * -1, GameModeManager.CurrentGameMode, true);
                            
                            // Increment when we first started watching an ad by bonusDuration
                            _playerManager.SetTimer(TimersConstants.ADS_STARTED, difference * bonusDuration, GameModeManager.CurrentGameMode,true);

                            // Send the event
                            OnAdUnstacked?.Invoke();
                        }

                        if (remainder > 0)
                        {
                            // We need to check again when to unstack (the +1 is to add one extra second, so the numbers are not round, and the above logic is simpler)
                            StartCoroutine(WaitUntilCheckUnstack(remainder * bonusDuration + 1));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called when the offline progress finished loading
        /// </summary>
        /// <param name="amountGenerated">Amount generated.</param>
        /// <param name="timeOffline">Time offline.</param>
        protected virtual void OfflineFinished(Dictionary<string, double> amountGenerated, long timeOffline)
        {
            //after the offline progress calculated, if the ad bonus is still active, add the modifier to be used during online generation
            if (IsAdBonusActive())
                AddAdBonusModifier();
            CheckIfShouldUnstack();
        }

        
        /// <summary>
        /// Add gameplay modifier
        /// </summary>
        protected virtual void AddAdBonusModifier()
        {
            AllGeneratorBonusMultiplier agbm = new AllGeneratorBonusMultiplier()
            {
                Group = "AdBonus",
                Tag = "AdBonus",
                Multiplier = GetAdBonusMultiplier()
            };
            GameplayModifiersManager.Instance.ApplyGameplayModifier(agbm);
        }

        //Remove gameplay modifier
        protected virtual void RemoveAdBonusModifier()
        {
            GameplayModifiersManager.Instance.RemoveGameplayModifiersWithTag("AdBonus");
        }

        /// <summary>
        /// Coroutine to wait until we need to check if we should unstack an ad
        /// </summary>
        /// <param name="seconds"> Seconds to wait</param>
        /// <returns></returns>
        protected virtual IEnumerator WaitUntilCheckUnstack(float seconds)
        {
            long startedTime = TimeHelper.GetTimeInServer();
            while (TimeHelper.GetTimeInServer() - startedTime < seconds)
            {
                yield return new WaitForSeconds(1);
            }
            
            CheckIfShouldUnstack();
        }

        /// <summary>
        /// Returns if the ad bonus is active
        /// </summary>
        /// <returns><c>true</c>, if ad bonus active is active, <c>false</c> otherwise.</returns>
        public virtual bool IsAdBonusActive()
        {
            // When was the last time we started a bonus
            long lastBonusStartTime = _playerManager.GetTimer(TimersConstants.ADS_STARTED, GameModeManager.CurrentGameMode);

            if (lastBonusStartTime > 0)
            {
                // Grab the total duration of the bonus
                long bonusDuration = GetTotalBonusDuration();

                if (bonusDuration > 0)
                {
                    // Get how long passed since the lastBonusStartTime
                    long timeSinceLastBonusWasStarted = TimeHelper.GetTimeElapsedSinceEvent(lastBonusStartTime);

                    // If the time that passed, is less than the duration, it means we still have the bonus active
                    if (timeSinceLastBonusWasStarted < bonusDuration)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the bonus time remaining.
        /// </summary>
        /// <returns>The bonus time remaining.</returns>
        public long GetBonusTimeRemaining()
        {
            long bonusEndTime = GetBonusExpirationTime();
            if (bonusEndTime > 0)
            {
                return TimeHelper.GetTimeRemainingUntilEvent(bonusEndTime);
            }

            return 0;
        }

        /// <summary>
        /// Gets the total duration of the bonus.
        /// </summary>
        /// <returns>The total bonus duration.</returns>
        int GetTotalBonusDuration()
        {
            return _adsModel.AdBonusDuration * GetAmountStackedAds();
        }

        /// <summary>
        /// Gets the bonus expiration time.
        /// </summary>
        /// <returns>The bonus expiration time.</returns>
        public virtual long GetBonusExpirationTime()
        {
            // Get when the lastBonus started
            long lastBonusStartTime = _playerManager.GetTimer(TimersConstants.ADS_STARTED, GameModeManager.CurrentGameMode);

            if (lastBonusStartTime > 0)
            {
                // Add to that time, the amount of ads stacked times the duration of an individual bonus
                return lastBonusStartTime + (GetAmountStackedAds() * _adsModel.AdBonusDuration);
            }

            return 0;
        }

        /// <summary>
        /// Called when the player watches an ad
        /// </summary>
        public virtual void AdWatched()
        {
            // Make sure he can watch more ads
            if(CanWatchMoreAds())
            {
                int stackedAdsCount = GetAmountStackedAds();

                if (stackedAdsCount == 0)
                {
                    // If it's the first ad watched, set the startTime to now
                    _playerManager.SetTimer(TimersConstants.ADS_STARTED, TimeHelper.GetTimeInServer(), GameModeManager.CurrentGameMode);
                    
                    //Add the gameplay modifier
                    AddAdBonusModifier();

                    // Check in the adBonusDuration, to unstack this ad (the +1 is to add one extra second, so the numbers are not round, and the above logic is simpler)
                    StartCoroutine(WaitUntilCheckUnstack( _adsModel.AdBonusDuration + 1));
                }

                // Increment the counter by one
                _playerManager.SetCounter(CountersConstants.ADS_STACKED, 1, GameModeManager.CurrentGameMode, true);

                // Schedule the notification
                if(_localNotificationsManager != null)
                    _localNotificationsManager.SendNotification("AdBonusExpires", GetBonusTimeRemaining());

                // Send the event that we watched an ad
                OnAdStacked?.Invoke();
            }
            else
            {
				Debug.LogWarning("Player is trying to watch ads, but he can't watch any more");
            }
        }
        
        /// <summary>
        /// Gets the ad bonus multiplier.
        /// </summary>
        /// <returns>The ad bonus multiplier.</returns>
        public float GetAdBonusMultiplier()
        {
            return _adsModel.AdBonusMultiplier;
        }

        /// <summary>
        /// Cans the user watch more ads.
        /// </summary>
        /// <returns><c>true</c>, if the user can watch more ads, <c>false</c> otherwise.</returns>
        public bool CanWatchMoreAds()
        {
            return GetAmountStackedAds() < _adsModel.MaxAdsStacked;
        }

        /// <summary>
        /// Gets the amount stacked ads.
        /// </summary>
        /// <returns>The amount stacked ads.</returns>
        public virtual int GetAmountStackedAds()
        {
            return _playerManager.GetCounter(CountersConstants.ADS_STACKED, GameModeManager.CurrentGameMode);
        }
        
        /// <summary>
        /// Gets the max amount of ads
        /// </summary>
        /// <returns>The max amount of ads.</returns>
        public int GetMaxAmountOfAds()
        {
            return _adsModel.MaxAdsStacked;
        }

        // Schedule the notification that no ad has been left set
        protected virtual void ScheduleNoAdsNotification()
        {
            if (_localNotificationsManager != null)
            {
                int stackedAds = GetAmountStackedAds();
                if (stackedAds == 0)
                {
                    // Schedule the notification
                    _localNotificationsManager.SendNotification("NoAdBonus");
                }
                else
                {
                    // Look for the scheduled notification
                    LocalNotificationModel notification =_localNotificationsManager.GetLocalNotifications()?.FirstOrDefault(notif => notif.Id == "NoAdBonus");
                    // Cancel the notification in case it exists
                    if(notification != null)
                        _localNotificationsManager.CancelNotification(notification.Id);
                }
            }
        }
    }
}
