﻿using BigfootSdk;
using TMPro;
using UnityEngine;

/// <summary>
/// Upgrade grid item.
/// </summary>
public class UpgradeGridItem : MonoBehaviour
{
    /// <summary>
    /// The name of the upgrade.
    /// </summary>
    public TextMeshProUGUI UpgradeName;

    /// <summary>
    /// The config.
    /// </summary>
    UpgradeModel _config;

    /// <summary>
    /// Populates the upgrade grid item.
    /// </summary>
    /// <param name="config">Config.</param>
    public void PopulateUpgradeGridItem(UpgradeModel config)
    {
        // Cache the config
        _config = config;

        // Update the name
        UpgradeName.text = config.UpgradeName;
    }

    /// <summary>
    /// Buy the upgrade on click
    /// </summary>
	public void OnClick() 
    {
        UpgradesManager.Instance.BuyUpgrade(_config.UpgradeName);
	}
}