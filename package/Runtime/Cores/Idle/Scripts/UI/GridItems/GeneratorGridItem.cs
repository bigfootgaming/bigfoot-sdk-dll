﻿using System;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk.Core.Idle
{
    public class GeneratorGridItem : MonoBehaviour
    {
        /// <summary>
        /// Event to refresh a generator. If string is empty, refreshes all
        /// </summary>
        public static Action<string> OnRefreshGeneratorGridItem;

        public static Action<string> OnGridItemPopulated;

        /// <summary>
        /// Name of the generator
        /// </summary>
        public TextMeshProUGUI NameLabel;

        /// <summary>
        /// How many are we trying to buy
        /// </summary>
        public TextMeshProUGUI BuyQuantityLabel;

        /// <summary>
        /// How much does it cost
        /// </summary>
        public TextMeshProUGUI CostAmountLabel;

        /// <summary>
        /// What resource does it cost
        /// </summary>
        public TextMeshProUGUI CostKeyLabel;

        /// <summary>
        /// How much time does it take to produce
        /// </summary>
        public TextMeshProUGUI ProductionIntervalLabel;

        /// <summary>
        /// Display when we are maxed out
        /// </summary>
        public TextMeshProUGUI MaxAmountLabel;

        /// <summary>
        /// The cost icon
        /// </summary>
        public Image CostIcon;
        
        /// <summary>
        /// Buy Button
        /// </summary>
        public Button BuyButton;

        /// <summary>
        /// The formatter to use
        /// </summary>
        public string CostFormatterName = FormatHelper.DEFAULT_FORMATTER;
        
        /// <summary>
        /// The formatter to use
        /// </summary>
        public string ProductionTimerFormatterName = FormatHelper.DEFAULT_FORMATTER;
        
        /// <summary>
        /// Config for this generator
        /// </summary>
        protected GeneratorItemModel _generatorModel;


        /// <summary>
        /// UserModel for this generator
        /// </summary>
        protected GeneratorUserModel _generatorUserModel; 

        /// <summary>
        /// Cached instance to GeneratorManager
        /// </summary>
        protected GeneratorManager _generatorManager;

        /// <summary>
        /// Cached instance to ResourceManager
        /// </summary>
        protected ResourceManager _resourceManager;

        /// <summary>
        /// Cached instance to IdleUserManager
        /// </summary>
        protected IdleUserManager _userManager;

        /// <summary>
        /// The rank up progress bar.
        /// </summary>
        public ProgressBar RankUpProgressBar;

        /// <summary>
        /// The rank up progress bar.
        /// </summary>
        public ProgressBar RankUpProjectionProgressBar;

        /// <summary>
        /// The production progress bar.
        /// </summary>
        public ProgressBar ProductionProgressBar;

        /// <summary>
        /// The current purchase quantity.
        /// </summary>
        protected int _purchaseQuantity = 1;

        /// <summary>
        /// The purchase quantity set in the GeneratorDialog.
        /// </summary>
        protected int _purchaseQuantitySet;

        /// <summary>
        /// The upgrade cost.
        /// </summary>
        protected ResourceCost _upgradeCost;

        /// <summary>
        /// The unlock cost.
        /// </summary>
        protected ICost _unlockCost;

        /// <summary>
        /// Flag to know if the component is initialized
        /// </summary>
        protected bool _initialized;
        
        public string GeneratorName {
            get
            {
                return _generatorModel.Name;
            }
        } 

        protected virtual void OnEnable()
        {
            // When we earn or spend money, we need to check if we can buy or not this generator
            ResourceManager.OnResourceBalanceChanged += CheckIfCanBuy;

            // When the amount to buy changes, we need to update the cost of the generator upgrade
            GeneratorDialog.OnPurchaseQuantityChanged += PurchaseQuantityChanged;
            
            // When a gameplay modifier it is applied update the generators
            GameplayModifiersManager.OnGameplayModifiersApplied += Refresh;
            GameplayModifiersManager.OnGameplayModifiersRemoved += Refresh;

            // When an ad is stacked, we need to refresh our production
            AdBonusManager.OnAdStacked += Refresh;
            AdBonusManager.OnAdUnstacked += Refresh;

            // Event to force a refresh from outside of the SDK
            OnRefreshGeneratorGridItem += SeeIfShouldRefresh;
        }

        protected virtual void OnDisable()
        {
            ResourceManager.OnResourceBalanceChanged -= CheckIfCanBuy;
            GeneratorDialog.OnPurchaseQuantityChanged -= PurchaseQuantityChanged;
            GameplayModifiersManager.OnGameplayModifiersApplied -= Refresh;
            GameplayModifiersManager.OnGameplayModifiersRemoved -= Refresh;
            AdBonusManager.OnAdStacked -= Refresh;
            AdBonusManager.OnAdUnstacked -= Refresh;
            OnRefreshGeneratorGridItem -= SeeIfShouldRefresh;
        }

        public virtual void PopulateGeneratorGridItem(GeneratorItemModel generatorModel)
        {
            // Cache the config
            this._generatorModel = generatorModel;

            // Cache the generators
            _generatorManager = GeneratorManager.Instance;
            _resourceManager = ResourceManager.Instance;
            _userManager = IdleUserManager.Instance;

            // Cache the generatorUserModel
            _generatorUserModel = _userManager.GetGeneratorFromUser(_generatorModel.Name);
            
            //costs
            _unlockCost = _generatorManager.GetGeneratorUnlockCost(_generatorModel.Name);
            _upgradeCost = _generatorManager.GetGeneratorUpgradeCost(_generatorModel.Name, _purchaseQuantity);

            // Update the name
            if (NameLabel != null)
                NameLabel.text = generatorModel.Name;

            // Set that we want to purchase 1 generator at start
            PurchaseQuantityChanged(1);
            
            if (generatorModel.WaitForCollection && !generatorModel.WaitForGeneration)
                _generatorUserModel.GenerationState = GeneratorsStates.StatesGeneration.GENERATING;

            // Set the initialized flag to true
            _initialized = true;
            
            // Refresh all of the ui elements
            Refresh();
            
            OnGridItemPopulated?.Invoke(GeneratorName);
        }
               

        /// <summary>
        /// Purchases the generator.
        /// </summary>
        public virtual bool PurchaseGenerator()
        {           
            // Buy the generators from the GeneratorManager
            bool purchaseResult = _generatorManager.PurchaseGenerator(_generatorModel.Name, _purchaseQuantity);

            // Update the purchase quantity (if we had rank up or max, this is needed)
            PurchaseQuantityChanged(_purchaseQuantitySet);

            // Refresh all of the ui elements
            Refresh();

            return purchaseResult;
        }

        /// <summary>
        /// Method that can be called from the UI
        /// </summary>
        public void DoPurchaseGenerator()
        {
            PurchaseGenerator();
        }
        
        /// <summary>
        /// Get the name 
        /// </summary>
        /// <returns></returns>
        public string GetGeneratorName(){
            if (_generatorModel == null)
            {
                LogHelper.LogWarning("Unable to get generator name. Null generator model instance");
                return string.Empty;
            }
            
            return _generatorModel.Name;
        }

        void CheckIfCanBuy(string resourceName, double delta)
        {
            if (resourceName == _upgradeCost.ResourceKey)
            {
                UpdateBuyButton();
                RefreshGenerationProjection();
            }
        }
        protected virtual void UpdateBuyButton()
        {
            // Make the button interactable if we have enough points to purchase this generator
            if (BuyButton != null)
            {
                if ((_generatorUserModel == null || _generatorUserModel.Amount == 0) && _generatorModel.UnlockCost != null)
                {
                    BuyButton.interactable = _unlockCost.HasEnough() && !_generatorManager.IsGeneratorAtMax(_generatorModel.Name);
                }
                else
                {
                    BuyButton.interactable = _upgradeCost.HasEnough() && !_generatorManager.IsGeneratorAtMax(_generatorModel.Name);
                }
            }
        }

        protected virtual void PurchaseQuantityChanged(int newPurchaseQuantity)
        {
            // Update the last purchaseQuantitySet
            _purchaseQuantitySet = newPurchaseQuantity;

            // Update the current purchase quantity
            UpdatePurchaseQuantity(newPurchaseQuantity);

            // Refresh all of the ui elements
            Refresh();
        }

        protected virtual void RefreshBuyButton()
        {       
            // If we are maxed out
            if (_generatorManager.IsGeneratorAtMax(_generatorModel.Name))
            {
                // Disable the buy label
                if (BuyQuantityLabel != null)
                    BuyQuantityLabel.gameObject.SetActive(false);

                // Make sure they can't tap the button
                if (BuyButton != null)
                    BuyButton.interactable = false;

                // Disable the cost label
                if(CostAmountLabel != null)
                    CostAmountLabel.gameObject.SetActive(false);

                // Disable the cost key label
                if (CostKeyLabel != null)
                    CostKeyLabel.gameObject.SetActive(false);
                
                // Enable the max label
                if (MaxAmountLabel != null)
                {
                    MaxAmountLabel.text = Localization.Localization.Get("GeneratorsDialog_MaxRank");
                    MaxAmountLabel.gameObject.SetActive(true);
                }

                if (CostIcon != null)
                {
                    CostIcon.gameObject.SetActive(false);
                }
            }
            else
            {
                // Disable the max label
                if(MaxAmountLabel != null)
                    MaxAmountLabel.gameObject.SetActive(false);
                
                _generatorUserModel = _userManager.GetGeneratorFromUser(_generatorModel.Name);
                if ((_generatorUserModel == null || _generatorUserModel.Amount == 0) && _generatorModel.UnlockCost != null)
                {
                    // Update how many we can buy
                    if (BuyQuantityLabel != null)
                        BuyQuantityLabel.text = string.Format(Localization.Localization.Get("GeneratorsDialog_Buy"), 1);

                    // Update the cost formatting the number
                    if (CostAmountLabel != null)
                        CostAmountLabel.text = FormatHelper.Instance.FormatNumber((double)_unlockCost.GetCost(), CostFormatterName);

                    string key = "";
                    if (_unlockCost is CurrencyCost ccost)
                        key = ccost.CurrencyKey;
                    else if (_unlockCost is ResourceCost rcost)
                        key = rcost.ResourceKey;

                    //set resource cost key
                    if (CostKeyLabel != null)
                        CostKeyLabel.text = Localization.Localization.Get(key);
                }
                else
                {
                    _upgradeCost = _generatorManager.GetGeneratorUpgradeCost(_generatorModel.Name, _purchaseQuantity);
                    // Update how many we can buy
                    if (BuyQuantityLabel != null)
                    {
                        if(_purchaseQuantity == _generatorManager.GetAmountOfGeneratorsUntilNextRank(_generatorModel.Name))
                            BuyQuantityLabel.text = Localization.Localization.Get("GeneratorsDialog_RankUp");
                        else
                            BuyQuantityLabel.text = string.Format(Localization.Localization.Get("GeneratorsDialog_Buy"), _purchaseQuantity);
                    }

                    // Update the cost formating the number
                    if (CostAmountLabel != null)
                        CostAmountLabel.text = FormatHelper.Instance.FormatNumber(_upgradeCost.Amount, CostFormatterName);

                    //set resource cost key
                    if (CostKeyLabel != null)
                        CostKeyLabel.text = Localization.Localization.Get(_upgradeCost.ResourceKey);
                }

                UpdateBuyButton();
            }
        }

        /// <summary>
        /// See if we should do a refresh based on the generator name (If its our name, or empty, we refresh)
        /// </summary>
        /// <param name="generatorName">Generator name.</param>
        void SeeIfShouldRefresh(string generatorName)
        {
            if(generatorName == _generatorModel.Name || string.IsNullOrEmpty(generatorName))
            {
                Refresh();
            }
        }

        protected virtual void Refresh()
        {
            if(_initialized)
            {
                RefreshBuyButton();

                // Update how many generators we own
                int amountOwned = _generatorManager.GetGeneratorAmount(_generatorModel.Name);

                if (RankUpProgressBar != null)
                    RankUpProgressBar.SetLabelValue(string.Format("{0:n0}", amountOwned));

                // Update the values in the production progress bar
                if (RankUpProgressBar != null)
                {
                    int amountInThisRank = _generatorManager.GetAmountOfGeneratorsInThisRank(_generatorModel.Name);
                    RankUpProgressBar.UpdateValues(amountInThisRank,
                        _generatorManager.GetAmountOfGeneratorsUntilNextRank(_generatorModel.Name) + amountInThisRank);
                }

                // Get the production & production interval
                double productionInterval = _generatorManager.GetProductionInterval(_generatorModel);
                double productionPerInterval =
                    _generatorManager.GetGeneratorProductionPerInterval(_generatorModel, amountOwned);

                // Update how long it takes to generate points
                if (ProductionIntervalLabel != null)
                {
                    ProductionIntervalLabel.text =
                        FormatHelper.Instance.FormatTimer(productionInterval, ProductionTimerFormatterName);
                }

                // Format the generated amount of money
                string generatedAmount = FormatGeneratedAmount(productionInterval, productionPerInterval);

                // Update how much we will generate in this period
                if (ProductionProgressBar != null)
                    ProductionProgressBar.SetLabelValue(generatedAmount);
            }
        }

        /// <summary>
        /// Formats the Generated amount
        /// </summary>
        /// <param name="productionInterval">How long it takes to produce</param>
        /// <param name="productionPerInterval">How much does it produce</param>
        /// <returns></returns>
        protected virtual string FormatGeneratedAmount(double productionInterval, double productionPerInterval)
        {
            return string.Format(Localization.Localization.Get("GeneratorsDialog_ProductionRate"), FormatHelper.Instance.FormatNumber(productionPerInterval), FormatHelper.Instance.FormatNumber(productionPerInterval / productionInterval));
        }
        
        void Update()
        {
            // Update the values in the production progress bar
            UpdateProductionProgressBar();
            
            // If we are with the max rank selected, we have to do this here
            if (_purchaseQuantitySet == GeneratorDialog.MAX_RANK || _purchaseQuantitySet == GeneratorDialog.MAX_POSSIBLE)
            {
                // Update how many we can buy
                UpdatePurchaseQuantity(_purchaseQuantitySet);
                
                // Refresh buy button
                RefreshBuyButton();
            }
        }
        
        protected virtual void UpdateProductionProgressBar()
        {
            if (ProductionProgressBar != null)
            {
                ProductionProgressBar.UpdateValues(_generatorManager.GetGeneratorProductionTime(_generatorModel.Name), _generatorManager.GetProductionInterval(_generatorModel));
            }
        }

        void RefreshGenerationProjection()
        {
            // Update the values in the production progress bar
            if (RankUpProjectionProgressBar != null)
            {
                if(_resourceManager.HasEnough(_upgradeCost.ResourceKey, _upgradeCost.Amount))
                {
                    int amountInThisRank = _generatorManager.GetAmountOfGeneratorsInThisRank(_generatorModel.Name);
                    RankUpProjectionProgressBar.UpdateValues(amountInThisRank + _purchaseQuantity, _generatorManager.GetAmountOfGeneratorsUntilNextRank(_generatorModel.Name) + amountInThisRank);
                }
                else
                {
                    RankUpProjectionProgressBar.UpdateValues(0, 1);
                }
            }
        }

        /// <summary>
        /// Updates the purchase quantity.
        /// </summary>
        /// <param name="amountToPurchase">Amount to purchase.</param>
        protected void UpdatePurchaseQuantity(int amountToPurchase)
        {
            if (amountToPurchase == GeneratorDialog.MAX_RANK)
            {
                _purchaseQuantity = _generatorManager.GetMaxGeneratorsPurchasable(_generatorModel.Name, _generatorManager.GetAmountOfGeneratorsUntilNextRank(_generatorModel.Name));
            }
            else if (amountToPurchase == GeneratorDialog.MAX_POSSIBLE)
            {
                _purchaseQuantity = _generatorManager.GetMaxGeneratorsPurchasable(_generatorModel.Name);
            }
            else if (amountToPurchase == GeneratorDialog.NEXT_RANK)
            {
                if (_generatorManager.GetGeneratorAmount(_generatorModel.Name) == 0)
                    _purchaseQuantity = 1;
                else
                {
                    _purchaseQuantity = _generatorManager.GetAmountOfGeneratorsUntilNextRank(_generatorModel.Name);
                }
            }
            else
            {
                // Set the purchase quantity
                _purchaseQuantity = amountToPurchase;
            }

            // Make sure we dont go over the max
            var amountOwned = _generatorManager.GetGeneratorAmount(_generatorModel.Name);
            var maxAmount = _generatorManager.GetMaxAmountOfGenerators();
            if (_purchaseQuantity + amountOwned > maxAmount)
            {
                _purchaseQuantity = maxAmount - amountOwned;
            }

            RefreshGenerationProjection();
        }
        
        /// <summary>
        /// Sets Generator State to start Generating Resources
        /// </summary>
        public virtual void GenerateResources()
        {
            _generatorManager.Generate(_generatorModel.Name);
        }

        /// <summary>
        /// Changes Generator State for resources to be awarded
        /// </summary>
        public virtual void CollectResources()
        {
            _generatorManager.Collect(_generatorModel.Name);
        }

        /// <summary>
        /// Sets the Generator to generate automatically
        /// </summary>
        public virtual void AutomateGeneration()
        {
            // Automates The Generator
            _generatorManager.AutomateGeneration(_generatorModel.Name);
        }
        
        /// <summary>
        /// Sets the Generator to collect automatically
        /// </summary>
        public virtual void AutomateCollection()
        {
            // Automates The Generator
            _generatorManager.AutomateCollection(_generatorModel.Name);
        }

        /// <summary>
        /// Sets the generator to generate manually
        /// </summary>
        public virtual void DeautomateGeneration()
        {
            _generatorManager.DeautomateGeneration(_generatorModel.Name);
        }
        
        /// <summary>
        /// Sets the generator to collect manually
        /// </summary>
        public virtual void DeautomateCollection()
        {
            _generatorManager.DeautomateCollection(_generatorModel.Name);
        }
    }
}
