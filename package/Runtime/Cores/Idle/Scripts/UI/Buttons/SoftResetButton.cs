﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    public class SoftResetButton : MonoBehaviour
    {
        public void SoftReset()
        {
            SoftResetManager.Instance.SoftReset();
        }

    }
}