﻿using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk.Core.Idle
{
    public class WatchAdButton : MonoBehaviour
    {
        /// <summary>
        /// The placement for this ad
        /// </summary>
        public string Placement;
        
        /// <summary>
        /// The button
        /// </summary>
        public Button WatchButton;

        /// <summary>
        /// The timer remaining
        /// </summary>
        public TimerLabel Timer;

        /// <summary>
        /// Reference to the AdBonusManager
        /// </summary>
        protected AdBonusManager _adBonusManager;

        /// <summary>
        /// Reference to the AdsManager
        /// </summary>
        protected AdsManager _adsManager;

        /// <summary>
        /// Are we subscribed for the initialize?
        /// </summary>
        private bool _suscribed = false;

        private bool _adCompleted;
        
        protected virtual void OnEnable()
        {
            if (_adBonusManager == null && AdBonusManager.Instance != null && AdBonusManager.Instance.IsLoaded)
            {
                Initialize();
            }
            else
            {
                DependencyManager.OnEverythingLoaded += Initialize;
                _suscribed = true;
            }
        }

        protected virtual void OnDisable()
        {
            if (_suscribed)
                DependencyManager.OnEverythingLoaded -= Initialize;
        }

        protected virtual void OnDestroy()
        {
            AdBonusManager.OnAdUnstacked -= RefreshButton;
        }

        protected virtual void Initialize()
        {
            // Cache manager instance
            _adBonusManager = AdBonusManager.Instance;
            _adsManager = AdsManager.Instance;

            // Refresh the button
            RefreshButton();

            // Subscribe to AdUnstacked event, in case we need to enable the button
            AdBonusManager.OnAdUnstacked += RefreshButton;
        }

        protected virtual void RefreshButton()
        {
            if(_adBonusManager == null)
                return;

            // Check if button is interactable based on if we can watch more ads
            if(WatchButton != null)
                WatchButton.interactable = _adBonusManager.CanWatchMoreAds();

            // Start the timer with the updated time remaining
            if (Timer != null)
                Timer.StartTimer(_adBonusManager.GetBonusTimeRemaining());
        }

        /// <summary>
        /// Method to call after an ad was watched
        /// </summary>
        public virtual bool WatchAd()
        {
            if(_adsManager.IsAdAvailable(AdTypes.REWARDED_VIDEO, Placement))
            {
                _adCompleted = false;
                AdsManager.OnRewardedVideoClosed += AdClosed;
                AdsManager.OnRewardedVideoCompleted += AdCompleted;
                _adsManager.ShowRewardedVideo(Placement);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Action method so it can be called from a Button
        /// </summary>
        public virtual void WatchAdAction()
        {
            WatchAd();
        }

        protected virtual void AdCompleted()
        {
            GiveBonus();
        }

        protected virtual void AdClosed()
        {
            AdsManager.OnRewardedVideoClosed -= AdClosed;
            AdsManager.OnRewardedVideoCompleted -= AdCompleted;
        }

        protected virtual void GiveBonus()
        {
            // Let the AdBonusManager know
            _adBonusManager.AdWatched();
            // Refresh the button
            RefreshButton();
        }
    }
}