﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    public class TimewarpButton : MonoBehaviour
    {
        public int Duration;

        public void ApplyTimewarp()
        {
            ResourceGenerationManager.Instance.ApplyTimewarp(Duration);
        }

    }
}