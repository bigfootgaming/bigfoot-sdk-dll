﻿using System;
using TMPro;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    public class PurchaseAmountLabel : MonoBehaviour
    {
        TextMeshProUGUI _purchaseAmountLabel;

        void OnEnable()
        {
            GeneratorDialog.OnPurchaseQuantityChanged += UpdateLabel;
        }

        void OnDisable()
        {
            GeneratorDialog.OnPurchaseQuantityChanged -= UpdateLabel;
        }

        void Awake()
        {
            _purchaseAmountLabel = GetComponent<TextMeshProUGUI>();
            UpdateLabel(1);
        }

        void UpdateLabel(int amount)
        {
            switch(amount)
            {
                case -1:
                    _purchaseAmountLabel.text = Localization.Localization.Get("GeneratorsDialogButton_HireNextRank");
                    break;
                case -2: case -3:
                    _purchaseAmountLabel.text = Localization.Localization.Get("GeneratorsDialogButton_HireMax");
                    break;
                default:
                    _purchaseAmountLabel.text = string.Format(Localization.Localization.Get("GeneratorsDialogButton_Hire"), amount);
                    break;
            }

        }
    }
}
