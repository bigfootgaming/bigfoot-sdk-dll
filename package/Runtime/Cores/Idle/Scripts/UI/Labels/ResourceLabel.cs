﻿using System;
using BigfootSdk.Helpers;
using TMPro;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    public class ResourceLabel : MonoBehaviour
    {
        /// <summary>
        /// The key of the resource
        /// </summary>
        public string ResourceKey;
        
        /// <summary>
        /// The formatter to use
        /// </summary>
        public string NumberFormatterName = FormatHelper.DEFAULT_FORMATTER;

        /// <summary>
        /// Show icon on the label
        /// </summary>
        public bool ShowCurrencyIcon;
        
        /// <summary>
        /// The Label
        /// </summary>
        TextMeshProUGUI _resourceLabel;

        /// <summary>
        /// The ResourceManager
        /// </summary>
        ResourceManager _resourceManager;

        void OnEnable()
        { 
            // Populate the generators once everything is done loading
            DependencyManager.OnSomethingFinished += CheckLoader;
            if (ResourceManager.Instance != null && ResourceManager.Instance.IsLoaded)
                InitializeLabel();
        }

        void OnDisable()
        {
            DependencyManager.OnSomethingFinished -= CheckLoader;
        }

        void OnDestroy()
        {
            ResourceManager.OnResourceBalanceChanged -= UpdateLabel;
        }

        /// <summary>
        /// If the loader needed finished loading, initialize the label
        /// </summary>
        /// <param name="loader"></param>
        void CheckLoader(string loader)
        {
            if(loader == ResourceManager.Instance.Id)
                InitializeLabel();
        }

        /// <summary>
        /// Initializes the label
        /// </summary>
        void InitializeLabel()
        {
            // Cache the manager
            _resourceManager = ResourceManager.Instance;

            // Grab a reference the the label component
            _resourceLabel = GetComponent<TextMeshProUGUI>();

            UpdateLabel(ResourceKey, 0);

            ResourceManager.OnResourceBalanceChanged += UpdateLabel;
        }

        /// <summary>
        /// Updates the label
        /// </summary>
        /// <param name="resourceName"></param>
        /// <param name="amount"></param>
        void UpdateLabel(string resourceName, double amount)
        {
            if(resourceName == ResourceKey)
            {
                if (ShowCurrencyIcon)
                    _resourceLabel.text = string.Format("<sprite name=\"{0}\">{1}", resourceName, FormatHelper.Instance.FormatNumber(_resourceManager.GetResourceAmount(ResourceKey), NumberFormatterName));    
                else
                    _resourceLabel.text = FormatHelper.Instance.FormatNumber(_resourceManager.GetResourceAmount(ResourceKey), NumberFormatterName);
            }
        }
    }
}
