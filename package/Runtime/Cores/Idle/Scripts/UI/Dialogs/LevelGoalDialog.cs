﻿using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    public class LevelGoalDialog : MonoBehaviour
    {
        /// <summary>
        /// The on level goal reached event.
        /// </summary>
        public static Action OnLevelGoalReached;

        /// <summary>
        /// The on level goal claimed event.
        /// </summary>
        public static Action OnLevelGoalClaimed;

        /// <summary>
        /// Progress Bar that shows how close to reaching the level goal
        /// </summary>
        public ProgressBar LevelProgress;

        /// <summary>
        /// Gameobject to display when we can claim the level
        /// </summary>
        public GameObject ClaimLevelButton;

        /// <summary>
        /// The formatter to use
        /// </summary>
        public string NumberFormatterName = FormatHelper.DEFAULT_FORMATTER;
        
        /// <summary>
        /// Cached version to the progress of the player
        /// </summary>
        protected SavedStatModel _progressSavedStat;

        /// <summary>
        /// The goal we have for this level
        /// </summary>
        protected double _levelGoal;

        /// <summary>
        /// Flag to know if the player can claim the level
        /// </summary>
        protected bool _canClaimLevel = false;

        /// <summary>
        /// The last score we updated with, so we only update if it changes
        /// </summary>
        private double _lastScore;
        
        /// <summary>
        /// The last goal we updated with, so we only update if it changes
        /// </summary>
        private double _lastGoal;
        
        protected virtual void OnEnable()
        {
            // Populate the generators once everything is done loading
            DependencyManager.OnEverythingLoaded += Initialize;
        }

        protected virtual void OnDisable()
        {
            DependencyManager.OnEverythingLoaded -= Initialize;
        }

        protected virtual void Initialize()
        {
            // Get a reference to the progress saved stat
            _progressSavedStat = IdleUserManager.Instance.GetSavedStat(IdleUserManager.PROGRESS_SAVED_STAT);

            // Update the level goal
            UpdateLevelGoal();
        }

        void Update()
        {
            if(!_canClaimLevel && _progressSavedStat != null)
            {
                if (LevelProgress != null && (_lastScore != _progressSavedStat.Game || _lastGoal != _levelGoal))
                {
                    _lastScore = _progressSavedStat.Game;
                    _lastGoal = _levelGoal;
                    // Update the progress bar
                    LevelProgress.UpdateValues(_progressSavedStat.Game, _levelGoal);

                    SetLabelValue();
                }
                
                // Check if we can claim the level
                _canClaimLevel = _progressSavedStat.Game > _levelGoal;

                // If we can, enable the button
                if(_canClaimLevel)
                {
                    OnLevelGoalReached?.Invoke();

                    if(ClaimLevelButton != null)
                        ClaimLevelButton.SetActive(true);
                }
            }
        }

        /// <summary>
        /// Set the label value
        /// </summary>
        protected virtual void SetLabelValue()
        {
            // Format the label to show on the bar. Cap the produced amount to the level max
            LevelProgress.SetLabelValue(string.Format("{0}/{1}", FormatHelper.Instance.FormatNumber(Math.Min(_levelGoal, _progressSavedStat.Game), NumberFormatterName), FormatHelper.Instance.FormatNumber(_levelGoal, NumberFormatterName)));
        }

        /// <summary>
        /// Claims the level
        /// </summary>
        /// <param name="grantedReward">TransactionResultModel with Granted Reward</param>
        public void ClaimLevel(Action<TransactionResultModel> grantedReward = null, int forLevel = -1)
        {
            if(_canClaimLevel)
            {
                // Claim the current level
                LevelManager.Instance.ClaimLevel(model => { grantedReward?.Invoke(model);}, forLevel);

                // Update the goal for this level
                UpdateLevelGoal();

                // Disable the button and reset the flag
                if(ClaimLevelButton != null)
                    ClaimLevelButton.SetActive(false);
                _canClaimLevel = false;

                OnLevelGoalClaimed?.Invoke();
            }
        }

        /// <summary>
        /// Update the goal for this level
        /// </summary>
        protected virtual void UpdateLevelGoal()
        {
            // Update the goal for this level
            _levelGoal = LevelManager.Instance.GetLevelGoal();
        }
    }
}
