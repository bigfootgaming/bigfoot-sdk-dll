﻿using System;
using System.Collections.Generic;
using BigfootSdk.Layout;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    /// <summary>
    /// Upgrades dialog.
    /// </summary>
    public class UpgradesDialog : MonoBehaviour
    {
        /// <summary>
        /// Parent for all of the upgrades
        /// </summary>
        public LayoutGroup LayoutGroup;

        /// <summary>
        /// Prefab to instanciate for each upgrade
        /// </summary>
        public GameObject UpgradeGridItemPrefab;

        void OnEnable()
        {
            // Populate the upgrades once everything is done loading
            DependencyManager.OnEverythingLoaded += PopulateUpgardes;
        }

        void OnDisable()
        {
            DependencyManager.OnEverythingLoaded -= PopulateUpgardes;
        }

        /// <summary>
        /// Populates the upgardes.
        /// </summary>
        void PopulateUpgardes()
        {
            if (LayoutGroup != null && UpgradeGridItemPrefab != null)
            {
                // Get all of the upgrades from the UpgradeManager
                List<UpgradeModel> upgradeModels = UpgradesManager.Instance.GetAllUpgrades();

                foreach(UpgradeModel model in upgradeModels)
                {
                    // Instantiate the GridItem
                    GameObject gridItem = Instantiate(UpgradeGridItemPrefab, Vector3.zero, Quaternion.identity, LayoutGroup.transform);

                    // Populate it with the appropiate config
                    UpgradeGridItem upgradeGridItem = gridItem.GetComponent<UpgradeGridItem>();
                    if(upgradeGridItem != null)
                    {
                        upgradeGridItem.PopulateUpgradeGridItem(model);
                    }
                }

                // After all of the generators are added, rebuild the layout so they position properly
                LayoutGroup.Rebuild();
            }
        }
    }
}
