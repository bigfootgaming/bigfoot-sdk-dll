﻿using System;
using System.Collections.Generic;
using BigfootSdk.Layout;
using UnityEngine;

namespace BigfootSdk.Core.Idle
{
    public class GeneratorDialog : MonoBehaviour
    {
        /// <summary>
        /// Tag of the generators you want to use
        /// </summary>
        public string GeneratorsTag = "";
        
        /// <summary>
        /// Event for when we change how many generators we want to purchase at the time 
        /// </summary>
        public static Action<int> OnPurchaseQuantityChanged;

        /// <summary>
        /// Parent for all of the generators
        /// </summary>
        public LayoutGroup LayoutGroup;

        /// <summary>
        /// Prefab to instanciate for each generator
        /// </summary>
        public GameObject GeneratorGridItemPrefab;

        /// <summary>
        /// Next Rank Constant
        /// </summary>
        public const int NEXT_RANK = -1;

        /// <summary>
        /// Buys up to the next rank, or the max it can
        /// </summary>
        public const int MAX_RANK = -2;

        /// <summary>
        /// Buy the max amount possible
        /// </summary>
        public const int MAX_POSSIBLE = -3;

        /// <summary>
        /// Array containing the purchase amounts
        /// </summary>
        protected int[] _purchaseAmounts = new int[8] { 1, 10, 20, 50, 100, NEXT_RANK, MAX_RANK, MAX_POSSIBLE };

        /// <summary>
        /// The index of the current purchase amount.
        /// </summary>
        int _currentPurchaseAmountIndex = 0;

        protected virtual void OnEnable()
        {
            // Populate the generators once everything is done loading
            DependencyManager.OnEverythingLoaded += PopulateGenerators;
        }

        void OnDisable()
        {
            DependencyManager.OnEverythingLoaded -= PopulateGenerators;
        }

        protected virtual void PopulateGenerators()
        {
            if (LayoutGroup != null && GeneratorGridItemPrefab != null)
            {
                // Get all of the generators from the GeneratorManager
                List<GeneratorItemModel> generatorModels;
                if (string.IsNullOrEmpty(GeneratorsTag))
                {
                    generatorModels = GeneratorManager.Instance.GetGenerators();
                }
                else
                {
                    generatorModels = GeneratorManager.Instance.GetGeneratorsWithTag(GeneratorsTag);
                }

                foreach(GeneratorItemModel model in generatorModels)
                {
                    // Instantiate the GridItem
                    GameObject gridItem = Instantiate(GeneratorGridItemPrefab, Vector3.zero, Quaternion.identity, LayoutGroup.transform);

                    // Populate it with the appropiate config
                    GeneratorGridItem generatorGridItem = gridItem.GetComponent<GeneratorGridItem>();
                    if(generatorGridItem != null)
                    {
                        generatorGridItem.PopulateGeneratorGridItem(model);
                    }
                }

                // After all of the generators are added, rebuild the layout so they position properly
                LayoutGroup.Rebuild();
            }
        }

        /// <summary>
        /// Changes the purchase amount via a button.
        /// </summary>
        public void ChangePurchaseAmount()
        {
            // Move the index one up in a circular array
            _currentPurchaseAmountIndex = (_currentPurchaseAmountIndex + 1) % _purchaseAmounts.Length;

            // Propagate the event that the index now changed
            OnPurchaseQuantityChanged?.Invoke(_purchaseAmounts[_currentPurchaseAmountIndex]);
        }
    }
}
