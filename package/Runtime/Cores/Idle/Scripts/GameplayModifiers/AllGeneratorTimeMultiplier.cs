using System.Collections.Generic;
using System.Linq;
using BigfootSdk;
using BigfootSdk.Core.Idle;
using BigfootSdk.Helpers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Time multipliers that applies to all generators
    /// </summary>
	public class AllGeneratorTimeMultiplier : GameplayModifierModel
    {
        /// <summary>
        /// Multiplier value for the generator bonus
        /// </summary>
        [ReadOnly]
        public double Multiplier;

        /// <summary>
        /// Applies this specific bonus
        /// </summary>
		public override void Apply()
        {
            // Grab the model for all of the generator modifiers
            GeneratorModifiersModel generatorModifiers = GeneratorModifiersModel.Instance;

            if (generatorModifiers == null)
            {
                return;
            }

            // If it is part of a group of modifiers, add the values
            // Else multiply the values
            if (!string.IsNullOrEmpty(Group))
            {
                // If the group already exist, just add the value.
                // Else set the multiplier as the starting value.
                if (generatorModifiers.AllGeneratorTimeMultiplierByGroup.ContainsKey(Group))
                {
                    generatorModifiers.AllGeneratorTimeMultiplierByGroup[Group] += Multiplier - 1;
                }
                else
                {
                    generatorModifiers.AllGeneratorTimeMultiplierByGroup.Add(Group, Multiplier);
                }
            }
            else
            {
                generatorModifiers.AllGeneratorTimeMultiplierByGroup[GameplayModifierConstants.NON_GROUPED] *= Multiplier;
            }

            // When the multiplier its grouped, multiply all values of each group and cache the result
            // Cache the current result so can be modified and later set it again. (use this to avoid the error of modifying the dict values while you loop it)
            double result = 1;
            foreach (string group in generatorModifiers.AllGeneratorTimeMultiplierByGroup.Keys)
            {
                if (string.Equals(group, GameplayModifierConstants.RESULT))
                {
                    continue;
                }

                result *= generatorModifiers.AllGeneratorTimeMultiplierByGroup[group];
            }

            generatorModifiers.AllGeneratorTimeMultiplierByGroup[GameplayModifierConstants.RESULT] = result;
        }

        public override void Undo()
        {
            // Grab the model for all of the generator modifiers
            GeneratorModifiersModel generatorModifiers = GeneratorModifiersModel.Instance;

            if (generatorModifiers == null)
            {
                return;
            }

            // If it is part of a group of modifiers, add the values
            // Else multiply the values
            if (!string.IsNullOrEmpty(Group))
            {
                // Remove the value for this group
                if (generatorModifiers.AllGeneratorTimeMultiplierByGroup.ContainsKey(Group))
                {
                    generatorModifiers.AllGeneratorTimeMultiplierByGroup[Group] -= Multiplier - 1;
                    if(System.Math.Abs(generatorModifiers.AllGeneratorTimeMultiplierByGroup[Group]) < Mathf.Epsilon)
                    {
                        generatorModifiers.AllGeneratorTimeMultiplierByGroup.Remove(Group);
                    }
                }
                else
                {
                    LogHelper.LogWarning("Trying to remove a GameplayModifier, but it doesn't seem to be added.");
                }
            }
            else
            {
                generatorModifiers.AllGeneratorTimeMultiplierByGroup[GameplayModifierConstants.NON_GROUPED] /= Multiplier;
            }

            // When the multiplier its grouped, multiply all values of each group and cache the result
            // Cache the current result so can be modified and later set it again. (use this to avoid the error of modifying the dict values while you loop it)
            double result = 1;
            foreach (string group in generatorModifiers.AllGeneratorTimeMultiplierByGroup.Keys)
            {
                if (string.Equals(group, GameplayModifierConstants.RESULT))
                {
                    continue;
                }

                result *= generatorModifiers.AllGeneratorTimeMultiplierByGroup[group];
            }

			generatorModifiers.AllGeneratorTimeMultiplierByGroup[GameplayModifierConstants.RESULT] = result;
		}

		public override object GetValue()
		{
			return Multiplier;
		}
	}
}
