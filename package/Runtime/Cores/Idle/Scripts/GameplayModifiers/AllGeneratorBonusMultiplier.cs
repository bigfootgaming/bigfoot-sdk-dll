using System.Collections.Generic;
using System.Linq;
using BigfootSdk;
using BigfootSdk.Core.Idle;
using BigfootSdk.Helpers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Bonus that applies to all generators
    /// </summary>
	public class AllGeneratorBonusMultiplier : GameplayModifierModel
    {
        /// <summary>
        /// Multiplier value for the generator bonus
        /// </summary>
        [ReadOnly]
        public double Multiplier;

        /// <summary>
        /// Applies this specific bonus
        /// </summary>
		public override void Apply()
        {
            // Grab the model for all of the generator modifiers
            GeneratorModifiersModel generatorModifiers = GeneratorModifiersModel.Instance;

            if (generatorModifiers == null)
            {
                return;
            }

            // If it is part of a group of modifiers, add the values
            // Else multiply the values
            if (!string.IsNullOrEmpty(Group))
            {
                // If the group already exist, just add the value.
                // Else set the multiplier as the starting value.
                if (generatorModifiers.AllGeneratorBonusMultiplierByGroup.ContainsKey(Group))
                {
                    generatorModifiers.AllGeneratorBonusMultiplierByGroup[Group] += Multiplier - 1;
                }
                else
                {
                    generatorModifiers.AllGeneratorBonusMultiplierByGroup.Add(Group, Multiplier);
                }
            }
            else
            {
                generatorModifiers.AllGeneratorBonusMultiplierByGroup[GameplayModifierConstants.NON_GROUPED] *= Multiplier;
            }

            // When the multiplier its grouped, multiply all values of each group and cache the result
            // Cache the current result so can be modified and later set it again. (use this to avoid the error of modifying the dict values while you loop it)
            double result = 1;
            foreach (string group in generatorModifiers.AllGeneratorBonusMultiplierByGroup.Keys)
            {
                if (string.Equals(group, GameplayModifierConstants.RESULT))
                {
                    continue;
                }

                result *= generatorModifiers.AllGeneratorBonusMultiplierByGroup[group];
            }

            generatorModifiers.AllGeneratorBonusMultiplierByGroup[GameplayModifierConstants.RESULT] = result;
        }

        public override void Undo()
        {
            // Grab the model for all of the generator modifiers
            GeneratorModifiersModel generatorModifiers = GeneratorModifiersModel.Instance;

            if (generatorModifiers == null)
            {
                return;
            }

            // If it is part of a group of modifiers, add the values
            // Else multiply the values
            if (!string.IsNullOrEmpty(Group))
            {
                // Remove the value for this group
                if (generatorModifiers.AllGeneratorBonusMultiplierByGroup.ContainsKey(Group))
                {
                    generatorModifiers.AllGeneratorBonusMultiplierByGroup[Group] -= Multiplier - 1;
                    if(System.Math.Abs(generatorModifiers.AllGeneratorBonusMultiplierByGroup[Group]) < Mathf.Epsilon)
                    {
                        generatorModifiers.AllGeneratorBonusMultiplierByGroup.Remove(Group);
                    }
                }
                else
                {
                    LogHelper.LogWarning("Trying to remove a GameplayModifier, but it doesn't seem to be added.");
                }
            }
            else
            {
                generatorModifiers.AllGeneratorBonusMultiplierByGroup[GameplayModifierConstants.NON_GROUPED] /= Multiplier;
            }

            // When the multiplier its grouped, multiply all values of each group and cache the result
            // Cache the current result so can be modified and later set it again. (use this to avoid the error of modifying the dict values while you loop it)
            double result = 1;
            foreach (string group in generatorModifiers.AllGeneratorBonusMultiplierByGroup.Keys)
            {
                if (string.Equals(group, GameplayModifierConstants.RESULT))
                {
                    continue;
                }

                result *= generatorModifiers.AllGeneratorBonusMultiplierByGroup[group];
            }

			generatorModifiers.AllGeneratorBonusMultiplierByGroup[GameplayModifierConstants.RESULT] = result;
		}

		public override object GetValue()
		{
			return Multiplier;
		}
	}
}
