using System.Collections.Generic;
using System.Linq;
using BigfootSdk;
using BigfootSdk.Core.Idle;
using BigfootSdk.Helpers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Specific generator time multiplier.
    /// </summary>
    public class SpecificGeneratorTimeMultiplier : GameplayModifierModel
    {
        /// <summary>
        /// The names of the generator to apply the time multiplier
        /// If empty, it means it applies to all
        /// </summary>
        [TableList(IsReadOnly = true, ShowIndexLabels = true), ShowInInspector]
        public string[] GeneratorNames;

        /// <summary>
        /// Multiplier value for the production time
        /// </summary>
        [ReadOnly]
        public double Multiplier;

        /// <summary>
        /// Applies this specific bonus
        /// </summary>
		public override void Apply()
        {
            // Check if the generator name list it is not null
            if (GeneratorNames == null)
            {
                LogHelper.LogWarning("Unable to apply GameplayModifier 'SpecificGeneratorTimeMultiplier'. Null list of generators names to apply modifier");
                return;
            }

            // Run this only if you have any generator to apply
            if (!GeneratorNames.Any())
            {
                LogHelper.LogWarning("Unable to apply GameplayModifier 'SpecificGeneratorTimeMultiplier'. Empty list of generators names to apply modifier");
                return;
            }

            // Grab the model for all of the generator modifiers
            GeneratorModifiersModel generatorModifiers = GeneratorModifiersModel.Instance;

            if (generatorModifiers == null)
            {
                return;
            }

            // Run over all the generators to apply
            int counter = GeneratorNames.Length;
            for (int i = 0; i < counter; i++)
            {
                string generator = GeneratorNames[i];

                if (string.IsNullOrEmpty(generator))
                {
                    continue;
                }

                // Get the multiplier group for the specific generator. If not exist, create a new group
                Dictionary<string, double> specificMultipliers = null;
                if (!generatorModifiers.SpecificGeneratorsTimeMultipliersByGroup.TryGetValue(generator, out specificMultipliers))
                {
                    specificMultipliers = new Dictionary<string, double>
                    {
                        { GameplayModifierConstants.NON_GROUPED, 1 },
                        { GameplayModifierConstants.RESULT, 1 }
                    };
                    generatorModifiers.SpecificGeneratorsTimeMultipliersByGroup.Add(generator, specificMultipliers);
                }

                // If this generator its part of a group. Add the values
                // Else multiply the values
                if (!string.IsNullOrEmpty(Group))
                {
                    if (specificMultipliers.ContainsKey(Group))
                    {
                        specificMultipliers[Group] += Multiplier - 1;
                    }
                    else
                    {
                        specificMultipliers.Add(Group, Multiplier);
                    }
                }
                else
                {
                    specificMultipliers[GameplayModifierConstants.NON_GROUPED] *= Multiplier;
                }

                // When the multiplier its grouped, multiply all values of each group and cache the result
                // Cache the current result so can be modified and later set it again. (use this to ovid the error of modify the dict values while you loop it)
                double result = 1;
                foreach (string groupName in specificMultipliers.Keys)
                {
                    if (string.Equals(groupName, GameplayModifierConstants.RESULT))
                    {
                        continue;
                    }

                    result *= specificMultipliers[groupName];
                }

                specificMultipliers[GameplayModifierConstants.RESULT] = result;
            }
        }

        public override void Undo()
        {
            // Check if the generator name list it is not null
            if (GeneratorNames == null)
            {
                LogHelper.LogWarning("Unable to remove GameplayModifier 'SpecificGeneratorTimeMultiplier'. Null list of generators names to apply modifier");
                return;
            }

            // Run this only if you have any generator to apply
            if (!GeneratorNames.Any())
            {
                LogHelper.LogWarning("Unable to remove GameplayModifier 'SpecificGeneratorTimeMultiplier'. Empty list of generators names to apply modifier");
                return;
            }

            // Grab the model for all of the generator modifiers
            GeneratorModifiersModel generatorModifiers = GeneratorModifiersModel.Instance;

            if (generatorModifiers == null)
            {
                return;
            }

            // Run over all the generators to apply
            int counter = GeneratorNames.Length;
            for (int i = 0; i < counter; i++)
            {
                string generator = GeneratorNames[i];

                if (string.IsNullOrEmpty(generator))
                {
                    continue;
                }

                // Get the multiplier group for the specific generator. If not exist, create a new group
                generatorModifiers.SpecificGeneratorsTimeMultipliersByGroup.TryGetValue(generator, out Dictionary<string, double> specificMultipliers);

                // If this generator its part of a group, substract the values
                // Else divide the values
                if (!string.IsNullOrEmpty(Group))
                {
                    if (specificMultipliers.ContainsKey(Group))
                    {
                        specificMultipliers[Group] -= Multiplier - 1;
                        if (System.Math.Abs(specificMultipliers[Group]) < Mathf.Epsilon)
                        {
                            specificMultipliers.Remove(Group);
                        }
                    }
                }
                else
                {
                    specificMultipliers[GameplayModifierConstants.NON_GROUPED] /= Multiplier;
                }

                // When the multiplier its grouped, multiply all values of each group and cache the result
                // Cache the current result so can be modified and later set it again. (use this to  the error of modify the dict values while you loop it)
                double result = 1;
                foreach (string groupName in specificMultipliers.Keys)
                {
                    if (string.Equals(groupName, GameplayModifierConstants.RESULT))
                    {
                        continue;
                    }

                    result *= specificMultipliers[groupName];
                }

				specificMultipliers[GameplayModifierConstants.RESULT] = result;
			}
		}

		public override object GetValue()
		{
			return Multiplier;
		}
	}
}