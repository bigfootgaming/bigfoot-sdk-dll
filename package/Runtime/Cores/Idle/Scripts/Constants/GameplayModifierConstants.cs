/// <summary>
/// Gameplay modifier constants.
/// </summary>
public static class GameplayModifierConstants
{
    /// <summary>
    /// Constant to identify the result of all the multipliers
    /// </summary>
	public const string RESULT = "result";

    /// <summary>
    /// Constant to identify all of the multipliers without a group
    /// </summary>
	public const string NON_GROUPED = "nonGrouped";
}