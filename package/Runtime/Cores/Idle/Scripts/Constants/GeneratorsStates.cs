﻿public static class GeneratorsStates
{
    /// <summary>
    /// States of resource generation for when its not automated
    /// </summary>
    public enum StatesGeneration
    {
        IDLE,
        GENERATING,
    }

    public enum StatesCollection
    {
        EMPTY,
        WAITING_COLLECTION,
        COLLECTING
    }
}
