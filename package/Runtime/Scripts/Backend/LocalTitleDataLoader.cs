﻿namespace BigfootSdk.Backend
{
    public class LocalTitleDataLoader : ILoadable
    {
        public override void StartLoading()
        {
            TitleManager.Instance.LoadLocalTitleData();
            FinishedLoading(true);
        }
    }
}