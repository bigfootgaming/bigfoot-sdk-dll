﻿using System;
using UnityEngine;

namespace BigfootSdk.Backend
{
    public class ServerTitleDataLoader : ILoadable
    {
        public static Action<Action> OnReload;
        [SerializeField] private DependencyManager _dependencyManager;
        private Action _reloadCallback;
        
        public override void StartLoading()
        {
            TitleManager.Instance.LoadServerTitleData(FinishedLoading);
            OnReload += Reload;
        }

        public void Reload(Action callback)
        {
            OnReload -= Reload;
            _reloadCallback = callback;
            _dependencyManager.LoadNextScene = false;
            DependencyManager.OnEverythingLoaded += ReloadFinish;
            for (int i = 0; i < _dependencyManager.ThingsDoneLoading.Count; i++)
            {
                if (_dependencyManager.ThingsDoneLoading[i].Id == Id)
                {
                    for (int j = i; j < _dependencyManager.ThingsDoneLoading.Count; j++)
                    {
                        _dependencyManager.ThingsDoneLoading[j].IsLoaded = false;
                        _dependencyManager.ThingsDoneLoading[j].IsLoading = false;
                    }

                    _dependencyManager.ThingsDoneLoading.RemoveRange(i, _dependencyManager.ThingsDoneLoading.Count - i);
                    break;
                }
            }
            _dependencyManager.ThingsToLoad.Add(this);
            _dependencyManager.StartLoading();
        }

        void ReloadFinish()
        {
            _reloadCallback?.Invoke();
            DependencyManager.OnEverythingLoaded -= ReloadFinish;
        }
    }
}