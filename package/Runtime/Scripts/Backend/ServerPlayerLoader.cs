﻿using BigfootSdk.Helpers;

namespace  BigfootSdk.Backend
{
    public class ServerPlayerLoader : ILoadable
    {
        public override void StartLoading()
        {
            LogHelper.LogSdk("Loading Server Player Data", LogHelper.DEPENDENCY_MANAGER_TAG);
            PlayerManager.Instance.LoadServerPlayer(CompletePlayerInitialization);
        }

        /// <summary>
        /// After player data is completely loaded and synced, finish the player intialization
        /// </summary>
        /// <param name="success"></param>
        void CompletePlayerInitialization(bool success)
        {
            PlayerManager.Instance.InitializePlayerInfo();
            FinishedLoading(success);
        }
    }
}