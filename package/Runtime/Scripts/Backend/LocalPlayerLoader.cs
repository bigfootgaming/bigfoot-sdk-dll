﻿namespace  BigfootSdk.Backend
{
    public class LocalPlayerLoader : ILoadable
    {
        public override void StartLoading()
        {
            PlayerManager.Instance.LoadLocalPlayer();
            FinishedLoading(true);
        }
    }
}


