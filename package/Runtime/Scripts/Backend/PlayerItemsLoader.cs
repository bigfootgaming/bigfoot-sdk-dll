﻿namespace BigfootSdk.Backend
{
    public class PlayerItemsLoader : ILoadable
    {
        public override void StartLoading()
        {
            PlayerManager.Instance.LoadPlayerItemsModifiers();
            FinishedLoading(true);
        }
    }

}

