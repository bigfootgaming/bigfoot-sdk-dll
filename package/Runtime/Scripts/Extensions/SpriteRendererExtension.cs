using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk.Extensions
{
    public static class SpriteRendererExtension
    {
        /// <summary>
        /// Set a sprite into an SpriteRenderer component
        /// </summary>
        /// <param name="source">The SpriteRenderer image instance</param>
        /// <param name="sprite">Sprite to add</param>
        /// <param name="debugWarning">Should debug?</param>
        public static bool SetSprite(this SpriteRenderer source, Sprite sprite, bool debugWarning = true)
        {
            if (source == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set SpriteRenderer sprite. Null image instance");
                return false;
            }

            if (sprite == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set SpriteRenderer sprite. Null sprite instance");
                return false;
            }

            source.sprite = sprite;

            return source.sprite != null;
        }

        /// <summary>
        /// Set a sprite into an SpriteRenderer component between 2 options via a condition result
        /// </summary>
        /// <param name="source">The source SpriteRenderer instance</param>
        /// <param name="spriteOnTrue">Sprite to add if condition equals true</param>
        /// <param name="spriteOnFalse">Sprite to add if condition equals false</param>
        /// <param name="condition">The condition result to decide the sprite to set</param>
        /// <param name="debugWarning">Should debug?</param>
        public static bool SetSprite(this SpriteRenderer source, Sprite spriteOnTrue, Sprite spriteOnFalse, bool condition, bool debugWarning = true)
        {
            if (source == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set SpriteRenderer sprite. Null image instance");
                return false;
            }

            if (spriteOnTrue == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set SpriteRenderer sprite. Null sprite on true parameter instance");
                return false;
            }

            if (spriteOnFalse == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set SpriteRenderer sprite. Null sprite on false condition instance");
                return false;
            }

            source.sprite = condition ? spriteOnTrue : spriteOnFalse;

            return source.sprite != null;
        }
    }
}