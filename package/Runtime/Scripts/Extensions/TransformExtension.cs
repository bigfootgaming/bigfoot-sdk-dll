﻿using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk.Extensions
{
    public static class TransformExtension
    {
        /// <summary>
        /// Set the scale for the transform to 0
        /// </summary>
        /// <param name="source">The source transform instance</param>
        /// <param name="debugWarning">Should debug?</param>
        public static bool SetScaleToZero(this Transform source, bool debugWarning = true)
        {
            if (source == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set image sprite. Null image instance");
                return false;
            }

            source.localScale = Vector3.zero;
            return true;
        }
    }
}