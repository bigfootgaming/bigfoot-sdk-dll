// Decompiled with JetBrains decompiler
// Type: Firebase.Extensions.TaskExtension
// Assembly: Firebase.TaskExtension, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 810B51FA-43A4-48CD-B49D-85F30DA47996
// Assembly location: /Users/martinamat/Documents/Bigfoot/BigfootSDK/Assets/Firebase/Plugins/Firebase.TaskExtension.dll

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BigfootSdk.ErrorReport;
using BigfootSdk.Helpers;

namespace BigfootSdk.Extensions
{
  public static class TaskExtension
  {
    public static Task ContinueWithOnMainThread(this Task task, Action<Task> continuation) => (Task) task.ContinueWith<Task<bool>>((Func<Task, Task<bool>>) (t => RunOnMainThreadAsync<bool>((Func<bool>) (() =>
    {
      continuation(t);
      return true;
    })))).Unwrap<bool>();

    public static Task ContinueWithOnMainThread(
      this Task task,
      Action<Task> continuation,
      CancellationToken cancellationToken)
    {
      return (Task) task.ContinueWith<Task<bool>>((Func<Task, Task<bool>>) (t => RunOnMainThreadAsync<bool>((Func<bool>) (() =>
      {
        continuation(t);
        return true;
      }))), cancellationToken).Unwrap<bool>();
    }

    public static Task<TResult> ContinueWithOnMainThread<TResult>(
      this Task task,
      Func<Task, TResult> continuation)
    {
      return task.ContinueWith<Task<TResult>>((Func<Task, Task<TResult>>) (t => RunOnMainThreadAsync<TResult>((Func<TResult>) (() => continuation(t))))).Unwrap<TResult>();
    }

    public static Task<TResult> ContinueWithOnMainThread<TResult>(
      this Task task,
      Func<Task, TResult> continuation,
      CancellationToken cancellationToken)
    {
      return task.ContinueWith<Task<TResult>>((Func<Task, Task<TResult>>) (t => RunOnMainThreadAsync<TResult>((Func<TResult>) (() => continuation(t)))), cancellationToken).Unwrap<TResult>();
    }

    public static Task ContinueWithOnMainThread<T>(
      this Task<T> task,
      Action<Task<T>> continuation)
    {
      return (Task) task.ContinueWith<Task<bool>>((Func<Task<T>, Task<bool>>) (t => RunOnMainThreadAsync<bool>((Func<bool>) (() =>
      {
        continuation(t);
        return true;
      })))).Unwrap<bool>();
    }

    public static Task<TResult> ContinueWithOnMainThread<TResult, T>(
      this Task<T> task,
      Func<Task<T>, TResult> continuation)
    {
      return task.ContinueWith<Task<TResult>>((Func<Task<T>, Task<TResult>>) (t => RunOnMainThreadAsync<TResult>((Func<TResult>) (() => continuation(t))))).Unwrap<TResult>();
    }
    
    private static Dispatcher ThreadDispatcher { get; set; }
    
    static Task<TResult> RunOnMainThreadAsync<TResult>(Func<TResult> f) => ThreadDispatcher != null ? ThreadDispatcher.RunAsync<TResult>(f) : Dispatcher.RunAsyncNow<TResult>(f);

    public static void Init()
    {
      ThreadDispatcher = new Dispatcher();
    }

    public static void Reset()
    {
      ThreadDispatcher.Reset();
    }

    public static void Update()
    {
      ThreadDispatcher.PollJobs();
    }
  }
  
  internal class Dispatcher
  {
    private int ownerThreadId;
    private Queue<Action> queue = new Queue<Action>();

    public Dispatcher() => this.ownerThreadId = Thread.CurrentThread.ManagedThreadId;

    public void Reset()
    {
      queue.Clear();
    }

    public TResult Run<TResult>(Func<TResult> callback)
    {
      if (this.ManagesThisThread())
        return callback();
      EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
      Dispatcher.CallbackStorage<TResult> result = new Dispatcher.CallbackStorage<TResult>();
      lock ((object) this.queue)
        this.queue.Enqueue((Action) (() =>
        {
          try
          {
            result.Result = callback();
          }
          catch (Exception ex)
          {
            result.Exception = ex;
          }
          finally
          {
            waitHandle.Set();
          }
        }));
      waitHandle.WaitOne();
      return result.Exception == null ? result.Result : throw result.Exception;
    }

    public Task<TResult> RunAsync<TResult>(Func<TResult> callback)
    {
      if (this.ManagesThisThread())
        return Dispatcher.RunAsyncNow<TResult>(callback);
      TaskCompletionSource<TResult> tcs = new TaskCompletionSource<TResult>();
      lock ((object) this.queue)
        this.queue.Enqueue((Action) (() =>
        {
          try
          {
            tcs.SetResult(callback());
          }
          catch (Exception ex)
          {
            tcs.SetException(ex);
          }
        }));
      return tcs.Task;
    }

    internal static Task<TResult> RunAsyncNow<TResult>(Func<TResult> callback)
    {
      try
      {
        return Task.FromResult<TResult>(callback());
      }
      catch (Exception ex)
      {
        TaskCompletionSource<TResult> completionSource = new TaskCompletionSource<TResult>();
        completionSource.TrySetException(ex);
        return completionSource.Task;
      }
    }

    internal bool ManagesThisThread() => Thread.CurrentThread.ManagedThreadId == this.ownerThreadId;

    public void PollJobs()
    {
      while (true)
      {
        Action action;
        lock ((object) this.queue)
        {
          if (this.queue.Count <= 0)
            break;
          action = this.queue.Dequeue();
        }
        ExceptionAggregator.Wrap(action);
      }
    }

    private class CallbackStorage<TResult>
    {
      public TResult Result { get; set; }

      public Exception Exception { get; set; }
    }
  }
  
  internal class ExceptionAggregator
  {
    [ThreadStatic]
    private static List<Exception> threadLocalExceptions;

    private static List<Exception> Exceptions
    {
      get
      {
        if (ExceptionAggregator.threadLocalExceptions == null)
          ExceptionAggregator.threadLocalExceptions = new List<Exception>();
        return ExceptionAggregator.threadLocalExceptions;
      }
    }

    public static Exception GetAndClearPendingExceptions()
    {
      List<Exception> exceptions = ExceptionAggregator.Exceptions;
      int count = exceptions.Count;
      Exception exception = (Exception) null;
      if (count == 1)
        exception = exceptions[0];
      else if (count > 1)
        exception = (Exception) new AggregateException((IEnumerable<Exception>) exceptions.ToArray());
      exceptions.Clear();
      return exception;
    }

    public static void ThrowAndClearPendingExceptions()
    {
      Exception pendingExceptions = ExceptionAggregator.GetAndClearPendingExceptions();
      if (pendingExceptions != null)
      {
        ExceptionAggregator.LogException(pendingExceptions);
        throw pendingExceptions;
      }
    }

    public static Exception LogException(Exception exception)
    {
      if (exception != null)
      {
        if (exception is AggregateException aggregateException2)
        {
          List<string> stringList = new List<string>();
          foreach (Exception innerException in aggregateException2.Flatten().InnerExceptions)
            stringList.Add(innerException.ToString());
          LogHelper.LogError(string.Join("\n\n", stringList.ToArray()), Environment.StackTrace);
        }
        else
          LogHelper.LogError(exception.ToString(), Environment.StackTrace);
      }
      return exception;
    }

    public static void Wrap(Action action)
    {
      try
      {
        action();
      }
      catch (Exception ex)
      {
        ErrorReportManager.Instance.LogException(ex);
        ExceptionAggregator.Exceptions.Add(ex);
      }
    }

    public static T Wrap<T>(Func<T> func, T errorValue)
    {
      try
      {
        return func();
      }
      catch (Exception ex)
      {
        ErrorReportManager.Instance.LogException(ex);
        ExceptionAggregator.Exceptions.Add(ex);
      }
      return errorValue;
    }
  }
}
