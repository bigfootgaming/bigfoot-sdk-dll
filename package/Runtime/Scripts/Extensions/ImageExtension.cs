using BigfootSdk.Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk.Extensions
{
    public static class ImageExtension
    {
        /// <summary>
        /// Set a sprite into an image component
        /// </summary>
        /// <param name="source">The source image instance</param>
        /// <param name="sprite">Sprite to add</param>
        /// <param name="debugWarning">Should debug?</param>
        public static bool SetSprite(this Image source, Sprite sprite, bool debugWarning = true)
        {
            if (source == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set image sprite. Null image instance");
                return false;
            }

            if (sprite == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set image sprite. Null sprite instance");
                return false;
            }

            source.sprite = sprite;

            return source.sprite != null;
        }

        /// <summary>
        /// Set a sprite into an image component between 2 options via a condition result
        /// </summary>
        /// <param name="source">The source image instance</param>
        /// <param name="spriteOnTrue">Sprite to add if condition equals true</param>
        /// <param name="spriteOnFalse">Sprite to add if condition equals false</param>
        /// <param name="condition">The condition result to decide the sprite to set</param>
        /// <param name="debugWarning">Should debug?</param>
        public static bool SetSprite(this Image source, Sprite spriteOnTrue, Sprite spriteOnFalse, bool condition, bool debugWarning = true)
        {
            if (source == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set image sprite. Null image instance");
                return false;
            }

            if (spriteOnTrue == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set image sprite. Null sprite on true parameter instance");
                return false;
            }

            if (spriteOnFalse == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set image sprite. Null sprite on false condition instance");
                return false;
            }

            source.sprite = condition ? spriteOnTrue : spriteOnFalse;

            return source.sprite != null;
        }

        /// <summary>
        /// Set the color tint
        /// </summary>
        /// <param name="source">The source image instance</param>
        /// <param name="color">Color tint</param>
        /// <param name="debugWarning">Should debug?</param>
        public static bool SetColor(this Image source, Color color, bool debugWarning = true)
        {
            if (source == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set color to an image. Null image instance");
                return false;
            }

            source.color = color;

            return true;
        }

        /// <summary>
        /// Set the color tint between 2 options via a condition result
        /// </summary>
        /// <param name="source">The source image instance</param>
        /// <param name="colorOnTrue">Color tint if condition equals true</param>
        /// <param name="colorOnFalse">Color tint if condition equals false</param>
        /// <param name="condition">The condition result to decide the color to set</param>
        /// <param name="debugWarning">Should debug?</param>
        public static bool SetColor(this Image source, Color colorOnTrue, Color colorOnFalse, bool condition, bool debugWarning = true)
        {
            if (source == null)
            {
                if (debugWarning)
                    LogHelper.LogWarning("Unable to set color to an image. Null image instance");
                return false;
            }

            source.color = condition ? colorOnTrue : colorOnFalse;

            return true;
        }
    }
}