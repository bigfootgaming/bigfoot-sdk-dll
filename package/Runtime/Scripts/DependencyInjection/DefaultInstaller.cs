using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;
using BigfootSdk.Shop;
using Zenject;

namespace BigfootSdk.DependencyInjection
{
    public class DefaultInstaller : MonoInstaller
    {
        /// <summary>
        /// Configuration to be used in Dev
        /// </summary>
        public SdkConfiguration Configuration_Dev;

        /// <summary>
        /// Configuration to be used in Staging
        /// </summary>
        public SdkConfiguration Configuration_Staging;

        /// <summary>
        /// Configuration to be used in Prod
        /// </summary>
        public SdkConfiguration Configuration_Prod;
        
        /// <summary>
        /// Install all of the needed bindings for DI
        /// </summary>
        public override void InstallBindings()
        {
            LogHelper.LogSdk("Install bindings");
            Container.UnbindAll();
            #if ENVIRONMENT_PROD
            Container.Bind<SdkConfiguration>().FromInstance(Configuration_Prod).AsSingle();
            #elif ENVIRONMENT_STAGING            
            Container.Bind<SdkConfiguration>().FromInstance(Configuration_Staging).AsSingle();
            #else
            Container.Bind<SdkConfiguration>().FromInstance(Configuration_Dev).AsSingle();
            #endif

            InstallAnalyticsManagerBinding();
            InstallTitleManagerBinding();
            InstallShopManagerBinding();
            InstallAddressablesService();
        }

        /// <summary>
        /// Install the AnalyticsManager DI
        /// </summary>
        protected virtual void InstallAnalyticsManagerBinding()
        {
            #if ANALYTICS_DDNA
                Container.Bind<IAnalyticsService>().To<DAnalyticsService>().AsSingle();
            #endif
            #if FACEBOOK_ANALYTICS
                Container.Bind<IAnalyticsService>().To<FacebookAnalyticsService>().AsSingle();
            #endif
            #if BIGFOOT_ANALYTICS
                Container.Bind<IAnalyticsService>().To<BigfootAnalyticsService>().AsSingle();
            #endif
            #if UNITY_ANALYTICS
                Container.Bind<IAnalyticsService>().To<UnityAnalyticsService>().AsSingle();
            #endif
            #if FIREBASE_ANALYTICS
                Container.Bind<IAnalyticsService>().To<FirebaseAnalyticsService>().AsSingle();
            #endif
            #if AMPLITUDE
                Container.Bind<IAnalyticsService>().To<AmplitudeAnalyticsService>().AsSingle();
            #endif
        }


        protected virtual void InstallTitleManagerBinding()
        {
            
#if BACKEND_AWS
            Container.Bind<ITitleService>().To<AWSTitleService>().AsSingle();
#elif BACKEND_GS
            Container.Bind<ITitleService>().To<GSTitleService>().AsSingle();
#elif BACKEND_FIREBASE
            Container.Bind<ITitleService>().To<FirebaseTitleService>().AsSingle();
#endif
        }

        /// <summary>
        /// Install the ShopManager DI
        /// </summary>
        protected virtual void InstallShopManagerBinding()
        {
            #if UNITY_PURCHASING && (UNITY_ANDROID || UNITY_IPHONE)
                Container.Bind<IIapService>().To<UnityIAPService>().AsSingle();
                Container.Bind<IIapValidationService>().To<UnityIAPValidationService>().AsSingle();
            #endif
        }

        protected virtual void InstallAddressablesService()
        {
#if FIREBASE_REMOTE_ADDRESSABLES
            Container.Bind<IAddressablesService>().To<FirebaseAddressablesService>().AsSingle();
#else
            Container.Bind<IAddressablesService>().To<AddressablesServiceStub>().AsSingle();
#endif
        }
    }
}

