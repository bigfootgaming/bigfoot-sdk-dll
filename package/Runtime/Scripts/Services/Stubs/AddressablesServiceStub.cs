using System;
using BigfootSdk.Backend;

public class AddressablesServiceStub : IAddressablesService
{
    public override void InitializeAddressables(Action<bool> loadedCallback)
    {
        loadedCallback.Invoke(true);
    }
}
