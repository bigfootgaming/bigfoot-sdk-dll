﻿#if USE_IRONSOURCE
using System;
using BigfootSdk.Backend;

namespace BigfootSdk
{
    public class IronsourceService : AdsService
    {
        /// <summary>
        /// Initializes the Ads Service using parameters from SDK Config
        /// </summary>
        /// <param name="initializationFinished">Callback after initialization completes</param>
        /// <param name="adTypes">Type of ads to use</param>
        override public void Initialize(Action initializationFinished, int[] adTypes)
        {
            // Grab our configuration from our sdk
            IronsourceAdsConfiguration sdkConfig = SdkManager.Instance.Configuration.AdsConfiguration.IronsourceAdsConfiguration;
            
            // Set the sdk key
            IronSource.Agent.init(sdkConfig.GetAppKey());
            
            SubscribeToEvents();

            // Cache the Initialization callback
            initializationFinished?.Invoke();
        }

        /// <summary>
        /// Subscribes to all the events
        /// </summary>
        void SubscribeToEvents()
        {
            IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent; 
            IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
            IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent; 
            
            IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;
            IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent;
            IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent;
            IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
            IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
            
            IronSourceEvents.onBannerAdLoadedEvent += BannerAdLoadedEvent;
            IronSourceEvents.onBannerAdLoadFailedEvent += BannerAdLoadFailedEvent;        
            IronSourceEvents.onBannerAdClickedEvent += BannerAdClickedEvent; 
        }

        /// <summary>
        /// <returns><c>true</c>, if the <paramref name="placementId"/> is available, <c>false</c> otherwise.</returns>
        /// </summary>
        /// <param name="adType"> AdType to check for</param>
        /// <param name="placementId">Optional: Placement Id to verify</param>
        /// <returns></returns>
        override public bool IsAdAvailable(int adType, string placementId = "")
        {

            if (adType == AdTypes.INTERSTITIAL)
                return IronSource.Agent.isInterstitialReady();
            else if (adType == AdTypes.REWARDED_VIDEO)
                return IronSource.Agent.isRewardedVideoAvailable();
            else if ((adType == AdTypes.BANNER_TOP) ||
                     (adType == AdTypes.BANNER_BOTTOM))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Tries to show an Interstitial 
        /// </summary>
        /// <param name="placementId"> The placement id</param>
        /// <returns>If it showed the ad</returns>
        override public bool ShowInterstitial(string placementId = "")
        {
            if (IsAdAvailable(AdTypes.INTERSTITIAL))
            {
                if (!string.IsNullOrEmpty(placementId))
                {
                    IronSource.Agent.showInterstitial(placementId);
                }
                else
                {
                    IronSource.Agent.showInterstitial();
                }

                return true;
            }
            else
            {
                IronSource.Agent.loadInterstitial();

                return false;
            }
        }

        /// <summary>
        /// Tries to show a RewardedVideo 
        /// </summary>
        /// <param name="placementId"> The placement id</param>
        /// <returns>If it showed the ad</returns>
        override public bool ShowVideoReward(string placementId = "")
        {

#if UNITY_EDITOR
            AdsManager.OnRewardedVideoCompleted?.Invoke();
            AdsManager.OnRewardedVideoClosed?.Invoke();
            return true;
#endif
            if (IsAdAvailable(AdTypes.REWARDED_VIDEO))
            {
                if (!string.IsNullOrEmpty(placementId))
                {
                    IronSource.Agent.showRewardedVideo(placementId);
                }
                else
                {
                    IronSource.Agent.showRewardedVideo();
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Tries to show a Banner
        /// </summary>
        /// <param name="bannerType"> The banner type</param>
        /// <returns>If it showed the ad</returns>
        public override bool ShowBanner(int bannerType)
        {
            if(bannerType == AdTypes.BANNER_TOP)
                IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.TOP);
            else if (bannerType == AdTypes.BANNER_BOTTOM)
                IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
            else
                return false;

            return true;
        }

        /// <summary>
        /// Tries to hide a Banner
        /// </summary>
        /// <param name="bannerType"> The banner type</param>
        public override void HideBanner(int bannerType)
        {
            IronSource.Agent.hideBanner();
        }

        void OnApplicationPause(bool isPaused) {                 
            IronSource.Agent.onApplicationPause(isPaused);
        }

         /// <summary>
         /// Invoked when the user completed the video and should be rewarded. 
         /// </summary>
         /// <param name="placement">Placement object which contains the reward data</param>
         void RewardedVideoAdRewardedEvent(IronSourcePlacement placement) {
              AdsManager.OnRewardedVideoCompleted?.Invoke();
         }
  
        /// <summary>
        /// Invoked when the Rewarded Video failed to show
        /// </summary>
        /// <param name="error">Contains information about the failure.</param>
        void RewardedVideoAdShowFailedEvent (IronSourceError error){
          AdsManager.OnRewardedVideoShowFailed?.Invoke(error.getErrorCode().ToString());
        }

        /// <summary>
        /// Invoked when the RewardedVideo ad view is about to be closed.
        /// </summary>
        void RewardedVideoAdClosedEvent() {
            AdsManager.OnRewardedVideoClosed?.Invoke();
        }
      
        /// <summary>
        /// Invoked when the initialization process has failed.
        /// </summary>
        /// <param name="error">Contains information about the failure</param>
        void InterstitialAdLoadFailedEvent (IronSourceError error)
        {
            AdsManager.OnInterstitialFailedToLoad();
        }
        
        /// <summary>
        /// Invoked right before the Interstitial screen is about to open.
        /// </summary>
        void InterstitialAdShowSucceededEvent()
        {
            AdsManager.OnInterstitialShown();
        }
          
        /// <summary>
        /// Invoked when the ad fails to show.
        /// </summary>
        /// <param name="error">Contains information about the failure</param>
        void InterstitialAdShowFailedEvent(IronSourceError error)
        {
            AdsManager.OnInterstitialShowFailed();
        }
        
        /// <summary>
        /// Invoked when end user clicked on the interstitial ad
        /// </summary>
        void InterstitialAdClickedEvent ()
        {
            AdsManager.OnInterstitialClicked();
        }
      
        /// <summary>
        /// Invoked when the interstitial ad closed and the user goes back to the application screen.
        /// </summary>
        void InterstitialAdClosedEvent ()
        {
            AdsManager.OnInterstitialClosed();
        }
        
        /// <summary>
        /// Invoked once the banner has loaded
        /// </summary>
        void BannerAdLoadedEvent()
        {
            AdsManager.OnBannerLoaded(0);
        }
        
        /// <summary>
        /// Invoked when the banner loading process has failed.
        /// </summary>
        /// <param name="error">Contains information about the failure</param>
        void BannerAdLoadFailedEvent (IronSourceError error)
        {
            AdsManager.OnBannerLoadFailed();
        }
        
        /// <summary>
        /// Invoked when end user clicks on the banner ad
        /// </summary>
        void BannerAdClickedEvent ()
        {
            AdsManager.OnBannerClicked();
        }
        
    }
}
#endif