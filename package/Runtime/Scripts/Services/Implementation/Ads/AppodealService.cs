﻿#if USE_APPODEAL
using UnityEngine;
using System;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using BigfootSdk.Backend;

namespace BigfootSdk
{
    public class AppodealService : AdsService, IInterstitialAdListener, IRewardedVideoAdListener, IBannerAdListener
    {
        private int _adTypes;
        
        /// <summary>
        /// Initializes the Ads Service using parameters from SDK Config
        /// </summary>
        /// <param name="initializationFinished">Callback after initialization completes</param>
        /// <param name="adTypes">Type of ads to use</param>
        override public void Initialize(Action initializationFinished, int[] adTypes)
        {
            // Save Ad types for later use
            foreach (var adType in adTypes)
            {
                _adTypes = _adTypes | adType;
            }
            
            // Grab our configuration from our sdk
            AppodealAdsConfiguration sdkConfig = SdkManager.Instance.Configuration.AdsConfiguration.AppodealAdsConfiguration;

            // Appkey from Appodeal website
            string appKey = sdkConfig.AppKey;
            
            // Testing Mode
            bool testingEnvironment = SdkManager.Instance.Configuration.Environment == "dev";
            
            // Check if Location Permissions are asked for
            bool permitLocation = sdkConfig.UseLocationServices;
            
            // Child Directed app
            bool isChildsApp = sdkConfig.ChildDirectedApp;

            
            if (!permitLocation)
            {
                Appodeal.disableLocationPermissionCheck();
            }

            if (testingEnvironment)
            {
                Appodeal.setLogLevel(Appodeal.LogLevel.Debug);
            }
            else
            {
                Appodeal.setLogLevel(Appodeal.LogLevel.None);                
            }

            // Disable Selected Networks:
            foreach (var network in sdkConfig.DisabledNetworks)
            {
                Debug.Log("Disabled Provider: " + network);
                Appodeal.disableNetwork(network);
            }
            
            // Set if Testing environment and initialize sdk
            Appodeal.setTesting(testingEnvironment);
            
            // Disable data collection for kids apps
            Appodeal.setChildDirectedTreatment(isChildsApp);
            
            Appodeal.initialize(appKey, _adTypes);

            // Set this class to listen for Appodeal Callbacks
            Appodeal.setInterstitialCallbacks(this);
            Appodeal.setRewardedVideoCallbacks(this);

            // Trigger Initialization callback.
            initializationFinished?.Invoke();
        }
        

        /// <summary>
        /// <returns><c>true</c>, if the <paramref name="placementId"/> is available, <c>false</c> otherwise.</returns>
        /// </summary>
        /// <param name="adType"> AdType to check for</param>
        /// <param name="placementId">Optional: Placement Id to verify</param>
        /// <returns></returns>
        override public bool IsAdAvailable(int adType, string placementId = "")
        {
            if (string.IsNullOrEmpty(placementId))
                return Appodeal.canShow(adType);
            
            return Appodeal.canShow(adType, placementId);
        }

        /// <summary>
        /// Ask to show Interstitial
        /// </summary>
        override public bool ShowInterstitial(string placementId = "")
        {
            if (Appodeal.isLoaded(Appodeal.INTERSTITIAL) && !Appodeal.isPrecache(Appodeal.INTERSTITIAL))
            {
                if (!string.IsNullOrEmpty(placementId))
                {
                    return Appodeal.show(Appodeal.INTERSTITIAL, placementId);
                }
                else
                {
                    return Appodeal.show(Appodeal.INTERSTITIAL);
                }
            }
            else
            {
                Appodeal.cache(Appodeal.INTERSTITIAL);
                return false;
            }
        }

        /// <summary>
        /// Ask to show RewardedVideo
        /// </summary>
        override public bool ShowVideoReward(string placementId)
        {
            #if UNITY_EDITOR
                AdsManager.OnRewardedVideoCompleted?.Invoke();
                AdsManager.OnRewardedVideoClosed?.Invoke();
                return true;
            #endif
            
            if (Appodeal.canShow(Appodeal.REWARDED_VIDEO))
            {
                if (!string.IsNullOrEmpty(placementId))
                {
                    return Appodeal.show(Appodeal.REWARDED_VIDEO, placementId);
                }
                else
                {
                    return Appodeal.show(Appodeal.REWARDED_VIDEO);
                }
            }
            else
            {
                Appodeal.cache(Appodeal.REWARDED_VIDEO);
                return false;
            }
        }

        /// <summary>
        /// Tries to show a Banner
        /// </summary>
        /// <param name="bannerType"> The banner type</param>
        /// <returns>If it showed the ad</returns>
        public override bool ShowBanner(int bannerType)
        {
            if(bannerType == AdTypes.BANNER_TOP)
                return Appodeal.show(Appodeal.BANNER_TOP);
            else if (bannerType == AdTypes.BANNER_BOTTOM)
                return Appodeal.show(Appodeal.BANNER_BOTTOM);
            else
                return false;
        }
        
        /// <summary>
        /// Tries to hide a Banner
        /// </summary>∫
        public override void HideBanner(int bannerType)
        {
            if (bannerType == AdTypes.BANNER_TOP)
                Appodeal.hide(AdTypes.BANNER_TOP);
            else if (bannerType == AdTypes.BANNER_BOTTOM)
                Appodeal.hide(AdTypes.BANNER_BOTTOM);
        }
        
        /// <summary>
        /// Notify the Ads SDK if it has application focus.
        /// </summary>
        /// <param name="hasFocus"> set to <c>true</c> if the application has focus</param>
        override public void OnApplicationFocus(bool hasFocus) {
            if(hasFocus) {
                Appodeal.onResume(_adTypes);
            }
        }

        #region Interstitial callback handlers
        // ----------------------------------------------------------------------------//
        // Make all Listeners private, to enforce the use of the AdService Callbacks. -//
        // ----------------------------------------------------------------------------//
        void IInterstitialAdListener.onInterstitialLoaded(bool isPrecache)
        {
            AdsManager.OnInterstitialLoaded?.Invoke();
        }

        void IInterstitialAdListener.onInterstitialFailedToLoad()
        {
            AdsManager.OnInterstitialFailedToLoad?.Invoke();
        }

        void IInterstitialAdListener.onInterstitialShown()
        {
            AdsManager.OnInterstitialShown?.Invoke();
        }

        void IInterstitialAdListener.onInterstitialShowFailed()
        {
            AdsManager.OnInterstitialShowFailed?.Invoke();
        }

        void IInterstitialAdListener.onInterstitialClosed()
        {
            AdsManager.OnInterstitialClosed?.Invoke();
        }

        void IInterstitialAdListener.onInterstitialClicked()
        {
            AdsManager.OnInterstitialClicked?.Invoke();
        }

        void IInterstitialAdListener.onInterstitialExpired()
        {
            AdsManager.OnInterstitialExpired?.Invoke();
        }
        
        
        #endregion
        #region Rewarded Video callback handlers
        // ----------------------------------------------------------------------------//
        // Make all Listeners private, to enforce the use of the AdService Callbacks. -//
        // ----------------------------------------------------------------------------//
        void IRewardedVideoAdListener.onRewardedVideoLoaded(bool precache)
        {
            AdsManager.OnRewardedVideoLoaded?.Invoke();
        }

        void IRewardedVideoAdListener.onRewardedVideoFailedToLoad()
        {
            AdsManager.OnRewardedVideoFailedToLoad?.Invoke("");
        }

        void IRewardedVideoAdListener.onRewardedVideoShown()
        {
            AdsManager.OnRewardedVideoShown?.Invoke();
        }

        void IRewardedVideoAdListener.onRewardedVideoShowFailed()
        {
            AdsManager.OnRewardedVideoShowFailed?.Invoke("");
        }

        void IRewardedVideoAdListener.onRewardedVideoFinished(double amount, string name)
        {
            AdsManager.OnRewardedVideoFinished?.Invoke(amount, name);
        }

        void IRewardedVideoAdListener.onRewardedVideoClosed(bool finished)
        {
            if (finished)
                AdsManager.OnRewardedVideoCompleted?.Invoke();
            AdsManager.OnRewardedVideoClosed?.Invoke();
        }

        void IRewardedVideoAdListener.onRewardedVideoExpired()
        {
            AdsManager.OnRewardedVideoExpired?.Invoke();
        }

        void IRewardedVideoAdListener.onRewardedVideoClicked()
        {
            AdsManager.OnRewardedVideoClicked?.Invoke();
        }
        #endregion

        public void onBannerLoaded(int height, bool isPrecache)
        {
            AdsManager.OnBannerLoaded?.Invoke(height);
        }

        public void onBannerFailedToLoad()
        {
            AdsManager.OnBannerLoadFailed?.Invoke();
        }

        public void onBannerShown()
        {
            AdsManager.OnBannerShown?.Invoke();
        }

        public void onBannerClicked()
        {
            AdsManager.OnBannerClicked?.Invoke();
        }

        public void onBannerExpired()
        {
            AdsManager.OnBannerExpired?.Invoke();
        }
    }
}
#endif