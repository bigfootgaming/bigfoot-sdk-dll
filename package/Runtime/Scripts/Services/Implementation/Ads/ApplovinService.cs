﻿#if USE_APPLOVIN
using System;
using System.Threading.Tasks;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using Firebase.Extensions;
using UnityEngine;

namespace BigfootSdk
{
    public class ApplovinService : AdsService
    {
        public static Action<string, MaxSdkBase.AdInfo> OnRewardedAdCompleted;
        public static Action<string, MaxSdkBase.AdInfo> OnRewardedAdClosed;
        public static Action<string, MaxSdkBase.ErrorInfo, MaxSdkBase.AdInfo> OnRewardedAdFailedToDisplay;
        
        /// <summary>
        /// The config for applovin
        /// </summary>
        private ApplovinAdsConfiguration _applovinConfig;

        /// <summary>
        /// The ad unit for interstitials
        /// </summary>
        private string _interstitialAdUnit;
        
        /// <summary>
        /// The ad unit for rewarded
        /// </summary>
        private string _rewardedAdUnit;
        
        /// <summary>
        /// The ad unit for banners 
        /// </summary>
        private string _bannerAdUnit;

        private int _retryAttempt = 0;

        private Action _initializationFinished;

        private bool _loadingRewardedAd;

        private MaxCmpError _failureMessage;

        private bool _cmpCallback;

        private int _waitingTime;
        
        /// <summary>
        /// Initializes the Ads Service using parameters from SDK Config
        /// </summary>
        /// <param name="initializationFinished">Callback after initialization completes</param>
        /// <param name="adTypes">Type of ads to use</param>
        override public void Initialize(Action initializationFinished, int[] adTypes)
        {
            // Subscribe to events
            MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent += OnInterstitialFailedEvent;
            MaxSdkCallbacks.Interstitial.OnAdHiddenEvent += OnInterstitialDismissedEvent;

            MaxSdkCallbacks.Rewarded.OnAdLoadedEvent += OnRewardedAdLoadedEvent;
            MaxSdkCallbacks.Rewarded.OnAdLoadFailedEvent += OnRewardedAdFailedToLoadEvent;
            MaxSdkCallbacks.Rewarded.OnAdDisplayedEvent += OnRewardedAdDisplayedEvent;
            MaxSdkCallbacks.Rewarded.OnAdClickedEvent += OnRewardedAdClickedEvent;
            MaxSdkCallbacks.Rewarded.OnAdRevenuePaidEvent += OnRewardedAdRevenuePaidEvent;
            MaxSdkCallbacks.Rewarded.OnAdHiddenEvent += OnRewardedAdDismissedEvent;
            MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent += OnRewardedAdFailedToDisplayEvent;
            MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;
            PlayerManager.OnPlayerIdSet += SetUserId;
            
            // Grab our configuration from our sdk
            _applovinConfig = SdkManager.Instance.Configuration.AdsConfiguration.ApplovingAdsConfiguration;
            _initializationFinished = initializationFinished;
#if UNITY_IPHONE
            _interstitialAdUnit = _applovinConfig.AppleInterstitialAdUnitId;
            _rewardedAdUnit = _applovinConfig.AppleRewardedVideoAdUnitId;
            _bannerAdUnit = _applovinConfig.AppleBannerAdUnitId;
#else
            _interstitialAdUnit = _applovinConfig.AndroidInterstitialAdUnitId;
            _rewardedAdUnit = _applovinConfig.AndroidRewardedVideoAdUnitId;
            _bannerAdUnit = _applovinConfig.AndroidBannerAdUnitId;
#endif

            var userId = PlayerManager.Instance.GetPlayerId();
            SetUserId(userId);
            
            // Set the sdk key
            MaxSdk.SetSdkKey(_applovinConfig.AppKey);
            
            // Initialize the sdk
            MaxSdk.InitializeSdk();

            MaxSdkCallbacks.OnSdkInitializedEvent += InitializationFinished;

                 
        }

        public override void Reset()
        {
            MaxSdkCallbacks.OnSdkInitializedEvent -= InitializationFinished;
            
            MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent -= OnInterstitialFailedEvent;
            MaxSdkCallbacks.Interstitial.OnAdHiddenEvent -= OnInterstitialDismissedEvent;

            MaxSdkCallbacks.Rewarded.OnAdLoadedEvent -= OnRewardedAdLoadedEvent;
            MaxSdkCallbacks.Rewarded.OnAdLoadFailedEvent -= OnRewardedAdFailedToLoadEvent;
            MaxSdkCallbacks.Rewarded.OnAdDisplayedEvent -= OnRewardedAdDisplayedEvent;
            MaxSdkCallbacks.Rewarded.OnAdClickedEvent -= OnRewardedAdClickedEvent;
            MaxSdkCallbacks.Rewarded.OnAdRevenuePaidEvent -= OnRewardedAdRevenuePaidEvent;
            MaxSdkCallbacks.Rewarded.OnAdHiddenEvent -= OnRewardedAdDismissedEvent;
            MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent -= OnRewardedAdFailedToDisplayEvent;
            MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent -= OnRewardedAdReceivedRewardEvent;
            PlayerManager.OnPlayerIdSet -= SetUserId;
        }
        
        void InitializationFinished(MaxSdkBase.SdkConfiguration sdkConfiguration)
        {
            if (_applovinConfig.CustomCpmFlow)
            {
                MaxSdk.SetDoNotSell(false); 
#if !UNITY_EDITOR 
            if (PlayerPrefs.GetInt("CMP_SETUP", 0) == 0)
            {
                MaxSdkBase.CmpService.ShowCmpForExistingUser(SetupCmpPromptCallback);
                _waitingTime = 0;
                while (!_cmpCallback && _waitingTime < 3000)
                {
                    _waitingTime += 50;
                    Task.Delay(50);
                }

                CmpContinue();
            }
            else
#endif
                {
                    CmpFinished();
                }
            }
            else
            {
                CmpFinished();
            }
            
        }

        private void CmpContinue()
        {
            // If Cmp prompt is not shown (there's a failure message), the consent value needs to be set manually
            if (_failureMessage != null)
            {
                switch (_failureMessage.Code)
                {
                    // If the failure is due to the user being outside of GDPR (FormNotRequired)
                    // the game is running in the Editor (FormUnavailable)
                    // The consent value should be set to true and the prompt won't be shown again
                    case MaxCmpError.ErrorCode.FormUnavailable:
                    case MaxCmpError.ErrorCode.FormNotRequired:
                        MaxSdk.SetHasUserConsent(true);
                        PlayerPrefs.SetInt("CMP_SETUP", 1);
                        break;
                    
                    // If the failure is integration or not specified
                    // the consent value should be set to false and the prompt will be shown again in the next launch
                    case MaxCmpError.ErrorCode.IntegrationError:
                    case MaxCmpError.ErrorCode.Unspecified:
                    default:
                        MaxSdk.SetHasUserConsent(false);
                        break;
                }
            }
            // If Cmp prompt is shown (no failure message) Applovin handles setting the consent value based on the player's choice
            else
            {
                PlayerPrefs.SetInt("CMP_SETUP", 1);
            }
             

            CmpFinished();
        }

#if !UNITY_EDITOR         
        private void SetupCmpPromptCallback(MaxCmpError failureMessage)
        {
            _failureMessage = failureMessage;
            _cmpCallback = true;
        }
#endif     
        void CmpFinished()

        {
            if(_applovinConfig.DebugIntegration)
                MaxSdk.ShowMediationDebugger();

            LoadAds();

            // Cache the Initialization callback 
            // Summary: In iOS this represent when consent flow has been clicked
            // Expected behaviour MaxSdkBase.AppTrackingStatus.Authorized
            _initializationFinished?.Invoke();
        }

        void LoadAds()
        {
            // Preload the Interstitial
            if (!string.IsNullOrEmpty(_interstitialAdUnit))
            {
                MaxSdk.LoadInterstitial(_interstitialAdUnit);
            }

            // Preload the RewardedVideo
            if (!string.IsNullOrEmpty(_rewardedAdUnit))
            {
                LoadRewardedAd();
            }  
                
        }

        void SetUserId(string userId)
        {
            LogHelper.LogSdk($"Setting ApplovinMax User Id: {userId}");
            MaxSdk.SetUserId(userId);
        }

        /// <summary>
        /// <returns><c>true</c>, if the <paramref name="placementId"/> is available, <c>false</c> otherwise.</returns>
        /// </summary>
        /// <param name="adType"> AdType to check for</param>
        /// <param name="placementId">Optional: Placement Id to verify</param>
        /// <returns></returns>
        override public bool IsAdAvailable(int adType, string placementId = "")
        {
            if (_applovinConfig == null)
                return false;

            if (adType == AdTypes.INTERSTITIAL)
                return MaxSdk.IsInterstitialReady(_interstitialAdUnit);
            else if (adType == AdTypes.REWARDED_VIDEO)
                return MaxSdk.IsRewardedAdReady(_rewardedAdUnit);
            else if ((adType == AdTypes.BANNER_TOP) ||
                     (adType == AdTypes.BANNER_BOTTOM))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Tries to show an Interstitial 
        /// </summary>
        /// <param name="placementId"> The placement id</param>
        /// <returns>If it showed the ad</returns>
        override public bool ShowInterstitial(string placementId = "")
        {
            if (_applovinConfig == null)
                return false;

            if (IsAdAvailable(AdTypes.INTERSTITIAL))
            {
                if (!string.IsNullOrEmpty(placementId))
                {
                    MaxSdk.ShowInterstitial(_interstitialAdUnit, placementId);
                }
                else
                {
                    MaxSdk.ShowInterstitial(_interstitialAdUnit);
                }

                return true;
            }
            else
            {
                MaxSdk.LoadInterstitial(_interstitialAdUnit);

                return false;
            }
        }

        /// <summary>
        /// Tries to show a RewardedVideo 
        /// </summary>
        /// <param name="placementId"> The placement id</param>
        /// <returns>If it showed the ad</returns>
        override public bool ShowVideoReward(string placementId = "")
        {
            if (_applovinConfig == null)
                return false;

            if (IsAdAvailable(AdTypes.REWARDED_VIDEO))
            {
                if (!string.IsNullOrEmpty(placementId))
                {
                    MaxSdk.ShowRewardedAd(_rewardedAdUnit ,placementId);
                }
                else
                {
                    MaxSdk.ShowRewardedAd(_rewardedAdUnit);
                }

                return true;
            }
            else
            {
                LoadRewardedAd();

                return false;
            }
        }

        /// <summary>
        /// Tries to show a Banner
        /// </summary>
        /// <param name="bannerType"> The banner type</param>
        /// <returns>If it showed the ad</returns>
        public override bool ShowBanner(int bannerType)
        {
            if (_applovinConfig == null)
                return false;

            if(bannerType == AdTypes.BANNER_TOP)
                MaxSdk.CreateBanner(_bannerAdUnit, MaxSdkBase.BannerPosition.TopCenter);
            else if (bannerType == AdTypes.BANNER_BOTTOM)
                MaxSdk.CreateBanner(_bannerAdUnit, MaxSdkBase.BannerPosition.BottomCenter);
            else
                return false;

            return true;
        }

        /// <summary>
        /// Tries to hide a Banner
        /// </summary>
        /// <param name="bannerType"> The banner type</param>
        public override void HideBanner(int bannerType)
        {
            MaxSdk.HideBanner(_bannerAdUnit);
        }
        
        private void OnInterstitialFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo error)
        {
            AdsManager.OnInterstitialFailedToLoad?.Invoke();
        }

        private void OnInterstitialDismissedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            AdsManager.OnInterstitialClosed?.Invoke();
            MaxSdk.LoadInterstitial(_interstitialAdUnit);
        }

        private void OnRewardedAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            _loadingRewardedAd = false;
            _retryAttempt = 0;
            AdsManager.OnRewardedVideoLoaded?.Invoke();
        }

        private void OnRewardedAdFailedToLoadEvent(string adUnitId, MaxSdkBase.ErrorInfo error)
        {
            _loadingRewardedAd = false;
            _retryAttempt++;
            int retryDelay = (int) Math.Pow(2, Math.Min(6, _retryAttempt)) * 1000;

            Task.Delay(retryDelay).ContinueWithOnMainThread(task => LoadRewardedAd());
            AdsManager.OnRewardedVideoFailedToLoad?.Invoke(error.Code.ToString());
        }
        
        private void OnRewardedAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo error, MaxSdkBase.AdInfo adInfo)
        {
            LoadRewardedAd();
            AdsManager.OnRewardedVideoShowFailed?.Invoke(error.Code.ToString());
            OnRewardedAdFailedToDisplay?.Invoke(adUnitId, error, adInfo);
        }

        void LoadRewardedAd()
        {
            if (!MaxSdk.IsRewardedAdReady(_rewardedAdUnit) && !_loadingRewardedAd)
            {
                AdsManager.OnRewardedVideoStartedLoading?.Invoke();
                _loadingRewardedAd = true;
                MaxSdk.LoadRewardedAd(_rewardedAdUnit);
            }
        }

        private void OnRewardedAdDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            AdsManager.OnRewardedVideoShown?.Invoke();
           
        }

        private void OnRewardedAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            AdsManager.OnRewardedVideoClicked?.Invoke();
        }

        private void OnRewardedAdDismissedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            AdsManager.OnRewardedVideoClosed?.Invoke();
            OnRewardedAdClosed?.Invoke(adUnitId, adInfo);
            LoadNextRewardAd();
        }

        async void LoadNextRewardAd()
        {
            await Task.Delay(500);
            MemoryHelper.TryCleanMemory(true, true, "RewardedAdCompleted");
            LoadRewardedAd();
        }

        private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward, MaxSdkBase.AdInfo adInfo)
        {
            AdsManager.OnRewardedVideoCompleted?.Invoke();
            OnRewardedAdCompleted?.Invoke(adUnitId, adInfo);
        }

        private void OnRewardedAdRevenuePaidEvent(string adUnitId,  MaxSdkBase.AdInfo adInfo)
        {
        }
    }
}
#endif