﻿using System;

namespace BigfootSdk
{
    public class AdsService : IAdsService
    {
        /// <summary>
        /// Callback will be called when a new rewarded video is loaded and ready to be shown
        /// </summary>
        public Action<bool> OnRewardedVideoLoaded;

        /// <summary>
        /// Called if any error has ocurred and Rewarded has not being loaded
        /// </summary>
        public Action OnRewardedVideoFailedToLoad;

        /// <summary>
        /// Called when RewardedVideo has started playing
        /// </summary>
        public Action OnRewardedVideoShown;
        
        /// <summary>
        /// Called when RewardedVideo has failed to show
        /// </summary>
        public Action OnRewardedVideoShowFailed;

        /// <summary>
        /// Notyfies if the user has closed the rewarded video
        /// </summary>
        public Action<bool> OnRewardedVideoClosed;

        /// <summary>
        /// Called when the rewarded video has succesfully been played
        /// </summary>
        public Action<double,string> OnRewardedVideoFinished;

        /// <summary>
        /// Called when clicked over a Rewarded Video
        /// </summary>
        public Action OnRewardedVideoClicked;
        
        /// <summary>
        /// Called when Rewarded Video has already expired
        /// </summary>
        public Action OnRewardedVideoExpired;
        
        /// <summary>
        /// Callback will be called when a new interstitial video is loaded and ready to be shown
        /// </summary>
        public Action<bool> OnInterstitialVideoLoaded;
        
        /// <summary>
        /// Called if any error has ocurred and Interstitial has not being loaded
        /// </summary>
        public Action OnInterstitialVideoFailedToLoad;
        
        /// <summary>
        /// Called when Interstitial has started playing
        /// </summary>
        public Action OnInterstitialVideoShown;
        
        /// <summary>
        /// Called when Interstitial has failed to show
        /// </summary>
        public Action OnInterstitialShowFailed;
        
        /// <summary>
        /// Called when clicked over a Interstitial Video
        /// </summary>
        public Action OnInterstitialVideoClicked;
        
        /// <summary>
        /// Called when Interstitial Video has already expired
        /// </summary>
        public Action OnInterstitialVideoExpired;
        
        /// <summary>
        /// Notyfies if the user has closed the interstitial video
        /// </summary>
        public Action OnInterstitialVideoClosed;
        
        /// <summary>
        /// Initializes the Ads Service
        /// </summary>
        /// <param name="initializationFinished">Callback after initialization completes</param>
        /// <param name="adTypes">Type of ads to use</param>
        public virtual void Initialize(Action initializationFinished, int[] adTypes)
        {
            if (initializationFinished != null)
                initializationFinished();
        }

        public virtual void Reset()
        {
        }

        /// <summary>
        /// <returns><c>true</c>, if the <paramref name="placementId"/> is available, <c>false</c> otherwise.</returns>
        /// </summary>
        /// <param name="adType">Type of the Ad to check</param>
        /// <param name="placementId">Optional Placement Id to verify</param>
        /// <returns></returns>
        public virtual bool IsAdAvailable(int adType, string placementId = "")
        {
            return false;
        }

        /// <summary>
        /// Tries to show an Interstitial 
        /// </summary>
        /// <param name="placementId"> The placement id</param>
        /// <returns>If it showed the ad</returns>
        public virtual bool ShowInterstitial(string placementId = "")
        {
            return false;
        }

        /// <summary>
        /// Tries to show a RewardedVideo 
        /// </summary>
        /// <param name="placementId"> The placement id</param>
        /// <returns>If it showed the ad</returns>
        public virtual bool ShowVideoReward(string placementId = "")
        {
            return false;
        }

        /// <summary>
        /// Tries to show a Banner
        /// </summary>
        /// <param name="bannerType"> The banner type</param>
        public virtual bool ShowBanner(int bannerType)
        {
            return false;
        }
        
        /// <summary>
        /// Tries to hide a Banner
        /// </summary>
        /// <param name="bannerType"> The banner type</param>
        /// <returns>If it hide the ad</returns>
        public virtual void HideBanner(int bannerType)
        {
        }
        
        /// <summary>
        /// Notify the Ads SDK if it has application focus.
        /// </summary>
        /// <param name="hasFocus"> set to <c>true</c> if the application has focus</param>
        public virtual void OnApplicationFocus(bool hasFocus)
        {
            
        }
    }
}
