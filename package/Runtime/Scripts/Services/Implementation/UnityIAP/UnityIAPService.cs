﻿#if UNITY_PURCHASING
using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.Shop;
using UnityEngine;
using UnityEngine.Purchasing;

namespace BigfootSdk
{
    public class UnityIAPService : IIapService, IStoreListener
    {
        // The Unity Purchasing system.
        private static IStoreController m_StoreController;

        // The store-specific Purchasing subsystems.
        private static IExtensionProvider m_StoreExtensionProvider;
        private IAppleExtensions m_AppleExtensions;
        private ITransactionHistoryExtensions m_TransactionHistoryExtensions;
        private IGooglePlayStoreExtensions m_GooglePlayStoreExtensions;
        private Dictionary<string, string> m_introductoryInfoDict;
        
        private Product m_product;
        private Action<bool> m_initializeCallback;
        private Action<string, string> m_failCallback;
        private Action<string, string, string, object> m_androidCallback;
        private Action<string, string, object> m_iosCallback;
        
        
        private List<string> _productsAdded = new List<string>();
        
        /// <inheritdoc/>
        public void Connect(Action<bool> callback)
        {
            m_initializeCallback = callback;
                
            InitializeUnityIAP();
        }

        public void InitializeUnityIAP()
        {

            // Create a Configuration Builder
            ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            
#if UNITY_ANDROID && !UNITY_EDITOR
            builder.Configure<IGooglePlayConfiguration>().SetServiceDisconnectAtInitializeListener(() =>
            {
                ShopManager.Instance.ConnectionToStoreFailed();
            });
#endif

            // Get All Items from ItemsManager
            List<ShopItemModel> shopItems = ShopManager.Instance.GetAllShopItems();

            // Iterate and Add a Product for each Item
            int counter = shopItems.Count;
            for (int i = 0; i < counter; i++)
            {
                if (shopItems[i].Cost is RealMoneyCost realMoneyCost)
                {
                    LogHelper.LogSdk(string.Format("Adding IAP {0} - Google: {1} - iOS: {2} of type {3}", 
                        shopItems[i].ItemName, realMoneyCost.GoogleSKU, realMoneyCost.IosSKU, realMoneyCost.ProductType), LogHelper.SHOP_TAG);

                    ProductType productType = ProductType.Consumable;
                    if (realMoneyCost.ProductType == "NonConsumable")
                    {
                        productType = ProductType.NonConsumable;
                    }
                    else if (realMoneyCost.ProductType == "Subscription")
                    {
                        productType = ProductType.Subscription;
                    }
                    #if UNITY_ANDROID
                    if (!_productsAdded.Contains(realMoneyCost.GoogleSKU))
                    {
                        builder.AddProduct(shopItems[i].Id, productType, new IDs
                        {
                            {realMoneyCost.GoogleSKU, GooglePlay.Name},
                            {realMoneyCost.IosSKU, AppleAppStore.Name}
                        });
                        _productsAdded.Add(realMoneyCost.GoogleSKU);
                    }
                    #elif UNITY_IOS
                    if (!_productsAdded.Contains(realMoneyCost.IosSKU))
                    {
                        builder.AddProduct(shopItems[i].Id, productType, new IDs
                        {
                            {realMoneyCost.GoogleSKU, GooglePlay.Name},
                            {realMoneyCost.IosSKU, AppleAppStore.Name}
                        });
                        _productsAdded.Add(realMoneyCost.IosSKU);
                    }
                    #endif
                    
                }
            }

            

            // Initialize UnityIAP
            UnityPurchasing.Initialize(this, builder);
        }
        
        
        
        /// <inheritdoc/>
        public bool IsConnected()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }
        
#if UNITY_IOS
        /// <inheritdoc/>
        public void RestorePurchases(Action<string, string, object> callback)
        {
            m_iosCallback = callback;
            // If Purchasing has not yet been set up ...
            if (!IsConnected())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                LogHelper.LogError("RestorePurchases FAIL. Not initialized.", Environment.StackTrace);
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer || 
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                LogHelper.LogSdk("RestorePurchases started ...", LogHelper.SHOP_TAG);

                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                m_AppleExtensions.RestoreTransactions((result) => {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    LogHelper.LogSdk("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.", LogHelper.SHOP_TAG);
                });
            }
        }
#endif
        
        /// <inheritdoc/>
        public void PurchaseItemOnAndroid(string productId, Action<string, string, string, object> callback, Action<string, string> callbackFail)
        {
            m_failCallback = callbackFail;
            m_androidCallback = callback;
            BuyProductID(productId);            
        }
        
        /// <inheritdoc/>
        public void PurchaseItemOnIos(string productId, Action<string, string, object> callback, Action<string, string> callbackFail)
        {
            m_failCallback = callbackFail;
            m_iosCallback = callback;
            BuyProductID(productId);
        }

        /// <inheritdoc/>
        public void PurchaseItemOnWeb(string productId, Action callback, Action callbackFail)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialize Purchase on UnityIAP
        /// </summary>
        /// <param name="productId"> ItemID </param>
        void BuyProductID(string productId)
        {
            if (!IsConnected())
            {
                string errorMessage = "UnityIAPService.cs :: SDK Not Initialized, Initialize before trying a purchase";
                LogHelper.LogError(errorMessage, Environment.StackTrace);
                return;
            }

            // Grab the product
            m_product = m_StoreController.products.WithID(productId);

            // If the product is setup and available start the purchase.
            if (m_product != null && m_product.availableToPurchase)
            {
                LogHelper.LogSdk(string.Format("Purchasing product asychronously: '{0}'", m_product.definition.id), LogHelper.SHOP_TAG);
                m_StoreController.InitiatePurchase(m_product);
            }
            else
            {
                string errorMessage = string.Format("UnityIAPService.cs :: ProductID: '{0}' is is not a valid product, or is not available", productId);
                LogHelper.LogError(errorMessage, Environment.StackTrace);
            }
        }

        /// <summary>
        /// Get a product by id
        /// </summary>
        /// <param name="productId">The id of the product</param>
        /// <returns>The product</returns>
        public Product GetProductById(string productId)
        {
            return m_StoreController.products.WithID(productId);
        }
        
        /// <summary>
        /// Called when Unity IAP is ready to make purchases.
        /// </summary>
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            LogHelper.LogSdk("OnInitialized PASS", LogHelper.SHOP_TAG);
            
            // Setup Store and ExtensionProvider references
            m_StoreController = controller;
            m_StoreExtensionProvider = extensions;
            m_AppleExtensions = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            m_TransactionHistoryExtensions = m_StoreExtensionProvider.GetExtension<ITransactionHistoryExtensions>();
            m_GooglePlayStoreExtensions = m_StoreExtensionProvider.GetExtension<IGooglePlayStoreExtensions>();
            m_introductoryInfoDict = m_AppleExtensions.GetIntroductoryPriceDictionary();
                
                
            foreach (var item in m_StoreController.products.all)
            {
                if (item.availableToPurchase)
                {
                    // Set all these products to be visible in the user's App Store according to Apple's Promotional IAP feature
                    // https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/StoreKitGuide/PromotingIn-AppPurchases/PromotingIn-AppPurchases.html
                    m_AppleExtensions.SetStorePromotionVisibility(item, AppleStorePromotionVisibility.Show);

                    // this is the usage of SubscriptionManager class
                    if (item.hasReceipt)
                    {
                        if (item.definition.type == ProductType.Subscription)
                        {
                            string introJson =
                                (m_introductoryInfoDict == null ||
                                 !m_introductoryInfoDict.ContainsKey(item.definition.storeSpecificId))
                                    ? null
                                    : m_introductoryInfoDict[item.definition.storeSpecificId];
                            SubscriptionManager p = new SubscriptionManager(item, introJson);
                            SubscriptionInfo info = p.getSubscriptionInfo();
                            LogHelper.LogSdk(info.getSubscriptionInfoJsonString(), LogHelper.SHOP_TAG);
                        }
                    }
                    else
                    {
                        LogHelper.LogSdk(item.definition.id +" doesn't have receipt", LogHelper.SHOP_TAG);
                    }
                }
                else
                {
                    LogHelper.LogSdk(item.definition.id +" is not avaliable to purchase", LogHelper.SHOP_TAG);
                }
            }
            
            // Invoke Initialization Success Callback
            m_initializeCallback?.Invoke(true);
        }
        
        /// <summary>
        /// Called when Unity IAP encounters an unrecoverable initialization error.
        ///
        /// Note that this will not be called if Internet is unavailable; Unity IAP
        /// will attempt initialization until it becomes available.
        /// </summary>
        public void OnInitializeFailed(InitializationFailureReason error, string? message)
        {
            // Debug Log with Initialization Error
            string errorMessage = string.Format("UnityIAPService.cs :: OnInitialize Failed, InitializationFailureReason: {0}  message: {1}", error, message);
            LogHelper.LogSdk(errorMessage, LogHelper.SHOP_TAG);

            // Invoke Initialization Failure Callback
            m_initializeCallback?.Invoke(false);
        }
        
        /// <summary>
        /// Called when Unity IAP encounters an unrecoverable initialization error.
        ///
        /// Note that this will not be called if Internet is unavailable; Unity IAP
        /// will attempt initialization until it becomes available.
        /// </summary>
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Debug Log with Initialization Error
            string errorMessage = string.Format("UnityIAPService.cs :: OnInitialize Failed, InitializationFailureReason: {0}", error);
            LogHelper.LogSdk(errorMessage, LogHelper.SHOP_TAG);

            // Invoke Initialization Failure Callback
            m_initializeCallback?.Invoke(false);
        }

        /// <summary>
        /// Called when a purchase completes.
        ///
        /// May be called at any time after OnInitialized().
        /// </summary>
        public virtual PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            // Check Platform
            if (Application.platform == RuntimePlatform.Android)
            {
                if (m_androidCallback == null)
                {
                    m_androidCallback = ShopManager.Instance.AndroidValidation;
                }
                m_androidCallback.Invoke(e.purchasedProduct.definition.id, "", "", e);
            }
            else
            {
                if (m_iosCallback == null)
                {
                    m_iosCallback = ShopManager.Instance.IosValidation;
                }
                m_iosCallback.Invoke(e.purchasedProduct.definition.id, "", e);
            }
            
            

            // Determine Wich kind of product was purchased. (Define if we use it or not)
            if (e.purchasedProduct.definition.type == ProductType.Consumable)
            {
                LogHelper.LogSdk(string.Format("Purchased Consumable Item {0}", e.purchasedProduct.definition.id), LogHelper.SHOP_TAG);                
            }
            else if (e.purchasedProduct.definition.type == ProductType.NonConsumable)
            {
                LogHelper.LogSdk(string.Format("Purchased NonConsumable Item {0}", e.purchasedProduct.definition.id), LogHelper.SHOP_TAG);
            }
            else if (e.purchasedProduct.definition.type == ProductType.Subscription)
            {
                LogHelper.LogSdk(string.Format("Purchased Subscription Item {0}", e.purchasedProduct.definition.id), LogHelper.SHOP_TAG);
            }
            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            // Or when the purchase is pending validation
            return PurchaseProcessingResult.Pending;
        }

        /// <inheritdoc/>
        public void PurchaseCompleted(string productId)
        {
            // Confirm Product
            Product product = m_StoreController.products.WithID(productId);

            LogHelper.LogSdk(string.Format("Confirming peding purchase for product {0} ", productId), LogHelper.SHOP_TAG);

            m_StoreController.ConfirmPendingPurchase(product != null ? product : m_product);
            
            m_introductoryInfoDict = m_AppleExtensions.GetIntroductoryPriceDictionary();
            if (product.hasReceipt)
            {
                if (product.definition.type == ProductType.Subscription)
                {
                    string introJson =
                        (m_introductoryInfoDict == null ||
                         !m_introductoryInfoDict.ContainsKey(product.definition.storeSpecificId))
                            ? null
                            : m_introductoryInfoDict[product.definition.storeSpecificId];
                    SubscriptionManager p = new SubscriptionManager(product, introJson);
                    SubscriptionInfo info = p.getSubscriptionInfo();
                    LogHelper.LogSdk(info.getSubscriptionInfoJsonString(), LogHelper.SHOP_TAG);
                }
            }
            else
            {
                LogHelper.LogSdk(product.definition.id +" doesn't have receipt", LogHelper.SHOP_TAG);
            }
        }

        /// <summary>
        /// Called when a purchase fails.
        /// </summary>
        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            string errorMessage = string.Format("UnityIAPService.cs :: OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", i.definition.storeSpecificId, p);
            
            if(p != PurchaseFailureReason.UserCancelled)
                LogHelper.LogWarning(errorMessage);
            
            if(p == PurchaseFailureReason.DuplicateTransaction)
            {
                LogHelper.LogSdk("Trying to complete a pending transaction...", LogHelper.SHOP_TAG);
                PurchaseCompleted(i.definition.id);
            }

            m_failCallback?.Invoke(i.definition.id, p.ToString());
        }

        /// <inheritdoc/>
        public string GetLocalizedPrice(string skuId)
        {
            // Confirm Product
            if (IsConnected())
            {
                Product product = m_StoreController.products.WithStoreSpecificID(skuId);

                if(product != null)
                {
                    return product.metadata.localizedPriceString;
                }
            }
            return "";
        }

        public object GetSubscriptionInfo(string productId)
        {
            if (IsConnected())
            {
                Product product = m_StoreController.products.WithID(productId);
                if (product.hasReceipt)
                {
                    if (product.definition.type == ProductType.Subscription)
                    {
                        string introJson =
                            (m_introductoryInfoDict == null ||
                             !m_introductoryInfoDict.ContainsKey(product.definition.storeSpecificId))
                                ? null
                                : m_introductoryInfoDict[product.definition.storeSpecificId];
                        SubscriptionManager p = new SubscriptionManager(product, introJson);
                        SubscriptionInfo info = p.getSubscriptionInfo();
                        return info;
                    }
                }
            }
            return null;
        }

        public bool IsSubscriptionActive(string productId)
        {
            if (!IsConnected()) return false;
            var info = GetSubscriptionInfo(productId) as SubscriptionInfo;
            if (info != null)
                return info.isExpired() != Result.True;
            LogHelper.LogSdk("subcription info for " + productId +" doesn't exists", LogHelper.SHOP_TAG);
            return false;
        }

        public bool IsSubscriptionAutoRenewing(string productId)
        {
            if (!IsConnected()) return false;
            var info = GetSubscriptionInfo(productId) as SubscriptionInfo;
            if (info != null)
                return info.isAutoRenewing() == Result.True;
            LogHelper.LogSdk("subcription info for " + productId +" doesn't exists", LogHelper.SHOP_TAG);
            return false;
        }

        public long GetSubscriptionRemainingTime(string productId)
        {
            if (!IsConnected()) return 0;

            var info = GetSubscriptionInfo(productId) as SubscriptionInfo;
            if (info != null)
                return (long)info.getRemainingTime().TotalSeconds;
            LogHelper.LogSdk("subcription info for " + productId +" doesn't exists", LogHelper.SHOP_TAG);
            return 0;
        }

        public DateTime GetSubscriptionExpirationDate(string productId)
        {
            if (!IsConnected()) return TimeHelper.GetTimeInServerDateTime();

            var info = GetSubscriptionInfo(productId) as SubscriptionInfo;
            if (info != null)
                return info.getExpireDate();
            LogHelper.LogSdk("subcription info for " + productId +" doesn't exists", LogHelper.SHOP_TAG);
            return TimeHelper.GetTimeInServerDateTime();
        }

        public void UpdateSubscriptionOnAndroid(string oldProductId, string newProductId, Action<string, string, string, object> callback, Action<string, string> callbackFail)
        {
            m_failCallback = callbackFail;
            m_androidCallback = callback;
            if (IsConnected())
            {
                Product newProduct = m_StoreController.products.WithID(newProductId);

                Product oldProduct = m_StoreController.products.WithID(oldProductId);

                SubscriptionManager.UpdateSubscriptionInGooglePlayStore(oldProduct, newProduct,
                    (productInfos, newSubscriptionId) =>
                    {
                        m_GooglePlayStoreExtensions.UpgradeDowngradeSubscription(oldProduct.definition.id, newProduct.definition.id);
                    });
            }
        }
        
        public void UpdateSubscriptionOnIos(string newProductId, Action<string, string, object> callback, Action<string, string> callbackFail)
        {
            m_failCallback = callbackFail;
            m_iosCallback = callback;
            if (IsConnected())
            {
                Product newProduct = m_StoreController.products.WithID(newProductId);

                SubscriptionManager.UpdateSubscriptionInAppleStore(newProduct, null, (newProductArg, unknownArg) =>
                    m_StoreController.InitiatePurchase(newProductArg));
            }
        }

        public bool CanSubscribe(string productId)
        {
            if (!IsConnected()) return false;
            m_product = m_StoreController.products.WithID(productId);
            return !m_product.hasReceipt && m_product.definition.type == ProductType.Subscription;
        }
    }
}
#endif