﻿#if UNITY_PURCHASING
using BigfootSdk.Helpers;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using System;
using BigfootSdk.Shop;
using UnityEngine;

namespace BigfootSdk
{
    public class UnityIAPValidationService : IIapValidationService
    {
        /// <inheritdoc/>
        public virtual void GooglePlayPurchaseValidation(string itemToPurchase, string signature, string purchaseData, object purchase, Action<object, IapValidationResultModel> validationCallback)
        {
            CrossPlatformValidation(purchase, validationCallback);
        }
        
        /// <inheritdoc/>
        public virtual void IosPurchaseValidation(string itemToPurchase, string receipt, object purchase, Action<object, IapValidationResultModel> validationCallback)
        {
            CrossPlatformValidation(purchase, validationCallback);
        }

        public virtual void CrossPlatformValidation(object purchase, Action<object, IapValidationResultModel> validationCallback)
        {
            
            // Presume valid for platforms with no R.V.
            bool validPurchase = true; 
            
            PurchaseEventArgs purchaseEventArgs = (PurchaseEventArgs) purchase;
            IapValidationResultModel validation = new IapValidationResultModel();
            validation.ItemId = purchaseEventArgs.purchasedProduct.definition.id;
                  
// Unity IAP's validation logic is only included on these platforms.
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX

            var config = SdkManager.Instance.Configuration.IapValidationConfiguration.UnityIapValidationConfiguration;
            var googlePublicKey = Obfuscator.DeObfuscate(Convert.FromBase64String(config.GoogleIapValidationData), JsonHelper.DesarializeObject<int[]>(config.GoogleIapValidationOrder), config.GoogleIapValidationKey);
            var appleRootCert = Obfuscator.DeObfuscate(Convert.FromBase64String(config.AppleIapValidationData), JsonHelper.DesarializeObject<int[]>(config.AppleIapValidationOrder), config.AppleIapValidationKey);
            
            var validator = new CrossPlatformValidator(googlePublicKey, appleRootCert, Application.identifier); 

            try 
            {
                // On Google Play, result has a single product ID.
                // On Apple stores, receipts contain multiple products.
                var result = validator.Validate(purchaseEventArgs.purchasedProduct.receipt);
            } catch (IAPSecurityException) {
                Debug.Log("Invalid receipt, not unlocking content");
                validPurchase = false;
            }
#endif    
            // If the purchase was validated Create a new IapValidationResultModel and fill it
            if (!validPurchase) 
            {
                // If Transaction Was NOT OK, Invoke Callback with an invalid ValidationModel.
                validation.Error = "Invalid Receipt";
            }
            
            validationCallback(purchase, validation);
        }
    }
}
#endif