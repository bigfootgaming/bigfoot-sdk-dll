﻿#if ANALYTICS_DDNA
using BigfootSdk.Helpers;
using DeltaDNA;
using System;
using System.Collections.Generic;

namespace BigfootSdk.Analytics
{
	/// <summary>
	/// DeltaDNA analytics service.
	/// </summary>
	public class DAnalyticsService : IAnalyticsService
	{
		public void InitializeAnalytics (Action initializationFinished)
		{
			// Grab our configuration from our sdk
			ConfigurationModel sdkConfig = SdkManager.Instance.Config;

			// Create the configuration
			Configuration config = new Configuration ();
			config.engageUrl = sdkConfig.ddna_engageUrl;
			config.collectUrl = sdkConfig.ddna_collectUrl;
			config.environmentKeyDev = sdkConfig.ddna_environmentKeyDev;
			config.environmentKeyLive = sdkConfig.ddna_environmentKeyLive;
			config.useApplicationVersion = true;
            config.environmentKey = sdkConfig.environment != "production"? 0 : 1;

			// Sanity Check for DDNA configuration
			if (string.IsNullOrEmpty (sdkConfig.ddna_engageUrl) || string.IsNullOrEmpty (sdkConfig.ddna_collectUrl) || string.IsNullOrEmpty (sdkConfig.ddna_environmentKeyDev) || string.IsNullOrEmpty (sdkConfig.ddna_environmentKeyLive))
				LogHelper.LogWarning ("Some DeltaDNA configs are missing. Please make sure they are all filled in.");

			// Start collecting data
			DDNA.Instance.StartSDK (config);
			// Callback
			if(initializationFinished != null)
				initializationFinished ();
		}
		
		/// <summary>
        /// Tracks an economy event.
        /// </summary>
        /// <param name="eventName">Event name.</param>
        /// <param name="economyEvent">Economy event.</param>
		public void TrackEconomyEvent (string eventName, EconomyEventModel economyEvent)
		{
            // Populate the items spent
            Product spent = new Product ();
			if (economyEvent.ItemsSpent != null && economyEvent.ItemsSpent.Count > 0) {
				for (int i = 0; i < economyEvent.ItemsSpent.Count; i++) {
					EconomyEventParamenterModel param = economyEvent.ItemsSpent [i];
					spent.AddItem (param.Key, param.Type, (int)param.Amount);
				}
			}
            // Populate the virtual currencies spent
            if (economyEvent.VirtualCurrencySpent != null && economyEvent.VirtualCurrencySpent.Count > 0) {
				for (int i = 0; i < economyEvent.VirtualCurrencySpent.Count; i++) {
                    EconomyEventParamenterModel param = economyEvent.VirtualCurrencySpent [i];
					spent.AddVirtualCurrency (param.Key, param.Type, param.Amount);
				}
			}
            // Populate the real money spent
            if (economyEvent.RealMoneySpent != null) {
				spent.SetRealCurrency (economyEvent.RealMoneySpent.Key, (int)economyEvent.RealMoneySpent.Amount);

			}
            // Populate the items earned
            Product earned = new Product ();
			if (economyEvent.ItemsEarned != null && economyEvent.ItemsEarned.Count > 0) {
				for (int i = 0; i < economyEvent.ItemsEarned.Count; i++) {
                    EconomyEventParamenterModel param = economyEvent.ItemsEarned [i];
					earned.AddItem (param.Key, param.Type, (int)param.Amount);
				}
			}
            // Populate the virtual currencies earned
            if (economyEvent.VirtualCurrencyEarned != null && economyEvent.VirtualCurrencyEarned.Count > 0) {
				for (int i = 0; i < economyEvent.VirtualCurrencyEarned.Count; i++) {
                    EconomyEventParamenterModel param = economyEvent.VirtualCurrencyEarned [i];
					earned.AddVirtualCurrency (param.Key, param.Type, param.Amount);

				}
			}
            // Create the transaction
            Transaction transaction = new Transaction (eventName, economyEvent.Type, earned, spent);

            // Record the event
            DDNA.Instance.RecordEvent (transaction);
        }

        /// <summary>
        /// Tracks a custom event.
        /// </summary>
        /// <param name="eventName">Event name.</param>
        /// <param name="data">Data.</param>
        public void TrackCustomEvent (string eventName, Dictionary<string, object> data)
        {
            DDNA.Instance.RecordEvent(eventName, data);
        }

        /// <summary>
        /// Send saved events to server
        /// </summary>
        public void DispatchEvents()
        {
        }

        /// <summary>
        /// Sends Session Start Event with userID
        /// </summary>
        public void TrackSessionStartEvent()
        {
	        //
        }

        /// <summary>
        /// Tracks Session End, sending duration
        /// </summary>
        public void TrackSessionEndEvent()
        {
	        //
        }

        /// <summary>
        /// Tracks Error Events
        /// </summary>
        public void TrackErrorEvent()
        {
	        throw new NotImplementedException();
        }
    }
}
#endif