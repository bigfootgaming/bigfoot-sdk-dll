﻿#if BACKEND_GS
using GameSparks.Core;
using System;
using System.Linq;
using GameSparks.Api.Requests;
using System.Collections.Generic;
using Newtonsoft.Json;
using CodeStage.AntiCheat.ObscuredTypes;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// GameSparks Player service.
	/// </summary>
	public class GSPlayerService : IPlayerService
	{
		/// <summary>
		/// Initializes the player from the backend, filling in the PlayerModel in the PlayerManager
		/// </summary>
		/// <param name="callback">Callback.</param>
		public void InitializePlayer (Action callback = null)
		{
			// Grab a reference to the player model
			PlayerModel player = PlayerManager.Instance.Player;

			BackendManager.Instance.ExecuteCloudCode("GetPlayerData", null, successResult => {

                // Parse the player id
                if (successResult.ContainsKey ("playerId"))
                {
                    player.PlayerId = (string)successResult["playerId"];
                }


                // Parse the Currencies
                if (successResult.ContainsKey("currencies"))
                {
                    LogHelper.Log("sucess currencies");
                    var data = (GSData)successResult["currencies"];
                    foreach (var item in data.BaseData)
                    {
                        player.Currencies.Add(item.Key, Int32.Parse(data.BaseData[item.Key].ToString()));
                    }
                }


                // Parse the items
                if (successResult.ContainsKey("items"))
                {
                    player.Items = JsonHelper.DeserializeObjectList <PlayerItemModel>(successResult["items"].ToString());
                }

                // Parse the Player Data
                if (successResult.ContainsKey ("data")) {
                    var data = (GSData)successResult["data"];
                    foreach (var item in data.BaseData)
                    {
                        player.Data.Add(item.Key, (string)data.BaseData[item.Key]);
                    }


                }

                // Throw event that the player is initialized
                PlayerEvents.PlayerInitialized ();

				if (callback != null)
					callback ();
			});
		}

		/// <summary>
		/// Sets the player data.
		/// </summary>
		/// <param name="data">Data.</param>
		public void SetPlayerData (Dictionary<string, string> data, Action<bool> action)
		{
            //TODO 
            //implement action callback
			Dictionary<string, string> dataToSend = new Dictionary<string, string>();
			dataToSend.Add ("data", JsonHelper.SerializeObject (data));
			BackendManager.Instance.ExecuteCloudCode ("SetPlayerData", dataToSend);
		}

        /// <summary>
        /// Consumes the items.
        /// </summary>
        /// <param name="items">Items.</param>
        public void ConsumeItems(List<PlayerItemModel> items)
        {
            Dictionary<string, string> dataToSend = new Dictionary<string, string>();
            dataToSend.Add("itemModels", JsonHelper.SerializeObject(items));
            BackendManager.Instance.ExecuteCloudCode("ConsumeItems", dataToSend);
        }
    }
}
#endif
