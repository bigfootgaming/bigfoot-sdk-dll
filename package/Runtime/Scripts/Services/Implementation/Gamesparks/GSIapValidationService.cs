﻿#if BACKEND_GS

using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using GameSparks.Api.Requests;

namespace BigfootSdk.Shop
{
    public class GSIapValidationService : IIapValidationService
    {

        /// <summary>
        /// Google Play purchase validation.
        /// </summary>
        /// <param name="transaction">The item transaction to handle if the iap is validated.</param>
        /// <param name="signature">Signature.</param>
        /// <param name="purchaseData">Purchase data.</param>
        /// <param name="purchase">Purchase object.</param>
        /// <param name="iapServiceCallback">Iap service callback.</param>
        /// <param name="validationCallback">Validation callback.</param>
        public void GooglePlayPurchaseValidation(ItemTransaction transaction, string signature, string purchaseData, object purchase, Action<object> iapServiceCallback, Action<IapValidationResultModel> validationCallback = null)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("transaction", JsonHelper.SerializeObject(transaction));
            data.Add("signature", signature);
            data.Add("purchaseData", purchaseData);
            BackendManager.Instance.ExecuteCloudCode("ValidateGoogleIap", data, (result) =>
            {

                if (iapServiceCallback != null)
                    iapServiceCallback(purchase);
                if (result.ContainsKey("result"))
                {
                    IapValidationResultModel resultData = JsonHelper.DesarializeObject<IapValidationResultModel>(result["result"].ToString());
                    validationCallback(resultData);
                }
                else
                {
                    LogHelper.LogError("Callback does not contain a result");
                }
            });
        }

        /// <summary>
        /// Ios purchase validation.
        /// </summary>
        /// <param name="transaction">The item transaction to handle if the iap is validated.</param>
        /// <param name="receipt">Receipt.</param>
        /// <param name="purchase">Purchase object.</param>
        /// <param name="iapServiceCallback">Iap service callback.</param>
        /// <param name="validationCallback">Validation callback.</param>
        public void IosPurchaseValidation(ItemTransaction transaction, string receipt, object purchase, Action<object> iapServiceCallback, Action<IapValidationResultModel> validationCallback = null)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("transaction", JsonHelper.SerializeObject(transaction));
            data.Add("receiptData", receipt);
            BackendManager.Instance.ExecuteCloudCode("ValidateIosIap", data, (result) =>
            {

                if (iapServiceCallback != null)
                    iapServiceCallback(purchase);

                if (result.ContainsKey("result"))
                {
                    IapValidationResultModel resultData = JsonHelper.DesarializeObject<IapValidationResultModel>(result["result"].ToString());
                    validationCallback(resultData);
                }
                else
                {
                    LogHelper.LogError("Callback does not contain a result");
                }
            });
        }
    }
}
#endif