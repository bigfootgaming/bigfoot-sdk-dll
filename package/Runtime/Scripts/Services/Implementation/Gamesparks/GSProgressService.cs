﻿#if BACKEND_GS
using GameSparks.Core;
using System;
using System.Linq;
using GameSparks.Api.Requests;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// GameSparks Progress service.
	/// </summary>
	public class GSProgressService : IProgressService
	{
		/// <summary>
		/// Called when the Title is initialized
		/// </summary>
		public void TitleInitialized ()
		{
			// Grab a reference to the TitleModel
			TitleModel title = TitleManager.Instance.TitleData;

			// Make sure we have worlds to parse
			List<WorldModel> worlds = new List<WorldModel> ();
			if (title.Data.ContainsKey ("worlds")) {
				// Parse the worlds
				worlds = JsonHelper.DeserializeObjectList<WorldModel> (title.Data ["worlds"]);
			}

			// Make sure we have levels to parse
			if (title.Data.ContainsKey ("levels")) {
				// Parse the levels
				List<LevelModel> levels = JsonHelper.DeserializeObjectList<LevelModel> (title.Data ["levels"]);

				WorldModel currentWorld = null;
				foreach (LevelModel level in levels) {
					// Probably they will all come in order, so we add this improvement
					if (currentWorld == null || currentWorld.WorldId != level.WorldId) {
						currentWorld = worlds.Where (world => world.WorldId == level.WorldId).FirstOrDefault ();
					}

					// Add them to their corresponding world
					if (currentWorld != null) {
						currentWorld.Levels.Add (level);
					}
				}
			}

			// Add them to the ProgressManager
			ProgressManager.Instance.SetWorlds (worlds);

			// Throw event that the worlds are initialized
			ProgressEvents.WorldsInitialized ();
		}

		/// <summary>
		/// Called when the Title is initialized
		/// </summary>
		public void PlayerInitialized ()
		{
			// Grab a reference to the TitleModel
			PlayerModel player = PlayerManager.Instance.Player;

			// Make sure we have progress to parse
			List<WorldProgressModel> worlds = new List<WorldProgressModel> ();
			if (player.Data.ContainsKey ("progress")) {
				// Parse the progress
				worlds = JsonHelper.DeserializeObjectList<WorldProgressModel> (player.Data ["progress"]);
			}

			// Add them to the ProgressManager
			ProgressManager.Instance.SetWorldsProgress (worlds);

			// Throw event that the world's progress are initialized
			ProgressEvents.WorldsProgressInitialized ();
		}
	}
}
#endif

