﻿#if BACKEND_GS
using GameSparks.Core;
using System;
using System.Linq;
using GameSparks.Api.Requests;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using CodeStage.AntiCheat.ObscuredTypes;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// GameSparks Title service.
	/// </summary>
	public class GSTitleService : ITitleService
	{
		/// <summary>
		/// Initializes the Title from the backend
		/// </summary>		
		/// <param name="callback">Callback.</param>
		public void InitializeTitle (Action callback = null)
		{
            BackendManager.Instance.ExecuteCloudCode("GetTitleData", null, successResult =>
            {
            // Grab a reference to the TitleManager
            TitleManager manager = TitleManager.Instance;

                // Iterate through the title data, and add them to the model
                foreach (var item in successResult)
                {
                    manager.TitleData.Data.Add(item.Key, (string)successResult[item.Key]);
                }


                // Throw event that the title is initialized
                TitleEvents.TitleInitialized ();

				if (callback != null)
					callback ();
			});
		}
	}
}
#endif

