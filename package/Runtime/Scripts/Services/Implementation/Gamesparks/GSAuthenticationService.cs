﻿#if BACKEND_GS
using System;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// GameSparks Authentication service.
	/// </summary>
	public class GSAuthenticationService : IAuthenticationService
	{
		/// <summary>
		/// Registers on GameSparks with the device identifier.
		/// </summary>
		public void RegisterWithDeviceId (Action callback = null)
		{
			new GameSparks.Api.Requests.DeviceAuthenticationRequest ()
				.Send ((response) => {
				if (!response.HasErrors) {
					LogHelper.LogSdk ("Registration Ok");
					AuthenticationEvents.PlayerRegistered ();
					if (callback != null)
						callback ();
				} else {
					// HANDLE ERROR HERE
				}
			}
			);
		}
	}
}
#endif
