﻿#if BACKEND_GS
using GameSparks.Core;
using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using System.Diagnostics;

namespace BigfootSdk.Backend
{
	
	/// <summary>
	/// GameSparks Backend service.
	/// </summary>
	public class GSBackendService : IBackendService
	{
		/// <summary>
		/// Cached callback for when the backend is available
		/// </summary>
		Action _backendAvailableCallback;

		/// <summary>
		/// Checks if the backend is available to be used.
		/// </summary>
		/// <returns><c>true</c> if the backend is available; otherwise, <c>false</c>.</returns>
		public bool IsBackendAvailable ()
		{
			return GS.Available;
		}

		/// <summary>
		/// Listens for the event that the backend becomes available. Besides the optional callback,
		/// it will call BackendEvents.OnBackendAvailable
		/// </summary>
		/// <param name="callback">Optional callback</param>
		public void ListenBackendAvailable (Action callback = null)
		{
			// If its available right away
			if (GS.Available) {
				HandleGameSparksAvailable (true);
			} else {
				// Add the callback to the list of callbacks
				_backendAvailableCallback += callback;

				// Subscribe to the GameSparksAvailable event
				GS.GameSparksAvailable += HandleGameSparksAvailable;
			}
		}

		/// <summary>
		/// Handles the game sparks available.
		/// </summary>
		/// <param name="available">If set to <c>true</c> available.</param>
		void HandleGameSparksAvailable (bool available)
		{
			// If it's available
			if (available) {
				// Throw the backend available event
				BackendEvents.BackendAvailable ();

				// If we have callbacks
				if (_backendAvailableCallback != null) {
					_backendAvailableCallback ();
					_backendAvailableCallback = null;
				}
			}
		}

		public void ExecuteCloudCode (string methodName, Dictionary<string, string> data, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
		{
			LogEventRequest request = new LogEventRequest ().SetEventKey (methodName);

			if (data != null) {
				foreach (KeyValuePair<string, string> pair in data) {
					request.SetEventAttribute (pair.Key, pair.Value);
				}
			}
			request.Send ((response) => {
                LogHelper.LogSdk(response.JSONString);
				if (response.HasErrors) {
					if (failureCallback != null)
						failureCallback ();
				}	
				else
				{
					if (successCallback != null)
					{
						if(response.ScriptData != null)
                        {
                            successCallback((Dictionary<string, object>)response.ScriptData.BaseData);
                        }

                        else
                            successCallback(new Dictionary<string, object>());
                    }
                }
			});
		}
	}
}
#endif

