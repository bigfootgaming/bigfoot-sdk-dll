﻿#if FACEBOOK_ANALYTICS
using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using Facebook.Unity;
using BigfootSdk.Helpers;

namespace BigfootSdk.Analytics
{
    /// <summary>
    /// Facebook analytics service.
    /// </summary>
    public class FacebookAnalyticsService : IAnalyticsService
    {
        /// <summary>
        /// Initializes the analytics.
        /// </summary>
        /// <param name="initializationFinished">Initialization finished callback.</param>
        public virtual void InitializeAnalytics(Action initializationFinished)
        {
            if (FB.IsInitialized) {
                FB.ActivateApp();
            } else {
                //Handle FB.Init
                FB.Init( () => {
                    FB.ActivateApp();
                });
            }
            initializationFinished?.Invoke();
        }
        
        public virtual void Reset()
        {
            
        }
        
        /// <summary>
        /// Sets the current user id.
        /// </summary>
        /// <param name="userId"></param>
        public void SetUserId(string userId)
        {
            
        }

        /// <summary>
        /// Dispatchs the events.
        /// </summary>
        public void DispatchEvents()
        {
            // Not Needed
        }

        /// <summary>
        /// Tracks a transaction event.
        /// </summary>
        /// <param name="data">Transaction data.</param>
        public virtual void TrackTransactionEvent(TransactionEventModel data)
        {
            #if !UNITY_EDITOR
            if (FB.IsInitialized)
            {
                string eventName = "Transaction";
                
                // If this event needs to be discriminated for LTEs
                if (SdkManager.Instance.Configuration.AnalyticsConfiguration.EventsForLTE != null && SdkManager.Instance.Configuration.AnalyticsConfiguration.EventsForLTE.Contains(eventName))
                {
                    if(GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                        eventName += "_LTE";
                }

                var dictData = new Dictionary<string, object>()
                {
                    {"TransactionType", data.TransactionType},
                    {"Sinks", JsonHelper.SerializeObject(data.Sinks, false)},
                    {"Sources", JsonHelper.SerializeObject(data.Sources, false)}
                };

                TrackCustomEvent(eventName, dictData);
            }    
            #endif
        }

        /// <summary>
        /// Tracks a custom event.
        /// </summary>
        /// <param name="eventName">Event name.</param>
        /// <param name="data">Data.</param>
        public virtual void TrackCustomEvent(string eventName, Dictionary<string, object> data)
        {
            #if !UNITY_EDITOR
            if (FB.IsInitialized)
            {
                FB.LogAppEvent(eventName, 0, data);

                if(SdkManager.Instance.Configuration.AnalyticsConfiguration.DebugAnalyticsEvents)
                    LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", eventName, JsonHelper.SerializeObject(data)));
            }
            #endif
        }

        /// <summary>
        /// Tracks a custom property.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        /// <param name="propertyValue">Value.</param>
        public virtual void TrackCustomProperty(string propertyName, string propertyValue)
        {
            //not implemented in this service.
        }

        /// <summary>
        /// Tracks session start event
        /// </summary>
        public virtual void TrackSessionStartEvent()
        {
            // Not Needed
        }

        /// <summary>
        /// Tracks session end event
        /// </summary>
        public virtual void TrackSessionEndEvent()
        {
            // Not Needed
        }

        /// <summary>
        /// Tracks error event
        /// </summary>
        public virtual void TrackErrorEvent()
        {
            // Not Needed
        }
        
		/// <summary>
		/// Tracks an IAP Purchase
		/// </summary>
		/// <param name="currency">The 3 letter currency code</param>
		/// <param name="price">The price of this iap</param>
		/// <param name="data">The data we want to send along</param>
        public virtual void TrackIapPurchase(BaseItemModel itemModel, RealMoneyCost cost, string origin = "")
        {
            #if !UNITY_EDITOR
            if (FB.IsInitialized)
            {
                var data = new Dictionary<string, object>
                {
                    {"ItemName", itemModel.ItemName},
                    {"origin", origin},
                    {"price", cost.RealMoneyValue},
                };
                
                if (SdkManager.Instance.Configuration.AnalyticsConfiguration.EventsToIgnore != null && SdkManager.Instance.Configuration.AnalyticsConfiguration.EventsToIgnore.Contains("IapPurchase"))
                {
                    EventIgnored("IapPurchase", data);
                    return;
                }
                FB.LogPurchase(
                    cost.RealMoneyValue / 100f,
                    "USD",
                    data
                );
            }
            #endif
        }

        /// <summary>
        /// An event was ignored
        /// </summary>
        /// <param name="eventName">Event name.</param>
        /// <param name="data">Data.</param>
        public virtual void EventIgnored(string eventName, Dictionary<string, object> data)
        {
            // Not needed
        }
    }
}
#endif