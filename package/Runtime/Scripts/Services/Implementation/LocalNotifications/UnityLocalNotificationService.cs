﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.Notifications;
#if UNITY_ANDROID
using BigfootSdk.Notifications.Android;
using Unity.Notifications.Android;
#elif UNITY_IOS
using BigfootSdk.Notifications.iOS;
using Unity.Notifications.iOS;
#endif
using UnityEngine;

namespace BigfootSdk
{
    public class UnityLocalNotificationService : ILocalNotificationService
    {
	    // Flag set when we're in the foreground
	    protected bool inForeground = true;
	    
	    private IGameNotificationsPlatform _platform;

	    public IGameNotificationsPlatform Platform
	    {
		    get { return _platform; }
		    protected set { _platform = value; }
	    }
	    
        // Default filename for notifications serializer
        private const string DefaultFilename = "notifications.bin";
        /// <summary>
        /// Gets or sets the serializer to use to save pending notifications to disk if we're in
        /// <see cref="OperatingMode.RescheduleAfterClearing"/> mode.
        /// </summary>
        public IPendingNotificationsSerializer Serializer { get; set; }

        /// <summary>
        /// Get all of the scheduled local notifications
        /// </summary>
        protected List<LocalNotificationModel> _localNotifications;
		
        /// <summary>
        /// Get all of the scheduled local notifications
        /// </summary>
        public List<LocalNotificationModel> LocalNotifications => _localNotifications;

        /// <summary>
        /// Gets a collection of notifications that are scheduled or queued.
        /// </summary>
        public List<PendingNotification> PendingNotifications = new List<PendingNotification>();

        [Flags]
        public enum OperatingMode
        {
            /// <summary>
            /// Do not perform any queueing at all. All notifications are scheduled with the operating system
            /// immediately.
            /// </summary>
            NoQueue = 0x00,

            /// <summary>
            /// <para>
            /// Queue messages that are scheduled with this manager.
            /// No messages will be sent to the operating system until the application is backgrounded.
            /// </para>
            /// <para>
            /// If badge numbers are not set, will automatically increment them. This will only happen if NO badge numbers
            /// for pending notifications are ever set.
            /// </para>
            /// </summary>
            Queue = 0x01,

            /// <summary>
            /// When the application is foregrounded, clear all pending notifications.
            /// </summary>
            ClearOnForegrounding = 0x02,

            /// <summary>
            /// After clearing events, will put future ones back into the queue if they are marked with <see cref="PendingNotification.Reschedule"/>.
            /// </summary>
            /// <remarks>
            /// Only valid if <see cref="ClearOnForegrounding"/> is also set.
            /// </remarks>
            RescheduleAfterClearing = 0x04,

            /// <summary>
            /// Combines the behaviour of <see cref="Queue"/> and <see cref="ClearOnForegrounding"/>.
            /// </summary>
            QueueAndClear = Queue | ClearOnForegrounding,

            /// <summary>
            /// <para>
            /// Combines the behaviour of <see cref="Queue"/>, <see cref="ClearOnForegrounding"/> and
            /// <see cref="RescheduleAfterClearing"/>.
            /// </para>
            /// <para>
            /// Ensures that messages will never be displayed while the application is in the foreground.
            /// </para>
            /// </summary>
            QueueClearAndReschedule = Queue | ClearOnForegrounding | RescheduleAfterClearing,
        }

        
        private OperatingMode _mode = OperatingMode.QueueAndClear;

        /// <summary>
        /// Gets the operating mode for this manager.
        /// </summary>
        /// <seealso cref="OperatingMode"/>
        public OperatingMode Mode => _mode;
        
        /// <summary>
        /// Event fired when a scheduled local notification is delivered while the app is in the foreground.
        /// </summary>
        public event Action<PendingNotification> LocalNotificationDelivered;
        
        /// <summary>
        /// Event fired when a queued local notification is cancelled because the application is in the foreground
        /// when it was meant to be displayed.
        /// </summary>
        /// <seealso cref="OperatingMode.Queue"/>
        public event Action<PendingNotification> LocalNotificationExpired;
        
        public void Initialize(List<LocalNotificationModel> notifications, params GameNotificationChannel[] channels)
        {
	        _mode = LocalNotificationsManager.Instance.Mode;
#if UNITY_ANDROID
            Platform = new AndroidNotificationsPlatform();

            // Register the notification channels
            var doneDefault = false;
            foreach (GameNotificationChannel notificationChannel in channels)
            {
                if (!doneDefault)
                {
                    doneDefault = true;
                    ((AndroidNotificationsPlatform) Platform).DefaultChannelId = notificationChannel.Id;
                }

                // Wrap channel in Android object
                var androidChannel = new AndroidNotificationChannel(notificationChannel.Id, notificationChannel.Name,
                    notificationChannel.Description,
                    (Importance) notificationChannel.Style)
                {
                    CanBypassDnd = notificationChannel.HighPriority,
                    CanShowBadge = notificationChannel.ShowsBadge,
                    EnableLights = notificationChannel.ShowLights,
                    EnableVibration = notificationChannel.Vibrates,
                    LockScreenVisibility = (LockScreenVisibility) notificationChannel.Privacy,
                    VibrationPattern = notificationChannel.VibrationPattern
                };

                AndroidNotificationCenter.RegisterNotificationChannel(androidChannel);
            }
#elif UNITY_IOS
			Platform = new IosNotificationsPlatform();
#endif
	        if (notifications != null)
	        {
		        _localNotifications = notifications;
	        }
	        
	        Platform.NotificationReceived += OnNotificationReceived;
	        
	        
	        // Check serializer
	        if (Serializer == null)
	        {
		        Serializer = new DefaultSerializer(Path.Combine(Application.persistentDataPath, DefaultFilename));
	        }
			
	        OnForegrounding();
        }

        public virtual void Update()
        {
            if ((_mode & OperatingMode.Queue) != OperatingMode.Queue)
            {
                return;
            }

            // If we have pending notifications
            if (PendingNotifications != null && PendingNotifications.Count > 0)
            {
                // Check each pending notification for expiry, then remove it
                for (int i = PendingNotifications.Count - 1; i >= 0; --i)
                {
                    PendingNotification queuedNotification = PendingNotifications[i];
                    DateTime? time = queuedNotification.Notification.DeliveryTime;
                    if (time != null && time < DateTime.Now)
                    {
                        PendingNotifications.RemoveAt(i);
                        LocalNotificationExpired?.Invoke(queuedNotification);
                    }
                }
            }
        }
        
        public void OnDestroy()
        {
            Platform.NotificationReceived -= OnNotificationReceived;

            if (Platform == null)
            {
                return;
            }

            if (Platform is IDisposable disposable)
            {
                disposable.Dispose();
            }
        }

        IGameNotificationsPlatform ILocalNotificationService.Platform()
        {
	        return _platform;
        }

        public void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus)
            {
                // Clears the notifications from the status bar
#if UNITY_ANDROID
                AndroidNotificationCenter.CancelAllDisplayedNotifications();
#endif
	            OnForegrounding();
	            
                return;
            }
            
           

			OnBackground();
			// Backgrounding
			// Queue future dated notifications
			if ((_mode & OperatingMode.Queue) == OperatingMode.Queue)
			{
				if (PendingNotifications != null && PendingNotifications.Count > 0)
				{
					// Filter out past events
					for (var i = PendingNotifications.Count - 1; i >= 0; i--)
					{
						PendingNotification pendingNotification = PendingNotifications[i];
						// Ignore already scheduled ones
						if (pendingNotification.Notification.Scheduled)
						{
							continue;
						}

						// If a non-scheduled notification is in the past (or not within our threshold)
						// just remove it immediately
						if (pendingNotification.Notification.DeliveryTime != null &&
						    pendingNotification.Notification.DeliveryTime - DateTime.Now < LocalNotificationsManager.MinimumNotificationTime)
						{
							PendingNotifications.RemoveAt(i);
						}
					}
					
					// Sort notifications by delivery time, if no notifications have a badge number set
					bool noBadgeNumbersSet = PendingNotifications.All(notification => notification.Notification.BadgeNumber == null);

					if (noBadgeNumbersSet && LocalNotificationsManager.Instance.AutoBadging)
					{
						PendingNotifications.Sort((a, b) =>
						{
							if (!a.Notification.DeliveryTime.HasValue)
							{
								return 1;
							}

							if (!b.Notification.DeliveryTime.HasValue)
							{
								return -1;
							}

							return a.Notification.DeliveryTime.Value.CompareTo(b.Notification.DeliveryTime.Value);
						});

						// Set badge numbers incrementally
						var badgeNum = 1;
						foreach (PendingNotification pendingNotification in PendingNotifications)
						{
							if (pendingNotification.Notification.DeliveryTime.HasValue &&
							    !pendingNotification.Notification.Scheduled)
							{
								pendingNotification.Notification.BadgeNumber = badgeNum++;
							}
						}
					}

					for (int i = PendingNotifications.Count - 1; i >= 0; i--)
					{
						PendingNotification pendingNotification = PendingNotifications[i];
						// Ignore already scheduled ones
						if (pendingNotification.Notification.Scheduled)
						{
							continue;
						}
						// Schedule it now
						Platform?.ScheduleNotification(pendingNotification.Notification);
					}

					// Clear badge numbers again (for saving)
					if (noBadgeNumbersSet && LocalNotificationsManager.Instance.AutoBadging)
					{
						foreach (PendingNotification pendingNotification in PendingNotifications)
						{
							if (pendingNotification.Notification.DeliveryTime.HasValue)
							{
								pendingNotification.Notification.BadgeNumber = null;
							}
						}
					}
				}
			}

			if (PendingNotifications != null)
			{
				// Calculate notifications to save
				var notificationsToSave = new List<PendingNotification>(PendingNotifications.Count);
				foreach (PendingNotification pendingNotification in PendingNotifications)
				{
					// If we're in clear mode, add nothing unless we're in rescheduling mode
					// Otherwise add everything
					if ((_mode & OperatingMode.ClearOnForegrounding) == OperatingMode.ClearOnForegrounding)
					{
						if ((_mode & OperatingMode.RescheduleAfterClearing) != OperatingMode.RescheduleAfterClearing)
						{
							continue;
						}
						
						// In reschedule mode, add ones that have been scheduled, are marked for
						// rescheduling, and that have a time
						if (pendingNotification.Reschedule &&
						    pendingNotification.Notification.Scheduled &&
						    pendingNotification.Notification.DeliveryTime.HasValue)
						{
							notificationsToSave.Add(pendingNotification);
						}
					}
					else
					{
						// In non-clear mode, just add all scheduled notifications
						if (pendingNotification.Notification.Scheduled)
						{
							notificationsToSave.Add(pendingNotification);
						}
					}
				}
				// Save to disk
				Serializer.Serialize(notificationsToSave);
			}
        }

        public void OnForeground()
        {
            Platform?.OnForeground();
            inForeground = true;
        }

        public void OnBackground()
        {
            Platform.OnBackground();
            inForeground = false;
        }

        public void OnForegrounding()
        {
	        PendingNotifications?.Clear();

	        OnForeground();

	        // Deserialize saved items
	        IList<IGameNotification> loaded = Serializer?.Deserialize(Platform);

	        // Foregrounding
	        if ((_mode & OperatingMode.ClearOnForegrounding) == OperatingMode.ClearOnForegrounding)
	        {
		        
		        // Clear on foregrounding
		        CancelAllNotifications();

		        // Only reschedule in reschedule mode, and if we loaded any items
		        if (loaded == null ||
		            (_mode & OperatingMode.RescheduleAfterClearing) != OperatingMode.RescheduleAfterClearing)
		        {
			        return;
		        }

		        // Reschedule notifications from deserialization
		        foreach (IGameNotification savedNotification in loaded)
		        {
			        if (savedNotification.DeliveryTime > DateTime.Now && !string.IsNullOrEmpty(savedNotification.LNId))
			        {
				        PendingNotification pendingNotification = ScheduleNotification(savedNotification);
				        pendingNotification.Reschedule = true;
			        }
		        }
	        }
	        else
	        {
		        // Just create PendingNotification wrappers for all deserialized items. 
		        // We're not rescheduling them because they were not cleared
		        if (loaded == null)
		        {
			        return;
		        }

		        foreach (IGameNotification savedNotification in loaded)
		        {
			        if (savedNotification.DeliveryTime > DateTime.Now)
			        {
				        PendingNotifications?.Add(new PendingNotification(savedNotification));
			        }
		        }
	        }
        }

        public PendingNotification ScheduleNotification(IGameNotification notification)
        {
	        if (notification.DeliveryTime != null)
	        {
		        TimeSpan aux = notification.DeliveryTime.Value - DateTime.Now;
		        LogHelper.LogSdk(string.Format("Scheduling Local notif {0} in {1}s ", notification.LNId, aux.TotalSeconds), LogHelper.LOCAL_NOTIFICATIONS_TAG);
	        }
	        // If we queue, don't schedule immediately.
	        // Also immediately schedule non-time based deliveries (for iOS)
	        if ((_mode & OperatingMode.Queue) != OperatingMode.Queue ||
	            notification.DeliveryTime == null)
	        {
		        Platform?.ScheduleNotification(notification);
	        }
	        else if (notification.Id.HasValue)
	        {
		        // If the notification already has an ID, search for it on the pendingNotifications
		        int elementIndex = PendingNotifications.FindIndex(item => item.Notification.Id == notification.Id);
				
		        if (elementIndex > -1 && elementIndex < PendingNotifications.Count && PendingNotifications[elementIndex] != null)
			        PendingNotifications.RemoveAt(elementIndex); // Remove the previous Notification, in order to update it with a new one.
	        }
	        else
	        {
		        // Generate an ID for items that don't have one (just so they can be identified later)
		        int id = Math.Abs(DateTime.Now.ToString("yyMMddHHmmssffffff", CultureInfo.InvariantCulture).GetHashCode());
		        notification.Id = id;
	        }

	        // Register pending notification
	        var result = new PendingNotification(notification);
	        
	        PendingNotifications.Add(result);
	        return result;
        }

        public void SendNotification(IGameNotification notification, bool reschedule = false)
        {
	        PendingNotification notificationToDisplay = ScheduleNotification(notification);
	        notificationToDisplay.Reschedule = reschedule;
        }

        public IGameNotification SendNotification(string notificationId, string title, string body, DateTime deliveryTime, int? badgeNumber = null,
	        bool reschedule = false, string channelId = null, string smallIcon = null, string largeIcon = null, string bigPicture = null)
        {
	        IGameNotification notification = CreateNotification();

	        if (notification == null)
	        {
		        return null;
	        }

	        notification.LNId = notificationId;
	        notification.Title = title;
	        notification.Body = body;
	        notification.Group = !string.IsNullOrEmpty(channelId) ? channelId : LocalNotificationsManager.Instance.DefaultChannelId;
	        notification.DeliveryTime = deliveryTime;
	        notification.SmallIcon = smallIcon;
	        notification.LargeIcon = largeIcon;
	        notification.BigPicture = bigPicture;
	        notification.ShouldAutoCancel = true;

			if (badgeNumber != null)
	        {
		        notification.BadgeNumber = badgeNumber;
	        }

	        SendNotification(notification, reschedule);
			
	        return notification;
        }

        public void SendNotification(string notificationId, double seconds = 0)
        {
	        LocalNotificationModel notification = _localNotifications?.FirstOrDefault(notif => notif.Id == notificationId);

	        if (notification != null)
	        {
		        // int.max in seconds is equal to 24,855 days. No need to schedule a notifaction that far out!
		        if (seconds + notification.Offset > int.MaxValue)
			        return;
		        
		        CancelNotification(notificationId);

		        SendNotification(notificationId,
			        Localization.Localization.Get(string.Format("LocalNotification_Title_{0}", notification.Id)),
			        Localization.Localization.Get(string.Format("LocalNotification_Description_{0}", notification.Id)),
			        DateTime.Now.ToLocalTime() + TimeSpan.FromSeconds(seconds) +
			        TimeSpan.FromSeconds(notification.Offset),
			        null,
			        true,
			        null,
			        notification.SmallIcon,
			        notification.LargeIcon,
			        notification.BigPicturePath
		        );
	        }
        }

        public IGameNotification CreateNotification()
        {
	        return Platform?.CreateNotification();
        }

        public void CancelNotification(string notificationId)
        {
	        LogHelper.LogSdk(string.Format("Cancelling previous Local notif {0}", notificationId), LogHelper.LOCAL_NOTIFICATIONS_TAG);
	        
	        PendingNotification pendingNotification =
		        PendingNotifications.FirstOrDefault(x => x.Notification.LNId == notificationId);

	        if (pendingNotification != null && pendingNotification.Notification.Id.HasValue)
	        {
		        Platform?.CancelNotification(pendingNotification.Notification.Id.Value);

		        // Remove the cancelled notification from scheduled list
		        int index = PendingNotifications.FindIndex(scheduledNotification =>
			        scheduledNotification.Notification.Id == pendingNotification.Notification.Id);

		        if (index >= 0)
		        {
			        PendingNotifications.RemoveAt(index);
		        }
	        }

	        
        }

        public void CancelAllNotifications()
        {
	        LogHelper.LogSdk("Cancelling all notifications", LogHelper.LOCAL_NOTIFICATIONS_TAG);
            Platform?.CancelAllScheduledNotifications();
#if UNITY_ANDROID
            AndroidNotificationCenter.CancelAllNotifications();
#endif
	        PendingNotifications?.Clear();
        }

        public void DismissNotification(int notificationId)
        {
            Platform?.DismissNotification(notificationId);
        }

        public void DismissAllNotifications()
        {
            Platform?.DismissAllDisplayedNotifications();
        }

        public void SetInitialLocalNotifications()
        {
	        if (_localNotifications == null) 
		        return;
			
	        List<LocalNotificationModel> notifs =
		        _localNotifications.Where(n => n.SendType == LocalNotificationConstants.SEND_ON_START || 
		                                       n.SendType == LocalNotificationConstants.SEND_ON_START_AND_SAVE).ToList();
			
	        foreach (var notif in notifs)
	        {
		        SendNotification(notif.Id);
	        }
        }

        public void SetOnSaveLocalNotifications()
        {
	        if (_localNotifications == null) 
		        return;

	        List<LocalNotificationModel> notifs =
		        _localNotifications.Where(n => n.SendType == LocalNotificationConstants.SEND_ON_SAVE || 
		                                       n.SendType == LocalNotificationConstants.SEND_ON_START_AND_SAVE).ToList();
	        foreach (var notif in notifs)
	        {
		        SendNotification(notif.Id);
	        }
        }

        private void OnNotificationReceived(IGameNotification deliveredNotification)
        {
            // Ignore for background messages (this happens on Android sometimes)
            if (!inForeground)
            {
                return;
            }

            // Find in pending list
            int deliveredIndex = PendingNotifications.FindIndex(scheduledNotification =>
                scheduledNotification.Notification.Id == deliveredNotification.Id);
            if (deliveredIndex >= 0)
            {
                LocalNotificationDelivered?.Invoke(PendingNotifications[deliveredIndex]);

                PendingNotifications.RemoveAt(deliveredIndex);
            }
        }
    }
}
