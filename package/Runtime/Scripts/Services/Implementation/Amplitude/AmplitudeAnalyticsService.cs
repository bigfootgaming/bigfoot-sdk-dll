#if AMPLITUDE
using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk.Analytics
{
    public class AmplitudeAnalyticsService : IAnalyticsService
    {

        /// <summary>
        /// amplitude instance
        /// </summary>
        protected Amplitude _amplitudeInstance;

        /// <summary>
        /// configuration
        /// </summary>
        protected AnalyticsConfiguration _analyticsConfiguration;

        /// <inheritdoc/>
        public virtual void InitializeAnalytics(Action initializationFinished)
        {
            _amplitudeInstance = Amplitude.getInstance();
            _analyticsConfiguration = SdkManager.Instance.Configuration.AnalyticsConfiguration;
            _amplitudeInstance.logging = _analyticsConfiguration.DebugAnalyticsEvents;
            _amplitudeInstance.trackSessionEvents(true);
            _amplitudeInstance.init(_analyticsConfiguration.AmplitudeConfiguration.APIKey);
            
            initializationFinished?.Invoke();
        }

        public void Reset()
        {
            _amplitudeInstance = null;
        }

        /// <inheritdoc/>
        public virtual void SetUserId(string userId)
        {
            _amplitudeInstance.setUserId(userId);
        }

        /// <inheritdoc/>
        public void TrackSessionStartEvent()
        {
        }

        /// <inheritdoc/>
        public void TrackSessionEndEvent()
        {
        }
        /// <inheritdoc/>
        public void TrackErrorEvent()
        {
        }

        /// <inheritdoc/>
        public virtual void TrackTransactionEvent(TransactionEventModel data)
        {
            Dictionary<string, object> eventData = new Dictionary<string, object>();
            string eventName = "";
            
            // Currencies transactions 
            if (data.TransactionType == "CurrencyTransaction")
            {
                if (data.Sources.Count > 0)
                {
                    eventName = string.Format("Earn_{0}", data.Sources[0].Key);
                    
                    eventData.Add("Value", data.Sources[0].Amount);
                    eventData.Add("Origin",data.Origin);
                }
                else if (data.Sinks.Count > 0)
                {
                    eventName = string.Format("Spend_{0}", data.Sinks[0].Key);

                    eventData.Add("Value", data.Sinks[0].Amount);
                    eventData.Add("Origin",data.Origin);
                }
                
                // If this event needs to be discriminated for LTEs
                if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName) && GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                {
                    eventName += "_LTE";
                }
                
                TrackCustomEvent(eventName, eventData);
            }
            // Items transactions
            else if (data.TransactionType == "ItemTransaction")
            {
                foreach (var source in data.Sources)
                {
                    if (source.Type == "Currency")
                    {
                        eventName = string.Format("Earn_{0}", source.Key);
                        eventData.Clear();
                        eventData.Add("Value", source.Amount);
                        eventData.Add("Origin",data.Origin);
                    }
                    else
                    {
                        eventName = string.Format("Earn_{0}", source.Type);
                        eventData.Clear();
                        eventData.Add("ItemName", source.Key);
                        eventData.Add("Value", source.Amount);
                        eventData.Add("Origin",data.Origin);
                    }
                    
                    // If this event needs to be discriminated for LTEs
                    if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName) && GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                    {
                        eventName += "_LTE";
                    }
                    
                    TrackCustomEvent(eventName, eventData);
                }

                foreach (var sink in data.Sinks)
                {
                    if (sink.Type == "Currency")
                    {
                        eventName = string.Format("Spend_{0}", sink.Key);
                        eventData.Clear();
                        eventData.Add("Value", sink.Amount);
                        eventData.Add("Origin",data.Origin);
                    }
                    else if (sink.Type == "Item")
                    {
                        eventName = string.Format("Spend_{0}", sink.Type);
                        eventData.Clear();
                        eventData.Add("ItemName",sink.Key);
                        eventData.Add("Value", sink.Amount);
                        eventData.Add("Origin",data.Origin);
                    }
                    
                    // If this event needs to be discriminated for LTEs
                    if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName) && GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                    {
                        eventName += "_LTE";
                    }
                    TrackCustomEvent(eventName, eventData);
                }
            }
        }

        /// <inheritdoc/>
        public virtual void TrackCustomEvent(string eventName, Dictionary<string, object> data)
        {
            _amplitudeInstance.logEvent(eventName, data);
            if(_analyticsConfiguration.DebugAnalyticsEvents)
                LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", eventName, JsonHelper.SerializeObject(data)));
        }

        /// <inheritdoc/>
        public virtual void TrackCustomProperty(string propertyName, string propertyValue)
        {
            _amplitudeInstance.setUserProperty(propertyName, propertyValue);
        }

        /// <inheritdoc/>
        public virtual void TrackIapPurchase(BaseItemModel itemModel, RealMoneyCost cost, string origin = "")
        {
            var data = new Dictionary<string, object>
            {
                {"ItemName", itemModel.ItemName},
                {"origin", origin},
                {"price", cost.RealMoneyValue}
            };
            string sku = "";
#if UNITY_ANDROID
            sku = cost.GoogleSKU;
            data.Add("sku", cost.GoogleSKU);
#elif UNITY_IOS
            sku = cost.IosSKU;
            data.Add("sku", cost.IosSKU);
#endif
            
            string eventName = "IapPurchase";
            // If in the EventsToIgnore, ignore sending this event
            if (_analyticsConfiguration.EventsToIgnore != null && _analyticsConfiguration.EventsToIgnore.Contains(eventName))
            {
                EventIgnored(eventName, data);
                return;
            }
            if (_analyticsConfiguration.EventsToRename != null && _analyticsConfiguration.EventsToRename.ContainsKey(eventName))
            {
                eventName = _analyticsConfiguration.EventsToRename[eventName];
            }
            if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName))
            {
                if(GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                    eventName += "_LTE";
            }
            //tracking as custom event
            TrackCustomEvent(eventName, data);

            //tracking as revenue event
            double doubleRealMoneyCost = ((double)cost.RealMoneyValue) / 100d;
            _amplitudeInstance.logRevenue(sku, 1, doubleRealMoneyCost);
        }
        
        public void DispatchEvents()
        {
        }

        /// <inheritdoc/>
        public virtual void EventIgnored(string eventName, Dictionary<string, object> data)
        {
            
        }

        
    }
}
#endif


