﻿#if BACKEND_AWS || BACKEND_FIREBASE
using System.Linq;
using System.Collections.Generic;
using BigfootSdk.Helpers;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// AWS Progress service.
	/// </summary>
	public class AWSProgressService : IProgressService
	{
		/// <summary>
		/// Called when the Title is initialized
		/// </summary>
		public void TitleInitialized ()
		{
			// Grab a reference to the TitleModel
			TitleModel title = TitleManager.Instance.TitleData;

			// Make sure we have worlds to parse
			List<WorldModel> worlds = new List<WorldModel> ();
            // If worlds data exists
			if (title.Data.ContainsKey ("worlds")) {
				// Parse the worlds
				worlds = JsonHelper.DeserializeObjectList<WorldModel> (title.Data ["worlds"]);
			}
            // If not, create them empty, as containers
            else
            {
                worlds = new List<WorldModel>();
            }

			// Make sure we have levels to parse
			if (title.Data.ContainsKey ("world_levels")) {

				// Parse the levels
				List<LevelModel> levels = JsonHelper.DeserializeObjectList<LevelModel> (title.Data ["world_levels"]);

				WorldModel currentWorld = null;
				foreach (LevelModel level in levels) {
					// Probably they will all come in order, so we add this improvement
					if (currentWorld == null || currentWorld.WorldId != level.WorldId) {
						currentWorld = worlds.Where (world => world.WorldId == level.WorldId).FirstOrDefault ();
					}
                    if(currentWorld == null)
                    {
                        currentWorld = new WorldModel(){WorldId = level.WorldId};
                        worlds.Add(currentWorld);
                    }

					// Add them to their corresponding world
					currentWorld.Levels.Add (level);
					
				}
			}

			// Add them to the ProgressManager
			ProgressManager.Instance.SetWorlds (worlds);

			// Throw event that the worlds are initialized
			ProgressEvents.WorldsInitialized ();
		}

		/// <summary>
		/// Called when the Title is initialized
		/// </summary>
		public void PlayerInitialized ()
		{
			// Make sure we have progress to parse
			List<WorldProgressModel> worlds = JsonHelper.DeserializeObjectList<WorldProgressModel> (PlayerManager.Instance.GetFromPlayerData("progress", GameModeManager.CurrentGameMode));

			// Add them to the ProgressManager
			ProgressManager.Instance.SetWorldsProgress (worlds);

			// Throw event that the world's progress are initialized
			ProgressEvents.WorldsProgressInitialized ();
		}
	}
}
#endif

