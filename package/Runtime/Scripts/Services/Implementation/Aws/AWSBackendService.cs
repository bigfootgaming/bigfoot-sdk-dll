﻿#if BACKEND_AWS

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using BigfootSdk.Helpers;

namespace BigfootSdk.Backend
{
	
	/// <summary>
	/// AWS Backend service.
	/// </summary>
    public class AWSBackendService : IBackendService
	{
        /// <summary>
        /// The backend manager.
        /// </summary>
        BackendManager _backendManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.Backend.AWSBackendService"/> class.
        /// </summary>
        /// <param name="backendManager">Backend manager.</param>
        public AWSBackendService(BackendManager backendManager)
        {
            _backendManager = backendManager;
        }

        /// <summary>
        /// Is the backend available.
        /// </summary>
        /// <returns><c>true</c>, if the backend is available, <c>false</c> otherwise.</returns>
        public bool IsBackendAvailable()
        {
            return true;
        }

        /// <summary>
        /// Listens when the backend is available.
        /// </summary>
        /// <param name="callback">Callback.</param>
        public void ListenBackendAvailable(Action callback = null)
        {
            callback?.Invoke();
        }

        /// <summary>
        /// Executes the cloud code.
        /// </summary>
        /// <param name="method">Method.</param>
        /// <param name="data">Data.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        public void ExecuteCloudCode(string method, Dictionary<string, string> data, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
        {
			SdkConfiguration sdkConfiguration = SdkManager.Instance.Configuration;
			if(!string.IsNullOrEmpty(sdkConfiguration.Environment))
            {
                if (data != null)
                    data.Add("environment", sdkConfiguration.Environment);
                else
                    data = new Dictionary<string, string>() { { "environment", sdkConfiguration.Environment } };
            }


            _backendManager.ExecuteWebRequest(method, data, successCallback, failureCallback);
        }

        /// <summary>
        /// Executes the cloud code. The data values have no need to be serialized.
        /// </summary>
        /// <param name="method">Method.</param>
        /// <param name="data">Data.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        public void ExecuteCloudCode(string method, Dictionary<string, object> data, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
        {
			SdkConfiguration sdkConfiguration = SdkManager.Instance.Configuration;
			if(!string.IsNullOrEmpty(sdkConfiguration.Environment))
	        {
		        if (data != null)
			        data.Add("environment", sdkConfiguration.Environment);
		        else
			        data = new Dictionary<string, object>() { { "environment", sdkConfiguration.Environment } };
	        }

	        string jsonData = "{}";
	        if (data != null)
		        jsonData = JsonHelper.SerializeObject(data);
            _backendManager.ExecuteWebRequest(method, jsonData, successCallback, failureCallback);
        }
    }
}
#endif

