﻿
using System.Collections.Generic;
#if BACKEND_AWS
using System;
using System.Text.RegularExpressions;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// AWS Title service.
	/// </summary>
	public class AWSTitleService : ITitleService
	{
		/// <summary>
		/// Initializes the Title from the backend
		/// </summary>		
		/// <param name="callback">Callback.</param>
		public void InitializeTitle (Action<bool> callback = null)
		{
            BackendManager.Instance.ExecuteCloudCode("GetTitleData", new Dictionary<string, object>(), successResult =>
            {
                // Grab a reference to the TitleManager
                TitleManager manager = TitleManager.Instance;

                // Iterate through the title data, and add them to the model
                foreach (var item in successResult)
                {
                    manager.TitleData.Data.Add(item.Key, Regex.Replace(item.Value.ToString(), @"\t|\n|\r", "").Trim('"'));
                }

                // Throw event that the title is initialized
                TitleEvents.TitleInitialized ();

                callback?.Invoke(true);
			}, () => {
	            // get title data locally
	            GetTitleDataLocally();
	            
                callback?.Invoke(true);
            });
		}

		/// <summary>
        /// Gets the default TitleData from resources.
        /// </summary>
		void GetTitleDataLocally()
		{
			TitleManager manager = TitleManager.Instance;
			TextAsset textAsset = Resources.Load<TextAsset>("TitleData");
			Dictionary<string, object> titleData = JsonHelper.DesarializeObject<Dictionary<string, object>>(textAsset.text);
			foreach (var item in titleData)
			{
				manager.TitleData.Data.Add(item.Key, Regex.Replace(item.Value.ToString(), @"\t|\n|\r", "").Trim('"'));
			}
		}
	}
}
#endif

