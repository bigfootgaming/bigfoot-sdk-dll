﻿#if BACKEND_AWS
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using BigfootSdk.Helpers;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// AWS Player service.
    /// </summary>
    public class AWSPlayerService : IPlayerService
    {
        /// <summary>
        /// long representation of date time last save.
        /// </summary>
        private long _lastSave;

        /// <summary>
        /// The current environment.
        /// </summary>
        private string _environment;

        /// <summary>
        /// AWS endpoint.
        /// </summary>
        private string _urlEndPoint;

        /// <summary>
        /// Application name.
        /// </summary>
        private string _appName;

        /// <summary>
        /// Initializes the player from the backend, filling in the PlayerModel in the PlayerManager
        /// </summary>
        /// <param name="callback">Callback.</param>
        public void InitializePlayer(Action<bool> callback = null)
        {
            // Grab a reference to the player model
            PlayerModel player = PlayerManager.Instance.Player;
            _lastSave = long.MinValue;
            _environment = SdkManager.Instance.Config.environment;
            _urlEndPoint = SdkManager.Instance.Config.endpoint_url;
            _appName = SdkManager.Instance.Config.app_name;
            BackendManager.Instance.ExecuteCloudCode("GetPlayerData", new Dictionary<string, object>(), successResult =>
            {
                if (successResult.ContainsKey("LastSave"))
                {
                    _lastSave = long.Parse(successResult["LastSave"].ToString());
                }
        
                if (player.LastSave < _lastSave || player.LastSave > TimeHelper.GetTimeInServer())
                {
                    player.LastSave = _lastSave;
                    
                    // Parse the Currencies
                    if (successResult.ContainsKey("Currencies"))
                    {
                        var currencies =
                            JsonHelper.DesarializeObject<Dictionary<string, int>>(successResult["Currencies"].ToString());
                        foreach (var item in currencies)
                        {
                            player.Currencies[item.Key] = item.Value;
                        }
                    }

                    // Parse the items
                    if (successResult.ContainsKey("PlayerItems"))
                    {
                        player.Items =
                            JsonHelper.DeserializeObjectList<PlayerItemModel>(successResult["PlayerItems"].ToString());
                    }

                    // Parse the Player Data
                    foreach (KeyValuePair<string, object> kvp in successResult)
                    {
                        if (kvp.Key != "LastSave" && kvp.Key != "Currencies" && kvp.Key != "PlayerItems" && kvp.Key != "PlayerId")
                        {
                            player.Data[kvp.Key] = kvp.Value.ToString();
                        }
                    }
                }

                // Throw event that the player is initialized
                PlayerEvents.PlayerInitialized();

                callback?.Invoke(true);

            }, () => {
                //playing offline
                PlayerEvents.PlayerInitialized();
                callback?.Invoke(true);
            });
        }

        /// <summary>
        /// Sets the player data.
        /// </summary>
        /// <param name="data">Data.</param>
        public void SetPlayerData(Dictionary<string, string> data, Action<bool> callback = null)
        {
            Dictionary<string, string> dataToSend = new Dictionary<string, string>
            {
                {"data", JsonHelper.SerializeObject(data)},
                {"environment", _environment}
            };
            
            string jsonData = "{}";
            if (data != null)
                jsonData = JsonHelper.SerializeObject(dataToSend);
            ExecuteWebRequest("SetPlayerData", jsonData,(result) => { callback?.Invoke(true); },
                () => { callback?.Invoke(false); });
        }

        /// <summary>
        /// Sets the player data.
        /// </summary>
        /// <param name="data">Data.</param>
        public void SetPlayerData(PlayerModel model, Action<bool> callback = null)
        {
            BackendPlayerModel backendPlayerModel = model.GetBackendPlayerModel();
            Dictionary<string, string> playerData = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> k in backendPlayerModel.D)
                playerData.Add(k.Key, k.Value);
            playerData.Add("LastSave", backendPlayerModel.L.ToString());
            playerData.Add("Currencies", JsonHelper.SerializeObject(backendPlayerModel.C));
            playerData.Add("PlayerItems", JsonHelper.SerializeObject(backendPlayerModel.I));
            
            Dictionary<string, string> dataToSend = new Dictionary<string, string>
            {
                {"data", JsonHelper.SerializeObject(playerData, false)},
                {"environment", _environment}
            };
           
            string jsonData = JsonHelper.SerializeObject(dataToSend);
            
            ExecuteWebRequest("SetPlayerData", jsonData,(result) => { callback?.Invoke(true); },
                () => { callback?.Invoke(false); });
        }

        public void ExecuteWebRequest(string method, string data = null,
            Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
        {
            string url = string.Format("{0}/{1}", _urlEndPoint, method);
            UnityWebRequest request = UnityWebRequest.Put(url, data);
            
            request.method = UnityWebRequest.kHttpVerbPOST;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("app_name", _appName);
            request.certificateHandler = new BigfootCertificateModel();

            UnityWebRequestAsyncOperation asyncOperation = request.SendWebRequest();
            asyncOperation.completed += (ap) =>
            {
                if (!request.isNetworkError)
                {
                    string responseString = request.downloadHandler.text;
                    var response = JsonHelper.DesarializeObject<AWSLambdaResponse>(responseString);
                    if (response.statusCode == 201)
                        successCallback?.Invoke(response.data);
                    else
                    {
                        failureCallback?.Invoke();
                    }
                }
                else
                {
                    failureCallback?.Invoke();
                }
            };
        }
        
        /// <summary>
        /// Consumes the items.
        /// </summary>
        /// <param name="items">Items.</param>
        /// <param name="callback">Callback.</param>
        public void ConsumeItems(ItemCost items, Action<bool> callback = null)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                DefaultValueHandling = DefaultValueHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple,
                TypeNameHandling = TypeNameHandling.All,
                MetadataPropertyHandling = MetadataPropertyHandling.ReadAhead
            };

            Dictionary<string, string> dataToSend = new Dictionary<string, string>
            {
                {"items", JsonHelper.SerializeObject(items, settings)}
            };
            BackendManager.Instance.ExecuteCloudCode("ConsumeItems", dataToSend, (obj) => callback?.Invoke(true),
                () => callback?.Invoke(false));
        }

        /// <summary>
        /// Updates the items custom data.
        /// </summary>
        /// <param name="items">Items.</param>
        /// <param name="callback">Callback.</param>
        public void UpdateItemsCustomData(List<PlayerItemModel> items, Action<bool> callback = null)
        {
            Dictionary<string, string> dataToSend = new Dictionary<string, string>
            {
                {"items", JsonHelper.SerializeObject(items)}
            };

            BackendManager.Instance.ExecuteCloudCode("UpdateItemsCustomData", dataToSend,
                (obj) => callback?.Invoke(true), () => callback?.Invoke(false));
        }

        /// <summary>
        /// Delete the player data.
        /// </summary>
        /// <param name="callback">Callback.</param>
        public void DeletePlayerData(Action callback = null)
        {
            BackendManager.Instance.ExecuteCloudCode("DeleteUser", new Dictionary<string, object>(),
                (obj) => { callback?.Invoke(); });
        }
    }
}
#endif
