﻿#if BACKEND_AWS
using System;
using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// GameSparks Authentication service.
    /// </summary>
    public class AWSAuthenticationService : IAuthenticationService
    {

        /// <summary>
        /// Registers on GameSparks with the device identifier.
        /// </summary>
        public void RegisterWithDeviceId(Action<bool> callback = null)
        {
            string deviceId = "";
            if (string.IsNullOrEmpty(deviceId))
            {
#if !UNITY_WEBGL
                Dictionary<string, string> dict = new Dictionary<string, string>
                {
                    { "username", SystemInfo.deviceUniqueIdentifier },
                    { "password", SystemInfo.deviceUniqueIdentifier }
                };
                PlayerManager.Instance.Player.PlayerId = SystemInfo.deviceUniqueIdentifier;
#else
                string userId = "";

                if(!KongregateManager.Instance.GetIsGuest())
                {
                    userId = KongregateManager.Instance.GetUserID();
                    KongregateManager.Instance.SubmitStatistic("initialized", 1);
                }
                else
                {
                    // For now, don't let guests play
                    KongregateManager.Instance.ShowRegistrationDialog();
                    return;
                }

                Dictionary<string, string> dict = new Dictionary<string, string>
                {
                    { "username", userId },
                    { "password", userId }
                };
                PlayerManager.Instance.Player.PlayerId = userId;
#endif
                BackendManager.Instance.ExecuteCloudCode("Authenticate", dict, (result) => {
                    if(result != null && result.ContainsKey("isNewPlayer"))
                    {
                        var parseResult = bool.TryParse(result["isNewPlayer"].ToString(), out bool isNewPlayer);
                        if (isNewPlayer)
                        {
                            LogHelper.LogAnalytics("Install event sent!");
                            
                            AnalyticsManager.Instance.TrackCustomEvent("Install", null);
                            
                            var variantJson = PlayerManager.Instance.GetFromPlayerData("Variant");
                            if (!string.IsNullOrEmpty(variantJson))
                            {
                                var variant = JsonHelper.DesarializeObject<VariantModel>(variantJson);
                                if (variant != null)
                                {
                                    var data = new Dictionary<string, object>()
                                    {
                                        {"variantId", variant.VariantId}
                                    };
                                    AnalyticsManager.Instance.TrackCustomEvent("PlayerBucketed", data);
                                }
                            }
                        }
                    }
                        PlayerRegistered(callback); 
                    }, () => { 
                        //offline mode
                        LogHelper.LogSdk("Authentication failed... Playing offline");
                        PlayerRegistered(callback);
                });
            }
            else
            {
                PlayerRegistered(callback);
            }
        }

        void PlayerRegistered(Action<bool> callback)
        {
            LogHelper.LogSdk("Registration Ok");

            AuthenticationEvents.PlayerRegistered(SystemInfo.deviceUniqueIdentifier);

            callback?.Invoke(true);
        }

#if GOOGLE_PLAY_GAMES
        /// <summary>
        /// Registers on the backend with the play games account.
        /// </summary>
        /// <param name="callback">Callback.</param>
        public void RegisterWithPlayGames(Action<bool> callback = null)
        {
            
        }
#endif
        /// <summary>
        /// Registers on the backend with the game center account.
        /// </summary>
        /// <param name="callback">Callback.</param>
        public void RegisterWithGameCenter(Action<bool> callback = null)
        {
        }
	}
}
#endif
