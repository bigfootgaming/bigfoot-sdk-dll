﻿using System.Collections.Generic;
namespace BigfootSdk.Backend
{
    [System.Serializable]
    public class AWSLambdaResponse
    {
        public int statusCode;

        public Dictionary<string, object> data;

        public string errorMessage;
    }
}

