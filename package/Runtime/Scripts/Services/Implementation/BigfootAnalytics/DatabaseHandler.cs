﻿#if BIGFOOT_ANALYTICS
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using BigfootSdk.Helpers;
using Mono.Data.Sqlite;


namespace BigfootSdk.Analytics
{

    /// <summary>
    /// Takes Care of Creating and maintaining the Analytics Database
    /// </summary>
    public class DatabaseHandler
    {

        #region TABLES KEYS & NAMES

        /// <summary>
        /// The session identifier key.
        /// </summary>
        private const string KEY_SESSION_ID = "SessionId";

        /// <summary>
        /// The user identifier key.
        /// </summary>
        private const string KEY_USER_ID = "UserId";
        
        /// <summary>
        /// The events table name.
        /// </summary>
        private const string EVENTS_TABLE_NAME = "Events";

        /// <summary>
        /// The event data key.
        /// </summary>
        private const string KEY_EVENT_DATA = "EventData";

        /// <summary>
        /// The event identifier key.
        /// </summary>
        private const string KEY_EVENT_ID = "EventId";

        /// <summary>
        /// The event name key.
        /// </summary>
        private const string KEY_EVENT_NAME = "EventName";

        /// <summary>
        /// The event server date time key.
        /// </summary>
        private const string KEY_TIME_STAMP = "EventDateTime";

        /// <summary>
        /// The event local date time key.
        /// </summary>
        private const string KEY_LOCAL_TIME_STAMP = "EventLocalDateTime";

        /// <summary>
        /// The game version key.
        /// </summary>
        private const string KEY_GAME_VERSION = "GameVersion";
        
        /// <summary>
        /// The country code (AR, US, ES)
        /// </summary>
        private const string KEY_COUNTRY_CODE = "CountryCode";
        
        /// <summary>
        /// The operating system
        /// </summary>
        private const string KEY_OPERATING_SYSTEM = "OperatingSystem";
        
        /// <summary>
        /// If we are on an a/b test, which variant we are on
        /// </summary>
        private const string KEY_VARIANT_NAME = "VariantName";

        #endregion

        /// <summary>
        /// The name of the database.
        /// </summary>
        private const string _databaseName = "bigfoot.db";

        /// <summary>
        /// The db connection string.
        /// </summary>
        private string db_connection_string;

        /// <summary>
        /// The db connection.
        /// </summary>
        private IDbConnection db_connection;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.Analytics.DatabaseHandler"/> class.
        /// </summary>
        public DatabaseHandler()
        {
            // Create or Open Database
            db_connection_string = "URI=file:" + Application.persistentDataPath + "/" + _databaseName;


            db_connection = new SqliteConnection(db_connection_string);
            db_connection.Open();

            CreateTable();
        }

        #region Events
        
        /// <summary>
        /// Delete Events already sent to server
        /// </summary>
        /// <param name="events"></param>
        public void DeleteSentEvents(DatabaseEventModel[] events)
        {
            for (int i = 0; i < events.Length; i++)
            {  
                DeleteFromTable(EVENTS_TABLE_NAME, KEY_EVENT_ID, events[i].EventId);
            }
        }

        /// <summary>
        /// Returns Events pending from the database
        /// </summary>
        /// <returns></returns>
        public DatabaseEventModel[] GetNewEvents()
        {
            System.Data.IDataReader reader = GetDataFromTable(EVENTS_TABLE_NAME);
            List<DatabaseEventModel> events = new List<DatabaseEventModel>(); 

            while (reader.Read())
            {
                events.Add(new DatabaseEventModel(
                    reader[KEY_EVENT_ID].ToString(),
                    reader[KEY_SESSION_ID].ToString(),
                    reader[KEY_USER_ID].ToString(),
                    reader[KEY_TIME_STAMP].ToString(),
                    reader[KEY_EVENT_NAME].ToString(),
                    reader[KEY_EVENT_DATA].ToString(),
                    reader[KEY_LOCAL_TIME_STAMP].ToString(),
                    reader[KEY_VARIANT_NAME].ToString()
                    ));
            }
            
            return events.ToArray();
        }
                
        /// <summary>
        /// Creates a new Event on the Database
        /// </summary>
        /// <param name="aEvent"></param>
        public void AddEventData(DatabaseEventModel aEvent)
        {
            IDbCommand dbcmd = GetDbCommand();

            dbcmd.CommandText =
                "INSERT INTO " + EVENTS_TABLE_NAME
                               + " ( "
                               + KEY_EVENT_ID + ","
                               + KEY_SESSION_ID + ","
                               + KEY_USER_ID + ","
                               + KEY_TIME_STAMP + ","
                               + KEY_EVENT_NAME + ","
                               + KEY_EVENT_DATA + ","
                               + KEY_LOCAL_TIME_STAMP + ","
                               + KEY_GAME_VERSION + ","
                               + KEY_COUNTRY_CODE + ","
                               + KEY_OPERATING_SYSTEM + ","
                               + KEY_VARIANT_NAME + " )"

                               + "VALUES ( '"
                               + aEvent.EventId + "', '"
                               + aEvent.SessionId + "', '"
                               + aEvent.UserId + "', '"
                               + aEvent.EventDateTime + "', '"
                               + aEvent.EventName + "', '"
                               + aEvent.EventData + "', '"
                               + aEvent.EventLocalDateTime + "', '"
                               + aEvent.GameVersion + "', '"
                               + aEvent.CountryCode + "', '"
                               + aEvent.OperatingSystem + "', '"
                               + aEvent.VariantId + "' )";

            try
            {
                dbcmd.ExecuteNonQuery();
                dbcmd.Dispose();
            }
            catch (SqliteException)
            {
                // Drop the table
                DropTable();
                
                // Create a new one
                CreateTable();
            }
            
        }

        void DropTable()
        {
            // Create Events Table into the database
            IDbCommand dbcmd = GetDbCommand();

            dbcmd.CommandText = "DROP TABLE IF EXISTS " + EVENTS_TABLE_NAME;

            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();       
        }

        void CreateTable()
        {
            // Create Events Table into the database
            IDbCommand dbcmd = GetDbCommand();
            
            dbcmd.CommandText = "CREATE TABLE IF NOT EXISTS " + EVENTS_TABLE_NAME + " ( " +
                                KEY_EVENT_ID + " TEXT, " +
                                KEY_SESSION_ID + " TEXT, " +
                                KEY_USER_ID + " TEXT, " +
                                KEY_TIME_STAMP + " TEXT, " +
                                KEY_EVENT_NAME + " TEXT, " +
                                KEY_EVENT_DATA + " TEXT, " +
                                KEY_LOCAL_TIME_STAMP + " TEXT, " +
                                KEY_GAME_VERSION + " TEXT, " +
                                KEY_COUNTRY_CODE + " TEXT, " +
                                KEY_OPERATING_SYSTEM + " TEXT, " +
                                KEY_VARIANT_NAME + " TEXT )";
            
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();        
        }
        
        #endregion


        #region DatabaseHandlingMethods

        /// <summary>
        /// Returns a new Command for Database
        /// </summary>
        /// <returns> Returns Command context</returns>
        protected IDbCommand GetDbCommand()
        {
            return db_connection.CreateCommand();
        }

        /// <summary>
        /// Returns <typeparam name="IDataReader"> DataReader</typeparam> with all the data from <paramref name="tableName"/>
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns><typeparam name="IDataReader"> DataReader</typeparam></returns>
        protected IDataReader GetDataFromTable(string tableName)
        {
            IDbCommand dbcmd = db_connection.CreateCommand();
            
            dbcmd.CommandText =
                "SELECT * FROM " + tableName;
            IDataReader reader = dbcmd.ExecuteReader();
            
            dbcmd.Dispose();
            
            return reader;
        }

        /// <summary>
        /// Returns data for specified parameters
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="keyId"></param>
        /// <param name="keyValue"></param>
        /// <returns>IDataReader</returns>
        protected IDataReader GetDataFor(string tableName, string keyId, string keyValue)
        {
            IDbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText = "SELECT FROM " + tableName + " WHERE " + keyId + " = '" + keyValue + "'";

            IDataReader reader = dbcmd.ExecuteReader();
            return reader;
        }
        
        /// <summary>
        /// Deletes Data for specified parameters
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="keyId"></param>
        /// <param name="keyValue"></param>
        protected void DeleteFromTable(string tableName, string keyId, string keyValue)
        {
            IDbCommand dbcmd = GetDbCommand();
            dbcmd.CommandText =
                "DELETE FROM " + tableName + " WHERE " + keyId + " = '" + keyValue + "'";
            dbcmd.ExecuteNonQuery();
            dbcmd.Dispose();
        }

        #endregion
    }
}
#endif