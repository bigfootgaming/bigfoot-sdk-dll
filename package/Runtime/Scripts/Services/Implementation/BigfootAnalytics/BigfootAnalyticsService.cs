﻿#if BIGFOOT_ANALYTICS
using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
#if BACKEND_FIREBASE
using Firebase.Functions;
using Firebase.Extensions;
#endif
using UnityEngine;

namespace BigfootSdk.Analytics
{
    /// <summary>
    /// Bigfoot analytics service.
    /// </summary>
    public class BigfootAnalyticsService : IAnalyticsService
    {
        /// <summary>
        /// The analytics configuration
        /// </summary>
        private AnalyticsConfiguration _analyticsConfiguration;
        
        /// <summary>
        /// The database handler.
        /// </summary>
        private DatabaseHandler _dbHandler;

        /// <summary>
        /// The user identifier.
        /// </summary>
        private string _userId;

        /// <summary>
        /// The session identifier.
        /// </summary>
        private string _sessionId;

        /// <summary>
        /// The name of the kinesis delivery stream.
        /// </summary>
        private string _kinesisDeliveryStreamName;

        /// <summary>
        /// If true the service is initialized
        /// </summary>
        private bool _initialized = false;

        /// <summary>
        /// If <see langword="true"/> the service is available to be used.
        /// </summary>
        private bool _readyToSend = false;

        /// <summary>
        /// Flag to know if we should safe check
        /// </summary>
        private bool _shouldSafeCheck = true;

        /// <summary>
        /// The events to send.
        /// </summary>
        List<DatabaseEventModel> _eventsToSend = new List<DatabaseEventModel>();

        /// <summary>
        /// If we are on an a/b test, the variant id
        /// </summary>
        private string _variantId;
        
        /// <summary>
        /// Everything is loaded, we are now ready to send events
        /// </summary>
        void DependencyManager_OnEverythingLoaded()
        {
            DependencyManager.OnEverythingLoaded -= DependencyManager_OnEverythingLoaded;
            
            // Check if we are in an ab test
            CheckIfInVariant();
            
            _readyToSend = true;
        }
        
        /// <summary>
        /// Sets the current user id.
        /// </summary>
        /// <param name="userId"></param>
        public void SetUserId(string userId)
        {
            // Cache the user Id
            _userId = PlayerManager.Instance.GetPlayerId();
        }

        /// <summary>
        /// Initializes the analytics.
        /// </summary>
        /// <param name="initializationFinished">Initialization finished callback.</param>
        public virtual void InitializeAnalytics(Action initializationFinished)
        {
            // Cache the analytics configuration
            _analyticsConfiguration = SdkManager.Instance.Configuration.AnalyticsConfiguration;
            
            DependencyManager.OnEverythingLoaded += DependencyManager_OnEverythingLoaded;

            _kinesisDeliveryStreamName = _analyticsConfiguration.BigfootAnalyticsConfiguration.KinesisDeliveryStreamName;
           
#if BACKEND_AWS
            Dictionary<string, string> checkAnalyticsData = new Dictionary<string, string>
            {
                { "DeliveryStreamName", _kinesisDeliveryStreamName }
            };

            Dictionary<string, object> request = new Dictionary<string, object>
            {
                { "request", checkAnalyticsData }
            };
            // Format into Json
            string requestJson = JsonHelper.SerializeObject(request, false);
            BackendManager.Instance.ExecuteWebRequest("CheckAnalytics", requestJson, (result) =>
            {
                _initialized = true;

            #if !UNITY_WEBGL
                _dbHandler = new DatabaseHandler();
            #endif

                _sessionId = AnalyticsManager.Instance.SessionId;

                LogHelper.LogSdk("Bigfoot analytics service initialized");

                TrackSessionStartEvent();
                initializationFinished?.Invoke();
            }, () =>
            {
                initializationFinished?.Invoke();
            });
#elif BACKEND_FIREBASE
            Dictionary<string, object> checkAnalyticsData = new Dictionary<string, object>
            {
                { "DeliveryStreamName", _kinesisDeliveryStreamName }
            };
            
            var function = FirebaseFunctions.DefaultInstance.GetHttpsCallable("checkAnalytics");
            function.CallAsync(checkAnalyticsData).ContinueWithOnMainThread(task =>
            {
                if (task.IsCompleted)
                {
                    #if !UNITY_WEBGL
                        _dbHandler = new DatabaseHandler();
                    #endif

                    _initialized = true;
                    _sessionId = AnalyticsManager.Instance.SessionId;
                    LogHelper.LogSdk("Bigfoot analytics service initialized");

                    TrackSessionStartEvent();
                }
                initializationFinished?.Invoke();
            });
#endif
        }

        /// <summary>
        /// Send saved events to server
        /// </summary>
        public void DispatchEvents()
        {
            if (_initialized && _readyToSend)
            {
                // Request the database for pending events
                DatabaseEventModel[] eventsToSend;

                #if !UNITY_WEBGL
                    eventsToSend = _dbHandler.GetNewEvents();
#else
                    eventsToSend = _eventsToSend.ToArray();
#endif

                // Safe check the events qeued up prior to the first sent
                if(_shouldSafeCheck)
                {
                    _shouldSafeCheck = false;
                    SafeCheckEvents(eventsToSend);
                }

                // If database is empty, Return
                if (eventsToSend.Length == 0)
                    return;

                SerializedEventModel[] events = new SerializedEventModel[eventsToSend.Length];
                for (int i = 0; i < events.Length; i++)
                {
                    events[i] = new SerializedEventModel()
                    {
                        Data = SerializeDataBaseEventModel(eventsToSend[i])
                    };
                }
                
#if BACKEND_AWS
                KinesisDataModel model = new KinesisDataModel()
                {
                    DeliveryStreamName = _kinesisDeliveryStreamName,
                    Records = events
                };

                Dictionary<string, object> request = new Dictionary<string, object>();
                request.Add("request", model);
    
                // Format events into Json
                string eventsJson = JsonHelper.SerializeObject(request, false);
                // Execute WebRequest to send Events
                BackendManager.Instance.ExecuteWebRequest("Analytics", eventsJson,
                    objects =>
                    {
                        #if !UNITY_WEBGL
                            // If SUCCESS, the delete sent events from Database
                            _dbHandler.DeleteSentEvents(eventsToSend);
                        #else
                            foreach (var item in eventsToSend)
                            {
                                _eventsToSend.Remove(item);
                            }
                        #endif
                    },
                    () => { }
                );
#elif BACKEND_FIREBASE
                Dictionary<string, object> data = new Dictionary<string, object>();
                data.Add("DeliveryStreamName", _kinesisDeliveryStreamName);
                data.Add("Records", JsonHelper.SerializeObject(events));

                var function = FirebaseFunctions.DefaultInstance.GetHttpsCallable("sendAnalyticsToFirehose");
                function.CallAsync(data).ContinueWithOnMainThread(task =>
                {
                    if (task.IsCompleted)
                    {
                        #if !UNITY_WEBGL
                            // If SUCCESS, the delete sent events from Database
                            _dbHandler.DeleteSentEvents(eventsToSend);
                        #else
                            foreach (var item in eventsToSend)
                            {
                                _eventsToSend.Remove(item);
                            }
                        #endif
                    }
                });
#endif
            }
        }

        /// <summary>
        /// Checks if any event has an invalid time (sent before analytics initialized) or invalid userId
        /// </summary>
        /// <param name="eventsToSend">Events to send.</param>
        void SafeCheckEvents(DatabaseEventModel[] eventsToSend)
        {
            foreach(DatabaseEventModel eventToSend in eventsToSend)
            {
                if(eventToSend.EventDateTime.Equals("0001-01-01 00:00:00"))
                {
                    DateTime dateTime = TimeHelper.GetTimeInServerDateTime();
                    eventToSend.EventDateTime = dateTime.ToString("yyyy-MM-dd HH:mm:ss");
                    eventToSend.EventLocalDateTime = TimeHelper.ToLocal(dateTime).ToString("yyyy-MM-dd HH:mm:ss");
                }

                if(string.IsNullOrEmpty(eventToSend.UserId))
                {
                    eventToSend.UserId = _userId;
                }

                if (string.IsNullOrEmpty(eventToSend.VariantId) && !string.IsNullOrEmpty(_variantId))
                {
                    eventToSend.VariantId = _variantId;
                }
            }
        }

        /// <summary>
        /// Serializes the data base event model.
        /// </summary>
        /// <returns>The data base event model.</returns>
        /// <param name="eventModel">Event model.</param>
        string SerializeDataBaseEventModel(DatabaseEventModel eventModel)
        {
            Dictionary<string, object> eventData = JsonHelper.DesarializeObject<Dictionary<string, object>>(eventModel.EventData);
            Dictionary<string, object> toSerialize = new Dictionary<string, object>
            {
                { "EventId", eventModel.EventId },
                { "SessionId", eventModel.SessionId },
                { "UserId", eventModel.UserId },
                { "EventDateTime", eventModel.EventDateTime },
                { "EventName", eventModel.EventName },
                { "EventData", eventData },
                { "EventLocalDateTime", eventModel.EventLocalDateTime },
                { "GameVersion", eventModel.GameVersion},
                { "CountryCode", eventModel.CountryCode },
                { "OperatingSystem", eventModel.OperatingSystem },
                { "VariantId", eventModel.VariantId}
            };
            return JsonHelper.SerializeObject(toSerialize, false);
        }


        /// <summary>
        /// Tracks a transaction event.
        /// </summary>
        /// <param name="data">Transaction data.</param>
        public virtual void TrackTransactionEvent(TransactionEventModel data)
        {
            if (_initialized)
            {
                string eventName = "Transaction";
                
                // If this event needs to be discriminated for LTEs
                if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName))
                {
                    if(GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                        eventName += "_LTE";
                }
                
                string jsonData = data == null? "" : JsonHelper.SerializeObject(data, false);
                DatabaseEventModel newEvent = new DatabaseEventModel(_sessionId, _userId, eventName, jsonData, _variantId);
                AddEvent(newEvent);

                LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", eventName, jsonData));
            }
                
        }

        /// <summary>
        /// Tracks a custom event.
        /// </summary>
        /// <param name="eventName">Event name.</param>
        /// <param name="data">Data.</param>
        public virtual void TrackCustomEvent(string eventName, Dictionary<string, object> data)
        {
            if (_initialized)
            {
                string jsonData = data == null ? "" : JsonHelper.SerializeObject(data, false);
                DatabaseEventModel newEvent = new DatabaseEventModel(_sessionId, _userId, eventName, jsonData, _variantId);
                AddEvent(newEvent);

                LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", eventName, jsonData));
            }

        }

        /// <summary>
        /// Tracks session start event
        /// </summary>
        public virtual void TrackSessionStartEvent()
        {
            if (_initialized)
            {
                Dictionary<string, string> eventData = new Dictionary<string, string>();
                eventData.Add("BigfootSdkVersion", SdkManager.Instance.Configuration.SdkVersion);
                eventData.Add("Platform", Application.platform.ToString());
                eventData.Add("OperatingSystem", SystemInfo.operatingSystem);
                eventData.Add("DeviceModel", SystemInfo.deviceModel);

                string jsonData = JsonHelper.SerializeObject(eventData, false);

                DatabaseEventModel eventSessionStart = new DatabaseEventModel(_sessionId, _userId, "SessionStart", jsonData, _variantId);
                AddEvent(eventSessionStart);

                LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", "SessionStart", jsonData));
            }
                
        }

        /// <summary>
        /// Tracks session end event
        /// </summary>
        public virtual void TrackSessionEndEvent()
        {
            if (_initialized)
            {
                Dictionary<string, string> eventData = new Dictionary<string, string>();
                DatabaseEventModel eventSessionEnd = new DatabaseEventModel(_sessionId, _userId, "SessionEnd", JsonHelper.SerializeObject(eventData, false), _variantId);
                AddEvent(eventSessionEnd);

                LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", "SessionEnd", ""));
            }
        }

        /// <summary>
        /// Adds the event.
        /// </summary>
        /// <param name="newEvent">New event.</param>
        void AddEvent(DatabaseEventModel newEvent)
        {
            #if !UNITY_WEBGL
                _dbHandler.AddEventData(newEvent);
            #else
                _eventsToSend.Add(newEvent);
            #endif
        }

        /// <summary>
        /// Tracks error event
        /// </summary>
        public virtual void TrackErrorEvent()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Tracks a custom property.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        /// <param name="propertyValue">Value.</param>
        public virtual void TrackCustomProperty(string propertyName, string propertyValue)
        {
            // TODO
        }

        /// <summary>
        /// Tracks an IAP Purchase
        /// </summary>
        /// <param name="iapSku">The SKU of the iap</param>
        /// <param name="currency">The 3 letter currency code</param>
        /// <param name="price">The price of this iap</param>
        public virtual void TrackIapPurchase(string currency, float price, Dictionary<string, object> data)
        {
            string eventName = "IapPurchase";
            
            // If in the EventsToIgnore, ignore sending this event
            if (_analyticsConfiguration.EventsToIgnore != null && _analyticsConfiguration.EventsToIgnore.Contains(eventName))
            {
                EventIgnored(eventName, data);
                return;
            }
            if (_analyticsConfiguration.EventsToRename != null && _analyticsConfiguration.EventsToRename.ContainsKey(eventName))
            {
                eventName = _analyticsConfiguration.EventsToRename[eventName];
            }
            if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName))
            {
                if(GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                    eventName += "_LTE";
            }
            
            TrackCustomEvent(eventName, data);
        }


        void CheckIfInVariant()
        {
            var variantJson = PlayerManager.Instance.GetFromPlayerData("Variant", GameModeConstants.MAIN_GAME);
            if (!string.IsNullOrEmpty(variantJson))
            {
                var variant = JsonHelper.DesarializeObject<VariantModel>(variantJson);
                if (variant != null)
                {
                    _variantId = variant.VariantId;
                    LogHelper.LogSdk(string.Format("Player is in variant: {0}", _variantId));
                }
            }
        }
        
        /// <summary>
        /// An event was ignored
        /// </summary>
        /// <param name="eventName">Event name.</param>
        /// <param name="data">Data.</param>
        public virtual void EventIgnored(string eventName, Dictionary<string, object> data)
        {
            // Not needed
        }

    }
}
#endif