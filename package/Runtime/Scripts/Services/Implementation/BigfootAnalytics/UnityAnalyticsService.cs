﻿#if UNITY_ANALYTICS
using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk.Analytics
{
    /// <summary>
    /// Unity analytics service.
    /// </summary>
    public class UnityAnalyticsService : IAnalyticsService
    {
        /// <summary>
        /// The analytics configuration
        /// </summary>
        private AnalyticsConfiguration _analyticsConfiguration;
        
        /// <inheritdoc/>
        public void InitializeAnalytics(Action initializationFinished)
        {
            // Cache the analytics configuration
            _analyticsConfiguration = SdkManager.Instance.Configuration.AnalyticsConfiguration;
            
            initializationFinished?.Invoke();
        }

        public void Reset()
        {
        }
        
        /// <inheritdoc/>
        public void SetUserId(string userId)
        {
            
        }

        /// <inheritdoc/>
        public void DispatchEvents()
        {
            // Not Needed
        }

        /// <inheritdoc/>
        public void TrackTransactionEvent(TransactionEventModel data)
        {
            string eventName = "Transaction";
                
            // If this event needs to be discriminated for LTEs
            if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName))
            {
                if(GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                    eventName += "_LTE";
            }
            
           var dictData = new Dictionary<string, object>()
            {
                {"TransactionType", data.TransactionType},
                {"Sinks", JsonHelper.SerializeObject(data.Sinks, false)},
                {"Sources", JsonHelper.SerializeObject(data.Sources, false)}
            };

            UnityEngine.Analytics.Analytics.CustomEvent(eventName, dictData);
            
            if(_analyticsConfiguration.DebugAnalyticsEvents)
                LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", eventName, JsonHelper.SerializeObject(dictData)));
        }

        /// <inheritdoc/>
        public void TrackCustomEvent(string eventName, Dictionary<string, object> data)
        {
            UnityEngine.Analytics.Analytics.CustomEvent(eventName, data);
            
            if(_analyticsConfiguration.DebugAnalyticsEvents)
                LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", eventName, JsonHelper.SerializeObject(data)));
        }

        /// <inheritdoc/>
        public void TrackCustomProperty(string propertyName, string propertyValue)
        {
            //not implemented in this service.
        }

        /// <inheritdoc/>
        public void TrackSessionStartEvent()
        {
            // Not Needed
        }

        /// <inheritdoc/>
        public void TrackSessionEndEvent()
        {
            // Not Needed
        }

        /// <inheritdoc/>
        public void TrackErrorEvent()
        {
            // Not Needed
        }
        
        /// <inheritdoc/>
        public void TrackIapPurchase(BaseItemModel itemModel, RealMoneyCost cost, string origin = "")
        {
            // Not Needed
        }
        
        /// <inheritdoc/>
        public void EventIgnored(string eventName, Dictionary<string, object> data)
        {
            // Not needed
        }

    }
}
#endif