﻿#if FIREBASE_ANALYTICS
using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using Firebase.Analytics;

namespace BigfootSdk.Analytics
{
    /// <summary>
    /// Firebase Analytics service.
    /// </summary>
    public class FirebaseAnalyticsService : IAnalyticsService
    {
        /// <summary>
        /// The analytics configuration
        /// </summary>
        protected AnalyticsConfiguration _analyticsConfiguration;
        
        /// <inheritdoc/>
        public virtual void InitializeAnalytics(Action initializationFinished)
        {
            LogHelper.LogSdk("firebase analytics started", LogHelper.DEPENDENCY_MANAGER_TAG);
            // Cache the analytics configuration
            _analyticsConfiguration = SdkManager.Instance.Configuration.AnalyticsConfiguration;
            
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
            LogHelper.LogSdk("firebase analytics finished", LogHelper.DEPENDENCY_MANAGER_TAG);
            initializationFinished();
        }

        public void Reset()
        {
        }

        /// <inheritdoc/>
        public void SetUserId(string userId)
        {
            FirebaseAnalytics.SetUserId(userId);
        }

        /// <inheritdoc/>
        public virtual void TrackSessionStartEvent()
        {
             //tracked automatically
             //https://support.google.com/firebase/answer/6317485?authuser=0
        }

        /// <inheritdoc/>
        public virtual void TrackSessionEndEvent()
        {
            //not needed   
        }

        /// <inheritdoc/>
        public void TrackErrorEvent()
        {
            //not needed
        }

        /// <inheritdoc/>
        public virtual void TrackTransactionEvent(TransactionEventModel data)
        {
            List<Parameter> parameters = new List<Parameter>();
            string eventName = "";
            
            // Currencies transactions 
            if (data.TransactionType == "CurrencyTransaction")
            {
                if (data.Sources.Count > 0)
                {
                    eventName = string.Format("Earn_{0}", data.Sources[0].Key);
                    
                    parameters.Add(new Parameter(FirebaseAnalytics.ParameterValue, data.Sources[0].Amount));
                    parameters.Add(new Parameter("Origin",data.Origin));
                }
                else if (data.Sinks.Count > 0)
                {
                    eventName = string.Format("Spend_{0}", data.Sinks[0].Key);
                    
                    parameters.Add(new Parameter(FirebaseAnalytics.ParameterValue, data.Sinks[0].Amount));
                    parameters.Add(new Parameter("Origin",data.Origin));
                }
                
                // If this event needs to be discriminated for LTEs
                if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName) && GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                {
                    eventName += "_LTE";
                }
                
                FirebaseAnalytics.LogEvent(eventName, parameters.ToArray());
                
                if(_analyticsConfiguration.DebugAnalyticsEvents)
                    LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", eventName, JsonHelper.SerializeObject(data)));
            }
            // Items transactions
            else if (data.TransactionType == "ItemTransaction")
            {
                foreach (var source in data.Sources)
                {
                    if (source.Type == "Currency")
                    {
                        eventName = string.Format("Earn_{0}", source.Key);
                        
                        parameters.Add(new Parameter(FirebaseAnalytics.ParameterValue, source.Amount));
                        parameters.Add(new Parameter("Origin",data.Origin));
                    }
                    else
                    {
                        eventName = string.Format("Earn_{0}", source.Type);

                        parameters.Add(new Parameter("ItemName", source.Key));
                        parameters.Add(new Parameter("Value", source.Amount));
                        parameters.Add(new Parameter("Origin",data.Origin));
                    }
                    
                    // If this event needs to be discriminated for LTEs
                    if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName) && GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                    {
                        eventName += "_LTE";
                    }
                    
                    FirebaseAnalytics.LogEvent(eventName, parameters.ToArray());
                    
                    if(_analyticsConfiguration.DebugAnalyticsEvents)
                        LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", eventName, JsonHelper.SerializeObject(data)));
                }

                foreach (var sink in data.Sinks)
                {
                    if (sink.Type == "Currency")
                    {
                        eventName = string.Format("Spend_{0}", sink.Key);
                        
                        parameters.Add(new Parameter(FirebaseAnalytics.ParameterValue, sink.Amount));
                        parameters.Add(new Parameter("Origin",data.Origin));
                    }
                    else if (sink.Type == "Item")
                    {
                        eventName = string.Format("Spend_{0}", sink.Type);
                        
                        parameters.Add(new Parameter("ItemName",sink.Key));
                        parameters.Add(new Parameter("Value", sink.Amount));
                        parameters.Add(new Parameter("Origin",data.Origin));
                    }
                    
                    // If this event needs to be discriminated for LTEs
                    if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName) && GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                    {
                        eventName += "_LTE";
                    }
                    
                    FirebaseAnalytics.LogEvent(eventName, parameters.ToArray());
                    
                    if(_analyticsConfiguration.DebugAnalyticsEvents)
                        LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", eventName, JsonHelper.SerializeObject(data)));
                }
            }
        }

        /// <inheritdoc/>
        public virtual void TrackCustomEvent(string eventName, Dictionary<string, object> data)
        {
            List<Parameter> parameters = new List<Parameter>();

            if (data != null)
            {
                foreach (KeyValuePair<string, object> p in data)
                {
                    if (p.Value is double d)
                        parameters.Add(new Parameter(p.Key, d));
                    else if (p.Value is long l)
                        parameters.Add(new Parameter(p.Key, l));
                    else if (p.Value is string s)
                        parameters.Add(new Parameter(p.Key, s));
                    else
                        parameters.Add(new Parameter(p.Key, JsonHelper.SerializeObject(p.Value, false)));
                }
            }

            FirebaseAnalytics.LogEvent(eventName, parameters.ToArray());
            
            if(_analyticsConfiguration.DebugAnalyticsEvents)
                LogHelper.LogAnalytics(string.Format("Type: {0} Data: {1}", eventName, JsonHelper.SerializeObject(data)));
        }

        /// <inheritdoc/>
        public virtual void TrackCustomProperty(string propertyName, string propertyValue)
        {
            FirebaseAnalytics.SetUserProperty(propertyName, propertyValue);
            
            LogHelper.LogAnalytics(string.Format("Custom Property Type: {0} Data: {1}", propertyName, propertyValue));
        }
        
        /// <inheritdoc/>
        public virtual void TrackIapPurchase(BaseItemModel itemModel, RealMoneyCost cost, string origin = "")
        {
            var data = new Dictionary<string, object>
            {
                {"ItemName", itemModel.ItemName},
                {"origin", origin},
                {"price", cost.RealMoneyValue}
            };
            
            #if UNITY_ANDROID
            data.Add("sku", cost.GoogleSKU);
            #elif UNITY_IOS
            data.Add("sku", cost.IosSKU);
            #endif
            
            string eventName = "IapPurchase";
            // If in the EventsToIgnore, ignore sending this event
            if (_analyticsConfiguration.EventsToIgnore != null && _analyticsConfiguration.EventsToIgnore.Contains(eventName))
            {
                EventIgnored(eventName, data);
                return;
            }
            if (_analyticsConfiguration.EventsToRename != null && _analyticsConfiguration.EventsToRename.ContainsKey(eventName))
            {
                eventName = _analyticsConfiguration.EventsToRename[eventName];
            }
            if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName))
            {
                if(GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                    eventName += "_LTE";
            }
            TrackCustomEvent(eventName, data);
        }
        
        /// <summary>
        /// Dispatch events
        /// </summary>
        public void DispatchEvents()
        {
            // Not needed
        }

        /// <summary>
        /// An event was ignored
        /// </summary>
        /// <param name="eventName">Event name.</param>
        /// <param name="data">Data.</param>
        public virtual void EventIgnored(string eventName, Dictionary<string, object> data)
        {
            // Not needed
        }
    }
}


#endif