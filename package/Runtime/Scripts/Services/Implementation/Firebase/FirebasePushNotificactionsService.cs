﻿#if FIREBASE_PUSH_NOTIFICATIONS || UNITY_EDITOR
using BigfootSdk.Helpers;

namespace BigfootSdk.Notifications
{
    public class FirebasePushNotificactionsService : IPushNotificationsService
    {
        private bool _subscribed;
        
        public void Init()
        {
            if (!_subscribed)
            {
                _subscribed = true;
                Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
            }
        }

        public void Reset()
        {
            if (_subscribed)
            {
                _subscribed = false;
                Firebase.Messaging.FirebaseMessaging.TokenReceived -= OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived -= OnMessageReceived;
            }
        }

        public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
        {
            LogHelper.LogSdk("Received Registration Token: " + token.Token);
        }

        public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
        {
            LogHelper.LogSdk("Received a new message from: " + e.Message.From);
            if (e.Message.NotificationOpened)
                PushNotificationsManager.OnPushNotificationOpened?.Invoke(e);
        }
    }
}
#endif
