﻿#if BACKEND_FIREBASE
using System;
using System.Security.Authentication;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BigfootSdk.ErrorReport;
using BigfootSdk.Helpers;
using Firebase.Auth;
using UnityEngine;
using BigfootSdk.Extensions;

namespace  BigfootSdk.Backend
{
    /// <summary>
    /// Firabase Authentication service.
    /// </summary>
    public class FirebaseAuthenticationService : IAuthenticationService
    {
        protected const string REGISTERED_KEY = "firebase_login_email_set";
        protected const string EMAIL_KEY = "firebase_login_email";
        protected const string PASSWORD_KEY = "firebase_login_password";
        protected const string SOCIAL_USER = "firebase_login_social_user";
        protected const string SOCIAL_USER_ID = "firebase_login_social_user_id";

        /// <summary>
        /// Firebase Authentication Instance
        /// </summary>
        protected FirebaseAuth _auth;

        protected FirebaseUser _user;

        /// <summary>
        /// Register Anonymously.
        /// </summary>
        /// <param name="callback"></param>

        protected CancellationTokenSource _cancellationTokenSource;

        protected SocialManager _socialManager;

        protected FirebaseBackendConfiguration _configuration;

        protected Credential _credential;

        public void Initialize()
        {
            _socialManager = SocialManager.Instance;
            _auth = FirebaseAuth.DefaultInstance;
            _user = FirebaseAuth.DefaultInstance.CurrentUser;
            _configuration = SdkManager.Instance.Configuration.BackendConfiguration.FirebaseBackendConfiguration;
            _auth.StateChanged += StateChanged;
        }

        void StateChanged(object sender, EventArgs eventArgs)
        {
            if (_auth.CurrentUser != _user) {
                bool signedIn = _auth.CurrentUser != null && _auth.CurrentUser.IsValid();
                if (!signedIn && _user != null) {
                    AuthenticationManager.OnUserStateChanged?.Invoke(_user.UserId, false);
                    LogHelper.LogSdk("Signed out " + _user.UserId);
                }
                _user = _auth.CurrentUser;
                if (signedIn) {
                    AuthenticationManager.OnUserStateChanged?.Invoke(_user.UserId, true);
                    LogHelper.LogSdk("Signed in " + _user.UserId);
                }
            }
        }

        public virtual void RegisterDefault(Action<bool> callback)
        {
            FirebaseUser user = _auth.CurrentUser;
            bool isSocial = IsSocialUser();
            if (user != null)
            {
                LogCurrentUser();
                LogHelper.LogSdk("LOGIN :: EXISTING USER " + user.UserId + " " + user.Email +" isSocial "+ isSocial);
            }

            if (user != null && !isSocial && _configuration.UseEmailAsDefaultAuthMethod)
            {
                if (string.IsNullOrEmpty(user.Email))
                {
                    LinkWithEmail((success) => { FinishRegister(callback); });
                }
                else
                {
                    FinishRegister(callback);
                }
            }
            else 
            {
                if (_configuration.UseEmailAsDefaultAuthMethod)
                {
                    RegisterWithEmail(callback);
                }
                else if (user == null || isSocial)
                {
                    LogHelper.LogSdk("LOGIN :: user null or social");
                    RegisterWithDeviceId(callback);
                }
                else
                {
                    LogHelper.LogSdk("LOGIN :: Finish with same user ");
                    FinishRegister(callback);
                }
            }
        }


        public virtual void RegisterWithDeviceId(Action<bool> callback = null)
        {
            LogHelper.LogSdk("Logging in anonymously", LogHelper.FIREBASE_SERVICES_TAG);
            RefreshCTS(new CancellationTokenSource());
            SetTimeOut(callback);
            FirebaseEvents.AuthenticationServiceStarted();
            BackendManager.Instance.SetBackendBusy("AuthenticationAnon");
            _auth.SignInAnonymouslyAsync().ContinueWithOnMainThread(task =>
            {
                BackendManager.Instance.SetBackendNotBusy("AuthenticationAnon");
                if (task.IsCanceled)
                {
                    _cancellationTokenSource.Cancel();
                    FirebaseEvents.AuthenticationServiceFailed();
                    string text = "Anonymous login was canceled.";
                    ;
                    ErrorReportManager.Instance.Log(text);
                    LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                    if (!SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                        BackendManager.OnBackendConnectionFailed?.Invoke("Authentication", "canceled");
                }
                else if (task.IsFaulted)
                {
                    _cancellationTokenSource.Cancel();
                    FirebaseEvents.AuthenticationServiceFailed();
                    LogHelper.LogSdk("Anonymous login was faulted", LogHelper.FIREBASE_SERVICES_TAG);
                    foreach (var inner in task.Exception.InnerExceptions)
                    {
                        if (inner is AuthenticationException)
                        {
                            var e = (AuthenticationException)inner;
                            string text = string.Format("Firebase authentication error: {0}", e.Message);
                            ErrorReportManager.Instance.Log(text);
                            LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        }
                    }

                    if (!SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                        BackendManager.OnBackendConnectionFailed?.Invoke("Authentication", task.Exception.Message);
                }
                else if (task.IsCompleted)
                {
                    FirebaseEvents.AuthenticationServiceEnded();
                    SetSocialUser(0);
                    FinishRegister(callback);
                }
            });
        }

        protected virtual void SetTimeOut(Action<bool> callback)
        {
            int timeout = SdkManager.Instance.Configuration.BackendConfiguration.FirebaseBackendConfiguration
                .AuthTimeout;
            int fraction = SdkManager.Instance.Configuration.BackendConfiguration.FirebaseBackendConfiguration
                .AuthTimeoutWarningFraction;
            BackendManager.Instance.SetBackendTimeout("Authentication", timeout, _cancellationTokenSource, fraction,
                (init) => { callback?.Invoke(true); }, RefreshCTS);
        }

        protected virtual void RefreshCTS(CancellationTokenSource cts)
        {
            _cancellationTokenSource = cts;
        }



        /// <summary>
        /// Register successful
        /// </summary>
        /// <param name="cancellationTokenSource"></param>
        /// <param name="callback"></param>
        protected virtual void FinishRegister(Action<bool> callback = null)
        {
            Continue(callback);
            BackendManager.OnBackendConnectionWorking?.Invoke("Authentication");
            LogHelper.LogSdk("User signed in successfully", LogHelper.FIREBASE_SERVICES_TAG);
        }

        protected virtual void Continue(Action<bool> callback = null)
        {
            FirebaseUser firebaseUser = FirebaseAuth.DefaultInstance.CurrentUser;
            ulong creation = firebaseUser.Metadata.CreationTimestamp;
            ulong singIn = firebaseUser.Metadata.LastSignInTimestamp;
            string userId = firebaseUser.UserId;
            if (string.IsNullOrEmpty(userId))
                userId = "not set";
#if UNITY_EDITOR
            if (SystemInfo.deviceUniqueIdentifier != SystemInfo.unsupportedIdentifier &&
                AuthenticationManager.Instance.UseDeviceIdInEditor && firebaseUser.IsAnonymous)
                userId = SystemInfo.deviceUniqueIdentifier;
#endif
            LogHelper.LogSdk($"UserId: {firebaseUser.DisplayName} ({userId}), created: {creation} ,singIn: {singIn}",
                LogHelper.FIREBASE_SERVICES_TAG);
            _cancellationTokenSource?.Cancel();
            SetServerPlayerId(userId);
            callback?.Invoke(true);
        }

        private void SetServerPlayerId(string userId)
        {
            PlayerManager.Instance.SetServerPlayerId(userId);
        }

        public void SetServerPlayerId()
        {
            FirebaseUser firebaseUser = FirebaseAuth.DefaultInstance.CurrentUser;
            LogCurrentUser();
            SetServerPlayerId(firebaseUser.UserId);
        }

        void LogCurrentUser()
        {
            FirebaseUser user = _auth.CurrentUser;
            if (user != null)
            {
                LogHelper.LogSdk(
                $"FIREBASE USER: id: {user.UserId}, email: {user.Email}, providerId: {user.ProviderId}, " +
                    $"displayName {user.DisplayName}, anonymous {user.IsAnonymous}, valid {user.IsValid()}");
                foreach (var userInfo in user.ProviderData)
                {
                    LogHelper.LogSdk("provider data id "+ userInfo.ProviderId);
                }
            }
            else
            {
                LogHelper.LogSdk("FIREBASE USER: user is null");
            }
        }

        public void ClearCredential()
        {
            if (_credential != null)
            {
                _credential.Dispose();
                _credential = null;
            }
        }


#if GOOGLE_PLAY_GAMES
        /// <summary>
        /// Registers on the backend with the play games account.
        /// </summary>
        /// <param name="callback">Callback.</param>
        public virtual void RegisterWithPlayGames(Action<bool> callback = null)
        {
            LogHelper.LogSdk("Logging in with Play Games", LogHelper.FIREBASE_SERVICES_TAG);
            if (_credential == null || !_credential.IsValid() || _credential.Provider != "google.com")
            {
                _credential = PlayGamesAuthProvider.GetCredential(SocialManager.Instance.AuthCode);
            } 
            RegisterWithCredential(callback);
        }
        
        /// <summary>
        /// Registers on the backend with the play games account.
        /// </summary>
        /// <param name="callback">Callback.</param>
        public virtual void LinkWithPlayGames(Action<bool> callback = null)
        {
            LogHelper.LogSdk("Linking with Play Games", LogHelper.FIREBASE_SERVICES_TAG);
            if (_credential == null || !_credential.IsValid() || _credential.Provider != "google.com")
            {
                LogHelper.LogSdk("new credential", LogHelper.FIREBASE_SERVICES_TAG);
                _credential = PlayGamesAuthProvider.GetCredential(SocialManager.Instance.AuthCode);
            } 
            LinkWithCredential(callback);
        }
#endif

#if UNITY_IOS && APPLE_ID
        Credential GetAppleIdCredential()
        {
            LogHelper.LogSdk("new credential", LogHelper.FIREBASE_SERVICES_TAG);
            string idToken = Encoding.UTF8.GetString(
                _socialManager.IdentityToken,
                0,
                _socialManager.IdentityToken.Length);
            string authorizationCode = Encoding.UTF8.GetString(
                _socialManager.AuthorizationCode,
                0,
                _socialManager.AuthorizationCode.Length);
            return OAuthProvider.GetCredential("apple.com", idToken, _socialManager.Nonce, authorizationCode);
        }

        public void RegisterWithAppleId(Action<bool> callback = null)
        {
            LogHelper.LogSdk("Logging in with Apple Id", LogHelper.FIREBASE_SERVICES_TAG);
            if (_credential == null || !_credential.IsValid() || _credential.Provider != "apple.com")
            {
                _credential = GetAppleIdCredential();
            }
            RegisterWithCredential(callback);
        }
        
        public void RegisterAndRetrieveDataWithAppleId(Action<bool> callback = null)
        {
            LogHelper.LogSdk("Logging in and retrieving data with Apple Id", LogHelper.FIREBASE_SERVICES_TAG);
            if (_credential == null || !_credential.IsValid() || _credential.Provider != "apple.com")
            {
                _credential = GetAppleIdCredential();
            }
            RegisterAndRetrieveDataWithCredential(callback);
        }
        
        public void LinkWithAppleId(Action<bool> callback = null)
        {
            LogHelper.LogSdk("Linking with Apple Id", LogHelper.FIREBASE_SERVICES_TAG);
            if (_credential == null || !_credential.IsValid() || _credential.Provider != "apple.com")
            {
                _credential = GetAppleIdCredential();
            }
            LinkWithCredential(callback);
        }
#endif
        public void RegisterWithEmail(Action<bool> callback = null)
        {
            string email = GetEmail();
            string password = GetPassword();
            if (IsRegisteredWithEmail())
            {
                LogHelper.LogSdk($"Logging in with Email: {email} {password}", LogHelper.FIREBASE_SERVICES_TAG);
                if (_credential == null || !_credential.IsValid())
                {
                    _credential = EmailAuthProvider.GetCredential(email, password);
                }

                RegisterWithCredential(callback);
            }
            else
            {
                RefreshCTS(new CancellationTokenSource());
                SetTimeOut(callback);
                LogHelper.LogSdk($"Creating user with Email: {email} {password}", LogHelper.FIREBASE_SERVICES_TAG);
                BackendManager.Instance.SetBackendBusy("AuthenticationEmail");
                _auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(task =>
                {
                    BackendManager.Instance.SetBackendNotBusy("AuthenticationEmail");
                    if (task.IsCanceled) {
                        string text = "Create user with email was canceled.";
                        ErrorReportManager.Instance.Log(text);
                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        _cancellationTokenSource.Cancel();
                        RegisterWithDeviceId(callback);
                    }
                    if (task.IsFaulted) {
                        foreach (var e in task.Exception.InnerExceptions)
                        {
                            string text = string.Format("Create user with email encountered an error: {0}", e.Message);
                            ErrorReportManager.Instance.Log(text);
                            LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        }
                        _cancellationTokenSource.Cancel();
                        RegisterWithDeviceId(callback);
                    }

                    if (task.IsCompleted)
                    {
                        PlayerPrefs.SetInt(REGISTERED_KEY, 1);
                        SetSocialUser(0);
                        FinishRegister(callback);
                    }
                });
            }
        }

        bool IsRegisteredWithEmail()
        {
            int set = PlayerPrefs.GetInt(REGISTERED_KEY, 0);
            return set != 0;
        }

        string GetEmail()
        {
            string email = PlayerPrefs.GetString(EMAIL_KEY, "");
            if (string.IsNullOrEmpty(email))
            {
                email = $"{Guid.NewGuid().ToString()}@{SystemInfo.deviceUniqueIdentifier}.com";
                PlayerPrefs.SetString(EMAIL_KEY, email);
            }
            return email;
        }

        string GetPassword()
        {
            string password = PlayerPrefs.GetString(PASSWORD_KEY, "");
            if (string.IsNullOrEmpty(password))
            {
                password = Guid.NewGuid().ToString();
                PlayerPrefs.SetString(PASSWORD_KEY, password);
            }
            return password;
        }
        
        
        public void LinkWithEmail(Action<bool> callback = null)
        {
            string email = GetEmail();
            string password = GetPassword();
            LogHelper.LogSdk($"Linking with Email: {email} {password}", LogHelper.FIREBASE_SERVICES_TAG);
            _credential = EmailAuthProvider.GetCredential(email, password);
            LinkWithCredential(callback);
        }


        protected virtual void RegisterWithCredential( Action<bool> callback = null)
        {
            RefreshCTS(new CancellationTokenSource());
            SetTimeOut(callback);
            BackendManager.Instance.SetBackendBusy("AuthenticationCredential");
            _auth.SignInWithCredentialAsync(_credential).ContinueWithOnMainThread(task => {
                BackendManager.Instance.SetBackendNotBusy("AuthenticationCredential");
                if (task.IsCanceled) {
                    string text = "SignInWithCredentialAsync was canceled.";
                    ErrorReportManager.Instance.Log(text);
                    LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                    _cancellationTokenSource.Cancel();
                    if (_credential.Provider == "password" || !_configuration.UseEmailAsDefaultAuthMethod)
                        RegisterWithDeviceId(callback);
                    else
                        RegisterWithEmail(callback);
                }
                else if (task.IsFaulted)
                {
                    _credential = null;
                    foreach (var e in task.Exception.InnerExceptions)
                    {
                        string text = string.Format("SignInWithCredentialAsync encountered an error {1}: {0}",e.Message, e.GetType());
                        ErrorReportManager.Instance.Log(text);
                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                    }
                    _cancellationTokenSource.Cancel();
                    if (_credential.Provider == "password" || !_configuration.UseEmailAsDefaultAuthMethod)
                        RegisterWithDeviceId(callback);
                    else
                        RegisterWithEmail(callback);
                }
                else if (task.IsCompleted)
                {
                    SetSocialUser(_credential.Provider != "password" ? 1 : 0);
                    if (_credential.Provider != "password")
                        SetSocialUserId();
                    FinishRegister(callback);
                }
            });
            
        }


        public virtual void RegisterAndRetrieveDataWithCredential(Action<bool> callback = null)
        {
            RefreshCTS(new CancellationTokenSource());
            SetTimeOut(callback);
            BackendManager.Instance.SetBackendBusy("AuthenticationCredential");
            _auth.SignInAndRetrieveDataWithCredentialAsync(_credential).ContinueWithOnMainThread(task => {
                BackendManager.Instance.SetBackendNotBusy("AuthenticationCredential");
                if (task.IsCanceled) {
                    string text = "SignInAndRetrieveDataWithCredentialAsync was canceled.";
                    ErrorReportManager.Instance.Log(text);
                    LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                    _cancellationTokenSource.Cancel();
                    if (_credential.Provider == "password" || !_configuration.UseEmailAsDefaultAuthMethod)
                        RegisterWithDeviceId(callback);
                    else
                        RegisterWithEmail(callback);
                }
                else if (task.IsFaulted)
                {
                    _credential = null;
                    bool updatedCredential = false;
                    foreach (var e in task.Exception.Flatten().InnerExceptions)
                    {
                        string text = string.Format("SignInAndRetrieveDataWithCredentialAsync encountered an error {1}: {0}",e.Message, e.GetType());
                        ErrorReportManager.Instance.Log(text);
                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        if (e is FirebaseAccountLinkException fe)
                        {
                            LogHelper.LogSdk("cred "+ fe.UserInfo.UpdatedCredential.Provider +" "+fe.UserInfo.UpdatedCredential.IsValid());
                            if (fe.UserInfo.UpdatedCredential.IsValid())
                            {
                                _credential = fe.UserInfo.UpdatedCredential;
                                updatedCredential = true;
                                LogHelper.LogSdk("updated credentials");
                            }
                        }
                    }
                    _cancellationTokenSource.Cancel();
                    if (updatedCredential)
                        RegisterAndRetrieveDataWithCredential(callback);
                    else
                    {
                        if (_credential.Provider == "password" || !_configuration.UseEmailAsDefaultAuthMethod)
                            RegisterWithDeviceId(callback);
                        else
                            RegisterWithEmail(callback);
                    }
                }
                else if (task.IsCompleted)
                {
                    SetSocialUser(_credential.Provider != "password" ? 1 : 0);
                    if (_credential.Provider != "password")
                        SetSocialUserId();
                    FinishRegister(callback);
                }
            });
        }


        /// <summary>
        /// Links the current user with given crendentials.
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="callback"></param>
        protected virtual void LinkWithCredential(Action<bool> callback = null)
        {
            LogHelper.LogSdk($"Linking with credential {_credential.Provider}", LogHelper.FIREBASE_SERVICES_TAG);
            LogCurrentUser();
            if (_auth.CurrentUser != null)
            {
                _auth.CurrentUser.LinkWithCredentialAsync(_credential).ContinueWithOnMainThread(task =>
                {
                    if (task.IsCanceled)
                    {
                        string text = "LinkWithCredentialAsync was canceled.";
                        ErrorReportManager.Instance.Log(text);
                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        callback?.Invoke(false);
                    }

                    else if (task.IsFaulted)
                    {
                        _credential = null;
                        foreach (var e in task.Exception.Flatten().InnerExceptions)
                        {
                            string text = string.Format("LinkWithCredentialAsync encountered an error {1}: {0}",e.Message, e.GetType());
                            ErrorReportManager.Instance.Log(text);
                            LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                            if (e is FirebaseAccountLinkException fe)
                            {
                                _credential = fe.UserInfo.UpdatedCredential;
                            }
                        }
                        callback?.Invoke(false);
                    }
                    else if (task.IsCompleted)
                    {
                        if (_credential.Provider != "password")
                        {
                            ResetEmailCredentials();
                            SetSocialUserId();
                        }
                        SetSocialUser(_credential.Provider != "password" ? 1 : 0);

                        LogHelper.LogSdk(string.Format("Credential linked successfully: {0} ({1})",
                            _credential.Provider, _auth.CurrentUser.UserId), LogHelper.FIREBASE_SERVICES_TAG);
                        callback?.Invoke(true);
                    }
                });
            }
            else
            {
                callback?.Invoke(false);
            }
            
        }
        

        /// <summary>
        /// Unlinks a given provider from current user. 
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="callback"></param>
        public virtual void Unlink(string provider, Action<bool> callback = null)
        {
            LogHelper.LogSdk("Unlinking "+ provider);
            LogCurrentUser();
            _auth.CurrentUser.UnlinkAsync(provider).ContinueWithOnMainThread(task =>
            {
                if (task.IsCanceled)
                {
                    string text = "Unlink was canceled.";
                    ErrorReportManager.Instance.Log(text);
                    LogHelper.LogSdk(text);
                    callback?.Invoke(false);
                }
                else if (task.IsFaulted)
                {
                    foreach (var e in task.Exception.InnerExceptions)
                    {
                        string text = string.Format("Unlink encountered an error {1}: {0}", e.Message, e.GetType());
                        ErrorReportManager.Instance.Log(text);
                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                    }
                    callback?.Invoke(false);
                }
                else if (task.IsCompleted)
                {
                    LogHelper.LogSdk(string.Format("Unlink successfully: {0} ({1})",
                        provider, _auth.CurrentUser.UserId), LogHelper.FIREBASE_SERVICES_TAG);
                    callback?.Invoke(true);
                }
            });
        }

        public virtual void Reset()
        {
            ClearCredential();
            _auth.StateChanged -= StateChanged;
            _auth?.Dispose();
            _auth = null;
            _cancellationTokenSource?.Dispose();
            _cancellationTokenSource = null;
        }


        void ResetEmailCredentials()
        {
            LogHelper.LogSdk("Reset email credentials ", LogHelper.FIREBASE_SERVICES_TAG);
            PlayerPrefs.SetString(EMAIL_KEY, "");
            PlayerPrefs.SetString(PASSWORD_KEY, "");
            PlayerPrefs.SetInt(REGISTERED_KEY, 0);
        }

        public bool IsSocialUser()
        {
            bool playerPref = PlayerPrefs.GetInt(SOCIAL_USER, 0) == 1;
            bool anonymous = true;
            bool provider = false;
            if (_auth.CurrentUser != null)
            {
                anonymous = _auth.CurrentUser.IsAnonymous;
                foreach (var userInfo in _auth.CurrentUser.ProviderData)
                {
                    if (userInfo.ProviderId is "apple.com" or "google.com")
                    {
                        provider = true;
                        break;
                    }
                }
            }
            bool result = (playerPref || provider) && !anonymous;
            LogHelper.LogSdk($"Firebase social user: {result} (playerPref: {playerPref}, anonymous: {anonymous}, provider: {provider})");
            return result;
        }

        public bool IsMatchingEmail(string email)
        {
            LogHelper.LogSdk($"matching email: {email} {_auth.CurrentUser.Email == email}");
            return !string.IsNullOrEmpty(email) && _auth.CurrentUser.Email == email;
        }

        public bool IsMatchingSocialUserId(string socialUserId)
        {
            LogHelper.LogSdk($"matching user: {socialUserId} {PlayerPrefs.GetString(SOCIAL_USER_ID, "") == socialUserId}");
            return !string.IsNullOrEmpty(socialUserId) && PlayerPrefs.GetString(SOCIAL_USER_ID, "") == socialUserId;
        }

        void SetSocialUserId()
        {
            string userId = _socialManager.GetUserId();
            LogHelper.LogSdk($"Firebase social user id set: {userId}");
            PlayerPrefs.SetString(SOCIAL_USER_ID, userId);
        }

        void SetSocialUser(int value)
        {
            LogHelper.LogSdk($"Firebase social user set: {value}");
            PlayerPrefs.SetInt(SOCIAL_USER, value);
        }

        public virtual void SignOut()
        {
            SetSocialUser(0);
            ResetEmailCredentials();
            if (_auth.CurrentUser != null)
                LogHelper.LogSdk("Sign out "+_auth.CurrentUser.UserId, LogHelper.FIREBASE_SERVICES_TAG);
            _auth.SignOut();
        }

        public virtual void Delete(Action<bool> callback = null)
        {
            LogHelper.LogSdk("Delete current user "+_auth.CurrentUser.UserId, LogHelper.FIREBASE_SERVICES_TAG);
            _auth.CurrentUser.DeleteAsync().ContinueWithOnMainThread(task =>
            {
                if (task.IsCanceled)
                {
                    string text = "Delete was canceled.";
                    ErrorReportManager.Instance.Log(text);
                    LogHelper.LogSdk(text);
                    callback?.Invoke(false);
                }
                else if (task.IsFaulted)
                {
                    foreach (var e in task.Exception.InnerExceptions)
                    {
                        string text = string.Format("Delete encountered an error: {0}", e.Message);
                        ErrorReportManager.Instance.Log(text);
                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                    }
                    callback?.Invoke(false);
                }
                else if (task.IsCompleted)
                {
                    LogHelper.LogSdk("Deleted successfully" , LogHelper.FIREBASE_SERVICES_TAG);
                    callback?.Invoke(true);
                }
            });
        }

        public virtual bool HasValidCredential()
        {
            return _credential != null && _credential.IsValid();
        }
    }
}
#endif
