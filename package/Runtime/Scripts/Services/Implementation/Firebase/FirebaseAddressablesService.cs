using System;
#if FIREBASE_REMOTE_ADDRESSABLES
using UnityEngine.AddressableAssets;
using RobinBird.FirebaseTools.Storage.Addressables;
#endif

namespace BigfootSdk.Backend
{
    public class FirebaseAddressablesService : IAddressablesService
    {
        public override void InitializeAddressables(Action<bool> loadedCallback)
        {
            #if FIREBASE_REMOTE_ADDRESSABLES
            Addressables.ResourceManager.ResourceProviders.Add(new FirebaseStorageAssetBundleProvider());
            Addressables.ResourceManager.ResourceProviders.Add(new FirebaseStorageJsonAssetProvider());
            Addressables.ResourceManager.ResourceProviders.Add(new FirebaseStorageHashProvider());
            
            Addressables.InternalIdTransformFunc += FirebaseAddressablesCache.IdTransformFunc;

            FirebaseAddressablesManager.IsFirebaseSetupFinished = true;
            #endif
            
            loadedCallback?.Invoke(true);
        }
    }
}
