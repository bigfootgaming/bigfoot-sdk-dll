using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BigfootSdk.Backend
{
    public static class FirebaseEvents
    {
        public static Action OnAuthenticationServiceStarted;
        public static Action OnAuthenticationServiceEnded;
        public static Action OnAuthenticationServiceFailed;
        public static Action OnFirestoreServiceStarted;
        public static Action OnFirestoreServiceEnded;
        public static Action OnFirestoreServiceFailed;
        public static Action OnRemoteConfigServiceStarted;
        public static Action OnRemoteConfigServiceEnded;
        public static Action OnRemoteConfigServiceFailed;
        public static Action OnStorageServiceStarted;
        public static Action OnStorageServiceEnded;
        public static Action OnStorageServiceFailed;

        public static void AuthenticationServiceStarted()
        {
            OnAuthenticationServiceStarted?.Invoke();
        }

        public static void AuthenticationServiceEnded()
        {
            OnAuthenticationServiceEnded?.Invoke();
        }
        
        public static void AuthenticationServiceFailed()
        {
            OnAuthenticationServiceFailed?.Invoke();
        }
        
        public static void FirestoreServiceStarted()
        {
            OnFirestoreServiceStarted?.Invoke();
        }
        
        public static void FirestoreServiceEnded()
        {
            OnFirestoreServiceEnded?.Invoke();
        }
        
        public static void FirestoreServiceFailed()
        {
            OnFirestoreServiceFailed?.Invoke();
        }
        
        public static void RemoteConfigServiceStarted()
        {
            OnRemoteConfigServiceStarted?.Invoke();
        }
        
        public static void RemoteConfigServiceEnded()
        {
            OnRemoteConfigServiceEnded?.Invoke();
        }
        
        public static void RemoteConfigServiceFailed()
        {
            OnRemoteConfigServiceFailed?.Invoke();
        }
        public static void StorageServiceStarted()
        {
            OnStorageServiceStarted?.Invoke();
        }
        
        public static void StorageServiceEnded()
        {
            OnStorageServiceEnded?.Invoke();
        }
        
        public static void StorageServiceFailed()
        {
            OnStorageServiceFailed?.Invoke();
        }
    }
}