﻿#if BACKEND_FIREBASE_FIRESTORE
using System;
using System.Collections.Generic;
using System.Threading;
using BigfootSdk.Helpers;
using BigfootSdk.Extensions;
using Firebase.Firestore;
using BigfootSdk.ErrorReport;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;
using SystemInfo = UnityEngine.Device.SystemInfo;


namespace BigfootSdk.Backend {
    public class FirebaseFirestorePlayerService : IPlayerService
    {
        
        /// <summary>
        /// User id.
        /// </summary>
        protected string _userId;

        /// <summary>
        /// Last time the user saved.
        /// </summary>
        protected long _lastSave;

        private ObscuredString _token;
        
        /// <summary>
        /// Firestore database
        /// </summary>
        protected FirebaseFirestore _db;

        protected ListenerRegistration _listener;

        protected CancellationTokenSource _initCTS;

        protected CancellationTokenSource _setCTS;

        protected virtual void RefreshInitCTS(CancellationTokenSource cts)
        {
            _initCTS = cts;
        }

        protected virtual void RefreshSetCTS(CancellationTokenSource cts)
        {
            _setCTS = cts;
        }

        public virtual void SetUserId(string userId)
        {
            LogHelper.LogSdk($"Setting _userId {userId}", LogHelper.FIREBASE_SERVICES_TAG);
            _userId = userId;
        }

        public virtual void InitializePlayer(bool forceUpdate, Action<bool> callback = null)
        {
            string origin = "InitPlayerData";
            if (!string.IsNullOrEmpty(_userId))
            {
                LogHelper.LogSdk("Getting user document from Firestore", LogHelper.FIREBASE_SERVICES_TAG);
                _lastSave = long.MinValue;
                RefreshInitCTS(new CancellationTokenSource());
                
                _db = FirebaseFirestore.DefaultInstance;
                DocumentReference documentRef = _db.Collection("users").Document(_userId);
                Source source = Source.Default;
                
                FirebaseEvents.FirestoreServiceStarted();
                BackendManager.Instance.SetBackendBusy("InitPlayerData");
                documentRef.GetSnapshotAsync(source).ContinueWithOnMainThread(task =>
                {
                    BackendManager.Instance.SetBackendNotBusy("InitPlayerData");
                    if (task.IsCanceled)
                    {
                        FirebaseEvents.FirestoreServiceFailed();
                        string text = "Get player data was canceled.";;
                        ErrorReportManager.Instance.Log(text);
                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        if (SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                            Continue(callback);
                        else
                            Interrupt(_initCTS, origin, "canceled");
                    }
                    else if (task.IsFaulted)
                    {
                        FirebaseEvents.FirestoreServiceFailed();
                        LogHelper.LogSdk("Get player data was faulted", LogHelper.FIREBASE_SERVICES_TAG);
                        foreach (var inner in task.Exception.InnerExceptions) {
                            if (inner is FirestoreException) {
                                var e = (FirestoreException) inner;
                                string text = string.Format("Firebase Firestore error: {0}", e.Message);
                                ErrorReportManager.Instance.Log(text);
                                LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                            }
                        }

                        if (SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                            Continue(callback);
                        else
                            Interrupt(_initCTS, origin, task.Exception.Message);
                    }
                    else
                    {
                        FirebaseEvents.FirestoreServiceEnded();
                        LogHelper.LogSdk("got firestore document from cache: "+task.Result.Metadata.IsFromCache, LogHelper.FIREBASE_SERVICES_TAG);
                        UpdatePlayer(task.Result, origin, forceUpdate, callback);
                        MetadataChanges changes = MetadataChanges.Exclude;
                        _listener = documentRef.Listen(changes, snapshot =>
                        {
                            PlayerModel player = PlayerManager.Instance.GetPlayerModel();
                            
                            if (snapshot.Exists && !snapshot.Metadata.IsFromCache)
                            {
                                FirestorePlayerModel firestorePlayer = snapshot.ConvertTo<FirestorePlayerModel>();
                                string token = firestorePlayer.Token;
                                //time check is added in case of crashes or situations where the app wasn't quit properly
                                if (_token == token || string.IsNullOrEmpty(token))
                                {
                                    LogHelper.LogSdk($"Listening: matching token");
                                    if (firestorePlayer.ForceReset)
                                    {
                                        PlayerManager.Instance.SetPlayerModel(new PlayerModel(){PlayerId = firestorePlayer.PlayerId, PlayerInfoModels = new Dictionary<string, PlayerInfoModel>(){ {"MainGame", new PlayerInfoModel()}}});
                                    }
                                    else if (PlayerManager.Instance.HasCheated() || TimeHelper.GetTimeInServer() < player.LastSave ||
                                             player.LastSave < firestorePlayer.LastSave || firestorePlayer.ForceUpdate)
                                    {
                                        string text = "Listening: loading server player info";
                                        ErrorReportManager.Instance.Log(text);
                                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                                        firestorePlayer.ForceUpdate = false;
                                        PlayerManager.Instance.SetPlayerModel(FirestoreHelper.FromFirestore(firestorePlayer), callback == null);
                                    }
                                }
                                else
                                {
                                    LogHelper.LogSdk($"Listening: Update from other source. my token: {_token}, remote token: {token}");
                                    //set user empty, so this client doens't save on quit
                                    _userId = "";
                                    PlayerManager.OnSimultaneousSessionsDetected?.Invoke();
                                }

                                
                            }
                        });
                    }
                    
                });

                SetTimeOut(origin, callback, _initCTS, RefreshInitCTS);
            }
            else
            {
                LogHelper.LogSdk("InitializePlayer: _userId is null or empty", LogHelper.FIREBASE_SERVICES_TAG);
                if (SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                {
                    callback?.Invoke(true);
                }
                else
                {
                    BackendManager.OnBackendConnectionFailed?.Invoke(origin, "user empty");
                }
            }
            
        }

        private void ForceSavePlayerData()
        {
            PlayerManager.Instance.SavePlayerData(false);
        }


        protected virtual void UpdatePlayer(DocumentSnapshot snapshot, string origin, bool forceUpdate, Action<bool> callback)
        {
            if (snapshot.Exists)
            {
                PlayerModel player = PlayerManager.Instance.GetPlayerModel();
                FirestorePlayerModel firestorePlayer = snapshot.ConvertTo<FirestorePlayerModel>();

                if (firestorePlayer.ForceReset)
                {
                    PlayerManager.Instance.SetPlayerModel(new PlayerModel(){PlayerId = firestorePlayer.PlayerId, PlayerInfoModels = new Dictionary<string, PlayerInfoModel>(){ {"MainGame", new PlayerInfoModel()}}});
                }
                else if (PlayerManager.Instance.HasCheated() || TimeHelper.GetTimeInServer() < player.LastSave ||
                         player.LastSave < firestorePlayer.LastSave || firestorePlayer.ForceUpdate || forceUpdate)
                {
                    _token = Guid.NewGuid().ToString();
                    firestorePlayer.Token = _token;
                    LogHelper.LogSdk($"Session token: {_token}");
                    string text = "Init: loading server player info";
                    ErrorReportManager.Instance.Log(text);
                    LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                    firestorePlayer.ForceUpdate = false;
                    PlayerManager.Instance.SetPlayerModel(FirestoreHelper.FromFirestore(firestorePlayer), callback == null);
                }
            }
            else
            {
#if BIGFOOT_ANALYTICS
                AnalyticsManager.Instance.TrackCustomEvent("Install", null);
#endif
                LogHelper.LogSdk(String.Format("Document {0} does not exist!", snapshot.Id), LogHelper.FIREBASE_SERVICES_TAG);
                if (!SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                    PlayerManager.Instance.SetPlayerModel(new PlayerModel(){PlayerId = _userId, PlayerInfoModels = new Dictionary<string, PlayerInfoModel>(){ {"MainGame", new PlayerInfoModel()}}});
            }

            Continue(callback);
            
            BackendManager.OnBackendConnectionWorking?.Invoke(origin);
        }

        protected virtual void Continue( Action<bool> callback)
        {
            if (_initCTS != null && !_initCTS.IsCancellationRequested)
            {
                _initCTS.Cancel();
            }
            callback?.Invoke(true);
        }

        protected virtual void Interrupt(CancellationTokenSource source, string origin, string message)
        {
            if (source != null && !source.IsCancellationRequested)
            {
                source.Cancel();
            }
            BackendManager.OnBackendConnectionFailed(origin, message);
        }

        protected virtual void SetTimeOut(string origin, Action<bool> callback, CancellationTokenSource cancellationTokenSource, Action<CancellationTokenSource> refresh)
        {
            int timeout = SdkManager.Instance.Configuration.BackendConfiguration.FirebaseBackendConfiguration.FirestoreTimeout;
            int fraction = SdkManager.Instance.Configuration.BackendConfiguration.FirebaseBackendConfiguration
                .FirestoreTimeoutWarningFraction;
            BackendManager.Instance.SetBackendTimeout(origin, timeout, cancellationTokenSource, fraction, callback, refresh);
        }
        

        public virtual void SetPlayerData(Dictionary<string, string> data, Action<bool> callback)
        {
            //Not needed
        }

        public virtual void SetPlayerData(PlayerModel model, bool withTimeout, Action<bool> callback)
        {
            
            if (!string.IsNullOrEmpty(_userId) && _db == null)
                _db = FirebaseFirestore.DefaultInstance;
            if (model.LastSave != _lastSave && _db != null && !string.IsNullOrEmpty(_userId) && !BackendManager.Instance.IsOffline)
            {
                LogHelper.LogSdk($"Sending player data ({_userId}) to Firestore with timeout: {withTimeout}", LogHelper.FIREBASE_SERVICES_TAG);
                DocumentReference docRef = _db.Collection("users").Document(_userId);
                long id = TimeHelper.GetTimeInServer();
                if (withTimeout)
                {
                    RefreshSetCTS(new CancellationTokenSource());
                    PlayerManager.OnServerSaveStarted?.Invoke(id);
                }

                BackendManager.Instance.SetBackendBusy("SyncPlayerData");
                
                docRef.SetAsync(FirestoreHelper.ToFirestore(model)).ContinueWithOnMainThread(task =>
                {
                    BackendManager.Instance.SetBackendNotBusy("SyncPlayerData");
                    if (task.IsCanceled)
                    {
                        string text = "Set firestore document was canceled.";
                        ErrorReportManager.Instance.Log(text);
                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        callback?.Invoke(false);
                    }
                    else if (task.IsFaulted)
                    {
                        foreach (var e in task.Exception.InnerExceptions)
                        {
                            string text = string.Format("Set firestore document encountered an error: {0}", e.Message);
                            ErrorReportManager.Instance.Log(text);
                            LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        }
                        if (withTimeout && !SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                        {
                            Interrupt( _setCTS, "SyncPlayerData", task.Exception.Message);
                        }
                        callback?.Invoke(false);
                    }
                    else if (task.IsCompleted)
                    {
                        LogHelper.LogSdk("Set firestore document was completed sucessfully", LogHelper.FIREBASE_SERVICES_TAG);
                        _lastSave = model.LastSave;
                        if (withTimeout)
                        {
                            PlayerManager.OnServerSaveCompleted?.Invoke(id);
                            BackendManager.OnBackendConnectionWorking?.Invoke("SyncPlayerData");
                            if (_setCTS != null && !_setCTS.IsCancellationRequested)
                            {
                                _setCTS.Cancel();
                            }
                        }
                        callback?.Invoke(true);
                    }
                });

                if (withTimeout)
                    SetTimeOut("SyncPlayerData", callback,  _setCTS, RefreshSetCTS);
            }
            else
            {
                if (model.LastSave == _lastSave)
                {
                    LogHelper.LogSdk("Not sending player data to Firestore because data didn't change", LogHelper.FIREBASE_SERVICES_TAG);
                }
                if (_db == null)
                {
                    LogHelper.LogSdk("Not sending player data to Firestore because database is null", LogHelper.FIREBASE_SERVICES_TAG);
                }
                if (string.IsNullOrEmpty(_userId))
                {
                    LogHelper.LogSdk("Not sending player data to Firestore because _userId is null", LogHelper.FIREBASE_SERVICES_TAG);
                }
                if (BackendManager.Instance.IsOffline)
                {
                    LogHelper.LogSdk("Not sending player data to Firestore because session is offline", LogHelper.FIREBASE_SERVICES_TAG);
                }
                callback?.Invoke(true);
            }
        }

        public virtual void ConsumeItems(ItemCost items, Action<bool> callback = null)
        {
            
            LogHelper.LogSdk("Firebase service doesn't implement ConsumeItems", LogHelper.FIREBASE_SERVICES_TAG);
        }

        public virtual void UpdateItemsCustomData(List<PlayerItemModel> items, Action<bool> callback)
        {
            LogHelper.LogSdk("Firebase service doesn't implement UpdateItemsCustomData", LogHelper.FIREBASE_SERVICES_TAG);
        }

        public virtual void DeletePlayerData(Action callback = null)
        {
            DocumentReference docRef = _db.Collection("users").Document(_userId);
            docRef.DeleteAsync().ContinueWithOnMainThread(task => callback?.Invoke());
        }

        public virtual void Reset()
        {
            LogHelper.LogSdk("reset firestore");
            _token = "";
            _listener?.Stop();
            _listener?.Dispose();
            _listener = null;
            _db = null;
            _userId = null;
            if (_initCTS != null && !_initCTS.IsCancellationRequested)
            {
                _initCTS.Cancel();
            }
            _initCTS?.Dispose();
            _initCTS = null;
            if (_setCTS != null && !_setCTS.IsCancellationRequested)
            {
                _setCTS.Cancel();
            }
            _setCTS?.Dispose();
            _setCTS = null;
        }
    }
}

#endif