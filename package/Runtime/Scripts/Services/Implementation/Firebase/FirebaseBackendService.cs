﻿#if BACKEND_FIREBASE
using BigfootSdk.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BigfootSdk.ErrorReport;
using BigfootSdk.Helpers;
using Firebase;
using Firebase.Functions;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Firebase backend service
    /// </summary>
    public class FirebaseBackendService : IBackendService
    {
        
        
        /// <summary>
        /// Returns true if firebase is initialized
        /// </summary>
        /// <returns></returns>
        public bool IsBackendAvailable()
        {
            return (false);
        }

        public void Reset()
        {
            FirebaseApp.DefaultInstance?.Dispose();
        }

        /// <summary>
        /// Listen when firebase app is initialized
        /// </summary>
        /// <param name="callback"></param>
        public void ListenBackendAvailable(Action callback = null)
        {  
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task => {
                
                var dependencyStatus = task.Result;
                LogHelper.LogSdk($"backend: {dependencyStatus}");
                ErrorReportManager.Instance.Log($"backend: {dependencyStatus}");
                if (dependencyStatus == DependencyStatus.Available)
                {
                    callback?.Invoke();
                } 
                else {
                    LogHelper.LogError(string.Format(
                        "Could not resolve all Firebase dependencies: {0}", dependencyStatus), Environment.StackTrace);
                }
            });
        }

        /// <summary>
        /// Execute cloud code.
        /// </summary>
        /// <param name="method">Method.</param>
        /// <param name="data">Data.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        public void ExecuteCloudCode(string method, Dictionary<string, string> data, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
        {
            LogHelper.LogWarning("Firebase backend service has no cloud code implementation");
        }

        /// <summary>
        /// Execute cloud code.
        /// </summary>
        /// <param name="method">Method.</param>
        /// <param name="data">Data.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        public void ExecuteCloudCode(string method, string data, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
        {
            LogHelper.LogWarning("Firebase backend service has no cloud code implementation");
        }


        /// <summary>
        /// Execute cloud code.
        /// </summary>
        /// <param name="method">Method.</param>
        /// <param name="data">Data. Can't be null</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        public void ExecuteCloudCode(string method, Dictionary<string, object> data, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
        {
            var function = FirebaseFunctions.DefaultInstance.GetHttpsCallable(method);
            BackendManager.Instance.SetBackendBusy("Functions");
            function.CallAsync(data).ContinueWithOnMainThread(task =>
            {
                BackendManager.Instance.SetBackendNotBusy("Functions");
                if (task.IsCanceled)
                {
                    string text = string.Format("Firebase functions was canceled. Method: {0}.", method);
                    LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                    ErrorReportManager.Instance.Log(text);
                    if (!SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                        BackendManager.OnBackendConnectionFailed?.Invoke("Functions", "canceled");
                    failureCallback?.Invoke();
                }
                else if (task.IsFaulted)
                {
                    foreach (var inner in task.Exception.InnerExceptions) {
                        if (inner is FunctionsException) {
                            var e = (FunctionsException) inner;
                            // Function error code, will be INTERNAL if the failure
                            // was not handled properly in the function call.
                            string text = string.Format("Firebase functions error. Method: {0}. Error code {1}: {2}",
                                method, e.ErrorCode, e.Message);
                            LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                            ErrorReportManager.Instance.Log(text);
                        }
                    }
                    if (!SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                        BackendManager.OnBackendConnectionFailed?.Invoke("Functions", task.Exception.Message);
                    failureCallback?.Invoke();
                }
                else if (task.IsCompleted)
                {
                    BackendManager.OnBackendConnectionWorking?.Invoke("Functions");
                    try
                    {
                        string stringResult = JsonHelper.SerializeObject(task.Result.Data, false);
                        Dictionary<string, object> result =
                            JsonHelper.DesarializeObject<Dictionary<string, object>>(stringResult);
                        successCallback?.Invoke(result);
                    }
                    catch (Exception e)
                    {
                        string stringResult = task.Result.Data.ToString();
                        Dictionary<string, object> result = new Dictionary<string, object>();
                        result.Add("value", stringResult);
                        successCallback?.Invoke(result);
                    }
                   
                }
            });
        }
    }

}
#endif
