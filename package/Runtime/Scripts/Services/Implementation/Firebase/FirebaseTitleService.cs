﻿#if BACKEND_FIREBASE
using System;
using BigfootSdk.Extensions;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BigfootSdk.ErrorReport;
using BigfootSdk.Helpers;
using Firebase.RemoteConfig;
using Firebase.Storage;
using UnityEngine;
using System.Threading;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Firebase title service.
    /// </summary>
    public class FirebaseTitleService : ITitleService
    {
        /// <summary>
        /// The manager
        /// </summary>
        protected TitleManager _manager;

        /// <summary>
        /// Initialize callback
        /// </summary>
        protected Action<bool> _callback;

        protected CancellationTokenSource _cancellationTokenSource;

        protected string _titleDataVersion;

        private Task _storageTask;

        private bool _updatedRealtime;
        
        /// <summary>
        /// Initialize the title service.
        /// </summary>
        /// <param name="callback"></param>
        public virtual void InitializeTitle(Action<bool> callback = null)
        {
            _manager = TitleManager.Instance;
            _callback = callback;
            
            if (TitleManager.Instance.LoadMode != TitleManager.TitleDataLoadMode.OnlyLocal)
            {
                FirebaseEvents.RemoteConfigServiceStarted();
                //Remote config
                BackendManager.Instance.SetBackendBusy("RemoteConfigFetch");
                ConfigSettings settings = new ConfigSettings();
                settings.MinimumFetchIntervalInMilliseconds = 60000;
                settings.FetchTimeoutInMilliseconds = 10000;
                FirebaseRemoteConfig.DefaultInstance.SetConfigSettingsAsync(settings);
                _updatedRealtime = true;
                CheckForUpdates();
            }
            else
            {
                FinishTitleData();
                LogHelper.LogSdk ("Using only local title data", LogHelper.FIREBASE_SERVICES_TAG);
            }
        }
        
        void RemoteConfigUpdatedRemotely(object o, ConfigUpdateEventArgs args)
        {
            LogHelper.LogSdk ("Remote config updated from server", LogHelper.FIREBASE_SERVICES_TAG);
            _updatedRealtime = true;
        }

        public void CheckForUpdates()
        {
            LogHelper.LogSdk ($"Check for remote config updates {_updatedRealtime}", LogHelper.FIREBASE_SERVICES_TAG);
            if (_updatedRealtime)
            {
                _updatedRealtime = false;
                FirebaseRemoteConfig.DefaultInstance.FetchAsync(TimeSpan.Zero).ContinueWithOnMainThread(task =>
                {
                    BackendManager.Instance.SetBackendNotBusy("RemoteConfigFetch");
    #if UNITY_EDITOR_WIN
                FinishTitleData();
                LogHelper.LogSdk ("Using only local title data");
    #else
                    if (task.IsCanceled)
                    {
                        FirebaseEvents.RemoteConfigServiceFailed();
                        string text = "Remote config fetch was canceled";
                        ErrorReportManager.Instance.Log(text);
                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        if (SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                            FinishTitleData();
                        else
                            BackendManager.OnBackendConnectionFailed?.Invoke("RemoteConfig", "canceled");
                    }
                    else if (task.IsFaulted)
                    {
                        FirebaseEvents.RemoteConfigServiceFailed();
                        foreach (var e in task.Exception.InnerExceptions)
                        {
                            string text = string.Format("Remote config fetch failed with exception: {0}", e.Message);
                            ErrorReportManager.Instance.Log(text);
                            LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        }
                        if (SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                            FinishTitleData();
                        else
                            BackendManager.OnBackendConnectionFailed?.Invoke("RemoteConfig", task.Exception.Message);
                    }
                    else if (task.IsCompleted)
                    {
                        FirebaseEvents.RemoteConfigServiceEnded();
                        BackendManager.OnBackendConnectionWorking?.Invoke("RemoteConfig");
                        ActivateFetched();
                    }
    #endif
                });
            }
            else
            {
                TitleDataUpdatedChecked(false);
            }
        }

        void TitleDataUpdatedChecked(bool needsUpdate)
        {
            TitleManager.OnTitleDataUpdateChecked?.Invoke(needsUpdate);
        }


        /// <summary>
        /// Activate the fetched title data.
        /// </summary>
        protected virtual void ActivateFetched()
        {
            BackendManager.Instance.SetBackendBusy("RemoteConfigActivate");
            FirebaseRemoteConfig.DefaultInstance.ActivateAsync().ContinueWithOnMainThread((task) =>
            {
                BackendManager.Instance.SetBackendNotBusy("RemoteConfigActivate");
                FinishActivateFetch();
            });
        }


        protected virtual void FinishActivateFetch()
        {
            string key = "";
#if UNITY_ANDROID
            key = "min_android_version";
#elif UNITY_IOS
            key = "min_ios_version";
#endif
            ConfigValue configAppVersionValue =   FirebaseRemoteConfig.DefaultInstance.GetValue(key);

            string minAppVersion = configAppVersionValue.StringValue.Trim('"');
            _manager.CheckAppVersion(minAppVersion, GetTitleData);
        }
        
        

        protected virtual void GetTitleData(string version)
        {
            //remote config doesn't support '.' in keys
            string fixedVersion = version.Replace('.', '_');
            string key = "";
#if UNITY_ANDROID
            key = string.Format("android_title_data_{0}", fixedVersion);
#elif UNITY_IOS
            key = string.Format("ios_title_data_{0}", fixedVersion);
#endif

            ConfigValue titleDataVersionValue = FirebaseRemoteConfig.DefaultInstance.GetValue(key);
            _titleDataVersion = titleDataVersionValue.StringValue.Trim('"');
            string titleDataLocalVersion = TitleManager.Instance.GetFromTitleData(TitleManager.LocalTitleDataVersionKey);
            LogHelper.LogSdk("UsingServerTitleData " + _manager.UsingServerTitleData, LogHelper.FIREBASE_SERVICES_TAG);
            if (_manager.UsingServerTitleData)
            {
                bool needsUpdate = _titleDataVersion != titleDataLocalVersion;
                TitleDataUpdatedChecked(needsUpdate);
                return;
            }
            
            
            if (_titleDataVersion != titleDataLocalVersion || _manager.LoadMode == TitleManager.TitleDataLoadMode.AlwaysServer)
            {
                GetTitleDataFromStorage();
            }
            else
            {
                FinishRemoteConfig();
            }
        }



        /// <summary>
        /// Download title data file from storage
        /// </summary>
        /// <param name="version"></param>
        protected virtual void GetTitleDataFromStorage()
        {
            string platform = "";
#if UNITY_ANDROID
            platform = "android";
#elif UNITY_IOS
            platform = "ios";
#endif
            LogHelper.LogSdk(string.Format("Downloading {0}_title_data_{1}.json from Firebase Storage", platform, _titleDataVersion), LogHelper.FIREBASE_SERVICES_TAG);
            
            string titleDataPath = string.Format("{0}_title_data_{1}.json", platform, _titleDataVersion);
            
            GetConfigFromUrl(titleDataPath, false, FinishRemoteConfig);
        }

        public virtual void GetConfigFromUrl(string file, bool additive, Action callback)
        {
            RefreshCTS(new CancellationTokenSource());
            string url =
                $"{SdkManager.Instance.Configuration.BackendConfiguration.FirebaseBackendConfiguration.StorageUrl}/{file}";
            StorageReference reference = FirebaseStorage.DefaultInstance.GetReferenceFromUrl(url);

            string tempPath = PathToPersistentDataPath("temp", true);
            
            FirebaseEvents.StorageServiceStarted();

            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
                InternalTimeout(cancellationTokenSource);
                // Start downloading a file
                _storageTask = reference.GetFileAsync(tempPath,
                    new StorageProgress <DownloadState>((DownloadState state) => {
                        // called periodically during the download
                        LogHelper.LogSdk(String.Format(
                            "Progress: {0} of {1} bytes transferred.",
                            state.BytesTransferred,
                            state.TotalByteCount
                        ), LogHelper.FIREBASE_SERVICES_TAG);
                    }), cancellationTokenSource.Token);
                BackendManager.Instance.SetBackendBusy("Storage");
                _storageTask.ContinueWithOnMainThread(resultTask => {
                    BackendManager.Instance.SetBackendNotBusy("Storage");
                    _cancellationTokenSource.Cancel();
                    
                    if (resultTask.IsCanceled)
                    {
                        FirebaseEvents.StorageServiceFailed();
                        string text = "Storage download was canceled";
                        ErrorReportManager.Instance.Log(text);
                        LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        if (!SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                            BackendManager.OnBackendConnectionFailed?.Invoke("Storage", "canceled");
                    }
                    else if (resultTask.IsFaulted)
                    {
                        FirebaseEvents.StorageServiceFailed();
                        foreach (var e in resultTask.Exception.InnerExceptions)
                        {
                            string text =
                                string.Format("Storage download of version {1} failed with exception: {0}",
                                    e.Message, _titleDataVersion);
                            ErrorReportManager.Instance.Log(text);
                            LogHelper.LogSdk(text, LogHelper.FIREBASE_SERVICES_TAG);
                        }

                        if (!SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                            BackendManager.OnBackendConnectionFailed?.Invoke("Storage", resultTask.Exception.Message);
                    }
                    else if (resultTask.IsCompleted)
                    {
                        FirebaseEvents.StorageServiceEnded();
                        string result = File.ReadAllText(PathToPersistentDataPath("temp"));
                        Dictionary<string, object> dic =
                            JsonHelper.DesarializeObject<Dictionary<string, object>>(result);
                        TitleManager.Instance.UpdateTitleData(dic, additive);
                        if (!SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                            callback?.Invoke();
                        BackendManager.OnBackendConnectionWorking?.Invoke("Storage");
                    }

                    if (SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                        callback?.Invoke();
                    
                });
                SetTimeOut(callback);
            }
            else
            {
                if (SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
                {
                    callback?.Invoke();
                }
                else
                {
                    BackendManager.OnBackendConnectionFailed?.Invoke("Storage", "no internet");
                }
            }
        }

        async void InternalTimeout(CancellationTokenSource source)
        {
            Task task = Task.Delay(SdkManager.Instance.Configuration.BackendConfiguration.FirebaseBackendConfiguration
                .StorageTimeout * 2);
            await task;
            
            if (_cancellationTokenSource is { IsCancellationRequested: false } && source != null && source.Token != null &&
            source is { IsCancellationRequested: false } && _storageTask is { IsCompleted: false })
            {
                LogHelper.LogSdk("Storage internal timeout reached");
                source.Cancel();
            }
        }


        protected virtual void SetTimeOut(Action callback)
        {
            int timeout = SdkManager.Instance.Configuration.BackendConfiguration.FirebaseBackendConfiguration.StorageTimeout;
            int fraction = SdkManager.Instance.Configuration.BackendConfiguration.FirebaseBackendConfiguration
                .StorageTimeoutWarningFraction;
            BackendManager.Instance.SetBackendTimeout("Storage", timeout, _cancellationTokenSource, fraction, (init) => 
            {
                callback?.Invoke();
            }, RefreshCTS);
        }
        
        protected virtual void RefreshCTS(CancellationTokenSource cts)
        {
            _cancellationTokenSource = cts;
        }
        
        protected virtual string PathToPersistentDataPath(string filename, bool useURI = false)
        {
#if UNITY_IPHONE
                if(useURI)
                {
                    var uriFileScheme = Uri.UriSchemeFile + "://";
                    if (filename.StartsWith(uriFileScheme))
                    {
                        return filename;
                    }

                    return String.Format("{0}{1}/{2}", uriFileScheme, Application.persistentDataPath, filename);
                }
                
                return Application.persistentDataPath + "/temp";
#else
                return Application.persistentDataPath + "/temp";
#endif
        }


        /// <summary>
        /// Last changes on data
        /// </summary>
        protected virtual void FinishRemoteConfig()
        {
            FirebaseRemoteConfig.DefaultInstance.OnConfigUpdateListener -= RemoteConfigUpdatedRemotely;
            FirebaseRemoteConfig.DefaultInstance.OnConfigUpdateListener += RemoteConfigUpdatedRemotely;
            OverrideRemoteConfigKeys();

            FinishTitleData();
        }

        /// <summary>
        /// changes for A/B tests or override keys
        /// </summary>
        protected virtual void OverrideRemoteConfigKeys()
        {
            IEnumerator<string> enumerator = FirebaseRemoteConfig.DefaultInstance.Keys.GetEnumerator();

            LogHelper.LogSdk("Overriding Remote Config keys", LogHelper.FIREBASE_SERVICES_TAG);
            while (enumerator.MoveNext())
            {
                ConfigValue value = FirebaseRemoteConfig.DefaultInstance.GetValue(enumerator.Current);

                TitleManager.Instance.UpdateTitleDataKeyValuePair(enumerator.Current, value.StringValue);
            }
        }

        protected virtual void FinishTitleData()
        {
            _callback?.Invoke(true);
        }
        
        public virtual void Reset()
        {
            _titleDataVersion = null;
            _manager = null;
            _callback = null;
            _cancellationTokenSource?.Dispose();
            _cancellationTokenSource = null;
            
        }
    }
    
    
}
#endif