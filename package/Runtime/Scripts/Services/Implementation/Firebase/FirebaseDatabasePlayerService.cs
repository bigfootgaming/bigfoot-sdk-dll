﻿#if BACKEND_FIREBASE_DATABASE
using CodeStage.AntiCheat.ObscuredTypes;
using System;
using Firebase.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using BigfootSdk.ErrorReport;
using Firebase;
using BigfootSdk.Helpers;
using Firebase.Database;
#if UNITY_EDITOR
using Firebase.Unity.Editor;
#endif

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Firebase player service.
    /// </summary>
    public class FirebaseDatabasePlayerService : IPlayerService
    {
        /// <summary>
        /// Reference to the user data in realtime database
        /// </summary>
        private DatabaseReference _reference;

        /// <summary>
        /// User id.
        /// </summary>
        private string _userId;

        /// <summary>
        /// Last time the user saved.
        /// </summary>
        private long _lastSave;

        /// <summary>
        /// returns true if the service was inited.
        /// </summary>
        private bool _inited;
        
        /// <summary>
        /// Initializes the player service.
        /// </summary>
        /// <param name="callback"></param>
        public void InitializePlayer(Action<bool> callback = null)
        {
    #if UNITY_EDITOR
            FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(SdkManager.Instance.Config.realtime_database_url);
            FirebaseApp.DefaultInstance.SetEditorP12FileName(SdkManager.Instance.Config.realtime_database_p12);
            FirebaseApp.DefaultInstance.SetEditorServiceAccountEmail(SdkManager.Instance.Config.realtime_database_email);
            FirebaseApp.DefaultInstance.SetEditorP12Password(SdkManager.Instance.Config.realtime_database_p12_password);
    #endif
            
            _userId = PlayerManager.Instance.Player.PlayerId;
            
            _lastSave = long.MinValue;
            
            //create a 3 sec task to continue locally if the server doesn't respond.
            int timeout = SdkManager.Instance.Config.services_time_out;
            Task timeOutTask = Task.Delay(timeout);
            
            GetDatabaseTimeNode(callback);
            timeOutTask.ContinueWithOnMainThread(task =>
            {
                if (!_inited)
                {
                    _inited = true;
                    LogHelper.LogSdk("Timeout reached, so use local data");
                    callback(true);
                }
            });
        }
        
        /// <summary>
        /// Gets the database node.
        /// </summary>
        /// <param name="callback"></param>
        void GetDatabaseTimeNode(Action<bool> callback)
        {
            string path = string.Format("{0}/L", _userId);
            _reference = FirebaseDatabase.DefaultInstance.GetReference(path);
            _reference.KeepSynced(false);
            _reference.GetValueAsync().ContinueWithOnMainThread(task =>
            {
                if (task.IsFaulted)
                {
                    foreach (var inner in task.Exception.InnerExceptions) {
                        if (inner is DatabaseException) {
                            var e = (DatabaseException) inner;
                            ErrorReportManager.Instance.LogException(
                                string.Format("Firebase database error: {0}", e.Message));
                        }
                    }
                }
                
                if (!_inited)
                {
                    if (task.IsCompleted)
                    {
                        long lastSave = long.MinValue;
                        long.TryParse(task.Result.Value.ToString(), out lastSave);
                        PlayerModel player = PlayerManager.Instance.Player;
                        if (TimeHelper.GetTimeInServer() < player.LastSave || player.LastSave < lastSave)
                        {
                            GetDatabaseUserNode(callback);
                        }
                        else
                        {
                            _inited = true;
                            callback(true);
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Gets the database node.
        /// </summary>
        /// <param name="callback"></param>
        void GetDatabaseUserNode(Action<bool> callback)
        {
            _reference = FirebaseDatabase.DefaultInstance.GetReference(_userId);
            _reference.GetValueAsync().ContinueWithOnMainThread(task =>
            {
                if (task.IsFaulted)
                {
                    foreach (var inner in task.Exception.InnerExceptions) {
                        if (inner is DatabaseException) {
                            var e = (DatabaseException) inner;
                            ErrorReportManager.Instance.LogException(
                                string.Format("Firebase database error: {0}", e.Message));
                        }
                    }
                }
                
                if (!_inited)
                {
                    _inited = true;

                    if (task.IsCompleted)
                    {
                        PlayerModel player = PlayerManager.Instance.Player;
                        DataSnapshot snapshot = task.Result;

                        Dictionary<string, object> dic = (Dictionary<string, object>) snapshot.Value;
                        if (dic != null)
                        {
                            LogHelper.LogSdk("loading server player info");
                            if (dic.ContainsKey("L"))
                            {
                                if (long.TryParse(dic["L"].ToString(), out _lastSave))
                                    player.LastSave = _lastSave;
                            }
                            else
                            {
                                player.LastSave = long.MinValue;
                            }

                            // Parse the Currencies
                            if (dic.ContainsKey("C"))
                            {
                                var currencies = (Dictionary<string, object>)dic["C"];
                                foreach (var item in currencies)
                                {
                                    int currencyValue = 0;
                                    if (int.TryParse(item.Value.ToString(), out currencyValue))
                                        player.Currencies[item.Key] = currencyValue;
                                }
                            }
                            else
                            {
                                player.Currencies = new Dictionary<string, ObscuredInt>();
                            }
                            // Parse the items
                            if (dic.ContainsKey("I"))
                            {
                                string serialized = JsonHelper.SerializeObject(dic["I"], false);
                                player.Items = JsonHelper.DesarializeObject<List<PlayerItemModel>>(serialized);
                            }
                            else
                            {
                                player.Items = new List<PlayerItemModel>();
                            }
                            // Parse the Player Data
                            if (dic.ContainsKey("D"))
                            {
                                var data = (Dictionary<string, object>)dic["D"];
                                foreach (var item in data)
                                {
                                    player.Data[item.Key] = item.Value.ToString();
                                }
                            }
                            else
                            {
                                player.Data = new Dictionary<string, ObscuredString>();
                            }
                        }
                    }

                    callback(true);
                }
            });
        }
        
        /// <summary>
        /// Sets the player data.
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="callback">Callback</param>
        public void SetPlayerData(Dictionary<string, string> data, Action<bool> callback)
        {
            long lastSave = long.MinValue;
            if (data.ContainsKey("L"))
            {
                long.TryParse(data["L"], out lastSave);
            }

            //if data has changed from the one fetched from server
            if (lastSave != _lastSave)
            {
                _reference = FirebaseDatabase.DefaultInstance.GetReference(_userId);
                string json = "";
                if (_reference.Key != _userId)
                {
                    Dictionary<string, object> newUser = new Dictionary<string, object>();
                    newUser.Add(PlayerManager.Instance.Player.PlayerId, data);
                    json = JsonHelper.SerializeObject(newUser, false);
                }
                else
                {
                    json = JsonHelper.SerializeObject(data, false);
                }

                _reference.SetRawJsonValueAsync(json).ContinueWithOnMainThread(task =>
                {
                    if (task.IsFaulted || task.IsCanceled)
                    {
                        callback?.Invoke(false);
                    }
                    else if (task.IsCompleted)
                    {
                        _lastSave = lastSave;
                        callback?.Invoke(true);
                    }
                });
            }
        }

        /// <summary>
        /// Sets the player data
        /// </summary>
        /// <param name="model">Model</param>
        /// <param name="callback">Callback</param>
        public void SetPlayerData(PlayerModel model, Action<bool> callback)
        {
            if (model.LastSave != _lastSave)
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                
                dic.Add("L", model.LastSave.ToString());
                dic.Add("C", JsonHelper.SerializeObject(model.Currencies, false));
                dic.Add("D", JsonHelper.SerializeObject(model.Data,false));
                dic.Add("I", JsonHelper.SerializeObject(model.Items, false));
                
                _reference = FirebaseDatabase.DefaultInstance.GetReference(_userId);
                string json = "";
                if (_reference.Key != _userId)
                {
                    Dictionary<string, object> newUser = new Dictionary<string, object>();
                    newUser.Add(_userId, model.GetBackendPlayerModel());
                    json = JsonHelper.SerializeObject(newUser, false);
                }
                else
                {
                    json = JsonHelper.SerializeObject(model.GetBackendPlayerModel(), false);
                }

                _reference.SetRawJsonValueAsync(json).ContinueWithOnMainThread(task =>
                {
                    if (task.IsFaulted || task.IsCanceled)
                    {
                        callback?.Invoke(false);
                    }
                    else if (task.IsCompleted)
                    {
                        _lastSave = model.LastSave;
                        callback?.Invoke(true);
                    }
                        
                });
            }
            
        }

        /// <summary>
        /// Consume items.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="callback"></param>
        public void ConsumeItems(ItemCost items, Action<bool> callback = null)
        {
            LogHelper.LogSdk("Firebase service doesn't implement ConsumeItems");
        }

        /// <summary>
        /// Update item custom data.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="callback"></param>
        public void UpdateItemsCustomData(List<PlayerItemModel> items, Action<bool> callback)
        {
            LogHelper.LogSdk("Firebase service doesn't implement UpdateItemsCustomData");
        }

        /// <summary>
        /// Delete the player data.
        /// </summary>
        /// <param name="callback">Callback.</param>
        public void DeletePlayerData(Action callback = null)
        {
            _reference = FirebaseDatabase.DefaultInstance.GetReference(_userId);
            _reference.RemoveValueAsync().ContinueWithOnMainThread(task => callback?.Invoke());
        }
    }

}



#endif