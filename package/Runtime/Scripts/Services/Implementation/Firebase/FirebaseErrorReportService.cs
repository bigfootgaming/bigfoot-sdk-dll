﻿#if FIREBASE_CRASHLYTICS
using Firebase.Extensions;
using BigfootSdk.Helpers;
using Firebase.Crashlytics;
using System;
using System.Collections.Generic;
using Firebase;

namespace BigfootSdk.ErrorReport
{
    /// <summary>
    /// Crashlytics error report service.
    /// </summary>
    public class FirebaseErrorReportService : IErrorReportService
    {
        /// <summary>
        /// The initilized flag.
        /// </summary>
        bool _initilized = false;

        /// <summary>
        /// if <see langword="true"/> the user Id should be set on intialization callback
        /// </summary>
        bool _shouldSetUserId = false;

        private List<string> _pendingLogs = new List<string>();
        
        private List<Exception> _pendingExceptions = new List<Exception>();

        /// <summary>
        /// Init the service.
        /// </summary>
        public void Init(Action callback)
        {
            #if !BACKEND_FIREBASE
            // Initialize Firebase
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task => {
                
                var dependencyStatus = task.Result;
                if (dependencyStatus == DependencyStatus.Available)
                {
                    _initilized = true;
                    callback?.Invoke();    
                } 
                else {
                    LogHelper.LogError(System.String.Format(
                        "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    callback?.Invoke();
                }
            });
            #else
            _initilized = true;
            foreach (var l in _pendingLogs)
                Crashlytics.Log($"pending log -> {l}");
            _pendingLogs.Clear();
            foreach (var e in _pendingExceptions)
                Crashlytics.LogException(e);
            callback?.Invoke();
#endif
        }

        /// <summary>
        /// Log the specified text. Useful to add information logs before a crash or non-fatal error.
        /// </summary>
        /// <param name="text">Text.</param>
        public void Log(string text)
        {
            if (_initilized)
            {
                Crashlytics.Log(text);
            }
            else
            {
                _pendingLogs.Add(text);
            }
        }



        /// <summary>
        /// Log the specified exception.
        /// </summary>
        /// <param name="e">E.</param>
        public void LogException(Exception e)
        {
            if (_initilized)
            {
                Crashlytics.LogException(e);
            }
            else
            {
                _pendingExceptions.Add(e);
            }
        }

        /// <summary>
        /// Sets the user identifier.
        /// </summary>
        /// <param name="id">Identifier.</param>
        public void SetUserId(string id)
        {
            if (_initilized)
            {
                Crashlytics.SetUserId(id);
                LogHelper.LogSdk("Crashlytics user set.", LogHelper.FIREBASE_SERVICES_TAG);
            }
        }
    }
}
#endif
