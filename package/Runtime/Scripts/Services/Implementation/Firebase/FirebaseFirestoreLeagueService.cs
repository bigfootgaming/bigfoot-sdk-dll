﻿


using System.Globalization;
using Random = UnityEngine.Random;
#if BACKEND_FIREBASE_FIRESTORE
using System;
using Firebase.Extensions;
using System.Collections.Generic;
using BigfootSdk.Helpers;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    public class FirebaseFirestoreLeagueService : ILeagueService
    {
        
        private FirebaseFirestore _db;

        private PlayerLeagueModel _playerLeagueModel;

        private Dictionary<string, object> _league;
        public void InitLeagues()
        {
            _db = FirebaseFirestore.DefaultInstance;
            string playerLeagueString = PlayerManager.Instance.GetFromPlayerData("League", GameModeManager.CurrentGameMode);
            _playerLeagueModel= null;
            if (!string.IsNullOrEmpty(playerLeagueString))
            {
                _playerLeagueModel = JsonHelper.DesarializeObject<PlayerLeagueModel>(playerLeagueString);
                GetLeague();
            }
            else
            {
                SetInitialLeague();
            }
        }


        void GetLeague()
        {
            
        }


        void SetInitialLeague()
        {
            LeagueModel model =
                JsonHelper.DesarializeObject<LeagueModel>(TitleManager.Instance.GetFromTitleData("leagues"));

            Query leagueQuery = _db.Collection("leagues").WhereLessThan("Players", model.Tiers[0].MaxPlayers);
            if (model.Tiers[0].MaxPlayers == -1)
                leagueQuery = _db.Collection("leagues");
    
            leagueQuery.GetSnapshotAsync().ContinueWithOnMainThread(task => {
                QuerySnapshot leagueQuerySnapshot = task.Result;
                
                List<Dictionary<string, object> > dics = new List<Dictionary<string, object>>();
                
                
                
                foreach (DocumentSnapshot documentSnapshot in leagueQuerySnapshot.Documents) {
                    if (documentSnapshot.ContainsField("Players"))
                        dics.Add(documentSnapshot.ToDictionary());
                }

                dics.Sort(delegate(Dictionary<string, object> dic1, Dictionary<string, object> dic2)
                {
                    return int.Parse(dic1["Players"].ToString()).CompareTo(int.Parse(dic2["Players"].ToString(), CultureInfo.InvariantCulture));
                });

                float r = Random.Range(0f, 1f);
                

                //most of the times use an existing league
                if ((r < 0.7f || model.Tiers[0].MaxPlayers == -1) && dics.Count > 0)
                {
                    _playerLeagueModel = new PlayerLeagueModel()
                    {
                        LeagueId = dics[0]["Id"].ToString(),
                        LastPosition = -1,
                        LeagueTier = 0,
                        LastLeagueTier = -1
                    };

                    _league = dics[0];
                    
                    
                    
                }
                //other times create a new one
                else
                {
                    
                }
            });
        }


    }
}
#endif
