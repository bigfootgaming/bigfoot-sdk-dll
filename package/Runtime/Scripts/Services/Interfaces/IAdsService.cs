﻿using System;

namespace BigfootSdk
{
    /// <summary>
    /// Interface for Ads Services
    /// </summary>
    public interface IAdsService
    {
        /// <summary>
        /// Initializes the provider
        /// </summary>
        /// <param name="initializationFinished">Callback for initialization completed</param>
        /// <param name="adTypes">Type of ads to use</param>
        void Initialize(Action initializationFinished, int[] adTypes);

        void Reset();

        /// <summary>
        /// Should <returns><c>true</c>, if the <paramref name="placementId"/> is available, <c>false</c> otherwise.</returns>
        /// </summary>
        /// <param name="placementId">Placement Id to verify</param>
        /// <returns></returns>
        bool IsAdAvailable(int adType, string placementId = "");

        /// <summary>
        /// Tries to show an Interstitial 
        /// </summary>
        /// <param name="placementId"> The placement id</param>
        /// <returns>If it showed the ad</returns>
        bool ShowInterstitial(string placementId = "");

        /// <summary>
        /// Tries to show a RewardedVideo 
        /// </summary>
        /// <param name="placementId"> The placement id</param>
        /// <returns>If it showed the ad</returns>
        bool ShowVideoReward(string placementId = "");

        /// <summary>
        /// Tries to show a Banner
        /// </summary>
        /// <param name="bannerType"> The banner type</param>
        /// <returns>If it showed the ad</returns>
        bool ShowBanner(int bannerType);
        
        /// <summary>
        /// Tries to hide a Banner
        /// </summary>
        /// <param name="bannerType"> The banner type</param>
        void HideBanner(int bannerType);

        /// <summary>
        /// Notify the Ads SDK if it has application focus.
        /// </summary>
        /// <param name="hasFocus"> set to <c>true</c> if the application has focus</param>
        void OnApplicationFocus(bool hasFocus);
    }
}
