﻿using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Notifications;

namespace BigfootSdk
{
    /// <summary>
    /// Interface for LocalNotifications Services
    /// </summary>
    public interface ILocalNotificationService
    {
        /// <summary>
        /// Gets the implementation of the notifications for the current platform;
        /// </summary>
        IGameNotificationsPlatform Platform();
        
        void OnApplicationFocus(bool hasFocus);
        void OnForeground();
        void OnBackground();
        void Initialize(List<LocalNotificationModel> notifications, params GameNotificationChannel[] channels);
        void OnDestroy();
        PendingNotification ScheduleNotification(IGameNotification notification);
        void SendNotification(IGameNotification notification, bool reschedule = false);
        void SendNotification(string notificationId, double seconds = 0);

        IGameNotification SendNotification(string notificationId, string title, string body, DateTime deliveryTime,
            int? badgeNumber = null, bool reschedule = false, string channelId = null, string smallIcon = null,
            string largeIcon = null, string bigPicture = null);
        IGameNotification CreateNotification();
        void CancelNotification(string notificationId);
        void CancelAllNotifications();
        void DismissNotification(int notificationId);
        void DismissAllNotifications();
        void Update();
        void SetInitialLocalNotifications();
        void SetOnSaveLocalNotifications();
        void OnForegrounding();
    }    
}

