﻿
using System;

namespace BigfootSdk.ErrorReport
{
    /// <summary>
    /// Error report service interface.
    /// </summary>
    public interface IErrorReportService
    {
        /// <summary>
        /// Init the service.
        /// </summary>
        void Init(Action callback);

        /// <summary>
        /// Log the specified text.
        /// </summary>
        /// <param name="text">Text.</param>
        void Log(string text);

        /// <summary>
        /// Log the specified exception.
        /// </summary>
        /// <param name="e">E.</param>
        void LogException(Exception e);

        /// <summary>
        /// Sets the user identifier.
        /// </summary>
        /// <param name="id">Identifier.</param>
        void SetUserId(string id);
    }
}

