﻿using System;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Abstract class to load implement Addressables
    /// </summary>
    public abstract class IAddressablesService
    {
        public abstract void InitializeAddressables(Action<bool> loadedCallback);
    }
}