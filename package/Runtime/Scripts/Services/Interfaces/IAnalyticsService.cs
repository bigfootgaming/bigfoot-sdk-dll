﻿using System;
using System.Collections.Generic;
using BigfootSdk.Backend;

namespace BigfootSdk.Analytics
{
	/// <summary>
	/// Interface for the Analytics
	/// </summary>
	public interface IAnalyticsService
	{
		/// <summary>
		/// Initializes the analytics.
		/// </summary>
		/// <param name="initializationFinished">Initialization finished callback.</param>
		void InitializeAnalytics(Action initializationFinished);

		void Reset();

		/// <summary>
		/// Sets the current user id.
		/// </summary>
		/// <param name="userId"></param>
		void SetUserId(string userId);

		/// <summary>
		/// Sends Session Start Event with userID
		/// </summary>
		void TrackSessionStartEvent();

		/// <summary>
		/// Tracks Session End, sending duration
		/// </summary>
		void TrackSessionEndEvent();

		/// <summary>
		/// Tracks Error Events
		/// </summary>
		void TrackErrorEvent();

        /// <summary>
        /// Tracks a transaction event.
        /// </summary>
        /// <param name="data">Transaction data.</param>
        void TrackTransactionEvent(TransactionEventModel data);

        /// <summary>
        /// Tracks a custom event.
        /// </summary>
        /// <param name="eventName">Event name.</param>
        /// <param name="data">Data.</param>
        void TrackCustomEvent(string eventName, Dictionary<string, object> data);

		/// <summary>
		/// Tracks a custom property.
		/// </summary>
		/// <param name="propertyName">Property name.</param>
		/// <param name="propertyValue">Value.</param>
        void TrackCustomProperty(string propertyName, string propertyValue);

		/// <summary>
		/// Tracks an IAP Purchase
		/// </summary>
		/// <param name="itemModel">The item that was purchased</param>
		/// <param name="cost">The real money cost of this IAP</param>
		/// <param name="origin">The origin</param>
		void TrackIapPurchase(BaseItemModel itemModel, RealMoneyCost cost, string origin = "");
		
        /// <summary>
        /// Send saved events to server
        /// </summary>
        void DispatchEvents();

        /// <summary>
        /// An event was ignored
        /// </summary>
        /// <param name="eventName">Event name.</param>
        /// <param name="data">Data.</param>
        void EventIgnored(string eventName, Dictionary<string, object> data);
	}
}
