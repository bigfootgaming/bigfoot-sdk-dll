﻿using System;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Interface for the Progress
	/// </summary>
	public interface IProgressService
	{
		/// <summary>
		/// Called when the title is initialized
		/// </summary>
		void TitleInitialized ();

		/// <summary>
		/// Called when the player is initialized
		/// </summary>
		void PlayerInitialized ();
	}
}
