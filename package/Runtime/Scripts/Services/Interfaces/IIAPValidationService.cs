﻿using System;
using BigfootSdk.Backend;

namespace BigfootSdk.Shop
{
    public interface IIapValidationService
    {
        /// <summary>
        /// Google Play purchase validation..
        /// </summary>
        /// <param name="itemToPurchase">The item to purchase.</param>
        /// <param name="signature">Signature.</param>
        /// <param name="purchaseData">Purchase data.</param>
        /// <param name="purchase">Purchase object.</param>
        /// <param name="validationCallback">Validation callback.</param>
        void GooglePlayPurchaseValidation(string itemToPurchase, string signature, string purchaseData, object purchase, Action<object, IapValidationResultModel> validationCallback);

        /// <summary>
        /// Ios purchase validation.
        /// </summary>
        /// <param name="itemToPurchase">The item to purchase.</param>
        /// <param name="receipt">Receipt.</param>
        /// <param name="purchase">Purchase object.</param>
        /// <param name="validationCallback">Validation callback.</param>
        void IosPurchaseValidation(string itemToPurchase, string receipt, object purchase, Action<object, IapValidationResultModel> validationCallback);
    }

   
}