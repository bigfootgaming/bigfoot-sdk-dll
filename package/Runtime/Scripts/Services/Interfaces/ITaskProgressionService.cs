﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface to work with the Task Progression
/// </summary>
public interface ITaskProgressionService
{ 
    /// <summary>
    /// Returns new tasks for the user
    /// </summary>
    /// <param name="amount">Amount of new tasks to get</param>
    /// <returns>The new tasks</returns>
    List<ITaskModel> GetNewTasks(int amount);

}