﻿using System.Collections.Generic;
using System.Linq;
using BigfootSdk;
using BigfootSdk.Backend;
using UnityEngine;

/// <summary>
/// Interface for Task Listeners.
/// Extend this class to implement your own task listener
/// </summary>
/// <typeparam name="T">The type of task model this listener will work for</typeparam>
public abstract class ITaskListener<T> : MonoBehaviour
{
    /// <summary>
    /// List containing all of the tasks of the type T
    /// </summary>
    protected List<T> _tasks;

    /// <summary>
    /// Cached reference to the TaskManager
    /// </summary>
    protected TasksManager _tasksManager;
    
    /// <summary>
    /// Subscribe to events
    /// </summary>
    protected virtual void OnEnable()
    {
        DependencyManager.OnSomethingFinished += CheckIfTaskManagerLoaded;
        TasksManager.OnActiveTasksChanged += RefreshActiveTasks;
    }

    /// <summary>
    /// Unsubscribe to events
    /// </summary>
    protected virtual void OnDisable()
    {
        DependencyManager.OnSomethingFinished -= CheckIfTaskManagerLoaded;
        TasksManager.OnActiveTasksChanged -= RefreshActiveTasks;

    }

    /// <summary>
    /// Check if the TasksManager already finished loading
    /// </summary>
    /// <param name="managerLoaded"></param>
    void CheckIfTaskManagerLoaded(string managerLoaded)
    {
        if (managerLoaded != TasksManager.Instance.Id) 
            return;
        
        // Cache the manager
        _tasksManager = TasksManager.Instance;
            
        // Refresh the active tasks
        RefreshActiveTasks();
    }
    
    /// <summary>
    /// Refresh the active tasks
    /// </summary>
    protected virtual void RefreshActiveTasks()
    {
        if(_tasksManager != null)
            _tasks = _tasksManager.ActiveTasks.OfType<T>().ToList();
    }
}
