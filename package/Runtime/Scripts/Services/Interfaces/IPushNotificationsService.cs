﻿namespace BigfootSdk.Notifications
{
    public interface IPushNotificationsService
    {
        void Init();

        void Reset();
    }

}

