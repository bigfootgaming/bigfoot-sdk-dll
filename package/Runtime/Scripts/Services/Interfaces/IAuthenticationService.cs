﻿using System;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Interface for Authentication
	/// </summary>
	public interface IAuthenticationService
	{

		void Initialize();
		
		void RegisterDefault(Action<bool> callback = null);

		bool IsSocialUser();

		bool IsMatchingEmail(string email);

		bool IsMatchingSocialUserId(string socialUserId);

		void SetServerPlayerId();
		
		/// <summary>
		/// Registers on the backend with the device identifier.
		/// </summary>
		/// <param name="callback">Callback.</param>
		void RegisterWithDeviceId (Action<bool> callback = null);

		void Reset();

		void ClearCredential();
		
		void RegisterWithEmail (Action<bool> callback = null);

#if UNITY_ANDROID && GOOGLE_PLAY_GAMES
		/// <summary>
		/// Registers on the backend with the play games account.
		/// </summary>
		/// <param name="callback">Callback.</param>
		void RegisterWithPlayGames (Action<bool> callback = null);
#endif

#if UNITY_IOS && APPLE_ID
		void RegisterWithAppleId (Action<bool> callback = null);
		
		void RegisterAndRetrieveDataWithAppleId (Action<bool> callback = null);
#endif
		void LinkWithEmail (Action<bool> callback = null);
		
#if UNITY_ANDROID && GOOGLE_PLAY_GAMES
		/// <summary>
		/// Links on the backend with the play games account.
		/// </summary>
		/// <param name="callback">Callback.</param>
		void LinkWithPlayGames (Action<bool> callback = null);
#endif

#if UNITY_IOS && APPLE_ID
		void LinkWithAppleId (Action<bool> callback = null);
#endif

		/// <summary>
		/// Unlinks provider account.
		/// </summary>
		/// <param name="callback"></param>
		void Unlink(string provider, Action<bool> callback = null);

		void SignOut();

		void Delete(Action<bool> callback = null);

		bool HasValidCredential();
	}
}
