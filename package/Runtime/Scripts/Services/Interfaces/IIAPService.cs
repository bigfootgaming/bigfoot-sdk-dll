﻿using System;

namespace BigfootSdk.Shop
{
    public interface IIapService
    {
        /// <summary>
        /// Connect with the store. Call this before any transaction attempt.
        /// </summary>
        void Connect(Action<bool> callback);

        /// <summary>
        /// Checks if the service is connected.
        /// </summary>
        /// <returns><c>true</c> if the service is connected; otherwise, <c>false</c>.</returns>
        bool IsConnected();

        /// <summary>
        /// Purchases the item on the Android store.
        /// </summary>
        /// <param name="productId">Product identifier.</param>
        /// <param name="callback">Purchase callback.</param>
        /// <param name="callbackFail">Purchase callback if fails.</param>
        void PurchaseItemOnAndroid(string productId, Action<string, string, string, object> callback, Action<string, string> callbackFail);

        /// <summary>
        /// Purchases the item on the iOS store.
        /// </summary>
        /// <param name="productId">Product identifier.</param>
        /// <param name="callback">Purchase callback.</param>
        /// <param name="callbackFail">Purchase callback if fails.</param>
        void PurchaseItemOnIos(string productId, Action<string, string, object> callback, Action<string, string> callbackFail);

        /// <summary>
        /// Purchases the item on web.
        /// </summary>
        /// <param name="productId">Product identifier.</param>
        /// <param name="callback">Callback.</param>
        /// <param name="callbackFail">Callback fail.</param>
        void PurchaseItemOnWeb(string productId, Action callback, Action callbackFail);

        /// <summary>
        /// Purchase completed.
        /// </summary>
        /// <param name="productId">Product identifier.</param>
        void PurchaseCompleted(string productId);

        /// <summary>
        /// Gets the localized price.
        /// </summary>
        /// <returns>The localized price.</returns>
        /// <param name="skuId">Store identifier.</param>
        string GetLocalizedPrice(string skuId);

#if UNITY_IOS
        /// <summary>
        /// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        /// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        /// </summary>
        void RestorePurchases(Action<string, string, object> callback);
#endif
        object GetSubscriptionInfo(string productId);

        bool IsSubscriptionActive(string productId);
        
        bool IsSubscriptionAutoRenewing(string productId);

        long GetSubscriptionRemainingTime(string productId);

        DateTime GetSubscriptionExpirationDate(string productId);

        void UpdateSubscriptionOnAndroid(string oldProductId, string newProductId, Action<string, string, string, object> callback, Action<string, string> callbackFail);
        void UpdateSubscriptionOnIos(string newProductId, Action<string, string, object> callback, Action<string, string> callbackFail);

        bool CanSubscribe(string shopItemId);
    }
}
