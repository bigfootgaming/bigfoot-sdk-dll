﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Backend
{
    public interface ILeagueService
    {
        void InitLeagues();
    }
}
