﻿using System;
using System.Collections.Generic;
using BigfootSdk.Backend;

/// <summary>
/// Interface for all Tasks
/// </summary>
[System.Serializable]
public abstract class ITaskModel : ICloneable
{
    /// <summary>
    /// The Task id
    /// </summary>
    public int TaskId;
    
    /// <summary>
    /// Task type. Usefull to get localization or to group tasks
    /// </summary>
    public string TaskType;
    
    /// <summary>
    /// The current amount for this Task
    /// </summary>
    public double Amount;

    /// <summary>
    /// The current objective for this Task
    /// </summary>
    public double Objective;
    
    /// <summary>
    /// The reward if task is completed
    /// </summary>
    public ITransaction Reward;

    /// <summary>
    /// The Task custom data.
    /// </summary>
    public Dictionary<string, object> Data;

    /// <summary>
    /// Initializes the TaskModel
    /// </summary>
    public virtual void InitializeTaskModel()
    {
        
    }
    
    /// <summary>
    /// Checks if it's complete
    /// </summary>
    /// <returns></returns>
    public virtual bool IsComplete()
    {
        return Amount >= Objective;
    }

    /// <summary>
    /// Update the Task's progress
    /// </summary>
    /// <param name="delta">The delta to add to the current amount</param>
    public virtual void UpdateProgress(double delta)
    {
        Amount += delta;
        
        if (IsComplete())
            TasksManager.Instance.CompleteTask(this);
    }

    /// <summary>
    /// Clones the object (takes into consideration children classes)
    /// </summary>
    /// <returns>A clone of this object</returns>
    public object Clone()
    {
        return MemberwiseClone();
    }
}
