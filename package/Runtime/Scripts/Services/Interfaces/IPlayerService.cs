﻿using System;
using System.Collections.Generic;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Interface for the Backend
	/// </summary>
	public interface IPlayerService
	{
		/// <summary>
		/// Initializes the player from the backend, filling in the PlayerModel in the PlayerManager
		/// </summary>
		/// <param name="callback">Callback.</param>
		void InitializePlayer (bool forceUpdate, Action<bool> callback = null);

		void SetUserId(string userId);

		/// <summary>
		/// Sets the player data.
		/// </summary>
		/// <param name="data">Data.</param>
        void SetPlayerData (Dictionary<string, string> data, Action<bool> callback);
		
		/// <summary>
		/// Sets the whole player data
		/// </summary>
		/// <param name="model"></param>
		/// <param name="callback"></param>
		void SetPlayerData (PlayerModel model, bool withTimeout, Action<bool> callback);

        /// <summary>
        /// Consumes the items.
        /// </summary>
        /// <param name="items">Items.</param>
        /// <param name="callback">Callback.</param>
        void ConsumeItems(ItemCost items, Action<bool> callback = null);

        /// <summary>
        /// Updates the items custom data.
        /// </summary>
        /// <param name="items">Items.</param>
        /// <param name="callback">Callback.</param>
        void UpdateItemsCustomData(List<PlayerItemModel> items, Action<bool> callback);

        /// <summary>
        /// Delete the player data.
        /// </summary>
        /// <param name="callback">Callback.</param>
        void DeletePlayerData(Action callback = null);

        /// <summary>
        /// Reset
        /// </summary>
        void Reset();
	}
}
