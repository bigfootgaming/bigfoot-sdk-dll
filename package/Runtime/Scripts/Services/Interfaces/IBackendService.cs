﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Interface for the Backend
	/// </summary>
    public interface IBackendService {
		/// <summary>
		/// Checks if the backend is available to be used.
		/// </summary>
		/// <returns><c>true</c> if the backend is available; otherwise, <c>false</c>.</returns>
		bool IsBackendAvailable ();

		/// <summary>
		/// Listens for the event that the backend becomes available. Besides the optional callback,
		/// it will call BackendEvents.OnBackendAvailable
		/// </summary>
		/// <param name="callback">Callback</param>
		void ListenBackendAvailable(Action callback = null);

		void Reset();

        /// <summary>
        /// Executes the cloud code.
        /// </summary>
        /// <param name="method">Method.</param>
        /// <param name="data">Data.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        void ExecuteCloudCode(string method, Dictionary<string, string> data, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null);
        
        /// <summary>
        /// Executes the cloud code.
        /// </summary>
        /// <param name="method"></param>
        /// <param name="data"></param>
        /// <param name="successCallback"></param>
        /// <param name="failureCallback"></param>
        void ExecuteCloudCode(string method, Dictionary<string, object> data, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null);
	}
}
