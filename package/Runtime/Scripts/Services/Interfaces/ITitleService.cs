﻿using System;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Interface for the Title
	/// </summary>
	public interface ITitleService
	{
		/// <summary>
		/// Initializes the Title from the backend
		/// </summary>		
		/// <param name="callback">Callback.</param>
		void InitializeTitle(Action<bool> callback = null);

		void CheckForUpdates();

		void GetConfigFromUrl(string url, bool additive, Action callback);

		void Reset();
	}
}