﻿using System;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Progress events.
	/// </summary>
	public class ProgressEvents
	{
		/// <summary>
		/// Event for when the the worlds are initialized
		/// </summary>
		public static Action OnWorldsInitialized;

		/// <summary>
		/// Worlds initialized
		/// </summary>
		public static void WorldsInitialized ()
		{
            OnWorldsInitialized?.Invoke();
        }

		/// <summary>
		/// Event for when the progress of the worlds are initialized
		/// </summary>
		public static Action OnWorldsProgressInitialized;

		/// <summary>
		/// Progress of the worlds initialized
		/// </summary>
		public static void WorldsProgressInitialized ()
		{
            OnWorldsProgressInitialized?.Invoke();
        }
	}
}

