﻿using System;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Backend events.
	/// </summary>
	public class BackendEvents
	{
		/// <summary>
		/// Event for when the backend becomes available
		/// </summary>
		public static Action OnBackendAvailable;

		/// <summary>
		/// Backend became available
		/// </summary>
		public static void BackendAvailable ()
		{
            OnBackendAvailable?.Invoke();
        }
	}
}

