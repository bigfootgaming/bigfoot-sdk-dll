﻿using UnityEngine;
using System.Collections;
using System;

namespace BigfootSdk.Tutorials
{
    public static class TutorialsEvents
    {
        /// <summary>
        /// Triggered after the stage started 
        /// </summary>
        public static Action<string, TutorialStageModel> OnStageStarted;

        /// <summary>
        /// Stage has started.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="currentStage">Current stage.</param>
        public static void StageStarted(string tutorialId, TutorialStageModel currentStage)
        {
            OnStageStarted?.Invoke(tutorialId, currentStage);

        }

        /// <summary>
        /// Triggered as soon as the stage was completed.
        /// </summary>
        public static Action<string,string> OnCompleteStage;

        /// <summary>
        /// Stage is completed.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stageName">Stage name.</param>
        public static void CompleteStage(string tutorialId, string stageName)
        {
            OnCompleteStage?.Invoke(tutorialId, stageName);
        }

        /// <summary>
        /// Triggered when the tutorial is skipped.
        /// </summary>
        public static Action<string, string> OnSkipTutorial;

        /// <summary>
        /// Tutorial is skipped.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stageName">Stage name.</param>
        public static void SkipTutorial(string tutorialId, string stageName)
        {
            OnSkipTutorial?.Invoke(tutorialId, stageName);
        }

        /// <summary>
        /// Triggered when the stage is ready to start. This means is it's turn to start.
        /// </summary>
        public static Action<string, string> OnStageIsReadyToStart;

        /// <summary>
        /// Stages the is ready to start.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stageName">Stage name.</param>
		public static void StageIsReadyToStart(string tutorialId, string stageName)
		{
			OnStageIsReadyToStart?.Invoke(tutorialId, stageName);
		}

        /// <summary>
        ///  Triggered when the tutorial has started.
        /// </summary>
        public static Action<string> OnTutorialStarted;

        /// <summary>
        /// Tutorial has started.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        public static void TutorialStarted(string tutorialId)
        {
            OnTutorialStarted?.Invoke(tutorialId);
        }

        /// <summary>
        /// Triggered when the tutorial has completed.
        /// </summary>
		public static Action<string> OnTutorialCompleted;

        /// <summary>
        /// Tutorial has completed.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
		public static void TutorialCompleted(string tutorialId)
		{
			OnTutorialCompleted?.Invoke(tutorialId);
		}

        /// <summary>
        /// The on highlight tutorial shown.
        /// </summary>
        public static Action<string, string> OnHighlightTutorialShown;

        /// <summary>
        /// Highlights.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stageName">The stage name.</param>
        public static void HighlightTutorialShown(string tutorialId, string stageName)
        {
            OnHighlightTutorialShown?.Invoke(tutorialId, stageName);
        }

        /// <summary>
        /// The on highlight item.
        /// </summary>
        public static Action<GameObject, bool> OnHighlightItem;

        /// <summary>
        /// Highlights the item.
        /// </summary>
        /// <param name="itemToHighlight">Item to highlight.</param>
        /// <param name="translatePos">If set to <c>true</c> translate position.</param>
        public static void HighlightItem(GameObject itemToHighlight, bool translatePos)
        {
            OnHighlightItem?.Invoke(itemToHighlight, translatePos);
        }
        
        /// <summary>
        /// The on dialog shown.
        /// </summary>
        public static Action<TutorialDialog> OnDialogShown;

        /// <summary>
        /// Shows the tutorial dialog.
        /// </summary>
        /// <param name="itemToHighlight">Dialog.</param>
        public static void DialogShown(TutorialDialog itemToHighlight)
        {
            OnDialogShown?.Invoke(itemToHighlight);
        }
    }
}