﻿using System;

namespace BigfootSdk.SceneManagement
{
    /// <summary>
    /// Scene events.
    /// </summary>
    public static class SceneEvents 
    {
        /// <summary>
        /// Triggered when a scene has started to load. 
        /// </summary>
        public static Action<string> OnStartLoadingScene;

        /// <summary>
        /// Scene has started to load.
        /// </summary>
        /// <param name="sceneName">Scene name.</param>
        internal static void StartLoadingScene(string sceneName)
        {
            OnStartLoadingScene?.Invoke(sceneName);
        }

        /// <summary>
        /// Triggered when a scene has been loaded. 
        /// </summary>
        public static Action<string> OnSceneLoaded;

        /// <summary>
        /// Scene has been loaded.
        /// </summary>
        /// <param name="sceneName">Scene name.</param>
        internal static void SceneLoaded(string sceneName)
        {
            OnSceneLoaded?.Invoke(sceneName);
        }
    }
}

