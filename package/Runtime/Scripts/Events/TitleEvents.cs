﻿using System;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Title events.
	/// </summary>
	public class TitleEvents
	{
		/// <summary>
		/// Event for when the title is initialized
		/// </summary>
		public static Action OnTitleInitialized;

		/// <summary>
		/// The title is initialized
		/// </summary>
		public static void TitleInitialized ()
		{
            OnTitleInitialized?.Invoke();
        }

	}
}

