﻿using System;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Player events.
	/// </summary>
	public class PlayerEvents
	{
		/// <summary>
		/// Event for when the player is initialized
		/// </summary>
		public static Action OnPlayerInitialized;

		/// <summary>
		/// Player is initialized
		/// </summary>
		public static void PlayerInitialized ()
		{
            OnPlayerInitialized?.Invoke();
        }

		/// <summary>
		/// Event for when the balance of a currency changes
		/// </summary>
		public static Action<string, int> OnCurrencyBalanceChanged;

		/// <summary>
		/// Balance of a currency changed
		/// </summary>
		public static void CurrencyBalanceChanged (string key, int delta)
		{
            OnCurrencyBalanceChanged?.Invoke(key, delta);
        }

        /// <summary>
        /// Event for when the balance of a VirtualGood changes
        /// </summary>
        public static Action<string, int> OnVirtualGoodBalanceChanged;

        /// <summary>
        /// Balance of a VirtualGood changed
        /// </summary>
        public static void VirtualGoodBalanceChanged(string key, int delta)
        {
            OnVirtualGoodBalanceChanged?.Invoke(key, delta);
        }
    }
}

