﻿using System;
namespace BigfootSdk.Audio
{
    public class AudioEvents
    {
        /// <summary>
        /// Event trigger to play an audio.
        /// </summary>
        public static Action<string, bool, int> OnPlay;

        /// <summary>
        /// Play the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="loop">If set to <c>true</c> the audio will loop.</param>
        /// <param name="groupId">Group identifier.</param>
        internal static void Play(string audioName, bool loop = false, int groupId = 0)
        {
            OnPlay?.Invoke(audioName, loop, groupId);
        }

        /// <summary>
        /// Event trigger to play music with crossfade if any one currently playing.
        /// </summary>
        public static Action<string, bool, int,float> OnPlayWithCrossfade;

        /// <summary>
        /// Plays audio with crossfade is any previously playing
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// /// <param name="loop">If set to <c>true</c> the audio will loop.</param>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="crossfadeTime">Crossfade time</param>
        internal static void PlayWithCrossfade(string audioName, bool loop = false, int groupId = 0, float crossfadeTime = 0.5f)
        {
            OnPlayWithCrossfade?.Invoke(audioName, loop, groupId, crossfadeTime);
        }

        /// <summary>
        /// Event trigger to play an audio with custom volume.
        /// </summary>
        public static Action<string, float, bool, int> OnPlayCustomVolume;

        /// <summary>
        /// Play the specified audio with custom volume.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="customVolume">Custom volume.</param>
        /// <param name="loop">If set to <c>true</c> the audio will loop.</param>
        /// <param name="groupId">Group identifier.</param>
        internal static void PlayCustomVolume(string audioName, float customVolume, bool loop = false, int groupId = 0)
        {
            OnPlayCustomVolume?.Invoke(audioName, customVolume, loop, groupId);
        }

        /// <summary>
        /// Event trigger to play an audio with its volume regulated by distance to target in a 2d/3d space
        /// </summary>
        public static Action<string, AudioTarget3D, bool, int> OnPlayTargetVolume;

        /// <summary>
        /// Plays the specifies audio with its volume regulated by target distance
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="target3D">Target 3D to regulate volume</param>
        /// <param name="loop">If set to <c>true</c> the audio will loop.</param>
        /// <param name="groupId">Group identifier.</param>
        public static void PlayTargetVolume(string audioName, AudioTarget3D target3D, bool loop = false, int groupId = 0)
        {
            OnPlayTargetVolume?.Invoke(audioName, target3D, loop, groupId);
        }

        /// <summary>
        /// Event trigger to pause an audio.
        /// </summary>
        public static Action<string, int> OnPause;

        /// <summary>
        /// Pause the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        internal static void Pause(string audioName, int groupId)
        {
            OnPause?.Invoke(audioName, groupId);
        }

        /// <summary>
        /// Event trigger to unpause an audio.
        /// </summary>
        public static Action<string, int> OnUnpause;

        /// <summary>
        /// Unpause the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        internal static void Unpause(string audioName, int groupId)
        {
            OnUnpause?.Invoke(audioName, groupId);
        }

        /// <summary>
        /// Event trigger to stop an audio.
        /// </summary>
        public static Action<string, int> OnStop;



        /// <summary>
        /// Stop the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        internal static void Stop(string audioName, int groupId)
        {
            OnStop?.Invoke(audioName, groupId);
        }

        /// <summary>
        /// Event trigger to mute a bus.
        /// </summary>
        public static Action<AudioBusEnum> OnMuteBus;

        /// <summary>
        /// Mutes the specified bus.
        /// </summary>
        /// <param name="bus">Bus.</param>
        internal static void MuteBus(AudioBusEnum bus)
        {
            OnMuteBus?.Invoke(bus);
        }

        /// <summary>
        /// Event trigger to unmute a bus.
        /// </summary>
        public static Action<AudioBusEnum> OnUnmuteBus;

        /// <summary>
        /// Unmutes the specified bus.
        /// </summary>
        /// <param name="bus">Bus.</param>
        internal static void UnmuteBus(AudioBusEnum bus)
        {
            OnUnmuteBus?.Invoke(bus);
        }

        public static Action OnUpdateBusVolume;
        internal static void UpdateBusVolume()
        {
            OnUpdateBusVolume?.Invoke();
        }

        /// <summary>
        /// Event trigger to fade a sound.
        /// </summary>
        public static Action<string, int, float, float> OnFade;

        /// <summary>
        /// Fades a sound.
        /// </summary>
        internal static void Fade(string audioName, int groupId, float targetVolume, float fadeTime)
        {
            OnFade?.Invoke(audioName, groupId, targetVolume, fadeTime);
        }

        /// <summary>
        /// Event trigger to play a sound with fade in.
        /// </summary>
        public static Action<string, int, float, bool> OnPlayWithFadeIn;

        /// <summary>
        /// Plays a sound with fade in.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        internal static void PlayWithFadeIn(string audioName, int groupId, float fadeTime, bool loop)
        {
            OnPlayWithFadeIn?.Invoke(audioName, groupId, fadeTime, loop);
        }

        /// <summary>
        /// Event trigger to stop a sound after a fade out
        /// </summary>
        public static Action<string, int, float> OnStopWithFadeOut;

        /// <summary>
        /// Fades out a sound and stops it
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        internal static void StopWithFadeOut(string audioName, int groupId, float fadeTime)
        {
            OnStopWithFadeOut?.Invoke(audioName, groupId, fadeTime);
        }

    }
}

