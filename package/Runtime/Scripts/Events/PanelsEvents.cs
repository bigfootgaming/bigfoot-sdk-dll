﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Panels
{
    public static class PanelsEvents
    {
        /// <summary>
        /// Event for when a panel system manager should be hidden.
        /// </summary>
        public static Action<int> OnHidePanelSystemManager;

        /// <summary>
        /// Hides the panel system manager.
        /// </summary>
        /// <param name="pmId">Panels manager identifier.</param>
        public static void HidePanelSystemManager(int pmId)
        {
            OnHidePanelSystemManager?.Invoke(pmId);
        }

        /// <summary>
        /// Event for when a panel system manager should be shown.
        /// </summary>
        public static Action<int> OnShowPanelSystemManager;

        /// <summary>
        /// Shows the panel system manager.
        /// </summary>
        /// <param name="psmId">Panel system manager identifier.</param>
        public static void ShowPanelSystemManager(int psmId)
        {
            OnShowPanelSystemManager?.Invoke(psmId);
        }

        /// <summary>
        /// Event for when a panel should be shown.
        /// </summary>
        public static Action<string, PanelBehavior, Action<PanelGameObject>, Action<PanelGameObject>, int> OnShowPanel;

        /// <summary>
        /// Shows the panel.
        /// </summary>
        /// <param name="panelName">Panel name.</param>
        /// <param name="previousPanelBehavior">Indicates how to proceed with the previous panel.</param>
        /// <param name="preAction">Pre action.</param>
        /// <param name="postAction">post action.</param>
        /// <param name="pmId">Panels manager identifier.</param>
        public static void ShowPanel(string panelName, PanelBehavior previousPanelBehavior, Action<PanelGameObject> preAction = null, Action<PanelGameObject> postAction = null, int pmId = 0)
        {
            OnShowPanel?.Invoke(panelName, previousPanelBehavior, preAction, postAction, pmId);
        }

        /// <summary>
        /// Event for when a panel should be shown.
        /// </summary>
        public static Action<PanelConfigModel, PanelBehavior, Action<PanelGameObject>, Action<PanelGameObject>, int> OnShowNewPanel;

        /// <summary>
        /// Shows the panel.
        /// </summary>
        /// <param name="panel">Panel config model.</param>
        /// <param name="previousPanelBehavior">Indicates how to proceed with the previous panel.</param>
        /// <param name="preAction">Pre action.</param>
        /// <param name="postAction">post action.</param>
        /// <param name="pmId">Panels manager identifier.</param>
        public static void ShowNewPanel(PanelConfigModel panel, PanelBehavior previousPanelBehavior, Action<PanelGameObject> preAction = null, Action<PanelGameObject> postAction = null, int pmId = 0)
        {
            OnShowNewPanel?.Invoke(panel, previousPanelBehavior, preAction, postAction, pmId);
        }

        /// <summary>
        /// Event for back action.
        /// </summary>
        public static Action<int> OnBack;

        /// <summary>
        /// Back action.
        /// </summary>
        /// <param name="pmId">Panels manager identifier.</param>
        public static void Back(int pmId = 0)
        {
            OnBack?.Invoke(pmId);
        }

        public static Action<int> OnClearStack;

        public static void ClearStack(int pmId = 0)
        {
            OnClearStack?.Invoke(pmId);
        }

        /// <summary>
        /// Event for when the panel stack is empty.
        /// </summary>
        public static Action OnPanelStackIsEmpty;

        /// <summary>
        /// Panel stack is empty
        /// </summary>
        public static void PanelStackIsEmpty()
        {
            OnPanelStackIsEmpty?.Invoke();
        }


        /// <summary>
        /// Event for when a panel start showing.
        /// </summary>
        public static Action<PanelModel, int> OnPanelStartShowing;

        /// <summary>
        /// Panel start showing.
        /// </summary>
        /// <param name="panel">Panel.</param>
        /// <param name="pmId">Panels identifier.</param>
        public static void PanelStartShowing(PanelModel panel, int pmId)
        {
            OnPanelStartShowing?.Invoke(panel, pmId);
        }

        /// <summary>
        /// Event for when a panel was shown.
        /// </summary>
        public static Action<PanelModel, int> OnPanelShown;

        /// <summary>
        /// Panel was shown.
        /// </summary>
        /// <param name="panel">Panel.</param>
        /// <param name="pmId">Panel system manager identifier.</param>
        public static void PanelShown(PanelModel panel, int pmId)
        {
            OnPanelShown?.Invoke(panel, pmId);
        }

        /// <summary>
        /// Event for when a panel start hiding.
        /// </summary>
        public static Action<PanelModel, int> OnPanelStartHiding;

        /// <summary>
        /// Panel start hiding.
        /// </summary>
        /// <param name="panel">Panel.</param>
        /// <param name="pmId">Panels manager identifier.</param>
        public static void PanelStartHiding(PanelModel panel, int pmId)
        {
           OnPanelStartHiding?.Invoke(panel, pmId);
        }

        /// <summary>
        /// Event for when a panel was hidden.
        /// </summary>
        public static Action<PanelModel, int> OnPanelHidden;

        /// <summary>
        /// Panel was hidden.
        /// </summary>
        /// <param name="panel">Panel.</param>
        /// <param name="pmId">Panels manager identifier.</param>
        public static void PanelHidden(PanelModel panel, int pmId)
        {
            OnPanelHidden?.Invoke(panel, pmId);
        }
        
        /// <summary>
        /// Event for when a the overlay is will be shown.
        /// </summary>
        public static Action<int, float> OnShowOverlay;

        /// <summary>
        /// Overlay will be shown.
        /// </summary>
        /// <param name="pmId">Panels manager identifier.</param>
        public static void ShowOverlay(int pmId, float duration)
        {
            OnShowOverlay?.Invoke(pmId, duration);
        }
        
        /// <summary>
        /// Event for when a the overlay is will be hidden.
        /// </summary>
        public static Action<int, float> OnHideOverlay;

        /// <summary>
        /// Overlay will be shown.
        /// </summary>
        /// <param name="pmId">Panels manager identifier.</param>
        public static void HideOverlay(int pmId, float duration)
        {
            OnHideOverlay?.Invoke(pmId, duration);
        }
        
    }
}

