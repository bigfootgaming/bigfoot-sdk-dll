﻿namespace BigfootSdk
{
    /// <summary>
    /// Constants for the RateUs responses
    /// </summary>
    public class RateUsConstants
    {
        /// <summary>
        /// Already Rated
        /// </summary>
        public const int RATED = 1;

        /// <summary>
        /// Doesn't want to rate
        /// </summary>
        public const int DONT_ASK_AGAIN = 2;

        /// <summary>
        /// We haven't asked yet
        /// </summary>
        public const int NOT_ASKET_YET = 3;
    }
}