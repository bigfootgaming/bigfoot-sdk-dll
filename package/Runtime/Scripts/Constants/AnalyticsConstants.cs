﻿namespace BigfootSdk.Analytics
{
    /// <summary>
    /// Analytics constants.
    /// </summary>
    public class AnalyticsConstants
    {
        /// <summary>
        /// REWARD
        /// </summary>
        public const string TRANSACTION_REWARD = "REWARD";

        /// <summary>
        /// PURCHASE
        /// </summary>
        public const string TRANSACTION_PURCHASE = "PURCHASE";

        /// <summary>
        /// SHOP
        /// </summary>
        public const string SINK_SHOP = "SHOP";

    }
}