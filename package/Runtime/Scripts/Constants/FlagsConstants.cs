﻿namespace BigfootSdk
{
    /// <summary>
    /// Constants for the flags
    /// </summary>
    public class FlagsConstants
    {
        /// <summary>
        /// If the user is spender or not.
        /// </summary>
        public const string IS_OLD_PLAYER = "IsOldPlayer";
        
        /// <summary>
        /// If the user is spender or not.
        /// </summary>
        public const string IS_SPENDER = "IsSpender";

        /// <summary>
        /// If the the ad banner is active or not.
        /// </summary>
        public const string IS_BANNER_ACTIVE = "IsBannerActive";

        /// <summary>
        /// Flag to know if we collected the reward
        /// </summary>
        public const string LTE_REWARDS_COLLECTED = "RewardCollected";
        
        /// <summary>
        /// Flag to know if it's the first time a player played a particular event
        /// </summary>
        public const string ALREADY_PLAYED_EVENT = "AlreadyPlayedEvent";
    }
}