﻿namespace BigfootSdk
{
    /// <summary>
    /// Constants for the game modes
    /// </summary>
    public class GameModeConstants
    {
        /// <summary>
        /// Main game
        /// </summary>
        public const string MAIN_GAME = "MainGame";
        
        /// <summary>
        /// Limited Time Event
        /// </summary>
        public const string LIMITED_TIME_EVENT = "LimitedTimeEvent";

        /// <summary>
        /// Used as a unset variable
        /// </summary>
        public const string NONE = "None";

    }
}