﻿namespace BigfootSdk
{
    /// <summary>
    /// Local Notification constants.
    /// </summary>
    public class LocalNotificationConstants
    {
        /// <summary>
        /// Scheduled on start.
        /// </summary>
        public const string SEND_ON_START = "start";

        /// <summary>
        /// Scheduled on save.
        /// </summary>
        public const string SEND_ON_SAVE = "save";

        /// <summary>
        /// Scheduled on start and on save.
        /// </summary>
        public const string SEND_ON_START_AND_SAVE = "start_and_save";
    }
}