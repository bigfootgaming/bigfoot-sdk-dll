﻿namespace BigfootSdk
{
    /// <summary>
    /// Constants for the counters
    /// </summary>
    public class CountersConstants
    {
        /// <summary>
        /// How many sessions a user played.
        /// </summary>
        public const string SESSIONS_AMOUNT = "SessionsAmount";
        
        /// <summary>
        /// How many days a user was active.
        /// </summary>
        public const string DAYS_ACTIVE = "DaysActive";
        
        /// <summary>
        /// How much money the user spent in usd (cents)
        /// </summary>
        public const string REAL_MONEY_SPENT = "RealMoneySpent";
        
        /// <summary>
        /// Amount of ads stacked
        /// </summary>
        public const string ADS_STACKED = "AdsStacked";
        
        /// <summary>
        /// How many times a popup was shown
        /// </summary>
        public const string POPUP_SHOWN = "{0}_PopupShownAmount";
        
        /// <summary>
        /// How many times the user did a soft reset
        /// </summary>
        public const string SOFT_RESET_AMOUNT = "SoftResetAmount";

        /// <summary>
        /// The max level reached in an LTE event. Should add the event id
        /// </summary>
        public const string LTE_MAX_LEVEL_REACHED = "LTEMaxLevelReached";
        
        /// <summary>
        /// Result of the player RateUs
        /// </summary>
        public const string RATE_US = "RateUs";
        
        /// <summary>
        /// How many times an item was purchased 
        /// </summary>
        public const string ITEM_PURCHASED = "Purchased_{0}";
    }
}