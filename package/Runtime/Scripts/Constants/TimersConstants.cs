﻿namespace BigfootSdk
{
    /// <summary>
    /// Constants for the timers
    /// </summary>
    public class TimersConstants
    {
        /// <summary>
        /// Indicates the first login.
        /// </summary>
        public const string FIRST_LOGIN = "FirstLogin";

        /// <summary>
        /// Indicates the last active day.
        /// </summary>
        public const string LAST_ACTIVE_DATE = "LastActiveDate";
        
        /// <summary>
        /// How much time travel the user has
        /// </summary>
        public const string TIME_TRAVEL = "TimeTravel";
        
        /// <summary>
        /// The time the last processing of the resources was made
        /// </summary>
        public const string LAST_IDLE = "LastIdle";
        
        /// <summary>
        /// When the ad timer started
        /// </summary>
        public const string ADS_STARTED = "AdsStarted";
        
        /// <summary>
        /// When was the last time this popup was shown
        /// </summary>
        public const string POPUP_LAST_SHOWN = "{0}_PopupLastShown";

    }
}