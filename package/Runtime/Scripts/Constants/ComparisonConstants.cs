﻿namespace BigfootSdk
{
    /// <summary>
    /// Comparison constants.
    /// </summary>
    public class ComparisonConstants
    {
        /// <summary>
        /// Equals
        /// </summary>
        public const string EQ = "eq";

        /// <summary>
        /// Not Equals
        /// </summary>
        public const string NE = "ne";

        /// <summary>
        /// Greater Than
        /// </summary>
        public const string GT = "gt";

        /// <summary>
        /// Greater Equals
        /// </summary>
        public const string GE = "ge";

        /// <summary>
        /// Less Than
        /// </summary>
        public const string LT = "lt";

        /// <summary>
        /// Less Equals
        /// </summary>
        public const string LE = "le";
    }
}