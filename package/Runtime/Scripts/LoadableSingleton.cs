﻿using UnityEngine;
using System.Collections;

namespace BigfootSdk
{
	public class LoadableSingleton<T> : ILoadable where T : MonoBehaviour {
		
		private static T s_Instance;

        public static T Instance
		{ 
			get {
				return s_Instance;
			}
		}
		
		public virtual void Awake ()
		{
			s_Instance = (T)(object)this;
		}

		public override void StartLoading()
		{
			
		}
	}
}