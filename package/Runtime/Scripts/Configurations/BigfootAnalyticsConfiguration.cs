﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file for events
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Analytics/Bigfoot")]
    [InlineEditor]
    [HideLabel]
    public class BigfootAnalyticsConfiguration : ScriptableObject
    {
        /// <summary>
        /// The name of the AWS Kinesis delivery stream.
        /// </summary>
        public string KinesisDeliveryStreamName;
    }
}
