﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file Unity Iap Validation
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/IapValidation/Unity")]
    [InlineEditor]
    public class UnityIapValidationConfiguration : BaseConfiguration
    {
        /// <summary>
        /// Validation Data (Get it from GoogleTangle, generated by Unity Iap -> Receipt Validator)
        /// </summary>
        public string GoogleIapValidationData;

        /// <summary>
        /// Order (Get it from GoogleTangle, generated by Unity Iap -> Receipt Validator)
        /// </summary>
        public string GoogleIapValidationOrder;
        
        /// <summary>
        /// Key (Get it from GoogleTangle, generated by Unity Iap -> Receipt Validator)
        /// </summary>
        public int GoogleIapValidationKey;
        
        /// <summary>
        /// Validation Data (Get it from GoogleTangle, generated by Unity Iap -> Receipt Validator)
        /// </summary>
        public string AppleIapValidationData;
        
        /// <summary>
        /// Order (Get it from GoogleTangle, generated by Unity Iap -> Receipt Validator)
        /// </summary>
        public string AppleIapValidationOrder;
        
        /// <summary>
        /// Key (Get it from GoogleTangle, generated by Unity Iap -> Receipt Validator)
        /// </summary>
        public int AppleIapValidationKey;
    }
}
