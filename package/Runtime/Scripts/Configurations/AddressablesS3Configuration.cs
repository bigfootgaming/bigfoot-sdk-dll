﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file for addressables upload
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Addressables S3")]
    [InlineEditor]
    [HideLabel]
    public class AddressablesS3Configuration : ScriptableObject
    {
        /// <summary>
        /// The region for the bucket
        /// </summary>
        public string BucketRegion;

        /// <summary>
        /// The bucket name
        /// </summary>
        public string BucketName;

        /// <summary>
        /// The IAM access key
        /// </summary>
        public string IamAccessKeyId;
        
        /// <summary>
        /// The IAM secret key
        /// </summary>
        public string IamSecretKey;

        /// <summary>
        /// The path to the addressables
        /// </summary>
        public string AddressablesPath;
    }
}
