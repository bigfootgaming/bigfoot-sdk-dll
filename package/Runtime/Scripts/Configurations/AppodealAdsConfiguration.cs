﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file Unity Iap Validation
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Ads/Appodeal")]
    [InlineEditor]
    public class AppodealAdsConfiguration : BaseConfiguration
    {
        /// <summary>
        /// App key for Appodeal
        /// </summary>
        public string AppKey;
        
        /// <summary>
        /// Use Location Services
        /// </summary>
        public bool UseLocationServices;
        
        /// <summary>
        /// Is the app directed to children
        /// </summary>
        public bool ChildDirectedApp;

        /// <summary>
        /// List of networks to disable
        /// </summary>
        public List<string> DisabledNetworks;
    }
}
