﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file for the Firebase backend
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Backend/Firebase")]
    [InlineEditor]
    [HideLabel]
    public class FirebaseBackendConfiguration : ScriptableObject
    {
        /// <summary>
        /// The firebase storage url to download title data.
        /// </summary>
        public string StorageUrl;

        /// <summary>
        /// Amount of miliseconds to wait for authentication, after that we assume no internet connection
        /// </summary>
        public int AuthTimeout = 6000;

        public int AuthTimeoutWarningFraction = 2;

        public bool UseEmailAsDefaultAuthMethod;
        
        /// <summary>
        /// Amount of miliseconds to wait for downloads of files from storage, after that we assume no internet connection
        /// </summary>
        public int StorageTimeout = 6000;
        
        public int StorageTimeoutWarningFraction = 2;

        /// <summary>
        /// Amount of miliseconds to wait for downloads of firestore documents
        /// </summary>
        public int FirestoreTimeout = 6000;
        
        public int FirestoreTimeoutWarningFraction = 2;
    }
}
