﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file for the Aws backend
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Backend/Aws")]
    [InlineEditor]
    [HideLabel]
    public class AwstBackendConfiguration : ScriptableObject
    {
        /// <summary>
        /// The name of the app.
        /// </summary>
        [BoxGroup("Basic Configuration")]
        public string AppName;

        
        /// <summary>
        /// The endpoint for the sdk
        /// </summary>
        [BoxGroup("Basic Configuration")]
        public string EndpointURL;
    }
}
