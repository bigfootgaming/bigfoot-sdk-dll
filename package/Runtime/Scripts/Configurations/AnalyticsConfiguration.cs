﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file for events
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Analytics/Main")]
    [InlineEditor]
    public class AnalyticsConfiguration : BaseConfiguration
    {
        /// <summary>
        /// Flag to debug all analytic events
        /// </summary>
        [BoxGroup("Basic Configuration")]
        public bool DebugAnalyticsEvents;
        
        /// <summary>
        /// The period to dispatch analytic events
        /// </summary>
        [BoxGroup("Basic Configuration")]
        [Range(15,60)]
        public float DispatchEventsPeriod;

        /// <summary>
        /// The period to dispatch the health analytic events
        /// </summary>
        [BoxGroup("Basic Configuration")]
        [Range(30,120)]
        public float HealthDispatchEventsPeriod;

        /// <summary>
        /// List of all of the events to ignore. For each ignored event, a call to the available services EventIgnored will be made.
        /// </summary>
        [TabGroup("ConfigTabs", "To Ignore")]
        public List<string> EventsToIgnore = new List<string>();
        
        /// <summary>
        /// Key-value pairs for the events we want to rename, and their new name
        /// </summary>
        [TabGroup("ConfigTabs", "To Rename")]
        public Dictionary<string, string> EventsToRename = new Dictionary<string, string>();
        
        /// <summary>
        /// List of all of the events to be added '_LTE' as a suffix
        /// </summary>
        [TabGroup("ConfigTabs", "For LTE's")]
        public List<string> EventsForLTE = new List<string>();
        
        /// <summary>
        /// Flag to know if we want to use BigfootAnalytics
        /// </summary>
        [TabGroup("Tabs", "Bigfoot Analytics"), LabelText("Enabled")]
        [OnValueChanged("UseBigfootAnalyticsChanged")]
        public bool UseBigfootAnalytics = false;
        
        /// <summary>
        /// The configuration for BigfootAnalytics
        /// </summary>
        [TabGroup("Tabs", "Bigfoot Analytics"), ShowIf("UseBigfootAnalytics")]
        public BigfootAnalyticsConfiguration BigfootAnalyticsConfiguration;
        
        /// <summary>
        /// Flag to know if we want to use FacebookAnalytics
        /// </summary>
        [TabGroup("Tabs", "Facebook Analytics"), LabelText("Enabled")]
        [OnValueChanged("UseFacebookAnalyticsChanged")]
        public bool UseFacebookAnalytics = false;
        
        /// <summary>
        /// Flag to know if we want to use FirebaseAnalytics
        /// </summary>
        [TabGroup("Tabs", "Firebase Analytics"), LabelText("Enabled")]
        [OnValueChanged("UseFirebaseAnalyticsChanged")]
        public bool UseFirebaseAnalytics = false;
        
        /// <summary>
        /// Flag to know if we want to use FirebaseAnalytics
        /// </summary>
        [TabGroup("Tabs", "Amplitude"), LabelText("Enabled")]
        [OnValueChanged("UseAmplitudeChanged")]
        public bool UseAmplitude = false;
        
        [TabGroup("Tabs", "Amplitude"), ShowIf("UseAmplitude")]
        public AmplitudeConfiguration AmplitudeConfiguration;
        

        void UseBigfootAnalyticsChanged()
        {
            SetDefineSymbol("BIGFOOT_ANALYTICS", UseBigfootAnalytics);
        }
        
        void UseFacebookAnalyticsChanged()
        {
            SetDefineSymbol("FACEBOOK_ANALYTICS", UseFacebookAnalytics);
        }
        
        void UseFirebaseAnalyticsChanged()
        {
            SetDefineSymbol("FIREBASE_ANALYTICS", UseFirebaseAnalytics);
        }
        
        void UseAmplitudeChanged()
        {
            SetDefineSymbol("AMPLITUDE", UseAmplitude);
        }
    }
}
