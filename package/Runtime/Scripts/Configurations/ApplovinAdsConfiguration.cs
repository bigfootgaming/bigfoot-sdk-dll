﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file Unity Iap Validation
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Ads/Applovin")]
    [InlineEditor]
    public class ApplovinAdsConfiguration : BaseConfiguration
    {
        /// <summary>
        /// If set to true, opens mediation debugger to check integration
        /// </summary>
        public bool DebugIntegration = false;
        
        public bool CustomCpmFlow = false;
        
        /// <summary>
        /// App key for Applovin
        /// </summary>
        public string AppKey;

        /// <summary>
        /// The rewarded video ad unit id
        /// </summary>
        public string AndroidRewardedVideoAdUnitId;
        
        /// <summary>
        /// The interstitial ad unit id
        /// </summary>
        public string AndroidInterstitialAdUnitId;
        
        /// <summary>
        /// The banner ad unit id
        /// </summary>
        public string AndroidBannerAdUnitId;
        
        /// <summary>
        /// The rewarded video ad unit id
        /// </summary>
        public string AppleRewardedVideoAdUnitId;
        
        /// <summary>
        /// The interstitial ad unit id
        /// </summary>
        public string AppleInterstitialAdUnitId;
        
        /// <summary>
        /// The banner ad unit id
        /// </summary>
        public string AppleBannerAdUnitId;
    }
}
