﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file Unity Iap Validation
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Ads/Ironsource")]
    [InlineEditor]
    public class IronsourceAdsConfiguration : BaseConfiguration
    {
        /// <summary>
        /// App key for Ironsource
        /// </summary>
        public string AppKey;

        /// <summary>
        /// If true, override AppKey value in Android patform
        /// </summary>
        [ToggleGroup("OverrideForAndroid")]
        public bool OverrideForAndroid;

        /// <summary>
        /// App key for Ironsource in Android
        /// </summary>
        [ToggleGroup("OverrideForAndroid")]
        public string AndroidAppKey;

        /// <summary>
        /// If true, override AppKey value in iOS patform
        /// </summary>
        [ToggleGroup("OverrideForIOS")]
        public bool OverrideForIOS;

        /// <summary>
        /// App key for Ironsource in iOS
        /// </summary>
        [ToggleGroup("OverrideForIOS")]
        public string IOSAppKey;

        public string GetAppKey()
        {
#if UNITY_ANDROID
            if (OverrideForAndroid) return AndroidAppKey;
#endif
#if UNITY_IOS
            if (OverrideForIOS) return IOSAppKey;
#endif
            return AppKey;
        }
    }
}
