﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file Iap Validation
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/IapValidation/Main")]
    [InlineEditor]
    public class IapValidationConfiguration : BaseConfiguration
    {
        /// <summary>
        /// Flag to know if we want to use Unity Iap Validation
        /// </summary>
        [TabGroup("Tabs", "Unity Iap Validation"), LabelText("Enabled")]
        [OnValueChanged("UseUnityIapValidationChanged")]
        public bool UseUnityIapValidation = false;
        
        /// <summary>
        /// The configuration for Unity Iap Validation
        /// </summary>
        [TabGroup("Tabs", "Unity Iap Validation"), ShowIf("UseUnityIapValidation")]
        public UnityIapValidationConfiguration UnityIapValidationConfiguration;
        
        void UseUnityIapValidationChanged()
        {
            SetDefineSymbol("UNITY_PURCHASING", UseUnityIapValidation);
        }
    }
}
