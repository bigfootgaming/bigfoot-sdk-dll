﻿using System.Collections.Generic;
using BigfootSdk.Helpers;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file for sprite compression
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Sprite Compression")]
    [InlineEditor]
    [HideLabel]
    public class SpriteCompressionConfiguration : ScriptableObject
    {
        /// <summary>
        /// The paths used to search sprites and compress them.
        /// </summary>
        public List<string> SpritesRelativePaths;
        
        /// <summary>
        /// The paths used to search sprite atlas and compress them.
        /// </summary>
        public List<string> AtlasRelativePaths;

#if UNITY_EDITOR
        /// <summary>
        /// The android compression texture format.
        /// </summary>
        public TextureImporterFormat AndroidTextureImporterFormat = TextureImporterFormat.ETC2_RGBA8Crunched;

        /// <summary>
        /// The compression quality
        /// </summary>
        public TextureImporterCompression AndroidCompressionQuality = TextureImporterCompression.CompressedHQ;
        
        /// <summary>
        /// The apple compression texture format.
        /// </summary>
        public TextureImporterFormat AppleTextureImporterFormat = TextureImporterFormat.ASTC_4x4;
        
        /// <summary>
        /// The compression quality
        /// </summary>
        public TextureImporterCompression AppleCompressionQuality = TextureImporterCompression.CompressedHQ;
#endif
        
        [PropertySpace(SpaceBefore = 10, SpaceAfter = 0), PropertyOrder(2)]
        [Button(ButtonSizes.Large), GUIColor(0, 1, 0)]
        private void CompressImages()
        {
        #if UNITY_EDITOR
            SpriteHelper.UpdateCompression();
        #endif
        }
    }
}
