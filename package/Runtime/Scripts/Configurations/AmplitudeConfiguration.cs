﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file Unity Iap Validation
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Analytics/Amplitude")]
    [InlineEditor]
    [HideLabel]
    public class AmplitudeConfiguration : BaseConfiguration
    {
        /// <summary>
        /// Amplitude API Key
        /// </summary>
        public string APIKey;
    }
}