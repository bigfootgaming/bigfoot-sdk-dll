﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file for ads
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Ads/Main")]
    [InlineEditor]
    public class AdsConfiguration : BaseConfiguration
    {
        /// <summary>
        /// Flag to know if we want to use Appodeal
        /// </summary>
        [TabGroup("Tabs", "Appodeal"), LabelText("Enabled")]
        [OnValueChanged("UseAppodealChanged")]
        public bool UseAppodeal = false;
        
        /// <summary>
        /// The configuration for Appodeal
        /// </summary>
        [TabGroup("Tabs", "Appodeal"), ShowIf("UseAppodeal")]
        public AppodealAdsConfiguration AppodealAdsConfiguration;
        
        /// <summary>
        /// Flag to know if we want to use Applovin
        /// </summary>
        [TabGroup("Tabs", "Applovin"), LabelText("Enabled")]
        [OnValueChanged("UseApplovinChanged")]
        public bool UseApplovin = false;
        
        /// <summary>
        /// The configuration for Applovin
        /// </summary>
        [TabGroup("Tabs", "Applovin"), ShowIf("UseApplovin")]
        public ApplovinAdsConfiguration ApplovingAdsConfiguration;

        /// <summary>
        /// Flag to know if we want to use Ironsource
        /// </summary>
        [TabGroup("Tabs", "Ironsource"), LabelText("Enabled")]
        [OnValueChanged("UseIronsourceChanged")]
        public bool UseIronsource = false;
        
        /// <summary>
        /// The configuration for Ironsource
        /// </summary>
        [TabGroup("Tabs", "Ironsource"), ShowIf("UseIronsource")]
        public IronsourceAdsConfiguration IronsourceAdsConfiguration;
        
        void UseAppodealChanged()
        {
            SetDefineSymbol("USE_APPODEAL", UseAppodeal);
        }
        
        void UseApplovinChanged()
        {
            SetDefineSymbol("USE_APPLOVIN", UseApplovin);
        }
        
        void UseIronsourceChanged()
        {
            SetDefineSymbol("USE_IRONSOURCE", UseIronsource);
        }
    }
}
