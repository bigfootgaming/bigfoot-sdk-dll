﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file for events
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Backend/Main")]
    [InlineEditor]
    public class BackendConfiguration : BaseConfiguration
    {
        /// <summary>
        /// We save player data locally every x amount of seconds.
        /// </summary>
        public float SecondsBetweenPlayerDataLocalSaves = 60;

        public bool AllowOfflineLoad = true;

        public bool ForceAlwaysServer = false;
        
        /// <summary>
        /// Set this to true to skip player server connection during first sessions.
        /// Call BackendManager.SetFirstSessionForPlayerDataComplete to complete first sessions.
        /// </summary>
        public bool SkipServerPlayerDataInFirstSessions;

        /// <summary>
        /// Set this to true to skip title server connection during first sessions.
        /// Call BackendManager.SetFirstSessionForTitleDataComplete to complete first sessions.
        /// </summary>
        public bool SkipServerTitleDataInFirstSessions;
        
        /// <summary>
        /// Flag to know if we want to use AwsBackend
        /// </summary>
        [TabGroup("Tabs", "Aws Backend"), LabelText("Enabled")]
        [OnValueChanged("UseAwsBackendChanged")]
        public bool UseAwsBackend = false;
        
        /// <summary>
        /// The configuration for BigfootBackend
        /// </summary>
        [TabGroup("Tabs", "Aws Backend"), ShowIf("UseAwsBackend")]
        public AwstBackendConfiguration AwsBackendConfiguration;
        
        /// <summary>
        /// Flag to know if we want to use FirebaseBackend
        /// </summary>
        [TabGroup("Tabs", "Firebase Backend"), LabelText("Enabled")]
        [OnValueChanged("UseFirebaseBackendChanged")]
        public bool UseFirebaseBackend = false;
        
        /// <summary>
        /// The configuration for FirebaseBackend
        /// </summary>
        [TabGroup("Tabs", "Firebase Backend"), ShowIf("UseFirebaseBackend")]
        public FirebaseBackendConfiguration FirebaseBackendConfiguration;

        void UseAwsBackendChanged()
        {
            SetDefineSymbol("BACKEND_AWS", UseAwsBackend);
        }
        
        void UseFirebaseBackendChanged()
        {
            SetDefineSymbol("BACKEND_FIREBASE", UseFirebaseBackend);
            SetDefineSymbol("BACKEND_FIREBASE_FIRESTORE", UseFirebaseBackend);
        }
    }
}
