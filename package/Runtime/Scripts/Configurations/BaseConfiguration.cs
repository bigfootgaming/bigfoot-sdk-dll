﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[InlineEditor]
[HideLabel]
public class BaseConfiguration : ScriptableObject
{
    /// <summary>
    /// Set the appropriate compiler flag is the implementation is enabled
    /// </summary>
    protected void SetDefineSymbol(string symbol, bool enable)
    {
        #if UNITY_EDITOR
        
        // Grab the current compiler flags
        string definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup ( EditorUserBuildSettings.selectedBuildTargetGroup );
        List<string> allDefines = definesString.Split ( ';' ).ToList ();

        // Add it to the list
        if (enable)
        {
            if(!allDefines.Contains(symbol))
                allDefines.Add(symbol);
        }
        else
        {
            if(allDefines.Contains(symbol))
                allDefines.Remove(symbol);
        }
        
        // Set the new list
        PlayerSettings.SetScriptingDefineSymbolsForGroup (
            EditorUserBuildSettings.selectedBuildTargetGroup,
            string.Join ( ";", allDefines.ToArray () ) );
        #endif
    }
}
