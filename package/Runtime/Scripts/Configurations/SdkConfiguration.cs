﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Configuration file for the sdk
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Sdk Configuration")]
    [InlineEditor]
    public class SdkConfiguration : ScriptableObject
    {
        /// <summary>
        /// The environment.
        /// </summary>
        [BoxGroup("Basic Configuration")]
        [ValueDropdown("_environments")]
        public string Environment;
        
        /// <summary>
        /// If <see langword="true"/> sdk logs will be debuged.
        /// </summary>
        [BoxGroup("Basic Configuration")]
        public bool DebugSdkEvents;

        public List<string> IgnoredTagsInSdkLogs;

        /// <summary>
        /// The bigfoot sdk version.
        /// </summary>
        [ReadOnly]
        [BoxGroup("Basic Configuration")]
        public string SdkVersion;

        [BoxGroup("Advanced Configurations")]
        public AnalyticsConfiguration AnalyticsConfiguration;
        
        [BoxGroup("Advanced Configurations")]
        public BackendConfiguration BackendConfiguration;
        
        [BoxGroup("Advanced Configurations")]
        public AdsConfiguration AdsConfiguration;
        
        [BoxGroup("Advanced Configurations")]
        public IapValidationConfiguration IapValidationConfiguration;
        
        private string[] _environments = new string[3]{"dev", "staging", "prod"};
    }
}
