﻿using BigfootSdk;
using DG.Tweening;
using TMPro;
using UnityEngine;

/// <summary>
/// Use this component to add a poolable floating text
/// </summary>
public class FloatingText : MonoBehaviour
{
    /// <summary>
    /// The text to show
    /// </summary>
    public TextMeshProUGUI Text;
    
    /// <summary>
    /// The amount to move (local in Y)
    /// </summary>
    public float AmountToMove;

    /// <summary>
    /// The time to move.
    /// </summary>
    public float TimeToMove;

    /// <summary>
    /// The starting color of the text
    /// </summary>
    public Color StartingColor;

    /// <summary>
    /// The end color.
    /// </summary>
    public Color EndColor;

    /// <summary>
    /// The pool.
    /// </summary>
    ObjectPool _pool;

    public void InitializeFloatingText(string text, ObjectPool pool = null)
    {
        // Cache the pool
        _pool = pool;

        // Set the text
        Text.text = text;

        // Initialize the values
        Text.color = StartingColor;

        // Make sure the object is active
        gameObject.SetActive(true);

        // Start doing the tweens
        Text.DOColor(EndColor, TimeToMove);
        transform.DOLocalMoveY(Text.transform.localPosition.y + AmountToMove, TimeToMove);

        // Pool the gameObject when finished
        Invoke("Pool", TimeToMove + 0.5f);
    }

    /// <summary>
    /// Method to pool the object once the tween is finished
    /// </summary>
    void Pool()
    {
        if(_pool != null)
            _pool.PoolObject(gameObject);
    }
}
