﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Class to handle the notifications
    /// </summary>
    public class InGameNotificationDot : MonoBehaviour
    {
        /// <summary>
        /// The id of the notification
        /// </summary>
        public string NotificationId;

        /// <summary>
        /// The notification
        /// </summary>
        public GameObject Notif;
    
        protected virtual void Awake()
        {
            DependencyManager.OnEverythingLoaded += CheckIfShouldShowNotification;
            InGameNotificationManager.OnShowNotification += CheckShowNotification;
            InGameNotificationManager.OnHideNotification += CheckHideNotification;
        }

        protected virtual void OnDestroy()
        {
            InGameNotificationManager.OnShowNotification -= CheckShowNotification;
            InGameNotificationManager.OnHideNotification -= CheckHideNotification;
            DependencyManager.OnEverythingLoaded -= CheckIfShouldShowNotification;
        }

        protected virtual void OnEnable()
        {
            if (InGameNotificationManager.Instance != null)
                CheckIfShouldShowNotification();
        }

        /// <summary>
        /// Check if we should show the notification
        /// </summary>
        /// <param name="notificationId">The notification id</param>
        public virtual void CheckShowNotification(string notificationId)
        {
            if (!string.IsNullOrEmpty(NotificationId) && notificationId == NotificationId)
            {
                ShowNotification();
            }
        }

        /// <summary>
        /// Show the notification
        /// </summary>
        protected virtual void ShowNotification()
        {
            Notif.SetActive(true);
        }

        /// <summary>
        /// Show the notification without any animation
        /// </summary>
        protected virtual void EnableNotification()
        {
            Notif.SetActive(true);
        }
    
        /// <summary>
        /// Check if we should hide the notification
        /// </summary>
        /// <param name="notificationId">The notification id</param>
        public virtual void CheckHideNotification(string notificationId)
        {
            if (!string.IsNullOrEmpty(NotificationId) && notificationId == NotificationId)
            {
                HideNotification();
            }
        }

        /// <summary>
        /// Hide the notification
        /// </summary>
        protected virtual void HideNotification()
        {
            Notif.SetActive(false);
        }

        /// <summary>
        /// Hide the notification without any animation
        /// </summary>
        protected virtual void DisableNotification()
        {
            Notif.SetActive(false);
        }

        /// <summary>
        /// Check if we should be showing the notification
        /// </summary>
        public virtual void CheckIfShouldShowNotification()
        {
            // Show or hide without animation
            if (!string.IsNullOrEmpty(NotificationId) && InGameNotificationManager.Instance.HasActiveNotification(NotificationId))
            {
                EnableNotification();
            }
            else
            {
                DisableNotification();
            }
        }
    }
}
