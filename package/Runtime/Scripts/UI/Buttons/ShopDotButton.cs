﻿using UnityEngine;

namespace BigfootSdk.Shop
{
    /// <summary>
    /// Shop dot buttons for the carousel
    /// </summary>
    public class ShopDotButton : MonoBehaviour
    {
        /// <summary>
        /// The scrollable area
        /// </summary>
        public ScrollableAreaHandler ScrollableArea;

        /// <summary>
        /// The index of this dot (starts at index 1)
        /// </summary>
        public int AreaIndex;

        /// <summary>
        /// The active dot
        /// </summary>
        public GameObject ActiveDot;

        /// <summary>
        /// The inactive dot
        /// </summary>
        public GameObject InactiveDot;

        /// <summary>
        /// Scroll to the area
        /// </summary>
        public virtual void DoScroll()
        {
            ScrollableArea.ScrollToArea(AreaIndex);
            SetActiveDot();
        }

        /// <summary>
        /// Set the dot as active
        /// </summary>
        public virtual void SetActiveDot()
        {
            if (ActiveDot != null)
                ActiveDot.SetActive(true);

            if (InactiveDot != null)
                InactiveDot.SetActive(false);
        }

        /// <summary>
        /// Set the dot as inactive
        /// </summary>
        public virtual void SetInactiveDot()
        {
            if (ActiveDot != null)
                InactiveDot.SetActive(true);

            if (InactiveDot != null)
                ActiveDot.SetActive(false);
        }
    }
}
