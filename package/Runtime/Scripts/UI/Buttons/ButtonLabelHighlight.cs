﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace BigfootSdk
{
	/// <summary>
	/// Used for the highlight of button elements when using SpriteSwap 
	/// </summary>
	public class ButtonLabelHighlight : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
	{
		/// <summary>
		/// Target to animate (i.e button label)
		/// </summary>
		public Transform Target;

		/// <summary>
		/// Gameobject to disable if desired on click event
		/// </summary>
		public GameObject ThingToDisable;

		/// <summary>
		/// Button Reference to listen for click events
		/// </summary>
		public Button ButtonScript;

		/// <summary>
		/// Offset to animate the target.
		/// </summary>
		public float Offset;

		bool _isHoldingDown = false;

		public void Fix_Position_Click()
		{
			if (ButtonScript == null || !ButtonScript.IsInteractable())
				return;
			if (Target != null && !_isHoldingDown)
				Target.localPosition = new Vector3(Target.localPosition.x, Target.localPosition.y - Offset,
					Target.localPosition.z);
			if (ThingToDisable != null)
				ThingToDisable.SetActive(false);
			_isHoldingDown = true;
		}

		public void Fix_Position_Release()
		{
			if (_isHoldingDown)
			{
				if (Target != null)
					Target.localPosition = new Vector3(Target.localPosition.x, Target.localPosition.y + Offset,
						Target.localPosition.z);
				if (ThingToDisable != null)
					ThingToDisable.SetActive(true);
				_isHoldingDown = false;
			}
		}

		//Detect current clicks on the GameObject (the one with the script attached)
		public void OnPointerDown(PointerEventData pointerEventData)
		{
			if (ButtonScript.interactable)
				Fix_Position_Click();
		}

		//Detect if clicks are no longer registering
		public void OnPointerUp(PointerEventData pointerEventData)
		{
			if (ButtonScript.interactable)
				Fix_Position_Release();
		}

		//Detect current clicks on the GameObject (the one with the script attached)
		public void OnPointerExit(PointerEventData pointerEventData)
		{
			Fix_Position_Release();
			if (ButtonScript != null)
			{
				ButtonScript.OnPointerUp(pointerEventData);
			}
		}
	}
}