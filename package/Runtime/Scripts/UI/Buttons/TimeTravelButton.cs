﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace BigfootSdk.Backend
{
    public class TimeTravelButton : MonoBehaviour
    {
        public int Duration;

        public void ApplyTimeTravel()
        {
            // Add the Time Travel
            PlayerManager.Instance.AddTimeTravel(Duration);
        }

    }
}