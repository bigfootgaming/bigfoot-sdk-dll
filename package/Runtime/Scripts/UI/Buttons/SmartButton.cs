﻿using System;
using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Panels;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BigfootSdk
{
    [RequireComponent(typeof(Button))]
    public class SmartButton : MonoBehaviour
    {


        public enum Usage
        {
            ShowPanel,
            Back,
            SendAnalyticsCustomEvent,
            SendAnalyticsUiEvent
        }

        [HideInInspector]
        public Button Button;

        public Usage UseAs;

        public string Value;
        public PanelBehavior PreviousPanelBehavior;

        public Action<PanelGameObject> PreAction = null;
        public Action<PanelGameObject> PostAction = null;

        public int PanelsManagerId = 0;

        #region Analytics UI Event

        public string UserAction;
        public string UiLocation;
        public string UiElement;
        public string UiElementType;

        #endregion

        public void OnClick ()
        { 
            switch (UseAs)
            {
                case Usage.ShowPanel:
                {
                    PanelsEvents.ShowPanel(Value, PreviousPanelBehavior, PreAction, PostAction, PanelsManagerId);
                    break;
                }
                case Usage.Back: 
                { 
                    PanelsEvents.Back(PanelsManagerId);
                    break;
                }
                case Usage.SendAnalyticsCustomEvent:
                {
                    AnalyticsManager.Instance.TrackCustomEvent(Value, null);
                    break;
                }
                case Usage.SendAnalyticsUiEvent:
                {
                    AnalyticsManager.Instance.TrackUIInteractionEvent(UserAction, UiLocation, UiElement, UiElementType);
                    break;
                }
            }

        }

         
    }
}

