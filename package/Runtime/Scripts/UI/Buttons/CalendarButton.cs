﻿using System;
using System.Collections;
using System.Collections.Generic;
using BigfootSdk;
using BigfootSdk.Calendar;
using UnityEngine;
using UnityEngine.UI;

public class CalendarButton : MonoBehaviour
{
    public Button CalendarDialogButton;
    public CalendarDialog CalendarDialog;
    private void OnEnable()
    {
        DependencyManager.OnEverythingLoaded += OnEverythingLoaded;
    }

    void OnDisable()
    {
        DependencyManager.OnEverythingLoaded -= OnEverythingLoaded;
    }
    

    private void OnEverythingLoaded()
    {
        if (CalendarDialogButton != null)
        {
            CalendarDialogButton.interactable = CalendarManager.Instance.IsCalendarAvailable();
        }
    }

    public void OpenCalendar()
    {
        if (CalendarDialog != null)
        {
            CalendarDialog.gameObject.SetActive(true);
            CalendarDialog.Initialize();
        }
            
    }
}
