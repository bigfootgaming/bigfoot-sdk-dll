﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Panels
{
    /// <summary>
    /// Panel animator game object.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class PanelAnimatorGameObject : PanelGameObject
    {
        /// <summary>
        /// The animator component.
        /// </summary>
        protected Animator _anim;
        /// <summary>
        /// The in animation name.
        /// </summary>
        public string InAnimationName = "Show";
        /// <summary>
        /// The out animation name.
        /// </summary>
        public string OutAnimationName = "Hide";

        private int _inAnimationHash;

        private int _outAnimationHash;

        /// <summary>
        /// Awake this instance. Get the animator component
        /// </summary>
        protected virtual void Awake()
        {
            _anim = GetComponent<Animator>();
            _inAnimationHash = Animator.StringToHash(InAnimationName);
            _outAnimationHash = Animator.StringToHash(OutAnimationName);
        }

        /// <summary>
        /// Show this panel with animation.
        /// </summary>
        /// <returns>In transition duration.</returns>
        public override float StartShowing()
        {
            if (_anim != null)
            {
                _anim.Play(_inAnimationHash, 0, 0);
            }
            return base.StartShowing();
        }

        /// <summary>
        /// Hide this panel with animation.
        /// </summary>
        /// <returns>Out transition duration.</returns>
        public override float StartHiding()
        {
            if (_anim != null)
            {
                _anim.Play(_outAnimationHash, 0, 0);
            }
            return base.StartHiding();
        }
    }
}
