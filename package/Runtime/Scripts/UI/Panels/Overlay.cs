﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk.Panels
{
    /// <summary>
    /// Overlay.
    /// </summary>
    public class Overlay : MonoBehaviour
    {
        /// <summary>
        /// The image component.
        /// </summary>
        Image _image;
        /// <summary>
        /// The color alpha.
        /// </summary>
        float _alpha;

        /// <summary>
        /// Should animate
        /// </summary>
        bool _withAnimation;
        
        /// <summary>
        /// Setup the overlay.
        /// </summary>
        /// <param name="alpha">Color alpha.</param>
        /// <param name="withAnimation">If <c>true</c> overlay will be shown and hidden with fade animations.</param>
        public void Setup (float alpha, bool withAnimation)
        {
            // Add image component
            _image = gameObject.AddComponent<Image>();
            
            // Config size and position
            _image.transform.localPosition = Vector3.zero;
            _image.rectTransform.anchoredPosition = Vector2.zero;   
            

            _image.rectTransform.anchorMin = Vector2.one * -0.1f;
            _image.rectTransform.anchorMax = Vector2.one * 1.1f;
            _image.rectTransform.sizeDelta = Vector2.zero;

            // Set initial alpha to 0
            _image.color = Color.clear;
            _alpha = alpha;

            // Leave deactivated
            gameObject.SetActive(false);
            
            _withAnimation = withAnimation;
        }

        /// <summary>
        /// Show the overlay with fade animation
        /// </summary>
        /// <param name="animTime">Animation time.</param>
        void OverlayInWithAnimation (float animTime, bool independentTimeScale = true)
        {
            gameObject.SetActive (true);
            _image.DOColor(Color.black * _alpha, animTime).SetUpdate(independentTimeScale);
        }

        /// <summary>
        /// Show the overlay without animation
        /// </summary>
        /// <param name="animTime">Animation time.</param>
        void OverlayInWithoutAnimation (float animTime = 0)
        {
            _image.color = Color.black * _alpha;
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Hide the overlay with fade animation
        /// </summary>
        /// <param name="animTime">Animation time.</param>
        void OverlayOutWithAnimation(float animTime, bool independentTimeScale = true)
        {
            _image.DOColor(Color.clear, animTime).SetUpdate(independentTimeScale).OnComplete(() => {
                gameObject.SetActive(false);
            });
        }

        /// <summary>
        /// Hide the overaly without animation
        /// </summary>
        /// <param name="animTime">Animation time.</param>
        void OverlayOutWithoutAnimation(float animTime = 0)
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Show the overlay.
        /// </summary>
        /// <param name="animTime">Animation time.</param>
        public void OverlayIn(float animTime = 0, bool independentTimeScale = true)
        {
            if (_withAnimation)
            {
                OverlayInWithAnimation(animTime, independentTimeScale);
            }
            else
            {
                OverlayInWithoutAnimation();
            }
        }

        /// <summary>
        /// Hide the overlay
        /// </summary>
        /// <param name="animTime">Animation time.</param>
        public void OverlayOut (float animTime = 0, bool independentTimeScale = true)
        {
            if (_withAnimation)
            {
                OverlayOutWithAnimation(animTime, independentTimeScale);
            }
            else
            {
                OverlayOutWithoutAnimation();
            }
        }


    }
}

