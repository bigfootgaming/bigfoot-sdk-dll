﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk
{
    /// <summary>
    /// Class to output the console log to the screen
    /// </summary>
    public class LogPanel : MonoBehaviour
    {
        /// <summary>
        /// The actual log
        /// </summary>
        public TextMeshProUGUI ConsoleLog;

        /// <summary>
        /// Cached reference to the transform
        /// </summary>
        private RectTransform _rectTransform;

        /// <summary>
        /// Is the panel open?
        /// </summary>
        private bool _open = false;
        
        /// <summary>
        /// The height of the canvas
        /// </summary>
        private float _canvasHeight;

        void Start()
        {
            // Cache the rect transform
            _rectTransform = transform as RectTransform;
            
            // Get the height of the canvas
            RectTransform parentCanvas = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
            _canvasHeight = parentCanvas.rect.height;
        }

        public void OnToggleButtonClick()
        {
            _open = !_open;
            this._rectTransform.offsetMax = new Vector2(0, _open ? _canvasHeight : 80);

            if (_open)
            {
                StartCoroutine(Refresh());
            }
            else
            {
                StopAllCoroutines();
            }
        }

        IEnumerator Refresh()
        {
            var cheatsManager = CheatsManager.Instance;
            ConsoleLog.text = cheatsManager.Log;
            while (true)
            {
                yield return new WaitForSeconds(5);
                ConsoleLog.text = cheatsManager.Log;
            }
        }
    }
}