﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Panels
{
    /// <summary>
    /// Panel game object. Every panel used through panel sytem must have this object (or a child object).
    /// </summary>
    public class PanelGameObject : MonoBehaviour
    {
        /// <summary>
        /// The max width this panel should have if expanded
        /// </summary>
        public float MaxWidth;
        /// <summary>
        /// The max height this panel should have if expanded
        /// </summary>
        public float MaxHeight;
        /// <summary>
        /// Event for when this panel start showing.
        /// </summary>
        public Action OnPanelStartedShowing;
        /// <summary>
        /// Event for when this panel was shown.
        /// </summary>
        public Action OnPanelFullyShown;
        /// <summary>
        /// Event for when this panel start hiding.
        /// </summary>
        public Action OnPanelStartedHiding;
        /// <summary>
        /// Event for when this panel was hidden.
        /// </summary>
        public Action OnPanelFullyHidden;
        /// <summary>
        /// The duration of the in transition.
        /// </summary>
        public float InDuration = 0;
        /// <summary>
        /// The duration of the out transition.
        /// </summary>
        public float OutDuration = 0;

        /// <summary>
        /// Show this panel.
        /// </summary>
        /// <returns>In transition duration.</returns>
        public virtual float StartShowing()
        {
            PanelStartedShowing();
            return InDuration;
        }

        public virtual void CompleteShowing()
        {
            PanelFullyShown();
        }

        /// <summary>
        /// Starts hiding this panel.
        /// </summary>
        /// <returns>Out transition duration.</returns>
        public virtual float StartHiding()
        {
            PanelStartedHiding();
            return OutDuration;
        }

        /// <summary>
        /// Completes hiding this panel.
        /// </summary>
        public virtual void CompleteHiding ()
        {
            PanelFullyHidden();
        }

        /// <summary>
        /// This panel starts showing.
        /// </summary>
        void PanelStartedShowing()
        {
            OnPanelStartedShowing?.Invoke();
        }

        /// <summary>
        /// This panel was shown.
        /// </summary>
        void PanelFullyShown()
        {
            OnPanelFullyShown?.Invoke();
        }

        /// <summary>
        /// This panel starts hiding.
        /// </summary>
        void PanelStartedHiding()
        {
            OnPanelStartedHiding?.Invoke();
        }

        /// <summary>
        /// This panel was hidden.
        /// </summary>
        void PanelFullyHidden()
        {
            OnPanelFullyHidden?.Invoke();
        }
    }
}

