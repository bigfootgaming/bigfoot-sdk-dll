﻿using UnityEngine;
using UnityEngine.UI;
namespace BigfootSdk
{
    [RequireComponent(typeof(LayoutElement))]
    public class ScrollableArea : MonoBehaviour
    {
        /// <summary>
        /// Layout Element of this game object
        /// </summary>
        private LayoutElement _layoutElement;
        private void Awake()
        {
            // cache the layout element
            _layoutElement = GetComponent <LayoutElement>();
        }
        /// <summary>
        /// The size of this game object based on the layout element preferred width 
        /// </summary>
        /// <returns></returns>
        public Vector2 GetAreaSize()
        {
            if (_layoutElement == null)
                _layoutElement = GetComponent<LayoutElement>();
            
            if(_layoutElement == null)
                return Vector2.zero;
            
            return new Vector2(_layoutElement.preferredWidth, _layoutElement.preferredHeight);
        }
    }
}