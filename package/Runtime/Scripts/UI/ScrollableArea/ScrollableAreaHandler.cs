﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using BigfootSdk.Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk
{
	[RequireComponent(typeof(ScrollableArea))]
	[RequireComponent(typeof(RectTransform))]
	public class ScrollableAreaHandler : MonoBehaviour
	{
		/// <summary>
		/// All the possible states of the scroll
		/// </summary>
		public enum ScrollState
		{
			systemDisable,
			idle,
			scrolling
		}
		
		/// <summary>
		/// The default position for the scroll when this system runs
		/// </summary>
		[Header("System Config")] public int StartingIndexPosition = 0;

		/// <summary>
		/// Animation curve for the scroll positioning
		/// </summary>
		public AnimationCurve PositionCurve;
		
		/// <summary>
		/// Speed multiplier for the animation
		/// </summary>
		[Range(0.001f, 2f)] public float AnimationSpeed;

		/// <summary>
		/// The new area name
		/// </summary>
		[Header("Add panels section")] public string NewAreaName;

		/// <summary>
		/// The size for the new area to create via inspector
		/// </summary>
		public float NewAreaHorizontalSize;

		/// <summary>
		/// Scrollable area prefab used to add in the game
		/// </summary>
		public GameObject ScrollableAreaPrefab;

		/// <summary>
		/// On scroll behaviour complete callback
		/// </summary>
		public Action <int> OnScrollComplete;

		/// <summary>
		/// On scroll behaviour start callback
		/// </summary>
		public Action <int> OnScrollStart;

		/// <summary>
		/// List of all scrollable areas
		/// </summary>
		private ScrollableArea[] _scrollableAreas;

		/// <summary>
		/// The game object rect transform
		/// </summary>
		private RectTransform _rectTransform;

		/// <summary>
		/// The index of the current selected area
		/// </summary>
		private int _currentAreaIndex;

		/// <summary>
		/// Te current state of the scroll
		/// </summary>
		private ScrollState _currentState;


		public void Initialize()
		{
			_scrollableAreas = GetComponentsInChildren<ScrollableArea>();
			if (_scrollableAreas == null)
			{
				LogHelper.LogWarning("Unable to initialize Scrollable area. Null ScrollableArea component");
				SetState(ScrollState.systemDisable);
				return;
			}

			_rectTransform = GetComponent<RectTransform>();
			if (_rectTransform == null)
			{
				LogHelper.LogWarning("Unable to initialize Scrollable area. Null RectTransform component");
				SetState(ScrollState.systemDisable);
				return;
			}

			if (PositionCurve.keys == null)
			{
				LogHelper.LogWarning("Unable to initialize Scrollable area. Empty position animation curve keys");
				SetState(ScrollState.systemDisable);
				return;
			}

			//Get distance to the starting position
			float distance = 0;
			if (!TryGetDistanceToArea(0, StartingIndexPosition, out distance))
			{
				LogHelper.LogWarning("Fail trying to get distance to area");
				SetState(ScrollState.systemDisable);
				return;
			}

			SetHorizontalPosition(distance * -1);

			_currentAreaIndex = StartingIndexPosition;

			SetState(ScrollState.idle);
		}

		/// <summary>
		/// Initialize this class in the unity Start method.
		/// </summary>
		private void Start()
		{
			Initialize();
		}

		/// <summary>
		/// Used to run the scroll behaviour from anywhere in the game
		/// </summary>
		/// <param name="areaIndex"></param>
		public void ScrollToArea(int areaIndex)
		{
			// run only if the state is idle
			if (_currentState != ScrollState.idle)
			{
				if (_currentState == ScrollState.systemDisable)
				{
					LogHelper.LogWarning("Unable to scroll to area. Scrollable Area Handler with error");
				}
				return;
			}
			
			if (_currentAreaIndex == areaIndex)
			{
				return;
			}

			if (areaIndex < 0)
			{
				LogHelper.LogWarning("Unable to show area. Invalid area index");
				return;
			}
			StartCoroutine(ScrollBehaviour(areaIndex));
		}

		/// <summary>
		/// The complete animation behaviour
		/// </summary>
		/// <param name="areaIndex">The target area to scroll</param>
		/// <param name="animatedScroll">Use this parameter to ignore the animation and jump directly to the area selected</param>
		/// <returns></returns>
		private IEnumerator ScrollBehaviour(int areaIndex, bool animatedScroll = true)
		{
			float rawStartingPositionValue = 0;
			if (!TryGetDistanceToArea(0, _currentAreaIndex, out rawStartingPositionValue))
			{
				LogHelper.LogWarning("Unable to run scroll behaviour. Fail trying to get starting position value");
				yield break;
			}

			float startingPositionValue = rawStartingPositionValue * -1;

			float rawEndingPositionValue = 0;
			if (!TryGetDistanceToArea(0, areaIndex, out rawEndingPositionValue))
			{
				LogHelper.LogWarning("Unable to run scroll behaviour. Fail trying to get ending position value");
				yield break;
			}

			// Run callback for the starting animation state
			OnScrollStart?.Invoke(areaIndex);
			
			// Set tje current state as Scrolling
			SetState(ScrollState.scrolling);

			// Set the position directly without animation if needed
			if (!animatedScroll)
			{
				SetHorizontalPosition(rawEndingPositionValue * -1);
				
				// Set tje current state as idle
				SetState(ScrollState.idle);

				// Run callback for the ending animation state
				OnScrollComplete?.Invoke(areaIndex);
				yield break;
			}

			// Values to calculate the animation time period
			float animationStartingTime = Time.time;
			float animationTimeLapsed = 0;

			// Distance to travel between the 2 panels
			float distanceToTravel = rawEndingPositionValue - rawStartingPositionValue;

			// Absolute difference between indexes

			// Animation loop
			while (true)
			{
				// Calculate the lapsed animation time
				animationTimeLapsed += Time.time - animationStartingTime;

				// Get normalized position for the animation based on the lapsed time
				float normalizedAnimationPosition = PositionCurve.Evaluate(animationTimeLapsed * AnimationSpeed);

				// Calculate the new position by adding the distance to travel normalized by time to the starting position
				SetHorizontalPosition(startingPositionValue + distanceToTravel * normalizedAnimationPosition * -1);

				// Break the loop when the normalized position equals 1.
				if (normalizedAnimationPosition == 1)
				{
					break;
				}

				yield return null;
			}

			// Set the new index as the current
			_currentAreaIndex = areaIndex;
			
			// Set tje current state as idle
			SetState(ScrollState.idle);
			
			// Run callback for the ending animation state
			OnScrollComplete?.Invoke(areaIndex);
		}

		/// <summary>
		/// Set the horizontal new position for the scroll (Horizontal value == RectTransform.x value)
		/// </summary>
		/// <param name="position">Horizontal value for the transfrom</param>
		private void SetHorizontalPosition(float position)
		{
			_rectTransform.anchoredPosition = new Vector2(position, _rectTransform.anchoredPosition.y);
		}

		/// <summary>
		/// Get the distance between 2 areas using their indexes values
		/// </summary>
		/// <param name="indexA">Area 1 index value </param>
		/// <param name="indexB">Area 2 index value</param>
		/// <param name="distance">The total distance value</param>
		/// <returns>false if there is something wrong on the calculation</returns>
		private bool TryGetDistanceToArea(int indexA, int indexB, out float distance)
		{
			distance = 0;
			if (_scrollableAreas == null)
			{
				return false;
			}

			// check if both indexes are valid in the amount of areas loaded
			int indexCount = _scrollableAreas.Length;
			if (indexA > indexCount - 1)
			{
				return false;
			}

			if (indexB > indexCount - 1)
			{
				return false;
			}

			// Get the distance between the starting point of the complete scroll size for the first area
			float distanceA = 0;
			for (int i = 0; i < indexA; i++)
			{
				distanceA += _scrollableAreas[i].GetAreaSize().x;
			}

			// Get the distance between the starting point of the complete scroll size for the second area
			float distanceB = 0;
			for (int i = 0; i < indexB; i++)
			{
				distanceB += _scrollableAreas[i].GetAreaSize().x;
			}

			// Subtract distance A from distance B to get the distance between the 2 areas
			distance = distanceB - distanceA;

			return true;
		}

		/// <summary>
		/// Set the system current state
		/// </summary>
		private void SetState(ScrollState newState)
		{
			_currentState = newState;
		}

		/// <summary>
		/// Used to add panel in the inspector
		/// </summary>
		[ContextMenu("Add Panel")]
		public void AddPanel()
		{
			if (string.IsNullOrEmpty(NewAreaName))
			{
				LogHelper.LogWarning("Unable to create a new panel. Null or empty new panel name");
				return;
			}

			if (ScrollableAreaPrefab == null)
			{
				LogHelper.LogWarning("Unable to create a new panel. Null prefab reference in inspector");
				return;
			}

			GameObject rawObj = Instantiate(ScrollableAreaPrefab, this.gameObject.transform);
			rawObj.name = "ScrollableArea - " + NewAreaName;
			rawObj.GetComponent <LayoutElement>().preferredWidth = NewAreaHorizontalSize;
		}
	}
}