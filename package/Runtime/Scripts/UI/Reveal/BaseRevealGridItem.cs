using BigfootSdk.Helpers;
using TMPro;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Base reveal grid item to be handled by the default reveal system for the sdk
    /// </summary>
    public class BaseRevealGridItem : MonoBehaviour
    {
        /// <summary>
        /// Text reference to hold the item name
        /// </summary>
        public TMP_Text Name;

        /// <summary>
        /// Text reference to hold the item amount
        /// </summary>
        public TMP_Text Amount;

        /// <summary>
        /// Grid item ID
        /// </summary>
        private string _id;

        /// <summary>
        /// Value for the grid item
        /// </summary>
        private double _value;

        /// <summary>
        /// Base populate method
        /// </summary>
        /// <param name="id">The item id</param>
        /// <param name="value">The item amount</param>
        public virtual void Populate(string id, double value)
        {
            _id = id;
            _value = value;

            SetName(_id);
            SetAmount(_value.ToString());
        }

        /// <summary>
        /// Set the name text label
        /// </summary>
        /// <param name="nameText">The text name</param>
        protected virtual void SetName(string nameText)
        {
            if (Name == null)
            {
                LogHelper.LogWarning("Unable to set name. Null inspector reference");
                return;
            }

            Name.SetText(nameText);
        }

        /// <summary>
        /// Set the amount text label
        /// </summary>
        /// <param name="amountText">The text amount</param>
        protected virtual void SetAmount(string amountText)
        {
            if (Amount == null)
            {
                LogHelper.LogWarning("Unable to set name. Null inspector reference");
                return;
            }

            Amount.SetText(amountText);
        }
    }
}