using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Core.Idle;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk
{
    public class RevealPanel : MonoBehaviour
    {
        /// <summary>
        /// Entries to show by an id.
        /// </summary>
        protected Dictionary<string, RevealEntryModel> _cachedRevealEntries;

        /// <summary>
        /// Reference to the pool for the reveal panel objects
        /// </summary>
        public ObjectPool Pool;

        /// <summary>
        /// Instance reference to the modular behavior handler which run all the reveal sequences
        /// </summary>
        public SequentialModularBehaviorHandler ModularBehaviorHandler;

        // Reference to the panel content
        public Transform PanelContent;

        /// <summary>
        /// Populate
        /// </summary>
        /// <param name="completeTransactionResultModel">The transaction results to show in the reveal</param>
        public virtual void Populate(TransactionResultModel completeTransactionResultModel)
        {
            if (completeTransactionResultModel == null)
            {
                LogHelper.LogWarning("Unable to create reveal model. Null transaction result model parameter");
                return;
            }

            if (PanelContent == null)
            {
                LogHelper.LogWarning("Null reference to the panel content in the inspector for the RevealPanel. This can lead to an unwanted behavior");
            }

            PanelContent?.gameObject.SetActive(false);

            // Create the new list of entries to reveal
            _cachedRevealEntries = new Dictionary<string, RevealEntryModel>();

            // Cache all the entries
            CacheRevealEntries(completeTransactionResultModel);

            // Initialize pool if needed
            if (Pool.IsInited())
            {
                PanelContent?.gameObject.SetActive(true);
                ModularBehaviorHandler?.Run();
            }
            else
            {
                Pool.StartLoading();

                // When the pool finish his initialization run the modular behavior
                Pool.OnFinishedLoading += (value) =>
                {
                    PanelContent?.gameObject.SetActive(true);
                    // Run the modular behaviors
                    ModularBehaviorHandler?.Run();
                };
            }
        }

        /// <summary>
        /// Get the cached reveal entries
        /// </summary>
        /// <param name="revealEntries">Resulting reveal entries</param>
        public bool GetCachedRevealEntries(out List<RevealEntryModel> revealEntries)
        {
            revealEntries = new List<RevealEntryModel>(_cachedRevealEntries.Values);
            return revealEntries != null;
        }

        /// <summary>
        /// Cache the reveal entries for currency, item and resource transaction
        /// In the case of multiple result with the same name via the composite transaction or custom results, merge the values of those result into 1 reveal entry
        /// </summary>
        /// <param name="transactionResultModel">The transaction result to cache from</param>
        protected virtual void CacheRevealEntries(TransactionResultModel transactionResultModel)
        {
            if (transactionResultModel == null)
            {
                LogHelper.LogWarning("Unable to create reveal model. Null transaction result model parameter");
                return;
            }

            // Cache entries for the currencies
            foreach (string currenciesKey in transactionResultModel.Currencies.Keys)
            {
                // Check if the currency exits in the current reveal entries, if so ... add the new value to the old one, else, create a new entry
                if (!_cachedRevealEntries.TryGetValue(currenciesKey, out RevealEntryModel revealEntryModel))
                {
                    _cachedRevealEntries.Add(currenciesKey, new RevealEntryModel(currenciesKey, transactionResultModel.Currencies[currenciesKey]));
                }
                else
                {
                    revealEntryModel.Amount += transactionResultModel.Currencies[currenciesKey];
                }
            }

            // Cache entries for resources
            foreach (string resouceKey in transactionResultModel.Resources.Keys)
            {
                // Check if the resource exits in the current reveal entries, if so ... add the new value to the old one, else, create a new entry
                if (!_cachedRevealEntries.TryGetValue(resouceKey, out RevealEntryModel revealEntryModel))
                {
                    _cachedRevealEntries.Add(resouceKey, new RevealEntryModel(resouceKey, transactionResultModel.Resources[resouceKey]));
                }
                else
                {
                    revealEntryModel.Amount += transactionResultModel.Resources[resouceKey];
                }
            }

            // Cache entries for the items
            foreach (PlayerItemModel playerItemModel in transactionResultModel.Items)
            {
                // Check if the item exits in the current reveal entries, if so ... add the new value to the old one, else, create a new entry
                if (!_cachedRevealEntries.TryGetValue(playerItemModel.Name, out RevealEntryModel revealEntryModel))
                {
                    _cachedRevealEntries.Add(playerItemModel.Name, new RevealEntryModel(playerItemModel.Name, playerItemModel.AmountRemaining));
                }
                else
                {
                    revealEntryModel.Amount += playerItemModel.AmountRemaining;
                }
            }

            // Run over the custom result
            foreach (string customResultKey in transactionResultModel.CustomResult.Keys)
            {
                // Check if the custom result has another transaction model inside and repeat the processes of cache for those transactions
                if (transactionResultModel.CustomResult[customResultKey] is TransactionResultModel)
                {
                    TransactionResultModel singleTransactionResultModel = (TransactionResultModel)transactionResultModel.CustomResult[customResultKey];
                    CacheRevealEntries(singleTransactionResultModel);
                }
            }
        }
    }
}