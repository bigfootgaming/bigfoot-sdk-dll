﻿using System.Collections.Generic;
using BigfootSdk.Extensions;
using DG.Tweening;
using UnityEngine.UI;

namespace BigfootSdk
{
    public class ShowGridItemsSummary : BaseSequentialModularBehavior
    {
        /// <summary>
        /// The grid layout group to contain all the summary
        /// </summary>
        public GridLayoutGroup SummaryGridLayoutGroup;

        /// <summary>
        /// The inspector button reference to skip this behavior
        /// </summary>
        public Button SkipButton;

        /// <summary>
        /// The time that takes to scale a single item in the summary
        /// </summary>
        public float SingleItemScaleAnimationTime = 0.3f;

        /// <summary>
        /// Delay time between each grid item animation
        /// </summary>
        public float AnimationDelayBetweenItems = 0.02f;

        /// <summary>
        /// The time that takes to scale a single item in the summary
        /// </summary>
        public Ease SingleItemEaseAnimation = Ease.Linear;

        /// <summary>
        /// Cache grid items to reveal instance reference
        /// </summary>
        public CacheGridItemsToReveal CacheGridItemsToReveal;

        /// <summary>
        /// List of cached grid items to show in the summary
        /// </summary>
        private List<BaseRevealGridItem> _gridItems;

        /// <summary>
        /// Show DoTween sequence
        /// </summary>
        private Sequence _sequence;

        public override bool Initialize()
        {
            _gridItems = CacheGridItemsToReveal.GetRevealGridItems();

            // Disable the skip button at the initialization
            SkipButton?.gameObject.SetActive(false);

            return true;
        }

        protected override void Begin()
        {
            // Run over all items and set his parent to the grid item
            foreach (BaseRevealGridItem baseRevealGridItem in _gridItems)
            {
                baseRevealGridItem.gameObject.SetActive(true);
                baseRevealGridItem.transform.SetParent(SummaryGridLayoutGroup.transform);
                baseRevealGridItem.transform.SetScaleToZero();
            }

            // Enable the skip button when the behavior starts
            SkipButton?.gameObject.SetActive(true);
        }

        protected override void Execution()
        {
            _sequence = DOTween.Sequence();
            // Run over all the grid items and set the show animation
            foreach (BaseRevealGridItem baseRevealGridItem in _gridItems)
            {
                _sequence.Append(baseRevealGridItem.transform.DOScale(1, SingleItemScaleAnimationTime).SetEase(SingleItemEaseAnimation));
                _sequence.SetDelay(AnimationDelayBetweenItems);
            }

            // End the execution when the sequence is completed
            _sequence.onComplete += EndExecution;

            // Play the sequence
            _sequence.Play();
        }

        protected override void End()
        {
            // Disable the skip button at the end of the behavior
            SkipButton?.gameObject.SetActive(false);
        }

        /// <summary>
        /// Button behavior to skip this animation
        /// </summary>
        public void OnSkipClick()
        {
            _sequence.Complete();
        }
    }
}