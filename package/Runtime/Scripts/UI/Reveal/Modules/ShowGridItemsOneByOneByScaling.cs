﻿using System.Collections.Generic;
using BigfootSdk.Extensions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk
{
    public class ShowGridItemsOneByOneByScaling : BaseSequentialModularBehavior
    {
        /// <summary>
        /// The transform where all the grid items are contained
        /// </summary>
        public RectTransform SingleGridItemContainer;

        /// <summary>
        /// The speed animation in which each individual grid item ar shown
        /// </summary>
        public float AnimationSpeed = 0.6f;

        /// <summary>
        /// The scale up ease animation
        /// </summary>
        public Ease InEaseAnimation = Ease.Linear;

        /// <summary>
        /// The scale down ease animation
        /// </summary>
        public Ease OutEaseAnimation = Ease.Linear;

        /// <summary>
        /// The inspector button reference to skip this behavior
        /// </summary>
        public Button SkipButton;

        /// <summary>
        /// The continue inspector button reference
        /// </summary>
        public Button ContinueButton;

        /// <summary>
        /// Cache grid items to reveal instance reference
        /// </summary>
        public CacheGridItemsToReveal CacheGridItemsToReveal;

        /// <summary>
        /// Queue containing all the grid items to show
        /// </summary>
        private Queue<BaseRevealGridItem> _gridItemsQueue;

        /// <summary>
        /// The current dequeued grid item
        /// </summary>
        private BaseRevealGridItem _currentSelectedGridItem;

        /// <summary>
        /// Hide DoTween sequence
        /// </summary>
        private Sequence _sequence;

        public override bool Initialize()
        {
            // Populate the queue based on the grid items to show
            List<BaseRevealGridItem> gridItems = CacheGridItemsToReveal.GetRevealGridItems();

            // Create a new queue with the grid items
            _gridItemsQueue = new Queue<BaseRevealGridItem>(gridItems);

            // Disable the skip button at the initialization
            SkipButton?.gameObject.SetActive(false);

            // Disable the continue button at the initialization
            ContinueButton?.gameObject.SetActive(false);

            return true;
        }

        protected override void Begin()
        {
            // Enable all the items and set their scale to 0 and disable it
            foreach (BaseRevealGridItem gridItem in _gridItemsQueue)
            {
                // Set the grid item to the correct parent and set his anchor and position
                RectTransform rectTransform = gridItem.GetComponent<RectTransform>();
                rectTransform.SetParent(SingleGridItemContainer);
                rectTransform.SetAnchor(AnchorPresets.MiddleCenter);
                rectTransform.anchoredPosition = Vector2.zero;

                gridItem.transform.SetScaleToZero();
                gridItem.gameObject.SetActive(false);
            }

            // Enable the skip button when the behavior starts
            SkipButton?.gameObject.SetActive(true);

            //  Enable the continue button when the behavior starts
            ContinueButton?.gameObject.SetActive(true);
        }

        protected override void Execution()
        {
            _sequence = DOTween.Sequence();

            // Loop every grid items and set his animation
            while (_gridItemsQueue != null && _gridItemsQueue.Count > 0)
            {
                // Select a grid item from the queue
                BaseRevealGridItem gridItem = _gridItemsQueue.Dequeue();

                // Enable grid item
                _sequence.AppendCallback(() => { gridItem.gameObject.SetActive(true); });
                // Scale up animation
                _sequence.Append(gridItem.transform.DOScale(1, AnimationSpeed).SetEase(InEaseAnimation));
                // Enable the continue button interaction and pause the sequence
                _sequence.AppendCallback(() => { _sequence.Pause(); });
                // Scale down animation
                _sequence.Append(gridItem.transform.DOScale(0, AnimationSpeed).SetEase(OutEaseAnimation));
                // Disable grid item
                _sequence.AppendCallback(() => { gridItem.gameObject.SetActive(false); });
            }

            // End the execution when the sequence is completed
            _sequence.onComplete += EndExecution;

            // Play the sequence
            _sequence.Play();
        }

        protected override void End()
        {
            // Disable the skip button at the end of the behavior
            SkipButton?.gameObject.SetActive(false);

            // Disable the continue button at the end of the behavior
            ContinueButton?.gameObject.SetActive(false);
        }

        /// <summary>
        /// Button behavior to show the next item
        /// </summary>
        public void OnShowNextClick()
        {
            if (_sequence == null)
            {
                return;
            }

            if (!_sequence.IsActive())
            {
                return;
            }

            if (_sequence.IsPlaying())
            {
                return;
            }

            _sequence.Play();
        }

        /// <summary>
        /// Button behavior to skip this animation
        /// </summary>
        public void OnSkipClick()
        {
            _sequence?.Complete();
        }
    }
}