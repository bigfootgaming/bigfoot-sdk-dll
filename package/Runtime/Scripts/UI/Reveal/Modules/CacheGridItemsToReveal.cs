﻿using System;
using System.Collections.Generic;
using BigfootSdk.Helpers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk
{
    public class CacheGridItemsToReveal : BaseSequentialModularBehavior
    {
        /// <summary>
        /// The grid item prefabs
        /// </summary>
        public Transform DefaultContainer;

        /// <summary>
        /// The name of the prefab in the addressable assets
        /// </summary>
        public string AddressablePrefabName;

        /// <summary>
        /// The reveal dialog instance
        /// </summary>
        public RevealPanel RevealPanel;

        /// <summary>
        /// Cached all grid items (all historical items instantiated, so if there is an item already in the list, there is no need to create a new grid item for it in the hierarchy)
        /// </summary>
        private Dictionary<string, BaseRevealGridItem> _allRevealGridItemsCache;

        /// <summary>
        /// Cache the grid items to use in this reveal instance
        /// </summary>
        private Dictionary<string, BaseRevealGridItem> _gridItemsToUse;

        public override bool Initialize()
        {
            if (DefaultContainer == null)
            {
                LogHelper.LogError("Unable to initialize BaseSequentialModularBehavior. Missing inspector reference for the default grid item container", Environment.StackTrace);
                return false;
            }

            // Be sure that the default container is disable to hide the item
            DefaultContainer.gameObject.SetActive(false);

            if (RevealPanel == null)
            {
                LogHelper.LogError("Unable to initialize BaseSequentialModularBehavior. Missing inspector reference for the RevealPanel", Environment.StackTrace);
                return false;
            }

            if (!RevealPanel.GetCachedRevealEntries(out List<RevealEntryModel> revealEntries))
            {
                LogHelper.LogError("Unable to initialize BaseSequentialModularBehavior. Fail tyring to get cached reveal entries", Environment.StackTrace);
                return false;
            }

            if (_allRevealGridItemsCache == null)
            {
                _allRevealGridItemsCache = new Dictionary<string, BaseRevealGridItem>();
            }

            // Reset the list of grid items
            _gridItemsToUse = new Dictionary<string, BaseRevealGridItem>();

            // Take each entry and create a grid item and then cache each one
            foreach (RevealEntryModel revealEntry in revealEntries)
            {
                if (!_allRevealGridItemsCache.TryGetValue(revealEntry.Id, out BaseRevealGridItem revealGridItem))
                {
                    GameObject gridItem = RevealPanel.Pool.GetObjectForType(AddressablePrefabName, DefaultContainer);

                    if (gridItem == null)
                    {
                        LogHelper.LogWarning($"Unable to cache reveal grid item. Fail trying to instantiate grid item prefab '{AddressablePrefabName}'");
                        continue;
                    }

                    revealGridItem = gridItem.GetComponent<BaseRevealGridItem>();

                    if (revealGridItem == null)
                    {
                        LogHelper.LogWarning("Unable to cache reveal grid item. RevealGridItem component not found in the prefab");
                        Destroy(gridItem);
                        continue;
                    }

                    _allRevealGridItemsCache.Add(revealEntry.Id, revealGridItem);
                }

                _gridItemsToUse.Add(revealEntry.Id, revealGridItem);

                revealGridItem.Populate(revealEntry.Id, revealEntry.Amount);
            }

            return true;
        }

        protected override void Begin()
        {
            // Set all grid items again to the default container
            SetGridItemsIntoDefaultContainer();
        }

        protected override void Execution()
        {
            EndExecution();
        }

        protected override void End()
        {
        }

        /// <summary>
        /// Get all grid items
        /// </summary>
        /// <returns></returns>
        public List<BaseRevealGridItem> GetRevealGridItems()
        {
            return new List<BaseRevealGridItem>(_gridItemsToUse.Values);
        }

        /// <summary>
        /// Set all the grid items into the default container
        /// </summary>
        public void SetGridItemsIntoDefaultContainer()
        {
            if (DefaultContainer == null)
            {
                LogHelper.LogWarning("Unable to set all grid items into the default container. Null inspector reference");
                return;
            }

            if (_allRevealGridItemsCache == null)
            {
                LogHelper.LogWarning("Unable to set all grid items into the default container. Null grid items list");
                return;
            }

            foreach (BaseRevealGridItem gridItem in _allRevealGridItemsCache.Values)
            {
                if (gridItem == null)
                {
                    LogHelper.LogWarning("Unable to set all grid item into the default container. Null listed grid item");
                    continue;
                }

                gridItem.transform.SetParent(DefaultContainer);
            }
        }
    }
}