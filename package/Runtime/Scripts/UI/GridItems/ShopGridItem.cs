﻿using System;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.Sprites;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk.Shop
{
    public class ShopGridItem : MonoBehaviour
    {
        /// <summary>
        /// Callback for successful purchase
        /// </summary>
        public Action<ShopItemModel,TransactionResultModel> SuccessfulPurchase; 
        
        /// <summary>
        /// Callback for unsuccessful purchase
        /// </summary>
        public Action<ShopItemModel,string> UnsuccessfulPurchase;

        /// <summary>
        /// What origin to send to analytics
        /// </summary>
        public string AnalyticsOrigin;
        
        /// <summary>
        /// The formatter to use
        /// </summary>
        public string NumberFormatterName = FormatHelper.DEFAULT_FORMATTER;

        /// <summary>
        /// Label for the title
        /// </summary>
        public TextMeshProUGUI TitleLabel;

        /// <summary>
        /// Label for the amount
        /// </summary>
        public TextMeshProUGUI AmountLabel;
        
        /// <summary>
        /// Label for the cost
        /// </summary>
        public TextMeshProUGUI CostLabel;

        /// <summary>
        /// The image for this item
        /// </summary>
        public Image ItemImage;
        
        /// <summary>
        /// The timer for this ShopItem
        /// </summary>
        public GameObject Timer;

        /// <summary>
        /// The timer label
        /// </summary>
        public TimerLabel TimerLabel;
        
        /// <summary>
        /// Cached reference of the shop item model
        /// </summary>
        protected ShopItemModel _shopItemModel;

        /// <summary>
        /// Cached reference of the base item model
        /// </summary>
        protected BaseItemModel _baseItemModel;

        /// <summary>
        /// The stock of the item
        /// </summary>
        protected PlayerShopItemModel _playerShopItemModel;
        
        private RectTransform _rectTransform;

        public RectTransform RT
        {
            get => _rectTransform;
            set => _rectTransform = value;
        }

        protected virtual void OnEnable()
        {
            Localization.Localization.onLocalize += SetTitleLabel;
        }
    
        protected virtual void OnDisable()
        {
            Localization.Localization.onLocalize-= SetTitleLabel;
        }
        
        /// <summary>
        /// Initialize the grid item
        /// </summary>
        /// <param name="itemModel"></param>
        public virtual void InitializeShopGridItem(ShopItemModel itemModel)
        {
            if (_rectTransform == null)
                _rectTransform = GetComponent<RectTransform>();
            _shopItemModel = itemModel;
            if (!string.IsNullOrEmpty(_shopItemModel.ItemName))
                _baseItemModel = ItemsManager.Instance.GetItem(_shopItemModel.ItemName);
            if (_baseItemModel != null)
                _playerShopItemModel = ShopManager.Instance.GetPlayerShopItemModel(itemModel.Id);
            if (_playerShopItemModel == null && _shopItemModel.Amount > 0)
                _playerShopItemModel = ShopManager.Instance.AddPlayerShopItemModel(_shopItemModel.Id, _shopItemModel.Amount);
            
            SetTitleLabel();
            SetItemImage();
            InitializeAmount();
            InitializeCost();
            InitializeTimer();
        }
        
        protected virtual void SetItemImage()
        {
            if (ItemImage == null) return;

            SpriteManager.Instance.GetMappedSprite(string.Format("ShopIcon_{0}", _shopItemModel.Id), sprite =>
            {
                ItemImage.sprite = sprite;
            });
        }

        protected virtual void SetTitleLabel()
        {
            if (TitleLabel == null)
                return;

            TitleLabel.text = Localization.Localization.Get(string.Format("ShopItemTitle_{0}", _shopItemModel.Id));
        }

        protected virtual void InitializeAmount()
        {
            if (AmountLabel != null)
            {
                if (_playerShopItemModel != null)
                {
                    AmountLabel.text = _playerShopItemModel.Amount.ToString();
                }
                else
                {
                    AmountLabel.text = "";
                }
            }
        }

        protected virtual void InitializeCost()
        {
            if (CostLabel != null && _shopItemModel != null)
            {
                CostLabel.text = FormatHelper.Instance.FormatCost(_shopItemModel.Cost);
            }
        }

        /// <summary>
        /// See if there is any timer that needs initialization
        /// </summary>
        protected virtual void InitializeTimer()
        {
            if (Timer != null)
            {
                Timer.SetActive(false);
                if (_shopItemModel.Timer != null)
                {
                    var timeRemaining = _shopItemModel.Timer.GetRemainingTime();
                    if (timeRemaining > 0)
                    {
                        Timer.SetActive(true);
                        TimerLabel.StartTimer(timeRemaining, EndedTimer);
                    }
                }
            }
        }


        /// <summary>
        /// Callback for when the timer ended
        /// </summary>
        protected virtual void EndedTimer()
        {
            Timer.SetActive(false);
        }
        
        /// <summary>
        /// Initialize the purchase of an item
        /// </summary>
        public virtual void Purchase()
        {
            if (_playerShopItemModel == null || _playerShopItemModel.Amount > 0)
            {
                // If it's free, just purchase it
                if (_shopItemModel.Cost == null)
                {
                    DoPurchase();
                    return;
                }

                // If we have enough
                if (_shopItemModel.Cost.HasEnough())
                {
                    // This will trigger the shop iap popup
                    if (_shopItemModel.Cost.IsIAP())
                    {
                        DoPurchase();
                    }
                    else
                    {
                        ShowConfirmationPopup();
                    }
                }
                else
                {
                    ShowNotEnoughCurrency();
                }
            }
        }

        /// <summary>
        /// Do the actual purchase
        /// </summary>
        protected virtual void DoPurchase()
        {
            ShopManager.Instance.PurchaseShopItem(_shopItemModel,
                AnalyticsOrigin,
                model =>
                {
                    LogHelper.LogSdk("Successfully purchased shop item: " + model);

                    if (_playerShopItemModel != null)
                    {
                        ShopManager.Instance.UpdatePlayerShopItemModel(_playerShopItemModel.Id);
                    }

                    ShopManager.Instance.UpdateEnabledShop();

                    // Handle the successful transaction 
                    SuccessfulPurchase?.Invoke(_shopItemModel, model);
                },
                error => {
                    LogHelper.LogSdk(string.Format("Error {0} when trying to purchase {1}", error, _shopItemModel));
                        
                    UnsuccessfulPurchase?.Invoke(_shopItemModel,error);
                });
        }

        /// <summary>
        /// Method to show the confirmation popup if we want to
        /// </summary>
        protected virtual void ShowConfirmationPopup()
        {
            // Just do the purchase here.
            DoPurchase();
        }

        /// <summary>
        /// Shows an indicator that you dont have enough currency
        /// </summary>
        protected virtual void ShowNotEnoughCurrency()
        {
            LogHelper.LogSdk("Not enough currrency to purchase " + _baseItemModel.ItemName);    
        }
    }
}


