﻿using System;
using BigfootSdk;
using BigfootSdk.Backend;
using BigfootSdk.Calendar;
using BigfootSdk.Helpers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using LayoutGroup = BigfootSdk.Layout.LayoutGroup;

public class CalendarCourseGridItem : MonoBehaviour
{
    /// <summary>
    /// LayoutGroup to Populate Rewards
    /// </summary>
    public LayoutGroup LayoutGroup;
    
    /// <summary>
    /// F2P CalendarCompletion Reward Label
    /// </summary>
    public TextMeshProUGUI BaseCompletionRewardLabel;

    /// <summary>
    /// Claims Completion Reward
    /// </summary>
    public Button ClaimCompletionRewardBtn;

    /// <summary>
    /// Label for Claim Completion Reward Button
    /// </summary>
    public TextMeshProUGUI ClaimCompletionRewardLabel;
    
    /// <summary>
    /// VIP CalendarCompletion Reward Label
    /// </summary>
    public TextMeshProUGUI VipCompletionRewardLabel;
    
    /// <summary>
    /// Reward Item Prefab
    /// </summary>
    public GameObject CalendarRewardGridItemPrefab;

    /// <summary>
    /// Footer with Completion Rewards Info
    /// </summary>
    public GameObject CompletionRewardParent;

    protected CalendarCourseModel _model;
    protected CalendarManager _calendarManager;
    
    private void OnEnable()
    {
        CalendarManager.OnCalendarDayClaimed += OnCalendarDayClaimed;
        CalendarManager.OnCourseRewardClaimed += OnCourseRewardClaimed;
    }
    private void OnDisable()
    {
        CalendarManager.OnCalendarDayClaimed -= OnCalendarDayClaimed;
        CalendarManager.OnCourseRewardClaimed -= OnCourseRewardClaimed;
    }
    public virtual void PopulateCourse(CalendarCourseModel courseModel, int courseNumer)
    {
        _calendarManager = CalendarManager.Instance;
        _model = courseModel;
        
        if (LayoutGroup != null && CalendarRewardGridItemPrefab != null)
        {
            // If we have vip Reward, check that are even with base rewards
            if (_model.VipRewards != null && _model.VipRewards.Count > 0)
            {
                if (_model.BaseRewards.Count != _model.VipRewards.Count)
                {
                    LogHelper.LogWarning("Calendar Rewards are not even");
                    return;
                }
            }
                
            
            //Iterate and instantiate each course reward
            for (int i = 0; i < _model.BaseRewards.Count; i++)
            {
                int calendarDay = i + (_model.BaseRewards.Count * courseNumer);
                
                GameObject gridItem = Instantiate(CalendarRewardGridItemPrefab, Vector3.zero, Quaternion.identity, LayoutGroup.transform);

                CalendarRewardGridItem rewardGridItem = gridItem.GetComponent<CalendarRewardGridItem>();

                if (rewardGridItem != null)
                {
                    rewardGridItem.PopulateCalendarGridItem(_model.BaseRewards[i], _model.VipRewards != null ? _model.VipRewards[i] : null, calendarDay);
                }
            }

            // Check if we have a completion Reward and populate it
            if (CompletionRewardParent != null)
            {
                CompletionRewardParent.SetActive(_model.HasCompletionRewards);
            
                if (_model.HasCompletionRewards)
                {
                    if (BaseCompletionRewardLabel != null)
                    {
                        if (_model.CompletionBaseRewards is CurrencyTransaction transaction)
                        {
                            BaseCompletionRewardLabel.text = string.Format("<sprite name=\"{0}\">{1}", transaction.CurrencyKey, transaction.Amount);
                        }
                        else if (_model.CompletionBaseRewards is CompositeTransaction compositeTransaction)
                        {
                            foreach (ITransaction reward in compositeTransaction.Transactions)
                            {
                                if (reward is CurrencyTransaction rewardTransaction)
                                    BaseCompletionRewardLabel.text = string.Format("<sprite name=\"{0}\">{1}", rewardTransaction.CurrencyKey, rewardTransaction.Amount);
                            }
                        }
                    }
                    

                    if (_model.CompletionVipRewards != null)
                    {
                        if (VipCompletionRewardLabel != null)
                        {
                            if (_model.CompletionVipRewards is CurrencyTransaction transaction)
                            {
                                VipCompletionRewardLabel.text = string.Format("<sprite name=\"{0}\">{1}", transaction.CurrencyKey, transaction.Amount);
                            }
                            else if (_model.CompletionVipRewards is CompositeTransaction compositeTransaction)
                            {
                                foreach (ITransaction reward in compositeTransaction.Transactions)
                                {
                                    if (reward is CurrencyTransaction rewardTransaction)
                                        VipCompletionRewardLabel.text = string.Format("<sprite name=\"{0}\">{1}", rewardTransaction.CurrencyKey, rewardTransaction.Amount);
                                }
                            }
                        }
                    }
                    else
                    {
                        VipCompletionRewardLabel.gameObject.SetActive(false);
                    }
                }

                UpdateCompletionRewardBtn();

            }
            
            // Rebuild Layout
            LayoutGroup.Rebuild();
        }
    }
    
    void UpdateCompletionRewardBtn()
    {
        if (_calendarManager.IsCourseClaimed(_model.Id))
        {
            ClaimCompletionRewardBtn.gameObject.SetActive(true);
            ClaimCompletionRewardBtn.interactable = false;
            ClaimCompletionRewardLabel.text = "Reward Claimed";
        }
        else if (_calendarManager.IsCourseCompleted(_model))
        {
            ClaimCompletionRewardBtn.gameObject.SetActive(true);
            ClaimCompletionRewardBtn.interactable = false;
            ClaimCompletionRewardLabel.text = "Reward Missed";
        }
        else
        {
            ClaimCompletionRewardBtn.gameObject.SetActive(_calendarManager.IsCourseClaimeable(_model));
        }
    }
    
    private void OnCourseRewardClaimed(int courseIndex)
    {
        if (courseIndex == _model.Id)
            UpdateCompletionRewardBtn();
    }
    private void OnCalendarDayClaimed(int dayIndex)
    {
        UpdateCompletionRewardBtn();
    }
    
    public void ClaimCompletionReward()
    {
        _model.CompletionBaseRewards.ApplyTransaction(string.Format("CalendarCourseReward_{0}", _model.Id));
        
        if (PlayerManager.Instance.GetFlag("VIP_CALENDAR", GameModeManager.CurrentGameMode, false))
        {
            if (_model.CompletionVipRewards != null)
                _model.CompletionVipRewards.ApplyTransaction(string.Format("VIP_CalendarCourseReward_{0}", _model.Id));
        }
        
        // Persist Reward has been claimed
        _calendarManager.CourseRewardClaimed(_model.Id);
    }
}
