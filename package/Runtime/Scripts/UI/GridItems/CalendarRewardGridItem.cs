﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using BigfootSdk;
using BigfootSdk.Backend;
using BigfootSdk.Calendar;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CalendarRewardGridItem : MonoBehaviour
{
    /// <summary>
    /// Button to claim reward
    /// </summary>
    public Button ClaimRewardButton;
    
    /// <summary>
    /// F2P Reward Label
    /// </summary>
    public TextMeshProUGUI BaseRewardLabel;
    
    /// <summary>
    /// Vip Reward Label
    /// </summary>
    public TextMeshProUGUI VipRewardLabel;

    public GameObject ClaimedOverlay;

    public GameObject MissedOverlay;
    
    /// <summary>
    /// Cached reference to CalendarManager
    /// </summary>
    protected CalendarManager _calendarManager;

    private PlayerManager _playerManager;
    /// <summary>
    /// The reward Transactions
    /// </summary>
    protected ITransaction _baseReward;
    protected ITransaction _vipReward;

    protected int _calendarDay;
    
    public virtual void PopulateCalendarGridItem(ITransaction baseReward, ITransaction vipReward, int calendarDay)
    {
        _baseReward = baseReward;
        _vipReward = vipReward;
        _calendarDay = calendarDay;
        
        _calendarManager = CalendarManager.Instance;
        _playerManager = PlayerManager.Instance;

        PopulateRewardsFields();
        
        ClaimRewardButton.onClick.AddListener(ClaimReward);
    }

    private void OnEnable()
    {
        CalendarManager.OnCalendarDayClaimed += OnCalendarDayClaimed;
    }

    private void OnDisable()
    {
        CalendarManager.OnCalendarDayClaimed -= OnCalendarDayClaimed;
    }
    
    private void OnCalendarDayClaimed(int dayIndex)
    {
        if (dayIndex == _calendarDay)
        {
            ClaimedOverlay.SetActive(true);
            ClaimRewardButton.interactable = false;
        }
    }

    /// <summary>
    /// Populate Button Fields with Reward Icons and text
    /// </summary>
    protected virtual void PopulateRewardsFields()
    {
        ClaimedOverlay.SetActive(_calendarManager.IsDayClaimed(_calendarDay));
        MissedOverlay.SetActive(_calendarManager.IsDayMissed(_calendarDay));
        
        ClaimRewardButton.interactable = _calendarManager.ActiveDay == _calendarDay;
        
        if (_baseReward is CurrencyTransaction baseRewardTransaction)
        {
            BaseRewardLabel.text = string.Format("<sprite name=\"{0}\">{1}", baseRewardTransaction.CurrencyKey, baseRewardTransaction.Amount);
        }

        if (_vipReward != null)
        {
            if (_vipReward is CurrencyTransaction vipRewardTransaction)
            {
                VipRewardLabel.text = string.Format("<sprite name=\"{0}\">{1}", vipRewardTransaction.CurrencyKey, vipRewardTransaction.Amount);
            }
        }
        else
        {
            VipRewardLabel.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Apply Reward and persist data
    /// </summary>
    protected virtual void ClaimReward()
    {
        _baseReward.ApplyTransaction(string.Format("CalendarReward_{0}", _calendarDay));

        if (_vipReward != null)
        {
            if (_playerManager.GetFlag("VIP_CALENDAR", GameModeManager.CurrentGameMode, false))
            {
                _vipReward.ApplyTransaction(string.Format("VIP_CalendarReward_{0}", _calendarDay));
            }
        }
        
        // Persist Reward has been claimed
        _calendarManager.RewardClaimed(_calendarDay);
    }
    
}
