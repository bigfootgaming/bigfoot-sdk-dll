﻿
using System.Threading.Tasks;
using BigfootSdk.Layout;

namespace BigfootSdk.Shop
{
    /// <summary>
    /// Dialog that contains all of the shops elements
    /// </summary>
    public class ShopDialog : ShopPlacement
    {
        /// <summary>
        /// Reference to the pool containing the sections and grid items
        /// </summary>
        public ObjectPool Pool;

        /// <summary>
        /// Vertical layout containing all offers
        /// </summary>
        public VerticalLayoutGroup Layout;

        
        protected virtual void OnEnable()
        {
            DependencyManager.OnEverythingLoaded += Populate;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            DependencyManager.OnEverythingLoaded -= Populate;
        }


        void Populate()
        {
            Populate(Pool);
        }

        public override async Task Refresh()
        {
            await base.Refresh();
            Layout.Rebuild();
        }
    }
}