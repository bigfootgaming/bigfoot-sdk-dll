﻿using BigfootSdk.Backend;
using BigfootSdk.Panels;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Class to display the RateUs Dialog
    /// </summary>
    public class RateUsDialog : MonoBehaviour
    {
        /// <summary>
        /// Cached reference to the RateUsManager
        /// </summary>
        protected RateUsManager _rateUsManager;
        
        /// <summary>
        /// Initialize method
        /// </summary>
        public virtual void InitializeRateUs()
        {
            _rateUsManager = RateUsManager.Instance;
        }

        /// <summary>
        /// Rate the game
        /// </summary>
        public virtual void RateAction()
        {
            // Update the value
            _rateUsManager.SetRateStatus(RateUsConstants.RATED);
            
            // Close the rate us panel
            PanelsEvents.Back();
            
            // Go to the store
            #if UNITY_ANDROID
                Application.OpenURL(TitleManager.Instance.GetFromTitleData("android_url"));
            #elif UNITY_IOS
                Application.OpenURL(TitleManager.Instance.GetFromTitleData("ios_url"));
            #endif
        }

        /// <summary>
        /// Remind me later
        /// </summary>
        public virtual void RemindLaterAction()
        {
            // Close the rate us panel
            PanelsEvents.Back();
        }

        /// <summary>
        /// Dont want to rate
        /// </summary>
        public virtual void IgnoreAction()
        {
            // Update the value
            _rateUsManager.SetRateStatus(RateUsConstants.DONT_ASK_AGAIN);
            
            // Close the rate us panel
            PanelsEvents.Back();
        }
    }
}