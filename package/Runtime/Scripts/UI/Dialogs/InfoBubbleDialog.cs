﻿using DG.Tweening;
using TMPro;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Dialog to show an info bubble
    /// </summary>
    public class InfoBubbleDialog : MonoBehaviour
    {
        /// <summary>
        /// The label to show the text
        /// </summary>
        public TextMeshProUGUI Label;

        /// <summary>
        /// How long should this notification stay for
        /// </summary>
        public float StayDuration;
        
        /// <summary>
        /// The scale duration
        /// </summary>
        public float ScaleDuration;

        /// <summary>
        /// The in scale Ease
        /// </summary>
        public Ease InScaleEase;

        /// <summary>
        /// The out scale Ease
        /// </summary>
        public Ease OutScaleEase;
        
        /// <summary>
        /// Cached reference for the InfoBubbleModel
        /// </summary>
        private InfoBubbleModel _bubbleModel;

        /// <summary>
        /// Reference for the InfoBubbleModel
        /// </summary>
        public InfoBubbleModel BubbleModel => _bubbleModel;
        
        /// <summary>
        /// Trigger a notification
        /// </summary>
        /// <param name="bubbleModel">The model for this bubble</param>
        /// <param name="offset">The position of the notification</param>
        /// <param name="textToShow">The text to show</param>
        public virtual void TriggerNotification(InfoBubbleModel bubbleModel, Vector3 offset, string textToShow)
        {
            _bubbleModel = bubbleModel;
            
            // Set the position
            transform.localPosition = offset;
            
            // Populate the label
            if (Label != null)
                Label.text = textToShow;
            
            // Show the notification
            ShowNotification();
        }

        /// <summary>
        /// Method to show the notification
        /// </summary>
        protected virtual void ShowNotification()
        {
            transform.gameObject.SetActive(true);
            transform.DOScale(1, ScaleDuration).SetUpdate(true).SetEase(InScaleEase).onComplete += () =>  {
                Invoke("HideNotification", StayDuration);
            };
        }

        /// <summary>
        /// Method to hide the notification
        /// </summary>
        public virtual void HideNotification()
        {
            transform.DOScale(0, ScaleDuration).SetUpdate(true).SetEase(OutScaleEase).onComplete += () =>  {
             InfoBubbleManager.Instance.RemoveFromShowing(this);   
            };

        }
    }
}
