﻿using System;
using System.Collections;
using UnityEngine;
using BigfootSdk.Backend;
using BigfootSdk.Layout;
using TMPro;

namespace BigfootSdk.Calendar
{
    public class CalendarDialog : MonoBehaviour
    {
        /// <summary>
        /// The Layout Group For Courses
        /// </summary>
        public LayoutGroup LayoutGroup;

        public TextMeshProUGUI NextClaimLabel;
        
        /// <summary>
        /// Course Prefab
        /// </summary>
        public GameObject CalendarCourseGridItemPrefab;
        
        /// <summary>
        /// Cached Reference to CalendarManager
        /// </summary>
        protected CalendarManager _calendarManager;

        protected bool _initialized = false;
        private bool _panelActive = false;
        private TimeSpan _nextRewardTime;
        
        protected void OnEnable()
        {
            _panelActive = true;
            
            CalendarManager.OnCalendarDayClaimed += OnCalendarDayClaimed;
            CalendarManager.OnCalendarCompletedEvent += OnCalendarCompleted;
        }

        private void OnDisable()
        {
            _panelActive = false;
            
            CalendarManager.OnCalendarDayClaimed -= OnCalendarDayClaimed;
            CalendarManager.OnCalendarCompletedEvent -= OnCalendarCompleted;
            StopCoroutine(UpdateNextRewardLabel());
        }

        public void Initialize()
        {
            if (!_initialized)
            {
                PopulateCalendarCourses();
            }
            
            _nextRewardTime = _calendarManager.GetTimeUntilNextReward();
            StartCoroutine(UpdateNextRewardLabel());
        }
        
        protected virtual void PopulateCalendarCourses()
        {
            if (_calendarManager == null)
                _calendarManager = CalendarManager.Instance;
            
            if (LayoutGroup != null && CalendarCourseGridItemPrefab != null)
            {
                for (int i = 0; i < _calendarManager.GetActiveCalendarCourses().Count; i++)
                {
                    // Intantiate and Populate Each Course
                    GameObject gridItem = Instantiate(CalendarCourseGridItemPrefab, Vector3.zero, Quaternion.identity, LayoutGroup.transform);

                    CalendarCourseGridItem courseGridItem = gridItem.GetComponent<CalendarCourseGridItem>();

                    if (courseGridItem != null)
                    {
                        courseGridItem.PopulateCourse(_calendarManager.GetActiveCalendarCourses()[i], i);
                    }
                }

                // Rebuild Layout
                LayoutGroup.Rebuild();
            }
            
            _initialized = true;
        }

        IEnumerator UpdateNextRewardLabel()
        {
            var wait = new WaitForSeconds(1);

            while (_panelActive)
            {
                _nextRewardTime = _nextRewardTime.Subtract(new TimeSpan(0, 0, 1));
                
                if (_nextRewardTime.TotalSeconds <= 0)
                    NextClaimLabel.text = "Reward Available";
                else
                    NextClaimLabel.text = string.Format("Next Claim In {0}:{1}:{2}", _nextRewardTime.Hours, _nextRewardTime.Minutes, _nextRewardTime.Seconds);
                
                yield return wait;
            }
        }
        
        private void OnCalendarDayClaimed(int reward)
        {
            _nextRewardTime = _calendarManager.GetTimeUntilNextReward();
        }
        
        private void OnCalendarCompleted(CalendarProgressionModel obj)
        {
            StopCoroutine(UpdateNextRewardLabel());
            NextClaimLabel.text = "Calendar Completed";
        }
    }
}
