﻿using BigfootSdk.Backend;
using UnityEngine;

/// <summary>
/// Class to do a Force Update
/// </summary>
public class ForceUpdateDialog : MonoBehaviour
{
    /// <summary>
    /// Bind this to the button to open up the corresponding store
    /// </summary>
    public void GoToStore()
    {
        #if UNITY_ANDROID
            Application.OpenURL(TitleManager.Instance.GetFromTitleData("android_url"));
        #elif UNITY_IPHONE
            Application.OpenURL(TitleManager.Instance.GetFromTitleData("ios_url"));
        #endif
    }
}
