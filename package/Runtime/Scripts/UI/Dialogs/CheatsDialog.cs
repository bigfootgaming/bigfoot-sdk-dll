﻿using System;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Backend;
using BigfootSdk.Core.Idle;
using BigfootSdk.SceneManagement;
using BigfootSdk.Tutorials;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk
{
    public class CheatsDialog : MonoBehaviour
    {
        public CheatsDialogExtension CheatsExtension;

        public void Initialize()
        {
            InitializeTutorials();

            InitializeResources();
            
            InitializeItems();
            
            InitializeTimeScales();

            CheatsExtension?.Initialize();
        }

        public void CloseCheats()
        {
            CheatsManager.Instance.CloseConsole();
        }
        
        #region Tutorials
        
        [BoxGroup("Tutorials")] 
        public Toggle ReloadSceneOnSkip;
        
        [BoxGroup("Tutorials")]
        public TMP_Dropdown TutorialsDropdowns;

        void InitializeTutorials()
        {
            TutorialsDropdowns.ClearOptions();
            var tutorials = TutorialsManager.Instance.Tutorials.Select(handler => handler.TutorialId).ToList();
            TutorialsDropdowns.AddOptions(tutorials);
        }
        
        public void SkipAllTutorials()
        {
            // Iterate through all of the tutorials
            var tutorials = TutorialsManager.Instance.Tutorials;
            foreach (var tutorial in tutorials)
            {
                // Skip
                TutorialsManager.Instance.SkipTutorial(tutorial.TutorialId);

                // Call the extension
                CheatsExtension?.TutorialSkipped(tutorial);
            }
            
            // Call the extension
            CheatsExtension?.SkipAllTutorials();
            
            // If toggle enabled, reload scene
            if(ReloadSceneOnSkip.isOn)
                BigfootSceneManager.LoadScene(BigfootSceneManager.LastScene);
        }

        public void SkipTutorial()
        {
            // Get the selected tutorial
            var selectedTutorial = TutorialsDropdowns.options[TutorialsDropdowns.value].text;
            if (!string.IsNullOrEmpty(selectedTutorial))
            {
                // Try to get the tutorial
                var tutorialHandler = TutorialsManager.Instance.GetTutorial(selectedTutorial);

                if (tutorialHandler != null)
                {
                    // Skip
                    TutorialsManager.Instance.SkipTutorial(tutorialHandler.TutorialId);

                    // Call the extension
                    CheatsExtension?.TutorialSkipped(tutorialHandler);
                }
                
                // If toggle enabled, reload scene
                if(ReloadSceneOnSkip.isOn)
                    BigfootSceneManager.LoadScene(BigfootSceneManager.LastScene);
            }
        }
        #endregion
        
        #region Resources
        
        [BoxGroup("Resources")]
        public TMP_Dropdown ResourcesDropdowns;
        
        [BoxGroup("Resources")]
        public TMP_InputField ResourcesInputField;

        void InitializeResources()
        {
            var resources = new List<string>() {"hard"};
            
            if(CheatsExtension != null)
                resources.AddRange(CheatsExtension.GetResources());
            
            ResourcesDropdowns.ClearOptions();
            ResourcesDropdowns.AddOptions(resources);
        }

        public void GiveResources()
        {
            Int32.TryParse(ResourcesInputField.text, out int amount);
            if (amount > 0)
            {
                var transaction = new CurrencyTransaction() {CurrencyKey = ResourcesInputField.text, Amount = amount};
                transaction.ApplyTransaction();
            }
        }
        
        #endregion
        
        #region Time Warp

        [BoxGroup("Time Warp")]
        public TMP_InputField TimewarpInputField;

        public void ApplyTimewarp()
        {
            Int32.TryParse(TimewarpInputField.text, out int amount);
            if (amount > 0)
            {
                ResourceGenerationManager.Instance.ApplyTimewarp(amount);
                
                CheatsExtension?.ApplyTimeWarp(amount);
            }
        }
        
        #endregion
        
        #region Items
        
        [BoxGroup("Items")]
        public TMP_Dropdown ItemsDropdowns;
        
        [BoxGroup("Items")]
        public TMP_InputField ItemsInputField;

        void InitializeItems()
        {
            var items = ItemsManager.Instance.GetAllItems().Select(model => model.ItemName).ToList();
            ItemsDropdowns.ClearOptions();
            ItemsDropdowns.AddOptions(items);
        }

        public void GiveItems()
        {
            var selectedItem = ItemsDropdowns.options[ItemsDropdowns.value].text;
            Int32.TryParse(ItemsInputField.text, out int amount);
            if (!string.IsNullOrEmpty(selectedItem) && amount > 0)
            {
                 var transaction = new ItemTransaction() {ItemName = selectedItem, Amount = amount};
                 transaction.ApplyTransaction();
            }
        }
        
        #endregion
        
        #region TimeScale

        [BoxGroup("TimeScales")]
        public TMP_Dropdown TimeScalesDropdown;

        private List<float> _timeScales = new List<float>{0.25f, 0.5f, 1, 2, 4, 8, 16, 32};
        
        void InitializeTimeScales()
        {
            TimeScalesDropdown.ClearOptions();
            TimeScalesDropdown.AddOptions(_timeScales.ConvertAll(new Converter<float, string>(FloatToString)));
        }

        public void ApplyTimeScale()
        {
            Time.timeScale = _timeScales[TimeScalesDropdown.value];
        }

        public void ResetTimeScale()
        {
            Time.timeScale = 1;
        }
        
        public static string FloatToString(float f)
        {
            return f.ToString();
        }
        #endregion
    }
}

public class CheatsDialogExtension : MonoBehaviour
{
    public void Initialize()
    {
        
    }
    
    public virtual void SkipAllTutorials()
    {
        
    }

    public virtual void TutorialSkipped(TutorialHandler tutorial)
    {
        
    }

    public List<string> GetResources()
    {
        return new List<string>();
    }

    public void ApplyTimeWarp(int duration)
    {
        
    }
}