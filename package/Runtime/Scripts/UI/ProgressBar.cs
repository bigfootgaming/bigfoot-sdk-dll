﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class ProgressBar : MonoBehaviour
{
    /// <summary>
    /// Bar showing the production progress
    /// </summary>
    public Slider BarFill;
    /// <summary>
    /// How much we are going to produce in this interval
    /// </summary>
    public TextMeshProUGUI Label;
    /// <summary>
    /// If the fill is animated, the speed
    /// </summary>
    public float AnimationSpeed;
    /// <summary>
    /// Updates the values of the progress bar
    /// </summary>
    /// <param name="current">Time.</param>
    /// <param name="total">Interval.</param>
    /// <param name="animate">Should the fill be animated ?</param>
    public virtual void UpdateValues(float current, float total, bool animate = false, bool independentTimeScale = true, float delay = 0)
    {
        if (BarFill != null)
        {
            if (!animate)
            {
                BarFill.value = current / total;
            }
            else
            {
                BarFill.DOValue(current / total, AnimationSpeed).SetUpdate(independentTimeScale).SetDelay(delay);
            }
        }
    }
    /// <summary>
    /// Updates the values of the progress bar
    /// </summary>
    /// <param name="current">Time.</param>
    /// <param name="total">Interval.</param>
    /// <param name="animate">Should the fill be animated ?</param>
    public virtual void UpdateValues(double current, double total, bool animate = false, bool independentTimeScale = true, float delay = 0)
    {
        if (BarFill != null)
        {
            if (!animate)
            {
                BarFill.value = (float)(current / total);
            }
            else
            {
                BarFill.DOValue((float)current / (float)total, AnimationSpeed).SetUpdate(independentTimeScale).SetDelay(delay);
            }
        }
    }
    /// <summary>
    /// Sets the labels value.
    /// </summary>
    /// <param name="value">Value.</param>
    public void SetLabelValue(string value)
    {
        if (Label != null)
            Label.text = value;
    }
}