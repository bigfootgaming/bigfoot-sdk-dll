﻿using UnityEngine;

namespace BigfootSdk.SafeArea
{
    /// <summary>
    /// Safe area border. To work as expected it is important to set the anchors and pivot 
    /// of the _panel RectTransform and the Content RectTransform properly. See examples.
    /// </summary>
    public class SafeAreaBorder : MonoBehaviour
    {

        /// <summary>
        /// The original size delta.
        /// </summary>
        private Vector2 _originalSizeDelta;
        /// <summary>
        /// The original anchored position.
        /// </summary>
        private Vector2 _originalAnchoredPosition;
        /// <summary>
        /// The rect transform.
        /// </summary>
        private RectTransform _panel;

        /// <summary>
        /// Conform to screen safe area on right (default true, disable to ignore).
        /// </summary>
        [SerializeField] bool Right = true;
        /// <summary>
        /// Conform to screen safe area on left (default true, disable to ignore).
        /// </summary>
        [SerializeField] bool Left = true;
        /// <summary>
        /// Conform to screen safe area on up (default true, disable to ignore).
        /// </summary>
        [SerializeField] bool Up = true;
        /// <summary>
        /// Conform to screen safe area on down (default true, disable to ignore).
        /// </summary>
        [SerializeField] bool Down = true;

        /// <summary>
        /// The original size delta.
        /// </summary>
        private Vector2 _originalContentSizeDelta;
        /// <summary>
        /// The original anchored position.
        /// </summary>
        private Vector2 _originalContentAnchoredPosition;

        /// <summary>
        /// The content of the border bar. It will respect the safe area. It is important to set properly the pivot of this rect transform.
        /// </summary>
        public RectTransform SafeAreaContent;

        private void Awake()
        {
            _panel = GetComponent<RectTransform>();
            _originalSizeDelta = _panel.sizeDelta;
            _originalAnchoredPosition = _panel.anchoredPosition;
            _originalContentSizeDelta = SafeAreaContent.sizeDelta;
            _originalContentAnchoredPosition = SafeAreaContent.anchoredPosition;
        }

        private void OnEnable()
        {
            SafeArea.OnUpdate += Refresh;
        }

        private void OnDisable()
        {
            SafeArea.OnUpdate -= Refresh;
        }

        private void Start()
        {
            Refresh();
        }

        /// <summary>
        /// Called when safe area is updated.
        /// </summary>
        void Refresh()
        {
            float extraHeight = 0;
            float offsetPosY = 0;
            float extraWidth = 0;
            float offsetPosX = 0;
            float contentOffsetX = 0;
            float contentOffsetY = 0;
            float value = (Screen.height - SafeArea.CurrentSA.height - SafeArea.CurrentSA.y);
            if (Up)
            {
                extraHeight += value;
                offsetPosY += value;
                contentOffsetY -= value;
            }
            value = SafeArea.CurrentSA.y;


            if (Down)
            {
                extraHeight += value;
                offsetPosY -= value;
                contentOffsetY += value;
            }
            value = (Screen.width - SafeArea.CurrentSA.width - SafeArea.CurrentSA.x);


            if (Right)
            {
                extraWidth += value;
                offsetPosX += value;
                contentOffsetX -= value;
            }
            value = SafeArea.CurrentSA.x;


            if (Left)
            {
                extraWidth += value;
                offsetPosX -= value;
                contentOffsetX += value;
            }

            if (Left & Right)
            {
                offsetPosX *= 0.5f;
                contentOffsetX *= 0.5f;
            }
            else
            {
                contentOffsetX = 0;
            }

            if (Up & Down)
            {
                offsetPosY *= 0.5f;
                contentOffsetY *= 0.5f;
            }
            else
            {
                contentOffsetY = 0;
            }

            _panel.sizeDelta = new Vector2(_originalSizeDelta.x + extraWidth, _originalSizeDelta.y + extraHeight);
            _panel.anchoredPosition = new Vector2(_originalAnchoredPosition.x + offsetPosX, _originalAnchoredPosition.y + offsetPosY);

            SafeAreaContent.sizeDelta = new Vector2(_originalContentSizeDelta.x - extraWidth, _originalContentSizeDelta.y - extraHeight);

            SafeAreaContent.anchoredPosition = new Vector2(_originalContentAnchoredPosition.x + contentOffsetX, _originalContentAnchoredPosition.y + contentOffsetY);

        }


    }

}
