using UnityEngine;
using System.Collections;
using System;
using TMPro;
using BigfootSdk.Helpers;

namespace BigfootSdk
{
    public class TimerLabel : MonoBehaviour
    {
        /// <summary>
        /// The time label.
        /// </summary>
        public TextMeshProUGUI TimeLabel;

        /// <summary>
        /// The name of the TimeFormatter to use
        /// </summary>
        public string TimeFormatterName = FormatHelper.DEFAULT_FORMATTER;
        
        /// <summary>
        /// The text to show if not running.
        /// </summary>
        public string TextIfNotRunning;

        /// <summary>
        /// The prefix.
        /// </summary>
        public string Prefix;

        /// <summary>
        /// The suffix.
        /// </summary>
        public string Suffix;

        /// <summary>
        /// The update timer delegate.
        /// </summary>
        Action updateTimerDelegate;

        /// <summary>
        /// The end timer delegate.
        /// </summary>
        Action endTimerDelegate;

        /// <summary>
        /// The started time.
        /// </summary>
        long startedTime;

        /// <summary>
        /// The duration.
        /// </summary>
        long duration;

        /// <summary>
        /// The time left.
        /// </summary>
        [HideInInspector]
        public long timeLeft;

        /// <summary>
        /// The total duration.
        /// </summary>
        long _totalDuration;

        // Can be used for a progress bar
        [HideInInspector]
        public float PercentageLeft;

        /// <summary>
        /// Flag to know if we should re-start the timer if it's disabled while running
        /// </summary>
        private bool _timerRunning = false;
        
        void Start()
        {
            UpdateTextIfNotRunning();
        }

        private void OnEnable()
        {
            if (_timerRunning)
            {
                StopAllCoroutines();
                StartCoroutine(UpdateTimer());
            }
        }

        /// <summary>
        /// Starts the timer.
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="endTimerDelegate">End timer delegate.</param>
        /// <param name="totalDuration">Total duration (if timer is started, to calculate percentages)</param>
        /// <param name="updateTimerDelegate">Update timer delegate.</param>
        public void StartTimer(long duration, Action endTimerDelegate = null, int totalDuration = 0, Action updateTimerDelegate = null)
        {
            StopAllCoroutines();
            // Cache the delegates
            this.endTimerDelegate = endTimerDelegate;
            this.updateTimerDelegate = updateTimerDelegate;

            // Cache the durations
            this.duration = duration;
            _totalDuration = totalDuration;

            // Save when the timer was started
            startedTime = TimeHelper.GetTimeInServer();

            _timerRunning = true;
            
            if(gameObject.activeInHierarchy)
                StartCoroutine(UpdateTimer());
        }

        /// <summary>
        /// Updates the text if the timer is not running.
        /// </summary>
        void UpdateTextIfNotRunning()
        {
            if (!string.IsNullOrEmpty(TextIfNotRunning) && TimeLabel != null)
            {
                // TODO: Localize this
                TimeLabel.text = TextIfNotRunning;
            }
        }

        /// <summary>
        /// Stops the timer.
        /// </summary>
        public void StopTimer()
        {
            StopAllCoroutines();
            // Update the text
            UpdateTextIfNotRunning();

            // If we had a end delegate, call it
            endTimerDelegate?.Invoke();

            // Make both delagetes null
            endTimerDelegate = null;
            updateTimerDelegate = null;

            _timerRunning = false;
        }

        public void ClearTimer()
        {
            UpdateTextIfNotRunning();
            _timerRunning = false;
            endTimerDelegate = null;
            updateTimerDelegate = null;
        }

        /// <summary>
        /// Update this instance.
        /// </summary>
        IEnumerator UpdateTimer()
        {
            // Update the time left
            timeLeft = duration - (TimeHelper.GetTimeInServer() - startedTime);

            WaitForSeconds waitOneSecond = new WaitForSeconds(1);

            // While we have time left
            while (timeLeft > 0)
            {
                TimeLeftUpdated();
                
                // Update the label
                if (TimeLabel != null)
                {
                    TimeLabel.text = string.Format("{0}{1}{2}", Prefix, FormatHelper.Instance.FormatTimer(timeLeft, TimeFormatterName), Suffix);
                }

                // Update the percentage
                PercentageLeft = _totalDuration == 0 ? (float)timeLeft / (float)duration : (float)timeLeft / (float)_totalDuration;

                // Call the update delagete
                updateTimerDelegate?.Invoke();

                // Wait for 1 second
                yield return waitOneSecond;
                
                // Update the time left
                timeLeft = duration - (TimeHelper.GetTimeInServer() - startedTime);
            }

            // Stop the timer once we have no more time left
            StopTimer();
        }
        
        /// <summary>
        /// The time left updated
        /// </summary>
        protected virtual void TimeLeftUpdated()
        {
    
        }
    }
}