﻿using System;
using BigfootSdk.Helpers;
using TMPro;
using UnityEngine;

namespace BigfootSdk.Backend
{
    public class TimeTravelLabel : MonoBehaviour
    {
        TextMeshProUGUI timeTravelLabel;

        void OnEnable()
        {
            DependencyManager.OnEverythingLoaded += Initialize;
            PlayerManager.OnTimeTravelApplied += UpdateLabel;
        }

        void OnDisable()
        {
            DependencyManager.OnEverythingLoaded -= Initialize;
            PlayerManager.OnTimeTravelApplied -= UpdateLabel;
        }

        void Initialize()
        {
            timeTravelLabel = GetComponent<TextMeshProUGUI>();
            UpdateLabel(1);
        }

        void UpdateLabel(int amount)
        {
            long timeTravel = PlayerManager.Instance.GetTimer(TimersConstants.TIME_TRAVEL, GameModeConstants.MAIN_GAME);

            timeTravelLabel.text = FormatHelper.Instance.FormatTimer(timeTravel);
        }
    }
}
