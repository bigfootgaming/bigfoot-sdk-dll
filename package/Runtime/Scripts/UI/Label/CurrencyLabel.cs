﻿using BigfootSdk;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using TMPro;
using UnityEngine;

public class CurrencyLabel : MonoBehaviour
{
    /// <summary>
    /// The key of the currency
    /// </summary>
    public string CurrencyKey;

    /// <summary>
    /// The formatter to use
    /// </summary>
    public string NumberFormatterName = FormatHelper.DEFAULT_FORMATTER;
    
    /// <summary>
    /// The Label
    /// </summary>
    TextMeshProUGUI _label;

    /// <summary>
    /// The ResourceManager
    /// </summary>
    PlayerManager _playerManager;

    void OnEnable()
    {
        // Populate the generators once everything is done loading
        PlayerManager.OnPlayerLoaded += InitializeLabel;
        if (PlayerManager.Instance != null && PlayerManager.Instance.IsLoaded)
            InitializeLabel();
    }

    void OnDisable()
    {
        PlayerManager.OnPlayerLoaded -= InitializeLabel;
    }

    void OnDestroy()
    {
        PlayerEvents.OnCurrencyBalanceChanged -= UpdateLabel;
    }
    
    
    /// <summary>
    /// Initializes the label
    /// </summary>
    void InitializeLabel()
    {
        // Cache the manager
        _playerManager = PlayerManager.Instance;

        // Grab a reference the the label component
        _label = GetComponent<TextMeshProUGUI>();

        UpdateLabel(CurrencyKey, 0);

        PlayerEvents.OnCurrencyBalanceChanged += UpdateLabel;
    }

    /// <summary>
    /// Updates the label
    /// </summary>
    /// <param name="currencyKey"></param>
    /// <param name="amount"></param>
    void UpdateLabel(string currencyKey, int amount)
    {
        if (currencyKey == CurrencyKey)
            _label.text = FormatHelper.Instance.FormatNumber(_playerManager.GetCurrencyBalance(currencyKey, GameModeManager.CurrentGameMode), NumberFormatterName);
    }
}