﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System;
using BigfootSdk.Helpers;
#if UNITY_EDITOR
using Sirenix.OdinInspector;
#endif

namespace BigfootSdk
{
	public abstract class ILoadable : MonoBehaviour
	{
		/// <summary>
		/// The identifier.
		/// </summary>
		[HideInInspector]
		public string Id;

		/// <summary>
		/// List of all the dependencies. This is used to check if this ILoadable is ready to be loaded
		/// </summary>
#if UNITY_EDITOR
		[DisableInEditorMode]
#endif
		public List<string> Dependencies = new List<string>();

		/// <summary>
		/// Event when this ILoadable finished loading
		/// </summary>
		public Action<string> OnFinishedLoading;

		/// <summary>
		/// List of triggers to start loading after this ILoadable is finished
		/// </summary>
		public List<ILoadable> Triggers;

		/// <summary>
		/// Flag to know if this ILoadable is finished
		/// </summary>
		[HideInInspector]
		public bool IsLoading;

        /// <summary>
        /// Flag to know if this ILoadable is finished
        /// </summary>
        [HideInInspector]
        public bool IsLoaded = false;

        /// <summary>
        /// Event for when this ILoadable is finished loading
        /// </summary>
        protected virtual void FinishedLoading(bool success)
        {
	        if (!IsLoaded)
	        {
		        if (gameObject != null)
					StartCoroutine(FinishedLoadingCo(success));
		        else
		        {
			        LogHelper.LogSdk("gameobject is null");
		        }
	        }
        }

        protected virtual IEnumerator FinishedLoadingCo(bool success)
        {
	        yield return null;
	        IsLoaded = success;
	        OnFinishedLoading?.Invoke(Id);
        }

        

        /// <summary>
		/// On Enable event
		/// </summary>
		protected virtual void OnEnable()
		{
			// Sewt the id to the name of the gameobject
			Id = gameObject.name;
		}

		/// <summary>
		/// Method that does the actual loading of this ILoadable
		/// </summary>
		public abstract void StartLoading ();
	}
}