﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;
namespace BigfootSdk
{
    public class DependencyNode : Node
    {

        [Input] public List<DependencyNode> Dependencies;

        [Output] public List<DependencyNode> Triggers;

        [HideInInspector]
        public int xPos;

        // Use this for initialization
        protected override void Init()
        {
            base.Init();

        }

        // Return the correct value of an output port when requested
        public override object GetValue(NodePort port)
        {
            return null; // Replace this
        }
    }
}
