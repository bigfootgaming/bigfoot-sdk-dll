﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Transaction that handles many transactions in one
    /// </summary>
    [System.Serializable]
    public class CompositeTransaction : ITransaction
    {
        /// <summary>
        /// Inner transactions
        /// </summary>
        public List<ITransaction> Transactions;
    
        /// <summary>
        /// Sends the transaction event.
        /// </summary>
        /// <param name="origin">Where this came from</param>
        /// <param name="sendTransactionEvent">send transaction event flag</param>
        public override TransactionResultModel ApplyTransaction(string origin = "", bool sendTransactionEvent = true)
        {
            TransactionResultModel result = new TransactionResultModel();
            for (int i = 0; i < Transactions.Count; i++)
                result.CustomResult.Add(i.ToString(), Transactions[i].ApplyTransaction(origin, sendTransactionEvent));
            return result;
        }

        /// <summary>
        /// Sends the transaction event.
        /// </summary>
        /// <param name="result">The result model</param>
        /// <param name="origin">Where this came from</param>
        protected override void SendTransactionEvent(TransactionResultModel result, string origin)
        {
        }
    }
}


