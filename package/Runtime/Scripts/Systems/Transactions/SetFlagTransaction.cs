﻿
namespace BigfootSdk.Backend
{
    /// <summary>
    /// This transaction can be used to set playerData flags as a reward
    /// </summary>
    public class SetFlagTransaction : ITransaction
    {
        /// <summary>
        /// Flag key
        /// </summary>
        public string Key;
        
        /// <summary>
        /// Flag value
        /// </summary>
        public bool Value;
    
        
        /// <summary>
        /// Applies the transaction. 
        /// </summary>
        /// <param name="origin">Where this came from</param>
        /// <param name="sendTransactionEvent">send transaction event flag</param>
        /// <returns>The result model as null</returns>
        public override TransactionResultModel ApplyTransaction(string origin = "", bool sendTransactionEvent = true)
        {
            PlayerManager.Instance.SetFlag(Key, Value, GameModeManager.CurrentGameMode);
            SendTransactionEvent(null, origin);
            TransactionResultModel result = new TransactionResultModel();
            result.CustomResult.Add(Key, Value);
            return result;
        }

        /// <summary>
        /// Sends the transaction event.
        /// </summary>
        /// <param name="result">The result model</param>
        /// <param name="origin">Where this came from</param>
        protected override void SendTransactionEvent(TransactionResultModel result, string origin)
        {
        
        }
    }
}


