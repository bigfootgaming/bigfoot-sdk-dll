﻿using System;
using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// This transaction can be used to add or substract a currency
    /// </summary>
    /// <code>
    /// <br/><b>Examples:</b>
    /// 
    /// { "$type":"CurrencyTransaction", "CurrencyKey":"hard", "Amount":3 }
    /// </code>
    [System.Serializable]
    public class CurrencyTransaction : ITransaction
    {
        /// <summary>
        /// The currency key.
        /// </summary>
        public string CurrencyKey;

        /// <summary>
        /// The amount.
        /// </summary>
        public int Amount;

        /// <summary>
        /// Sends the transaction event.
        /// </summary>
        /// <param name="result">The result model</param>
        /// <param name="origin">Where this came from</param>
        protected override void SendTransactionEvent(TransactionResultModel result, string origin)
        {
            TransactionEventModel transactionEventModel = new TransactionEventModel()
            {
                TransactionType = "CurrencyTransaction",
                Sinks = new List<TransactionEventParamenterModel>(),
                Sources = new List<TransactionEventParamenterModel>(),
                Origin = origin
            };

            if (Amount > 0)
                transactionEventModel.Sources.Add(new TransactionEventParamenterModel() { Key = CurrencyKey, Amount = Amount, Type = "Currency" });
            else
                transactionEventModel.Sinks.Add(new TransactionEventParamenterModel() { Key = CurrencyKey, Amount = Amount, Type = "Currency" });

            AnalyticsManager.Instance.TrackTransactionEvent(transactionEventModel);
        }

        /// <summary>
        /// Applies the transaction.
        /// </summary>
        /// <param name="origin">Where this came from</param>
        /// <param name="sendTransactionEvent">send transaction event flag</param>
        /// <returns>The result model</returns>
        public override TransactionResultModel ApplyTransaction(string origin = "", bool sendTransactionEvent = true)
        {
            // Apply the currency
            TransactionResultModel transactionResult = new TransactionResultModel();
            transactionResult.Currencies.Add(CurrencyKey, Amount);
            
            if (sendTransactionEvent)
                // Create and send the economy event
                SendTransactionEvent(transactionResult, origin);

            // Persist it in the server
            // Change the currency local
            PlayerManager.Instance.ChangeCurrency(CurrencyKey, Amount, GameModeManager.CurrentGameMode);

            // Send the appropriate callback
            return transactionResult;
        }
    }
}
