﻿namespace BigfootSdk.Backend
{
    /// <summary>
    /// This transaction can be used to set playerData counters as a reward
    /// </summary>
    public class SetCounterTransaction : ITransaction
    {
        /// <summary>
        /// Counter key
        /// </summary>
        public string Key;
        
        /// <summary>
        /// Counter value
        /// </summary>
        public int Value;
        
        /// <summary>
        /// if true, the transaction value will be added to the player data existing value.
        /// </summary>
        public bool Increment;
    
        /// <summary>
        /// Applies the transaction. 
        /// </summary>
        /// <param name="origin">Where this came from</param>
        /// <param name="sendTransactionEvent">send transaction event flag</param>
        /// <returns>The result model as null</returns>
        public override TransactionResultModel ApplyTransaction(string origin = "", bool sendTransactionEvent = true)
        {
            PlayerManager.Instance.SetCounter(Key, Value, GameModeManager.CurrentGameMode, Increment);
            SendTransactionEvent(null, origin);
            TransactionResultModel result = new TransactionResultModel();
            result.CustomResult.Add(Key, Value);
            return result;
        }

        /// <summary>
        /// Sends the transaction event.
        /// </summary>
        /// <param name="result">The result model</param>
        /// <param name="origin">Where this came from</param>
        protected override void SendTransactionEvent(TransactionResultModel result, string origin)
        {
        
        }
    }
}
