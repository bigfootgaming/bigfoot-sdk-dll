﻿using System;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Transaction abstract class
	/// </summary>
    public abstract class ITransaction
	{
		/// <summary>
		/// Applies the transaction. 
		/// </summary>
		/// <param name="origin">Where this came from</param>
		/// <param name="sendTransactionEvent">send transaction event flag</param>
		/// <returns>The result model</returns>
        public abstract TransactionResultModel ApplyTransaction(string origin = "", bool sendTransactionEvent = true);

		/// <summary>
		/// Sends the transaction event.
		/// </summary>
		/// <param name="result">The result model</param>
		/// <param name="origin">Where this came from</param>
        protected abstract void SendTransactionEvent(TransactionResultModel result, string origin);
    }
}
