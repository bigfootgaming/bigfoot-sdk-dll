﻿using BigfootSdk.Analytics;
using System;
using System.Collections.Generic;
namespace BigfootSdk.Backend
{
    /// <summary>
    /// This transaction can be used to buy VirtualGoods
    /// </summary>
    /// <code>
    /// <br/><b>Examples:</b>
    /// 
    /// {"ItemName":"item_1","Amount":1,"Key":"ItemTransaction"
    /// </code>
    [System.Serializable]
    public class ItemTransaction : ITransaction
    {
        /// <summary>
        /// The name of the item.
        /// </summary>
        public string ItemName;

        /// <summary>
        /// The amount to buy.
        /// </summary>
        public int Amount;

        /// <summary>
        /// The item model.
        /// </summary>
        protected BaseItemModel _itemModel;

        /// <summary>
        /// The cost of the item. Set to null if it is a reward.
        /// </summary>
        public ICost Cost;
        
        
        protected BaseItemModel ItemModel
        {
            get 
            { 
                if (_itemModel == null)
                {
                    _itemModel = ItemsManager.Instance.GetItem(ItemName);
                }
                return _itemModel;
            }
        }
        
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.Backend.ItemTransaction"/> class.
        /// </summary>
        /// <param name="item">Item.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="cost">Cost.</param>
        public ItemTransaction(BaseItemModel item, int amount = 1, ICost cost = null)
        {
            // Cache the item model
            _itemModel = item;

            ItemName = item.ItemName;
            Amount = amount;
            Cost = cost;
        }


        public ItemTransaction()
        {

        }

        /// <summary>
        /// Sends the transaction event.
        /// </summary>
        /// <param name="result">The result model</param>
        /// <param name="origin">Where this came from</param>
        protected override void SendTransactionEvent(TransactionResultModel resultModel, string origin)
        {
            TransactionEventModel transactionEventModel = new TransactionEventModel()
            {
                TransactionType = "ItemTransaction",
                Sinks = new List<TransactionEventParamenterModel>(),
                Sources = new List<TransactionEventParamenterModel>(),
                Origin = origin,
                ItemName = ItemName
            };

            if (ItemModel != null && Cost != null)
                Cost.PopulateTransactionEvent(transactionEventModel);
            

            transactionEventModel.Sources.Add (new TransactionEventParamenterModel() { Key = ItemName, Amount = Amount, Type = ItemModel.Class });

            foreach (var currency in resultModel.Currencies)
            {
                if (currency.Value > 0)
                    transactionEventModel.Sources.Add(new TransactionEventParamenterModel() { Key = currency.Key, Amount = currency.Value, Type = "Currency" });
                else
                    transactionEventModel.Sinks.Add(new TransactionEventParamenterModel() { Key = currency.Key, Amount = currency.Value, Type = "Currency" });
            }
            
            foreach (var item in resultModel.Items)
            {
                if (!transactionEventModel.Sources.Exists(x => x.Key == item.Name))
                    transactionEventModel.Sources.Add (new TransactionEventParamenterModel() { Key = item.Name, Amount = item.AmountRemaining, Type = item.Class});
            }
            AnalyticsManager.Instance.TrackTransactionEvent(transactionEventModel);
        }

        /// <summary>
        /// Applies the transaction.
        /// </summary>
        /// <param name="origin">Where this came from</param>
        /// <param name="sendTransactionEvent">send transaction event flag</param>
        /// <returns>The result model</returns>
        public override TransactionResultModel ApplyTransaction(string origin = "", bool sendTransactionEvent = true)
        {
            if (_itemModel == null)
                _itemModel = ItemsManager.Instance.GetItem(ItemName);
            
            if (_itemModel != null)
            {
                // If it's not a reward, see if we have enough to cost the item
                if (Cost == null || Cost.HasEnough())
                {
                    if(Cost != null)
                        Cost.DeductCost(origin, false);
                }
                else
                {
                    return new TransactionResultModel()
                    {
                        Error = "not_enough"
                    };
                }

                // Apply the item based on it's type
                TransactionResultModel resultModel =  ApplyByType(_itemModel,  origin);
                
                if (sendTransactionEvent)
                    SendTransactionEvent(resultModel, origin);
                
                return resultModel;
            }
            else
            {
                return new TransactionResultModel()
                {
                    Error = "Item doesn't exist."
                };
            }
        }

        protected virtual TransactionResultModel ApplyByType(BaseItemModel item, string origin)
        {
            // Apply the item based on it's type
            if (item is BundleItemModel bundleItem)
            {
                return ApplyBundleItem(bundleItem, Amount, origin, false);
            }
            else  if (item is DroptableItemModel droptableItemModel)
            {
                return ApplyDroptableItem(droptableItemModel, Amount, origin, false);
            }
            else if (item is GachaItemModel gachaItemModel)
            {
                return ApplyGachaItem(gachaItemModel, Amount, origin, false);
            }
            else
            {
                return ApplyBaseItem(item, Amount);
            }
        }
        
        protected virtual TransactionResultModel ApplyGachaItem(GachaItemModel gachaItem, int amount, string origin, bool sendTransactionEvent)
        {
            // Create an empty result model
            TransactionResultModel resultModel = new TransactionResultModel();

           

            // Iterate as many times as needed
            for (int i = 0; i < amount; i++)
            {
                int currentValue = gachaItem.TotalValue;
                while (currentValue > 0)
                {
                    var applyResult = new TransactionResultModel();
                    //get something from gacha
                    GachableItemModel gachaResult = PullFromGacha(gachaItem.Items, currentValue);

                    if (gachaResult != null)
                    {
                        currentValue -= gachaResult.Value;

                        //handle different subtypes
                        BaseItemModel item = ItemsManager.Instance.GetItem(gachaResult.ItemName);

                        if (item is BundleItemModel bundleItem)
                        {
                            applyResult = ApplyBundleItem(bundleItem, gachaResult.Amount, origin, false);
                        }
                        else if (item is DroptableItemModel droptableItemModel)
                        {
                            //here we need only one pull
                            DropableModel dropTableResult = PullFromDroptable(droptableItemModel.Items);
                            if (dropTableResult is DropableCurrencyModel dropableCurrencyModel)
                            {
                                CurrencyTransaction currencyTransacton = new CurrencyTransaction()
                                {
                                    CurrencyKey = dropableCurrencyModel.CurrencyKey,
                                    Amount = dropableCurrencyModel.Amount * gachaResult.Amount
                                };
                                applyResult = currencyTransacton.ApplyTransaction(origin, false);
                            }
                            else if (dropTableResult is DropableItemModel dropableItemModel)
                            {
                                ItemTransaction itemTransaction = new ItemTransaction()
                                {
                                    ItemName = dropableItemModel.ItemName,
                                    Amount = dropableItemModel.Amount * gachaResult.Amount
                                };
                                applyResult = itemTransaction.ApplyTransaction(origin, false);
                            }
                        }
                        else if (item is GachaItemModel gachaItemModel)
                        {
                            applyResult = ApplyGachaItem(gachaItemModel, gachaResult.Amount, origin, false);
                        }
                        else
                        {
                            applyResult = ApplyBaseItem(item, gachaResult.Amount);
                        }

                        // Append the result
                        resultModel.Union(applyResult);
                    }
                    else
                    {
                        currentValue = 0;
                    }
                }
            }
            
            return resultModel;
        }

        protected virtual TransactionResultModel ApplyDroptableItem(DroptableItemModel droptableItem, int amount, string origin, bool sendTransactionEvent)
        {
            // Create an empty result model
            TransactionResultModel resultModel = new TransactionResultModel();

            // Iterate as many times as needed
            for (int i = 0; i < amount; i++)
            {
                // Pull from the droptable
                var pullResult = PullFromDroptable(droptableItem.Items);
                
                // Apply the item
                var applyResult = new TransactionResultModel();
                if (pullResult is DropableCurrencyModel dropableCurrencyModel)
                {
                    CurrencyTransaction currencyTransacton = new CurrencyTransaction()
                    {
                        CurrencyKey = dropableCurrencyModel.CurrencyKey,
                        Amount = dropableCurrencyModel.Amount
                    };
                    applyResult = currencyTransacton.ApplyTransaction(origin, sendTransactionEvent);
                }
                else if (pullResult is DropableItemModel dropableItemModel)
                {
                    ItemTransaction itemTransaction = new ItemTransaction()
                    {
                        ItemName = dropableItemModel.ItemName,
                        Amount = dropableItemModel.Amount
                    };
                    applyResult = itemTransaction.ApplyTransaction(origin, sendTransactionEvent);
                }
                
                // Append the result
                resultModel.Union(applyResult);
            }
            
            return resultModel;
        }

        /// <summary>
        /// Applies a bundle
        /// </summary>
        /// <param name="bundleItem">The bundle </param>
        /// <param name="amount">The amount to grant</param>
        /// <returns>The transaction result model</returns>
        protected virtual TransactionResultModel ApplyBundleItem(BundleItemModel bundleItem, int amount, string origin, bool sendTransactionEvent)
        {
            // Create an empty result model
            TransactionResultModel resultModel = new TransactionResultModel();
            
            // Iterate as many times as needed
            for (int i = 0; i < amount; i++)
            {
                // Iterate through all of the items in the droptable
                foreach(var dropable in bundleItem.Items)
                {
                    // Apply the item
                    var applyResult = new TransactionResultModel();
                    if (dropable is DropableCurrencyModel dropableCurrencyModel)
                    {
                        CurrencyTransaction currencyTransacton = new CurrencyTransaction()
                        {
                            CurrencyKey = dropableCurrencyModel.CurrencyKey,
                            Amount = dropableCurrencyModel.Amount
                        };
                        applyResult = currencyTransacton.ApplyTransaction(origin, sendTransactionEvent);
                    }
                    else if (dropable is DropableItemModel dropableItemModel)
                    {
                        ItemTransaction itemTransaction = new ItemTransaction()
                        {
                            ItemName = dropableItemModel.ItemName,
                            Amount = dropableItemModel.Amount
                        };
                        applyResult = itemTransaction.ApplyTransaction(origin, sendTransactionEvent);
                    }
                    // Append the result
                    resultModel.Union(applyResult);
                }
            }
            return resultModel;
        }

        /// <summary>
        /// Applies an item
        /// </summary>
        /// <param name="baseItem">The base item</param>
        /// <param name="amount">The amount to grant</param>
        /// <returns>The transaction result model</returns>
        protected virtual TransactionResultModel ApplyBaseItem(BaseItemModel baseItem, int amount)
        {
            var newItem = new PlayerItemModel()
            {
                Id = Guid.NewGuid().ToString(),
                Name = baseItem.ItemName,
                Type = baseItem.Type,
                Class = baseItem.Class,
                AmountRemaining = amount
            };
            
            // Create an empty result model
            TransactionResultModel resultModel = new TransactionResultModel();
            
            resultModel.Items.Add(newItem);
            
            PlayerManager.Instance.AddItem(newItem, GameModeManager.CurrentGameMode);

            // Return the result model
            return resultModel;
        }

        /// <summary>
        /// Pulls from a droptable
        /// </summary>
        /// <param name="items">The items where we can pull from</param>
        /// <param name="amountOfPulls">The amount of pulls</param>
        /// <returns></returns>
        public virtual DropableModel PullFromDroptable(List<DropableModel> items)
        {
            // Figure out the total sum of the weight
            float totalWeight = 0;
            foreach (var item in items)
            {
                totalWeight += item.Weight;
            }
            
            // Adjust the weights of the items
            foreach (var item in items)
            {
                item.Weight = item.Weight / totalWeight;
            }
            
            // Get a random number between 0 & 1
            var probablity = UnityEngine.Random.Range(0f, 1f);
        
            // Find an item
            float probablityAccum = 0;
            for(var l = 0; l < items.Count; l++)
            {
                probablityAccum += items[l].Weight;   
                if(probablity <= probablityAccum)
                {
                    return items[l];
                }
            }

            return items[0];
        }

        public virtual GachableItemModel PullFromGacha(List<GachableItemModel> items, int currentValue)
        {
            // Figure out the total sum of the weight
            float totalWeight = 0;
            List<GachableItemModel> tempGacha = new List<GachableItemModel>();
            foreach (var item in items)
            {
                if (item.Value <= currentValue)
                {
                    tempGacha.Add(item);
                    totalWeight += item.Weight;
                }
            }

            if (tempGacha.Count == 0)
            {
                return null;
            }

            // Adjust the weights of the items
            foreach (var item in items)
            {
                item.Weight = item.Weight / totalWeight;
            }
            
            // Get a random number between 0 & 1
            var probablity = UnityEngine.Random.Range(0f, 1f);
        
            // Find an item
            float probablityAccum = 0;
            for(var l = 0; l < tempGacha.Count; l++)
            {
                probablityAccum += tempGacha[l].Weight;   
                if(probablity <= probablityAccum)
                {
                    return tempGacha[l];
                }
            }
            return tempGacha[0];
        }
    }
}
