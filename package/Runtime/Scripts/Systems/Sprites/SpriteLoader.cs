﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using BigfootSdk;
using BigfootSdk.Helpers;
using BigfootSdk.SceneManagement;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.U2D;

/// <summary>
/// Used to load any sprites needed to memory
/// </summary>
public class SpriteLoader : ILoadable
{
    /// <summary>
    /// List of all of the sprites to load
    /// </summary>
    private List<string> spritesToLoad;
    
    public override void StartLoading()
    {
        // Create the list of all the sprites to load
        spritesToLoad = new List<string>();
            
        // Get the SpriteLoader configuration file for this scene
        string spriteLoaderName = string.Format("SpriteLoader_{0}", BigfootSceneManager.GetActiveScene().name);
        AddressablesHelper.LoadAssetAsync<TextAsset>(spriteLoaderName, OnLoadedCallback);
    }

    private void OnLoadedCallback(TextAsset result)
    {
        if (result == null)
        {
            FinishedLoading(true);
            return;
        }
        
        // Split the file by lines
        string fs = result.text;
        string[] fLines = Regex.Split ( fs, "\n" );
                
        // Iterate through all of the lines and add them to the list
        foreach (var spriteName in fLines)
        {
            // Check if name contains type
            string[] nameAndType = spriteName.Split(',');

            if (nameAndType.Length > 1 && nameAndType[1] == "SpriteAtlas")
            {
                // Pass it to the scene manager
                BigfootSceneManager.LoadAddressable<SpriteAtlas>(nameAndType[0]);
            }
            else
            {
                // Pass it to the scene manager
                BigfootSceneManager.LoadAddressable<Sprite>(spriteName);
            }
        }
                
        // Add any other sprites needed
        AddToSpriteList();
                
        FinishedLoading(true);
    }

    /// <summary>
    /// Add any other sprites needed to the list
    /// </summary>
    protected virtual void AddToSpriteList()
    {
        
    }
}
