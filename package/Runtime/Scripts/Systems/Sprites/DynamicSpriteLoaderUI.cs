﻿
using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk.Sprites
{
    public class DynamicSpriteLoaderUI : MonoBehaviour
    {
        public Image Target;
        public bool LoadOnEnable;
        public SpriteLoaderType LoaderType;
        public string Key;
        public string SpriteElement;

        private void OnEnable()
        {
            if (LoadOnEnable)
                LoadSprite();
        }

        public void LoadSprite()
        {
            if (LoaderType == SpriteLoaderType.Sprite)
                SpriteManager.Instance.GetSprite(Key, (sprite) => {
                    Target.sprite = sprite;
                });
            else
                SpriteManager.Instance.GetSpriteFromSpriteAtlas(Key, SpriteElement, (sprite) => {
                    Target.sprite = sprite;
                });
        }

        private void OnDisable()
        {
            if (Target != null && Target.sprite != null && LoaderType == SpriteLoaderType.Sprite)
                SpriteManager.Instance.ReleaseSprite(Target.sprite);
        }
    }
}

