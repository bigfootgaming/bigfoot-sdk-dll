﻿
using UnityEngine;

namespace BigfootSdk.Sprites
{
    public class DynamicSpriteLoader : MonoBehaviour
    {
        public SpriteRenderer Target;
        public bool LoadOnEnable;
        public SpriteLoaderType LoaderType;
        public string Key;
        public string SpriteElement;

        private void OnEnable()
        {
            if (LoadOnEnable)
                LoadSprite();
        }

        public void LoadSprite()
        {
            if (LoaderType == SpriteLoaderType.Sprite)
                SpriteManager.Instance.GetSprite(Key, LoadSpriteCallback);
            else
                SpriteManager.Instance.GetSpriteFromSpriteAtlas(Key, SpriteElement, LoadSpriteCallback);
        }

        void LoadSpriteCallback (Sprite sprite)
        {
            Target.sprite = sprite;
        }

        private void OnDisable()
        {
            if (Target != null && Target.sprite != null && LoaderType == SpriteLoaderType.Sprite && SpriteManager.Instance != null)
                SpriteManager.Instance.ReleaseSprite(Target.sprite);
        }
    }

    public enum SpriteLoaderType
    {
        Sprite,
        SpriteAtlas 
    }
}



