﻿using System;
using BigfootSdk.Helpers;

namespace BigfootSdk
{
    /// <summary>
    /// Default TimeFormatter
    /// </summary>
    public class DefaultTimeFormatter : ITimeFormatter
    {
        protected string _secondsLocalization;

        protected string _minutesLocalization;

        protected string _hoursLocalization;
        
        protected string _daysLocalization;

        protected bool _localizationsSet;

        /// <summary>
        /// Constructor for the default TimeFormatter
        /// </summary>
        public DefaultTimeFormatter()
        {
            Name = FormatHelper.DEFAULT_FORMATTER;
        }

        protected virtual void SetLocalizations()
        {
            _secondsLocalization = Localization.Localization.Get("timer_formatter_seconds");
            _minutesLocalization = Localization.Localization.Get("timer_formatter_minutes");
            _hoursLocalization = Localization.Localization.Get("timer_formatter_hours");
            _daysLocalization = Localization.Localization.Get("timer_formatter_days");
        }
        
        /// <summary>
        /// Method to override to format accordingly
        /// </summary>
        /// <param name="timeLeft">How much time left we have</param>
        /// <returns>The formatted time</returns>
        public override string FormatTimer(TimeSpan timeLeft)
        {
            if (!_localizationsSet)
            {
                SetLocalizations();
                _localizationsSet = true;
            }
            
            if (timeLeft.TotalSeconds < 60)
            {
                return string.Format("{0}{1}", timeLeft.Seconds, _secondsLocalization);
            }
            else if (timeLeft.TotalSeconds < 3600)
            {
                return string.Format("{0}{1} {2}{3}", timeLeft.Minutes, _minutesLocalization, timeLeft.Seconds, _secondsLocalization);
            }
            else if (timeLeft.TotalSeconds < 86400)
            {
                return string.Format("{0}{1} {2}{3}", timeLeft.Hours, _hoursLocalization, timeLeft.Minutes, _minutesLocalization);
            }
            else 
            {
                return string.Format("{0}{1} {2}{3}", timeLeft.Days, _daysLocalization, timeLeft.Hours, _hoursLocalization);
            }
        }
    }  

}


