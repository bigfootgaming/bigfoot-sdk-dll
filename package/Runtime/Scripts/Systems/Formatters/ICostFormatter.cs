﻿using System;
using BigfootSdk.Backend;

namespace BigfootSdk
{
    /// <summary>
    /// Formatter parent class
    /// </summary>
    public abstract class ICostFormatter
    {
        /// <summary>
        /// The name for this formatter
        /// </summary>
        public string Name;
        
        /// <summary>
        /// Method to override to format accordingly
        /// </summary>
        /// <param name="cost">The cost to format</param>
        /// <returns>The formatted cost</returns>
        public abstract string FormatCost(ICost cost);
    }
}


