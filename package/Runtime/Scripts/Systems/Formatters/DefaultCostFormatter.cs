﻿using System;
using BigfootSdk.Backend;
using BigfootSdk.Core.Idle;
using BigfootSdk.Helpers;
using BigfootSdk.Shop;

namespace BigfootSdk
{
    /// <summary>
    /// Default CostFormatter
    /// </summary>
    public class DefaultCostFormatter : ICostFormatter
    {
        /// <summary>
        /// Constructor for the default CostFormatter
        /// </summary>
        public DefaultCostFormatter()
        {
            Name = FormatHelper.DEFAULT_FORMATTER;
        }

        /// <summary>
        /// Method to override to format accordingly
        /// </summary>
        /// <param name="cost">The cost to format</param>
        /// <returns>The formatted cost</returns>
        public override string FormatCost(ICost cost)
        {
            if (cost == null)
            {
                return Localization.Localization.Get("Cost_free");
            }
            
            if (cost is CurrencyCost currencyCost)
            {
                return string.Format("<sprite name=\"{0}\">{1}", currencyCost.CurrencyKey, FormatHelper.Instance.FormatNumber(currencyCost.Amount));
            }
            
            if (cost is ResourceCost resourceCost)
            {
                return string.Format("<sprite name=\"{0}\">{1}", resourceCost.ResourceKey, FormatHelper.Instance.FormatNumber(resourceCost.Amount));
            }

            if (cost is RealMoneyCost realMoneyCost)
            {
                #if UNITY_IPHONE
                    return ShopManager.Instance.GetLocalizedPrice(realMoneyCost.IosSKU);
                #else
                    return ShopManager.Instance.GetLocalizedPrice(realMoneyCost.GoogleSKU);
                #endif
            }
            
            return cost.GetCost().ToString();
        }
    }  

}


