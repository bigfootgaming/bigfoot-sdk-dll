﻿using System;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Default NumberFormatter
    /// </summary>
    public class DefaultNumberFormatter : INumberFormatter
    {
        /// <summary>
        /// Under this value, no suffix will be added to the number, and it will just be returned as is
        /// </summary>
        protected readonly int STANDARD_NOTATION_MIN_VALUE = 999;
        
        /// <summary>
        /// The suffixes to add to the numbers
        /// </summary>
        protected string[] suffix = { "", "k", "M", "B", "T", "q", "Q","s","S"};

        /// <summary>
        /// How many digits to use
        /// </summary>
        protected int _totalDigits = 3;
        
        /// <summary>
        /// Constructor for the default NumberFormatter
        /// </summary>
        public DefaultNumberFormatter()
        {
            Name = FormatHelper.DEFAULT_FORMATTER;
        }

        /// <summary>
        /// Method to override to format accordingly
        /// </summary>
        /// <param name="number">The number to format</param>
        /// <returns>The formatted number</returns>
        public override string FormatNumber(double number)
        {
            // If it's under the min value for standard notation, just add the thousand ',' and return
            if (number <= STANDARD_NOTATION_MIN_VALUE)
            {
                return String.Format("{0:#,##0}", number);
            }

            // Calculate which suffix we should add
            int scale = 0;
            double original = number;
            while (number >= 1000d)
            {
                number /= 1000d;
                scale++;

                // If we overflow our suffixes, fallback to  Scientific Notation
                if (scale >= suffix.Length)
                    return original.ToString("0.#e0");
            }

            string format = "";
            bool isDecimal = false;
            for (int i = 0; i < _totalDigits; i++)
            {
                if (!isDecimal)
                {
                    var rest = number / Mathf.Pow(10, i);
                    if(rest >= 1)
                        format += "#";
                    else
                    {
                        isDecimal = true;
                        format += ".#";
                    }
                }
                else
                {
                    format += "#";
                }
            }
            
            return string.Format("{0}{1}", number.ToString(format), suffix[scale]);
        }
        
    }  

}


