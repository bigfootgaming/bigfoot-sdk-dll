﻿using System;
using BigfootSdk.Backend;

namespace BigfootSdk
{
    /// <summary>
    /// Formatter parent class
    /// </summary>
    public abstract class INumberFormatter
    {
        /// <summary>
        /// The name for this formatter
        /// </summary>
        public string Name;

        /// <summary>
        /// Method to override to format accordingly
        /// </summary>
        /// <param name="number">The number to format</param>
        /// <returns>The formatted number</returns>
        public abstract string FormatNumber(double number);
    }
}


