﻿
namespace BigfootSdk
{
    /// <summary>
    /// Default NumberFormatter
    /// </summary>
    public class ScientificNumberFormatter : INumberFormatter
    {
        /// <summary>
        /// Constructor for the default NumberFormatter
        /// </summary>
        public ScientificNumberFormatter()
        {
            Name = "Scientific";
        }

        /// <summary>
        /// Method to override to format accordingly
        /// </summary>
        /// <param name="number">The number to format</param>
        /// <returns>The formatted number</returns>
        public override string FormatNumber(double number)
        {
            return number.ToString("0.#e0");
        }
        
    }  

}


