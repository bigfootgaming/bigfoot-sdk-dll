﻿using System;

namespace BigfootSdk
{
    /// <summary>
    /// Formatter parent class
    /// </summary>
    public abstract class ITimeFormatter
    {
        /// <summary>
        /// The name for this formatter
        /// </summary>
        public string Name;
        
        /// <summary>
        /// Method to override to format accordingly
        /// </summary>
        /// <param name="timeLeft">How much time left we have</param>
        /// <returns>The formatted time</returns>
        public abstract string FormatTimer(TimeSpan timeLeft);
    }
}


