using System;
using System.Collections.Generic;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk
{
    public class EnableDisableGameObjects : BaseSequentialModularBehavior
    {
        /// <summary>
        /// Class to hold the settings of the enable/disable game object
        /// </summary>
        [Serializable]
        public class EnableDisableSetting
        {
            public enum Value
            {
                Enable,
                Disable
            }

            /// <summary>
            /// The game object to target
            /// </summary>
            public GameObject TargetGameObject;

            /// <summary>
            /// The target enable/disable value 
            /// </summary>
            public Value TargetValue;
        }

        /// <summary>
        /// List of game objects to set his activation value
        /// </summary>
        public List<EnableDisableSetting> Settings;

        public override bool Initialize()
        {
            return true;
        }

        protected override void Begin()
        {
        }

        protected override void Execution()
        {
            if (Settings == null)
            {
                LogHelper.LogWarning("Unable to execute EnableDisableGameObjects ModularBehavior. Null inspector list");
                EndExecution();
                return;
            }

            // Run over the settings and enable/disable all the objects
            foreach (EnableDisableSetting setting in Settings)
            {
                if (setting == null)
                {
                    LogHelper.LogWarning("Fail trying to run enable/disable setting. Null inspector reference");
                    continue;
                }

                if (setting.TargetGameObject == null)
                {
                    LogHelper.LogWarning("Fail trying to run enable/disable setting. Null inspector reference");
                    continue;
                }

                switch (setting.TargetValue)
                {
                    case EnableDisableSetting.Value.Disable:
                    {
                        setting.TargetGameObject.SetActive(false);
                        break;
                    }
                    case EnableDisableSetting.Value.Enable:
                    {
                        setting.TargetGameObject.SetActive(true);
                        break;
                    }
                }
            }

            EndExecution();
        }

        protected override void End()
        {
        }
    }
}