﻿using BigfootSdk.Helpers;
using DG.Tweening;
using UnityEngine;

namespace BigfootSdk
{
    public class ScaleRectTransformWithTween : BaseSequentialModularBehavior
    {
        /// <summary>
        /// The target rect transform to scale
        /// </summary>
        public RectTransform TargetRectTransform;

        /// <summary>
        /// The time to the animation to run
        /// </summary>
        public float ScaleTransitionTime;

        /// <summary>
        /// The final scale value for the animation
        /// </summary>
        public float FinalScale;

        /// <summary>
        /// The initial scale value for the animation
        /// </summary>
        public float InitialScale;

        /// <summary>
        /// Cache the original scale of the rect transform
        /// </summary>
        private Vector2 _originalScale;

        public override bool Initialize()
        {
            // Null check he target transform
            if (TargetRectTransform == null)
            {
                LogHelper.LogWarning("Unable to initialize 'ScaleRectTransformWithTween'. Null rect transform reference in inspector");
                return false;
            }

            // Cache the scale original value
            _originalScale = TargetRectTransform.localScale;

            return true;
        }

        protected override void Begin()
        {
            // Set the initial value for the scale
            TargetRectTransform.localScale = new Vector2(InitialScale, InitialScale);
        }

        protected override void Execution()
        {
            // Tween scale and set the complete callback
            TargetRectTransform.DOScale(FinalScale, ScaleTransitionTime).OnComplete(EndExecution);
        }

        protected override void End()
        {
            // Set the scale value to the original value
            TargetRectTransform.localScale = _originalScale;
        }
    }
}