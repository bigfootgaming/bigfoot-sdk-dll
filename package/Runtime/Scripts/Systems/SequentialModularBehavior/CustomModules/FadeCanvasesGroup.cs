using System;
using System.Collections.Generic;
using BigfootSdk.Helpers;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk
{
    public class FadeCanvasesGroup : BaseSequentialModularBehavior
    {
        /// <summary>
        /// Class to hold the settings of the fade animation for each canvas group
        /// </summary>
        [Serializable]
        public class CanvasGroupFadeSetting
        {
            /// <summary>
            /// The canvas group to apply the fade effect
            /// </summary>
            public CanvasGroup TargetCanvasGroup;

            /// <summary>
            /// Bool to check if we want the initial alpha value not to be the current one of the canvas group
            /// </summary>
            public bool OverrideInitialValue;

            /// <summary>
            /// The initial alpha value of the canvas group
            /// </summary>
            [EnableIf("OverrideInitialValue")]
            public float InitialFadeValue;

            /// <summary>
            /// Bool to check if we want the final interaction value not to be the current one of the canvas group
            /// </summary>
            public bool OverrideInteractionValue;

            /// <summary>
            /// The interaction final value for the canvas group
            /// </summary>
            [EnableIf("OverrideInteractionValue")]
            public bool InteractionFinalValue;

            /// <summary>
            /// The final alpha value of the canvas group
            /// </summary>
            public float TargetFadeValue;

            /// <summary>
            /// The time for the animation to run
            /// </summary>
            public float AnimationSpeed;

            /// <summary>
            /// The ease for this animation
            /// </summary>
            public Ease Ease = Ease.Linear;
        }

        /// <summary>
        /// Inspector reference to each canvas group fade animation setting
        /// </summary>
        public List<CanvasGroupFadeSetting> Settings;

        /// <summary>
        /// The time in seconds when this module should end
        /// </summary>
        public uint EndModuleAfterSeconds;

        public override bool Initialize()
        {
            return true;
        }

        protected override void Begin()
        {
        }

        protected override void Execution()
        {
            if (Settings == null)
            {
                LogHelper.LogError("Unable run FadeCanvasesGroup modular behavior. Null inspector reference", Environment.StackTrace);
                EndExecution();
                return;
            }

            Sequence sequence = DOTween.Sequence();
            sequence.AppendCallback(() =>
            {
                foreach (CanvasGroupFadeSetting setting in Settings)
                {
                    if (setting == null)
                    {
                        LogHelper.LogWarning("Fail trying to run fade setting. Null inspector reference");
                        continue;
                    }

                    if (setting.TargetCanvasGroup == null)
                    {
                        LogHelper.LogWarning("Fail trying to run fade setting. Null inspector reference");
                        continue;
                    }

                    // Override the interaction value of the canvas group if needed
                    setting.TargetCanvasGroup.interactable = setting.OverrideInteractionValue ? setting.InteractionFinalValue : setting.TargetCanvasGroup.interactable;

                    // Set initial value if needed
                    setting.TargetCanvasGroup.alpha = setting.OverrideInitialValue ? setting.InitialFadeValue : setting.TargetCanvasGroup.alpha;

                    // Run the animation
                    setting.TargetCanvasGroup.DOFade(setting.TargetFadeValue, setting.AnimationSpeed).SetEase(setting.Ease);
                }
            });

            // Add a delay time
            sequence.AppendInterval(Math.Abs(EndModuleAfterSeconds));

            // When is complete end the module execution
            sequence.onComplete += () => { EndExecution(); };

            // Play sequence
            sequence.Play();
        }

        protected override void End()
        {
        }
    }
}