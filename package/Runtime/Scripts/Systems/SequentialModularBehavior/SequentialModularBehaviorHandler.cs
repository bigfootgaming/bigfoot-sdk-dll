﻿using System;
using System.Collections.Generic;
using BigfootSdk.Helpers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk
{
    public class SequentialModularBehaviorHandler : MonoBehaviour
    {
#if UNITY_EDITOR
        /// <summary>
        /// Button in the inspector to execute these modules.
        /// This is used for the devs only for testing propouses.
        /// </summary>
        [Button("Run modules")]
        private void InspectorExecution()
        {
            Run();
        }
#endif
        /// <summary>
        /// Queued sequential modular behaviors
        /// </summary>
        private Queue<BaseSequentialModularBehavior> _sequentialModularBehaviorsQueue;

        /// <summary>
        /// List of sequential modular behaviors to set in the inspector
        /// </summary>
        public List<BaseSequentialModularBehavior> SortedSequentialModularBehaviors;

        /// <summary>
        /// Run the sequential modular behavior based based on the list filled in the inspector
        /// </summary>
        public virtual void Run()
        {
            if (!QueueModules())
            {
                LogHelper.LogWarning("Unable to queue the modules. Fail queueing modules process");
                return;
            }

            RunModules();
        }

        /// <summary>
        /// Queue the modules based on the list of modules set in the inspector
        /// </summary>
        private bool QueueModules()
        {
            _sequentialModularBehaviorsQueue = new Queue<BaseSequentialModularBehavior>();

            if (SortedSequentialModularBehaviors == null)
            {
                LogHelper.LogWarning("Unable to queue the modules. Empty list of modules to run in inspector");
                return false;
            }

            // Run over all the modules set in the inspector and queue them
            foreach (BaseSequentialModularBehavior baseSequentialModularBehavior in SortedSequentialModularBehaviors)
            {
                _sequentialModularBehaviorsQueue.Enqueue(baseSequentialModularBehavior);
                if (!baseSequentialModularBehavior.Initialize())
                {
                    LogHelper.LogError("Fail module behavior initialization", Environment.StackTrace);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Run the modules
        /// </summary>
        private void RunModules()
        {
            if (_sequentialModularBehaviorsQueue == null)
            {
                return;
            }

            if (_sequentialModularBehaviorsQueue.Count <= 0)
            {
                return;
            }

            // Dequeue module
            BaseSequentialModularBehavior baseSequentialModularBehavior = _sequentialModularBehaviorsQueue.Dequeue();

            // Set the end of the module callback to run the next module by calling this method again
            baseSequentialModularBehavior.OnEndExecution = RunModules;

            // Run the current dequeued module
            baseSequentialModularBehavior.RunModule();
        }
    }
}