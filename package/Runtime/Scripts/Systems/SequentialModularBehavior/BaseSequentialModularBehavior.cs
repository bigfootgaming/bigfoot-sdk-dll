﻿using System;
using UnityEngine;

namespace BigfootSdk
{
    public abstract class BaseSequentialModularBehavior : MonoBehaviour
    {
        /// <summary>
        /// Action for the end of this module
        /// </summary>
        public Action OnEndExecution;

        /// <summary>
        /// Initialization
        /// </summary>
        public abstract bool Initialize();

        /// <summary>
        /// Behavior prior the main execution of the module
        /// </summary>
        protected abstract void Begin();

        /// <summary>
        /// Execution behavior.
        /// This is where the main code of the module behavior must be placed
        /// </summary>
        protected abstract void Execution();

        /// <summary>
        /// Behavior post the main execution of the module behavior
        /// </summary>
        protected abstract void End();

        /// <summary>
        /// Run the module instance
        /// </summary>
        public void RunModule()
        {
            Begin();
            Execution();
        }

        /// <summary>
        /// Method to call to announce the end of the execution of this behavior instance
        /// </summary>
        protected void EndExecution()
        {
            End();
            OnEndExecution?.Invoke();
        }
    }
}