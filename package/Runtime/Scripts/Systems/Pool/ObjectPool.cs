﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Linq;
using System.Threading.Tasks;
using BigfootSdk.Helpers;

namespace BigfootSdk
{
    /// <summary>
    /// Object pool. Used to minimize instantiation of gameObjects.
    /// </summary>
    public class ObjectPool : ILoadable
    {
        public static Action OnAssetLoadFailed;
        
        /// <summary>
        /// Object pool asset.
        /// </summary>
        [Serializable]
        public class ObjectPoolAsset
        {
            /// <summary>
            /// The asset reference. Used through Addressable.
            /// </summary>
            public AssetReferenceGameObject AssetReference;

            /// <summary>
            /// The Addressable asset key. Used for assets loaded remotely.
            /// </summary>
            public string AssetKey;

            /// <summary>
            /// Operation that handles the asset load
            /// </summary>
            public AsyncOperationHandle<GameObject> HandleOp;

            /// <summary>
            /// The game object reference.
            /// </summary>
            public GameObject GameObjectReference;
            
            /// <summary>
            /// The amount to spawn when the pool inits.
            /// </summary>
            public int AmountToSpawn;
            
            /// <summary>
            /// The instances.
            /// </summary>
            public List<GameObject> Instances;
        }
        
        /// <summary>
        /// The object pool assets.
        /// </summary>
        public List<ObjectPoolAsset> ObjectPoolAssets;

        /// <summary>
        /// If true, the pool will init on Start.
        /// </summary>
        public bool InitializeOnStart = true;
        
        /// <summary>
        /// The container of inactive gameObjects.
        /// </summary>
        protected Transform _container;
        
        /// <summary>
        /// The inited flag.
        /// </summary>
        protected bool _inited = false;
        
        /// <summary>
        /// The initing flag.
        /// </summary>
        protected bool _initing = false;

        protected int _assetsInstantiated = 0;

        protected int _assetsToLoad;

        protected int _assetsLoaded;

        public int AssetsToLoad => _assetsToLoad;
        
        public int AssetsLoaded => _assetsLoaded;

        /// <summary>
        /// Start this instance.
        /// </summary>
        void Start()
        {
            if (InitializeOnStart && !_inited)
                StartLoading();
        }

        /// <summary>
        /// Release assets on destroy.
        /// </summary>
        private void OnDestroy()
        {
            UnloadAssets();
        }

        public virtual void UnloadAssets()
        {
            for (int i = 0; i < ObjectPoolAssets.Count; i++)
            {
                if (ObjectPoolAssets[i].HandleOp.IsValid())
                    Addressables.Release(ObjectPoolAssets[i].HandleOp);
            }
        }

        /// <summary>
        /// checks if the pool is inited.
        /// </summary>
        /// <returns><c>true</c>, if the pool is inited, <c>false</c> otherwise.</returns>
        public virtual bool IsInited()
        {
            return _inited;
        }

        /// <summary>
        /// Init the pool.
        /// </summary>
        public override void StartLoading()
        {
            if (!_initing)
            {
                _initing = true;

                _container = transform;
                
                //load, instantiate if needed, then finish loading.
                LoadAssets(() => InstantiateInitials(CompleteLoading));
            }
        }

        /// <summary>
        /// Loads the assets that must be preloaded.
        /// </summary>
        /// <param name="callback">callback action.</param>
        protected virtual void LoadAssets (Action callback)
        {
            _assetsToLoad = ObjectPoolAssets.Count;
            _assetsLoaded = 0;
            for (int a = 0; a < _assetsToLoad; a++)
            {
                if (ObjectPoolAssets[a].AssetReference != null)
                {
                    ObjectPoolAssets[a].HandleOp = AddressablesHelper.LoadAssetAsyncByAssetReference<GameObject>(ObjectPoolAssets[a].AssetReference);
                    ObjectPoolAssets[a].HandleOp.Completed += (load1) => {
                       LoadAssetCallback(load1, callback);
                    };
                }
                else if (!string.IsNullOrEmpty(ObjectPoolAssets[a].AssetKey))
                {
                    ObjectPoolAssets[a].HandleOp =  AddressablesHelper.LoadAssetAsyncByKey<GameObject>( ObjectPoolAssets[a].AssetKey);
                    ObjectPoolAssets[a].HandleOp.Completed += (load2) => {
                        LoadAssetCallback(load2, callback);
                    };
                }
            }
        }

        protected virtual void LoadAssetCallback(AsyncOperationHandle<GameObject> load, Action callback)
        {
            if (load.Status == AsyncOperationStatus.Failed)
                OnAssetLoadFailed?.Invoke();
            else if (load.Status == AsyncOperationStatus.Succeeded)
            {
                LogHelper.LogSdk("loaded "+ load.Result.name, LogHelper.POOL);
                _assetsLoaded++;
            } 
            if (_assetsLoaded == _assetsToLoad)
                callback();
        }

        /// <summary>
        /// Instantiates the initial instances.
        /// </summary>
        /// <param name="callback">Callback.</param>
        protected virtual void InstantiateInitials(Action callback)
        {
            if (this != null)
            {
                int instancesAmount = 0;

                //fetch instances amount to spawn
                for (int a = 0; a < ObjectPoolAssets.Count; a++)
                {
                    instancesAmount += ObjectPoolAssets[a].AmountToSpawn;
                }
                if (instancesAmount == 0)
                    callback();
                else
                {
                    for (int a = 0; a < ObjectPoolAssets.Count; a++)
                    {
                        InstantiateInitials(a, instancesAmount, callback);
                    }
                }
            }
        }

        /// <summary>
        /// Instantiates the initial instances.
        /// </summary>
        /// <param name="a">The asset index.</param>
        /// <param name="instancesAmount">Total instances amount. Used to know when do the <paramref name="callback"/>.</param>
        /// <param name="callback">Callback.</param>
        protected virtual void InstantiateInitials (int a, int instancesAmount, Action callback)
        {
            ObjectPoolAssets[a].Instances = new List<GameObject>();
            
            // Instantiate initial amount of assets
            for (int i = 0; i < ObjectPoolAssets[a].AmountToSpawn; i++)
            {
                if (ObjectPoolAssets[a].AssetReference != null && ObjectPoolAssets[a].AssetReference.Asset != null)
                {
                    ObjectPoolAssets[a].AssetReference.InstantiateAsync(_container, false).Completed += (instatiation) => {
                        InstantiateCallback(ObjectPoolAssets[a], ObjectPoolAssets[a].AssetReference.Asset.name, instancesAmount, instatiation.Result, callback);
                    };
                }
                else if (!string.IsNullOrEmpty(ObjectPoolAssets[a].AssetKey))
                {
                    Addressables.InstantiateAsync(ObjectPoolAssets[a].AssetKey, _container, false).Completed +=
                        (instatiation) =>
                        {
                            InstantiateCallback(ObjectPoolAssets[a], ObjectPoolAssets[a].AssetKey, instancesAmount, instatiation.Result, callback);
                        };
                }
                else if (ObjectPoolAssets[a].GameObjectReference != null)
                {
                    var instance = Instantiate(ObjectPoolAssets[a].GameObjectReference, _container);
                    InstantiateCallback(ObjectPoolAssets[a], ObjectPoolAssets[a].GameObjectReference.name, instancesAmount, instance, callback);
                }
                
                if (_assetsInstantiated == instancesAmount)
                    callback();
            }
        }

        protected virtual void InstantiateCallback(ObjectPoolAsset asset, string assetName, int instancesAmount, 
            GameObject instance, Action callback)
        {
            OnObjectInstantiated(instance);
            instance.SetActive(false);

            // Update name
            instance.name = assetName;

            // Add the instantiated object to its list
            asset.Instances.Add(instance);

            _assetsInstantiated++;

            if (_assetsInstantiated == instancesAmount)
                callback();
        }

        /// <summary>
        /// Completes the loading process.
        /// </summary>
        void CompleteLoading ()
        {
            _inited = true;
            FinishedLoading(true);
        }


        /// <summary>
        /// Gets an instance of the object required.
        /// </summary>
        /// <returns>The gameObject instance.</returns>
        /// <param name="objectName">Object name.</param>
        /// <param name="newParent">New parent.</param>
        public virtual GameObject GetObjectForType(string objectName, Transform newParent)
        {
            //if pool is not inited, then init it.
            if (!_inited)
            {
                StartLoading();
                LogHelper.LogWarning("Calling pool before it is inited");
                return null;
            }
                
            else
            {
                //get the asset
                ObjectPoolAsset asset = ObjectPoolAssets.FirstOrDefault(x => x.AssetReference != null && x.AssetReference.Asset != null && x.AssetReference.Asset.name == objectName || 
                                                                             x.AssetKey == objectName);
                //if the asset exists
                if (asset != null)
                {
                    GameObject result = null;
                    if (asset.Instances.Count > 0)
                    {
                        // The asset is instantiated, so we remove it from the list
                        result = asset.Instances[0];
                        asset.Instances.RemoveAt(0);
                        // Re-parent before activate for performance
                        result.transform.SetParent(newParent);
                    }
                    else
                    {
                        // No instances, so we need a new one
                        if (asset.AssetReference != null && asset.AssetReference.Asset != null)
                        {
                            AsyncOperationHandle<GameObject> instance = asset.AssetReference.InstantiateAsync(newParent, false);

                            instance.Completed += (obj) => { instance.Result.name = asset.AssetReference.Asset.name; };

                            result = instance.Result; 
                        }
                        else if (!string.IsNullOrEmpty(asset.AssetKey))
                        {
                            AsyncOperationHandle<GameObject> instance =
                                Addressables.InstantiateAsync(asset.AssetKey, newParent, false);
                            instance.Completed += (obj) => { instance.Result.name = asset.AssetKey; };

                            result = instance.Result; 
                        }
                        else if (asset.GameObjectReference != null)
                        {
                            result = Instantiate(asset.GameObjectReference, newParent);
                        }
                    }
                    OnObjectInstantiated(result);
                    return result;
                }
                else
                {
                    //asset doesn't exist
                    return null;
                }
            }

        }
        
        /// <summary>
        /// Gets an instance of the object required.
        /// </summary>
        /// <returns>The gameObject instance.</returns>
        /// <param name="objectName">Object name.</param>
        /// <param name="newParent">New parent.</param>
        public virtual async Task<GameObject> GetObjectForTypeAsync(string objectName, Transform newParent)
        {
            try
            {
//if pool is not inited, then init it.
                if (!_inited)
                {
                    StartLoading();
                    LogHelper.LogWarning("Calling pool before it is inited");
                    return null;
                }

                else
                {
                    //get the asset
                    ObjectPoolAsset asset = ObjectPoolAssets.FirstOrDefault(x => x.AssetReference.Asset != null && x.AssetReference.Asset.name == objectName);

                    //if the asset exists
                    if (asset != null)
                    {
                        GameObject result = null;
                        if (asset.Instances.Count > 0)
                        {
                            // The asset is instantiated, so we remove it from the list
                            result = asset.Instances[0];
                            asset.Instances.RemoveAt(0);
                            // Re-parent before activate for performance
                            result.transform.SetParent(newParent);
                        }
                        else
                        {
                            // No instances, so we need a new one
                            if (asset.AssetReference != null)
                            {
                                AsyncOperationHandle<GameObject> task = asset.AssetReference.InstantiateAsync(newParent, false);
                                result = await task.Task;
                                result.name = asset.AssetReference.Asset.name;
                            }
                            else if (asset.GameObjectReference != null)
                            {
                                result = Instantiate(asset.GameObjectReference, newParent);
                            }
                        }
                        if (result != null) OnObjectInstantiated(result);
                        return result;
                    }
                    else
                    {
                        //asset doesn't exist
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                LogHelper.LogWarning($"ObjectPool.cs :: caught exception during GetObjectForTypeAsync for {objectName}");
                throw;
            }
        }

        /// <summary>
        /// Return the instance to the pool
        /// </summary>
        /// <returns><c>true</c>, if object was pooled, <c>false</c> otherwise.</returns>
        /// <param name="instance">Instance.</param>
        /// <param name="changeParent">If set to <c>true</c> change parent.</param>
        public virtual bool PoolObject(GameObject instance, bool changeParent = true)
        {
            ObjectPoolAsset asset = ObjectPoolAssets.FirstOrDefault(x =>
                x.AssetReference != null && x.AssetReference.Asset != null && x.AssetReference.Asset.name == instance.name ||
                x.AssetKey == instance.name);
            if (asset != null)
            {
                if (!asset.Instances.Contains(instance))
                {
                    asset.Instances.Add(instance);
                    // Disable the gameObject
                    var canvas = instance.GetComponent<Canvas>();
                    if (canvas != null)
                        canvas.enabled = false;
                    else
                        instance.SetActive(false);
                    
                    // Change parent after deactivate for performance
                    if (changeParent)
                        instance.transform.SetParent(_container.transform, false);
                    
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Adds a new object to the pool
        /// </summary>
        /// <param name="prefab">The prefab to use for this object</param>
        /// <param name="name">The name of the prefab</param>
        public void AddObjectToPool(GameObject prefab, string name)
        {
            ObjectPoolAssets.Add(new ObjectPoolAsset()
            {
                GameObjectReference = prefab,
                AmountToSpawn = 1,
                Instances = new List<GameObject>()
            });
        }

        /// <summary>
        /// Override this method to do some initial configuration after new object instantiation.
        /// </summary>
        /// <param name="prefab">GameObject to setup.</param>
        protected virtual void OnObjectInstantiated(GameObject gameObject) {}
    }


}
