﻿using System.Collections.Generic;
using BigfootSdk.Backend;

[System.Serializable]
public class SerializablePlayerModel
{
    public string PlayerId;
    public long LastSave;
    public Dictionary<string, SerializablePlayerInfoModel> PlayerInfoModels;

    public SerializablePlayerModel() { }
    public SerializablePlayerModel(PlayerModel playerModel)
    {
        this.PlayerId = playerModel.PlayerId.GetDecrypted();
        this.LastSave = playerModel.LastSave.GetDecrypted();
        this.PlayerInfoModels = new Dictionary<string, SerializablePlayerInfoModel>();
        foreach (var item in playerModel.PlayerInfoModels)
        {
            this.PlayerInfoModels[item.Key] = new SerializablePlayerInfoModel(item.Value);
        }
    }

    public PlayerModel ToPlayerModel()
    {
        PlayerModel ret = new PlayerModel()
        {
            PlayerId = this.PlayerId,
            LastSave = this.LastSave,
            PlayerInfoModels = new Dictionary<string, PlayerInfoModel>()
        };

        foreach (var item in this.PlayerInfoModels)
        {
            ret.PlayerInfoModels[item.Key] = item.Value.ToPlayerInfoModel();
        }
        return ret;
    }
}
