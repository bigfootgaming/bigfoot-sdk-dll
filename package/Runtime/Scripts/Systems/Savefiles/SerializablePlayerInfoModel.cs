﻿using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

/// <summary>
/// Player Info model.
/// </summary>
[System.Serializable]
public class SerializablePlayerInfoModel
{
    /// <summary>
    /// The player data.
    /// </summary>
    public Dictionary<string, string> Data = new Dictionary<string, string>();

    /// <summary>
    /// The currencies.
    /// </summary>
    public Dictionary<string, int> Currencies = new Dictionary<string, int>();

    /// <summary>
    /// The currencies.
    /// </summary>
    public Dictionary<string, int> Counters = new Dictionary<string, int>();

    /// <summary>
    /// The currencies.
    /// </summary>
    public Dictionary<string, bool> Flags = new Dictionary<string, bool>();

    /// <summary>
    /// The currencies.
    /// </summary>
    public Dictionary<string, long> Timers = new Dictionary<string, long>();

    /// <summary>
    /// The items.
    /// </summary>
    public List<SerializablePlayerItemModel> Items = new List<SerializablePlayerItemModel>();

    public BigfootSdk.Backend.PlayerInfoModel ToPlayerInfoModel()
    {
        BigfootSdk.Backend.PlayerInfoModel ret = new BigfootSdk.Backend.PlayerInfoModel();

        ret.Data = new Dictionary<string, ObscuredString>();
        foreach (var item in this.Data)
        {
            ret.Data[item.Key] = item.Value;
        }
        ret.Currencies = new Dictionary<string, ObscuredInt>();
        foreach (var item in this.Currencies)
        {
            ret.Currencies[item.Key] = item.Value;
        }
        ret.Counters = new Dictionary<string, ObscuredInt>();
        foreach (var item in this.Counters)
        {
            ret.Counters[item.Key] = item.Value;
        }
        ret.Flags = new Dictionary<string, ObscuredBool>();
        foreach (var item in this.Flags)
        {
            ret.Flags[item.Key] = item.Value;
        }
        ret.Timers = new Dictionary<string, ObscuredLong>();
        foreach (var item in this.Timers)
        {
            ret.Timers[item.Key] = item.Value;
        }
        ret.Items = new List<BigfootSdk.Backend.PlayerItemModel>();
        foreach (var item in this.Items)
        {
            ret.Items.Add(item.ToPlayerItemModel());
        }
        return ret;
    }

    public SerializablePlayerInfoModel() { }
    public SerializablePlayerInfoModel(BigfootSdk.Backend.PlayerInfoModel playerInfoModel)
    {
        foreach (var item in playerInfoModel.Data)
        {
            Data[item.Key] = item.Value.GetDecrypted();
        }
        foreach (var item in playerInfoModel.Currencies)
        {
            Currencies[item.Key] = item.Value.GetDecrypted();
        }
        foreach (var item in playerInfoModel.Counters)
        {
            Counters[item.Key] = item.Value.GetDecrypted();
        }
        foreach (var item in playerInfoModel.Flags)
        {
            Flags[item.Key] = item.Value.GetDecrypted();
        }
        foreach (var item in playerInfoModel.Timers)
        {
            Timers[item.Key] = item.Value.GetDecrypted();
        }
        foreach (var item in playerInfoModel.Items)
        {
            Items.Add(new SerializablePlayerItemModel(item));
        }
    }

}


