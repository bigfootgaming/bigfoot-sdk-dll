﻿using System.Collections.Generic;



/// <summary>
/// Player Item model.
/// </summary>
[System.Serializable]
public class SerializablePlayerItemModel
{
    /// <summary>
    /// The identifier.
    /// </summary>
    public string Id;

    /// <summary>
    /// The item name. Reference to BaseItemModel.
    /// </summary>
    public string Name;

    /// <summary>
    /// The type.
    /// </summary>
    public string Type;

    /// <summary>
    /// The class of this item
    /// </summary>
    public string Class;

    /// <summary>
    /// The amount remaining.
    /// </summary>
    public int AmountRemaining;

    /// <summary>
    /// The item instance custom data.
    /// </summary>
    public Dictionary<string, object> Data;

    public SerializablePlayerItemModel() { }
    public SerializablePlayerItemModel(BigfootSdk.Backend.PlayerItemModel item)
    {
        this.Id = item.Id.GetDecrypted();
        this.Name = item.Name.GetDecrypted();
        this.Type = item.Type.GetDecrypted();
        this.Class = item.Class.GetDecrypted();
        this.AmountRemaining = item.AmountRemaining.GetDecrypted();
        this.Data = new Dictionary<string, object>(item.Data);
    }

    public override string ToString()
    {
        return string.Format("ItemName: {0}  Id: {1}", Name, Id);
    }

    public BigfootSdk.Backend.PlayerItemModel ToPlayerItemModel()
    {
        BigfootSdk.Backend.PlayerItemModel ret = new BigfootSdk.Backend.PlayerItemModel()
        {
            Id = this.Id,
            Name = this.Name,
            Type = this.Type,
            Class = this.Class,
            AmountRemaining = this.AmountRemaining,
            Data = new Dictionary<string, object>(this.Data)
        };
        return ret;
    }
}
