using UnityEngine;
using TMPro;

namespace BigfootSdk.Localization
{
    /// <summary>
    /// Simple script that lets you localize a UIWidget.
    /// </summary>
    [ExecuteInEditMode]
    public class UILocalize : MonoBehaviour
    {
        /// <summary>
        /// Localization key.
        /// </summary>
        public string key;

        /// <summary>
        /// Manually change the value of whatever the localization component is attached to.
        /// </summary>
        public string value
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    TextMeshProUGUI lbl = GetComponent<TextMeshProUGUI>();
                    if (lbl != null)
                    {
                        lbl.text = value;
                    }
                    else
                    {
                        TextMeshPro tmpLbl = GetComponent<TextMeshPro>();
                        if (tmpLbl != null)
                        {
                            tmpLbl.text = value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Has this been started
        /// </summary>
        bool mStarted = false;

        /// <summary>
        /// Localize the widget on enable, but only if it has been started already.
        /// </summary>
        void OnEnable()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif
            if (mStarted) OnLocalize();
            Localization.onLocalize += OnLocalize;
        }

        void OnDisable()
        {
            Localization.onLocalize -= OnLocalize;
        }

        /// <summary>
        /// Localize the widget on start.
        /// </summary>
        void Start()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif
            mStarted = true;
            OnLocalize();
        }

        /// <summary>
        /// This function is called by the Localization manager via a broadcast SendMessage.
        /// </summary>
        void OnLocalize()
        {
            // If no localization key has been specified, use the label's text as the key
            if (string.IsNullOrEmpty(key))
            {
                TextMeshProUGUI lbl = GetComponent<TextMeshProUGUI>();
                if (lbl != null) 
                    key = lbl.text;
                else
                {
                    TextMeshPro tmpLbl = GetComponent<TextMeshPro>();
                    if (tmpLbl != null)
                    {
                        key = tmpLbl.text;
                    }
                }
            }

            // If we still don't have a key, leave the value as blank
            if (!string.IsNullOrEmpty(key)) value = Localization.Get(key);
        }

        /// <summary>
        /// Sets the key.
        /// </summary>
        /// <param name="key">Key.</param>
        public void SetKey(string key)
        {
            this.key = key;
            OnLocalize();
        }
    }

}