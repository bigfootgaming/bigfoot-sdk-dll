﻿using UnityEngine;
using System;

namespace BigfootSdk.Localization
{
    /// <summary>
    /// Initializes the localization
    /// </summary>
    public class LocalizationLoader : ILoadable
    {
        /// <summary>
        /// Starts loading.
        /// </summary>
        public override void StartLoading()
        {
            Localization.LoadDictionary(PlayerPrefs.GetString("Language", Application.systemLanguage.ToString()), FinishedLoading);
        }
    }
}

