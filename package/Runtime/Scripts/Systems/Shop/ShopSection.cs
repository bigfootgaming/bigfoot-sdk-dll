﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace BigfootSdk.Shop
{
    /// <summary>
    /// Base class for ShopGridItems
    /// </summary>
    public class ShopSection : MonoBehaviour
    {
        /// <summary>
        /// The title of the section
        /// </summary>
        public TextMeshProUGUI TitleLabel;

        public Transform ItemsParent;
        
        /// <summary>
        /// Cached reference to the section model
        /// </summary>
        protected ShopSectionModel _sectionModel;

        /// <summary>
        /// Cached reference to the pool
        /// </summary>
        protected ObjectPool _pool;

        /// <summary>
        /// List of all the instantiated ShopGridItems
        /// </summary>
        protected List<ShopGridItem> _gridItems = new List<ShopGridItem>();

        /// <summary>
        /// Section Id
        /// </summary>
        public string SectionId => _sectionModel.Id;

        /// <summary>
        /// The timer for this ShopItem
        /// </summary>
        public GameObject Timer;

        /// <summary>
        /// The timer label
        /// </summary>
        public TimerLabel TimerLabel;

        protected RectTransform _rectTransform;

        public RectTransform RT
        {
            get => _rectTransform;
            set => _rectTransform = value;
        }

        protected virtual void Awake()
        {
            
        }
        
        protected virtual void OnEnable()
        {
            Localization.Localization.onLocalize += SetTitleLabel;
        }
    
        protected virtual void OnDisable()
        {
            Localization.Localization.onLocalize-= SetTitleLabel;
        }

        /// <summary>
        /// Initializes the ShopSection
        /// </summary>
        /// <param name="sectionModel">The section for this grid item</param>
        /// <param name="pool">The object pool</param>
        public virtual async Task<bool> Initialize(ShopSectionModel sectionModel, ObjectPool pool)
        {
            if (_rectTransform == null)
                _rectTransform = GetComponent<RectTransform>();
            _sectionModel = sectionModel;
            _pool = pool;

            SetTitleLabel();
            
            InitializeTimer();
            await Refresh();
            if (_gridItems.Count > 0)
                return true;
            return false;
        }

        protected virtual void SetTitleLabel()
        {
            if (TitleLabel != null)
            {
                TitleLabel.text = Localization.Localization.Get(string.Format("ShopSection_Title_{0}", SectionId));
            }
        }

        /// <summary>
        /// See if there is any timer that needs initialization
        /// </summary>
        protected virtual void InitializeTimer()
        {
            if (Timer != null)
            {
                Timer.SetActive(false);
                if (_sectionModel.Timer != null)
                {
                    var timeRemaining = _sectionModel.Timer.GetRemainingTime();
                    if (timeRemaining > 0)
                    {
                        Timer.SetActive(true);
                        TimerLabel.StartTimer(timeRemaining, EndedTimer);
                    }
                }
            }
        }


        protected virtual void EndedTimer()
        {
            Timer.SetActive(false);
        }


        public virtual async Task Refresh()
        {
            foreach (var item in _gridItems)
            {
                _pool.PoolObject(item.gameObject);
            }
            _gridItems.Clear();
            
            // Grab the items
            var itemModels = ShopManager.Instance.GetAllShopItemsForSection(SectionId);

            // Iterate through all of the items
            foreach (var item in itemModels)
            {
                // Grab a prefab from the pool
                var shopItem = await _pool.GetObjectForTypeAsync(item.PrefabName, ItemsParent);
                // Initialize it
                var shopGridItem = shopItem.GetComponent<ShopGridItem>();
                if (shopGridItem != null)
                {
                    shopGridItem.InitializeShopGridItem(item);
                    _gridItems.Add(shopGridItem);
                }
                
                shopGridItem.RT.anchoredPosition = Vector2.zero;
                shopItem.SetActive(true);
            }
        }
    }   

}
