namespace BigfootSdk.Shop
{
    public abstract class ActionShopTimer : ShopTimer
    {
        
        public override long GetRemainingTime()
        {
            if (IsTimerActive())
            {
                return base.GetRemainingTime();
            }
            return 0;
        }
        
        protected abstract bool IsTimerActive();
    }

    
}

