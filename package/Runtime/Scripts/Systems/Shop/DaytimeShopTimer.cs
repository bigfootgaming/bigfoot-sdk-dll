using System;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk.Shop
{
    
    public class DaytimeShopTimer : ShopTimer
    {
        public DateTime StartingTime;

       

        public override void StartTimer()
        {
            //get the next starting time (equal to current finishing time)
            DateTime serverTime = TimeHelper.GetTimeInServerDateTime();
            DateTime nextStartingTime =  DateTime.Today.AddHours(StartingTime.Hour).AddMinutes(StartingTime.Minute)
                .AddSeconds(StartingTime.Second);
            DateTime prevStartingTime = nextStartingTime.Subtract(new TimeSpan(0, 0, Duration));
            while (nextStartingTime < serverTime)
            {
                prevStartingTime = nextStartingTime;
                nextStartingTime = nextStartingTime.AddSeconds(Duration);
            }

            _timer = TimeHelper.ToUnixTime(prevStartingTime);
            PlayerManager.Instance.SetTimer(_id, _timer, GameModeManager.CurrentGameMode);
            OnTimerStarted?.Invoke(_id);
        }
    }
}

