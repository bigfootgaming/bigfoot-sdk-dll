using System;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk.Shop
{
    public class ShopTimer
    {
        public static Action<string> OnTimerStarted;
        
        public int Duration;
        protected string _id;
        protected long _timer;
        protected long _elapsedTime;

        protected virtual void InitTimer(string id)
        {
            _id = id;
            _timer = PlayerManager.Instance.GetTimer(_id, GameModeManager.CurrentGameMode);
        }

        public virtual long GetRemainingTime ()
        {
            _elapsedTime = TimeHelper.GetTimeElapsedSinceEvent(_timer);
            var remainingTime = Duration - _elapsedTime;
            return remainingTime;
        }
        
        public virtual long GetRemainingTime (string id)
        {
            InitTimer(id);
            _elapsedTime = TimeHelper.GetTimeElapsedSinceEvent(_timer);
            var remainingTime = Duration - _elapsedTime;
            return remainingTime;
        }

        public virtual void StartTimer()
        {
            _timer = TimeHelper.GetTimeInServer();
            PlayerManager.Instance.SetTimer(_id, _timer, GameModeManager.CurrentGameMode);
            OnTimerStarted?.Invoke(_id);
        }

        public virtual void EndTimer()
        {
        }

        public virtual bool UpdateTimer(string id)
        {
            InitTimer(id);
            if (GetRemainingTime() <= 0)
            {
                EndTimer();
                StartTimer();
                return true;
            }

            return false;
        }
    }
}

