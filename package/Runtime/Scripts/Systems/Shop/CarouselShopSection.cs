﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BigfootSdk.Layout;
using Sirenix.OdinInspector;

namespace BigfootSdk.Shop
{
    /// <summary>
    /// Carousel Shop Section
    /// </summary>
    public class CarouselShopSection : ShopSection
    {
        /// <summary>
        /// Flag to Auto Scroll the Carousel
        /// </summary>
        [BoxGroup("AutoScroll")]
        public bool AutoScroll = true;

        /// <summary>
        /// How many seconds to wait between auto scrolls
        /// </summary>
        [BoxGroup("AutoScroll")]
        public float AutoScrollSecondsStill;

        /// <summary>
        /// Once you manually scrolled, how much time to wait to resume the auto scroll
        /// </summary>
        [BoxGroup("AutoScroll")]
        public float AutoScrollSecondsToResume;

        /// <summary>
        /// The shop dot button asset
        /// </summary>
        [BoxGroup("Dot Navigation")]
        public string DotNavigationButtonPrefabName;

        /// <summary>
        /// The parent for the dot buttons
        /// </summary>
        [BoxGroup("Dot Navigation")]
        public HorizontalLayoutGroup DotNavigationButtonLayout;
        
        /// <summary>
        /// The ScrollableAreaHandler
        /// </summary>
        public ScrollableAreaHandler ScrollableArea;

        /// <summary>
        /// The current index of the carousel
        /// </summary>
        protected int _currentIndex;

        /// <summary>
        /// The shop dot buttons
        /// </summary>
        protected List<ShopDotButton> _shopDotButtons = new List<ShopDotButton>();

        void OnEnable()
        {
            // Start the Auto Scrolling
            if (AutoScroll)
                InvokeRepeating("DoScrollNext", AutoScrollSecondsStill, AutoScrollSecondsStill);
        }
        
        void OnDisable()
        {
            CancelInvoke("DoScrollNext");
        }
        
        public override async Task<bool> Initialize(ShopSectionModel sectionModel, ObjectPool pool)
        {
            if (await base.Initialize(sectionModel, pool))
            {
                RefreshDotButtonNavigation();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Manually scroll to the next item
        /// </summary>
        public void ScrollNext()
        {
            // Cancel the auto scroll, and program it to start after AutoScrollSecondsToResume
            if (AutoScroll)
            {
                CancelInvoke("DoScrollNext");
                InvokeRepeating("DoScrollNext", AutoScrollSecondsToResume, AutoScrollSecondsStill);
            }

            // Do the scrolling
            DoScrollNext();
        }

        /// <summary>
        /// Manually scroll to the previous item
        /// </summary>
        public void ScrollPrevious()
        {
            // Cancel the auto scroll, and program it to start after AutoScrollSecondsToResume
            if (AutoScroll)
            {
                CancelInvoke("DoScrollNext");
                InvokeRepeating("DoScrollNext", AutoScrollSecondsToResume, AutoScrollSecondsStill);
            }

            // Do the scrolling
            DoScrollPrevious();
        }

        /// <summary>
        /// Do the actual scroll to the next item
        /// </summary>
        void DoScrollNext()
        {
            // Get the index
            if (_currentIndex == ScrollableArea.transform.childCount - 1)
                _currentIndex = 0;
            else
                _currentIndex++;

            // Update the dots
            UpdateDotButtons();

            // Scroll
            ScrollableArea.ScrollToArea(_currentIndex + 1);
        }

        /// <summary>
        /// Do the actual scroll to the previous item
        /// </summary>
        void DoScrollPrevious()
        {
            // Get the index
            if (_currentIndex == 0)
                _currentIndex = ScrollableArea.transform.childCount - 1;
            else
                _currentIndex--;

            // Update the dots
            UpdateDotButtons();

            // Scroll
            ScrollableArea.ScrollToArea(_currentIndex + 1);
        }

        /// <summary>
        /// Create the dot button navigation
        /// </summary>
        protected virtual void RefreshDotButtonNavigation()
        {
            foreach (var dot in _shopDotButtons)
            {
                _pool.PoolObject(dot.gameObject);
            }
            _shopDotButtons.Clear();

            for (int i = 0; i < ScrollableArea.transform.childCount; i++)
            {
                // Create the dot
                var dotInstance = _pool.GetObjectForType(DotNavigationButtonPrefabName,
                    DotNavigationButtonLayout.transform);
                dotInstance.SetActive(true);
                var dotButton = dotInstance.GetComponent<ShopDotButton>();
            
                // Add it to the list
                if (dotButton != null)
                {
                    _shopDotButtons.Add(dotButton);
                }
                
            }
            
            // Rebuild the layout
            DotNavigationButtonLayout.Rebuild();

            // Update the dots
            UpdateDotButtons();
            
        }
        
        /// <summary>
        /// Update the dot buttons
        /// </summary>
        protected virtual void UpdateDotButtons()
        {
            if (_shopDotButtons != null)
            {
                for (int i = 0; i < _shopDotButtons.Count; i++)
                {
                    if (i == _currentIndex)
                        _shopDotButtons[i].SetActiveDot();
                    else
                        _shopDotButtons[i].SetInactiveDot();
                }
            }
        }
    }
}
