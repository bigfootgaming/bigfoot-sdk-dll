using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace BigfootSdk.Shop
{
    public class ShopPlacement : MonoBehaviour
    {
        public string Placement;

        public Transform SectionParent;

        protected List<ShopSection> _sections = new List<ShopSection>();

        protected ObjectPool _pool;

        protected bool _populated;

        protected virtual void OnDisable()
        {
            if (_populated)
            {
                ShopManager.OnShopRefreshed -= ShopRefreshed;
                _populated = false;
            }
        }

        public virtual void Populate(ObjectPool pool)
        {
            if (!_populated)
            {
                ShopManager.OnShopRefreshed += ShopRefreshed;
                _populated = true;
                _pool = pool;
                ShopManager.Instance.UpdateEnabledShop();
            }
        }

        protected virtual async void ShopRefreshed()
        {
            await Refresh();
        }

        public virtual async Task Refresh()
        {
            if (_populated)
            {
                foreach (var section in _sections)
                {
                    _pool.PoolObject(section.gameObject);
                }
                _sections.Clear();
                
                var sectionModels = ShopManager.Instance.GetShopSectionModelsForPlacement(Placement);
            
                foreach (var sectionModel in sectionModels)
                {
                    if (ShopManager.Instance.GetAllShopItemsForSection(sectionModel.Id).Count > 0)
                    {
                        // Grab the section from the pool
                        var newSection = _pool.GetObjectForType(sectionModel.PrefabName, SectionParent);
                        // Initialize the section
                        var shopSection = newSection.GetComponent<ShopSection>();
                        if (shopSection != null)
                        {
                            bool inited = await shopSection.Initialize(sectionModel, _pool);
                            if (inited)
                            {
                                newSection.SetActive(true);
                                _sections.Add(shopSection);
                                shopSection.RT.anchoredPosition = Vector2.zero;
                            }
                        }
                    }
                }
            }
        }
    }

}

