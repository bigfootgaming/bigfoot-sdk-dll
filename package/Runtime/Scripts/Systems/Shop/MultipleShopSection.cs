﻿
using System.Threading.Tasks;
using BigfootSdk.Layout;

namespace BigfootSdk.Shop
{
    /// <summary>
    /// Multiple Shop Section
    /// </summary>
    public class MultipleShopSection : ShopSection
    {
        /// <summary>
        /// The layout for the whole section
        /// </summary>
        public LayoutGroup SectionLayout;
        
        /// <summary>
        /// The layout containing the offers
        /// </summary>
        public LayoutGroup OffersLayout;
        
        public override async Task<bool> Initialize(ShopSectionModel sectionModel, ObjectPool pool)
        {
            if (await base.Initialize(sectionModel, pool))
            {
                // Rebuild the layout
                OffersLayout.Rebuild();
                SectionLayout.Rebuild();
                return true;
            }
            return false;
        }
        
        
    }
}
