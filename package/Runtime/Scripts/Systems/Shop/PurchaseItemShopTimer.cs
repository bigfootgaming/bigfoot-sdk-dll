using BigfootSdk.Backend;

namespace BigfootSdk.Shop
{
    public class PurchaseItemShopTimer : ActionShopTimer
    {
        public string ItemId;
        private bool _activated = false;
        private bool _suscribed = false;

        protected override void InitTimer(string id)
        {
            base.InitTimer(id);
            _activated = PlayerManager.Instance.GetFlag(_id, GameModeManager.CurrentGameMode);
            if (!_suscribed)
            {
                _suscribed = true;
                ShopManager.OnPurchaseSuccess += Activate;
            }
        }

        protected override bool IsTimerActive()
        {
            return _activated;
        }


        void Activate(string shopItemId, TransactionResultModel result, object purchase)
        {
            ShopItemModel itemModel = ShopManager.Instance.GetShopItemModel(shopItemId);
            bool shouldActivate = itemModel.Id == ItemId;
            if (shouldActivate)
            {
                _activated = true;
                PlayerManager.Instance.SetFlag(_id, _activated, GameModeManager.CurrentGameMode);
                StartTimer();
            }
        }

        public override void StartTimer()
        {
            if (_activated)
            {
                base.StartTimer();
            }
        }

        public override void EndTimer()
        {
            _activated = false;
            PlayerManager.Instance.SetFlag(_id, _activated, GameModeManager.CurrentGameMode);
        }
    }
}
