using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

namespace BigfootSdk.Shop
{
    public class FeaturedSection : ShopSection
    {
        private bool _timerRunning;
        private Coroutine _coroutine;
        private long _remainingTime;
        
        public override async Task<bool> Initialize(ShopSectionModel sectionModel, ObjectPool pool)
        {
            await base.Initialize(sectionModel, pool);
            return true;
        }

        protected override void InitializeTimer()
        {
            if (_sectionModel.Timer != null)
            {
                _timerRunning = false;
                _remainingTime = _sectionModel.Timer.GetRemainingTime();
                if (_remainingTime > 0)
                {
                    _timerRunning = true;
                    if (gameObject.activeInHierarchy)
                        _coroutine = StartCoroutine(CoTimer());
                }
            }
        }
        

        IEnumerator CoTimer()
        {
            yield return  new WaitForSeconds(_remainingTime);
            _timerRunning = false;
            EndedTimer();
        }

        protected override void EndedTimer()
        {
            _coroutine = null;
            base.EndedTimer();
        }

        private void OnEnable()
        {
            if (_timerRunning && _coroutine == null)
            {
                _coroutine = StartCoroutine(CoTimer());
            }
        }

        private void OnDisable()
        {
            if (_coroutine != null)
            {
                StopCoroutine(_coroutine);
                _coroutine = null;
            }
        }
    }

}

