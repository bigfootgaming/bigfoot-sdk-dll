﻿using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Currency Cost
    /// </summary>
    [System.Serializable]
    public class CurrencyCost : ICost
	{
        /// <summary>
        /// The currency key.
        /// </summary>
        public string CurrencyKey;

        /// <summary>
        /// The amount.
        /// </summary>
        public int Amount;

        /// <summary>
        /// Deducts the cost.
        /// </summary>
        public override void DeductCost(string origin, bool sendEvent)
        {
            PlayerManager.Instance.ChangeCurrency(CurrencyKey, Amount * -1, GameModeManager.CurrentGameMode);
            if (sendEvent)
	            TrackCostEvent(origin);
        }

        /// <summary>
        /// Returns if has enough to cover this cost
        /// </summary>
        /// <returns><c>true</c>, if the player has enough to cover the cost, <c>false</c> otherwise.</returns>
        public override bool HasEnough()
        {
            return PlayerManager.Instance.GetCurrencyBalance(CurrencyKey, GameModeManager.CurrentGameMode) >= Amount;
        }

        /// <summary>
        /// Is this an IAP
        /// </summary>
        /// <returns><c>true</c>, if this is an IAP, <c>false</c> otherwise.</returns>
        public override bool IsIAP()
        {
            return false;
        }

        /// <summary>
        /// Populates the transaction event.
        /// </summary>
        /// <param name="transactionEventModel">Transaction event model.</param>
        public override void PopulateTransactionEvent(TransactionEventModel transactionEventModel)
        {
            transactionEventModel.Sinks.Add(new TransactionEventParamenterModel() { Key = CurrencyKey, Amount = Amount, Type = "Currency" });
        }

        /// <summary>
        /// Tracks Cost event
        /// </summary>
        /// <param name="origin"></param>
        public override void TrackCostEvent(string origin)
        {
	        TransactionEventModel transactionEventModel = new TransactionEventModel()
	        {
		        TransactionType = "CurrencyCost",
		        Sinks = new List<TransactionEventParamenterModel>(),
		        Sources = new List<TransactionEventParamenterModel>(),
		        Origin = origin
	        };
	        PopulateTransactionEvent(transactionEventModel);
	        AnalyticsManager.Instance.TrackTransactionEvent(transactionEventModel);
        }

        /// <summary>
	    /// Returns the amount of this cost type as an object
	    /// </summary>
	    /// <returns>Object with the cost amount</returns>
	    public override object GetCost()
	    {
	        return Amount;
	    }

		/// <summary>
		/// Returns the type of this cost
		/// </summary>
		/// <returns></returns>
	    public override string GetCostType()
	    {
	        return CostType.Currency;
	    }

	    /// <summary>
        /// Reverts the deduct cost.
        /// </summary>
        public override void RevertDeductCost()
        {
            PlayerManager.Instance.ChangeCurrency(CurrencyKey, Amount, GameModeManager.CurrentGameMode);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:BigfootSdk.Backend.CurrencyCost"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:BigfootSdk.Backend.CurrencyCost"/>.</returns>
        public override string ToString()
        {
            return string.Format("Currency Cost :: Key: {0}  Amount: {1}", CurrencyKey, Amount);
        }
    }
}
