﻿using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Currency Cost
    /// </summary>
    [System.Serializable]
    public class CompositeCost : ICost
	{
        /// <summary>
        /// All of the costs
        /// </summary>
        public List<ICost> Costs;

        /// <summary>
        /// Deducts the cost.
        /// </summary>
        public override void DeductCost(string origin, bool sendEvent)
        {
            foreach (ICost cost in Costs)
            {
                cost.DeductCost(origin, false);
            }
            if (sendEvent)
                TrackCostEvent(origin);
        }

        /// <summary>
        /// Returns if has enough to cover this cost
        /// </summary>
        /// <returns><c>true</c>, if the player has enough to cover the cost, <c>false</c> otherwise.</returns>
        public override bool HasEnough()
        {
            foreach (ICost cost in Costs)
            {
                // If the player doesn't have enough for one, return false
                if (!cost.HasEnough())
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Is this an IAP (we will assume we dont have compisite costs with IAPs)
        /// </summary>
        /// <returns><c>true</c>, if this is an IAP, <c>false</c> otherwise.</returns>
        public override bool IsIAP()
        {
            return false;
        }

        /// <summary>
        /// Populates the transaction event.
        /// </summary>
        /// <param name="transactionEventModel">Transaction event model.</param>
        public override void PopulateTransactionEvent(TransactionEventModel transactionEventModel)
        {
            foreach (ICost cost in Costs)
            {
                cost.PopulateTransactionEvent(transactionEventModel);
            }
        }
        
        /// <summary>
        /// Tracks Cost event
        /// </summary>
        /// <param name="origin"></param>
        public override void TrackCostEvent(string origin)
        {
            TransactionEventModel transactionEventModel = new TransactionEventModel()
            {
                TransactionType = "CompositeCost",
                Sinks = new List<TransactionEventParamenterModel>(),
                Sources = new List<TransactionEventParamenterModel>(),
                Origin = origin
            };
            PopulateTransactionEvent(transactionEventModel);
            AnalyticsManager.Instance.TrackTransactionEvent(transactionEventModel);
        }

	    /// <summary>
	    /// Returns the amount of this cost type as an object
	    /// </summary>
	    /// <returns>Object with the cost amount</returns>
	    public override object GetCost()
	    {
	        return Costs;
	    }

	    /// <summary>
	    /// Returns the type of this cost
	    /// </summary>
	    /// <returns></returns>
	    public override string GetCostType()
	    {
	        return CostType.Composite;
	    }

	    /// <summary>
        /// Reverts the deduct cost.
        /// </summary>
        public override void RevertDeductCost()
        {
            foreach (ICost cost in Costs)
            {
                cost.RevertDeductCost();
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:BigfootSdk.Backend.CompositeCost"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:BigfootSdk.Backend.CompositeCost"/>.</returns>
        public override string ToString()
        {
            string result = "Composite Cost: \n";
            foreach (ICost cost in Costs)
            {
                result += string.Format("{0}\n", cost);
            }
            return result;
        }
    }
}
