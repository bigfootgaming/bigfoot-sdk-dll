﻿using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;
using BigfootSdk.Shop;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Real Money Cost
    /// </summary>
    [System.Serializable]
    public class RealMoneyCost : ICost
    {
        /// <summary>
        /// The GooglePlay SKU
        /// </summary>
        public string GoogleSKU;

        /// <summary>
        /// The IOS SKU
        /// </summary>
        public string IosSKU;

        /// <summary>
        /// Type of product 
        /// </summary>
        public string ProductType = "Consumable";

        /// <summary>
        /// The real money value of this offer
        /// </summary>
        public int RealMoneyValue;
        
        /// <summary>
        /// Deducts the cost locally.
        /// </summary>
        public override void DeductCost(string origin, bool sendEvent)
        {
            if (sendEvent)
                TrackCostEvent(origin);
        }

        /// <summary>
        /// Returns if has enough to cover this cost
        /// </summary>
        /// <returns><c>true</c>, if the player has enough to cover the cost, <c>false</c> otherwise.</returns>
        public override bool HasEnough()
        {
            return true;
        }

        /// <summary>
        /// Is this an IAP
        /// </summary>
        /// <returns><c>true</c>, if this is an IAP, <c>false</c> otherwise.</returns>
        public override bool IsIAP()
        {
            return true;
        }

        /// <summary>
        /// Populates the transaction event.
        /// </summary>
        /// <param name="transactionEventModel">Transaction event model.</param>
        public override void PopulateTransactionEvent(TransactionEventModel transactionEventModel)
        {
            transactionEventModel.Sinks.Add(new TransactionEventParamenterModel() { Key = "RM", Amount = 0, Type = "RM" });
        }
        
        /// <summary>
        /// Tracks Cost event
        /// </summary>
        /// <param name="origin"></param>
        public override void TrackCostEvent(string origin)
        {
            TransactionEventModel transactionEventModel = new TransactionEventModel()
            {
                TransactionType = "RealMoneyCost",
                Sinks = new List<TransactionEventParamenterModel>(),
                Sources = new List<TransactionEventParamenterModel>(),
                Origin = origin
            };
            PopulateTransactionEvent(transactionEventModel);
            AnalyticsManager.Instance.TrackTransactionEvent(transactionEventModel);
        }

        /// <summary>
        /// Returns the amount of this cost type as an object
        /// </summary>
        /// <returns>Object with the cost amount</returns>
        public override object GetCost()
        {
#if UNITY_ANDROID
            return ShopManager.Instance.GetLocalizedPrice(GoogleSKU);
#elif UNITY_IPHONE
            return ShopManager.Instance.GetLocalizedPrice(IosSKU);
#elif UNITY_WEBGL && WEBGL_KONGREGATE
            return KredCostAmount;
#else
            return "0";
#endif
        }

        /// <summary>
        /// Returns the type of this cost
        /// </summary>
        /// <returns></returns>
        public override string GetCostType()
	    {
		    return CostType.RealMoney;
	    }

	    /// <summary>
        /// Reverts the deduct cost locally.
        /// </summary>
        public override void RevertDeductCost()
        {

        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:BigfootSdk.Backend.RealMoneyCost"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:BigfootSdk.Backend.RealMoneyCost"/>.</returns>
        public override string ToString()
        {
            return string.Format("Real Money Cost :: GoogleSKU: {0}  IosSKU: {1}", GoogleSKU, IosSKU);
        }
    }
}
