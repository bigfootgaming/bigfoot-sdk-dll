﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Backend
{
    public static class CostType
    {
        public const string Composite = "Composite";
        public const string Currency = "Currency";
        public const string RealMoney = "RealMoney";
        public const string Resource = "Resource";
        public const string Item = "Item";
    }
}