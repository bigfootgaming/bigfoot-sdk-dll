﻿using BigfootSdk.Analytics;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Cost abstract class.
    /// </summary>
    public abstract class ICost
	{
        /// <summary>
        /// Is this an IAP
        /// </summary>
        /// <returns><c>true</c>, if this is an IAP, <c>false</c> otherwise.</returns>
        public abstract bool IsIAP();

        /// <summary>
        /// Returns if has enough to cover this cost
        /// </summary>
        /// <returns><c>true</c>, if the player has enough to cover the cost, <c>false</c> otherwise.</returns>
        public abstract bool HasEnough();

        /// <summary>
        /// Deducts the cost
        /// </summary>
        public abstract void DeductCost(string origin, bool sendEvent);

        /// <summary>
        /// Reverts the deduct cost.
        /// </summary>
        public abstract void RevertDeductCost();

        /// <summary>
        /// Populates the transaction event.
        /// </summary>
        /// <param name="transactionEventModel">Transaction event model.</param>
        public abstract void PopulateTransactionEvent(TransactionEventModel transactionEventModel);

        /// <summary>
        /// Tracks Cost event
        /// </summary>
        /// <param name="origin"></param>
        public abstract void TrackCostEvent(string origin);

        /// <summary>
        /// Returns the amount of this cost type as an object
        /// </summary>
        /// <returns>Object with the cost amount</returns>
        public abstract object GetCost();

        /// <summary>
        /// Returns the type of this cost
        /// </summary>
        /// <returns></returns>
        public abstract string GetCostType();
	}
}
