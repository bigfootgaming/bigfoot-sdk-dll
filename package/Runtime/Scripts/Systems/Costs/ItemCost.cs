﻿using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Item Cost
    /// </summary>
    [System.Serializable]
    public class ItemCost : ICost
	{
        /// <summary>
        /// The item name
        /// </summary>
        public string ItemName;

        /// <summary>
        /// The amount.
        /// </summary>
        public int Amount;

        /// <summary>
        /// The items to consume.
        /// </summary>
        List<PlayerItemModel> _itemsToConsume;

        /// <summary>
        /// Deducts the cost
        /// </summary>
        public override void DeductCost(string origin, bool sendEvent)
        {
            PlayerManager playerManager = PlayerManager.Instance;

            _itemsToConsume = new List<PlayerItemModel>();
            int amount = Amount;

            var playerItems = playerManager.GetPlayerItems(GameModeManager.CurrentGameMode);
            for (int i = 0; i < playerItems.Count; i++)
            {
                var currentItem = playerItems[i];
                if (currentItem.Name == ItemName && amount > 0)
                {
                    if (currentItem.AmountRemaining >= amount)
                    {
                        //clone the item in case we should restore it
                        _itemsToConsume.Add(new PlayerItemModel()
                        {
                            Id = currentItem.Id,
                            AmountRemaining = amount,
                            Data = currentItem.Data,
                            Name = currentItem.Name,
                            Type = currentItem.Type
                        });
                        //update amount
                        amount = 0;
                    }
                    else
                    {
                        //clone the item in case we should restore it
                        _itemsToConsume.Add(new PlayerItemModel()
                        {
                            Id = currentItem.Id,
                            AmountRemaining = currentItem.AmountRemaining,
                            Data = currentItem.Data,
                            Name = currentItem.Name,
                            Type = currentItem.Type
                        });
                        //update amount
                        amount -= currentItem.AmountRemaining;
                    }
                }
            }

            PlayerManager.Instance.ConsumeItems(_itemsToConsume, GameModeManager.CurrentGameMode); 
            
            if (sendEvent)
                TrackCostEvent(origin);
        }

        /// <summary>
        /// Returns if has enough to cover this cost
        /// </summary>
        /// <returns><c>true</c>, if the player has enough to cover the cost, <c>false</c> otherwise.</returns>
        public override bool HasEnough()
        {
            List<PlayerItemModel> items = PlayerManager.Instance.GetPlayerItemsByName(ItemName, GameModeManager.CurrentGameMode);
            int itemsAmount = 0;
            foreach (PlayerItemModel i in items)
                itemsAmount += i.AmountRemaining;
            return itemsAmount >= Amount;
        }

        /// <summary>
        /// Is this an IAP
        /// </summary>
        /// <returns><c>true</c>, if this is an IAP, <c>false</c> otherwise.</returns>
        public override bool IsIAP()
        {
            return false;
        }

        /// <summary>
        /// Populates the transaction event.
        /// </summary>
        /// <param name="transactionEventModel">Transaction event model.</param>
        public override void PopulateTransactionEvent(TransactionEventModel transactionEventModel)
        {
            transactionEventModel.Sinks.Add(new TransactionEventParamenterModel() { Key = ItemName, Amount = Amount, Type = "Item" });
        }
        
        /// <summary>
        /// Tracks Cost event
        /// </summary>
        /// <param name="origin"></param>
        public override void TrackCostEvent(string origin)
        {
            TransactionEventModel transactionEventModel = new TransactionEventModel()
            {
                TransactionType = "ItemCost",
                Sinks = new List<TransactionEventParamenterModel>(),
                Sources = new List<TransactionEventParamenterModel>(),
                Origin = origin
            };
            PopulateTransactionEvent(transactionEventModel);
            AnalyticsManager.Instance.TrackTransactionEvent(transactionEventModel);
        }

	    /// <summary>
	    /// Returns the amount of this cost type as an object
	    /// </summary>
	    /// <returns>Object with the cost amount</returns>
	    public override object GetCost()
	    {
	        return Amount;
	    }

		/// <summary>
		/// Returns the type of this cost
		/// </summary>
		/// <returns></returns>
	    public override string GetCostType()
	    {
            return CostType.Item;
	    }

	    /// <summary>
        /// Reverts the deduct cost.
        /// </summary>
        public override void RevertDeductCost()
        {
            if (_itemsToConsume != null)
                PlayerManager.Instance.AddItems(_itemsToConsume, GameModeManager.CurrentGameMode);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:BigfootSdk.Backend.CurrencyCost"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:BigfootSdk.Backend.CurrencyCost"/>.</returns>
        public override string ToString()
        {
            return string.Format("Item Cost :: Id: {0}  Amount: {1}", ItemName, Amount);
        }
    }
}
