﻿namespace BigfootSdk
{
	/// <summary>
	/// Prerequisite Interface.
	/// </summary>
	public abstract class IPrerequisite
	{
		/// <summary>
		/// Checks if the prerequisite is valid
		/// </summary>
		public virtual bool Check()
		{
			return false;
		}

		/// <summary>
		/// Localizes the prerequisite
		/// </summary>
		public virtual string Localize ()
		{
			return "";
		}
	}
}
