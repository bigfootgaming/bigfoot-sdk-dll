﻿using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk
{
	/// <summary>
	/// This prerequisite can be used to check if a user has a certain amount of currency
	/// </summary>
	[System.Serializable]
	public class CurrencyPrereq : IPrerequisite
	{
		/// <summary>
		/// The currency key.
		/// </summary>
		public string Currency;

		/// <summary>
		/// The amount.
		/// </summary>
		public int Amount;

		/// <summary>
		/// The comparison.
		/// </summary>
		public string Comparison = "eq";

		/// <summary>
		/// The game mode to use this in
		/// </summary>
		public string GameMode = GameModeConstants.NONE;
		
		/// <summary>
		/// Checks if the prerequisite is valid
		/// </summary>
		public override bool Check ()
		{
			// If we dont set a game mode, use the current one
			if (GameMode == GameModeConstants.NONE)
				GameMode = GameModeManager.CurrentGameMode;
			
			// Grab the amount of currency
			int amountCurrency = PlayerManager.Instance.GetCurrencyBalance (Currency, GameMode);

			// Make the comparisson
			return ComparisonHelper.Compare (amountCurrency, Amount, Comparison);
		}
	}
}
