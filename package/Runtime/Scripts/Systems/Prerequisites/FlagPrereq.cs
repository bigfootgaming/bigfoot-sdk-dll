﻿using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk
{
	/// <summary>
	/// This prerequisite can be used to check if certain flag is set on a user
	/// </summary>
	[System.Serializable]
	public class FlagPrereq : IPrerequisite
	{
		/// <summary>
		/// The flag key.
		/// </summary>
		public string Key;

		/// <summary>
		/// The value.
		/// </summary>
		public bool Value;

		/// <summary>
		/// The game mode
		/// </summary>
		public string GameMode = GameModeConstants.NONE;
		
		/// <summary>
		/// The comparison.
		/// </summary>
		public string Comparison = "eq";
		
		/// <summary>
		/// Checks if the prerequisite is valid
		/// </summary>
		public override bool Check ()
		{
			// If we dont set a game mode, use the current one
			if (GameMode == GameModeConstants.NONE)
				GameMode = GameModeManager.CurrentGameMode;
			
			var flagValue = PlayerManager.Instance.GetFlag(Key, GameMode);

			return ComparisonHelper.Compare(flagValue, Value, Comparison);
		}
	}
}
