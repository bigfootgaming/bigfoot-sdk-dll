
using BigfootSdk;
using BigfootSdk.Shop;

namespace BigfootSdk
{
    public class SubscriptionPrereq : IPrerequisite
    {
        public bool Subscribed;
        public string ItemName;

        public override bool Check()
        {
            string p;
            return Subscribed == ShopManager.Instance.HasExistingSubscriptionForGroup(ItemName, out p);
        }
    }
}