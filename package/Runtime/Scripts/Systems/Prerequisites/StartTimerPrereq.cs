using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk
{
    /// <summary>
    /// This prerequisite can be used to check if certain time has started before or after a given moment
    /// </summary>
    [System.Serializable]
    public class StartTimerPrereq : IPrerequisite
    {
        /// <summary>
        /// The timer key.
        /// </summary>
        public string Key;

        /// <summary>
        /// The amount.
        /// </summary>
        public int Amount;

        /// <summary>
        /// The comparison.
        /// </summary>
        public string Comparison = "eq";

        /// <summary>
        /// The game mode
        /// </summary>
        public string GameMode = GameModeConstants.NONE;
		
        /// <summary>
        /// Checks if the prerequisite is valid
        /// </summary>
        public override bool Check ()
        {
            // If we dont set a game mode, use the current one
            if (GameMode == GameModeConstants.NONE)
                GameMode = GameModeManager.CurrentGameMode;
			
            // Grab the amount of the timer
            long amountTimer = PlayerManager.Instance.GetTimer(Key, GameMode);
			
            // Make the comparison
            return ComparisonHelper.Compare (amountTimer, Amount, Comparison);
        }
    }
}