﻿using System;
using System.Globalization;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk
{
    /// <summary>
    /// This prerequisite can be used to compare the time in the server to a specified one
    /// </summary
    [System.Serializable]
    public class TimePrereq : IPrerequisite
    {
        /// <summary>
        /// The date to compare with
        /// </summary>
        public string Date;

        /// <summary>
        /// The comparison.
        /// </summary>
        public string Comparison = "eq";
		
        /// <summary>
        /// The date as a DateTime
        /// </summary>
        private DateTime _dateTime = DateTime.MinValue;
        public DateTime DateTime
        {
            get
            {
                if (_dateTime == DateTime.MinValue)
                {
                    DateTime.TryParse(Date, CultureInfo.InvariantCulture, DateTimeStyles.None, out _dateTime);
                }
                return _dateTime;
            }
        }

        /// <summary>
        /// The date as UnixTime
        /// </summary>
        private long _unixTime = 0;
        public long UnixTime
        {
            get
            {
                if (_unixTime == 0)
                {
                    _unixTime = TimeHelper.ToUnixTime(DateTime);
                }
                return _unixTime;
            }
        }

        /// <summary>
        /// Checks if the prerequisite is valid
        /// </summary>
        public override bool Check ()
        {
            if (DateTime != DateTime.MinValue)
            {
                // Make the comparison
                return ComparisonHelper.Compare (UnixTime, TimeHelper.GetTimeInServer(), Comparison);
            }
            else
            {
                // If we failed to parse the date, return false
                return false;
            }
        }
    }
}