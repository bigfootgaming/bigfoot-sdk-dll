﻿using BigfootSdk.Helpers;

namespace BigfootSdk
{
	/// <summary>
	/// This prerequisite can be used to check on which game mode we are
	/// </summary>
	[System.Serializable]
	public class GameModePrereq : IPrerequisite
	{
		/// <summary>
		/// The game mode
		/// </summary>
		public string GameMode;

		/// <summary>
		/// The comparison.
		/// </summary>
		public string Comparison = "eq";

		/// <summary>
		/// Checks if the prerequisite is valid
		/// </summary>
		public override bool Check ()
		{
			// Make the Comparison
			return ComparisonHelper.Compare (GameModeManager.CurrentGameMode, GameMode, Comparison);
		}
	}
}
