﻿using BigfootSdk.Tutorials;

namespace BigfootSdk
{
	/// <summary>
	/// This prerequisite can be used to check if certain tutorial is finished or not
	/// </summary>
	[System.Serializable]
	public class TutorialFinishedPrereq : IPrerequisite
	{
		/// <summary>
		/// The id of the Tutorial.
		/// </summary>
		public string TutorialId;

		/// <summary>
		/// Flag to check if the tutorial is finished or not
		/// </summary>
		public bool IsFinised = true;
		
		/// <summary>
		/// Checks if the prerequisite is valid
		/// </summary>
		public override bool Check ()
		{
			var tutorial = TutorialsManager.Instance.GetTutorial(TutorialId);

			if (tutorial == null)
				return false;

			return tutorial.IsCompleted() == IsFinised;
		}
	}
}
