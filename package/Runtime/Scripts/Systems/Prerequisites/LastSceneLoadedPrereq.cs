﻿using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.SceneManagement;
using UnityEngine;

namespace BigfootSdk
{
	/// <summary>
	/// This prerequisite can be used to check which was the last scene loaded
	/// </summary>
	[System.Serializable]
	public class LastSceneLoadedPrereq : IPrerequisite
	{
		/// <summary>
		/// The name of the scene
		/// </summary>
		public string SceneName;

		/// <summary>
		/// The comparison.
		/// </summary>
		public string Comparison = "eq";

		/// <summary>
		/// Checks if the prerequisite is valid
		/// </summary>
		public override bool Check ()
		{
			// Grab the amount of currency
			string lastSceneLoaded = BigfootSceneManager.LastScene;

			// Make the Comparison
			return ComparisonHelper.Compare (lastSceneLoaded, SceneName, Comparison);
		}
	}
}
