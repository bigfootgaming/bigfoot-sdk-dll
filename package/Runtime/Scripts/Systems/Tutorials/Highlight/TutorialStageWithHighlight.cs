﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial stage with highlight.
    /// </summary>
	public class TutorialStageWithHighlight : TutorialStage
	{
        /// <summary>
        /// The parent of highligthed objects. 
        /// </summary>
		public Image Parent;

        /// <summary>
        /// The highlight delay after setup.
        /// </summary>
		public float HighlightDelay;

        /// <summary>
        /// The highlighted objects.
        /// </summary>
		protected List<HighlightedItem> _highlightedObjects = new List<HighlightedItem>();

        /// <summary>
        /// On script enabled.
        /// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();
			TutorialsEvents.OnHighlightItem += HighlightItem;
		}

        /// <summary>
        /// On script disabled.
        /// </summary>
		protected override void OnDisable()
		{
            base.OnDisable();
			TutorialsEvents.OnHighlightItem -= HighlightItem;
		}

		/// <summary>
        /// Setup the stage.
        /// </summary>
        /// <param name="model">Model.</param>
        /// <param name="trigger">Trigger.</param>
        public override void Setup(TutorialStageModel model, TutorialTrigger trigger)
        {
            base.Setup(model, trigger);
            Parent.enabled = false;
            StartCoroutine(Highlight());
        }

        /// <summary>
        /// Trigger the highlight.
        /// </summary>
        /// <returns>The highlight.</returns>
        protected virtual IEnumerator Highlight()
        {
            yield return new WaitForSecondsRealtime(HighlightDelay);
            Parent.enabled = true;
            TutorialsEvents.HighlightTutorialShown(TutorialId, _model.Name);
        }

        /// <summary>
        /// Highlights the item.
        /// </summary>
        /// <param name="item">Item.</param>
        /// <param name="translatePos">If set to <c>true</c> translate position.</param>
        protected virtual void HighlightItem(GameObject item, bool translatePos)
		{
			// Add the current items
			_highlightedObjects.Add(new HighlightedItem() { Item = item, Parent = item.transform.parent, TranslatePosition = translatePos, LocalPosition = item.transform.localPosition, Scale = item.transform.localScale, SiblingIndex = item.transform.GetSiblingIndex()});
			_highlightedObjects.Sort(((item1, item2) => item2.SiblingIndex - item1.SiblingIndex));
			
			Vector3 screenPos = Camera.main.WorldToScreenPoint(item.transform.position);

            item.transform.SetParent(Parent.transform, true);

			item.transform.SetAsLastSibling();

			if (translatePos)
			{
				float scaleFactorX = FindObjectOfType<CanvasScaler>().referenceResolution.x / Screen.width;
				float scaleFactorY = FindObjectOfType<CanvasScaler>().referenceResolution.y / Screen.height;

				item.GetComponent<RectTransform>().anchoredPosition = new Vector2(screenPos.x * scaleFactorX, screenPos.y * scaleFactorY);
			}
		}

        /// <summary>
        /// Handles the stage completion. Returns the highlighted items to its original place.
        /// </summary>
		protected override void HandleStageCompletion()
		{
			base.HandleStageCompletion();
			if (_highlightedObjects != null)
			{
				for (int i = 0; i < _highlightedObjects.Count; i++)
				{
					var entry = _highlightedObjects[i];
					if (entry != null)
					{
						entry.Item.transform.SetParent(entry.Parent, true);
						entry.Item.transform.localScale = entry.Scale;
						entry.Item.transform.SetSiblingIndex(entry.SiblingIndex);
						if (entry.TranslatePosition)
						{
							entry.Item.transform.localPosition = entry.LocalPosition;
						}
					}
				}
			}
            if(Parent != null)
				Parent.enabled = false;
        }
	}

    /// <summary>
    /// Highlighted item.
    /// </summary>
	public class HighlightedItem
	{
        /// <summary>
        /// The item.
        /// </summary>
		public GameObject Item;

        /// <summary>
        /// The parent.
        /// </summary>
		public Transform Parent;

        /// <summary>
        /// The sibling index
        /// </summary>
        public int SiblingIndex;

        /// <summary>
        /// if <see langword="true"/> the position will change when hinhlighted.
        /// </summary>
		public bool TranslatePosition;

        /// <summary>
        /// The local position.
        /// </summary>
		public Vector3 LocalPosition;

        /// <summary>
        /// The scale.
        /// </summary>
		public Vector3 Scale;
	}
}