﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial highlight item.
    /// </summary>
    public class TutorialHighlightItem : MonoBehaviour
    {
        /// <summary>
        /// The tutorial identifier.
        /// </summary>
        public string TutorialId;

        /// <summary>
        /// The name of the stage.
        /// </summary>
        public string StageName;

        /// <summary>
        /// if <see langword="true"/> the position will change when hinhlighted.
        /// </summary>
        public bool TranslatePosition;

        /// <summary>
        /// On script enabled.
        /// </summary>
        void OnEnable()
        {
            TutorialsEvents.OnHighlightTutorialShown += HighlightTutorialShown;
        }

        /// <summary>
        /// On script disabled.
        /// </summary>
        void OnDisable()
        {
            TutorialsEvents.OnHighlightTutorialShown -= HighlightTutorialShown;
        }

        /// <summary>
        /// The highlight tutorial is shown
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stageName">Stage name.</param>
        void HighlightTutorialShown(string tutorialId, string stageName)
        {
            if (tutorialId == TutorialId && stageName == StageName)
            {
                TutorialsEvents.HighlightItem(gameObject, TranslatePosition);
            }
        }
    }
}

