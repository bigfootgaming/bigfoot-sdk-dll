﻿using UnityEngine;
using System;
using BigfootSdk.Panels;
using BigfootSdk.Analytics;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial handler.
    /// </summary>
	[Serializable]
    public class TutorialHandler
    {
        /// <summary>
        /// Should this tutorial be skipped? (Ignored in production)
        /// </summary>
        public bool ShouldSkipTutorial;

        /// <summary>
        /// The tutorial identifier.
        /// </summary>
		public string TutorialId = "TutorialXXX";
        
        /// <summary>
        /// Show tutorial in event mode
        /// </summary>
        public bool TutorialActiveInEventMode;

        /// <summary>
        /// The tutorial panels manager identifier.
        /// </summary>
        public int TutorialPanelsManagerId;

        /// <summary>
        /// The tutorial dependencies.
        /// </summary>
		public string[] Dependencies;

        /// <summary>
        /// The steps.
        /// </summary>
		public TutorialStageModel[] Stages;

        /// <summary>
        /// The current stage number.
        /// </summary>
        int _currentStageNumber = 0;

        public int CurrentStageNumber => _currentStageNumber;

        /// <summary>
        /// The current stage.
        /// </summary>
        [HideInInspector]
        public string CurrentStage;

        /// <summary>
        /// If <see langword="true"/> current stage already started.
        /// </summary>
        bool _currentStageStarted;

        #region Tutorial Progression

        /// <summary>
        /// Loads the tutorial progression.
        /// </summary>
        /// <param name="progress">Progress.</param>
        public void LoadTutorialProgress(TutorialProgressModel progress)
        {
			_currentStageStarted = false;
			_currentStageNumber = progress.CurrentStage;
			if(Stages.Length > 0 && _currentStageNumber < Stages.Length)
            {
                CurrentStage = Stages[_currentStageNumber].Name;
            }
        }



		#endregion

        /// <summary>
        /// Starts the stage.
        /// </summary>
        /// <param name="stageName">Step name.</param>
        /// <param name="triggeredBy">Triggered by.</param>
		public bool StartStage(string stageName, TutorialTrigger triggeredBy)
        {
            if (CheckGameMode() && (string.IsNullOrEmpty (CurrentStage) && Stages[0].Name == stageName || CurrentStage.Equals (stageName) && !_currentStageStarted) 
                                && MeetsDependencies() && _currentStageNumber < Stages.Length)
            {
                ShowStage (stageName, triggeredBy);
                return true;
            }

            return false;
        }
        
        bool CheckGameMode()
        {
            return (TutorialActiveInEventMode || GameModeManager.CurrentGameMode == GameModeConstants.MAIN_GAME);
        }

        /// <summary>
        /// Shows the tutorial if needed.
        /// </summary>
        /// <param name="stageName">Stage name.</param>
        /// <param name="triggeredBy">Triggered by.</param>
        void ShowStage(string stageName, TutorialTrigger triggeredBy)
        {
            var stageModel = Stages[_currentStageNumber];

            if (_currentStageNumber == 0)
            {
                TutorialsManager.Instance.TutorialStarted(TutorialId);
            }

            //Everything is ok, let's get to work
            //Set up extra info
            stageModel.Name = Stages[_currentStageNumber].Name;
            CurrentStage = Stages[_currentStageNumber].Name;
            _currentStageStarted = true;

            PanelConfigModel panel = new PanelConfigModel()
            {
                AcceptHardwareBack = stageModel.AcceptHardwareBack,
                PanelName = stageName,
                UseOverlay = stageModel.UseOverlay,
                WaitForPanelAnimations = true,
                Position = Vector3.zero
            };

            LogHelper.LogSdk("Show stage " + stageName + " (" + TutorialId + ")", LogHelper.TUTORIAL_TAG);

            int panelManagerToUse = stageModel.OverridePanelManagerId
                ? stageModel.OverridenPanelManagerId
                : TutorialPanelsManagerId;
            PanelsEvents.ShowNewPanel(panel, PanelBehavior.RemoveFromStack, (pgo) => {
                TutorialStage stage = pgo.GetComponent<TutorialStage>();
                if (stage != null)
                    stage.Setup(stageModel, triggeredBy);
                TutorialsEvents.StageStarted(TutorialId, stageModel);
            }, (pgo) =>
            {
                int index = TutorialsManager.Instance.GetStartingTutorialIndex(TutorialId) + _currentStageNumber;
                if (index == 0)
                    AnalyticsManager.Instance.TrackTutorialStartedEvent();
                AnalyticsManager.Instance.TrackTutorialEvent(TutorialId, stageName, index, "STARTED");
                TutorialStage stage = pgo.GetComponent<TutorialStage>();
                if (stage != null)
                    stage.StageShown();
            }, panelManagerToUse);
        }

        /// <summary>
        /// Meets the dependencies.
        /// </summary>
        /// <returns><c>true</c>, if dependencies was meetsed, <c>false</c> otherwise.</returns>
        bool MeetsDependencies()
        {
            foreach (var d in Dependencies)
            {
                if (!TutorialsManager.Instance.IsCompleted(d))
                {
                    return false;
                }
            }
            return true;
        }



        /// <summary>
        /// Stage completed.
        /// </summary>
        /// <param name="stageName">Stage name.</param>
		public void StageCompleted(string stageName)
        {
            if (CheckGameMode() && stageName.Equals(CurrentStage) && _currentStageNumber < Stages.Length)
			{
                LogHelper.LogSdk("Stage completed " + stageName + " (" + TutorialId + ")", LogHelper.TUTORIAL_TAG);

                //Check if the completed stage was a milestone
                bool isMilestone = Stages[_currentStageNumber].IsMilestone;

                int index = TutorialsManager.Instance.GetStartingTutorialIndex(TutorialId) + _currentStageNumber;

                //Advance
                _currentStageNumber++;

                _currentStageStarted = false;

                AnalyticsManager.Instance.TrackTutorialEvent(TutorialId, stageName, index, "COMPLETED");

                //If it was a milestone, then we'll save the new starting point
                if (isMilestone)
                {
                    AnalyticsManager.Instance.TrackMilestoneEvent(index);
                    if (IsCompleted())
                    {
                        TutorialsManager.Instance.SaveTutorialProgress(TutorialId, Stages.Length);
                        if (TutorialsManager.Instance.IsLastTutorial(TutorialId))
                            AnalyticsManager.Instance.TrackTutorialEndedEvent();
                    }
                    else
                    {
                        TutorialsManager.Instance.SaveTutorialProgress(TutorialId, _currentStageNumber);
                    }
                }

                // Get the previous stage model
                var previousStageModel = Stages[_currentStageNumber - 1];

                if (_currentStageNumber < Stages.Length)
				{
                    // Get the current stage model
                    var currentStageModel = Stages[_currentStageNumber];

                    // If we are overriding the panel manager id
										if (currentStageModel.OverridePanelManagerId || previousStageModel.OverridePanelManagerId)
                    {
                        // See which panel id the previous one had
                        int panelManagerToUse = previousStageModel.OverridePanelManagerId
                            ? previousStageModel.OverridenPanelManagerId
                            : TutorialPanelsManagerId;

                        // Do a back on it
                        PanelsEvents.Back(panelManagerToUse);
                    }

					CurrentStage = Stages [_currentStageNumber].Name;
                    //Send event to keep goin
                    TutorialsEvents.StageIsReadyToStart(TutorialId, CurrentStage);
                }
                else
                {
                    CurrentStage = "";

                    int panelManagerToUse = previousStageModel.OverridePanelManagerId
                        ? previousStageModel.OverridenPanelManagerId
                        : TutorialPanelsManagerId;
                    PanelsEvents.Back(panelManagerToUse);

                    if (!HasAnyMilestone())
                    {
                        _currentStageNumber = 0;
                    }

                    TutorialsManager.Instance.TutorialCompleted(TutorialId);
                }
            }
		}

        /// <summary>
        /// Has milestone.
        /// </summary>
        /// <returns><c>true</c>, if the tutorial has any milestone, <c>false</c> otherwise.</returns>
		bool HasAnyMilestone ()
		{
			for (int i = 0; i < Stages.Length; i++)
			{
				if (Stages [i].IsMilestone)
					return true;
			}
			return false;
		}

        /// <summary>
        /// Is completed.
        /// </summary>
        /// <returns><c>true</c>, if completed was ised, <c>false</c> otherwise.</returns>
		public bool IsCompleted()
        {
            int lastMilestoneNumber = 0;
            for (int i = 0; i < Stages.Length; i++)
            {
                if (Stages[i].IsMilestone)
                    lastMilestoneNumber = i;
            }
            //Is completed if the amount of stagges, is equals to the current stage
			return lastMilestoneNumber < _currentStageNumber;
		}

        /// <summary>
        /// Is the stage completed?
        /// </summary>
        /// <param name="stageNumber">the stage we want to check agains</param>
        /// <returns>if it's completed</returns>
        public bool IsStageCompleted(int stageNumber)
        {
            return stageNumber <= _currentStageNumber;
        }


        /// <summary>
        /// Skips the tutorial.
        /// </summary>
		public bool SkipTutorial()
        {
            if (CheckGameMode())
            {
                _currentStageNumber = Stages.Length + 1;

                LogHelper.LogSdk("Tutorial skipped (" + TutorialId + ")", LogHelper.TUTORIAL_TAG);
                int index = TutorialsManager.Instance.GetStartingTutorialIndex(TutorialId) + _currentStageNumber;
                AnalyticsManager.Instance.TrackTutorialEvent(TutorialId, CurrentStage, index,"SKIPPED");

                PanelsEvents.Back(TutorialPanelsManagerId);
                _currentStageStarted = false;

                TutorialsManager.Instance.TutorialCompleted(TutorialId);
                return true;
            }

            return false;
        }

        public bool ResetTutorial()
        {
            if (CheckGameMode())
            {
                _currentStageNumber = 0;
                CurrentStage = Stages[_currentStageNumber].Name;
                LogHelper.LogSdk("Tutorial reset (" + TutorialId + ")", LogHelper.TUTORIAL_TAG);
                int index = TutorialsManager.Instance.GetStartingTutorialIndex(TutorialId) + _currentStageNumber;
                AnalyticsManager.Instance.TrackTutorialEvent(TutorialId, CurrentStage, index,"RESET");
                _currentStageStarted = false;
                return true;
            }

            return false;
        }
    }
}
