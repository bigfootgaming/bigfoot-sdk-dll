﻿
using UnityEngine;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial stage.
    /// </summary>
    public class TutorialStage : MonoBehaviour
    {
        /// <summary>
        /// The tutorial identifier.
        /// </summary>
        public string TutorialId;

        /// <summary>
        /// The model.
        /// </summary>
        protected TutorialStageModel _model;

        /// <summary>
        /// The trigger.
        /// </summary>
        protected TutorialTrigger _trigger;


        /// <summary>
        /// On script enabled.
        /// </summary>
        protected virtual void OnEnable()
        {
            TutorialsEvents.OnCompleteStage += CompleteStage;
            TutorialsEvents.OnSkipTutorial += SkipTutorial;
        }

        /// <summary>
        /// On script disabled.
        /// </summary>
        protected virtual void OnDisable()
        {
            TutorialsEvents.OnCompleteStage -= CompleteStage;
            TutorialsEvents.OnSkipTutorial -= SkipTutorial;
        }

        /// <summary>
        /// Setup the stage.
        /// </summary>
        /// <param name="model">Model.</param>
        /// <param name="trigger">Trigger.</param>
        public virtual void Setup(TutorialStageModel model, TutorialTrigger trigger)
        {
            _model = model;
            _trigger = trigger;
        }

        public virtual void StageShown()
        {
        }


        /// <summary>
        /// Completes the stage.
        /// </summary>
        /// <param name="tutId">Tut identifier.</param>
        /// <param name="stageName">Stage name.</param>
        void CompleteStage(string tutId, string stageName)
        {
            if (TutorialId == tutId && _model != null && _model.Name == stageName)
            {
                HandleStageCompletion();
            }
        }

        /// <summary>
        /// Handles the stage completion.
        /// </summary>
        protected virtual void HandleStageCompletion()
        {
            TutorialsManager.Instance.StageCompleted(TutorialId, _model.Name);
        }

        /// <summary>
        /// Skips the tutorial.
        /// </summary>
        /// <param name="tutId">Tut identifier.</param>
        /// <param name="stageName">Stage name.</param>
        void SkipTutorial (string tutId, string stageName)
        {
            if (TutorialId == tutId && _model != null && _model.Name == stageName)
            {
                HandleTutorialSkipped();
            }
        }

        /// <summary>
        /// Handles the tutorial skipped.
        /// </summary>
        protected virtual void HandleTutorialSkipped()
        {
            TutorialsManager.Instance.SkipTutorial(TutorialId);
        }
	}
}