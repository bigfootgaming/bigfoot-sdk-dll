﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using BigfootSdk.Panels;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial trigger.
    /// </summary>
	public class TutorialTrigger : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler
    {
        /// <summary>
        /// The trigger models.
        /// </summary>
        public List<TutorialTriggerModel> TriggerModels;

        /// <summary>
        /// If <see langword="true"/> it is suscribed to OnStageIsReadyToStart and OnTutorialCompleted events.
        /// </summary>
		protected bool _suscribed;

        /// <summary>
        /// The panel game object attached to this game object.
        /// </summary>
        protected PanelGameObject _panelGameObject;

        /// <summary>
        /// The mouse in collider flag.
        /// </summary>
        bool _mouseIn;

        /// <summary>
        /// On script enabled
        /// </summary>
        protected virtual void OnEnable()
        {
            DependencyManager.OnEverythingLoaded += OnEverythingLoaded;
            TutorialsEvents.OnStageStarted += SomeStageStarted;
            _panelGameObject = GetComponent<PanelGameObject>();
            if (_panelGameObject != null)
            {
                PanelsEvents.OnPanelShown += PanelShown;
                PanelsEvents.OnPanelHidden += PanelHidden;
            }


			if (TriggerModels != null)
			{
				foreach (var t in TriggerModels)
				{
					if (t.TriggerType == TutorialTriggerType.OnEnable)
					{
						TryTriggerAction(t);
					}

					if (t.ListenForRetriggers && !_suscribed)
					{
						TutorialsEvents.OnStageIsReadyToStart += Retrigger;
						TutorialsEvents.OnTutorialCompleted += Retrigger;
						_suscribed = true;
					}
				}
			}
        }

        /// <summary>
        /// On script disabled.
        /// </summary>
        protected virtual void OnDisable()
        {
            DependencyManager.OnEverythingLoaded -= OnEverythingLoaded;
            TutorialsEvents.OnStageStarted -= SomeStageStarted;
            if (_panelGameObject)
            {
                PanelsEvents.OnPanelShown -= PanelShown;
                PanelsEvents.OnPanelHidden -= PanelHidden;
            }


            foreach (var t in TriggerModels)
            {
                if (t.TriggerType == TutorialTriggerType.OnDisable)
                {
                    TryTriggerAction(t);
                }

                if (t.ListenForRetriggers && _suscribed)
                {
                    TutorialsEvents.OnStageIsReadyToStart -= Retrigger;
                    TutorialsEvents.OnTutorialCompleted -= Retrigger;
                    _suscribed = false;
                }
            }
        }

        /// <summary>
        /// On script started.
        /// </summary>
        protected virtual void Start()
        {
            foreach (var t in TriggerModels)
            {
                if (t.TriggerType == TutorialTriggerType.OnStart)
                {
                    TryTriggerAction(t);
                }
            }
        }

        /// <summary>
        /// Ons script destroyed.
        /// </summary>
        protected virtual void OnDestroy()
        {
            foreach (var t in TriggerModels)
            {
                if (t.TriggerType == TutorialTriggerType.OnDestroy)
                {
                    TryTriggerAction(t);
                }
            }
        }

        /// <summary>
        /// On everything loaded (from DependencyManager).
        /// </summary>
        protected void OnEverythingLoaded()
        {
            foreach (var t in TriggerModels)
            {
                if (t.TriggerType == TutorialTriggerType.OnEverythingLoaded)
                {
                    TryTriggerAction(t);
                }
            }
        }

        /// <summary>
        /// On panel hidden.
        /// </summary>
        /// <param name="panel">Panel.</param>
        /// <param name="panelId">Panel identifier.</param>
        protected void PanelHidden(PanelModel panel, int panelId)
        {

            foreach (var t in TriggerModels)
            {
                if (t.TriggerType == TutorialTriggerType.OnPanelHidden && panel.PanelGO == _panelGameObject)
                {
                    TryTriggerAction(t);
                }
            }
        }

        /// <summary>
        /// On panel shown.
        /// </summary>
        /// <param name="panel">Panel.</param>
        /// <param name="panelId">Panel identifier.</param>
        protected void PanelShown(PanelModel panel, int panelId)
        {
            foreach (var t in TriggerModels)
            {
                if (t.TriggerType == TutorialTriggerType.OnPanelShown && panel.PanelGO == _panelGameObject)
                {
                    TryTriggerAction(t);
                }
            }
        }

        /// <summary>
        /// On pointer click over UI element.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            foreach (var t in TriggerModels)
            {
                if (t.TriggerType == TutorialTriggerType.OnClick)
                {
                    TryTriggerAction(t);
                }
            }
        }

        /// <summary>
        /// On mouse up.
        /// </summary>
        void OnMouseUp()
        {
            if (_mouseIn)
            {
                foreach (var t in TriggerModels)
                {
                    if (t.TriggerType == TutorialTriggerType.OnClick)
                    {
                        TryTriggerAction(t);
                    }
                }
                _mouseIn = false;
            }
        }


        /// <summary>
        /// On pointer enter in UI element.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            foreach (var t in TriggerModels)
            {
                if (t.TriggerType == TutorialTriggerType.OnEnter)
                {
                    TryTriggerAction(t);
                }
            }
        }

        /// <summary>
        /// On mouse down.
        /// </summary>
        void OnMouseDown()
        {
            _mouseIn = true;
        }

        /// <summary>
        /// On mouse enter.
        /// </summary>
        void OnMouseEnter()
        {
            _mouseIn = true;
            foreach (var t in TriggerModels)
            {
                if (t.TriggerType == TutorialTriggerType.OnEnter)
                {
                    TryTriggerAction(t);
                }
            }
        }

        /// <summary>
        /// On mouse exit.
        /// </summary>
        void OnMouseExit()
        {
            _mouseIn = false;
        }

        /// <summary>
        /// Some stage has started, so the previous trigger already finished.
        /// </summary>
        /// <param name="tutorialId"></param>
        /// <param name="stage"></param>
        void SomeStageStarted(string tutorialId, TutorialStageModel stage)
        {
            //Stop all coruotines started before the active stage has started
            StopAllCoroutines();
        }

        /// <summary>
        /// Retrigger the specified tutorialId and StageName.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stageName">Stage name.</param>
        protected virtual void Retrigger(string tutorialId, string stageName)
        {
            foreach (var t in TriggerModels)
            {				
				if (tutorialId.Equals (t.TutorialId) && stageName.Equals (t.StageName) && t.ListenForRetriggers)
				{
					TryTriggerAction (t);
				}
            }
        }

        /// <summary>
        /// Others the tutorial completed.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
		protected virtual void Retrigger (string tutorialId)
		{
			foreach (var t in TriggerModels)
			{
				if (t.ListenForRetriggers)
				{
					TryTriggerAction (t);
				}
			}
		}



        /// <summary>
        /// Tries to trigger the action.
        /// </summary>
        /// <param name="model">Model.</param>
        public virtual void TryTriggerAction(TutorialTriggerModel model)
        {
            if (CanBeTriggerAfter(model.TriggerType))
            {
                if(model.TriggerAfterSeconds > Mathf.Epsilon)
                {
                    StartCoroutine(TriggerAfterSeconds(model));
                }
                else if(model.TriggerAfterFrames != 0)
                {
                    StartCoroutine(TriggerAfterFrames(model));
                }
                else
                {
                    TriggerAction(model);
                }
            }
            else
            {
                TriggerAction(model);
            }
        }

        /// <summary>
        /// Can be trigger delayed.
        /// </summary>
        /// <returns><c>true</c>, if be trigger after was caned, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        public bool CanBeTriggerAfter (TutorialTriggerType type)
        {
            return (type == TutorialTriggerType.OnClick ||
                type == TutorialTriggerType.OnEnable ||
                type == TutorialTriggerType.OnEnter ||
                type == TutorialTriggerType.OnPanelShown ||
                type == TutorialTriggerType.OnStart ||
                type == TutorialTriggerType.OnEverythingLoaded ||
                type == TutorialTriggerType.Other);
        }

        /// <summary>
        /// Triggers after seconds.
        /// </summary>
        /// <returns>The seconds.</returns>
        /// <param name="model">Cfg.</param>
        IEnumerator TriggerAfterSeconds(TutorialTriggerModel model)
        {
            yield return new WaitForSecondsRealtime(model.TriggerAfterSeconds);
            TriggerAction(model);
        }

        /// <summary>
        /// Triggers after frames.
        /// </summary>
        /// <returns>The frames.</returns>
        /// <param name="model">Model.</param>
        private IEnumerator TriggerAfterFrames(TutorialTriggerModel model)
        {
            var frames = 0;
            while (frames < model.TriggerAfterFrames)
            {
                yield return null;
                frames++;
            }
            TriggerAction(model);
        }

        /// <summary>
        /// Triggers the action.
        /// </summary>
        /// <param name="model">Cfg.</param>
        public virtual void TriggerAction(TutorialTriggerModel model)
        {
            switch (model.Action)
            {
                case TutorialAction.StartStage:
                    TutorialsManager.Instance.StartStage(model.TutorialId, model.StageName, this);
                    break;
			    case TutorialAction.CompleteStage:
                    TutorialsEvents.CompleteStage(model.TutorialId, model.StageName);
                    break;
                case TutorialAction.SkipTutorial:
                    TutorialsEvents.SkipTutorial(model.TutorialId, model.StageName);
                    break;
            }
        }
    }
}