﻿using System.Collections;
using System.Collections.Generic;
using BigfootSdk.Localization;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial stage with character.
    /// </summary>
    public class TutorialStageWithCharacter : TutorialStage
    {
        /// <summary>
        /// The name of the dialog prefab.
        /// </summary>
        public string DialogPrefabName;

        /// <summary>
        /// The localization key for the dialog text.
        /// </summary>
        public string LocalizationKey;

        /// <summary>
        /// Time in second to show the next buttton in the dialog.
        /// </summary>
        public float NextButtonDelay;

        /// <summary>
        /// The name of the character.
        /// </summary>
        public string CharacterName;

        /// <summary>
        /// If <see langword="true"/> will show the character at the beginning of the stage.
        /// </summary>
        public bool ShowCharacterAtBeginning;

        /// <summary>
        /// The animation model.
        /// </summary>
        public TutorialCharacterAnimationModel AnimationModel;

        /// <summary>
        /// If <see langword="true"/> will hide the character with tween position animation at the end of the stage.
        /// </summary>
        public bool HideCharacterAtEnd;

        /// <summary>
        /// If <see langword="true"/> the dialog will be hidden first, and then the character. Otherwise, they will hide simultaneously.
        /// </summary>
        public bool HideSequential;

        /// <summary>
        /// If <see langword="true"/> the dialog has been hidden.
        /// </summary>
        protected bool _dialogHidden;

        /// <summary>
        /// If <see langword="true"/> the character has been hidden.
        /// </summary>
        protected bool _characterHidden;

        /// <summary>
        /// Setup the stage.
        /// </summary>
        /// <param name="model">Model.</param>
        /// <param name="trigger">Trigger.</param>
        public override void Setup(TutorialStageModel model, TutorialTrigger trigger)
        {
            base.Setup(model, trigger);
            if (ShowCharacterAtBeginning)
                TutorialCharacterManager.Instance.ShowCharacter(CharacterName, DialogPrefabName, PlayAnimation);
            else
                PlayAnimation();
        }

        /// <summary>
        /// Plays the animation.
        /// </summary>
        void PlayAnimation ()
        {
            TutorialCharacterManager.Instance.PlayAnimation(CharacterName, AnimationModel, DialogPrefabName, LocalizationKey, NextButtonDelay);
        }

        /// <summary>
        /// Handles the stage completion.
        /// </summary>
        protected override void HandleStageCompletion()
        {
            if (!string.IsNullOrEmpty(DialogPrefabName))
            {
                if (HideSequential)
                {
                    //hide dialog, then character
                    TutorialCharacterManager.Instance.HideDialog(DialogPrefabName, DialogHiddenSequentially);
                } 
                else
                {
                    //hide dialog and character at the same time
                    TutorialCharacterManager.Instance.HideDialog(DialogPrefabName, DialogHiddenParallelly);
                    HideCharacter();
                }
            }
            else
            {
                HideCharacter();
            }   
        }

        /// <summary>
        /// Dialog hidden sequentially.
        /// </summary>
        void DialogHiddenSequentially ()
        {
            _dialogHidden = true;
            HideCharacter();
        }

        /// <summary>
        /// Dialog hidden parallelly.
        /// </summary>
        void DialogHiddenParallelly ()
        {
            _dialogHidden = true;
            CheckToComplete();
        }

        /// <summary>
        /// Hides the character.
        /// </summary>
        void HideCharacter ()
        {
            if (HideCharacterAtEnd)
                TutorialCharacterManager.Instance.HideCharacter(CharacterName, CharacterHidden);
            else
                CheckToComplete();
        }

        /// <summary>
        /// Character hidden.
        /// </summary>
        void CharacterHidden ()
        {
            _characterHidden = true;
            CheckToComplete();
        }

        /// <summary>
        /// Checks if dialog and characters are in its final state to complete.
        /// </summary>
        void CheckToComplete ()
        { 
            if ((string.IsNullOrEmpty(DialogPrefabName) || _dialogHidden) && (!HideCharacterAtEnd || _characterHidden))
                base.HandleStageCompletion();
        }

        /// <summary>
        /// Handles the tutorial skipped.
        /// </summary>
        protected override void HandleTutorialSkipped()
        {
            if (!string.IsNullOrEmpty(DialogPrefabName))
                TutorialCharacterManager.Instance.HideDialog(DialogPrefabName, null);

            TutorialCharacterManager.Instance.HideCharacter(CharacterName, base.HandleTutorialSkipped);

        }
    }
}

