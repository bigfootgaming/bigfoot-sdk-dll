﻿using System;
using DG.Tweening;
using UnityEngine;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial character.
    /// </summary>
    public abstract class TutorialCharacter : MonoBehaviour
    {
        /// <summary>
        /// The shown anchor position.
        /// </summary>
        public Vector2 ShownAnchorPos;
        /// <summary>
        /// The hidden anchor position.
        /// </summary>
        public Vector2 HiddenAnchorPos;
        /// <summary>
        /// The show animation time.
        /// </summary>
        public float ShowAnimationTime;
        /// <summary>
        /// The rectTransform.
        /// </summary>
        private RectTransform _rTransform;

        /// <summary>
        /// Setup this instance.
        /// </summary>
        public virtual void Setup()
        {
            _rTransform = gameObject.GetComponent<RectTransform>();
            _rTransform.anchoredPosition = HiddenAnchorPos;
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Shows the character.
        /// </summary>
        /// <param name="ignoreAnimation">If true the character is shown instantly</param>
        /// <param name="callback">Callback.</param>
        public virtual void ShowCharacter(Action callback, bool independentTimeScale = true)
        {
            gameObject.SetActive(true);
            _rTransform.DOAnchorPos(ShownAnchorPos, ShowAnimationTime).SetUpdate(independentTimeScale).OnComplete(() =>
            {
                callback();
            });
        }

        /// <summary>
        /// Hides the character.
        /// </summary>
        /// <param name="ignoreAnimation">If true the character is hidden instantly</param>
        /// <param name="callback">Callback.</param>
        public virtual void HideCharacter (Action callback, bool independentTimeScale = true)
        {
            
            _rTransform.DOAnchorPos(HiddenAnchorPos, ShowAnimationTime).SetUpdate(independentTimeScale).OnComplete(() => {
                gameObject.SetActive(false);
                callback();
            });
            
               
        }

        /// <summary>
        /// Play the character animation.
        /// </summary>
        /// <param name="animationModel">Animation model.</param>
        public abstract void PlayAnimation(TutorialCharacterAnimationModel animationModel);
    }
}

