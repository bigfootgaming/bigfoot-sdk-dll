﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using System.Collections.Generic;
using BigfootSdk.Localization;
using System;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial dialog.
    /// </summary>
    public class TutorialDialog : MonoBehaviour
    {
        /// <summary>
        /// The text.
        /// </summary>
        public UILocalize Text;

        /// <summary>
        /// The tip.
        /// </summary>
        public Transform Tip;

        /// <summary>
        /// The next button.
        /// </summary>
        public TutorialTrigger NextButton;

        /// <summary>
        /// The intial scale.
        /// </summary>
        private Vector3 _intialScale;

        /// <summary>
        /// The animation time.
        /// </summary>
        public float AnimationTime;

        /// <summary>
        /// The animation ease.
        /// </summary>
        public Ease AnimationEase;

        /// <summary>
        /// Setup this instance.
        /// </summary>
        public virtual void Setup ()
        {
            _intialScale = Tip.localScale;
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="localizationKey">Localization key.</param>
        /// <param name="nextDelay">Next delay.</param>
        /// <param name="ignoreAnimation">If true the dialog is shown instantly.</param>
        public virtual void ShowDialog (string localizationKey, float nextDelay)
        {
            gameObject.SetActive(true);
            Text.SetKey(localizationKey);
            Text.transform.localScale = Vector2.one;
            NextButton.gameObject.SetActive(false);
            
            transform.DOScale(Vector3.one, AnimationTime).SetEase(AnimationEase).SetUpdate(true).OnComplete(() => {
                if (nextDelay >= 0)
                    StartCoroutine(ShowNext(nextDelay));
            });
            
        }

        /// <summary>
        /// Hides the dialog.
        /// </summary>
        /// <param name="callback">Callback.</param>
        /// <param name="ignoreAnimation">If true the dialog is hidden instantly.</param>
        public virtual void HideDialog (Action callback)
        {
            transform.DOScale(Vector3.zero, AnimationTime).SetEase(AnimationEase).SetUpdate(true).OnComplete(() => {
                gameObject.SetActive(false);
                callback?.Invoke();
            });
        }

        /// <summary>
        /// Sets the tip scale.
        /// </summary>
        /// <param name="value">Value.</param>
        public virtual void UpdateTip(float value)
        {
            Tip.localScale = new Vector3(_intialScale.x * value, _intialScale.y, _intialScale.z);
        }

        /// <summary>
        /// Shows the next button.
        /// </summary>
        /// <returns>The next.</returns>
        /// <param name="delay">Delay.</param>
        IEnumerator ShowNext(float delay)
        {
            if (NextButton.TriggerModels == null)
                NextButton.TriggerModels = new List<TutorialTriggerModel>();
            else
                NextButton.TriggerModels.Clear();

            string tutorialId = TutorialsManager.Instance.TutorialRunning;
            string stageName = TutorialsManager.Instance.GetTutorial(tutorialId).CurrentStage;


            TutorialTriggerModel nextModel = new TutorialTriggerModel()
            {
                Action = TutorialAction.CompleteStage,
                ListenForRetriggers = false,
                StageName = stageName,
                TutorialId = tutorialId,
                TriggerType = TutorialTriggerType.OnClick
            };

            NextButton.TriggerModels.Add(nextModel);

            yield return new WaitForSecondsRealtime(delay);
            NextButton.gameObject.SetActive(true);
        }
    }
}

