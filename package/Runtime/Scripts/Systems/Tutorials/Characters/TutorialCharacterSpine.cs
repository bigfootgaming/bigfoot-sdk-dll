﻿#if SPINE_ENABLED
using Spine.Unity;
#endif

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial character spine.
    /// </summary>
    public class TutorialCharacterSpine : TutorialCharacter
    {
#if SPINE_ENABLED
        /// <summary>
        /// The spine animation state.
        /// </summary>
        public Spine.AnimationState AnimState;
#endif
        /// <summary>
        /// Setup this instance.
        /// </summary>
        public override void Setup()
        {
            base.Setup();
#if SPINE_ENABLED            
            AnimState = gameObject.GetComponent<SkeletonGraphic>().AnimationState;
#endif
        }

        /// <summary>
        /// Plays the character spine animation.
        /// </summary>
        /// <param name="animationModel">Animation model.</param>
        public override void PlayAnimation(TutorialCharacterAnimationModel animationModel)
        {
#if SPINE_ENABLED
            if (!string.IsNullOrEmpty(animationModel.TransitionAnimation))
                AnimState.SetAnimation(0, animationModel.TransitionAnimation, false).TrackEnd = float.PositiveInfinity;


            //And another one for looping after the first one ends
            if (!string.IsNullOrEmpty(animationModel.LoopedAnimation))
                AnimState.AddAnimation(0, animationModel.LoopedAnimation, true, 0);
#endif
        }
    }
}
