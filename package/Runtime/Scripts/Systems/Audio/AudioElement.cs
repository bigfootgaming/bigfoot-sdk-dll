﻿using System;
using DG.Tweening;
using System.Collections;
using UnityEngine;

namespace BigfootSdk.Audio
{
    [System.Serializable]
    public class AudioTarget3D
    {
        public Transform SoundSourcePosition;
        public Transform TargetPosition;
        public Vector3 Position;
        public float Distance;
        public float Area;
    }

    [RequireComponent(typeof(AudioSource))]
    /// <summary>
    /// AudioElement is added to every audio that needs to be played. Has functions to Play, Stop, and adjust many values for each audio.
    /// </summary>
    public class AudioElement : MonoBehaviour
    {
        /// <summary>
        /// Name of the audio. Needed for AudioManager to perform actions.
        /// </summary>
        public string Name;
        /// <summary>
        /// Main AudioSource with clip attached to it.
        /// </summary>
        public AudioSource Source;
        /// <summary>
        /// Bus group for this audio. Mainly to perform actions like mute from AudioManager
        /// </summary>
        public AudioBusEnum Bus;

        [Range(1, 5)]
        public int Voices = 1;

        [Space]

        [Range(0, 1f)]
        public float Volume;

        [Space]

        [Range(0, 100f)]

        /// <summary>
        /// Trims a percentage of the audioClip from the start of it
        /// </summary>
        public float TrimStartPercentage;
        [Range(0, 100f)]
        /// <summary>
        /// Trims a percentage of the audioClip from the end of it
        /// </summary>
        public float TrimEndPercentage;

        [Space]

        /// <summary>
        /// Duration of the original clip
        /// </summary>
        public float OriginalDuration;
        /// <summary>
        /// Duration of the clip with trims applied
        /// </summary>
        public float Duration;
        bool _loop = false;

        // Target to perform volume up/down by distance
        AudioTarget3D _target;

        /// <summary>
        /// Tweener for fade in/out fx.
        /// </summary>
        Tweener _fadeTween;

        /// <summary>
        /// Updates clip duration on Inspector when trim from the start/end of the clip applied to it.
        /// Also checks that both start/end trim precentages don't goes beyond 100 %
        /// </summary>
        public float UpdateDuration()
        {
            var duration = Source.clip.length;
            var startTrim = duration * (TrimStartPercentage / 100f);
            var endTrim = duration - (duration * (TrimEndPercentage / 100f));

            // Check both trims dont goes beyond duration
            if (startTrim + endTrim >= duration)
            {
                startTrim = 0f;
                endTrim = 0f;
                TrimStartPercentage = 0f;
                TrimEndPercentage = 100f;
                UpdateDuration();
            }

            Duration = duration - startTrim - endTrim;
            OriginalDuration = duration;

            return Duration;
        }

        /// <summary>
        /// Plays the audio.
        /// </summary>
        /// <param name="loop">If set to <c>true</c> loops the audio.</param>
        public void PlayAudio(bool loop)
        {
            _loop = loop;
            Play();
        }

        public float GetCurrentTime
        {
            get
            {
                return Source.time;
            }
        }

        /// <summary>
        /// Plays audio with a custom volume
        /// </summary>
        /// <param name="customVolume"></param>
        /// <param name="loop"></param>
        public void PlayCustomVolume(float customVolume, bool loop)
        {
            _loop = loop;
            if (AudioIsReady())
            {
                PlayAudioInternal(customVolume);
            }
            else
            {
                StartCoroutine(WaitUntilLoaded());
            }
        }

        /// <summary>
        /// Plays an audio with its volume based on distance between target and sourcePosition
        /// </summary>
        /// <param name="target">Values to determine volume.</param>
        /// <param name="loop">If set to <c>true</c> loops the audio.</param>
        public void PlayTargetVolume(AudioTarget3D target, bool loop)
        {
            _loop = loop;
            if (AudioIsReady())
            {
                PlayAudioInternal(0f, target);
            }
            else
            {
                StartCoroutine(WaitUntilLoaded());
            }
        }

        /// <summary>
        /// Plays the audio once if it is ready.
        /// </summary>
        void Play()
        {
            if (AudioIsReady())
            {
                PlayAudioInternal();
            }
            else
            {
                StartCoroutine(WaitUntilLoaded());
            }
        }
        
        IEnumerator WaitUntilLoaded()
        {
            var wait = new WaitForEndOfFrame();
            while (!AudioIsReady())
            {
                yield return wait;
            }
            
            Play();
        }
        
        /// <summary>
        /// Plays the audio, custom volume could be set, otherwise will play its preset volume.
        /// </summary>
        /// <param name="customVolume">Custom volume will be applied when greater than 0f.</param>
        private void PlayAudioInternal(float customVolume = 0f, AudioTarget3D target = null)
        {
            _target = target;

            // If any fade tween active, will be killed;
            _fadeTween.Kill();

            float targetVolume = customVolume > 0 ? customVolume : Volume; 

            Source.volume = targetVolume * GetBusVolume();
            Source.time = Source.clip.length * (TrimStartPercentage / 100f);
            Source.Stop();
            Source.loop = _loop;
            Source.Play();
        }

        private void Update()
        {
            if (_target != null)
            {
                Source.volume = GetVolumeByDistance() * GetBusVolume();
            }
        }

        private void OnDisable()
        {
            _fadeTween?.Kill();
        }

        /// <summary>
        /// Gets volume value based on distance between targets
        /// </summary>
        /// <returns></returns>
        float GetVolumeByDistance()
        {
            Vector3 targetPosition = _target.TargetPosition == null ? _target.Position : _target.TargetPosition.position;

            float distance = _target.Distance - (Vector3.Distance(_target.SoundSourcePosition.position, targetPosition));
            
            float percentage = (distance * 100) / _target.Distance;  

            return Volume * (percentage/100f);
        }

        /// <summary>
        /// Pauses the audio. Also will end loop if its active.
        /// </summary>
        public void PauseAudio()
        {
            Source.Pause();
        }
        
        /// <summary>
        /// Unpauses the audio.
        /// </summary>
        public void UnpauseAudio()
        {
            float startTime = Source.clip.length * (TrimStartPercentage / 100f);
            float tempDuration = Duration - (Source.time + startTime);
            Source.UnPause();
        }
        
        /// <summary>
        /// Sets audioSource volume to 0
        /// </summary>
        public void Mute()
        {
            Source.volume = 0;
            Source.mute = true;
        }

        /// <summary>
        /// Sets audioSource volume to stored value on AudioElement.Volume (float).
        /// </summary>
        public void Unmute()
        {
            Source.volume = Volume * GetBusVolume();
            Source.mute = false;
        }
        
        /// <summary>
        /// Check if the audio source is ready
        /// </summary>
        /// <returns><c>true</c>, if the audi source is ready, <c>false</c> otherwise.</returns>
        bool AudioIsReady()
        {
            return (Source.clip.loadState == AudioDataLoadState.Loaded);
        }

        /// <summary>
        /// Fades a sound volume
        /// </summary>
        /// <param name="targetVolume">Target volume to reach.</param>
        /// <param name="fadeTime">Time to reach target volume.</param>
        public void Fade(float targetVolume, float fadeTime = .5f)
        {
            _fadeTween.Kill();

            _fadeTween = Source.DOFade(targetVolume * GetBusVolume(), fadeTime).SetUpdate(true);
        }

        /// <summary>
        /// Plays a sound with fadeIN FX.
        /// </summary>
        /// <param name="fadeTime">time to fade in.</param>
        public void PlayWithFadeIn(float fadeTime, bool loop = false)
        {
            _loop = loop;

            _fadeTween.Kill();

            Source.volume = 0;
            Source.time = Source.clip.length * (TrimStartPercentage / 100f);
            Source.Stop();
            Source.loop = _loop;
            Source.Play();
            
            _fadeTween = Source.DOFade(Volume * GetBusVolume(), fadeTime).SetUpdate(true);
        }

        /// <summary>
        /// Stops the audio. Also will end loop if its active.
        /// </summary>
        public void StopAudio()
        {
            _loop = false;
            Source.Stop();
        }

        /// <summary>
        /// Stops a sound after performing fade out.
        /// </summary>
        /// <param name="fadeTime">time to fade out.</param>
        public void StopWithFadeOut(float fadeTime)
        {
            _fadeTween.Kill();
            
            _fadeTween = Source.DOFade(0, fadeTime).OnComplete(() => 
            {
                Source.Stop();
                _loop = false;
            }).SetUpdate(true);

        }

        /// <summary>
        /// Updates the volume based on bus updated
        /// </summary>
        public void UpdateBusVolume()
        {
            Source.volume = Volume * GetBusVolume();
        }

        float GetBusVolume()
        {
            float value = 0;

            if (Bus == AudioBusEnum.MUSIC)
                value = AudioManager.Instance.MusicBusVolume;
            if (Bus == AudioBusEnum.SFX)
                value = AudioManager.Instance.SfxBusVolume;

            return value;
        }

    }
}