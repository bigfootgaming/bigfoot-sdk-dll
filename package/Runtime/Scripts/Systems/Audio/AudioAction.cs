﻿using UnityEngine;

namespace BigfootSdk.Audio
{
    public enum AudioEventActionEnum
    {
        PLAY,
        PLAY_WITH_CROSSFADE,
        PLAY_CUSTOM_VOLUME,
        PLAY_TARGET_3D,
        PLAY_AND_LOOP,
        PLAY_PLAYLIST,
        STOP_PLAYLIST,
        PAUSE,
        STOP,
        FADE,
        PLAY_WITH_FADEIN,
        STOP_WITH_FADEOUT,
        MUTE_SFX,
        UNMUTE_SFX,
        MUTE_MUSIC,
        UNMUTE_MUSIC
    }

    /// <summary>
    /// AudioAction is a helper that will trigger audios, or actions related to audios.
    /// Can be triggered by Animator events, or states of the object (enable, disable, and so on).
    /// </summary>
    public class AudioAction : MonoBehaviour
    {
        /// <summary>
        /// The manager.
        /// </summary>
        private AudioManager _manager;

        /// <summary>
        /// Name of the audio/playlist to be played when EventAction Triggered. If EventAction is mute/unmute name won't be needed.
        /// </summary>
        public string TargetName;
        /// <summary>
        /// Action that will be triggered. Could be play/pause/stop for audios, and mute/unmute for busses
        /// </summary>
        public AudioEventActionEnum EventAction;

        /// <summary>
        /// The audio gruop identifier.
        /// </summary>
        public int AudioGruopId = 0;

        public bool Loop = false;
        public bool SaveBusState = false;

        [Space]

        public AudioTarget3D Target3D;

        [Space]
        
        /// <summary>
        /// Play a sound with specific volume
        /// </summary>
        public float CustomVolume = 0f;

        /// <summary>
        /// When trying to achieve a specific target volume on Fade
        /// </summary>
        public float FadeTargetVolume = 0.5f;

        /// <summary>
        /// Time to reach the volume on Fade
        /// </summary>
        public float FadeTime = 0.5f;

        [Space]

        /// <summary>
        /// When this option enabled, the action selected (eventAction) will be triggered OnStart for this gameObject.
        /// </summary>
        public bool TriggerOnStart;
        /// <summary>
        /// When this option enabled, the action selected (eventAction) will be triggered OnEnable for this gameObject.
        /// </summary>
        public bool TriggerOnEnable;
        /// <summary>
        /// When this option enabled, the action selected (eventAction) will be triggered OnDisable for this gameObject.
        /// </summary>
        public bool TriggerOnDisable;
        /// <summary>
        /// When this option enabled, the action selected (eventAction) will be triggered when this object is destroyed.
        /// </summary>
        public bool TriggerOnDestroy;
        /// <summary>
        /// When this option enabled, the action selected (eventAction) will be triggered when this object is clicked.
        /// </summary>
        public bool TriggerOnClick;

        private void Awake()
        {
            _manager = AudioManager.Instance;
        }

        private void Start()
        {
            if (TriggerOnStart)
                SendEvent();
        }
        private void OnEnable()
        {
            if (TriggerOnEnable)
                SendEvent();
        }
        private void OnDisable()
        {
            if (TriggerOnDisable)
                SendEvent();
        }
        private void OnDestroy()
        {
            if (TriggerOnDestroy)
                SendEvent();
        }

        /// <summary>
        /// Sends the audio event previously selected. By default will play a sound.
        /// Can be called on this gameObject states (start/onEnable/onDisable/onDestroy), animator events and script refering to this one.
        /// </summary>
        public void SendEvent()
        {
            switch (EventAction)
            {
                case AudioEventActionEnum.PLAY:
                    _manager.Play(TargetName.ToLower(), Loop, AudioGruopId);
                    break;
                case AudioEventActionEnum.PLAY_WITH_CROSSFADE:
                    _manager.PlayWithCrossfade(TargetName.ToLower(), Loop, AudioGruopId, FadeTime);
                    break;
                case AudioEventActionEnum.PLAY_CUSTOM_VOLUME:
                    _manager.PlayCustomVolume(TargetName.ToLower(), CustomVolume, Loop, AudioGruopId);
                    break;
                case AudioEventActionEnum.PLAY_TARGET_3D:
                    _manager.PlayTargetVolume(TargetName.ToLower(), Target3D, Loop, AudioGruopId);
                    break;
                case AudioEventActionEnum.PLAY_AND_LOOP:
                    _manager.Play(TargetName.ToLower(), true, AudioGruopId);
                    break;
                case AudioEventActionEnum.PLAY_PLAYLIST:
                    _manager.PlayPlaylist(TargetName.ToLower());
                    break;
                case AudioEventActionEnum.STOP_PLAYLIST:
                    _manager.StopPlaylist();
                    break;
                case AudioEventActionEnum.PAUSE:
                    _manager.Pause(TargetName.ToLower(), AudioGruopId);
                    break;
                case AudioEventActionEnum.STOP:
                    _manager.Stop(TargetName.ToLower(), AudioGruopId);
                    break;
                case AudioEventActionEnum.FADE:
                    _manager.Fade(TargetName.ToLower(), AudioGruopId, FadeTargetVolume, FadeTime);
                    break;
                case AudioEventActionEnum.PLAY_WITH_FADEIN:
                    _manager.PlayWithFadeIn(TargetName.ToLower(), AudioGruopId, FadeTime);
                    break;
                case AudioEventActionEnum.STOP_WITH_FADEOUT:
                    _manager.StopWithFadeOut(TargetName.ToLower(), AudioGruopId, FadeTime);
                    break;
                case AudioEventActionEnum.MUTE_SFX:
                    _manager.MuteBus(AudioBusEnum.SFX, SaveBusState);
                    break;
                case AudioEventActionEnum.UNMUTE_SFX:
                    _manager.UnmuteBus(AudioBusEnum.SFX, SaveBusState);
                    break;
                case AudioEventActionEnum.MUTE_MUSIC:
                    _manager.MuteBus(AudioBusEnum.MUSIC, SaveBusState);
                    break;
                case AudioEventActionEnum.UNMUTE_MUSIC:
                    _manager.UnmuteBus(AudioBusEnum.MUSIC, SaveBusState);
                    break;
                default:
                    break;
            }
        }
    }
}
