﻿using BigfootSdk.Helpers;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BigfootSdk.Audio
{
    [System.Serializable]
    /// <summary>
    /// Playlist.
    /// </summary>
    public class Playlist
    {
        /// <summary>
        /// The playlist name.
        /// </summary>
        public string Name;
        /// <summary>
        /// The playlist audio elements.
        /// </summary>
        public List<AudioElement> Elements;
    }

    /// <summary>
    /// AudioGroup holds a group of sounds, will be in charge of taking actions on them.
    /// </summary>
    public class AudioGroup : MonoBehaviour
    {
        /// <summary>
        /// Dont destroy between scenes.
        /// </summary>
        public bool DontDestroy;

        /// <summary>
        /// The identifier.
        /// </summary>
        public int Id = 0; 

        /// <summary>
        /// The group playlist. Contains music.
        /// </summary>
        public Playlist GroupPlaylist = new Playlist() { Name = "playlist", Elements = new List<AudioElement>() };

        /// <summary>
        /// Dont destroy between scenes.
        /// </summary>
        public bool OneMusicAtATime;

        /// <summary>
        /// The group sfxs.
        /// </summary>
        public List<AudioElement> Sfxs = new List<AudioElement>();

        /// <summary>
        /// The group music.
        /// </summary>
        public List<AudioElement> Music = new List<AudioElement>();

        void Awake()
        {
            if(DontDestroy)
                DontDestroyOnLoad(this.gameObject);

        }

        /// <summary>
        /// Load playlist, suscribe to events and get busses state.
        /// </summary>
        private void OnEnable()
        {
            AudioManager.Instance?.AddAudioGroup(this);
            AudioManager.Instance?.LoadPlaylist(GroupPlaylist);
            Subscribe();
            GetBussesState();
            AddVoices();
        }

        /// <summary>
        /// Unload playlist and unsuscribe from events.
        /// </summary>
        private void OnDisable()
        {
            AudioManager.Instance?.RemoveAudioGroup(this);
            AudioManager.Instance?.UnloadPlaylist(GroupPlaylist);
            Unsubscribe();
        }

        #region Events
        /// <summary>
        /// Subscribe to events.
        /// </summary>
        void Subscribe()
        {
            AudioEvents.OnPlay += Play;
            AudioEvents.OnPlayWithCrossfade += PlayWithCrossfade;
            AudioEvents.OnPlayCustomVolume += PlayCustomVolume;
            AudioEvents.OnPlayTargetVolume += PlayTargetVolume;
            AudioEvents.OnPause += Pause;
            AudioEvents.OnUnpause += Unpause;
            AudioEvents.OnStop += Stop;
            AudioEvents.OnFade += Fade;
            AudioEvents.OnPlayWithFadeIn += PlayWithFadeIn;
            AudioEvents.OnStopWithFadeOut += StopWithFadeOut;
            AudioEvents.OnMuteBus += MuteBus;
            AudioEvents.OnUnmuteBus += UnmuteBus;
            AudioEvents.OnUpdateBusVolume += UpdateBusVolume;
        }

        /// <summary>
        /// Unsubscribe from events.
        /// </summary>
        void Unsubscribe()
        {
            AudioEvents.OnPlay -= Play;
            AudioEvents.OnPlayWithCrossfade -= PlayWithCrossfade;
            AudioEvents.OnPlayCustomVolume -= PlayCustomVolume;
            AudioEvents.OnPlayTargetVolume -= PlayTargetVolume;
            AudioEvents.OnPause -= Pause;
            AudioEvents.OnUnpause -= Unpause;
            AudioEvents.OnStop -= Stop;
            AudioEvents.OnFade -= Fade;
            AudioEvents.OnPlayWithFadeIn -= PlayWithFadeIn;
            AudioEvents.OnStopWithFadeOut -= StopWithFadeOut;
            AudioEvents.OnMuteBus -= MuteBus;
            AudioEvents.OnUnmuteBus -= UnmuteBus;
            AudioEvents.OnUpdateBusVolume -= UpdateBusVolume;
        }

        /// <summary>
        /// Play the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="loop">If set to <c>true</c> the audio will loop.</param>
        /// <param name="groupId">Group identifier.</param>
        public void Play(string audioName, bool loop, int groupId)
        {
            if (groupId == Id)
            {
                var audio = FindAudio(audioName);
                if (OneMusicAtATime && audio?.Bus == AudioBusEnum.MUSIC)
                {
                    StopAllMusic();
                }
                audio?.PlayAudio(loop);
            }
        }

        /// <summary>
        /// Play the specified audio with a custom value.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="customVolume">Custom volume.</param>
        /// <param name="loop">If set to <c>true</c> the audio will loop.</param>
        /// <param name="groupId">Group identifier.</param>
        public void PlayCustomVolume(string audioName, float customVolume, bool loop, int groupId)
        {
            if (groupId == Id)
            {
                var audio = FindAudio(audioName);
                if (OneMusicAtATime && audio?.Bus == AudioBusEnum.MUSIC)
                {
                    StopAllMusic();
                }
                audio?.PlayCustomVolume(customVolume, loop);
            }
        }

        public void PlayTargetVolume(string audioName, AudioTarget3D target, bool loop, int groupId)
        {
            if (groupId == Id)
            {
                var audio = FindAudio(audioName);
                if (OneMusicAtATime && audio?.Bus == AudioBusEnum.MUSIC)
                {
                    StopAllMusic();
                }
                audio?.PlayTargetVolume(target, loop);
            }
        }

        /// <summary>
        /// Pause the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        public void Pause(string audioName, int groupId)
        {
            if (groupId == Id)
            {
                var audio = FindAudio(audioName);
                audio?.PauseAudio();
            }
        }

        /// <summary>
        /// Unpause the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        public void Unpause(string audioName, int groupId)
        {
            if (groupId == Id)
            {
                var audio = FindAudio(audioName);
                audio?.UnpauseAudio();
            }           
        }

        /// <summary>
        /// Stop the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        public void Stop(string audioName, int groupId)
        {
            if (groupId == Id)
            {
                var audio = FindAudioVoiceReadyToStop(audioName);
                audio?.StopAudio();
            }
        }

        public void PlayWithCrossfade(string audioName, bool loop, int groupId, float crossfadeTime)
        {
            if (groupId == Id)
            {
                var audio = FindAudio(audioName);
                if (OneMusicAtATime && audio?.Bus == AudioBusEnum.MUSIC)
                {
                    var currentMusic = GetCurrentMusic();
                    foreach (var item in currentMusic)
                    {
                        item.StopWithFadeOut(crossfadeTime);
                    }
                }
                audio?.PlayWithFadeIn(crossfadeTime, loop);
            }
        }

        public void PlayWithFadeIn(string audioName, int groupId, float fadeTime, bool loop)
        {
            if (groupId == Id)
            {
                var audio = FindAudio(audioName);
                if (OneMusicAtATime && audio?.Bus == AudioBusEnum.MUSIC)
                {
                    StopAllMusic();
                }
                audio?.PlayWithFadeIn(fadeTime, loop);
            }
        }

        public void StopWithFadeOut(string audioName, int groupId, float fadeTime)
        {
            if (groupId == Id)
            {
                var audio = FindAudioVoiceReadyToStop(audioName);
                audio?.StopWithFadeOut(fadeTime);
            }
        }

        /// <summary>
        /// Fade an audio already playing to a specific volume target.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="targetVolume">Volume target.</param>
        /// <param name="fadeTime">Time to reach target volume.</param>
        public void Fade(string audioName, int groupId, float targetVolume, float fadeTime = 0.5f)
        {
            if (groupId == Id)
            {
                var audio = FindAudio(audioName);
                audio?.Fade(targetVolume, fadeTime);
            }
        }

        /// <summary>
        /// Check on all sounds which one has voices and duplicate them
        /// </summary>
        void AddVoices()
        {
            List<AudioElement> allAudios = new List<AudioElement>();
            allAudios.AddRange(Sfxs);
            allAudios.AddRange(Music);

            foreach (var item in allAudios)
            {
                var voices = item.Voices;
                if (voices > 1)
                {
                    // Arrange original at the bottom
                    item.transform.SetAsLastSibling();
                    for (int i = 2; i <= voices; i++)
                    {
                        // Create a new Audiosource based on the original item
                        var go = Instantiate(item, this.transform);
                        go.name = string.Format("{0} Voice[{1}]", item.name, i);

                        // Get the audioelement
                        var ae = go.GetComponent<AudioElement>();
                        // Reference the source to our own audioSource (otherwise will reference to the original item)
                        ae.Source = go.GetComponent<AudioSource>();
                        
                        // Add to its corresponding bus
                        if (ae.Bus == AudioBusEnum.MUSIC)
                        {
                            Music.Add(ae);
                        }
                        else
                        {
                            Sfxs.Add(ae);
                        }
                    }
                    // Name the first one
                    item.name = string.Format("{0} Voice[1]", item.name);
                }
                
            }
        }

       /// <summary>
       /// Mute the audios on the specified bus.
       /// </summary>
       /// <param name="bus">Bus.</param>
        public void MuteBus(AudioBusEnum bus)
        {
            if (bus == AudioBusEnum.SFX)
            {
                MuteAudios(Sfxs);
            }
            if (bus == AudioBusEnum.MUSIC)
            {
                MuteAudios(Music);
            }
        }

        /// <summary>
        /// Unmute the audios on the specified bus.
        /// </summary>
        /// <param name="bus">Bus.</param>
        public void UnmuteBus(AudioBusEnum bus)
        {
            if (bus == AudioBusEnum.SFX)
            {
                UnmuteAudios(Sfxs);
            }
            if (bus == AudioBusEnum.MUSIC)
            {
                UnmuteAudios(Music);
            }
        }

        /// <summary>
        /// Gets the state of the busses.
        /// </summary>
        void GetBussesState ()
        {
            if (AudioManager.Instance.MusicBusOn)
                UnmuteAudios(Music);
            else
                MuteAudios(Music);

            if (AudioManager.Instance.SfxBusOn)
                UnmuteAudios(Sfxs);
            else
                MuteAudios(Sfxs);
        }

        /// <summary>
        /// Updates volumes of bus on each audioItem, applying variation on current audio volume.
        /// </summary>
        void UpdateBusVolume()
        {
            foreach (var item in Sfxs)
            {
                item.UpdateBusVolume();
            }

            foreach (var item in Music)
            {
                item.UpdateBusVolume();
            }
        }

        /// <summary>
        /// Finds the audio.
        /// </summary>
        /// <returns>The audio.</returns>
        /// <param name="audioName">Audio name.</param>
        public AudioElement FindAudio(string audioName)
        {
            AudioElement audioFound = Sfxs.FirstOrDefault(x => x.Name == audioName.ToLower());
            if (audioFound == null)
            {
                audioFound = Music.FirstOrDefault(x => x.Name == audioName.ToLower());
            }
            if (audioFound != null)
            {
                if (audioFound.Voices > 1)
                {
                    return FindAudioVoiceReadyToPlay(audioName);
                }
            }

            if (audioFound == null)
            {
                LogHelper.LogWarning(string.Format("Audio \"{0}\" not found", audioName));
            }

            return audioFound;
        }

        /// <summary>
        /// Finds an audio ready to play when it has voices
        /// </summary>
        /// <param name="audioName">Name of the audio.</param>
        /// <returns></returns>
        AudioElement FindAudioVoiceReadyToPlay(string audioName)
        {
            var allVoices = FindAudios(audioName);

           
            // First try to find if anyone isnt playing yet
            foreach (var item in allVoices)
            {
                if (item.GetCurrentTime == 0)
                    return item;
            }
            

            // When every sound is already playing, find the closest to end by its source.time
            float sourceTime = 0f;
            AudioElement audioElement = null;
            for (int i = 0; i < allVoices.Count; i++)
            {
                if (allVoices[i].GetCurrentTime >= sourceTime)
                {
                    sourceTime = allVoices[i].Source.time;
                    audioElement = allVoices[i];
                }
            }
            return audioElement;
        }

        /// <summary>
        /// Finds and audio ready to stop on all voices actives.
        /// </summary>
        /// <param name="audioName">Name of the audio.</param>
        /// <returns></returns>
        AudioElement FindAudioVoiceReadyToStop(string audioName)
        {
            var allVoices = FindAudios(audioName);

            // When every sound is already playing, find the closest to end by its source.time
            float sourceTime = 0f;
            AudioElement audioElement = null;
            for (int i = 0; i < allVoices.Count; i++)
            {
                if (allVoices[i].GetCurrentTime >= sourceTime)
                {
                    sourceTime = allVoices[i].Source.time;
                    audioElement = allVoices[i];
                }
            }
            return audioElement;
        }

        /// <summary>
        /// Looks for all audios, both sfx and music
        /// </summary>
        /// <param name="audioName">Name of the audio.</param>
        /// <returns></returns>
        public List<AudioElement> FindAudios(string audioName)
        {
            List<AudioElement> result = new List<AudioElement>();

            foreach (var item in Sfxs)
            {
                if (item.Name == audioName.ToLower())
                    result.Add(item);
            }
            foreach (var item in Music)
            {
                if (item.Name == audioName.ToLower())
                    result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// Mutes the audios.
        /// </summary>
        /// <param name="audios">Audios.</param>
        void MuteAudios(List<AudioElement> audios)
        {
            foreach (var item in audios)
            {
                item.Mute();
            }
        }

        /// <summary>
        /// Unmutes the audios.
        /// </summary>
        /// <param name="audios">Audios.</param>
        void UnmuteAudios(List<AudioElement> audios)
        {
            foreach (var item in audios)
            {
                item.Unmute();
            }
        }

        #endregion

        #region Editor functions
        /// <summary>
        /// Creates the audio.
        /// </summary>
        /// <param name="audioClip">Audio clip.</param>
        /// <param name="music">If set to <c>true</c> music.</param>
        public void CreateAudio(AudioClip audioClip, bool music)
        {
            bool exists = false;
            if (music)
            {
                foreach (AudioElement e in Music)
                    if (e.Source.clip == audioClip)
                        exists = true;
            }
            else
            {
                foreach (AudioElement e in Sfxs)
                    if (e.Source.clip == audioClip)
                        exists = true;
            }

            if (!exists)
            {
                // Create Main GameObject
                GameObject go = new GameObject(audioClip.name.ToLower());
                go.transform.SetParent(this.transform);

                //Add AudioElement
                AudioElement audioElement = go.AddComponent<AudioElement>();

                // Set inital values for AudioSource
                AudioSource source = go.GetComponent<AudioSource>();
                source.clip = audioClip;
                source.playOnAwake = false;

                //set initial values for AudioElement
                audioElement.Name = audioClip.name.ToLower();
                audioElement.Source = source;
                audioElement.Volume = 1f;
                audioElement.TrimStartPercentage = 0f;
                audioElement.TrimEndPercentage = 100f;
                audioElement.UpdateDuration();
                if (music)
                {
                    audioElement.Bus = AudioBusEnum.MUSIC;
                    if (Music == null)
                        Music = new List<AudioElement>();
                    Music.Add(audioElement);
                }
                else 
                {
                    audioElement.Bus = AudioBusEnum.SFX;
                    if (Sfxs == null)
                        Sfxs = new List<AudioElement>();
                    Sfxs.Add(audioElement);
                }
            }

        }
        /// <summary>
        /// Deletes the audio.
        /// </summary>
        /// <param name="audioName">audio name.</param>
        /// <param name="music">If set to <c>true</c> music will delete the audio from Music list, otherwise will delete the audio from Sfxs list.</param>
        public void DeleteAudio (string audioName, bool music)
        {
            if (music)
            {
                AudioElement toDelete = Music.First(x => x.Name == audioName);
                Music.Remove(toDelete);
                DestroyImmediate(toDelete.gameObject);
            }
            else
            {
                AudioElement toDelete = Sfxs.First(x => x.Name == audioName);
                Sfxs.Remove(toDelete);
                DestroyImmediate(toDelete.gameObject);
            }
        }

        /// <summary>
        /// Adds the audio to playlist.
        /// </summary>
        /// <param name="audio">Audio element.</param>
        public void AddToPlaylist (AudioElement audio)
        {
            GroupPlaylist.Elements.Add(audio);
        }

        /// <summary>
        /// Removes the audio from playlist.
        /// </summary>
        /// <param name="audio">Audio element.</param>
        public void RemoveFromPlaylist(AudioElement audio)
        {
            GroupPlaylist.Elements.Remove(audio);
        }

        /// <summary>
        /// Updates the lists.
        /// </summary>
        public void UpdateLists ()
        {
            if (Sfxs == null)
                Sfxs = new List<AudioElement>();
            if (Music == null)
                Music = new List<AudioElement>();

            AudioElement[] childs = GetComponentsInChildren<AudioElement>();
            int i = 0;

            while (i < Sfxs.Count)
            {
                bool exists = childs.Any(x => x == Sfxs[i]);
                if (!exists)
                    Sfxs.RemoveAt(i);
                else
                    i++;
            }
            i = 0;
            while (i < Music.Count)
            {
                bool exists = childs.Any(x => x == Music[i]);
                if (!exists)
                    Music.RemoveAt(i);
                else
                    i++;
            }
            i = 0;
            while (i < GroupPlaylist.Elements.Count)
            {
                bool exists = Music.Any(x => x == GroupPlaylist.Elements[i]);
                if (!exists)
                    GroupPlaylist.Elements.RemoveAt(i);
                else
                    i++;
            }
        }

        /// <summary>
        /// Play random audio from specified bus.
        /// </summary>
        /// <param name="bus">Bus.</param>
        public void PlayRandom(AudioBusEnum bus)
        {
            // Find random sound from bus and play it
            if (bus == AudioBusEnum.MUSIC)
            {
                if (Music.Count > 0)
                {
                    int rndPick = Random.Range(0, Music.Count);
                    if (Music[rndPick])
                        Music[rndPick].PlayAudio(false);
                }
            }
            else
            {
                if (Sfxs.Count > 0)
                {
                    int rndPick = Random.Range(0, Sfxs.Count);
                    if (Sfxs[rndPick])
                        Sfxs[rndPick].PlayAudio(false);
                }
            }

        }

        /// <summary>
        /// Stops all audios.
        /// </summary>
        public void StopAll()
        {
            // Stop all sounds
            StopAllSFX();
            StopAllMusic();
        }

        void StopAllSFX()
        {
            foreach (var item in Sfxs)
            {
                item.StopAudio();
            }
        }
        void StopAllMusic()
        {
            foreach (var item in Music)
            {
                item.StopAudio();
            }
        }

        List<AudioElement> GetCurrentMusic()
        {
            List<AudioElement> musicPlaying = new List<AudioElement>();

            foreach (var item in Music)
            {
                if (item.Source.isPlaying == true)
                    musicPlaying.Add(item);
            }

            return musicPlaying;
        }

        #endregion

       
    }
}
