﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Backend;
using UnityEngine;

namespace BigfootSdk.Helpers
{
    /// <summary>
    /// Format helper for numbers, time, etc.
    /// </summary>
    public class FormatHelper : Singleton<FormatHelper>
    {
        /// <summary>
        /// The name for the default formatter
        /// </summary>
        public const string DEFAULT_FORMATTER = "default";

        /// <summary>
        /// List of all the available time formatters
        /// </summary>
        protected List<ITimeFormatter> _timeFormatters = new List<ITimeFormatter>();
        
        /// <summary>
        /// List of all the available cost formatters
        /// </summary>
        protected List<ICostFormatter> _costFormatters = new List<ICostFormatter>();
        
        /// <summary>
        /// List of all the available number formatters
        /// </summary>
        protected List<INumberFormatter> _numberFormatters = new List<INumberFormatter>();
        
        private void OnEnable()
        {
            InitializeFormatters();
        }

        /// <summary>
        /// Method to initialize the formatters
        /// </summary>
        protected virtual void InitializeFormatters()
        {
            _timeFormatters.Add(new DefaultTimeFormatter());
            _costFormatters.Add(new DefaultCostFormatter());
            _numberFormatters.Add(new DefaultNumberFormatter());
        }
        
        /// <summary>
        /// Formats the number in either scientific notation or standar notation.
        /// </summary>
        /// <param name="number">The number to format</param>
        /// <param name="formatterName">Which formatter to use</param>
        /// <returns>The number.</returns>
        public string FormatNumber(double number, string formatterName = DEFAULT_FORMATTER)
        {
            var formatter = _numberFormatters.FirstOrDefault(f => f.Name == formatterName);
            
            if (formatter == null)
            {
                formatter = _numberFormatters.FirstOrDefault(f => f.Name == DEFAULT_FORMATTER);
                if (formatter == null)
                {
                    Debug.LogWarning("Default Formatter is null!");
                    return string.Empty;
                }
            }
            
            return formatter.FormatNumber(number);
        }

        /// <summary>
        /// Formats a timer using a formatter
        /// </summary>
        /// <param name="totalSeconds">The amount of seconds to format</param>
        /// <param name="formatterName">Which formatter to use</param>
        /// <returns>The formatted string</returns>
        public string FormatTimer(int totalSeconds, string formatterName = DEFAULT_FORMATTER)
        {
            TimeSpan timeLeft = new TimeSpan(0, 0, totalSeconds);

            var formatter = _timeFormatters.FirstOrDefault(f => f.Name == formatterName);
            
            if (formatter == null)
            {
                formatter = _timeFormatters.FirstOrDefault(f => f.Name == DEFAULT_FORMATTER);
                if (formatter == null)
                {
                    Debug.LogWarning("Default Formatter is null!");
                    return string.Empty;
                }
            }
            
            return formatter.FormatTimer(timeLeft);
        }
        
        /// <summary>
        /// Formats a timer using a formatter
        /// </summary>
        /// <param name="totalSeconds">The amount of seconds to format</param>
        /// <param name="formatterName">Which formatter to use</param>
        /// <returns>The formatted string</returns>
        public string FormatTimer(long totalSeconds, string formatterName = DEFAULT_FORMATTER)
        {
            return FormatTimer((int) totalSeconds, formatterName);
        }
        
        /// <summary>
        /// Formats a timer using a formatter
        /// </summary>
        /// <param name="totalSeconds">The amount of seconds to format</param>
        /// <param name="formatterName">Which formatter to use</param>
        /// <returns>The formatted string</returns>
        public string FormatTimer(double totalSeconds, string formatterName = DEFAULT_FORMATTER)
        {
            return FormatTimer((int) totalSeconds, formatterName);
        }
        
        /// <summary>
        /// Formats a cost using a formatter
        /// </summary>
        /// <param name="cost">The cost to format</param>
        /// <param name="formatterName">Which formatter to use</param>
        /// <returns>The formatted string</returns>
        public string FormatCost(ICost cost, string formatterName = DEFAULT_FORMATTER)
        {
            var formatter = _costFormatters.FirstOrDefault(f => f.Name == formatterName);
            
            if (formatter == null)
            {
                formatter = _costFormatters.FirstOrDefault(f => f.Name == DEFAULT_FORMATTER);
                if (formatter == null)
                {
                    Debug.LogWarning("Default Formatter is null!");
                    return string.Empty;
                }
            }
            
            return formatter.FormatCost(cost);
        }
    }
}
