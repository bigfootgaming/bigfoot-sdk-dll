﻿using System;
using System.IO;
using BigfootSdk.Helpers;
using UnityEngine;

internal static class ProgressFileHelper
{
    internal static void LoadFileAsJson (string fileName, Action<string> successCallback, Action errorCallback)
    {
        string filePath = Path.Combine(Application.persistentDataPath, "Savefiles/" + fileName + ".json");
        
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            successCallback.Invoke(dataAsJson);
        }
        else
        {
            LogHelper.LogError("Cannot find file! '" + filePath + "'", Environment.StackTrace);
            errorCallback.Invoke();
        }
    }

    internal static void SaveJsonAsFile(string fileName, string jsonContent)
    {
        string folderPath = Path.Combine(Application.persistentDataPath, "Savefiles");
        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
        }
        string path = folderPath + "/" + fileName + ".json";
        File.WriteAllText(path, jsonContent);
        LogHelper.LogSdk(string.Format("<color=red>'{0}' saved!</color>", path));
    }
}
