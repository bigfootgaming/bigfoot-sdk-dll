﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;
using BigfootSdk.Backend;
using UnityEditor.U2D;
using UnityEngine.U2D;

namespace BigfootSdk.Helpers
{
    public class SpriteHelper
    {
        public static void UpdateCompression()
        {
            SpriteCompressionConfiguration config = ScriptableObjectsHelper.GetInstance<SpriteCompressionConfiguration>();
            if (config != null)
            {
                TextureImporterPlatformSettings androidSettings = new TextureImporterPlatformSettings()
                {
                    format = config.AndroidTextureImporterFormat,
                    compressionQuality = 100,
                    overridden = true,
                    textureCompression = config.AndroidCompressionQuality,
                    name = "Android"
                };

                TextureImporterPlatformSettings iosSettings = new TextureImporterPlatformSettings()
                {
                    format = config.AppleTextureImporterFormat,
                    compressionQuality = 100,
                    overridden = true,
                    textureCompression = config.AppleCompressionQuality,
                    name = "iPhone"
                };

                List<string> imagesFiles = new List<string>();
                foreach (var relativePath in config.SpritesRelativePaths)
                {
                    string path = string.Format("{0}{1}", Application.dataPath, "/" + relativePath);

                    string[] pngFiles = Directory.GetFiles(path, "*.png", SearchOption.AllDirectories);
                    string[] jpgFiles = Directory.GetFiles(path, "*.jpg", SearchOption.AllDirectories);
                
                    imagesFiles.AddRange(pngFiles.ToList());
                    imagesFiles.AddRange(jpgFiles.ToList());
                }

                LogHelper.LogSdk("Setting compression format on " + imagesFiles.Count + " images files");

                foreach (string imageFile in imagesFiles)
                {
                    string assetPath = string.Format("Assets{0}",imageFile.Replace(Application.dataPath, "").Replace('\\', '/'));
                    TextureImporter importer = AssetImporter.GetAtPath(assetPath) as TextureImporter;

                    importer.SetPlatformTextureSettings(androidSettings);
                    importer.SetPlatformTextureSettings(iosSettings);
                    AssetDatabase.ImportAsset(assetPath);
                }

                List<string> atlasFiles = new List<string>();
                foreach (var relativePath in config.AtlasRelativePaths)
                {
                    string path = string.Format("{0}{1}", Application.dataPath, "/" + relativePath);
                    string[] files = Directory.GetFiles(path, "*.spriteatlas", SearchOption.AllDirectories);
                    atlasFiles.AddRange(files.ToList());
                }

                LogHelper.LogSdk("Setting compression format on " + atlasFiles.Count + " sprite atlas files");
                foreach (string atlasFile in atlasFiles)
                {
                    string assetPath = string.Format("Assets{0}",atlasFile.Replace(Application.dataPath, "").Replace('\\', '/'));
                    SpriteAtlas atlas = AssetDatabase.LoadAssetAtPath<SpriteAtlas>(assetPath);
                    SpriteAtlasExtensions.SetPlatformSettings(atlas, androidSettings);
                    SpriteAtlasExtensions.SetPlatformSettings(atlas, iosSettings);
                    AssetDatabase.ImportAsset(assetPath);
                }
            }
            else
            {
                LogHelper.LogWarning("You need to create a SpriteCompressionConfiguration file before running this method.");
            }
        }
        
        
    }
}
#endif