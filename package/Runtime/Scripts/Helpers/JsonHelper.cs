﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters;
using System.Text.RegularExpressions;
using CodeStage.AntiCheat.ObscuredTypes;

namespace BigfootSdk.Helpers
{
	/// <summary>
	/// Json helper.
	/// </summary>
	public static class JsonHelper
	{
		/// <summary>
		/// Assembly sufix needed for TypeNameHandling in Json.net
		/// </summary>
		public const string ASSEMBLY_SUFFIX = ", Assembly-CSharp";

		/// <summary>
		/// Regex used to find where to insert the ASSEMBLY_SUFFIX
		/// </summary>
		private static readonly Regex typeWithoutAssemblyRegex = new Regex (@"(""\$type""\s*:\s*""[\w.]+)("")");

		/// <summary>
		/// Settings used to be able to deserialiaze using TypeNameHandling
		/// NOTE: MetadataPropertyHandling.ReadAhead is needed since we cant guarantee that $type will be the
		/// first value in the JSON
		/// </summary>
		static JsonSerializerSettings settings = new JsonSerializerSettings () {
			DefaultValueHandling = DefaultValueHandling.Ignore,
			NullValueHandling = NullValueHandling.Ignore,
			ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple,
			TypeNameHandling = TypeNameHandling.Auto,
			MetadataPropertyHandling = MetadataPropertyHandling.ReadAhead,
			Converters = new List<JsonConverter>(){new ObscuredValueConverter()}
		};

        #region Public Methods

        /// <summary>
        ///  Serializes an object into JSON format.
        /// </summary>
        /// <returns>The object.</returns>
        /// <param name="obj">Object.</param>
        /// <param name="useSettings">If set to <c>true</c> use settings.</param>
        /// <typeparam name="T">Type parameter.</typeparam>
        public static string SerializeObject<T> (T obj, bool useSettings = true)
		{
            if (useSettings)
                return JsonConvert.SerializeObject(obj, settings);
            else
                return JsonConvert.SerializeObject(obj);
		}

        /// <summary>
        /// Serializes the object.
        /// </summary>
        /// <returns>The object.</returns>
        /// <param name="obj">Object.</param>
        /// <param name="customSettings">Custom settings.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static string SerializeObject<T>(T obj, JsonSerializerSettings customSettings)
        {
            return JsonConvert.SerializeObject(obj, customSettings);
        }

        /// <summary>
        /// Desarializes an object from JSON format
        /// </summary>
        /// <returns>The object.</returns>
        /// <param name="json">JSON to deserialize.</param>
        /// <typeparam name="T">Type parameter.</typeparam>
        public static T DesarializeObject<T> (string json)
		{
            // We first have to inject the assembly suffix, in case we need to use TypeNameHandling
            json = InjectAssemblySuffixes (json);

            try
            {
                return JsonConvert.DeserializeObject<T>(json, settings);
            }
            catch (Exception e)
            {
                LogHelper.LogError(string.Format("Error '{0}' while deserializing Json '{1}'", e.Message, json), Environment.StackTrace);
                return default;
            }
        }

		/// <summary>
		/// Serializes an object list into JSON format.
		/// </summary>
		/// <returns>JSON</returns>
		/// <param name="objs">The object list to serialize.</param>
		/// <typeparam name="T">Type parameter.</typeparam>
		public static string SerializeObjectList<T> (List<T> objs)
		{
			// Wrap items before serializing
			Wrapper<T> wrapper = new Wrapper<T> ();
			wrapper.items = objs;

			// Serialize
			return JsonConvert.SerializeObject (wrapper);
		}

		/// <summary>
		/// Serializes an object array into JSON format
		/// </summary>
		/// <param name="objs">The object array to serialize</param>
		/// <typeparam name="T">Type parameter</typeparam>
		/// <returns>JSON</returns>
		public static string SerializeObjectArray<T>(T[] objs)
		{
			ArrayWrapper<T> wrapper = new ArrayWrapper<T>();
			wrapper.items = objs;
			
			// Serialize
			return JsonConvert.SerializeObject(wrapper);
		}

		/// <summary>
		/// Desarializes an object list from JSON format
		/// </summary>
		/// <returns>The object list.</returns>
		/// <param name="json">JSON to deserialize</param>
		/// <typeparam name="T">Type parameter.</typeparam>
		public static List<T> DeserializeObjectList<T> (string json)
		{
			// We first have to inject the assembly suffix, in case we need to use TypeNameHandling
			json = InjectAssemblySuffixes (json);

            try
            {
                // We wrap the items and deserialize them
                json = WrapJsonList(json);
                Wrapper<T> wrapper = JsonConvert.DeserializeObject<Wrapper<T>>(json, settings);

                return wrapper.items;
            }
            catch (Exception e)
            {
	            LogHelper.LogError(string.Format("Error '{0}' while deserializing Json '{1}'", e.Message, json), Environment.StackTrace);
                return default;
            }
		}

		#endregion

		[Serializable]
		private class ArrayWrapper<T>
		{
			public T[] items;
		}
		
		[Serializable]
		private class Wrapper<T>
		{
			public List<T> items;
		}

		/// <summary>
		/// Wraps a JSON containing a list, so JSON.net can deserialize it
		/// </summary>
		/// <returns>The wrapped JSON.</returns>
		/// <param name="jsonList">JSON to wrap.</param>
		static string WrapJsonList (string jsonList)
		{
			return "{ \"items\": " + jsonList + "}";
		}

		/// <summary>
		/// Injects the assembly suffixes.
		/// </summary>
		/// <returns>The json input with the Assembly Sufix injected, if the regex matches</returns>
		/// <param name="jsonInput">JSON.</param>
		static string InjectAssemblySuffixes (string jsonInput)
		{
			string result = jsonInput;

			// Try to inject the Assembly Sufix into the json
            if (typeWithoutAssemblyRegex != null && !string.IsNullOrEmpty (jsonInput) && jsonInput.Contains("$type")) 
            {
                #if !UNITY_WEBGL
                                result = typeWithoutAssemblyRegex.Replace(jsonInput, "$1, " + System.Reflection.Assembly.GetCallingAssembly().GetName().Name + "$2");
                #else
                                result = typeWithoutAssemblyRegex.Replace(jsonInput, "$1, BigfootSdk$2");
                #endif
            }
                return result;
		}
	}

	public class ObscuredValueConverter : JsonConverter
	{
		private readonly Type[] _types = {
            typeof(ObscuredInt),
            typeof(ObscuredFloat),
            typeof(ObscuredDouble),
            typeof(ObscuredDecimal),
            typeof(ObscuredChar),
            typeof(ObscuredByte),
            typeof(ObscuredBool),
            typeof(ObscuredLong),
            typeof(ObscuredQuaternion),
            typeof(ObscuredSByte),
            typeof(ObscuredShort),
            typeof(ObscuredUInt),
            typeof(ObscuredULong),
            typeof(ObscuredUShort),
            typeof(ObscuredVector2),
            typeof(ObscuredVector3),
            typeof(ObscuredString)
        };
 
        #region implemented abstract members of JsonConverter
 
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            if (value is ObscuredInt) {
                writer.WriteValue((int)(ObscuredInt)value);
            } else if (value is ObscuredBool) {
                writer.WriteValue((bool)(ObscuredBool)value);
            } else if (value is ObscuredFloat) {
                writer.WriteValue((float)(ObscuredFloat)value);
            }else if (value is ObscuredString) {
	            writer.WriteValue((string)(ObscuredString)value);
            }else if (value is ObscuredLong) {
	            writer.WriteValue((long)(ObscuredLong)value);
            }else if (value is ObscuredDouble) {
	            writer.WriteValue((double)(ObscuredDouble)value);
            }else {
                Debug.Log("ObscuredValueConverter type " + value.GetType().ToString() + " not implemented");
                writer.WriteValue(value.ToString());
            }
        }
 
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
            if (reader.Value != null) {
                if (objectType == typeof(ObscuredInt)) {
                    ObscuredInt value = Convert.ToInt32(reader.Value);
                    return value;
                } else if (objectType == typeof(ObscuredBool)) {
                    ObscuredBool value = Convert.ToBoolean(reader.Value);
                    return value;
                } else if (objectType == typeof(ObscuredFloat)) {
                    ObscuredFloat value = Convert.ToSingle(reader.Value);
                    return value;
                } else if (objectType == typeof(ObscuredString)) {
	                ObscuredString value = Convert.ToString(reader.Value);
	                return value;
                } else if (objectType == typeof(ObscuredLong)) {
	                ObscuredLong value = Convert.ToInt64(reader.Value);
	                return value;
                }else if (objectType == typeof(ObscuredDouble)) {
	                ObscuredDouble value = Convert.ToDouble(reader.Value);
	                return value;
                }else {
                    Debug.LogError("Code not implemented yet!");
                }
            }
            return null;
        }
 
        public override bool CanConvert(Type objectType) {
            return _types.Any(t => t == objectType);
        }
 
        #endregion
	}
}