﻿using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Helpers
{
	/// <summary>
	/// Prerequisite helper.
	/// </summary>
	public static class PrerequisiteHelper
	{
		/// <summary>
		/// Checks that all prerequisites are met
		/// </summary>
		/// <returns><c>true</c>, if all prerequisites are valid, <c>false</c> otherwise.</returns>
		/// <param name="prereqs">Prereqs.</param>
		public static bool CheckPrerequisites (List<IPrerequisite> prereqs)
		{
			bool result = true;

			if (prereqs == null || prereqs.Count == 0)
				return true;

			for (int i = 0; i < prereqs.Count; i++)
			{
				if (prereqs [i].Check () == false)
					return false;
			}

			return result;
		}
	}
}
