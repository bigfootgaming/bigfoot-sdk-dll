﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class ScriptableObjectsHelper
{
    /// <summary>
    /// Get all of the instances of a given ScriptableObject type in the project
    /// </summary>
    /// <typeparam name="T">The type</typeparam>
    /// <returns>All of the instances</returns>
    public static List<T> GetAllInstances<T>() where T : ScriptableObject
    {
        List<T> result = new List<T>();
        #if UNITY_EDITOR
        string[] guids = AssetDatabase.FindAssets("t:"+ typeof(T).Name);

        for(int i =0; i < guids.Length; i++)        
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            if(!path.Contains("Packages/com.bigfoot.sdk/"))
                result.Add(AssetDatabase.LoadAssetAtPath<T>(path));
        }
        #endif
        return result;
    }

    /// <summary>
    /// Gets the first instance of a given ScriptableObject type in the project
    /// </summary>
    /// <typeparam name="T">The type</typeparam>
    /// <returns>The instances</returns>
    public static T GetInstance<T>() where T : ScriptableObject
    {
        var allInstances = GetAllInstances<T>();
        if (allInstances != null && allInstances.Any())
        {
            return allInstances[0];
        }

        return null;
    }
}
