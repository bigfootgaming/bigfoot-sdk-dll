using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BigfootSdk.Helpers;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace BigfootSdk
{
    public static class AddressablesHelper
    {
        public static Action<string> OnAssetLoadFailed;
        public static Action OnAssetDownloadFailed;
        private static Dictionary<string,AsyncOperationHandle> _operationHandles = new ();

        public static async Task WaitForCompletion(string assetKey)
        {
            if (_operationHandles.ContainsKey(assetKey))
            {
                if (_operationHandles[assetKey].Status != AsyncOperationStatus.Succeeded)
                {
                    await _operationHandles[assetKey].Task;
                }
            }
        }


        public static async Task<T> LoadAssetAsync<T>(string assetKey)
        {
            if (_operationHandles.ContainsKey(assetKey))
            {
                if (_operationHandles[assetKey].Status == AsyncOperationStatus.Failed)
                {
                    _operationHandles.Remove(assetKey );
                    return await LoadAssetAsync<T>(assetKey);
                }

                if (_operationHandles[assetKey].Status != AsyncOperationStatus.Succeeded)
                {
                    await _operationHandles[assetKey].Task;
                }
                LogHelper.LogSdk($"returning existing operation handle for {assetKey}", LogHelper.ADDRESSABLES_TAG);
                return (T)_operationHandles[assetKey].Result;
            }

            try
            {
                var loadAsyncOperator = LoadAssetAsyncByKey<T>(assetKey);
                LogHelper.LogSdk($"adding operation handle for {assetKey}", LogHelper.ADDRESSABLES_TAG);
                _operationHandles.Add(assetKey,loadAsyncOperator);
                await loadAsyncOperator.Task;
                if (loadAsyncOperator.Status == AsyncOperationStatus.Succeeded)
                {
                    return loadAsyncOperator.Result;
                }
                else
                {
                    _operationHandles.Remove(assetKey );
                    OnAssetLoadFailed?.Invoke(assetKey);
                    LogHelper.LogError($"Failed to load Addressables with asset key {assetKey}", Environment.StackTrace);
                    return default;
                }
            }
            catch (Exception e)
            {
                _operationHandles.Remove(assetKey );
                OnAssetLoadFailed?.Invoke(assetKey);
                LogHelper.LogError($"Failed to load Addressables with asset key {assetKey}", Environment.StackTrace);
                return default;
            }
        }
        
        public static async Task LoadAssetAsync<T>(string assetKey, Action<T> onLoadedCallback)
        {
            try
            {
                var result = await LoadAssetAsync<T>(assetKey);
                onLoadedCallback?.Invoke(result);
            }
            catch (Exception e)
            {
                OnAssetLoadFailed?.Invoke(assetKey);
                LogHelper.LogError($"Failed to load Addressables with asset key {assetKey}: {e.Message}", Environment.StackTrace);
                onLoadedCallback?.Invoke(default);
            }
        }
        
        public static AsyncOperationHandle<T> LoadAssetAsyncByAssetReference<T>(AssetReference assetReference)
        {
            try
            {
                var loadAsyncOperator = assetReference.LoadAssetAsync<T>();
                return loadAsyncOperator;
            }
            catch (Exception e)
            {
                OnAssetLoadFailed?.Invoke(assetReference.RuntimeKey.ToString());
                LogHelper.LogError($"Failed to load Addressables with asset key {assetReference.RuntimeKey}: {e.Message}", Environment.StackTrace);
                return default;
            }
        }
        
        public static AsyncOperationHandle<T> LoadAssetAsyncByKey<T>(string assetKey)
        {
            try
            {
                var loadAsyncOperator = Addressables.LoadAssetAsync<T>(assetKey);
                return loadAsyncOperator;
            }
            catch (Exception e)
            {
                OnAssetLoadFailed?.Invoke(assetKey);
                LogHelper.LogError($"Failed to load Addressables with asset key {assetKey}: {e.Message}", Environment.StackTrace);
                return default;
            }
        }

        public static void Release(string assetKey)
        {
            if (_operationHandles.ContainsKey(assetKey))
            {
                LogHelper.LogSdk($"releasing operation handle for {assetKey}", LogHelper.ADDRESSABLES_TAG);
                Addressables.Release(_operationHandles[assetKey]);
                _operationHandles.Remove(assetKey);
            }
        }

        public static async void GetDownloadSizeAsync(List<string> list, Action<long> callback)
        {
            try
            {
                var getDownloadOperator = Addressables.GetDownloadSizeAsync(list);
                var result = await getDownloadOperator.Task;
                callback?.Invoke(result);
                Addressables.Release(getDownloadOperator);
            }
            catch (Exception e)
            {
                OnAssetDownloadFailed?.Invoke();
                LogHelper.LogError($"Failed to get download size for list: {e.Message}", Environment.StackTrace);
                callback?.Invoke(0);
            }
        }
        
        public static async void GetDownloadSizeAsync(string key, Action<long> callback)
        {
            try
            {
                var getDownloadOperator = Addressables.GetDownloadSizeAsync(key);
                var result = await getDownloadOperator.Task;
                callback?.Invoke(result);
                Addressables.Release(getDownloadOperator);
            }
            catch (Exception e)
            {
                OnAssetDownloadFailed?.Invoke();
                LogHelper.LogError($"Failed to get download size for list: {e.Message}", Environment.StackTrace);
                callback?.Invoke(0);
            }
        }
    }
}