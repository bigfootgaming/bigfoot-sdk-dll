﻿using System;
using BigfootSdk.Backend;

namespace BigfootSdk.Helpers
{
	/// <summary>
	/// Config helper.
	/// </summary>
	public static class ConfigHelper
	{
        /// <summary>
        /// Loads a config from the server (should be in TitleData)
        /// </summary>
        /// <returns>The config.</returns>
        /// <param name="name">Name.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T LoadConfig<T>(string name)
        {
            string jsonValue = TitleManager.Instance.GetFromTitleData(name);

            if (!string.IsNullOrEmpty(jsonValue))
                return JsonHelper.DesarializeObject<T>(jsonValue);
            else
                return default;
        }
	}
}