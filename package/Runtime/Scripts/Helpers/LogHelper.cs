﻿using UnityEngine;
using BigfootSdk.ErrorReport;

namespace BigfootSdk.Helpers
{
	/// <summary>
	/// Log helper. Use these methods instead of Unity's Debug.Log
	/// </summary>
	public static class LogHelper
	{
		public const string DEPENDENCY_MANAGER_TAG = "DependencyManager";
		public const string BACKEND_TIMEOUT_TAG = "BackendTimeout";
		public const string FIREBASE_SERVICES_TAG = "FirebaseServices";
		public const string TUTORIAL_TAG = "Tutorial";
		public const string LOCAL_NOTIFICATIONS_TAG = "LocalNotifications";
		public const string PANELS_MANAGER_TAG = "PanelsManager";
		public const string PLAYER_MANAGER_TAG = "PlayerManager";
		public const string TITLE_MANAGER_TAG = "TitleManager";
		public const string SPRITE_MANAGER_TAG = "SpriteManager";
		public const string ADDRESSABLES_TAG = "Addressables";
		public const string SHOP_TAG = "Shop";
		public const string POOL = "Pool";
		
		/// <summary>
		/// Gets the name of the current class.
		/// </summary>
		/// <value>Class name.</value>
		static string CurrentClass {
			get {
				var st = new System.Diagnostics.StackTrace ();

				var index = Mathf.Min (st.FrameCount - 1, 2);

				if (index < 0)
					return "{NoClass}";

				return st.GetFrame (index).GetMethod ().DeclaringType.Name;
			}
		}

		/// <summary>
		/// Log the specified message along with the name of the sender
		/// </summary>
		/// <param name="msg">Message.</param>
		public static void Log (string msg)
		{
			Debug.Log (string.Format ("<color=cyan>{0}</color> :: {1}", CurrentClass, msg));
		}

		/// <summary>
		/// Log the specified warning along with the name of the sender
		/// </summary>
		/// <param name="warning">Warning msg.</param>
		public static void LogWarning (string warning)
		{
			Debug.LogWarning (string.Format ("<color=yellow>{0}</color> :: {1}", CurrentClass, warning));
		}

		/// <summary>
		/// Log the specified message along with the name of the sender
		/// </summary>
		/// <param name="error">Error msg.</param>
		/// <param name="stackTrace">The error's stacktrace.</param>
		public static void LogError (string error, string stackTrace)
		{
            if (Application.isPlaying && ErrorReportManager.Instance != null)
            {
	            ErrorReportManager.Instance.LogException(error, stackTrace);
                Debug.LogError(string.Format("<color=red>{0}</color> :: {1}", CurrentClass, error));
            }
		}

		/// <summary>
		/// Log the specified message along with the name of the sender.
		/// ONLY FOR SDK LOGS
		/// </summary>
		/// <param name="msg">Message.</param>
		public static void LogSdk (string msg, string tag = "")
		{
			if(!Application.isPlaying || (SdkManager.Instance != null && SdkManager.Instance.Configuration.DebugSdkEvents && 
			                              (string.IsNullOrEmpty(tag) || !SdkManager.Instance.Configuration.IgnoredTagsInSdkLogs.Contains(tag))))
				Debug.Log (string.Format ("<color=cyan>{0}</color> :: {1}", CurrentClass, msg));
		}

        /// <summary>
        /// Log the specified message along with the name of the sender for all of the analytic events
        /// </summary>
        /// <param name="msg">Message.</param>
        public static void LogAnalytics(string msg)
        {
            if (!Application.isPlaying || (SdkManager.Instance != null && SdkManager.Instance.Configuration.AnalyticsConfiguration.DebugAnalyticsEvents))
                Debug.Log(string.Format("<color=green>{0}</color> :: {1}", CurrentClass, msg));
        }
    }
}
