﻿using System;
using System.Globalization;
using BigfootSdk.Backend;
using UnityEngine;

namespace BigfootSdk.Helpers
{
	/// <summary>
	/// Helper to work with Time
	/// </summary>
	public static class TimeHelper
	{
        /// <summary>
        /// The server time in title data.
        /// </summary>
        private static string _serverTimeInTitleData;

        private static bool _isServerTimeSync = false;

        static void GetServerValue()
        {
            // Grab the server time in TitleData
            if (TitleManager.Instance != null && !_isServerTimeSync)
            {
                _isServerTimeSync = BackendManager.Instance.IsServerTimeSync;
                _serverTimeInTitleData = TitleManager.Instance.GetFromTitleData("server_time");
            }

            if (string.IsNullOrEmpty(_serverTimeInTitleData))
            {
                //if title data hasn't server_time create it locally
                _serverTimeInTitleData = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
            }
        }

        /// <summary>
        /// Gets the time in server.
        /// </summary>
        /// <returns>The time in server.</returns>
        public static DateTime GetTimeInServerDateTime()
        {
            GetServerValue();
            
            // Parse the string
            long serverStartTime = long.Parse(_serverTimeInTitleData, CultureInfo.InvariantCulture);

            // Convert to DateTime
            DateTime dt = FromUnixTime(serverStartTime);
            long timeTravel = 0;
            if(PlayerManager.Instance != null)
                timeTravel = PlayerManager.Instance.GetTimer(TimersConstants.TIME_TRAVEL, GameModeConstants.MAIN_GAME, 0);
            // Add the seconds since startup
            return dt.AddSeconds(Time.realtimeSinceStartup + timeTravel);
        }

        /// <summary>
        /// Gets the time in server.
        /// </summary>
        /// <returns>The time in server.</returns>
        public static long GetTimeInServer()
        {
            GetServerValue();

            long timeTravel = 0;
            if(PlayerManager.Instance != null)
                timeTravel =PlayerManager.Instance.GetTimer(TimersConstants.TIME_TRAVEL, GameModeConstants.MAIN_GAME, 0);

            // Parse the string & add the seconds since the app started
            return long.Parse(_serverTimeInTitleData, CultureInfo.InvariantCulture) + (long)(Time.realtimeSinceStartup) + timeTravel;
        }

        /// <summary>
        /// Gets how many seconds passed since the event
        /// </summary>
        /// <returns>The time elapsed since event.</returns>
        /// <param name="eventTime">Event time.</param>
        public static long GetTimeElapsedSinceEvent(long eventTime)
        {
            return GetTimeInServer() - eventTime;
        }

        /// <summary>
        /// Get how many seconds remain until the event
        /// </summary>
        /// <returns>The time remaining until event.</returns>
        /// <param name="eventTime">Event time.</param>
        public static long GetTimeRemainingUntilEvent(long eventTime)
        {
            return eventTime - GetTimeInServer();
        }

        /// <summary>
        /// Converts seconds in unix time to Datetime
        /// </summary>
        /// <returns>the time in DateTime.</returns>
        /// <param name="unixTime">Unix time.</param>
        public static DateTime FromUnixTime(long unixTime)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        /// <summary>
        /// Converts a Datetime to unix time
        /// </summary>
        /// <param name="dateTime">The time in DateTime</param>
        /// <returns>The time in unix time</returns>
        public static long ToUnixTime(DateTime dateTime)
        {
            return (long)dateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public static DateTime ToLocal (DateTime utcTime)
        {
            return utcTime.ToLocalTime();
        }

    }
}