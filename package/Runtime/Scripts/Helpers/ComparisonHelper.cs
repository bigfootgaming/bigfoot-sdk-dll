﻿namespace BigfootSdk.Helpers
{
	/// <summary>
	/// Comparison helper.
	/// </summary>
	public class ComparisonHelper
	{
		/// <summary>
		/// Compares value1 against value2, using the comparisonType.
		/// </summary>
		/// <param name="value1">Value1.</param>
		/// <param name="value2">Value2.</param>
		/// <param name="comparisonType">Comparison type constant</param>
		public static bool Compare (int value1, int value2, string comparisonType)
		{
			bool result = false;

			if (!string.IsNullOrEmpty (comparisonType)) {
				switch (comparisonType) {
				case ComparisonConstants.EQ:
					result = (value1 == value2);
					break;

				case ComparisonConstants.GE:
					result = (value1 >= value2);
					break;

				case ComparisonConstants.GT:
					result = (value1 > value2);
					break;

				case ComparisonConstants.LE:
					result = (value1 <= value2);
					break;

				case ComparisonConstants.LT:
					result = (value1 < value2);
					break;

				case ComparisonConstants.NE:
					result = (value1 != value2);
					break;

				default:
					LogHelper.LogSdk ("Unsupported comparison type " + comparisonType);
					break;
				}
			} else {
				LogHelper.LogSdk ("Comparison type is null or empty.");
			}

			return result;
		}
		
		/// <summary>
		/// Compares value1 against value2, using the comparisonType.
		/// </summary>
		/// <param name="value1">Value1.</param>
		/// <param name="value2">Value2.</param>
		/// <param name="comparisonType">Comparison type constant</param>
		public static bool Compare (long value1, long value2, string comparisonType)
		{
			bool result = false;

			if (!string.IsNullOrEmpty (comparisonType)) {
				switch (comparisonType) {
					case ComparisonConstants.EQ:
						result = (value1 == value2);
						break;

					case ComparisonConstants.GE:
						result = (value1 >= value2);
						break;

					case ComparisonConstants.GT:
						result = (value1 > value2);
						break;

					case ComparisonConstants.LE:
						result = (value1 <= value2);
						break;

					case ComparisonConstants.LT:
						result = (value1 < value2);
						break;

					case ComparisonConstants.NE:
						result = (value1 != value2);
						break;

					default:
						LogHelper.LogSdk ("Unsupported comparison type " + comparisonType);
						break;
				}
			} else {
				LogHelper.LogSdk ("Comparison type is null or empty.");
			}

			return result;
		}
		
		/// <summary>
		/// Compares value1 against value2, using the comparisonType.
		/// </summary>
		/// <param name="value1">Value1.</param>
		/// <param name="value2">Value2.</param>
		/// <param name="comparisonType">Comparison type constant</param>
		public static bool Compare (string value1, string value2, string comparisonType)
		{
			bool result = false;

			if (!string.IsNullOrEmpty (comparisonType)) {
				switch (comparisonType) {
					case ComparisonConstants.EQ:
						result = (value1 == value2);
						break;
					case ComparisonConstants.NE:
						result = (value1 != value2);
						break;

					default:
						LogHelper.LogSdk ("Unsupported comparison type " + comparisonType);
						break;
				}
			} else {
				LogHelper.LogSdk ("Comparison type is null or empty.");
			}

			return result;
		}
		
		/// <summary>
		/// Compares value1 against value2, using the comparisonType.
		/// </summary>
		/// <param name="value1">Value1.</param>
		/// <param name="value2">Value2.</param>
		/// <param name="comparisonType">Comparison type constant</param>
		public static bool Compare (bool value1, bool value2, string comparisonType)
		{
			bool result = false;

			if (!string.IsNullOrEmpty (comparisonType)) {
				switch (comparisonType) {
					case ComparisonConstants.EQ:
						result = (value1 == value2);
						break;
					case ComparisonConstants.NE:
						result = (value1 != value2);
						break;

					default:
						LogHelper.LogSdk ("Unsupported comparison type " + comparisonType);
						break;
				}
			} else {
				LogHelper.LogSdk ("Comparison type is null or empty.");
			}

			return result;
		}
	}
}