﻿#if BACKEND_FIREBASE_FIRESTORE
using System.Collections;
using System.Collections.Generic;
using BigfootSdk.Backend;
using UnityEngine;

namespace BigfootSdk.Helpers
{
    public static class FirestoreHelper 
    {
        public static FirestorePlayerModel ToFirestore(PlayerModel model)
        {
            FirestorePlayerModel result = new FirestorePlayerModel();
            result.PlayerId = model.PlayerId;
            result.LastSave = model.LastSave;
            result.Token = model.Token;
            result.PlayerInfoModels = ToFirestore(model.PlayerInfoModels);
            return result;
        }

        static Dictionary<string, FirestorePlayerInfoModel> ToFirestore(Dictionary<string, PlayerInfoModel> model)
        {
            var result = new Dictionary<string, FirestorePlayerInfoModel>();

            foreach (KeyValuePair<string, PlayerInfoModel> infoModel in model)
            {
                FirestorePlayerInfoModel firestorePlayerInfoModel = new FirestorePlayerInfoModel();
                foreach (var d in infoModel.Value.Data)
                {
                    firestorePlayerInfoModel.Data.Add(d.Key, d.Value);
                }
                foreach (var c in infoModel.Value.Currencies)
                {
                    firestorePlayerInfoModel.Currencies.Add(c.Key, c.Value);
                }
                foreach (var c in infoModel.Value.Counters)
                {
                    firestorePlayerInfoModel.Counters.Add(c.Key, c.Value);
                }
                foreach (var f in infoModel.Value.Flags)
                {
                    firestorePlayerInfoModel.Flags.Add(f.Key, f.Value);
                }
                foreach (var t in infoModel.Value.Timers)
                {
                    firestorePlayerInfoModel.Timers.Add(t.Key, t.Value);
                }
                foreach (var i in infoModel.Value.Items)
                {
                    firestorePlayerInfoModel.Items.Add(ToFirestore(i));
                }
                
                result.Add(infoModel.Key, firestorePlayerInfoModel);
            }
            
            return result;
        }

        static FirestorePlayerItemModel ToFirestore(PlayerItemModel model)
        {
            FirestorePlayerItemModel result = new FirestorePlayerItemModel();
            result.Id = model.Id;
            result.Name = model.Name;
            result.Type = model.Type;
            result.Class = model.Class;
            result.AmountRemaining = model.AmountRemaining;
            foreach (var d in model.Data)
            {
                result.Data.Add(d.Key, d.Value);
            }

            return result;
        }

        public static PlayerModel FromFirestore(FirestorePlayerModel model)
        {
            PlayerModel result = new PlayerModel();
            result.PlayerId = model.PlayerId;
            result.LastSave = model.LastSave;
            result.Token = model.Token;
            result.PlayerInfoModels = FromFirestore(model.PlayerInfoModels);
            return result;
        }

        static Dictionary<string, PlayerInfoModel> FromFirestore(Dictionary<string, FirestorePlayerInfoModel> model)
        {
            var result = new Dictionary<string, PlayerInfoModel>();

            foreach (KeyValuePair<string, FirestorePlayerInfoModel> infoModel in model)
            {
                PlayerInfoModel playerInfoModel = new PlayerInfoModel();
                foreach (var d in infoModel.Value.Data)
                {
                    playerInfoModel.Data.Add(d.Key, d.Value);
                }
                foreach (var c in infoModel.Value.Currencies)
                {
                    playerInfoModel.Currencies.Add(c.Key, c.Value);
                }
                foreach (var c in infoModel.Value.Counters)
                {
                    playerInfoModel.Counters.Add(c.Key, c.Value);
                }
                foreach (var f in infoModel.Value.Flags)
                {
                    playerInfoModel.Flags.Add(f.Key, f.Value);
                }
                foreach (var t in infoModel.Value.Timers)
                {
                    playerInfoModel.Timers.Add(t.Key, t.Value);
                }
                foreach (var i in infoModel.Value.Items)
                {
                    playerInfoModel.Items.Add(FromFrestore(i));
                }
                
                result.Add(infoModel.Key, playerInfoModel);
            }

            return result;
        }

        static PlayerItemModel FromFrestore (FirestorePlayerItemModel model)
        {
            PlayerItemModel result = new PlayerItemModel();
            result.Id = model.Id;
            result.Name = model.Name;
            result.Type = model.Type;
            result.Class = model.Class;
            result.AmountRemaining = model.AmountRemaining;
            foreach (var d in model.Data)
            {
                result.Data.Add(d.Key, d.Value);
            }

            return result;
        }
    }
}


#endif