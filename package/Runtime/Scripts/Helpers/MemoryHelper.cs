using System;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk 
{
    public static class MemoryHelper
    {
        
        public static void TryCleanMemory(bool garbageCollector, bool unloadResources, string origin)
        {
            if (unloadResources)
            {
                LogHelper.LogSdk($"Calling UnloadUnusedAssets from {origin}");
                Resources.UnloadUnusedAssets();
                LogHelper.LogSdk($"UnloadUnusedAssets done from {origin}");
            }
            if (garbageCollector)
            {
                LogHelper.LogSdk($"Calling GC collect from {origin}");
                GC.Collect();
                LogHelper.LogSdk($"GC collect done from {origin}");
            }
        }
    }
}