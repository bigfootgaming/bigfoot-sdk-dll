﻿
namespace BigfootSdk
{
	public class AdTypes
	{
		public const int NONE                = 0;
		public const int INTERSTITIAL 		 = 3;
		public const int BANNER_BOTTOM       = 8;
		public const int BANNER_TOP          = 16;
		public const int REWARDED_VIDEO      = 128;
	#if UNITY_ANDROID || UNITY_EDITOR
		public const int NON_SKIPPABLE_VIDEO = 128;
	#elif UNITY_IPHONE
		public const int NON_SKIPPABLE_VIDEO = 256;
	#endif
	}
}