﻿using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Core.Idle;
using UnityEngine;

namespace BigfootSdk
{
	/// <summary>
	/// Upgrade model
	/// </summary>
	public class UpgradeModel
	{
        /// <summary>
        /// The name of the upgrade.
        /// </summary>
        public string UpgradeName;

        /// <summary>
        /// The cost.
        /// </summary>
        public ICost Cost;

		/// <summary>
		/// The list of modifiers to apply
		/// </summary>
		public List <GameplayModifierModel> GameplayModifiers;
	}
}