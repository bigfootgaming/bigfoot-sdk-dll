﻿
using System.Collections.Generic;

namespace BigfootSdk.Shop
{
    /// <summary>
    /// Model for the shop sections
    /// </summary>
    [System.Serializable]
    public class ShopSectionModel
    {
        /// <summary>
        /// The id of the section
        /// </summary>
        public string Id;

        /// <summary>
        /// The name of the prefab for this section
        /// </summary>
        public string PrefabName;

        /// <summary>
        /// The placement where this section belong to.
        /// </summary>
        public string Placement;

        /// <summary>
        /// The type of the timer.
        /// </summary>
        public ShopTimer Timer;

        /// <summary>
        /// Prerequisites to enable this section.
        /// </summary>
        public List<IPrerequisite> Prereqs;

        /// <summary>
        /// The custom data.
        /// </summary>
        public Dictionary<string, object> Data;
    }
}

