﻿using System.Collections;
using System.Collections.Generic;
using BigfootSdk.Backend;
using UnityEngine;

/// <summary>
/// League model
/// </summary>
public class LeagueModel
{
    /// <summary>
    /// League tier models
    /// </summary>
    public LeagueTierModel[] Tiers;
    
    /// <summary>
    /// Duration of leagues (in days)
    /// </summary>
    public int Duration;
}

public class LeagueTierModel
{
    /// <summary>
    /// Tier index
    /// </summary>
    public int Tier;
    
    /// <summary>
    /// Max amount of players per league in this tier. Default value -1 means no restrictions
    /// </summary>
    public int MaxPlayers = -1;
    
    /// <summary>
    /// The reward given to players when league ends.
    /// </summary>
    public LeagueTierReward[] Rewards;
}


public class LeagueTierReward
{
    /// <summary>
    /// Min percentage for this reward. Values between 0 and 1. Lower values mean better positions.
    /// </summary>
    public float MinPercentage;
    /// <summary>
    /// Max percentage for this reward. Values between 0 and 1. Lower values mean better positions.
    /// </summary>
    public float MaxPercentage;
    
    /// <summary>
    /// The reward
    /// </summary>
    public ITransaction Reward;
}