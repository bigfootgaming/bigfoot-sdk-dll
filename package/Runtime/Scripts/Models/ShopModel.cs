﻿using System;
using System.Collections.Generic;

namespace BigfootSdk.Shop
{
	[Serializable]
	/// <summary>
	/// Shop model
	/// </summary>
	public class ShopModel
	{
		/// <summary>
		/// The shop sections.
		/// </summary>
		public List<ShopSectionModel> ShopSections;
		
        /// <summary>
        /// The shop items.
        /// </summary>
        public List<ShopItemModel> ShopItems;
    }
}