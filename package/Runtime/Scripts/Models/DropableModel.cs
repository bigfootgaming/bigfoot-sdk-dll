﻿using Sirenix.OdinInspector;
namespace BigfootSdk.Backend
{
    /// <summary>
    /// Dropable model.
    /// </summary>
	[System.Serializable]
    public abstract class DropableModel
    {
        /// <summary>
        /// The weight of drop for this model
        /// </summary>
        [ReadOnly]
        public float Weight = 1;

        /// <summary>
        /// The amount.
        /// </summary>
        [ReadOnly]
        public int Amount = 1;
    }
}
