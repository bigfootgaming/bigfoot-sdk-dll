﻿using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Player Info model.
	/// </summary>
	public class PlayerInfoModel
	{
		/// <summary>
		/// The player data.
		/// </summary>
		public Dictionary<string, ObscuredString> Data { get; set; } = new Dictionary<string, ObscuredString>();

		/// <summary>
		/// The currencies.
		/// </summary>
		public Dictionary<string, ObscuredInt> Currencies { get; set; } = new Dictionary<string, ObscuredInt>();

		/// <summary>
		/// The currencies.
		/// </summary>
		public Dictionary<string, ObscuredInt> Counters { get; set; } = new Dictionary<string, ObscuredInt>(); 
		
		/// <summary>
		/// The currencies.
		/// </summary>
		public Dictionary<string, ObscuredBool> Flags { get; set; } = new Dictionary<string, ObscuredBool>();
		
		/// <summary>
		/// The currencies.
		/// </summary>
		public Dictionary<string, ObscuredLong> Timers { get; set; } = new Dictionary<string, ObscuredLong>();
		
		/// <summary>
        /// The items.
        /// </summary>
        public List<PlayerItemModel> Items { get; set; } = new List<PlayerItemModel>();

	}

}
