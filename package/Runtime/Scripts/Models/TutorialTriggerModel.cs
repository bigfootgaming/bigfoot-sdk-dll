﻿using System;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial trigger model.
    /// </summary>
    [Serializable]
    public class TutorialTriggerModel
    {
        /// <summary>
        /// The tutorial identifier.
        /// </summary>
        public string TutorialId;
        /// <summary>
        /// The name of the stage.
        /// </summary>
        public string StageName;
        /// <summary>
        /// The trigger type.
        /// </summary>
        public TutorialTriggerType TriggerType;
        /// <summary>
        /// The action.
        /// </summary>
        public TutorialAction Action;
        /// <summary>
        /// if <see langword="true"/> may retrigger.
        /// </summary>
        public bool ListenForRetriggers = true;
        /// <summary>
        /// seconds to wait to trigger.
        /// </summary>
        public float TriggerAfterSeconds;
        /// <summary>
        /// frames to wait to trigger.
        /// </summary>
        public int TriggerAfterFrames;
    }

    /// <summary>
    /// Tutorial trigger type.
    /// </summary>
    public enum TutorialTriggerType
    {
        OnEnable = 0,
        OnDisable = 1,
        OnStart = 2,
        OnDestroy = 3,
        OnClick = 4,
        OnEnter = 5,
        OnPanelHidden = 6,
        OnPanelShown = 7,
        OnEverythingLoaded = 9,
        Other = 8
    }

    /// <summary>
    /// Tutorial action.
    /// </summary>
    public enum TutorialAction
    {
        StartStage,
        CompleteStage,
        SkipTutorial
    }
}

