
using Sirenix.OdinInspector;

namespace BigfootSdk.Backend
{
    [System.Serializable]
    public class GachableItemModel : DropableItemModel
    {
        [ReadOnly]
        public int Value;
    }
}