﻿using System;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk.Analytics
{
    /// <summary>
    /// Variant Model
    /// </summary>
    [Serializable]
    public class VariantModel
    {
        /// <summary>
        /// The experiment id
        /// </summary>
        public string Id;

        /// <summary>
        /// The table
        /// </summary>
        public string Table;

        /// <summary>
        /// The variant id
        /// </summary>
        public string VariantId;
    }
}