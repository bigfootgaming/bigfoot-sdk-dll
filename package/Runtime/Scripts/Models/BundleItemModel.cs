﻿using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Bundle item model.
    /// </summary>
	[System.Serializable]
    public class BundleItemModel : BaseItemModel
    {
        /// <summary>
        /// List of dropable items
        /// </summary>
        [VerticalGroup("Data")]
        [TableList(IsReadOnly = true, ShowIndexLabels = true), ShowInInspector, ShowIf("ItemsIsNotNullOrEmpty"), PropertyOrder(2)]
        public List<DropableModel> Items;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.Backend.BundleItemModel"/> class.
        /// </summary>
        public BundleItemModel()
        {
            Type = "Bundle";
        }

#if UNITY_EDITOR
        /// <summary>
        /// Check for the inspector to know if there is some items to show
        /// </summary>
        private bool ItemsIsNotNullOrEmpty()
        {
            return !(Items == null || Items.Count <= 0);
        }
#endif
    }
}
