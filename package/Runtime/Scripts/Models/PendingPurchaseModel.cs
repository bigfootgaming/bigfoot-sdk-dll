
using BigfootSdk.Backend;
namespace BigfootSdk.Shop
{
    public class PendingPurchaseModel
    {

        public string ShopItemId;

        public TransactionResultModel ResultModel;

        public object Purchase;
    }
}