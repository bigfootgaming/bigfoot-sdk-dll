﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Level progress model.
	/// </summary>
	[System.Serializable]
	public class LevelProgressModel
	{
		/// <summary>
		/// The world identifier.
		/// </summary>
		public int WorldId;

		/// <summary>
		/// The level identifier.
		/// </summary>
		public int LevelId;

		/// <summary>
		/// The score.
		/// </summary>
		public int Score;

	}
}
