﻿#if BACKEND_FIREBASE_FIRESTORE
using System;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Player model.
    /// </summary>
    [FirestoreData]
    [Serializable]
    public class FirestorePlayerModel 
    {
        /// <summary>
        /// The player identifier.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredStringConverter))]
        public ObscuredString PlayerId { get; set; }

        /// <summary>
        /// Last time the player model was saved.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredLongConverter))]
        public ObscuredLong LastSave { get; set; }
        
        /// <summary>
        /// Last time the player model was saved.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredStringConverter))]
        public ObscuredString Token { get; set; }

        /// <summary>
        /// The player info.
        /// </summary>
        [FirestoreProperty]
        public Dictionary<string, FirestorePlayerInfoModel> PlayerInfoModels { get; set; }

        /// <summary>
        /// If true the firestore data will override the local data.
        /// </summary>
        [FirestoreProperty]
        public bool ForceUpdate { get; set; }
        
        /// <summary>
        /// If true the firestore data will be reset and a new user will be created
        /// </summary>
        [FirestoreProperty]
        public bool ForceReset { get; set; }
    }

}
#endif
