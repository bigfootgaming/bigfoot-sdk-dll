﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Level model.
	/// </summary>
	[System.Serializable]
	public class LevelModel
	{
		/// <summary>
		/// The world identifier.
		/// </summary>
		public int WorldId;

		/// <summary>
		/// The level identifier.
		/// </summary>
		public int LevelId;

		/// <summary>
		/// The level milestones
		/// </summary>
		public int[] LevelMilestones;

		/// <summary>
		/// The rewards.
		/// </summary>
		public List<ITransaction> Rewards;

		/// <summary>
		/// The prerequisites.
		/// </summary>
		public List<IPrerequisite> Prereqs;

		/// <summary>
		/// The custom data.
		/// </summary>
		public Dictionary<string, string> CustomData;
	}
}
