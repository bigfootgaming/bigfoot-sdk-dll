﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace BigfootSdk.Panels
{
    /// <summary>
    /// Panel configuration.
    /// </summary>
    [System.Serializable]
    public class PanelConfigModel
    {
        /// <summary>
        /// The name or identifier of the panel.
        /// </summary>
        public string PanelName;
        /// <summary>
        /// if <see langword="true"/> the will use an overlay behind itself.
        /// </summary>
        public bool UseOverlay = true;
        /// <summary>
        /// Should this panel send an analytics event
        /// </summary>
        public bool SendAnalyticsEvent = false;
        /// <summary>
        /// If <see langword="true"/> the panel will respond on hardware back button.
        /// </summary>
        public bool AcceptHardwareBack = true;

        /// <summary>
        /// The panel local position after being instantiated.
        /// </summary>
        public Vector3 Position;
        /// <summary>
        /// If <see langword="true"/> the panel will wait the previous panel animation before showing up.
        /// </summary>
        public bool WaitForPanelAnimations = true;
    }
}
