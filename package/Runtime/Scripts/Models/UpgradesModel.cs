﻿using System.Collections.Generic;

namespace BigfootSdk
{
	public class UpgradesConfig
	{
		/// <summary>
		/// All the upgrades models
		/// </summary>
		public List<UpgradeModel> Upgrades;
	}
}