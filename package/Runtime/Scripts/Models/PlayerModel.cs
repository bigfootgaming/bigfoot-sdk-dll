﻿using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Player model.
	/// </summary>
	public class PlayerModel
	{
		/// <summary>
		/// The player identifier.
		/// </summary>
		public ObscuredString PlayerId { get; set; }

		/// <summary>
		/// Last time the player model was saved.
		/// </summary>
		public ObscuredLong LastSave { get; set; }
		
		public ObscuredString Token { get; set; }

		/// <summary>
		/// The multiple player info
		/// </summary>
		public Dictionary<string,PlayerInfoModel> PlayerInfoModels { get; set; }

	}

}
