﻿namespace BigfootSdk.Backend
{
	/// <summary>
	/// Data Update Model.
	/// </summary>
	[System.Serializable]
	public class DataUpdateModel
    {
        /// <summary>
        /// The value.
        /// </summary>
        public string Value;

        /// <summary>
        /// if <see langword="true"/> the (number) is added to the existing value.
        /// </summary>
        public bool IsUpdate;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.Backend.DataUpdateModel"/> class.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="isUpdate">If set to <c>true</c> is update.</param>
        public DataUpdateModel(string value, bool isUpdate = false)
        {
            Value = value;
            IsUpdate = isUpdate;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.Backend.DataUpdateModel"/> class.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="isUpdate">If set to <c>true</c> is update.</param>
        public DataUpdateModel(int value, bool isUpdate = false)
        {
            Value = value.ToString();
            IsUpdate = isUpdate;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.Backend.DataUpdateModel"/> class.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="isUpdate">If set to <c>true</c> is update.</param>
        public DataUpdateModel(long value, bool isUpdate = false)
        {
            Value = value.ToString();
            IsUpdate = isUpdate;
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.Backend.DataUpdateModel"/> class.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="isUpdate">If set to <c>true</c> is update.</param>
        public DataUpdateModel(double value, bool isUpdate = false)
        {
            Value = value.ToString();
            IsUpdate = isUpdate;
        }

        public override string ToString()
        {
            return string.Format("{0} : {1}", Value, IsUpdate);
        }
    }
}
