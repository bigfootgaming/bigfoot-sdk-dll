﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BigfootSdk.Tutorials {
    /// <summary>
    /// Tutorial progress.
    /// </summary>
    [System.Serializable]
    public class TutorialProgressModel
    {
        /// <summary>
        /// The tutorial identifier.
        /// </summary>
        public string TutorialId;

        /// <summary>
        /// The current stage.
        /// </summary>
        public int CurrentStage;
    }
}

