﻿using System;
using System.Collections.Generic;
using BigfootSdk.Helpers;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Base item model.
	/// </summary>
	[Serializable]
	public class BaseItemModel
	{
		/// <summary>
		/// The item name.
		/// </summary>
		[ReadOnly]
		[VerticalGroup("MainInfo")]
		public string ItemName;

		/// <summary>
		/// The type. It can be Item, Bundle, DropTable, etc
		/// </summary>
		[ReadOnly]
		[VerticalGroup("MainInfo")]
		public string Type;

		/// <summary>
		/// If <see langword="true"/> all instances will be in one object.
		/// </summary>
		[ReadOnly]
		[VerticalGroup("MainInfo")]
		public bool IsStackable;

		/// <summary>
		/// The class of this item
		/// </summary>
		[ReadOnly]
		[VerticalGroup("MainInfo")]
		public string Class = "";

		/// <summary>
		/// Which sprite to use
		/// </summary>
		[ReadOnly]
		[VerticalGroup("MainInfo")]
		public string SpriteKey;

		/// <summary>
		/// Localization key representing the item's name
		/// </summary>
		[ReadOnly]
		[VerticalGroup("MainInfo")]
		public string LocalizationKey;

		/// <summary>
		/// The item custom data.
		/// </summary>
		public Dictionary<string, object> Data;

#if UNITY_EDITOR
		/// <summary>
		/// Serialized data to show from Data
		/// </summary>
		[VerticalGroup("Data"), ShowInInspector, DictionaryDrawerSettings(IsReadOnly = true), ShowIf("ShowDataInInspector"), PropertyOrder(1)]
		public Dictionary<string, string> SerializedRawData;
#endif
		
		/// <summary>
		/// Initializes a new instance of the <see cref="T:BigfootSdk.Backend.BaseItemModel"/> class.
		/// </summary>
		public BaseItemModel()
		{
			Type = "Item";
		}


#if UNITY_EDITOR
		/// <summary>
		/// Used to show in the inspector the serialized data from the dict Data 
		/// </summary>
		public void SerializeRawDataForInspector()
		{
			if (Data == null)
			{
				return;
			}

			SerializedRawData = null;

			foreach (KeyValuePair<string, object> keyValuePair in Data)
			{
				if (SerializedRawData == null)
				{
					SerializedRawData = new Dictionary<string, string>();
				}

				SerializedRawData.Add(keyValuePair.Key, JsonHelper.SerializeObject(keyValuePair.Value));
			}
		}

		/// <summary>
		/// Check for the inspector to know if there is some data to serialize
		/// </summary>
		private bool DataIsNotNullOrEmpty()
		{
			return !(Data == null || Data.Count <= 0);
		}

		/// <summary>
		/// Check for the inspector to know if there is some serialized data to show
		/// </summary>
		private bool ShowDataInInspector()
		{
			return SerializedRawData != null && SerializedRawData.Count > 0;
		}
#endif

		/// <summary>
		/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:BigfootSdk.Backend.BaseItemModel"/>.
		/// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:BigfootSdk.Backend.BaseItemModel"/>.</returns>
        public override string ToString()
        {
            return string.Format("ItemName: {0} Type: {1}", ItemName, Type);
        }

        /// <summary>
        /// Determines whether the specified <see cref="object"/> is equal to the current <see cref="T:BigfootSdk.Backend.BaseItemModel"/>.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare with the current <see cref="T:BigfootSdk.Backend.BaseItemModel"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="object"/> is equal to the current
        /// <see cref="T:BigfootSdk.Backend.BaseItemModel"/>; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                BaseItemModel item = (BaseItemModel)obj;
                return ItemName == item.ItemName;
            }
        }

        /// <summary>
        /// Serves as a hash function for a <see cref="T:BigfootSdk.Backend.BaseItemModel"/> object.
        /// </summary>
        /// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a
        /// hash table.</returns>
        public override int GetHashCode()
        {
            return this.ItemName.GetHashCode();
        }
    }
}
