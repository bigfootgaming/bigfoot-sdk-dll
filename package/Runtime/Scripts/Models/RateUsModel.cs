﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
    [System.Serializable]
    public class RateUsModel
    {
        /// <summary>
        /// The list of prereqs to show
        /// </summary>
        public List<IPrerequisite> ShowPrereqs;
    }
}
