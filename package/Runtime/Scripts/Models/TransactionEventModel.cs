﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Analytics
{ 
    /// <summary>
    /// Transaction event model.
    /// </summary>
    [Serializable]
    public class TransactionEventModel
    {
        /// <summary>
        /// The type of the transaction.
        /// </summary>
        public string TransactionType;

        /// <summary>
        /// Where this transaction came from
        /// </summary>
        public string Origin;

        /// <summary>
        /// The name of the item
        /// </summary>
        public string ItemName;

        /// <summary>
        /// The sinks.
        /// </summary>
        public List<TransactionEventParamenterModel> Sinks;

        /// <summary>
        /// The sources.
        /// </summary>
        public List<TransactionEventParamenterModel> Sources;
    }
}