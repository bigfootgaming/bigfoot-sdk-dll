﻿using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Title model.
	/// </summary>
	[System.Serializable]
	public class TitleModel
	{
		/// <summary>
		/// The title data
		/// </summary>
		public Dictionary<string, ObscuredString> Data = new Dictionary<string, ObscuredString> ();
	}
}
