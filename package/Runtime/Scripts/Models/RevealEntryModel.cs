﻿namespace BigfootSdk
{
    /// <summary>
    /// This class contain the basic info needed to create reveal entries for the reveal system
    /// </summary>
    public class RevealEntryModel
    {
        /// <summary>
        /// Entry id
        /// </summary>
        public string Id;

        /// <summary>
        /// Entry amount
        /// </summary>
        public double Amount;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">Id of the entry</param>
        /// <param name="amount">Entry amount</param>
        public RevealEntryModel(string id, double amount)
        {
            Id = id;
            Amount = amount;
        }
    }
}