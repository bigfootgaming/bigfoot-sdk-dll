﻿using System;
using System.Collections.Generic;
using BigfootSdk.Backend;

namespace BigfootSdk.Shop
{
	[Serializable]
	/// <summary>
	/// ShopItem model
	/// </summary>
	public class ShopItemModel
	{
        /// <summary>
        /// The name of the shop item.
        /// </summary>
        public string Id;

        /// <summary>
        /// The item name.
        /// </summary>
        public string ItemName;

        /// <summary>
        ///The id of the section to show this on
        /// </summary>
        public string SectionId;

        /// <summary>
        /// The name of the prefab to use
        /// </summary>
        public string PrefabName;

        /// <summary>
        /// The amount of times the user can buy this shop item. If it is less than or equal to 0, it is unlimited.
        /// </summary>
        public int Amount;
        
        /// <summary>
        /// The cost of this item
        /// </summary>
        public ICost Cost;

        /// <summary>
        /// The type of the timer.
        /// </summary>
        public ShopTimer Timer;
        
        /// <summary>
        /// Prerequisites to enable this item.
        /// </summary>
        public List<IPrerequisite> Prereqs = new List<IPrerequisite>();

        /// <summary>
        /// The ShopItem custom data.
        /// </summary>
        public Dictionary<string, object> Data;
    }
}