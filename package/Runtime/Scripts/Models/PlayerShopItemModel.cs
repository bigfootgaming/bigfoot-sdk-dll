using System;

namespace BigfootSdk.Shop
{
    [Serializable]
    public class PlayerShopItemModel
    {
        /// <summary>
        /// The id of the shop item
        /// </summary>
        public string Id;
    
        /// <summary>
        /// The amount remaining to purchase for this shop item
        /// </summary>
        public int Amount;
    }
}

