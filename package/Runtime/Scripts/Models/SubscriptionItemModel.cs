using System;
using System.Collections.Generic;
using BigfootSdk.Helpers;
using Sirenix.OdinInspector;

namespace BigfootSdk.Backend
{
    public class SubscriptionItemModel : BaseItemModel
    {
        /// <summary>
        /// List of dropable items
        /// </summary>
        [VerticalGroup("Data")]
        [TableList(IsReadOnly = true, ShowIndexLabels = true), ShowInInspector, ShowIf("ItemsIsNotNullOrEmpty"),
         PropertyOrder(2)]
        public List<DropableModel> Items;


        public List<DropableModel> FirstTimeItems;

        public string Frequency; // Daily, Weekly, Monthly. You can add you own frequency and override the HandleSubscription method to support it

        public SubscriptionItemModel()
        {
            Type = "Subscription";
        }

#if UNITY_EDITOR
        /// <summary>
        /// Check for the inspector to know if there is some items to show
        /// </summary>
        private bool ItemsIsNotNullOrEmpty()
        {
            return !(Items == null || Items.Count <= 0);
        }
#endif

        public virtual TransactionResultModel HandleSubscription()
        {
            TransactionResultModel result = new TransactionResultModel();
            int rewardTimes =
                PlayerManager.Instance.GetCounter(string.Format("{0}_times", ItemName), GameModeConstants.MAIN_GAME, 0);
            if (rewardTimes == 0)
            {
                result = HandleReward(FirstTimeItems, "first_subscription_reward");
            }

            if (ShouldGiveRewards())
            {
                result.Union(HandleReward(Items, "subscription_reward"));
                PlayerManager.Instance.SetTimer(string.Format("{0}_timer", ItemName), TimeHelper.GetTimeInServer(),
                    GameModeConstants.MAIN_GAME);
                PlayerManager.Instance.SetCounter(string.Format("{0}_times", ItemName), 1, GameModeConstants.MAIN_GAME,
                    true);
            }

            return result;
        }

        protected virtual TransactionResultModel HandleReward(List<DropableModel> items, string origin)
        {
            if (items != null)
            {
                BundleItemModel bundle = new BundleItemModel();
                bundle.ItemName = origin;
                bundle.Items = items;
                ItemTransaction t = new ItemTransaction(bundle);
                return t.ApplyTransaction(origin, true);
            }

            return new TransactionResultModel();
        }

        public virtual bool ShouldGiveRewards()
        {
            long lastRewardTimer =
                PlayerManager.Instance.GetTimer(string.Format("{0}_timer", ItemName), GameModeConstants.MAIN_GAME, 0);
            DateTime lastRewardInDateTime = TimeHelper.FromUnixTime(lastRewardTimer);
            DateTime now = TimeHelper.GetTimeInServerDateTime();
            TimeSpan diff = now - lastRewardInDateTime;
            return lastRewardTimer == 0 || Frequency == "Daily" && diff.Days >= 1 || Frequency == "Weekly" && diff.Days >= 7 ||
                   Frequency == "Monthly" && lastRewardInDateTime.Month != now.Month;
        }
    }
}