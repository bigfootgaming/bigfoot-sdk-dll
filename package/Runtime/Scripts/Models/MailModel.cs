
using System.Collections.Generic;
using BigfootSdk.Backend;

namespace BigfootSdk
{
    public class MailModel
    {
        public int Id;
        public string Url;
        public ITransaction Reward;
        public bool Individual;
        public Dictionary<string, object> Data;
    }

    public class PlayerMailModel
    {
        public int Id;
        public long Time;
    }
}
