﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigfootSdk.Calendar
{
    /// <summary>
    /// Model used to persis Calendar progression
    /// Claimed and missed days, last claimed and other data
    /// </summary>
    [System.Serializable]
    public class CalendarProgressionModel
    {
        /// <summary>
        /// Completed Calendars
        /// </summary>
        public List<string> CompletedCalendars;
        
        /// <summary>
        /// Currenctly Assigned Calendar
        /// </summary>
        public string AssignedCalendar;

        /// <summary>
        /// Claimed Days for current Calendar
        /// </summary>
        public Dictionary<int, bool> ClaimedDays;

        /// <summary>
        /// Claimed Courses for current Calendar
        /// </summary>
        public Dictionary<int, bool> ClaimedCourses;

        /// <summary>
        /// Last Day a reward has been Claimed
        /// </summary>
        public DateTime LastClaimedDay;

        /// <summary>
        /// Starting date for this calendar
        /// </summary>
        public DateTime StartingDate;
    }
}