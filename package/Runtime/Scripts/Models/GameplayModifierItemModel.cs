﻿using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Gameplay modifier item model.
    /// </summary>
	[System.Serializable]
    public class GameplayModifierItemModel : BaseItemModel
    {
        /// <summary>
        /// List of modifiers
        /// </summary>
        [VerticalGroup("Data"), TableList(IsReadOnly = true, ShowIndexLabels = true), ShowInInspector, ShowIf("ModifiersIsNotNullOrEmpty"), PropertyOrder(2)]
        public List<GameplayModifierModel> Modifiers;

        /// <summary>
        /// Constructor to set the appropriate type.
        /// </summary>
        public GameplayModifierItemModel()
        {
            Type = "GameplayModifierItemModel";
        }

#if UNITY_EDITOR
        /// <summary>
        /// Check for the inspector to know if there is some modifiers to show
        /// </summary>
        private bool ModifiersIsNotNullOrEmpty()
        {
            return !(Modifiers == null || Modifiers.Count <= 0);
        }
#endif
    }
}
