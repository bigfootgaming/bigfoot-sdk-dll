﻿using System.Collections.Generic;

/// <summary>
/// Class to hold the model for the Tasks
/// </summary>
[System.Serializable]
public class TasksModel
{
    /// <summary>
    /// The level to unlock Tasks at
    /// </summary>
    public int UnlockLevel;

    /// <summary>
    /// The amount of tasks to hold at the same time
    /// </summary>
    public int SimultaneousTasks;
    
    /// <summary>
    /// List of all the tasks
    /// </summary>
    public List<ITaskModel> Tasks;
}
