﻿using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Class to model the InfoBubble
    /// </summary>
    public class InfoBubbleModel
    {
        /// <summary>
        /// The id for this bubble
        /// </summary>
        public string Id;

        /// <summary>
        /// The instance of this bubble
        /// </summary>
        public GameObject GameObject;
    }
}
