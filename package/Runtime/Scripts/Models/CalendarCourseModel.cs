﻿using System.Collections.Generic;
using BigfootSdk.Backend;

namespace BigfootSdk.Calendar
{
    [System.Serializable]
    public class CalendarCourseModel
    {
        /// <summary>
        /// Course ID
        /// </summary>
        public int Id;

        /// <summary>
        /// Returns Calendar Lenght
        /// </summary>
        public int CourseLenght => BaseRewards.Count;
        
        /// <summary>
        /// List of Rewards for F2P Users
        /// </summary>
        public List<ITransaction> BaseRewards;

        /// <summary>
        /// List of Rewards for VIP Users
        /// </summary>
        public List<ITransaction> VipRewards;

        /// <summary>
        /// Minimum Days claimed to earn extra reward
        /// </summary>
        public int MinClaimForBonus;

        /// <summary>
        /// Extra Reward for F2P Users
        /// </summary>
        public ITransaction CompletionBaseRewards;

        /// <summary>
        /// Extra Reward for VIP Users
        /// </summary>
        public ITransaction CompletionVipRewards;

        /// <summary>
        /// Property that returns if the calendar has completion rewards defined
        /// </summary>
        public bool HasCompletionRewards => CompletionBaseRewards != null || CompletionVipRewards != null;
    }
}