﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// World model.
	/// </summary>
	[System.Serializable]
	public class WorldModel
	{
		/// <summary>
		/// The world identifier.
		/// </summary>
		public int WorldId;

		/// <summary>
		/// The levels.
		/// </summary>
		public List<LevelModel> Levels = new List<LevelModel> ();

		/// <summary>
		/// The prerequisites.
		/// </summary>
		public List<IPrerequisite> Prereqs;
	}
}
