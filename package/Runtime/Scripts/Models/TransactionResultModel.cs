﻿using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Helpers;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Complex item transaction result. The model of the <see cref="BigfootSdk.ItemTransactionResult"/> callback.
    /// </summary>
    [System.Serializable]
    public class TransactionResultModel
    {
        /// <summary>
        /// Error message for this transaction
        /// </summary>
        public string Error = "";

        /// <summary>
        /// The currency changes of the transaction.
        /// </summary>
        public Dictionary<string, int> Currencies = new Dictionary<string, int>();

        /// <summary>
        /// The items changes of the transaction.
        /// </summary>
        public List<PlayerItemModel> Items = new List<PlayerItemModel>();

        /// <summary>
        /// Custom results of the transaction.
        /// </summary>
        public Dictionary<string, double> Resources = new Dictionary<string, double>();

        /// <summary>
        /// Custom results of the transaction.
        /// </summary>
        public Dictionary<string, object> CustomResult = new Dictionary<string, object>();

        public override string ToString()
        {
            string items = "";
            foreach (PlayerItemModel item in Items)
            {
                items += string.Format("{0} ({1})/ ", item.Name, item.AmountRemaining);
            }

            string currencies = "";
            foreach (KeyValuePair<string, int> kvp in Currencies)
            {
                currencies += string.Format("{0}:{1} / ", kvp.Key, kvp.Value);
            }
            return string.Format("Items: {0} \n Currencies: {1}", items, currencies);
        }

        public void Union(TransactionResultModel other)
        {
            if (other == null)
            {
                return;
            }

            foreach (KeyValuePair<string, int> kvp in other.Currencies)
            {
                if (Currencies.ContainsKey(kvp.Key))
                    Currencies[kvp.Key] += kvp.Value;
                else
                    Currencies.Add(kvp.Key, kvp.Value);
            }

            foreach (KeyValuePair<string, double> kvp in other.Resources)
            {
                if (Resources.ContainsKey(kvp.Key))
                    Resources[kvp.Key] += kvp.Value;
                else
                    Resources.Add(kvp.Key, kvp.Value);
            }

            var itemsManager = ItemsManager.Instance;
            foreach (PlayerItemModel item in other.Items)
            {
                if (itemsManager.GetItem(item.Name).IsStackable)
                {
                    var existingItem = Items.FirstOrDefault(i => i.Name == item.Name);
                    if (existingItem != null)
                    {
                        existingItem.AmountRemaining += item.AmountRemaining;
                    }
                    else
                    {
                        Items.Add(item);
                    }
                }
                else
                {
                    Items.Add(item);
                }
            }
            foreach (KeyValuePair<string, object> kvp in other.CustomResult)
            {
                if (CustomResult.ContainsKey(kvp.Key))
                    LogHelper.LogWarning("Transactions results have the same key in CustomResult");
                else
                    CustomResult.Add(kvp.Key, kvp.Value);
            }
        }
    }
}
