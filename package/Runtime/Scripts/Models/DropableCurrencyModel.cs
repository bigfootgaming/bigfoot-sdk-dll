﻿using Sirenix.OdinInspector;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Dropable currency model.
    /// </summary>
	[System.Serializable]
    public class DropableCurrencyModel : DropableModel
    {
        /// <summary>
        /// The currency key.
        /// </summary>
        [ReadOnly]
        public string CurrencyKey;
    }
}
