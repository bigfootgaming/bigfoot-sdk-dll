﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial model.
    /// </summary>
    [Serializable]
    public class TutorialStageModel
    {
        /// <summary>
        /// The name of the prefab that has to be instantiated.
        /// </summary>
        public string Name;

        /// <summary>
        /// If <see langword="true"/> the tutorial progress will be saved after this stage is completed.
        /// </summary>
        public bool IsMilestone = false;

        /// <summary>
        /// If <see langword="true"/> the panels manager will use an overlay behind. It is true by default.
        /// </summary>
        public bool UseOverlay = true;
        
        /// <summary>
        /// Flag if you want to override the PanelManagerId from the TutorialHandler
        /// </summary>
        public bool OverridePanelManagerId;

        /// <summary>
        /// Flag if you want to allow certains tutorial panels to close with button back. 
        /// </summary>
        public bool AcceptHardwareBack = false;

        /// <summary>
        /// The Overriden PanelManagerId
        /// </summary>
        [ShowIf("OverridePanelManagerId")]
        public int OverridenPanelManagerId;
    }
}

