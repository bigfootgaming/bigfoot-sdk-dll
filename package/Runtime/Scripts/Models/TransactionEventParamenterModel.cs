﻿using System.Collections.Generic;

namespace BigfootSdk.Analytics
{
	/// <summary>
    /// Transaction event paramenter model.
    /// </summary>
	public class TransactionEventParamenterModel
	{
		/// <summary>
		/// The key.
		/// </summary>
		public string Key;

		/// <summary>
		/// The amount.
		/// </summary>
		public double Amount;

		/// <summary>
		/// The type.
		/// </summary>
		public string Type;
	}
}
