using System;
using Sirenix.OdinInspector;

namespace BigfootSdk
{
    /// <summary>
    /// Gameplay modifier model.
    /// </summary>
    public abstract class GameplayModifierModel : ICloneable
	{
        // Auto generated id
		[ReadOnly]
        public string Id = Guid.NewGuid().ToString();

        /// <summary>
        /// The group to which this modifiers belongs to
        /// </summary>
        [ReadOnly]
		public string Group;

        /// <summary>
        /// Tag to differentiate modifiers, without any gameplay effect
        /// </summary>
        [ReadOnly]
        public string Tag;
        
        /// <summary>
        /// Method that applies the modifier
        /// </summary>
		public abstract void Apply();

        /// <summary>
        /// Method that removes the modifier
        /// </summary>
        public abstract void Undo();

		/// <summary>
		/// Get the value of this gameplay modifier
		/// </summary>
		/// <returns>return the value as an object</returns>
		public abstract object GetValue();

        /// <summary>
        /// Clone this instance.
        /// </summary>
        /// <returns>The clone.</returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}