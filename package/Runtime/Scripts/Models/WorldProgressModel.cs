﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// World progress model.
	/// </summary>
	[System.Serializable]
	public class WorldProgressModel
	{
		/// <summary>
		/// The world identifier.
		/// </summary>
		public int WorldId;

		/// <summary>
		/// The current level.
		/// </summary>
		public int CurrentLevel;

		/// <summary>
		/// The levels progress.
		/// </summary>
		public List<LevelProgressModel> LevelsProgress = new List<LevelProgressModel> ();

	}
}
