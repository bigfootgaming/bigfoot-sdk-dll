﻿using Sirenix.OdinInspector;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Dropable item model.
    /// </summary>
	[System.Serializable]
    public class DropableItemModel : DropableModel
    {
        /// <summary>
        /// The name of the item.
        /// </summary>
        [ReadOnly]
        public string ItemName;
    }
}
