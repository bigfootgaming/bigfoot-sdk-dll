﻿using System;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial character animation model.
    /// </summary>
    [Serializable]
    public class TutorialCharacterAnimationModel
    {
        /// <summary>
        /// The transition animation name.
        /// </summary>
        public string TransitionAnimation;

        /// <summary>
        /// The looped animation name.
        /// </summary>
        public string LoopedAnimation;
    }
}

