﻿using System;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk.Analytics
{
    /// <summary>
    /// Database event model.
    /// </summary>
    [Serializable]
    public class DatabaseEventModel
    {
        /// <summary>
        /// The event identifier.
        /// </summary>
        public string EventId;

        /// <summary>
        /// The session identifier.
        /// </summary>
        public string SessionId;

        /// <summary>
        /// The user identifier.
        /// </summary>
        public string UserId;

        /// <summary>
        /// The event date time.
        /// </summary>
        public string EventDateTime;

        /// <summary>
        /// The name of the event.
        /// </summary>
        public string EventName;

        /// <summary>
        /// The event data.
        /// </summary>
        public string EventData;

        /// <summary>
        /// The event local date time.
        /// </summary>
        public string EventLocalDateTime;

        /// <summary>
        /// The game version.
        /// </summary>
        public string GameVersion;

        /// <summary>
        /// The country code (AR, US, ES)
        /// </summary>
        public string CountryCode;

        /// <summary>
        /// The operating system
        /// </summary>
        public string OperatingSystem;

        /// <summary>
        /// If we are on an a/b test, which variant we are on
        /// </summary>
        public string VariantId;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.Analytics.DatabaseEventModel"/> class.
        /// </summary>
        /// <param name="sessionId">Session identifier.</param>
        /// <param name="userId">User identifier.</param>
        /// <param name="eventName">Event name.</param>
        /// <param name="eventString">Event string.</param>
        public DatabaseEventModel(string sessionId, string userId, string eventName, string eventString, string variantId)
        {
            EventId = Guid.NewGuid().ToString();
            SessionId = sessionId;
            UserId = userId;
            DateTime dateTime = TimeHelper.GetTimeInServerDateTime();
            EventDateTime = dateTime.ToString("yyyy-MM-dd HH:mm:ss");
            EventName = eventName;
            EventData = eventString;
            EventLocalDateTime = TimeHelper.ToLocal(dateTime).ToString("yyyy-MM-dd HH:mm:ss");
            GameVersion = Application.version;
            CountryCode = System.Globalization.RegionInfo.CurrentRegion.ToString();
            OperatingSystem = Application.platform.ToString();
            VariantId = variantId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.Analytics.DatabaseEventModel"/> class.
        /// </summary>
        /// <param name="eventId">Event identifier.</param>
        /// <param name="sessionId">Session identifier.</param>
        /// <param name="userId">User identifier.</param>
        /// <param name="eventTime">Event time.</param>
        /// <param name="eventName">Event name.</param>
        /// <param name="eventString">Event string.</param>
        /// <param name="eventLocalTime">Event local time.</param>
        public DatabaseEventModel(string eventId, string sessionId, string userId, string eventTime, string eventName, string eventString, string eventLocalTime, string variantId)
        {
            EventId = eventId;
            SessionId = sessionId;
            UserId = userId;
            EventDateTime = eventTime;
            EventName = eventName;
            EventData = eventString;
            EventLocalDateTime = eventLocalTime;
            GameVersion = Application.version;
            CountryCode = System.Globalization.RegionInfo.CurrentRegion.ToString();
            OperatingSystem = Application.platform.ToString();
            VariantId = variantId;
        }
    }
}