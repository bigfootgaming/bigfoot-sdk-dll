﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Limited time event
	/// </summary>
	[System.Serializable]
	public class LimitedTimeEventModel
	{
		/// <summary>
		/// Number that needs to be incremented every event (needed to cycle)
		/// </summary>
		public int Number;

		/// <summary>
		/// The name of the event
		/// </summary>
		public string Name;
		
		/// <summary>
		/// Identifier
		/// </summary>
		public string Id => string.Format("{0}_{1}", Number, Name);
		
		/// <summary>
		/// The prerequisites for this event
		/// </summary>
		public List<IPrerequisite> Prerequisites;

		/// <summary>
		/// The different configuration mappings
		/// </summary>
		public Dictionary<string, string> Configurations;
		
		/// <summary>
		/// The custom data.
		/// </summary>
		public Dictionary<string, object> CustomData;
	}
}
