﻿using BigfootSdk.Backend;
namespace BigfootSdk.Shop
{
    /// <summary>
    /// Iap validation result model.
    /// </summary>
    [System.Serializable]
    public class IapValidationResultModel
    {
        /// <summary>
        /// The iap item identifier.
        /// </summary>
        public string ItemId;

        /// <summary>
        /// The error message.
        /// </summary>
        public string Error;

        /// <summary>
        /// The iap transaction result.
        /// </summary>
        public TransactionResultModel TransactionResult;
    }
}