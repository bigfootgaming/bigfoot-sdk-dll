﻿using System;

namespace BigfootSdk.Analytics
{
    /// <summary>
    /// Kinesis data model.
    /// </summary>
    [Serializable]
    public class KinesisDataModel
    {
        /// <summary>
        /// The name of the kinesis delivery stream.
        /// </summary>
        public string DeliveryStreamName;

        /// <summary>
        /// The records.
        /// </summary>
        public SerializedEventModel[] Records;
    }

    /// <summary>
    /// Serialized event model.
    /// </summary>
    [Serializable]
    public class SerializedEventModel {
        /// <summary>
        /// The stringyfied data.
        /// </summary>
        public string Data;
    }
}

