﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace BigfootSdk.Panels
{
    /// <summary>
    /// Panel model used by the PanelSystemManager.
    /// </summary>
    [System.Serializable]
    public class PanelModel
    {
        /// <summary>
        /// The config.
        /// </summary>
        public PanelConfigModel Config;
        /// <summary>
        /// The panel gameObject.
        /// </summary>
        public PanelGameObject PanelGO;
		
        /// <summary>
        /// Initializes a new instance of the <see cref="T:BigfootSdk.PanelModel"/> class.
        /// </summary>
        /// <param name="config">Config.</param>
        /// <param name="panelGO">Panel gameObject.</param>
        public PanelModel(PanelConfigModel config, PanelGameObject panelGO)
        {
            Config = config;
            PanelGO = panelGO;
        }
    }
}