﻿using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Player Item model.
    /// </summary>
    public class PlayerItemModel
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public ObscuredString Id { get; set; }

        /// <summary>
        /// The item name. Reference to BaseItemModel.
        /// </summary>
        public ObscuredString Name { get; set; }

        /// <summary>
        /// The type.
        /// </summary>
        public ObscuredString Type { get; set; }
        
        /// <summary>
        /// The class of this item
        /// </summary>
        public ObscuredString Class { get; set; }
        
        /// <summary>
        /// The amount remaining.
        /// </summary>
        public ObscuredInt AmountRemaining { get; set; }

        /// <summary>
        /// The item instance custom data.
        /// </summary>
        public Dictionary<string, object> Data { get; set; } = new Dictionary<string, object>();

        public override string ToString()
        {
            return string.Format("ItemName: {0}  Id: {1}", Name, Id);
        }
    }
}