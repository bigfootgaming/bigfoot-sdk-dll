﻿// Based on https://www.owasp.org/index.php/Certificate_and_Public_Key_Pinning#.Net

using System.Security.Cryptography.X509Certificates;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;
using UnityEngine.Networking;

class BigfootCertificateModel : CertificateHandler
{
    // Encoded RSAPublicKey
    private static ObscuredString PUB_KEY = "3082010A02820101008AF9E35D9BA56E0B33CFEE3C64CF9FFEE6505C0A97F7119C3CB5109423397CC210FDD2310B312228D2CEFF07B87B910C0CD456ADB0FBE3AEBDFCF7B463043EF3A821ED9A1222ADC3E2BD0CF5FC83A6CFE543FD36F6B38F8284683DBC3D72676F6B067940835CAC4282F6D0A4E7E37D66435172C5AA8858969BE74DE0CBECD564E58EFCC60B9736104DC832A36B6E40638A900F9BED4AE934E167DA0C478FC787BB7A6EF7C6A5C101B57C5D041849D8FC89A70FBDCB581AA99FBB0D87F3F9EDB39DD818FF14886E7EF84217D9ACF2A5F50714AFC93276E863F42D4D4E2AA2FC1B0B14C2ED3DEB88DA1059F12962472A662D2E3EB445D0655523D63416338BD5170203010001";

    protected override bool ValidateCertificate(byte[] certificateData)
    {
        X509Certificate2 certificate = new X509Certificate2(certificateData);
        string pk = certificate.GetPublicKeyString();
        if (pk.Equals(PUB_KEY))
            return true;
        
        return false;
    }
}