﻿using System;

[Serializable]
public class PlayerLeagueModel
{
    public string LeagueId;

    public int LastPosition = -1;

    public int LeagueTier;

    public int LastLeagueTier;
    
    
}
