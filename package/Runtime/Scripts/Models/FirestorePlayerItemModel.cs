﻿#if BACKEND_FIREBASE_FIRESTORE
using System;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Player Item model.
    /// </summary>
    [FirestoreData]
    [Serializable]
    public class FirestorePlayerItemModel 
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredStringConverter))]
        public ObscuredString Id { get; set; }

        /// <summary>
        /// The item name. Reference to BaseItemModel.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredStringConverter))]
        public ObscuredString Name { get; set; }

        /// <summary>
        /// The type.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredStringConverter))]
        public ObscuredString Type { get; set; }
        
        /// <summary>
        /// The class of this item
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredStringConverter))]
        public ObscuredString Class { get; set; }
        
        /// <summary>
        /// The amount remaining.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredIntConverter))]
        public ObscuredInt AmountRemaining { get; set; }

        /// <summary>
        /// The item instance custom data.
        /// </summary>
        [FirestoreProperty]
        public Dictionary<string, object> Data { get; set; } = new Dictionary<string, object>();
    } 
}

#endif