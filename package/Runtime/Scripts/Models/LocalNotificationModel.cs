﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Local Notification model.
	/// </summary>
	[System.Serializable]
    public class LocalNotificationModel
	{
		/// <summary>
		/// The id for this local notification
		/// </summary>
		public string Id;

		/// <summary>
		/// When should we send this notification?
		/// </summary>
		public string SendType;
		
		/// <summary>
		/// How many seconds after NOW should we send it (some notification may override this)
		/// </summary>
		public double Offset;
		
		/// <summary>
		/// The small icon
		/// </summary>
		public string SmallIcon;

		/// <summary>
		/// The large icon
		/// </summary>
		public string LargeIcon;

		public string BigPicture;

		public string BigPicturePath;

	}
}
