using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace BigfootSdk.Backend
{
    [System.Serializable]
    public class GachaItemModel : BaseItemModel
    {
        public int TotalValue;
        
        [VerticalGroup("Data")]
        [TableList(IsReadOnly = true, ShowIndexLabels = true), ShowInInspector, ShowIf("ItemsIsNotNullOrEmpty"), PropertyOrder(2)]
        public List<GachableItemModel> Items;
        
        
        public GachaItemModel()
        {
            Type = "Gacha";
        }
        
#if UNITY_EDITOR
        /// <summary>
        /// Check for the inspector to know if there is some items to show
        /// </summary>
        private bool ItemsIsNotNullOrEmpty()
        {
            return !(Items == null || Items.Count <= 0);
        }
#endif

    }
}