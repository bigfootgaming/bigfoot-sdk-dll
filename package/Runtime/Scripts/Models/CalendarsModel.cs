﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Model that contains all the game calendars
    /// </summary>
    [System.Serializable]
    public class CalendarsModel
    {
        /// <summary>
        /// List of the calendars
        /// </summary>
        public List<CalendarModel> Calendars;

        /// <summary>
        /// Feature Unlock prerequisites
        /// </summary>
        public List<IPrerequisite> UnlockPrerequisites;
        
        /// <summary>
        /// Should Missed days be accounted 
        /// </summary>
        public bool CanMissDays;

        /// <summary>
        /// Should the Calendar send Notifications
        /// </summary>
        public bool SendLocalNotifications;
        
        /// <summary>
        /// The custom data.
        /// </summary>
        public Dictionary<string, string> CustomData;
    }
}