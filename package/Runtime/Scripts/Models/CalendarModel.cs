﻿using System.Collections.Generic;


namespace BigfootSdk.Backend
{
    /// <summary>
    /// Model that cointains each calendar
    /// </summary>
    [System.Serializable]
    public class CalendarModel
    {
        /// <summary>
        /// Calendar Name
        /// </summary>
        public string CalendarName;

        /// <summary>
        /// Calendar Prerequisites
        /// </summary>
        public List<IPrerequisite> Prerequisites;
            
        /// <summary>
        /// Courses for the calendar
        /// </summary>
        public Dictionary<string, object> CalendarCourses;
        
    }
}