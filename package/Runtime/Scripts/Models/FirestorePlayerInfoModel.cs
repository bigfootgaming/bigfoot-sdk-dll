﻿
using System;
#if BACKEND_FIREBASE_FIRESTORE
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Player Info model.
    /// </summary>
    [FirestoreData]
    [Serializable]
    public class FirestorePlayerInfoModel 
    {
        /// <summary>
        /// The player data.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredStringDictionaryConverter))]
        public Dictionary<string, ObscuredString> Data { get; set; } = new Dictionary<string, ObscuredString>();

        /// <summary>
        /// The currencies.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredIntDictionaryConverter))]
        public Dictionary<string, ObscuredInt> Currencies { get; set; } = new Dictionary<string, ObscuredInt>();

        /// <summary>
        /// The currencies.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredIntDictionaryConverter))]
        public Dictionary<string, ObscuredInt> Counters { get; set; } = new Dictionary<string, ObscuredInt>();
		
        /// <summary>
        /// The currencies.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredBoolDictionaryConverter))]
        public Dictionary<string, ObscuredBool> Flags { get; set; } = new Dictionary<string, ObscuredBool>();
		
        /// <summary>
        /// The currencies.
        /// </summary>
        [FirestoreProperty(ConverterType = typeof(ObscuredLongDictionaryConverter))]
        public Dictionary<string, ObscuredLong> Timers { get; set; } = new Dictionary<string, ObscuredLong>();
		
        /// <summary>
        /// The items.
        /// </summary>
        [FirestoreProperty]
        public List<FirestorePlayerItemModel> Items { get; set; } = new List<FirestorePlayerItemModel>();
    }
}


#endif