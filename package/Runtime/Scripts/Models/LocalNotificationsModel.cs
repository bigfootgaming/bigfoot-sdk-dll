﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Local Notifications model.
	/// </summary>
	[System.Serializable]
    public class LocalNotificationsModel
	{
		/// <summary>
		/// List containing all of the local notifications for this game
		/// </summary>
		public List<LocalNotificationModel> Notifications;

		/// <summary>
		/// Flag to cancel all local notifications on start
		/// </summary>
		public bool CancelAllOnStart = true;
	}
}
