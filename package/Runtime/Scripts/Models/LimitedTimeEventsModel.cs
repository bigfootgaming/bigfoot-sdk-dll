﻿using System.Collections.Generic;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Limited time event
	/// </summary>
	[System.Serializable]
	public class LimitedTimeEventsModel
	{
		/// <summary>
		/// The event list
		/// </summary>
		public List<LimitedTimeEventModel> Events;
		
		/// <summary>
		/// The prerequisites to unlock the limited time events
		/// </summary>
		public List<IPrerequisite> UnlockPrerequisites;

		/// <summary>
		/// The custom data.
		/// </summary>
		public Dictionary<string, string> CustomData;

		/// <summary>
		/// How many events prior to the current one should we keep
		/// </summary>
		public int EventDifferenceToKeep = 3;
	}
}
