﻿
using System;
using BigfootSdk.Helpers;
using BigfootSdk.Panels;
using UnityEngine;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Manager in charge of of the Rate Us
	/// </summary>
	public class RateUsManager : LoadableSingleton<RateUsManager>
	{
		/// <summary>
		/// PlayerManager Instance
		/// </summary>
		protected PlayerManager _playerManager;

		/// <summary>
		/// The rate us model
		/// </summary>
		protected RateUsModel _rateUsModel;
		public RateUsModel RateUsModel => _rateUsModel;

		/// <summary>
		/// The current rate status
		/// </summary>
		protected int _rateStatus;

		/// <summary>
		/// Do we need to rate?
		/// </summary>
		public bool NeedToRate => _rateStatus == RateUsConstants.NOT_ASKET_YET;

		/// <summary>
		/// Initializes rate us manager
		/// </summary>
		public override void StartLoading ()
		{
			// Cache the PlayerManager
			_playerManager = PlayerManager.Instance;

			// Fetch the status from the Counters
			_rateStatus = _playerManager.GetCounter(CountersConstants.RATE_US, GameModeConstants.MAIN_GAME, RateUsConstants.NOT_ASKET_YET);

			// If we need to ask the user in the future
			if (_rateStatus == RateUsConstants.NOT_ASKET_YET)
			{
				// Grab the config
				_rateUsModel = ConfigHelper.LoadConfig<RateUsModel>("rate_us");
			}
			
			FinishedLoading(true);
		}

		/// <summary>
		/// Update the saved value for the Rate Us
		/// </summary>
		/// <param name="newRateStatus">New value to save</param>
		public virtual void SetRateStatus(int newRateStatus)
		{
			// Update the status
			_rateStatus = newRateStatus;
			
			// Update the counter
			_playerManager.SetCounter(CountersConstants.RATE_US, newRateStatus, GameModeConstants.MAIN_GAME);
		}

		public virtual void ShowRateUsPanel()
		{
			PanelsEvents.ShowPanel("RateUsDialog", PanelBehavior.KeepActive, panel =>
			{
				var rateUsDialog = panel.GetComponent<RateUsDialog>();
				if (rateUsDialog != null)
				{
					rateUsDialog.InitializeRateUs();
				}
			});	
		}
	}
}
