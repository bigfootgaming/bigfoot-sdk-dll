﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asyncoroutine;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;
using UnityEngine;
using BigfootSdk.SceneManagement;
using BigfootSdk.Tutorials;
using UnityEngine.UI;

namespace BigfootSdk.Panels
{
    /// <summary>
    /// Panel system manager.
    /// </summary>
    public class PanelsManager : MonoBehaviour
    {
        /// <summary>
        /// Event for when the user attemp to exit the game
        /// </summary>
        public static Action OnAttemptToExit;
        
        /// <summary>
        /// The panel system identifier.
        /// </summary>
        public int ID;
        
        /// <summary>
        /// If <see langword="true"/> the panel system will use an overlay behind the panel that need it.
        /// </summary>
        public bool UseOverlay;
        
        /// <summary>
        /// The overlay color alpha.
        /// </summary>
        public float OverlayAlpha = 0.5f;
        
        /// <summary>
        /// If <see langword="true"/> the overley will be shown and hidden with fade animations.
        /// </summary>
        public bool OverlayWithAnimation;

        /// <summary>
        /// The panels configurations.
        /// </summary>
        public List<PanelConfigModel> Panels;

        /// <summary>
        /// The Pool.
        /// </summary>
        public ObjectPool UIPool;

        /// <summary>
        /// If <see langword="true"/> an attempt to exit event will be triggered if there are no panels in the stack.
        /// </summary>
        [Tooltip("If true, PopUp panel with exit description will be shown if there are no panels in the stack, and will exit on Yes")]
        public bool UseExitPanel = true;
        
        /// <summary>
        /// If <see langword="true"/> the back button will trigger back coroutine.
        /// </summary>
        public bool UseBackButton = true;

        public bool UseBackDuringTutorial = true;
        
        /// <summary>
        /// List of panels already instantiated
        /// </summary>
        protected List<PanelModel> _panels;
        
        /// <summary>
        /// List of panels active at the moment
        /// </summary>
        protected List<PanelModel> _panelStack;
        
        /// <summary>
        /// List of panels queued to be shown
        /// </summary>
        protected List<QueuedPanel> _panelsQueued;
        
        /// <summary>
        /// Current panel being hide
        /// </summary>
        protected PanelGameObject _panelHiding;
        
        /// <summary>
        /// The overlay.
        /// </summary>
        protected Overlay _overlay;

        /// <summary>
        /// Flag to know if we are waiting for a queued panel to load
        /// </summary>
        protected bool _isQueueAsyncLoading;
        
        /// <summary>
        /// Awake this instance. Create structures. Suscribe events that take action on this object.
        /// </summary>
        protected virtual void Awake()
        {
            _panels = new List<PanelModel>();
            _panelStack = new List<PanelModel>();
            _panelsQueued = new List<QueuedPanel>();
            
            PanelsEvents.OnShowPanelSystemManager += ShowPanelController;
            PanelsEvents.OnHidePanelSystemManager += HidePanelController;
        }

        /// <summary>
        /// Desuscribe events that take action on this object.
        /// </summary>
        protected virtual void OnDestroy()
        {
            PanelsEvents.OnShowPanelSystemManager -= ShowPanelController;
            PanelsEvents.OnHidePanelSystemManager -= HidePanelController;
        }

        /// <summary>
        /// Hides the panel controller.
        /// </summary>
        /// <param name="pmId">Panels manager identifier.</param>
        protected virtual void HidePanelController(int pmId)
        {
            if (ID == pmId)
                gameObject.SetActive(false);
        }

        /// <summary>
        /// Shows the panel controller.
        /// </summary>
        /// <param name="pmId">Panels manager identifier.</param>
        protected virtual void ShowPanelController(int pmId)
        {
            if (ID == pmId)
                gameObject.SetActive(true);
        }

        /// <summary>
        /// Start this instance.
        /// </summary>
        protected virtual void Start()
        {
            if (UseOverlay)
            {
                CreateOverlay();
            }
        }

        /// <summary>
        /// Creates the overlay.
        /// </summary>
        protected virtual void CreateOverlay ()
        {
            // Create the overlay
            var overlayGO = new GameObject();
            overlayGO.name = "OVERLAY";
            overlayGO.transform.SetParent(transform);
            overlayGO.transform.localScale = Vector2.one;
            overlayGO.transform.localEulerAngles = Vector3.zero;
            //set UI layer
            overlayGO.layer = 5;

            // Add overlay component
            _overlay = overlayGO.AddComponent<Overlay>();
            _overlay.Setup(OverlayAlpha, OverlayWithAnimation);
        }

        /// <summary>
        /// Shows the overlay
        /// </summary>
        /// <param name="panel">Panel.</param>
        protected virtual void OverlayIn(PanelGameObject panel)
        {
            if (panel != null && UseOverlay && _overlay != null)
            {
                // Enable the overlay
                _overlay.OverlayIn(panel.InDuration);
                // Set as last child
                _overlay.transform.SetAsLastSibling();
            }
        }

        /// <summary>
        /// Move the overlay behind the previous panel or hides the overlay if necessary.
        /// </summary>
        /// <param name="panel">Panel.</param>
        protected virtual void OverlayOut(PanelGameObject panel)
        {
            if (UseOverlay && _overlay != null)
            {
                // Start tween to hide the overlay, when completed deactivate the gameobject
                int activesStacked = OverlaysActive();
                if (activesStacked <= 1)
                {
                    _overlay.OverlayOut(panel.OutDuration);
                }
                else
                {
                    // Apply new index n Check index does not go out of range
                    int newSiblingIndex = Math.Max(0, _overlay.transform.GetSiblingIndex() - 1);
                    _overlay.transform.SetSiblingIndex(newSiblingIndex);
                }
            }
        }

        /// <summary>
        /// Returns the amount of panels with overlay.
        /// </summary>
        /// <returns>amount of panels with overlay.</returns>
        protected virtual int OverlaysActive()
        {
            int activeOverlays = 0;
            for (int i = 0; i < _panelStack.Count; i++)
            {
                if (_panelStack[i].PanelGO != null && _panelStack[i].Config.UseOverlay)
                {
                    activeOverlays++;
                }
            }
            return activeOverlays;
        }

        /// <summary>
        /// Subscribe panel events.
        /// </summary>
        protected virtual void OnEnable()
        {
            PanelsEvents.OnShowPanel += ShowOrInstantiate;
            PanelsEvents.OnShowNewPanel += ShowNewPanel;
            PanelsEvents.OnBack += Back;
            PanelsEvents.OnShowOverlay += ShowOverlay;
            PanelsEvents.OnHideOverlay += HideOverlay;
            PanelsEvents.OnClearStack += ClearStack;
        }

        /// <summary>
        /// Unsubscribe panel events
        /// </summary>
        protected virtual void OnDisable()
        {
            PanelsEvents.OnShowPanel -= ShowOrInstantiate;
            PanelsEvents.OnShowNewPanel -= ShowNewPanel;
            PanelsEvents.OnBack -= Back;
            PanelsEvents.OnShowOverlay -= ShowOverlay;
            PanelsEvents.OnHideOverlay -= HideOverlay;
            PanelsEvents.OnClearStack -= ClearStack;
        }

        /// <summary>
        /// Get the active panel in this panels manager.
        /// </summary>
        /// <returns></returns>
        public virtual PanelModel GetActivePanel()
        {
            if (_panelStack != null && _panelStack.Count > 0)
                return _panelStack.Last();
            return null;
        }

        protected virtual void ShowOverlay(int pmId, float duration)
        {
            if (pmId == ID)
                _overlay.OverlayIn(duration);
        }
        
        protected virtual void HideOverlay(int pmId, float duration)
        {
            if (pmId == ID)
                _overlay.OverlayOut(duration);
        }

        /// <summary>
        /// Checks if is the same panel
        /// </summary>
        /// <returns><c>true</c>, if the last panel in stack is <paramref name="panelName"/>, <c>false</c> otherwise.</returns>
        /// <param name="panelName">Panel name.</param>
        protected virtual bool IsSamePanel(string panelName)
        {
            if (_panelStack != null)
            {
                if (_panelStack.Count > 0 && _panelStack.Last() != null)
                {
                    if (_panelStack.Last().Config.PanelName == panelName)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Shows the new panel.
        /// </summary>
        /// <param name="panel">Panel.</param>
        /// <param name="previousPanelBehavior">Indicates how to proceed with the previous panel.</param>
        /// <param name="preAction">Pre action.</param>
        /// <param name="postAction">post action.</param>
        /// <param name="pmId">Panels manager identifier.</param>
        protected virtual void ShowNewPanel(PanelConfigModel panel, PanelBehavior previousPanelBehavior, Action<PanelGameObject> preAction, Action<PanelGameObject> postAction, int pmId)
        {
            if (ID.Equals(pmId))
            {
                var p = Panels.FirstOrDefault(x => x.PanelName.Equals(panel.PanelName));
                if (p == null)
                {
                    p = panel;
                    Panels.Add(panel);
                }

                ShowOrInstantiate(p.PanelName, previousPanelBehavior, preAction, postAction, pmId);
            }
        }


        /// <summary>
        /// Instantiates the <paramref name="panelName"/> if needed. Then Shows it up.
        /// </summary>
        /// <param name="panelName">Panel name.</param>
        /// <param name="previousPanelBehavior">Indicates how to proceed with the previous panel.</param>
        /// <param name="preAction">Pre action.</param>
        /// <param name="postAction">post action.</param>
        /// <param name="pmId">Panels manager identifier.</param>
        protected virtual void ShowOrInstantiate(string panelName, PanelBehavior previousPanelBehavior, Action<PanelGameObject> preAction, Action<PanelGameObject> postAction, int pmId)
        {
            if ((pmId == ID) && !IsSamePanel(panelName))
            {
                ShowOrInstantiate(panelName, pmId, previousPanelBehavior, preAction, postAction);
            }
        }

        /// <summary>
        /// Instantiates the <paramref name="panelName"/> if needed. Then Shows it up.
        /// </summary>
        /// <param name="panelName">Panel name.</param>
        /// <param name="pmId">Panels manager identifier.</param>
        /// <param name="previousPanelBehavior">Indicates how to proceed with the previous panel.</param>
        /// <param name="preAction">Pre action.</param>
        /// <param name="postAction">post action.</param>
        protected virtual async Task ShowOrInstantiate(string panelName, int pmId, PanelBehavior previousPanelBehavior, Action<PanelGameObject> preAction, Action<PanelGameObject> postAction)
        { 
            PanelModel panelToShow = _panels.FirstOrDefault(p => p.Config.PanelName == panelName);
            if (panelToShow == null)
            {
                // If we are trying to queue a panel
                if(previousPanelBehavior == PanelBehavior.Queue)
                {
                    // If we have something showing, or something loaded, add it to be shown later
                    if (_panelStack.Count > 0 || _isQueueAsyncLoading)
                    {
                        _panelsQueued.Add(new QueuedPanel{PanelName = panelName, PreAction = preAction, PostAction = postAction});
                        return;
                    }
                    
                    // If it's the first panel, set the flag that we are loading a queued panel async
                    // If 2 queued panels are loaded back to back, this prevents it from breaking due to the async in the pool
                    if(_panelStack.Count == 0)
                    {
                        _isQueueAsyncLoading = true;
                    }
                }
                
                PanelConfigModel panel = Panels.FirstOrDefault(p => p.PanelName == panelName);

                LogHelper.LogSdk("PanelSystemManager.cs :: calling : " + panelName, LogHelper.PANELS_MANAGER_TAG);

                GameObject instance = await UIPool.GetObjectForTypeAsync(panel.PanelName, transform);
                
                if (instance != null)
                {
                    instance.transform.localScale = Vector3.one;

                    instance.transform.localPosition = panel.Position;

                    panelToShow = new PanelModel(panel, instance.GetComponent<PanelGameObject>());

                    SeeSizeConstraints(instance, panelToShow);
                    
                    _panels.Add(panelToShow);
                }
            }

            //Hide previous panels if needed
            if (_panelStack.Count > 0 && _panelStack.Last() != null)
            {
                //By default, we'll hide the previous panel. 
                if (previousPanelBehavior == PanelBehavior.RemoveFromStack 
                    || previousPanelBehavior == PanelBehavior.KeepInStack 
                    || previousPanelBehavior == PanelBehavior.ClearStack)
                {
                    PanelModel p = _panelStack.Last();
                    //check if we have to remove it from stack
                    if (previousPanelBehavior == PanelBehavior.RemoveFromStack)
                    {
                        _panelStack.RemoveAt(_panelStack.Count - 1);
                    }
                    else if (previousPanelBehavior == PanelBehavior.ClearStack)
                    {
                        //if some previous panels are still active we have to hide them
                        foreach (var pm in _panelStack)
                        {
                            if (pm != p)
                            {
                                if (pm.Config.WaitForPanelAnimations)
                                    await Hide(pm, pmId, null);
                                else
                                    Hide(pm, pmId, null);
                            }
                        }
                        _panelStack.Clear();
                    }
                    if (p.Config.WaitForPanelAnimations)
                        await Hide(p, pmId, panelToShow);
                    else
                        Hide(p, pmId, panelToShow);
                }
            }


            if (panelToShow != null)
            {
                if (_panelStack.Count == 0 || _panelStack.Last() != panelToShow || (_panelStack.Last() == panelToShow && !panelToShow.PanelGO.gameObject.activeInHierarchy))
                {
                    try
                    {
                        preAction?.Invoke(panelToShow.PanelGO);
                        await Show(panelToShow, pmId);
                        if (_panelStack.Count == 0 || _panelStack.Last() != panelToShow)
                        {
                            _panelStack.Add(panelToShow);
                        
                            // Reset the async loading flag
                            if (previousPanelBehavior == PanelBehavior.Queue)
                            {
                                _isQueueAsyncLoading = false;
                            }
                        }

                        postAction?.Invoke(panelToShow.PanelGO);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                        throw;
                    }
                    
                }
            }
            else
            {
                string errorMessage = string.Format("PanelsManager.cs :: The panel you want to instanciate ({0}) couldn't be instantiated or doesn't exist", panelName);
                LogHelper.LogError(errorMessage, Environment.StackTrace);
            }
        }


        protected virtual async void ClearStack(int pmId)
        {
            if (pmId == ID)
            {
                foreach (var pm in _panelStack)
                {
                    if (gameObject != null && pm.PanelGO != null)
                    {
                        if (pm.Config.WaitForPanelAnimations)
                            await Hide(pm, pmId, null);
                        else
                            Hide(pm, pmId, null);
                    }
                }
                _panelStack.Clear();
            }
        }

        protected virtual void SeeSizeConstraints(GameObject instance, PanelModel panelToShow)
        {
            // respect max width
            if (panelToShow != null && panelToShow.PanelGO != null && !Mathf.Approximately(panelToShow.PanelGO.MaxWidth, 0.0f))
            {
                RectTransform rt = instance.GetComponent<RectTransform>();
                if (rt != null && rt.rect.width > panelToShow.PanelGO.MaxWidth)
                {
                    LayoutElement layoutElement = instance.GetComponent<LayoutElement>();
                    if(layoutElement == null)
                    {
                        layoutElement = instance.AddComponent<LayoutElement>();
                    }
                    layoutElement.preferredWidth = panelToShow.PanelGO.MaxWidth;

                    ContentSizeFitter sizeFitter = instance.GetComponent<ContentSizeFitter>();
                    if (sizeFitter == null)
                    {
                        sizeFitter = instance.AddComponent<ContentSizeFitter>();
                    }
                    sizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
                }
            }

            // respect max height
            if (!Mathf.Approximately(panelToShow.PanelGO.MaxHeight, 0.0f))
            {
                RectTransform rt = instance.GetComponent<RectTransform>();
                if (rt != null && rt.rect.height > panelToShow.PanelGO.MaxHeight)
                {
                    LayoutElement layoutElement = instance.GetComponent<LayoutElement>();
                    if (layoutElement == null)
                    {
                        layoutElement = instance.AddComponent<LayoutElement>();
                    }
                    layoutElement.preferredHeight = panelToShow.PanelGO.MaxHeight;

                    ContentSizeFitter sizeFitter = instance.GetComponent<ContentSizeFitter>();
                    if (sizeFitter == null)
                    {
                        sizeFitter = instance.AddComponent<ContentSizeFitter>();
                    }
                    sizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
                }
            }
        }

        /// <summary>
        /// Hides the specified <paramref name="panel"/>.
        /// </summary>
        /// <param name="panel">Panel.</param>
        /// <param name="pmId">Panels manager identifier.</param>
        /// <param name="nextPanel">Panel to show after hide the current panel.</param>
        protected virtual async Task Hide(PanelModel panel, int pmId, PanelModel nextPanel = null)
        {
            try
            {
                if (panel.PanelGO != null)
                {
                    _panelHiding = panel.PanelGO;

                    PanelsEvents.PanelStartHiding(panel, pmId);

                    // Play Hide animation
                    float animDuration = _panelHiding.StartHiding();
                    if (panel.Config.UseOverlay && (nextPanel == null || !nextPanel.Config.UseOverlay))
                        OverlayOut(_panelHiding);

                    // Send analytics ui interaction event
                    if(panel.Config.SendAnalyticsEvent)
                        AnalyticsManager.Instance.TrackUIInteractionEvent("Hide", BigfootSceneManager.GetActiveScene().name, panel.Config.PanelName, "Panel");

                    // Then if needed, wait for the hide anim to complete
                    if (panel.Config.WaitForPanelAnimations)
                        await Task.Delay((int)(animDuration * 1000));

                    if (panel.PanelGO != null)
                    {
                        // And finally hide the panel
                        panel.PanelGO.CompleteHiding();
                        panel.PanelGO.gameObject.SetActive(false);
                        PanelsEvents.PanelHidden(panel, pmId);
                    }

                    _panelHiding = null;
                }
                else
                {
                    string errorMessage = string.Format("PanelsManager.cs :: PanelGameObject is missing in panel: {0}", panel.Config.PanelName);
                    LogHelper.LogError(errorMessage, Environment.StackTrace);
                }
            }
            catch (Exception e)
            {
                LogHelper.LogWarning($"PanelsManager.cs :: caught exception during Hide for {panel.Config.PanelName}");
                throw;
            }
            
        }

        /// <summary>
        /// Show the specified panel.
        /// </summary>
        /// <param name="panel">Panel.</param>
        /// <param name="pmId">Panels manager identifier.</param>
        protected virtual async Task Show(PanelModel panel, int pmId, bool playAnimation = true)
        {
            if (panel.PanelGO != null)
            {
                if (panel.Config.UseOverlay)
                    OverlayIn(panel.PanelGO);

                PanelsEvents.PanelStartShowing(panel, pmId);

                panel.PanelGO.transform.SetAsLastSibling();

                panel.PanelGO.gameObject.SetActive(true);

                if (playAnimation)
                {
                    float animDuration = panel.PanelGO.StartShowing();
                    if (panel.Config.WaitForPanelAnimations)
                        await Task.Delay((int)(animDuration * 1000));
                }
                else
                {
                    panel.PanelGO.OnPanelStartedShowing?.Invoke();
                }

                // Send analytics ui interaction event
                if (panel.Config.SendAnalyticsEvent)
                    AnalyticsManager.Instance.TrackUIInteractionEvent("Show", BigfootSceneManager.GetActiveScene().name, panel.Config.PanelName, "Panel");

                panel.PanelGO.CompleteShowing();

                PanelsEvents.PanelShown(panel, pmId);
            }
            else
            {
                string errorMessage = string.Format("PanelsManager.cs :: PanelGameObject is missing in panel: {0}", panel.Config.PanelName);
                LogHelper.LogError(errorMessage, Environment.StackTrace);
            }
        }

        /// <summary>
        /// Update. Called once per frame. Checks BackButton usage.
        /// </summary>
        protected virtual void Update()
        {
            if (ID == 0)
            {
                //Back functionallity
                if (UseBackButton && _panelStack != null && _panelStack.Count > 0)
                {
                    if (!UseBackDuringTutorial && TutorialsManager.Instance.IsAnyTutorialRunning)
                    {
                        return;
                    }
                    var panel = _panelStack.Last();
                    if (panel != null && panel.Config.AcceptHardwareBack)
                    {
                        if (Input.GetKeyDown(KeyCode.Escape))
                        {
                            Back();
                            return;
                        }
                    }
                }
                if (UseExitPanel && UseBackButton && _panelStack.Count == 0)
                {
                    if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        OnAttemptToExit?.Invoke();
                    }

                }
            }

        }

        /// <summary>
        /// Back. Hides the previous panel or tries to exit.
        /// </summary>
        /// <param name="pm">Panel Manager Id.</param>
        public virtual void Back(int pm = 0)
        {
            if(pm == ID)
            {
                BackCoroutine();
            }
        }

        /// <summary>
        /// Back. Hides the previous panel or tries to exit.
        /// </summary>
        protected virtual async Task BackCoroutine()
        {
            QueuedPanel queuedPanel = null;
            if (_panelHiding == null && _panelStack.Count > 0)
            {
                //Get the last panel
                PanelModel lastPanel = _panelStack.Last();
                if (lastPanel != null)
                {
                    //Remove it from the stack
                    _panelStack.RemoveAt(_panelStack.Count - 1);

                    PanelModel nextPanel = null;
                    
                    if (_panelStack.Count > 0)
                    {
                        nextPanel = _panelStack.Last();
                    }
                    else if (_panelsQueued.Count > 0)
                    {
                        queuedPanel = _panelsQueued[0];
                        _panelsQueued.RemoveAt(0);
                    }

                    //Hide it
                    await Hide(lastPanel, ID, nextPanel);

                    //Find previous active panel
                    
                    if (nextPanel != null)
                    {
                        await Show(nextPanel, ID, !nextPanel.PanelGO.gameObject.activeSelf);
                    }
                    else if (queuedPanel != null)
                    {
                        await ShowOrInstantiate(queuedPanel.PanelName, ID, PanelBehavior.KeepInStack, queuedPanel.PreAction, queuedPanel.PostAction);
                    }
                    else
                    {
                        //Send event that lets everyone know that there are no popups showing at the moment
                        PanelsEvents.PanelStackIsEmpty();
                    }
                }
            }
        }
    }

    public enum PanelBehavior
    {
        KeepInStack,
        KeepActive,
        RemoveFromStack,
        ClearStack,
        Queue
    }

    /// <summary>
    /// Class to keep the queued up panels
    /// </summary>
    public class QueuedPanel
    {
        /// <summary>
        ///  The name of the panel
        /// </summary>
        public string PanelName;

        /// <summary>
        /// The pre action
        /// </summary>
        public Action<PanelGameObject> PreAction;

        /// <summary>
        /// The post action
        /// </summary>
        public Action<PanelGameObject> PostAction;
    }
}