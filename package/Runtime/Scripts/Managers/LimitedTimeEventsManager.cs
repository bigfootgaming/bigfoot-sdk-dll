﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Analytics;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.SceneManagement;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;

namespace BigfootSdk.LTE
{
	/// <summary>
	/// Manager to handle everything relate to Limited time events
	/// </summary>
	public class LimitedTimeEventsManager : LoadableSingleton<LimitedTimeEventsManager>
	{
		public static Action OnNewEventStarted;
	
		/// <summary>
		/// The events config
		/// </summary>
		protected LimitedTimeEventsModel _config;

		// Coroutine that checks when the event is over, and kicks the player out
		protected Coroutine _checkEventOverCoroutine;
		
		/// <summary>
		/// The active event
		/// </summary>
		protected LimitedTimeEventModel _activeEvent;
		public LimitedTimeEventModel ActiveEvent => _activeEvent;

		public override void StartLoading()
		{
			// Load the config
			_config = ConfigHelper.LoadConfig<LimitedTimeEventsModel>("events");

			// Check for an active event
			CheckActiveEvent();

			// Check if there are any events that need pruning
			SeeIfShouldPruneEvents();
			
			FinishedLoading(true);
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			GameModeManager.OnGameModeChanged += GameModeChanged;
		}

		protected virtual void OnDisable()
		{
			GameModeManager.OnGameModeChanged -= GameModeChanged;
		}

		/// <summary>
		/// The GameMode changed
		/// </summary>
		/// <param name="currentGameMode"></param>
		protected virtual void GameModeChanged(string currentGameMode, bool isRestart)
		{
			if (currentGameMode == GameModeConstants.MAIN_GAME)
			{
				if(_checkEventOverCoroutine != null)
					StopCoroutine(_checkEventOverCoroutine);
			}
			else
			{
				// If it's the first time we are in the event, send an analytics
				if (!PlayerManager.Instance.GetFlag(FlagsConstants.ALREADY_PLAYED_EVENT,
					GameModeManager.CurrentGameMode, false))
				{
					NewEventStarted();
					
					var data = new Dictionary<string, object>();
					data.Add("eventId", _activeEvent.Id);
					AnalyticsManager.Instance.TrackCustomEvent("EventStarted", data);
					
					PlayerManager.Instance.SetFlag(FlagsConstants.ALREADY_PLAYED_EVENT, true, GameModeManager.CurrentGameMode);
				}
				_checkEventOverCoroutine = StartCoroutine("CheckIfEventOver");
			}
		}
		
		/// <summary>
		/// Method called once per LTE. Use this for initialization
		/// </summary>
		protected virtual void NewEventStarted()
		{
			OnNewEventStarted?.Invoke();
		}

		/// <summary>
		/// Make sure we leave the event if the timer is over
		/// </summary>
		/// <returns></returns>
		IEnumerator CheckIfEventOver()
		{
			var waitForSeconds = new WaitForSeconds(5);
			while (GetTimeUntilActiveEventEnds() > 0)
			{
				yield return waitForSeconds;
			}
			
			GameModeManager.ChangeToMainGame(BigfootSceneManager.LastScene);
		}

		/// <summary>
		/// Check for an active event
		/// </summary>
		public virtual void CheckActiveEvent()
		{
			if (_config != null)
			{
				// Iterate through all of the events
				foreach (var limitedTimeEvent in _config.Events)
				{
					// If we find one that meets our prereqs
					if (PrerequisiteHelper.CheckPrerequisites(limitedTimeEvent.Prerequisites))
					{
						// Set it as our active event
						_activeEvent = limitedTimeEvent;

						// Stop looking
						return;
					}
				}
			}
		}

		/// <summary>
		/// Get the unlock prerequisites for this feature
		/// </summary>
		/// <returns>The prerequisites</returns>
		public virtual List<IPrerequisite> GetUnlockPrerequisites()
		{
			return _config?.UnlockPrerequisites;
		}
		
		/// <summary>
		/// Checks if the events are Locked
		/// </summary>
		/// <returns>if the events are locked</returns>
		public virtual bool AreEventsLocked()
		{
			return !PrerequisiteHelper.CheckPrerequisites(GetUnlockPrerequisites());
		}

		/// <summary>
		/// Get in how much time the active event ends
		/// </summary>
		/// <returns>The time until the active event ends</returns>
		public virtual long GetTimeUntilActiveEventEnds()
		{
			// If we don't have an event, return -1
			if (ActiveEvent == null)
				return -1;

			// Return the time until the active event ends
			return GetTimeUntilEventEnds(ActiveEvent);
		}
		
		/// <summary>
		/// Get how much time until a new event starts
		/// </summary>
		/// <returns>The time until a new event starts</returns>
		public virtual long GetTimeUntilNextEventStarts()
		{
			LimitedTimeEventModel upcomingEvent = GetUpcomingEventModel();

			if (upcomingEvent != null)
			{
				// Find the start IPrerequisite
				var startPrerequisite = upcomingEvent.Prerequisites.FirstOrDefault(prereq => prereq is TimePrereq timePrereq && timePrereq.Comparison == "lt");
						
				// If it exists
				if (startPrerequisite != null)
				{
					// Cast it to a TimePrereq
					var startTimePrerequiste = startPrerequisite as TimePrereq;

					if (startTimePrerequiste != null)
					{
						// Return how long we have to wait until 
						return TimeHelper.GetTimeRemainingUntilEvent(startTimePrerequiste.UnixTime);
					}
				}
			}

			return -1;
		}

		/// <summary>
		/// Get the Upcoming event in the schedule
		/// </summary>
		/// <returns>Upcoming Event</returns>
		public virtual LimitedTimeEventModel GetUpcomingEventModel()
		{
			if (_config != null && _config.Events != null)
			{
				// Iterate through all of the events
				foreach (var eventModel in _config.Events)
				{
					// If it's not over, it means it's the upcoming event
					if (GetTimeUntilEventEnds(eventModel) > 0)
					{
						return eventModel;
					}
				}
			}

			return null;
		}
		
		/// <summary>
		/// Get in how much time an event ends
		/// </summary>
		/// <returns>The time until an event ends</returns>
		protected virtual long GetTimeUntilEventEnds(LimitedTimeEventModel eventModel)
		{
			// Find the end IPrerequisite
			var endPrerequisite = eventModel.Prerequisites.FirstOrDefault(prereq => prereq is TimePrereq timePrereq && timePrereq.Comparison == "gt");

			// If it exists
			if (endPrerequisite != null)
			{
				// Cast it to a TimePrereq
				var endTimePrerequiste = endPrerequisite as TimePrereq;

				if (endTimePrerequiste != null)
				{
					// Return how long we have to wait until 
					return TimeHelper.GetTimeRemainingUntilEvent(endTimePrerequiste.UnixTime);
				}

				return -1;
			}
			
			return -1;
		}

		/// <summary>
		/// Maps a key to an event key
		/// </summary>
		/// <param name="key">The key to look for</param>
		/// <param name="fromConfiguration">If set to true, it will look in the Configuration dictionary, if false it will append '_{EventId}'</param>
		/// <returns>The mapped key</returns>
		public virtual string GetConfigurationMapping(string key, bool fromConfiguration = true)
		{
			// If there is no event active or we are in the main game, just return the same key
			if (_activeEvent == null || GameModeManager.CurrentGameMode == GameModeConstants.MAIN_GAME)
			{
				return key;
			}

			if (!fromConfiguration)
			{
				return string.Format("{0}_{1}", key, _activeEvent.Name);
			}

			// Grab the key mapping for this config
			if (_activeEvent.Configurations.TryGetValue(key, out var mappedKey))
			{
				LogHelper.LogSdk(string.Format("Using mapped version of {0}: {1}", key, mappedKey));
				return mappedKey;
			}

			// If we dont have a mapping, use the same one
			return key;
		}

		/// <summary>
		/// Check if a specific LimitedTimeEvent is over
		/// </summary>
		/// <param name="eventModel"></param>
		/// <returns></returns>
		public virtual bool IsEventOver(LimitedTimeEventModel eventModel)
		{
			// Find the end IPrerequisite
			var endPrerequisite = eventModel.Prerequisites.FirstOrDefault(prereq => prereq is TimePrereq timePrereq && timePrereq.Comparison == "gt");
			
			// If it exists
			if (endPrerequisite != null)
			{
				// Cast it to a TimePrereq
				var endTimePrerequiste = endPrerequisite as TimePrereq;

				if (endTimePrerequiste != null)
				{
					// See if its over
					return endTimePrerequiste.UnixTime < TimeHelper.GetTimeInServer();
				}
			}

			return false;
		}
		
		/// <summary>
		/// Get all of the events that have not been collected
		/// </summary>
		/// <returns></returns>
		public virtual List<LimitedTimeEventModel> GetEventsNotCollected()
		{
			// Create the result
			var result = new List<LimitedTimeEventModel>();
			PlayerManager playerManager = PlayerManager.Instance;
			
			// Iterate through all of the events
			foreach (var eventModel in _config.Events)
			{
				// Make sure they are finished
				if (IsEventOver(eventModel))
				{
					// Grab the max level reached (used to see if the user played the event)
					var levelReached = PlayerManager.Instance.GetCounterForSpecificEvent(CountersConstants.LTE_MAX_LEVEL_REACHED, eventModel.Id, -1);
					
					// And that it hasn't been collected
					var isEventCollected = playerManager.GetFlagForSpecificEvent(FlagsConstants.LTE_REWARDS_COLLECTED, eventModel.Id, false);
					if (!isEventCollected && levelReached != -1)
					{
						result.Add(eventModel);
					}
				}
			}

			return result;
		}

		/// <summary>
		/// Try to prune old events
		/// </summary>
		protected virtual void SeeIfShouldPruneEvents()
		{
			var playerManager = PlayerManager.Instance;

			var playerInfoModels = playerManager.GetAllPlayerInfoModels();
			
			// Get the number of the current event
			var currentEventNumber = -1;
			if (ActiveEvent != null)
			{
				currentEventNumber = ActiveEvent.Number;
			}
			else
			{
				var upcomingEventModel = GetUpcomingEventModel();
				if (upcomingEventModel != null)
				{
					currentEventNumber = upcomingEventModel.Number;
				}
			}

			// Exit if we dont have any event
			if (currentEventNumber == -1)
				return;

			// Create a list to contain all of the events that need pruning
			var eventsToPrune = new List<string>();
			
			// Check each of the events, and if needed add to the list to be pruned
			foreach (var playerInfoModel in playerInfoModels)
			{
				ObscuredInt eventNumber;
				if (playerInfoModel.Value.Counters.ContainsKey("EventNumber") && playerInfoModel.Value.Counters.TryGetValue("EventNumber", out eventNumber))
				{
					if (eventNumber.GetDecrypted() >= 0 && currentEventNumber - eventNumber.GetDecrypted() >= _config.EventDifferenceToKeep)
					{
						eventsToPrune.Add(playerInfoModel.Key);
					}
				}
				
			}
			
			// Prune the events
			foreach (var eventId in eventsToPrune)
			{
				playerInfoModels.Remove(eventId);
			}
		}

		public virtual void TrackEventCompleted()
		{
			var data = new Dictionary<string, object>();
			data.Add("eventId", _activeEvent.Id);
			AnalyticsManager.Instance.TrackCustomEvent("EventCompleted", data);
		}
	}
}
