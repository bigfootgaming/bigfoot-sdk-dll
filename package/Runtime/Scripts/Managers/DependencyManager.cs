﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk.SceneManagement;
using XNode;
using System;
using System.Diagnostics;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;
#if UNITY_EDITOR
using Sirenix.OdinInspector;
using UnityEditor;
using System.IO;
#endif

namespace BigfootSdk
{
    /// <summary>
    /// Dependency manager. This class handles the dependencies between loaders.
    /// </summary>
    public class DependencyManager : Singleton<DependencyManager>
    {
        public static Action OnDependencyManagerStarted;
        /// <summary>
        /// Event for when a all the loader have finished successfully.
        /// </summary>
        public static Action OnEverythingLoaded;

        /// <summary>
        /// Event for when a loader has failed.
        /// </summary>
        public static Action<string> OnSomethingFailed;

        /// <summary>
        /// Event for when a loader has finished.
        /// </summary>
        public static Action<string> OnSomethingFinished;

        /// <summary>
        /// Should it start loading automatically when the scene starts?
        /// </summary>
        public bool StartLoadingAutomatically = true;

        /// <summary>
        /// Should it be destroyed on load?
        /// </summary>
        public bool ShouldntDestroyOnLoad;

        /// <summary>
        /// If true the the Scene to load will be laoded.
        /// </summary>
        public bool LoadNextScene = true;

        /// <summary>
        /// Which Scene to Load after everything is finished
        /// </summary>
#if UNITY_EDITOR
        [ValueDropdown("GetScenesNames")]
#endif
        public string SceneToLoad;

        /// <summary>
        /// How many things to load at the same time
        /// </summary>
        public int ThingsToLoadAtTheSameTime = 1;

        /// <summary>
        /// The things to load.
        /// </summary>
        public List<ILoadable> ThingsToLoad;

        /// <summary>
        /// The things that are done loading.
        /// </summary>
#if UNITY_EDITOR
        [DisableInEditorMode]
#endif
        public List<ILoadable> ThingsDoneLoading;

        /// <summary>
        /// The percentage of things loaded
        /// </summary>
        public float PercentageLoaded => _percentageLoaded;

        /// <summary>
        /// The percentage of things loaded
        /// </summary>
        private float _percentageLoaded = 0;

        /// <summary>
        /// How many things we are currently loading
        /// </summary>
        private int _thingsLoading = 0;

        /// <summary>
        /// How many things we need to load
        /// </summary>
        int _thingsToLoad = 0;
        
        /// <summary>
        /// Flag to know if we are changing scene
        /// </summary>
        bool _changingScene = false;

        /// <summary>
        /// Time.time at the start of this object.
        /// </summary>
        float _initialTime;

        /// <summary>
        /// The current loading scene. 
        /// </summary>
        private string _currentScene;
        
        // List of ILoadables that we already checked
        List<string> _alreadyChecked;

        private Stopwatch _stopwatch;

        struct StartedTime
        {
            public string Id;
            public long Milliseconds;
        }

        private List<StartedTime> _startedTimes;

        private string _fullLog = "";

        void Start()
        {
            if (StartLoadingAutomatically) StartLoading();
        }

        public void StartLoading()
        {
            _stopwatch = new Stopwatch();
            _startedTimes = new List<StartedTime>();
            _stopwatch.Start();
            OnDependencyManagerStarted?.Invoke();
            // Cache the initial time
            _initialTime = Time.time;
            _currentScene = BigfootSceneManager.GetActiveScene().name;
            if (ShouldntDestroyOnLoad)
            {
                // Make sure this persists between scenes
                DontDestroyOnLoad(this);
            }

            _changingScene = false;

            // Calculate how many things we need to load
            _alreadyChecked = new List<string>();
            CalculateThingsToLoad(ThingsToLoad);
            
            // Start loading things
            SeeIfThingsReadyToLoad();
        }

        /// <summary>
        /// Calculates how many things we need to load
        /// </summary>
        /// <param name="thingsToLoad"></param>
        void CalculateThingsToLoad(List<ILoadable> thingsToLoad)
        {
            // Iterate through all of the list
            foreach (var loadable in thingsToLoad)
            {
                // If that loadable hasn't been checked
                if (!_alreadyChecked.Contains(loadable.Id))
                {
                    // Add it to the already checked list
                    _alreadyChecked.Add(loadable.Id);
                    
                    // Increment our counter
                    _thingsToLoad++;
                
                    // Calculate all of their triggers as well
                    CalculateThingsToLoad(loadable.Triggers);
                }
            }
        }
        
        /// <summary>
        /// Sees if things ready to load.
        /// </summary>
        void SeeIfThingsReadyToLoad()
        {
            // Iterate though all the things we need to load
            for (int i = 0; i < ThingsToLoad.Count; i++)
            {
                // If we have room in the queue
                if (_thingsLoading < ThingsToLoadAtTheSameTime)
                {
                    // Grab the ILoadable
                    var thingToLoad = ThingsToLoad[i];

                    // Check that all of it's dependencies are already loaded
                    if (AllDependenciesLoaded(thingToLoad) && !thingToLoad.IsLoading)
                    {
                        LogHelper.LogSdk(
                            string.Format("{0} Started Loading. Current Load time: {1}", thingToLoad.Id,
                                Time.time - _initialTime), LogHelper.DEPENDENCY_MANAGER_TAG);
                        
                        _startedTimes.Add(new StartedTime(){Id = thingToLoad.Id, Milliseconds = _stopwatch.ElapsedMilliseconds});
                        
                        // Start loading
                        // Increment our counter of things that are currently loading
                        _thingsLoading++;
                        thingToLoad.OnFinishedLoading += SomethingFinished;
                        thingToLoad.IsLoading = true;
                        thingToLoad.StartLoading();
    
                        
                    }
                }
                else
                    return;
            }
        }

        /// <summary>
        /// Method to be called when something finished loading
        /// </summary>
        /// <param name="id">Identifier.</param>
        void SomethingFinished(string id)
        {
            if (gameObject != null)
            {
                StartedTime startedTime;
                startedTime.Milliseconds = 0;
                for (int i = 0; i < _startedTimes.Count; i++)
                {
                    if (_startedTimes[i].Id == id)
                    {
                        startedTime = _startedTimes[i];
                        _startedTimes.RemoveAt(i);
                        break;
                    }
                }

                long diff = _stopwatch.ElapsedMilliseconds - startedTime.Milliseconds;
                if (SdkManager.Instance.Configuration.DebugSdkEvents)
                    _fullLog += $"{id},{diff.ToString()},\n";

                LogHelper.LogSdk(string.Format("{0} Finished Loading. Current Load time: {1}", id, Time.time - _initialTime), LogHelper.DEPENDENCY_MANAGER_TAG);
                
                // Get the thing that finished loading
                var thingFinished = ThingsToLoad.Where(il => il.Id == id).FirstOrDefault();

                thingFinished.OnFinishedLoading -= SomethingFinished;

                // Decrement our counter of things that are currently loading
                if (thingFinished.IsLoaded)
                {
                    // Decrement the things loaded
                    _thingsLoading--;
                    // Add it to the ThingsDoneLoading
                    ThingsDoneLoading.Add(thingFinished);

                    // Update the percentage
                    _percentageLoaded = (float)ThingsDoneLoading.Count / (float)_thingsToLoad;
                    
                    // Send the event that something finished loading
                    OnSomethingFinished?.Invoke(thingFinished.Id);

                    // If we have some Triggers, we should load them up
                    if (thingFinished.Triggers != null && thingFinished.Triggers.Count > 0)
                        AddDependenciesToThingsToLoad(thingFinished);

                    // Remove it from the ThingsToLoad
                    ThingsToLoad.Remove(thingFinished);

                    // See if there is anything we can start loading now
                    SeeIfThingsReadyToLoad();

                    // Check if we are finished
                    CheckIfFinished();
                }
                else
                {
                    OnSomethingFailed?.Invoke(thingFinished.Id);
                }
            }
        }

        /// <summary>
        /// Adds the dependencies for this ILoadable to the things that we need to load
        /// </summary>
        /// <param name="lodeable">Lodeable.</param>
        void AddDependenciesToThingsToLoad(ILoadable lodeable)
        {
            // For each of the triggers the thing that finished has
            for (int i = 0; i < lodeable.Triggers.Count; i++)
            {
                // Grab the trigger
                var currentTrigger = lodeable.Triggers[i];

                // Only load it if it wasnt loaded before
                if (!ThingsDoneLoading.Contains(currentTrigger) && !ThingsToLoad.Contains(currentTrigger))
                {
                    // Add it to the things to load
                    currentTrigger.IsLoading = false;
                    ThingsToLoad.Add(currentTrigger);
                }
            }
        }

        /// <summary>
        /// Checks if everything is loading
        /// </summary>
        void CheckIfFinished()
        {
            // If we are finished loading everything and we are not already in the process of changing scenes
            if (ThingsToLoad.Count == 0 && !_changingScene)
            {
                _stopwatch.Stop();
                _stopwatch = null;
                FullLog();
                float loadTime = Time.time - _initialTime;
                AnalyticsManager.Instance.TrackDependencyLoadEvent(loadTime, _currentScene);
                _changingScene = true;
                if (!string.IsNullOrEmpty(SceneToLoad) && LoadNextScene)
                    BigfootSceneManager.LoadScene(SceneToLoad);
                OnEverythingLoaded?.Invoke();
            }
        }

        void FullLog()
        {
            if (SdkManager.Instance.Configuration.DebugSdkEvents)
            {
                string[] split = _fullLog.Split('\n');
                int index = 0;
                while (split.Length - index > 40)
                {
                    string log = "";
                    int limit = index + 40;
                    for (int i = index; i < limit; i++)
                    {
                        log += split[i] + '\n';
                    }
                    LogHelper.LogSdk(log);
                    index += 40;
                }
                string lastlog = "";
                for (int i = index; i < split.Length; i++)
                {
                    lastlog += split[i] + '\n';
                }
                LogHelper.LogSdk(lastlog);
            }
        }

        /// <summary>
        /// Check if all of the dependencies for this ILoadable are loaded.
        /// </summary>
        /// <returns><c>true</c>, if all dependencies are loaded, <c>false</c> otherwise.</returns>
        /// <param name="thingToLoad">Thing to load.</param>
        bool AllDependenciesLoaded(ILoadable thingToLoad)
        {
            for (int i = 0; i < thingToLoad.Dependencies.Count; i++)
            {
                var ilodeable = ThingsDoneLoading.Where(il => il.Id == thingToLoad.Dependencies[i]).FirstOrDefault();
                if (ilodeable == null)
                {
                    return false;
                }

            }
            return true;
        }
        
    #if UNITY_EDITOR
        #region Editor Only Region

        // Reference to the node graph
        public NodeGraph Graph;

        

        // Offsets for the graph
        int _xOffset = 300;
        int _yOffset = 150;

        // Cached positions used to position items on the Y axis
        Dictionary<int, int> yPositions;

        /// <summary>
        /// Method that gets called everytime something changes in the inspector for this component
        /// </summary>
        [Button("Refresh Graph", ButtonSizes.Medium)]
        [ButtonGroup("Graph")]
        public void OnValidate()
        {
            // Reset all of the values
            yPositions = new Dictionary<int, int>();
            if (Graph != null)
                Graph.nodes = new List<Node>();
            
            // Start generating the graph
            bool notNull = true;
            foreach (var l in ThingsToLoad)
                if (l == null)
                    notNull = false;
            if (notNull)
            {
                RemoveIndirectTriggers(ThingsToLoad);
                FillDependencies(ThingsToLoad);
                UpdateGraph(ThingsToLoad);
            }
        }
        
        /// <summary>
        /// Fills the dependencies of all loadables.
        /// </summary>
        /// <param name="loadables"></param>
        void FillDependencies(List<ILoadable> loadables)
        {
            //temp list
            List<ILoadable> toFill = new List<ILoadable>();
            toFill.AddRange(loadables);
            
            foreach (var t in toFill)
            {
                t.Dependencies = new List<string>();
            }
            
            int i = 0;
            //complete loadables and reset dependencies
            while (toFill.Count > i)
            {
                ILoadable l = toFill[i];
                foreach (var t in l.Triggers)
                {
                    if (!toFill.Contains(t))
                        toFill.Add(t);
                    t.Dependencies = new List<string>();
                }
                i++;
            }
            //fill dependencies
            i = 0;
            while (toFill.Count > i)
            {
                ILoadable l = toFill[i];
                if (l != null)
                {
                    foreach (var t in l.Triggers)
                    {
                        if (!t.Dependencies.Contains(l.name))
                            t.Dependencies.Add(l.name);
                    }
                    i++;
                }
            }
        }

        /// <summary>
        /// Simplify graph cleaning unnecessary triggers.
        /// </summary>
        /// <param name="loadables"></param>
        void RemoveIndirectTriggers(List<ILoadable> loadables)
        {
            List<ILoadable> toCheck = new List<ILoadable>();
            toCheck.AddRange(loadables);
            int i = 0;
            //add loaders to check
            while (toCheck.Count > i)
            {
                ILoadable l = toCheck[i];
                foreach (var t in l.Triggers)
                {
                    if (!toCheck.Contains(t))
                        toCheck.Add(t);
                }
                i++;
            }
            foreach (var l in toCheck)
            {
                i = 0;
                while(i < l.Triggers.Count)
                {
                    CheckAllIndirectTriggers(l, l.Triggers[i], ref i);
                    i++;
                }
            }            
        }

        /// <summary>
        /// Checks repeated triggers recursively against one loadable.
        /// </summary>
        /// <param name="loadable">Loadable to compare against</param>
        /// <param name="startingTrigger">Starting trigger in recursion</param>
        /// <param name="currentIndex">Current index in Triggers loadable</param>
        void CheckAllIndirectTriggers(ILoadable loadable, ILoadable startingTrigger, ref int currentIndex)
        {
            int i = 0;
            while(i < startingTrigger.Triggers.Count)
            {
                CheckAllIndirectTriggers(loadable, startingTrigger.Triggers[i], ref currentIndex);
                if (loadable.Triggers.Contains(startingTrigger.Triggers[i]))
                {
                    int removedIndex = loadable.Triggers.IndexOf(startingTrigger.Triggers[i]);
                    // Back one in index if one trigger is removed
                    if (removedIndex <= currentIndex) currentIndex--;
                    loadable.Triggers.RemoveAt(removedIndex);
                }
                else
                {
                    i++;
                }
            }
        }

        /// <summary>
        /// Stores the depth of each node in the graph
        /// </summary>
        private Dictionary<string, int> _nodeDepth;
        
        /// <summary>
        /// Updates the graph
        /// </summary>
        /// <param name="loadables"></param>
        void UpdateGraph(List<ILoadable> loadables)
        {
            _nodeDepth = new Dictionary<string, int>();
            //Add all nodes
            foreach (var l in loadables)
                AddNode(l);
        }

        void AddNode(ILoadable loadable)
        {
            //if the loadable wasn't added and all dependecies were added
            if (!_nodeDepth.ContainsKey(loadable.name) && AllDependeciesAdded(loadable))
            {
                Node node = Graph.AddNode<DependencyNode>();
                node.name = loadable.name;
                var childInput = node.GetInputPort("Dependencies");
                int depth = 0;
                //create the links between current node and its dependencies
                foreach (var d in loadable.Dependencies)
                {
                    depth = Math.Max(depth, _nodeDepth[d] + 1);
                    Node parentNode = Graph.nodes.Where(n => n.name == d).FirstOrDefault();
                    var parentOutput = parentNode.GetOutputPort("Triggers");

                    if (childInput != null && parentOutput != null)
                    {
                        // Make the actual connection on the graph
                        parentOutput.Connect(childInput);
                    }
                }
                //add node depth
                _nodeDepth.Add(loadable.name, depth);

                int xPos = _xOffset * depth;
                int yPos = 0;
                int mod = depth % 3; 
                yPos = _yOffset * (mod + 1) / 3 ;
                if (yPositions.ContainsKey(xPos))
                {
                    yPos = yPositions[xPos];
                    yPositions[xPos] += _yOffset;
                }
                else
                {
                    yPositions.Add(xPos, yPos + _yOffset);
                }
                    
                node.position = new Vector2(xPos, yPos);
                //call recursively with trigger loadables
                foreach (var t in loadable.Triggers)
                {
                    AddNode(t);
                }
            }
        }

        /// <summary>
        /// returns true if all dependencies of current loadable were already added to the graph.
        /// </summary>
        /// <param name="loadable"></param>
        /// <returns></returns>
        bool AllDependeciesAdded(ILoadable loadable)
        {
            foreach (var d in loadable.Dependencies)
            {
                if (!_nodeDepth.ContainsKey(d))
                    return false;
            }

            return true;
        }

        [Button("Open Graph", ButtonSizes.Medium)]
        [ButtonGroup("Graph")]
		void OpenGraph()
		{
			Selection.activeObject = Graph;
			EditorGUIUtility.PingObject(Selection.activeObject);
            AssetDatabase.OpenAsset(Graph);
		}

        IEnumerable<string> GetScenesNames()
        {
            return new string[]{ "" }.Concat(EditorBuildSettings.scenes.Select(scene => Path.GetFileNameWithoutExtension(scene.path)));
        }
        #endregion
#endif
    }
}