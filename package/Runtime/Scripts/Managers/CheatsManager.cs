﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace BigfootSdk
{
    public class CheatsManager : Singleton<CheatsManager>
    {
        /// <summary>
        /// Flag to know if the gesture began
        /// </summary>
        private bool _gestureBegan = false;

        /// <summary>
        /// The Y start position of the gesture
        /// </summary>
        private float _gestureStartYPosition;

        /// <summary>
        /// The Y end position of the gesture
        /// </summary>
        private float _gestureEndYPosition;

        /// <summary>
        /// Flag to know if the cheat panel is open
        /// </summary>
        private bool _open = false;

        /// <summary>
        /// String containing the log
        /// </summary>
        private string _log = "";
        public string Log => _log;

        /// <summary>
        /// The cheats panel
        /// </summary>
        public AssetReferenceGameObject CheatsPrefab;
        
        /// <summary>
        /// Reference to the cheats dialog
        /// </summary>
        CheatsDialog _cheatsDialog;

        protected void OnEnable()
        {
        #if ENVIRONMENT_DEV || UNITY_EDITOR
            Application.logMessageReceived += HandleLog;
         #endif
        }
        
        private void OnDisable()
        {
        #if ENVIRONMENT_DEV || UNITY_EDITOR
            Application.logMessageReceived -= HandleLog;
        #endif
        }

        /// <summary>
        /// Check for gestures
        /// </summary>
        private void Update()
        {
    #if ENVIRONMENT_DEV || UNITY_EDITOR
            if (!_open && (Input.inputString == "º" || Input.GetKey(KeyCode.BackQuote) || GestureDestected(500)))
            {
                CheatsPrefab.LoadAssetAsync().Completed += (load) => {
                    if (load.Result != null)
                    {
                        var instance = Instantiate(load.Result, transform);
                        _cheatsDialog = instance.GetComponent<CheatsDialog>();
                        _cheatsDialog.Initialize();
                        OpenConsole();
                    }
                };
            }
    #endif
        }

        /// <summary>
        /// Open the console
        /// </summary>
        void OpenConsole()
        {
            _open = true;
            _cheatsDialog.gameObject.SetActive(true);
        }

        public void CloseConsole()
        {
            _open = false;
            _cheatsDialog.gameObject.SetActive(false);
        }
        
        /// <summary>
        /// Try to detect the gesture to open the console
        /// </summary>
        /// <param name="gestureLength"></param>
        /// <returns></returns>
        private bool GestureDestected(float gestureLength)
        {
            var touches = Input.touches;
            if (!_gestureBegan && touches.Length >= 3)
            {
                _gestureBegan = true;
                _gestureStartYPosition = touches[0].position.y;
            }

            if (_gestureBegan && touches.Length >= 3)
            {
                _gestureEndYPosition = touches[0].position.y;
            }

            if (_gestureBegan && touches.Length < 3)
            {
                _gestureBegan = false;

                float delta = Mathf.Abs(_gestureEndYPosition - _gestureStartYPosition);
                if (_gestureEndYPosition > _gestureStartYPosition && delta > gestureLength)
                {
                    return true;
                }
            }

            return false;
        }
        
        public void HandleLog(string logString, string stackTrace, LogType type)
        {
            // Queue the text
            string formattedType;
            if (type == LogType.Error)
                formattedType = "<color=red>[Error]</color>";
            else if (type == LogType.Warning)
                formattedType = "<color=yellow>[Warning]</color>";
            else
                formattedType = "<color=#00ffff>[Log]</color>";
                
            string newString =  string.Format("\n {0}: {1}", formattedType, logString);
            _log += newString;
            
            if (type == LogType.Exception)
            {
                newString = "\n" + stackTrace;
                _log += newString;
            }
        }
    }
}
