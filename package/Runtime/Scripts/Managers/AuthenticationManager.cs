﻿using System;
using System.Threading.Tasks;
using BigfootSdk.Helpers;
using Unity.Collections;
using UnityEngine;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Manager in charge of register, login & syncing of users
	/// </summary>
	public class AuthenticationManager :  LoadableSingleton<AuthenticationManager>
	{
		public static Action OnSimultaneousSessionsDetected;

		public static Action OnDifferentPlayerIdsDetected;

		public static Action<string, bool> OnUserStateChanged;
		
		/// <summary>
		/// this is only used in the editor. If true, the user id will be the device unique id.
		/// </summary>
		public bool UseDeviceIdInEditor = true;
		
		/// <summary>
		/// The authentication service interface.
		/// </summary>
		protected IAuthenticationService _authenticationService;

		protected IAuthenticationService AuthenticationService {
			get {
                if (_authenticationService == null)
                {
                    #if BACKEND_AWS
                        _authenticationService = new AWSAuthenticationService();
                    #elif BACKEND_GS
                        _authenticationService = new GSAuthenticationService ();
	                #elif BACKEND_FIREBASE
						_authenticationService = new FirebaseAuthenticationService();
                    #endif
                }
                return _authenticationService;
			}
		}
		

		/// <summary>
		/// Initializes the Authentication of the player
		/// </summary>
		public override void StartLoading ()
		{
			bool isSocial = false;
			if (SocialManager.Instance != null)
				isSocial = SocialManager.Instance.Authenticated;
			LogHelper.LogSdk ($"Authenticating Player... Social: {isSocial}", LogHelper.DEPENDENCY_MANAGER_TAG);
			AuthenticationService.Initialize();
			if (isSocial)
			{
				if (AuthenticationService.IsSocialUser())
				{
					RegisterSocial();
				}
				else
				{
					LinkSocialAccount(null);
				}
			}
			else
			{
				if (AuthenticationService.IsSocialUser())
				{
					AuthenticationService.SignOut();
				}
				AuthenticationService.RegisterDefault(DoRegisterSuccess);
			}
		}

		protected virtual void RegisterSocial()
		{
			if (AuthenticationService.IsMatchingEmail(SocialManager.Instance.GetEmail()) || AuthenticationService.IsMatchingSocialUserId(SocialManager.Instance.GetUserId()))
			{
				LogHelper.LogSdk("detected already signed in");
				AuthenticationService.SetServerPlayerId();
				DoRegisterSuccess(true);
			}
			else if (IsSocialAuthenticated())
			{
				DoRegisterSocial();
			}
			else
			{
				SocialManager.Instance.Reauthenticate((result) =>
				{
					if (result)
						DoRegisterSocial();
					else
					{
						AuthenticationService.RegisterDefault(DoRegisterSuccess);
					}
				});
			}
		}

		protected virtual bool IsSocialAuthenticated()
		{
			return SocialManager.Instance.Authenticated;
		}

		protected void DoRegisterSocial()
		{
#if UNITY_ANDROID && GOOGLE_PLAY_GAMES
				RegisterWithPlayGames(HandleRegisterResult);
#elif UNITY_IOS && APPLE_ID
				RegisterAndRetrieveDataWithAppleId(HandleRegisterResult);
#endif
		}

		protected virtual void HandleRegisterResult(bool success)
		{
			LogHelper.LogSdk($"Handling register result: {success}", LogHelper.FIREBASE_SERVICES_TAG);
			bool authCompleted = true;
			if (success)
			{
				string localPlayerId = PlayerManager.Instance.LocalPlayerId;
				string firebasePlayerId = PlayerManager.Instance.ServerPlayerId;
				LogHelper.LogSdk($"Player ids => local id: {localPlayerId}, firebase id: {firebasePlayerId}", LogHelper.DEPENDENCY_MANAGER_TAG);
				if (!string.IsNullOrEmpty(localPlayerId) && localPlayerId != firebasePlayerId)
				{
					authCompleted = false;
					OnDifferentPlayerIdsDetected?.Invoke();
				}
			}
			if (authCompleted)
				DoRegisterSuccess(success);
		}

		public void DoRegisterSuccess(bool overridePlayerId)
		{
			if (overridePlayerId)
				PlayerManager.Instance.OverridePlayerId();
			RegisterSuccess(true);
		}

		void RegisterSuccess(bool success)
		{
			LogHelper.LogSdk(("Authenticating "+ success), LogHelper.DEPENDENCY_MANAGER_TAG);
			PlayerManager.Instance.SetPlayerId();
			if (!IsLoaded)
				FinishedLoading (success);
		}
		
		public virtual void LinkSocialAccount(Action<bool> callback)
		{
			if (IsSocialAuthenticated())
			{
				DoLinkSocialAccount(callback);
			}
			else
			{
				SocialManager.Instance.Reauthenticate((result) =>
				{
					if (result)
						DoLinkSocialAccount(callback);
					else
					{
						AuthenticationService.RegisterDefault(DoRegisterSuccess);
					}
				});
			}
		}


		public void DoLinkSocialAccount(Action<bool> callback)
		{
#if UNITY_ANDROID && GOOGLE_PLAY_GAMES
            LinkWithPlayGames(linkResult =>
            {
                if (linkResult)
                {
                    LinkSuccessful(callback);
                }
                else
                {
					AuthenticationService.ClearCredential();
					PlayGamesLinkFailed(callback);
                }
            });
#endif
#if UNITY_IOS && APPLE_ID
            LinkWithAppleId(linkResult =>
            {
                if (linkResult)
                {
                    LinkSuccessful(callback);
                }
                else
                {
	                if (AuthenticationService.HasValidCredential())
	                {
						LogHelper.LogSdk ("valid cred");
		                RegisterAndRetrieveDataWithAppleId(registerResult =>
		                {
			                callback?.Invoke(registerResult);
			                HandleRegisterResult(registerResult);
		                });
	                }
	                else
	                {
						LogHelper.LogSdk ("re auth social");
		                SocialManager.Instance.Reauthenticate((result) =>
		                {
							if (result)
							{
								RegisterAndRetrieveDataWithAppleId(registerResult =>
								{
									callback?.Invoke(registerResult);
									HandleRegisterResult(registerResult);
								});
							}
							else
							{
								AuthenticationService.RegisterDefault(DoRegisterSuccess);
							}
		                });
	                }
                }
            });
#endif
		}
		
		

		public virtual void LinkSuccessful(Action<bool> callback)
		{
			callback?.Invoke(true);
			RegisterSuccess(true);
		}
		

#if UNITY_ANDROID && GOOGLE_PLAY_GAMES
		public virtual void PlayGamesLinkFailed(Action<bool> callback)
		{
			RegisterWithPlayGames(registerResult =>
			{
				callback?.Invoke(registerResult);
				HandleRegisterResult(registerResult);
			});
		}

		public void RegisterWithPlayGames(Action<bool> callback)
		{
			AuthenticationService.RegisterWithPlayGames(callback);
		}
		
		public void LinkWithPlayGames(Action<bool> callback)
		{
			AuthenticationService.LinkWithPlayGames(callback);
		}
#endif

#if UNITY_IOS && APPLE_ID
		public void RegisterAndRetrieveDataWithAppleId(Action<bool> callback)
		{
			AuthenticationService.RegisterAndRetrieveDataWithAppleId(callback);
		}
		
		public void LinkWithAppleId(Action<bool> callback)
		{
			AuthenticationService.LinkWithAppleId(callback);
		}
#endif

		public void Unlink(string provider, Action<bool> callback)
		{
			AuthenticationService.Unlink(provider, callback);
		}

		public void SignOut()
		{
			_authenticationService.SignOut();
		}

		internal void Reset()
		{
			AuthenticationService.Reset();
		}
		
		
	}
}
