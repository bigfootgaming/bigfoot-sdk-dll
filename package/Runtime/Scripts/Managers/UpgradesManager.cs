﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk
{
	/// <summary>
    /// Upgrades manager.
    /// </summary>
	public class UpgradesManager : LoadableSingleton <UpgradesManager>
	{
		/// <summary>
		/// Upgrades manager configuration
		/// </summary>
		private UpgradesConfig _upgradesConfig;

        /// <summary>
        /// List with the bought upgrades.
        /// </summary>
        List<string> _boughtUpgrades;

        /// <summary>
        /// The player manager.
        /// </summary>
        PlayerManager _playerManager;

		public override void StartLoading() 
        {
            // Cache the playerManager
            _playerManager = PlayerManager.Instance;

            // Parse all of the bought upgrades
            string boughtUpgradesJson = _playerManager.GetFromPlayerData("Upgrades", GameModeManager.CurrentGameMode);
            if (!string.IsNullOrEmpty(boughtUpgradesJson))
                _boughtUpgrades = JsonHelper.DesarializeObject<List<string>>(boughtUpgradesJson);
            else
                _boughtUpgrades = new List<string>();

            // Fetch the upgrades config from the server
            _upgradesConfig = ConfigHelper.LoadConfig<UpgradesConfig>("upgrades");

            // Apply each of the bough upgrades
            foreach (string upgradeName in _boughtUpgrades)
            {
                ApplyUpgrade(upgradeName);
            }

			FinishedLoading(true);
		}

        public void BuyUpgrade(string upgradeName)
        {
            UpgradeModel upgradeModel = GetUpgradeByName(upgradeName);
            if (upgradeModel != null)
            {
                // If we have enough
                if(upgradeModel.Cost == null || upgradeModel.Cost.HasEnough())
                {
                    if (upgradeModel.Cost != null)
                    // Deduct the cost locally
                        upgradeModel.Cost.DeductCost("Upgrade", true);

                    // Serialize the object to be sent
                    Dictionary<string, string> data = new Dictionary<string, string>()
                    {
                        {"upgrade", JsonHelper.SerializeObject(upgradeModel)}
                    };

                    // Do the buy in the server
                    BackendManager.Instance.ExecuteCloudCode("BuyUpgrade", data, (success) => 
                    {
                        // Add the upgrade to the bought list
                        _boughtUpgrades.Add(upgradeName);

                        // Apply the upgrade
                        ApplyUpgrade(upgradeName);
                    }, () =>
                    {
                        if (upgradeModel.Cost != null)
                            upgradeModel.Cost.RevertDeductCost();
                    });
                }
                else
                {
                    LogHelper.Log("Unable to buy upgrade. Not enough to cover the cost!");
                }
            }
            else
            {
                LogHelper.LogWarning("Unable to buy upgrade. Fail trying to get upgrade by name: " + upgradeName);
            }
        }

		/// <summary>
		/// Execute upgrade by name
		/// </summary>
		/// <param name="upgradeName">Upgrade´s name</param>
        void ApplyUpgrade(string upgradeName) 
        {
            if (string.IsNullOrEmpty(upgradeName)) {
				LogHelper.LogWarning("Unable to execute upgrade. Null or empty upgrade name");
				return;
			}

            UpgradeModel upgradeModel = GetUpgradeByName(upgradeName);
			if (upgradeModel != null) 
            {
                GameplayModifiersManager.Instance.ApplyGameplayModifiers(upgradeModel.GameplayModifiers);
			}
            else
            {
                LogHelper.LogWarning("Unable to execute upgrade. Fail trying to get upgrade by name: " + upgradeName);
            }
        }

        /// <summary>
        /// Gets all the upgrades.
        /// </summary>
        /// <returns>The all upgrades.</returns>
        public List<UpgradeModel> GetAllUpgrades()
        {
            if (_upgradesConfig == null)
            {
                LogHelper.LogWarning("Unable to get upgrade by name. Null upgrades config");
                return null;
            }

            if (_upgradesConfig.Upgrades == null)
            {
                LogHelper.LogWarning("Unable to get upgrade by name. Null upgrade dictionary");
                return null;
            }

            return _upgradesConfig.Upgrades;
        }

        /// <summary>
        /// Gets an Upgrade by name
        /// </summary>
        /// <returns>The upgrade by name.</returns>
        /// <param name="upgradeName">Upgrade name.</param>
        public UpgradeModel GetUpgradeByName(string upgradeName)
        {
			if (string.IsNullOrEmpty(upgradeName)) {
				LogHelper.LogWarning("Unable to get upgrade by name. Null or empty upgrades name");
				return null;
			}

			if (_upgradesConfig == null) {
				LogHelper.LogWarning("Unable to get upgrade by name. Null upgrades config");
                return null;
            }

			if (_upgradesConfig.Upgrades == null) {
				LogHelper.LogWarning("Unable to get upgrade by name. Null upgrade dictionary");
                return null;
            }

            return _upgradesConfig.Upgrades.FirstOrDefault(up => up.UpgradeName == upgradeName);
		}
	}
}