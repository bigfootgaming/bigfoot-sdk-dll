﻿using System.Collections;
using BigfootSdk.Backend;
using DG.Tweening;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Class to animate company logos
    /// </summary>
    public class CompanyLogoManager : ILoadable
    {
        /// <summary>
        /// The canvas for the logos
        /// </summary>
        public CanvasGroup Logos;

        /// <summary>
        /// How long the logos will stay on screen
        /// </summary>
        public float StayTime;

        /// <summary>
        /// How long it will take to fade out
        /// </summary>
        public float FadeoutTime;

        /// <summary>
        /// The delay after the fadeout is finished to load the bar
        /// </summary>
        public float DelayLoadingBar;
        
         /// <summary>
        /// Make sure we will see the loading bar at least this amount of time
        /// </summary>
        public float MinDelayToLoadNextScene;

         public override void StartLoading()
        {
            LoadingScreenManager.Instance.WaitForLogos = true;

            StartCoroutine(FadeOutLogos());
        }

        private IEnumerator FadeOutLogos(bool independentTimeScale = true)
        {
            yield return new WaitForSeconds(StayTime);

            if (Logos != null)
                Logos.DOFade(0, FadeoutTime).SetUpdate(independentTimeScale);

            yield return new WaitForSeconds(FadeoutTime + DelayLoadingBar);

            Logos.interactable = false;
            Logos.blocksRaycasts = false;
            
            LoadingScreenManager.Instance.WaitForLogos = false;
            
            float remainingDelay = MinDelayToLoadNextScene - DelayLoadingBar;
            if (remainingDelay > 0)
                yield return new WaitForSecondsRealtime(remainingDelay);
            FinishedLoading(true);
        }
    }
}
