﻿using System;
using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;
using BigfootSdk.SceneManagement;

namespace BigfootSdk
{
    public class AdsManager : LoadableSingleton<AdsManager>
    {
        // Cached placementId
        protected string _placementId;
        
        // Persisted AdTypes to send when resuming application focus
        protected AdsService _adsService;

        // Cached analytics manager
        protected AnalyticsManager _analyticsManager;
        
        // Initialization Complete
        public static Action OnInitializeCompleted;

        // The custom event data
        protected Dictionary<string, object> _eventData = new Dictionary<string, object>();
        
        // Ads Services Interstitial Callbacks        
        public static Action OnInterstitialLoaded;
        public static Action OnInterstitialFailedToLoad;
        public static Action OnInterstitialShown;
        public static Action OnInterstitialShowFailed;
        public static Action OnInterstitialClosed;
        public static Action OnInterstitialClicked;
        public static Action OnInterstitialExpired;
        
        //Ads Services Banner Callbacks
        public static Action<int> OnBannerLoaded;
        public static Action OnBannerLoadFailed;
        public static Action OnBannerShown;
        public static Action OnBannerClicked;
        public static Action OnBannerExpired;
        
        // Ads Services RewardedVideo Callbacks
        public static Action OnRewardedVideoStartedLoading;
        public static Action OnRewardedVideoLoaded;
        public static Action<string> OnRewardedVideoFailedToLoad;
        public static Action OnRewardedVideoShown;
        public static Action<string> OnRewardedVideoShowFailed;
        public static Action<double, string> OnRewardedVideoFinished;
        public static Action OnRewardedVideoClosed;
        public static Action OnRewardedVideoCompleted;
        public static Action OnRewardedVideoExpired;
        public static Action OnRewardedVideoClicked;
        
        protected override void OnEnable()
        {
            base.OnEnable();

            OnInterstitialClosed += InterstitialClosed;
            OnInterstitialClicked += InterstitialClicked;
            OnInterstitialShown += InterstitialShown;

            OnRewardedVideoClicked += RewardedVideoClicked;
            OnRewardedVideoClosed += RewardedVideoClosed;
            OnRewardedVideoCompleted += RewardedVideoCompleted;
            OnRewardedVideoShown += RewardedVideoShown;
            OnRewardedVideoFinished += RewardedVideoFinished;
            OnRewardedVideoShowFailed += RewardedVideoShowFailed;
        }

        protected virtual void OnDisable()
        {
            OnInterstitialClosed -= InterstitialClosed;
            OnInterstitialClicked -= InterstitialClicked;
            OnInterstitialShown -= InterstitialShown;

            OnRewardedVideoClicked -= RewardedVideoClicked;
            OnRewardedVideoClosed -= RewardedVideoClosed;
            OnRewardedVideoCompleted -= RewardedVideoCompleted;
            OnRewardedVideoShown -= RewardedVideoShown;
            OnRewardedVideoFinished -= RewardedVideoFinished;
            OnRewardedVideoShowFailed -= RewardedVideoShowFailed;
        }

        public override void StartLoading()
        {
#if USE_APPODEAL            
            _adsService = new AppodealService();
#elif USE_APPLOVIN
            _adsService = new ApplovinService();
#elif USE_IRONSOURCE
            _adsService = new IronsourceService();
#else 
            _adsService = new AdsService();
            LogHelper.LogWarning("No AdService Provided, initializing with Default.");
#endif

            _analyticsManager = AnalyticsManager.Instance;
            
            _adsService.Initialize(() =>
            {
                OnInitializeCompleted?.Invoke();
                
                FinishedLoading(true);
                LogHelper.LogSdk("Ads Manager Finished Initialization", LogHelper.DEPENDENCY_MANAGER_TAG);
            },
    new int[2]{AdTypes.INTERSTITIAL , AdTypes.REWARDED_VIDEO});
        }

        public virtual void Reset()
        {
            _adsService?.Reset();
        }

        #region AdServices Methods
 
        /// <summary>
        /// <returns><c>true</c>, if the <paramref name="placementId"/> is available, <c>false</c> otherwise.</returns>
        /// </summary>
        /// <param name="adType">Type of the Ad to check</param>
        /// <param name="placementId">Optional Placement Id to verify</param>
        /// <returns><c>true</c>, if the <paramref name="placementId"/> is available, <c>false</c> otherwise.</returns>
        public bool IsAdAvailable(int adType, string placementId = "")
        {
            return _adsService.IsAdAvailable(adType, placementId);
        }

        /// <summary>
        /// Ask to show Interstitial
        /// </summary>
        public virtual void ShowInterstitial(string placementId = "")
        {
            _placementId = placementId;
            
            _eventData.Clear();
            _eventData.Add("adType", "Interstitial");
            _eventData.Add("adPlacement", _placementId);
            _eventData.Add("actionType", "started");

            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
            
            _adsService?.ShowInterstitial(placementId);
        }

        /// <summary>
        /// Ask to show RewardedVideo
        /// </summary>
        public virtual void ShowRewardedVideo(string placementId = "")
        {
            _placementId = placementId;
            
            _eventData.Clear();
            _eventData.Add("adType", "RewardedVideo");
            _eventData.Add("adPlacement", _placementId);
            _eventData.Add("actionType", "started");
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
            
            _adsService?.ShowVideoReward(placementId);
        }

        /// <summary>
        /// Ask to show Banner
        /// </summary>
        public virtual void ShowBanner(int bannerType)
        {
            _eventData.Clear();
            _eventData.Add("adType", "Banner");
            _eventData.Add("adPlacement", BigfootSceneManager.GetActiveScene().name);
            _eventData.Add("actionType", "started");
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
            
            _adsService?.ShowBanner(bannerType);
        }

        /// <summary>
        /// Ask to hide Banner
        /// </summary>
        public virtual void HideBanner(int bannerType)
        {
            _eventData.Clear();
            _eventData.Add("adType", "Banner");
            _eventData.Add("adPlacement", BigfootSceneManager.GetActiveScene().name);
            _eventData.Add("actionType", "closed");
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
            
            _adsService?.HideBanner(bannerType);
        }

        /// <summary>
        /// Comunicates to AdsService if the application has focus.
        /// </summary>
        /// <param name="hasFocus"></param>
        protected virtual void OnApplicationFocus(bool hasFocus)
        {
            _adsService?.OnApplicationFocus(hasFocus);
        }

        protected virtual void InterstitialClosed()
        {
            _eventData.Clear();
            _eventData.Add("adType", "Interstitial");
            _eventData.Add("adPlacement", _placementId);
            _eventData.Add("actionType", "closed");
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
        }

        protected virtual void InterstitialClicked()
        {
            _eventData.Clear();
            _eventData.Add("adType", "Interstitial");
            _eventData.Add("adPlacement", _placementId);
            _eventData.Add("actionType", "clicked");
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
        }

        protected virtual void InterstitialShown()
        {
            _eventData.Clear();
            _eventData.Add("adType", "Interstitial");
            _eventData.Add("adPlacement", _placementId);
            _eventData.Add("actionType", "shown");
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
        }
        
        protected virtual void RewardedVideoClicked()
        {
            _eventData.Clear();
            _eventData.Add("adType", "RewardedVideo");
            _eventData.Add("adPlacement", _placementId);
            _eventData.Add("actionType", "clicked");
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
        }
        
        protected virtual void RewardedVideoClosed()
        {
            _eventData.Clear();
            _eventData.Add("adType", "RewardedVideo");
            _eventData.Add("actionType", "closed");
            _eventData.Add("adPlacement", _placementId);
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
        }
        
        
        protected virtual void RewardedVideoCompleted()
        {
            _eventData.Clear();
            _eventData.Add("adType", "RewardedVideo");
            _eventData.Add("actionType", "completed");
            _eventData.Add("adPlacement", _placementId);
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
        }

        protected virtual void RewardedVideoShown()
        {
            _eventData.Clear();
            _eventData.Add("adType", "RewardedVideo");
            _eventData.Add("adPlacement", _placementId);
            _eventData.Add("actionType", "shown");
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
        }

        protected virtual void RewardedVideoFinished(double amount, string currency)
        {
            _eventData.Clear();
            _eventData.Add("adType", "RewardedVideo");
            _eventData.Add("adPlacement", _placementId);
            _eventData.Add("actionType", "finished");
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
        }
        
        protected virtual void RewardedVideoShowFailed(string error)
        {
            _eventData.Clear();
            _eventData.Add("adType", "RewardedVideo");
            _eventData.Add("adPlacement", _placementId);
            _eventData.Add("actionType", "failed");
            _eventData.Add("error", error);
            
            _analyticsManager.TrackCustomEvent("AdAction", _eventData);
        }
        #endregion
    }
}