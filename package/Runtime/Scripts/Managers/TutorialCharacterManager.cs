﻿using System;
using System.Linq;
using System.Collections.Generic;
using BigfootSdk.Helpers;

using UnityEngine;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorial character manager.
    /// </summary>
    public class TutorialCharacterManager : Singleton<TutorialCharacterManager>
    {
        /// <summary>
        /// The character parent.
        /// </summary>
        public Transform CharacterParent;

        /// <summary>
        /// The dialog parent.
        /// </summary>
        public Transform DialogParent;

        /// <summary>
        /// The pool.
        /// </summary>
        public ObjectPool Pool;

        /// <summary>
        /// The characters.
        /// </summary>
        List<TutorialCharacter> _characters;

        /// <summary>
        /// The dialogs.
        /// </summary>
        List<TutorialDialog> _dialogs;

        /// <summary>
        /// Awake this instance.
        /// </summary>
        public override void Awake()
        {
            base.Awake();
            _characters = new List<TutorialCharacter>();
            _dialogs = new List<TutorialDialog>();
            if (CharacterParent == null)
                CharacterParent = transform;
            if (DialogParent == null)
                DialogParent = transform;
        }

        /// <summary>
        /// Shows the specified character.
        /// </summary>
        /// <param name="characterName">Character name.</param>
        /// <param name="dialogPanelName">Dialog panel name.</param>
        /// <param name="callback">Callback.</param>
        /// <param name="ignoreAnimation">If true the character is show instantly.</param>
        public void ShowCharacter(string characterName, string dialogPanelName, Action callback)
        {
            if (!string.IsNullOrEmpty(dialogPanelName))
            {
                TutorialDialog dialog = _dialogs.FirstOrDefault(x =>  x.gameObject.name.Contains(dialogPanelName));
                if (dialog == null)
                {
                    GameObject dialogGO = Pool.GetObjectForType(dialogPanelName, DialogParent);
                    dialog = dialogGO.GetComponent<TutorialDialog>();
                    dialog.Setup();

                    _dialogs.Add(dialog);
                }
            }


            TutorialCharacter character = _characters.FirstOrDefault(x =>  x.gameObject.name.Contains(characterName));
            if (character != null)
                character.ShowCharacter(() => callback?.Invoke());
            else
            {
                GameObject characterGO = Pool.GetObjectForType(characterName, CharacterParent);
                if (characterGO != null)
                {
                    character = characterGO.GetComponent<TutorialCharacter>();
                    character.Setup();

                    _characters.Add(character);

                    character.ShowCharacter(() => callback?.Invoke());
                }
                else
                {
                    LogHelper.LogWarning("Character pooled is null");
                }
            }
        }

        /// <summary>
        /// Hides the specified character.
        /// </summary>
        /// <param name="characterName">Character name.</param>
        /// <param name="callback">Callback.</param>
        /// <param name="ignoreAnimation">If true the character is hidden instanlty</param>
        public void HideCharacter(string characterName, Action callback)
        {
            TutorialCharacter character = _characters.FirstOrDefault(x => x.gameObject.name.Contains(characterName));
            if (character != null)
                character.HideCharacter(() => callback?.Invoke());
            else
                LogHelper.LogWarning("Character doesn't exists");
        }

        /// <summary>
        /// Plays the character animation with dialog if needed.
        /// </summary>
        /// <param name="characterName">Character name.</param>
        /// <param name="animationModel">Animation model.</param>
        /// <param name="dialogPanelName">Dialog panel name.</param>
        /// <param name="dialogLocalizationKey">Dialog localization key.</param>
        /// <param name="nextDelay">Next delay.</param>
        /// <param name="ignoreAnimation">If true the dialog is shown instantly.</param>
        public void PlayAnimation(string characterName, TutorialCharacterAnimationModel animationModel, string dialogPanelName = null, string dialogLocalizationKey = null, float nextDelay = 0)
        {
            TutorialCharacter character = _characters.FirstOrDefault(x =>  x.gameObject.name.Contains(characterName));
            if (character != null)
            {
                character.PlayAnimation(animationModel);
            }
            else
                LogHelper.LogWarning(string.Format("Character {0} doesn't exists", characterName));

            if (!string.IsNullOrEmpty(dialogPanelName) && !string.IsNullOrEmpty(dialogLocalizationKey))
            {
                ShowDialog(dialogPanelName, dialogLocalizationKey, nextDelay);
            }
        }

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="dialogPanelName">Dialog panel name.</param>
        /// <param name="dialogLocalizationKey">Dialog localization key.</param>
        /// <param name="nextDelay">Next delay.</param>
        /// <param name="ignoreAnimation">If true the dialog is shown instantly.</param>
        public void ShowDialog(string dialogPanelName, string dialogLocalizationKey, float nextDelay)
        {
            TutorialDialog dialog = _dialogs.FirstOrDefault(x => x.gameObject.name.Contains(dialogPanelName));
            if (dialog == null)
            {
                GameObject dialogGO = Pool.GetObjectForType(dialogPanelName, DialogParent);
                if (dialogGO != null)
                {
                    dialog = dialogGO.GetComponent<TutorialDialog>();
                    dialog.Setup();

                    _dialogs.Add(dialog);
                }
            }
            if (dialog != null)
            {
                dialog.ShowDialog(dialogLocalizationKey, nextDelay);
                TutorialsEvents.DialogShown(dialog);
            }
            else
            {
                LogHelper.LogWarning(string.Format("Dialog {0} doesn't exists", dialogPanelName));
            }
            
        }

        /// <summary>
        /// Hides the dialog.
        /// </summary>
        /// <param name="dialogPanelName">Dialog panel name.</param>
        /// <param name="callback">Callback.</param>
        /// <param name="ignoreAnimation">If true the dialog is hidden instantly.</param>
        public void HideDialog (string dialogPanelName, Action callback = null)
        {
            TutorialDialog dialog = _dialogs.FirstOrDefault(x => x.gameObject.name.Contains(dialogPanelName));
            if (dialog != null)
            {
                dialog.HideDialog(callback);
            }
            else
                LogHelper.LogWarning(string.Format("Dialog {0} doesn't exists", dialogPanelName));
        }
    }
}

