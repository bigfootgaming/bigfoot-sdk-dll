﻿using System;
using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.SceneManagement;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Manager in charge to show the appropriate information & handle the loading screen
	/// </summary>
	public class LoadingScreenManager : Singleton<LoadingScreenManager>
	{
		/// <summary>
		/// Flag to know if we are waiting for logos
		/// </summary>
		[HideInInspector]
		public bool WaitForLogos = false;
		
		/// <summary>
		/// Loading screen's background
		/// </summary>
		public GameObject Background;

		/// <summary>
		/// The label for the player id
		/// </summary>
		public TextMeshProUGUI PlayerIdLabel;

		/// <summary>
		/// The label for the environment
		/// </summary>
		public TextMeshProUGUI EnvironmentLabel;

		/// <summary>
		/// The weight for the dependency manager's percentage loaded
		/// </summary>
		public float DependencyPercentageWeight = 0.7f;

		/// <summary>
		/// The weight for the scene loading's percentage loaded
		/// </summary>
		public float SceneloadPercentageWeight = 0.3f;
		
		/// <summary>
		/// The progress.
		/// </summary>
		public ProgressBar Progress;

		/// <summary>
		/// Should we show the Loading Screen every time a scene is loaded
		/// </summary>
		public bool AutoShowOnSceneLoad = true;
		
		/// <summary>
		/// Should we hide the Loading Screen every time a scene is loaded
		/// </summary>
		public bool AutoHideOnSceneDoneLoading = false;

		/// <summary>
		/// The environment used in the build
		/// </summary>
		protected string _environment = "";
		
		/// <summary>
		/// The player id
		/// </summary>
		protected string _playerId = "";

		/// <summary>
		/// The max percentage loaded (so the bar never goes backwards)
		/// </summary>
		protected float _maxPercentageLoaded;

		/// <summary>
		/// Flag to know if it's the first load
		/// </summary>
		private bool _isFirstLoad = true;

		/// <summary>
		/// Flag to know if the loading screen is visible
		/// </summary>
		protected bool _isVisible;
		
		/// <summary>
		/// Subscribe to events
		/// </summary>
		protected virtual void OnEnable()
		{
			SceneEvents.OnStartLoadingScene += OnStartLoadingScene;
			SceneEvents.OnSceneLoaded += OnSceneLoaded;
			PlayerEvents.OnPlayerInitialized += PlayerInitialized;
			PlayerManager.OnPlayerIdSet += PlayerAuthenticated;
		}

		/// <summary>
		/// Unsubscribe from Events
		/// </summary>
		protected virtual void OnDisable()
		{
			SceneEvents.OnStartLoadingScene -= OnStartLoadingScene;
			SceneEvents.OnSceneLoaded -= OnSceneLoaded;
			PlayerEvents.OnPlayerInitialized -= PlayerInitialized;
			PlayerManager.OnPlayerIdSet -= PlayerAuthenticated;
		}

		void Start()
		{
			// Cache the environment used
			_environment = SdkManager.Instance.Configuration.Environment;
				
			// Populate the environment's label
			PopulateEnvironmentLabel();

			// Set visibility to true
			_isVisible = true;
		}

		private void ResetUI()
		{
			_maxPercentageLoaded = 0;
			
			if (Progress != null)
			{
				Progress.UpdateValues(0, 1f, false);
			}
		}
		
		/// <summary>
		/// Update Loading Progress
		/// </summary>
		protected virtual void Update()
		{
			// Dont update the bar if we are waiting for the logos
			if (WaitForLogos || !_isVisible)
				return;
				
			// Calculate the percentage based on the weights
			float scenePercent = BigfootSceneManager.PercentageLoaded * SceneloadPercentageWeight;
			if (float.IsNaN(scenePercent))
				scenePercent = 0;

			float depPercent = DependencyManager.Instance.PercentageLoaded * DependencyPercentageWeight;
			if (float.IsNaN(depPercent))
				depPercent = 0;

			// Make sure the total percentage never goes backwards
			float totalPercentage = scenePercent + depPercent;
			if (totalPercentage < _maxPercentageLoaded)
			{
				totalPercentage = _maxPercentageLoaded;
			}
			else
			{
				_maxPercentageLoaded = totalPercentage;
			}
			
			// Update the progress bar's fill & text
			if (Progress != null)
			{
				Progress.UpdateValues(totalPercentage, 1f, true);
			}

			// Populate the Loading Progress's Label
			PopulateLoadingProgressLabel(totalPercentage);
		}
		
		/// <summary>
		/// OnStartLoadingScene Event
		/// </summary>
		/// <param name="obj"></param>
		private void OnStartLoadingScene(string obj)
		{
			// Reset the max percentage loaded
			_maxPercentageLoaded = 0;
			
			if (AutoShowOnSceneLoad)
				ToggleBackground(true);
		}

		/// <summary>
		/// OnSceneLoaded Events
		/// </summary>
		/// <param name="obj"></param>
		private void OnSceneLoaded(string obj)
		{
			
			if (AutoHideOnSceneDoneLoading)
				ToggleBackground(false);
		}
		
		/// <summary>
		/// Sets the Background Enabled/Disabled
		/// </summary>
		/// <param name="visible">Is the background visible?</param>
		public virtual void ToggleBackground(bool visible)
		{
			_isVisible = visible;
			
			if(Background != null)
				Background.SetActive(visible);

			// If we are hiding the Background, make sure we reset the values for the next time
			if (!visible)
			{
				ResetUI();

				// If it's the first time we are loading this, track how long it took until the loading screen was hidden.
				if (_isFirstLoad)
				{
					_isFirstLoad = false;
					AnalyticsManager.Instance.TrackCustomEvent("TimeUntilLoadingHidden", new Dictionary<string, object>() {{"Time", Time.realtimeSinceStartup}});
				}
			}
		}
		
		/// <summary>
		/// The player initialized
		/// </summary>
		void PlayerInitialized()
		{
			string playerId = PlayerManager.Instance.GetPlayerId();
			
			if (!string.IsNullOrEmpty(playerId))
			{
				// Cache the playerId
				_playerId = playerId;
				
				// Populate Player Id's label
				PopulatePlayerIdLabel();
			}
		}

		/// <summary>
		/// The player authenticated
		/// </summary>
		void PlayerAuthenticated(string playerId)
		{
			if (!string.IsNullOrEmpty(playerId))
			{
				// Cache the playerId
				_playerId = playerId;
				
				// Populate Player Id's label
				PopulatePlayerIdLabel();
			}
		}

		/// <summary>
		/// Populates the player id label
		/// </summary>
		protected virtual void PopulatePlayerIdLabel()
		{
			if (PlayerIdLabel != null)
				PlayerIdLabel.text = _playerId;
		}
		
		/// <summary>
		/// Populates the environment label
		/// </summary>
		protected virtual void PopulateEnvironmentLabel()
		{
			if (EnvironmentLabel != null)
				EnvironmentLabel.text = string.Format("v{0} {1}", Application.version, _environment);
		}

		/// <summary>
		/// Populates the Loading Progress's label
		/// </summary>
		/// <param name="progress"></param>
		protected virtual void PopulateLoadingProgressLabel(float progress)
		{
			if (Progress != null)
				Progress.SetLabelValue(progress.ToString("P"));
		}
    }
}