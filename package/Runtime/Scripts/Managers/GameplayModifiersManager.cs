using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using System.Linq;
using UnityEngine;

namespace BigfootSdk
{
	/// <summary>
    /// Gameplay modifiers manager.
    /// </summary>
	public partial class GameplayModifiersManager : Singleton <GameplayModifiersManager>
	{
		/// <summary>
		/// Event for when a list of modifiers are applied
		/// </summary>
		public static Action OnGameplayModifiersApplied;

        /// <summary>
        /// Event for when a modifier is removed
        /// </summary>
        public static Action OnGameplayModifiersRemoved;

        /// <summary>
        /// The applied modifiers.
        /// </summary>
        public List<GameplayModifierModel> AppliedModifiers = new List<GameplayModifierModel>();

        protected void OnEnable()
        {
	        PlayerManager.OnItemsAdded += NewItemsAdded;
        }

        protected void OnDisable()
        {
	        PlayerManager.OnItemsAdded -= NewItemsAdded;
        }

        /// <summary>
        /// Used to apply a list of modifiers
        /// </summary>
        /// <param name="gameplayModifiers">List of modifiers to apply</param>
        public void ApplyGameplayModifiers(List<GameplayModifierModel> gameplayModifiers) 
        {
			if (gameplayModifiers == null) 
            {
				LogHelper.LogWarning("Unable to apply gameplay modifiers. Null list of modifiers");
				return;
			}

			// Apply each callback from the list
			for (int i = 0; i < gameplayModifiers.Count; i++)
            {
				if (gameplayModifiers[i] == null) 
                {
					continue;
				}

				gameplayModifiers[i].Apply();
			}

            // Add the applied modifiers
            AppliedModifiers.AddRange(gameplayModifiers);

            // Run the callback after all modifiers are applied
            OnGameplayModifiersApplied?.Invoke();
		}

        /// <summary>
        /// Applies the gameplay modifier.
        /// </summary>
        /// <param name="gameplayModifier">Gameplay modifier.</param>
        public void ApplyGameplayModifier(GameplayModifierModel gameplayModifier)
        {
            if (gameplayModifier == null)
            {
                LogHelper.LogWarning("Unable to apply gameplay modifier. Null config for modifier");
                return;
            }

            // Apply the modifier
            gameplayModifier.Apply();

            // Add it to the applied modifiers
            AppliedModifiers.Add(gameplayModifier);

            // Run the callback after the modifier is applied
            OnGameplayModifiersApplied?.Invoke();
        }
        
        /// <summary>
        /// Removes a gameplay modifier.
        /// </summary>
        /// <param name="gameplayModifier">Gameplay modifier.</param>
        public void RemoveGameplayModifiers(List<GameplayModifierModel> gameplayModifiers) 
        {
	        if (gameplayModifiers == null) 
	        {
		        LogHelper.LogWarning("Unable to remove gameplay modifiers. Null list of modifiers");
		        return;
	        }

	        // Apply each callback from the list
	        for (int i = 0; i < gameplayModifiers.Count; i++)
	        {
		        if (gameplayModifiers[i] == null) 
		        {
			        continue;
		        }

		        // Remove the modifier
		        gameplayModifiers[i].Undo();
		        
		        // Remove it from the applied modifiers
		        AppliedModifiers.Remove(gameplayModifiers[i]);
	        }

	        // Run the callback after the modifier is applied
	        OnGameplayModifiersRemoved?.Invoke();
        }

        /// <summary>
        /// Removes a gameplay modifier.
        /// </summary>
        /// <param name="gameplayModifier">Gameplay modifier.</param>
        public void RemoveGameplayModifier(GameplayModifierModel gameplayModifier)
        {
            if (gameplayModifier == null)
            {
                LogHelper.LogWarning("Unable to apply gameplay modifier. Null config for modifier");
                return;
            }
            
            // Remove the modifier
            gameplayModifier.Undo();

            // Remove it from the applied modifiers
            AppliedModifiers.Remove(gameplayModifier);

            // Run the callback after the modifier is applied
            OnGameplayModifiersRemoved?.Invoke();
        }
        
        /// <summary>
        /// Removes all Modifiers of a type
        /// </summary>
        /// <param name="tag">The tag to look for</param>
        public void RemoveGameplayModifiersWithTag(string tag)
        {
	        List<GameplayModifierModel> modifiers = AppliedModifiers.Where(mod => mod.Tag == tag).ToList();
	        
	       foreach(GameplayModifierModel gameplayModifier in modifiers)
	        {
		        // Remove the modifier
		        gameplayModifier.Undo();

		        // Remove it from the applied modifiers
		        AppliedModifiers.Remove(gameplayModifier);
	        }

	        // Run the callback after the modifier is applied
	        OnGameplayModifiersRemoved?.Invoke();
        }

        /// <summary>
        /// Remove all of the applied gameplay modifiers
        /// </summary>
        public void RemoveAllGameplayModifiers()
        {
	        foreach(GameplayModifierModel gameplayModifier in AppliedModifiers)
	        {
		        // Remove the modifier
		        gameplayModifier.Undo();
	        }
	        
	        // Reset all applied modifiers
	        AppliedModifiers.Clear();

	        // Run the callback after the modifier is applied
	        OnGameplayModifiersRemoved?.Invoke();
        }

        void NewItemsAdded(List<PlayerItemModel> playerItems)
        {
	        // Grab all of the 
	        List<GameplayModifierItemModel> gameplayModifierItems = PlayerManager.Instance.GetModifiersItemsFromPlayerItems(playerItems);

	        foreach (GameplayModifierItemModel itemModel in gameplayModifierItems)
	        {
		        ApplyGameplayModifiers(itemModel.Modifiers);
	        }
        }
    }
}