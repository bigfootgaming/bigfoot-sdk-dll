﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk.Tutorials
{
    /// <summary>
    /// Tutorials manager.
    /// </summary>
	[ExecuteInEditMode]
	public class TutorialsManager : LoadableSingleton<TutorialsManager>
	{
        /// <summary>
        /// If <see langword="true"/> tutorials won't be used. (Ignored in production)
        /// </summary>
        public bool SkipAllTutorials;

        /// <summary>
        /// The tutorials.
        /// </summary>
		public TutorialHandler[] Tutorials;

        

        /// <summary>
        /// The tutorial progresses.
        /// </summary>
		protected List<TutorialProgressModel> TutorialProgresses;

        /// <summary>
        /// Gets a value indicating whether is any tutorial running.
        /// </summary>
        /// <value><c>true</c> if is any tutorial running; otherwise, <c>false</c>.</value>
		public bool IsAnyTutorialRunning 
		{
			get
			{
				return _isAnyTutorialRunning;
			}
		}

        /// <summary>
        /// Gets the tutorial running.
        /// </summary>
        /// <value>The tutorial running.</value>
		public string TutorialRunning
		{
			get
			{
				if (_isAnyTutorialRunning)
					return _tutorialRunning;
				return null;
			}
		}
        
        /// <summary>
        /// Gets the stage of the tutorial running.
        /// </summary>
        /// <value>The stage running.</value>
        public string StageRunning
        {
	        get
	        {
		        if (_isAnyTutorialRunning)
			        return _stageRunning;
		        return null;
	        }
        }

        /// <summary>
        /// Is any tutorial running.
        /// </summary>
		protected bool _isAnyTutorialRunning;

        /// <summary>
        /// The tutorial running.
        /// </summary>
        protected string _tutorialRunning;
        
        /// <summary>
        /// The stage running.
        /// </summary>
        protected string _stageRunning;

        /// <summary>
        /// Load the tutorial progress.
        /// </summary>
        public override void StartLoading()
        {
            string tutorialsString = PlayerManager.Instance.GetFromPlayerData("tutorial_progress", GameModeConstants.MAIN_GAME);
            TutorialProgresses = JsonHelper.DesarializeObject<List<TutorialProgressModel>>(tutorialsString);

            if (TutorialProgresses == null)
            {
                TutorialProgresses = new List<TutorialProgressModel>();
            }
            
            foreach (var t in Tutorials)
            {
	            if (!TutorialProgresses.Exists(x => x.TutorialId == t.TutorialId))
	            {
		            TutorialProgresses.Add(new TutorialProgressModel()
		            {
			            TutorialId = t.TutorialId,
			            CurrentStage = 0
		            });
	            }
            }
            
            LoadTutorialProgress();

#if !ENVIRONMENT_PROD
            foreach (var tutorial in Tutorials)
            {
	            if (tutorial.ShouldSkipTutorial || SkipAllTutorials)
	            {
		            tutorial.SkipTutorial(); 
	            }
            }
#endif

            FinishedLoading(true);
        }

        /// <summary>
        /// Saves the tutorial progress.
        /// </summary>
        protected virtual void SaveTutorialProgress()
        {
	        PlayerManager.Instance.SetOldPlayerFlag();
            DataUpdateModel data = new DataUpdateModel(JsonHelper.SerializeObject(TutorialProgresses), false);
            PlayerManager.Instance.SetPlayerData("tutorial_progress", data, GameModeConstants.MAIN_GAME);
        }

        /// <summary>
        /// Loads the tutorial progress into handlers.
        /// </summary>
        protected virtual void LoadTutorialProgress()
        {
            //Send all our progress info to the tutorial controllers.
            foreach (var t in TutorialProgresses)
            {
                var tutHandler = Tutorials.FirstOrDefault(x => x.TutorialId == t.TutorialId);
                tutHandler?.LoadTutorialProgress(t);
            }
        }

        

        /// <summary>
        /// Starts the stage.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stageName">Stage name.</param>
        /// <param name="triggeredBy">Triggered by.</param>
        public virtual void StartStage(string tutorialId, string stageName, TutorialTrigger triggeredBy)
        {
	        var tutHandler = GetTutorial(tutorialId);
	        if (tutHandler != null && tutHandler.StartStage(stageName, triggeredBy))
	        {
		        _isAnyTutorialRunning = true;
		        _tutorialRunning = tutorialId;
		        _stageRunning = stageName;
	        }
        }

        /// <summary>
        /// Stage has completed.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stageName">Stage name.</param>
        public virtual void StageCompleted(string tutorialId, string stageName)
        {
	        var tutHandler = GetTutorial(tutorialId);
	        tutHandler?.StageCompleted(stageName);
        }

        /// <summary>
        /// Skips the tutorial.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        public virtual void SkipTutorial(string tutorialId)
		{
			var tutHandler = Tutorials.FirstOrDefault(x => x.TutorialId.Equals(tutorialId));
			if (tutHandler != null && tutHandler.SkipTutorial())
			{
				UpdateTutorialProgress(tutHandler.TutorialId, tutHandler.Stages.Length + 1);
				SaveTutorialProgress();
			}
		}

        /// <summary>
        /// Resets the tutorial
        /// </summary>
        /// <param name="tutorialId"></param>
        public virtual void ResetTutorial(string tutorialId)
        {
	        var tutHandler = Tutorials.FirstOrDefault(x => x.TutorialId.Equals(tutorialId));
	        if (tutHandler != null && tutHandler.ResetTutorial())
	        {
		        UpdateTutorialProgress(tutHandler.TutorialId, 0);
		        SaveTutorialProgress();
	        }
        }

        /// <summary>
        /// Gets the amount of stages before this tutorial
        /// </summary>
        /// <param name="tutorialId"></param>
        /// <returns></returns>
        public virtual int GetStartingTutorialIndex(string tutorialId)
        {
	        int index = 0;
	        for (int i = 0; i < Tutorials.Length; i++)
	        {
		        if (Tutorials[i].TutorialId != tutorialId)
		        {
			        index += Tutorials[i].Stages.Length;
		        }
		        //if we reach the current tutorial, stop the loop
				else
					break;
	        }

	        return index;
        }

        /// <summary>
        /// Returns true if is the last tutorial
        /// </summary>
        /// <param name="tutorialId"></param>
        /// <returns></returns>
        public virtual bool IsLastTutorial(string tutorialId)
        {
	        return Tutorials[Tutorials.Length - 1].TutorialId == tutorialId;
        }

        /// <summary>
        /// Updates the tutorial progress.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stage">Stage.</param>
        protected virtual void UpdateTutorialProgress(string tutorialId, int stage)
        {
            TutorialProgressModel tutProgress = TutorialProgresses.FirstOrDefault(x => x.TutorialId == tutorialId);
            if (tutProgress != null)
                tutProgress.CurrentStage = stage;
            else
                TutorialProgresses.Add(new TutorialProgressModel() { TutorialId = tutorialId, CurrentStage = stage });
        }

        /// <summary>
        /// Tutorial has started.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
		public virtual void TutorialStarted (string tutorialId)
        {
            TutorialsEvents.TutorialStarted(tutorialId);
        }

        /// <summary>
        /// Tutorial has been completed.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
		public virtual void TutorialCompleted (string tutorialId)
		{
			_isAnyTutorialRunning = false;
			_tutorialRunning = null;
			_stageRunning = null;
            TutorialsEvents.TutorialCompleted(tutorialId);
		}

        /// <summary>
        /// Saves the tutorial progress.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stage">Stage.</param>
        public virtual void SaveTutorialProgress (string tutorialId, int stage)
        {
	        UpdateTutorialProgress(tutorialId, stage);
            SaveTutorialProgress();
        }

        /// <summary>
        /// Is specified tutorial completed.
        /// </summary>
        /// <returns><c>true</c>, if the tutorial was completed, <c>false</c> otherwise.</returns>
        /// <param name="tutId">Tut identifier.</param>
		public virtual bool IsCompleted(string tutId)
		{
			var tutHandler = GetTutorial(tutId);
			if(tutHandler != null)
            {
                return tutHandler.IsCompleted();               
            }
            return false;
		}

        /// <summary>
        /// Is the stage completed?
        /// </summary>
        /// <param name="tutId">Tut identifier.</param>
        /// <param name="stageNumber">the stage we want to check agains</param>
        /// <returns>if it's completed</returns>
        public virtual bool IsStageCompleted(string tutId, int stageNumber)
        {
	        var tutHandler = GetTutorial(tutId);
	        if(tutHandler != null)
	        {
		        return tutHandler.IsStageCompleted(stageNumber);               
	        }
	        return false;
        }

        /// <summary>
        /// Are all the tutorials completed.
        /// </summary>
        /// <returns><c>true</c>, if all the tutorials were completed, <c>false</c> otherwise.</returns>
        public virtual bool IsCompleted()
        {
            foreach (TutorialHandler tutHandler in Tutorials)
                if (!tutHandler.IsCompleted())
                    return false;
            return true;
        }

        /// <summary>
        /// Gets the tutorial.
        /// </summary>
        /// <returns>The tutorial.</returns>
        /// <param name="tutorialId">Tutorial identifier.</param>
        public virtual TutorialHandler GetTutorial(string tutorialId)
		{
            return Tutorials.FirstOrDefault(x => x.TutorialId == tutorialId);
		}
	}
}