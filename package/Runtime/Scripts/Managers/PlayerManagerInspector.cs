﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Globalization;
using BigfootSdk.Helpers;
using CodeStage.AntiCheat.ObscuredTypes;
using Sirenix.OdinInspector;
using Sirenix.Utilities.Editor;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Partial class of the PlayerManager to handle all the inspector visuals in the editor
    /// </summary>
    public partial class PlayerManager
    {
        #region PlayerID

        /// <summary>
        /// The played id field to show in the inspector
        /// </summary>
        [ShowInInspector, ReadOnly, BoxGroup("Player Info"), PropertyOrder(-2)]
        private string playerId
        {
            get { return Player?.PlayerId; }
        }

        /// <summary>
        /// Button in the inspector to copy the id to the clipboard
        /// </summary>
        [Button("Copy to clipboard"), BoxGroup("Player Info"), PropertyOrder(-2)]
        private void CopyPlayerId()
        {
            Clipboard.Copy(playerId);
        }

        #endregion
        /// <summary>
        /// The last save field to show in the inspector
        /// </summary>
        [ShowInInspector, ReadOnly, BoxGroup("Player Info"), PropertyOrder(-1)]
        private long lastSave
        {
            get { return long.Parse(Player?.LastSave.ToString(), CultureInfo.InvariantCulture); }
        }
        
        #region LastSave
        
        
        #endregion

        #region Currencies

        /// <summary>
        /// Dictionary containing all the player currencies in a <string,string> format so it can be readable for the inspector 
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Currency"), DictionaryDrawerSettings(KeyLabel = "Name", ValueLabel = "Value", IsReadOnly = true), PropertyOrder(1)]
        private Dictionary<string, string> Currencies
        {
            get { return GetPlayerCurrencyInfo(); }
        }

        /// <summary>
        /// String field for the currency key to be modified
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Currency/Modify"), PropertyOrder(0)]
        private string currencyKey = string.Empty;

        /// <summary>
        /// Int field for the currency value to be modified
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Currency/Modify"), PropertyOrder(0)]
        private int currencyValue = 0;

        /// <summary>
        /// Button for the inspector to add the new currency value wanted for the user/dev
        /// </summary>
        [Button("Modify"), FoldoutGroup("Player Info/Currency/Modify"), PropertyOrder(0)]
        private void AddCurrency()
        {
            if (currencyValue == 0 || string.IsNullOrEmpty(currencyKey))
                return;

            ChangeCurrency(currencyKey, currencyValue, GameModeManager.CurrentGameMode);
        }

        /// <summary>
        /// Method used to get the current value of all the currencies in the game
        /// </summary>
        private Dictionary<string, string> GetPlayerCurrencyInfo()
        {
            Dictionary<string, string> currencyInfo = null;

            PlayerInfoModel playerInfo = GetPlayerInfoModel(GameModeManager.CurrentGameMode);

            if (playerInfo == null || playerInfo.Currencies == null)
            {
                return currencyInfo;
            }

            foreach (KeyValuePair<string, ObscuredInt> entry in playerInfo.Currencies)
            {
                if (currencyInfo == null)
                {
                    currencyInfo = new Dictionary<string, string>();
                }

                currencyInfo.Add(string.IsNullOrEmpty(entry.Key) ? "Null or empty key" : entry.Key, entry.Value.ToString());
            }

            return currencyInfo;
        }

        #endregion

        #region PlayerData

        /// <summary>
        /// Dictionary containing all the player custom data in a <string,string> format so it can be readable for the inspector 
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Custom Data"), DictionaryDrawerSettings(KeyLabel = "Data", ValueLabel = "Value", IsReadOnly = true), PropertyOrder(1)]
        private Dictionary<string, string> PlayerData
        {
            get { return GetPlayerData(); }
        }

        /// <summary>
        /// String field for the custom data key to be modified
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Custom Data/Modify"), PropertyOrder(0)]
        private string playerDataKey = string.Empty;

        /// <summary>
        /// String field for the custom data value to be modified
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Custom Data/Modify"), PropertyOrder(0)]
        private string playerDataValue = String.Empty;

        /// <summary>
        /// Method used to get the current value of all the custom data in the game for this player
        /// </summary>
        private Dictionary<string, string> GetPlayerData()
        {
            Dictionary<string, string> dataInfo = null;

            PlayerInfoModel playerInfo = GetPlayerInfoModel(GameModeManager.CurrentGameMode);

            if (playerInfo == null || playerInfo.Data == null)
            {
                return dataInfo;
            }

            foreach (KeyValuePair<string, ObscuredString> entry in playerInfo.Data)
            {
                if (dataInfo == null)
                {
                    dataInfo = new Dictionary<string, string>();
                }

                dataInfo.Add(string.IsNullOrEmpty(entry.Key) ? "Null or empty key" : entry.Key, entry.Value == null ? "Null value" : entry.Value.ToString());
            }

            return dataInfo;
        }

        /// <summary>
        /// Button for the inspector to add the new custom data value wanted for the user/dev
        /// </summary>
        [Button("Set Data"), FoldoutGroup("Player Info/Custom Data/Modify"), PropertyOrder(0)]
        private void PushPlayerData()
        {
            if (string.IsNullOrEmpty(playerDataKey) || string.IsNullOrEmpty(playerDataValue))
            {
                return;
            }

            SetPlayerData(playerDataKey, new DataUpdateModel(playerDataValue), GameModeManager.CurrentGameMode);
        }

        #endregion

        #region Timers

        /// <summary>
        /// Dictionary containing all the player timers in a <string,string> format so it can be readable for the inspector 
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Timers"), DictionaryDrawerSettings(KeyLabel = "Timer Name", ValueLabel = "Timer Value", IsReadOnly = true), PropertyOrder(1)]
        private Dictionary<string, string> Timers
        {
            get { return GetTimers(); }
        }

        /// <summary>
        /// String field for the timer key to be modified
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Timers/Modify"), PropertyOrder(0)]
        private string timerKey = String.Empty;

        /// <summary>
        /// String field for the timer value to be modified
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Timers/Modify"), PropertyOrder(0)]
        private long timerValue = 0;

        /// <summary>
        /// Method used to get the current value of all the timers in the game for this player
        /// </summary>
        private Dictionary<string, string> GetTimers()
        {
            Dictionary<string, string> timers = null;

            PlayerInfoModel playerInfo = GetPlayerInfoModel(GameModeManager.CurrentGameMode);

            if (playerInfo == null || playerInfo.Timers == null)
            {
                return timers;
            }

            foreach (KeyValuePair<string, ObscuredLong> entry in playerInfo.Timers)
            {
                if (timers == null)
                {
                    timers = new Dictionary<string, string>();
                }

                timers.Add(string.IsNullOrEmpty(entry.Key) ? "Null or empty key" : entry.Key, entry.Value.ToString());
            }

            return timers;
        }

        /// <summary>
        /// Button for the inspector to add the new timer value wanted for the user/dev
        /// </summary>
        [Button("Set Timer"), FoldoutGroup("Player Info/Timers/Modify"), PropertyOrder(0)]
        private void PushTimer()
        {
            if (string.IsNullOrEmpty(timerKey))
            {
                return;
            }

            SetTimer(timerKey, timerValue, GameModeManager.CurrentGameMode);
        }

        #endregion

        #region Counters

        /// <summary>
        /// Dictionary containing all the player counters in a <string,string> format so it can be readable for the inspector 
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Counters"), DictionaryDrawerSettings(KeyLabel = "Counter Name", ValueLabel = "Counter Value", IsReadOnly = true), PropertyOrder(1)]
        private Dictionary<string, string> Counters
        {
            get { return GetCounters(); }
        }

        /// <summary>
        /// String field for the counter key to be modified
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Counters/Modify"), PropertyOrder(0)]
        private string counterKey = String.Empty;

        /// <summary>
        /// Int field for the counter value to be modified
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Counters/Modify"), PropertyOrder(0)]
        private int counterValue = 0;

        /// <summary>
        /// Method used to get the current value of all the counters in the game for this player
        /// </summary>
        private Dictionary<string, string> GetCounters()
        {
            Dictionary<string, string> counters = null;

            PlayerInfoModel playerInfo = GetPlayerInfoModel(GameModeManager.CurrentGameMode);

            if (playerInfo == null || playerInfo.Counters == null)
            {
                return counters;
            }

            foreach (KeyValuePair<string, ObscuredInt> entry in playerInfo.Counters)
            {
                if (counters == null)
                {
                    counters = new Dictionary<string, string>();
                }

                counters.Add(string.IsNullOrEmpty(entry.Key) ? "Null or empty key" : entry.Key, entry.Value.ToString());
            }

            return counters;
        }

        /// <summary>
        /// Button for the inspector to add the new counter value wanted for the user/dev
        /// </summary>
        [Button("Set Counter"), FoldoutGroup("Player Info/Counters/Modify"), PropertyOrder(0)]
        private void PushCounter()
        {
            if (string.IsNullOrEmpty(counterKey))
            {
                return;
            }

            SetCounter(counterKey, counterValue, GameModeManager.CurrentGameMode);
        }

        #endregion

        #region Flags

        /// <summary>
        /// Dictionary containing all the player flags in a <string,string> format so it can be readable for the inspector 
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Flags"), DictionaryDrawerSettings(KeyLabel = "Flag Name", ValueLabel = "Flag Value", IsReadOnly = true), PropertyOrder(1)]
        private Dictionary<string, string> Flags
        {
            get { return GetFlags(); }
        }

        /// <summary>
        /// String field for the flags key to be modified
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Flags/Modify"), PropertyOrder(0)]
        private string flagKey = String.Empty;

        /// <summary>
        /// Bool field for the flag value to be modified
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Flags/Modify"), PropertyOrder(0)]
        private bool flagValue = false;

        /// <summary>
        /// Method used to get the current value of all the flags in the game for this player
        /// </summary>
        private Dictionary<string, string> GetFlags()
        {
            Dictionary<string, string> flags = null;

            PlayerInfoModel playerInfo = GetPlayerInfoModel(GameModeManager.CurrentGameMode);

            if (playerInfo == null || playerInfo.Flags == null)
            {
                return flags;
            }

            foreach (KeyValuePair<string, ObscuredBool> entry in playerInfo.Flags)
            {
                if (flags == null)
                {
                    flags = new Dictionary<string, string>();
                }

                flags.Add(string.IsNullOrEmpty(entry.Key) ? "Null or empty key" : entry.Key, entry.Value.ToString());
            }

            return flags;
        }

        /// <summary>
        /// Button for the inspector to add the new Flag value wanted for the user/dev
        /// </summary>
        [Button("Set Flag"), FoldoutGroup("Player Info/Flags/Modify"), PropertyOrder(0)]
        private void PushFlag()
        {
            if (string.IsNullOrEmpty(flagKey))
            {
                return;
            }

            SetFlag(flagKey, flagValue, GameModeManager.CurrentGameMode);
        }

        #endregion

        #region Items

        /// <summary>
        /// The list of items that the player owns in the current game mode
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Items"), PropertyOrder(1), TableList()]
        private List<InspectorPlayerItemModel> playerItems
        {
            get { return GetPlayerItems(); }
        }

        /// <summary>
        /// Method used to obtain all the player owned items
        /// </summary>
        private List<InspectorPlayerItemModel> GetPlayerItems()
        {
            List<InspectorPlayerItemModel> itemData = null;

            PlayerInfoModel playerInfo = GetPlayerInfoModel(GameModeManager.CurrentGameMode);
            if (playerInfo == null || playerInfo.Data == null)
            {
                return itemData;
            }

            foreach (PlayerItemModel entry in playerInfo.Items)
            {
                if (entry == null)
                {
                    continue;
                }

                if (itemData == null)
                {
                    itemData = new List<InspectorPlayerItemModel>();
                }

                itemData.Add(new InspectorPlayerItemModel(entry));
            }

            return itemData;
        }

        /// <summary>
        /// Field used to decide the amount of items to use in a transaction.
        /// </summary>
        [InfoBox("To add a new item via a transaction the 'Amount To Add should be greater than Zero'", InfoMessageType.Warning, VisibleIf = "AmountToAddLessToOne")]
        [ShowInInspector, FoldoutGroup("Player Info/Items/Modify"), PropertyOrder(0)]
        private static int amountToAdd = 0;

        /// <summary>
        /// Method used to check it the amount to add in a transaction is less or equal to zero
        /// </summary>
        private bool AmountToAddLessToOne()
        {
            return amountToAdd < 1;
        }

        /// <summary>
        /// The list of items that can be used in the items transaction
        /// </summary>
        [ShowInInspector, FoldoutGroup("Player Info/Items/Modify"), PropertyOrder(0), TableList()]
        private List<InspectorGameItemComponent> itemsList
        {
            get { return GetListOfGameItemsNames(); }
            set { itemsList = value; }
        }

        /// <summary>
        /// Method used to get a list of items that can be used in a transaction for this player in the inspector
        /// </summary>
        private List<InspectorGameItemComponent> GetListOfGameItemsNames()
        {
            if (ItemsManager.Instance == null)
            {
                return null;
            }

            List<BaseItemModel> gameItems = ItemsManager.Instance.GetAllItems();
            if (gameItems == null)
            {
                return null;
            }

            List<InspectorGameItemComponent> items = new List<InspectorGameItemComponent>();
            foreach (BaseItemModel baseItemModel in gameItems)
            {
                items.Add(new InspectorGameItemComponent()
                {
                    Name = baseItemModel.ItemName
                });
            }

            return items;
        }

        /// <summary>
        /// Private class to be use in the inspector to show all the items info in a clear way
        /// </summary>
        private class InspectorPlayerItemModel
        {
            public InspectorPlayerItemModel(PlayerItemModel itemModel)
            {
                this.itemModel = itemModel;
            }

            /// <summary>
            /// Player item model
            /// </summary>
            private PlayerItemModel itemModel;

            /// <summary>
            /// Identifier
            /// </summary>
            [ShowInInspector]
            public string Id
            {
                get { return (itemModel == null) ? "Null item model" : itemModel.Id.ToString(); }
            }

            /// <summary>
            /// Item name
            /// </summary>
            [ShowInInspector]
            public string Name
            {
                get { return (itemModel == null) ? "Null item model" : itemModel.Name.ToString(); }
            }

            /// <summary>
            /// Item type
            /// </summary>
            [ShowInInspector]
            public string Type
            {
                get { return (itemModel == null) ? "Null item model" : itemModel.Type.ToString(); }
            }

            /// <summary>
            /// Item class
            /// </summary>
            [ShowInInspector]
            public string Class
            {
                get { return (itemModel == null) ? "Null item model" : itemModel.Class.ToString(); }
            }

            /// <summary>
            /// Amount of items remaining
            /// </summary>
            [ShowInInspector]
            public string AmountRemaining
            {
                get { return (itemModel == null) ? "Null item model" : itemModel.AmountRemaining.ToString(); }
            }

            /// <summary>
            /// Custom data for this item in a <string, string> format so it can be easy readable for the inspector tool
            /// </summary>
            public Dictionary<string, string> Data
            {
                get { return GetItemData(); }
            }

            /// <summary>
            /// Method used to get the current value of the custom data for this player item
            /// </summary>
            private Dictionary<string, string> GetItemData()
            {
                Dictionary<string, string> itemData = null;

                if (itemModel == null)
                {
                    return itemData;
                }

                foreach (KeyValuePair<string, object> entry in itemModel.Data)
                {
                    if (itemData == null)
                    {
                        itemData = new Dictionary<string, string>();
                    }

                    itemData.Add(string.IsNullOrEmpty(entry.Key) ? "Null or empty key" : entry.Key, entry.Value == null ? "Null value" : JsonHelper.SerializeObject(entry.Value));
                }

                return itemData;
            }
        }

        private class InspectorGameItemComponent
        {
            /// <summary>
            /// The name of the item that can be applied in a transaction
            /// </summary>
            [ShowInInspector, HorizontalGroup(GroupID = "Item", LabelWidth = 100), ReadOnly]
            public string Name;

            /// <summary>
            /// Button for the inspector to add the new counter value wanted for the user/dev
            /// </summary>
            [Button("Send Transaction"), HorizontalGroup(GroupID = "Item"), DisableIf("NegativeAmountToAdd")]
            private void AddItem()
            {
                ItemTransaction transaction = new ItemTransaction(ItemsManager.Instance.GetItem(Name), amountToAdd);
                transaction.ApplyTransaction("", false);
            }

            /// <summary>
            /// Method used to check if there is any amount of items to add to the player manager.
            /// So we can disable the add button in the inspector
            /// </summary>
            private bool NegativeAmountToAdd()
            {
                return amountToAdd <= 0;
            }
        }

        #endregion
    }
}
#endif
