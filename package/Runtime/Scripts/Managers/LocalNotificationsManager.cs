﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Backend;
using BigfootSdk.Core.Idle;
using BigfootSdk.Helpers;
using System.IO;

#if UNITY_ANDROID

using Unity.Notifications.Android;
using BigfootSdk.Notifications.Android;
#elif UNITY_IOS
using Unity.Notifications.iOS;
using BigfootSdk.Notifications.iOS;
#endif
using UnityEngine;
using UnityEngine.Networking;

namespace BigfootSdk.Notifications
{
	/// <summary>
	/// Global notifications manager that serves as a wrapper for multiple platforms' notification systems.
	/// </summary>
	public class LocalNotificationsManager : LoadableSingleton<LocalNotificationsManager>
	{
		public string DefautBigPicture;
		// Minimum amount of time that a notification should be into the future before it's queued when we background.
		public static readonly TimeSpan MinimumNotificationTime = new TimeSpan(0, 0, 2);
	 
		/// <summary>
		/// Check to make the notifications manager automatically set badge numbers so that they increment.
		/// Schedule notifications with no numbers manually set to make use of this feature. 
		/// </summary>
		protected bool autoBadging = true;

		public bool AutoBadging
		{
			get => autoBadging;
			set => autoBadging = value;
		}
		
		/// <summary>
		/// Gets whether this manager has been initialized.
		/// </summary>
		public bool Initialized { get; protected set; }

		public UnityLocalNotificationService.OperatingMode Mode = UnityLocalNotificationService.OperatingMode.QueueAndClear;

		protected List<LocalNotificationModel> _localNotificationsModels = new List<LocalNotificationModel>();

		protected Dictionary<string, string> _bigPicturesLoaded = new Dictionary<string, string>();

		protected override void OnEnable()
		{
			IdleUserManager.OnIdleUserSaved += SetOnSaveLocalNotifications;
			base.OnEnable();
		}

		protected virtual void OnDisable()
		{
			IdleUserManager.OnIdleUserSaved -= SetOnSaveLocalNotifications;
		}

		/// <summary>
		/// Clean up platform object if necessary 
		/// </summary>
		protected virtual void OnDestroy()
		{
			_notificationService?.OnDestroy();
		}
		

		/// <summary>
		/// Respond to application foreground/background events.
		/// </summary>
		protected void OnApplicationFocus(bool hasFocus)
		{
			if (!Initialized)
			{
				return;
			}
			_notificationService?.OnApplicationFocus(hasFocus);
		}

#region BigFoot
        protected string _defaultChannelId = "game_channel0";

        public string DefaultChannelId
        {
	        get => _defaultChannelId;
	        set => _defaultChannelId = value;
        }

        protected string _defaultChannelName = "Default Game Channel";

        public string DefaultChannelName
        {
	        get => _defaultChannelName;
	        set => _defaultChannelName = value;
        }

        protected string _defaultChannelDescription = "Generic Notifications";

        public string DefaultChannelDescription
        {
	        get => _defaultChannelDescription;
	        set => _defaultChannelDescription = value;
        }

        protected ILocalNotificationService _notificationService;
        
        public override void StartLoading ()
        {
	        if (!Initialized)
	        {
		        _notificationService = new UnityLocalNotificationService();
		        Initialize();
		        StartCoroutine(UpdateServices());
	        }
	        
	        FinishedLoading(true);
        }

        IEnumerator UpdateServices()
        {
	        while (true)
	        {
				_notificationService?.Update();
		        
		        yield return new WaitForSeconds(1); 
	        }
        }

        public virtual void Reinitialize()
        {
	        Initialized = false;
	        Initialize();
        }

        /// <summary>
        /// Initialize the notifications manager.
        /// </summary>
        /// <param name="channels">An optional collection of channels to register, for Android</param>
        /// <exception cref="InvalidOperationException"><see cref="Initialize"/> has already been called.</exception>
        protected virtual void Initialize()
        {
	        if (Initialized)
	        {
		        return;
	        }

	        Initialized = true;

	        // Load the local notifications for this game
	        var config = ConfigHelper.LoadConfig<LocalNotificationsModel>("local_notifications");

	        if (config != null)
	        {
		        // THE LOCAL NOTIFICATION MODELS LOADED FROM THE CONFIG DATA 
				// WERE NEVER ADDED TO THE LIST OF LOCAL NOTIFICATION MODELS
				_localNotificationsModels = config.Notifications;

		        _notificationService?.Initialize(config.Notifications, new GameNotificationChannel(_defaultChannelId, _defaultChannelName, _defaultChannelDescription));

				if (config.CancelAllOnStart)
			        _notificationService?.CancelAllNotifications();
	        }
			
	        // Set the starting local notifications
	        SetInitialLocalNotifications();
        }

        
        
        

        public List<LocalNotificationModel> GetLocalNotifications()
        {
	        if (_notificationService is UnityLocalNotificationService unityService)
				return unityService.LocalNotifications;
	        
	        return null;
        }
        
		// UPDATED TO RECEIVE A STRING WITH THE BIG PICTURE NAME (AND EXTENSION) AS PARAMETER, INSTEAD OF A NOTIFICATION MODEL
		// REMOVED THE EXTRA RESPONSIBILITIES TO SET THE BIG PICTURE PATH ON THE NOTIFICATION MODEL
		// NOW, THE BIG PICTURE IS DOWNLOADED AND THE PATH IS MAPPED (WITH THE BIG PICTURE NAME AS ID, AND BEFORE DOWNLOAD THE PICTURE) IN THE LOADED PICTURES REGISTRY
		protected IEnumerator LoadBigPictureFromStreamingAssets(string bigPictureName)
        {
	        string streamingAssetPath = $"{Application.streamingAssetsPath}/Notifications/{bigPictureName}";
	        string savePath = $"{Application.persistentDataPath}/{bigPictureName}";
			
			_bigPicturesLoaded.Add(bigPictureName, savePath);

			using (UnityWebRequest webRequest = new UnityWebRequest(streamingAssetPath, UnityWebRequest.kHttpVerbGET))
	        {
		        webRequest.downloadHandler = new DownloadHandlerFile(savePath);

		        yield return webRequest.SendWebRequest();
		        
		        if (webRequest.result != UnityWebRequest.Result.Success)
		        {
			        Debug.LogError(webRequest.error);

					_bigPicturesLoaded[bigPictureName] = DefautBigPicture;
				}
	        }
        }

        public virtual void SendNotification(string notificationId, double seconds = 0)
        {
	        StartCoroutine(SendNotificationCo(notificationId, seconds));
        }
        
        IEnumerator SendNotificationCo(string notificationId, double seconds = 0)
        {
	        LocalNotificationModel notification = _localNotificationsModels.FirstOrDefault(notif => notif.Id == notificationId);
	        
			// CATCH THE BIG PICTURE (IF ANY) DEFINED IN THE NOTIFICATION MODEL
			string notificationBigPicture = notification.BigPicture;

			
	        
			// ONLY CONFIGURE THE BIG PICTURE IN THE NOTIFICATION IF A BIG PICTURE NAME WAS PROVIDED IN THE NOTIFICATION MODEL
			// ONLY DOWNLOAD THE BIG PICTURE IF IT WASN'T DOWNLOADED BEFORE (NOT INCLUDED IN THE REGISTRY OF BIG PICTURES LOADED)
			if (!string.IsNullOrEmpty(notificationBigPicture))
			{
#if UNITY_ANDROID
				if (!_bigPicturesLoaded.ContainsKey(notificationBigPicture))
				{
					yield return LoadBigPictureFromStreamingAssets(notificationBigPicture);
				}

				notification.BigPicturePath = _bigPicturesLoaded[notificationBigPicture];
				
#elif UNITY_IOS
				string path = Application.dataPath + "/Raw/Notifications";
				string filePath = Path.Combine(path, notificationBigPicture);
				notification.BigPicturePath = filePath;
#endif
			}
			

			_notificationService?.SendNotification(notificationId, seconds);

	        yield return null;
        }
#endregion



/// <summary>
		/// Creates a new notification object for the current platform.
		/// </summary>
		/// <returns>The new notification, ready to be scheduled, or null if there's no valid platform.</returns>
		/// <exception cref="InvalidOperationException"><see cref="Initialize"/> has not been called.</exception>
		protected IGameNotification CreateNotification()
		{
			if (!Initialized)
			{
				throw new InvalidOperationException("Must call Initialize() first.");
			}

			return _notificationService?.CreateNotification();
		}

		/// <summary>
		/// Schedules a notification to be delivered.
		/// </summary>
		/// <param name="notification">The notification to deliver.</param>
		protected void ScheduleNotification(IGameNotification notification)
		{
			if (!Initialized)
			{
				throw new InvalidOperationException("Must call Initialize() first.");
			}
			
			_notificationService?.ScheduleNotification(notification);

		}

		/// <summary>
		/// Cancels a scheduled notification.
		/// </summary>
		/// <param name="notificationId">The ID of the notification to cancel.</param>
		/// <exception cref="InvalidOperationException"><see cref="Initialize"/> has not been called.</exception>
		public void CancelNotification(string notificationId)
		{
			if (!Initialized)
			{
				throw new InvalidOperationException("Must call Initialize() first.");
			}

			_notificationService?.CancelNotification(notificationId);
		}

		/// <summary>
		/// Cancels all scheduled notifications.
		/// </summary>
		/// <exception cref="InvalidOperationException"><see cref="Initialize"/> has not been called.</exception>
		public void CancelAllNotifications()
		{
			if (!Initialized)
			{
				throw new InvalidOperationException("Must call Initialize() first.");
			}

			_notificationService?.CancelAllNotifications();
		}

		/// <summary>
		/// Dismisses a displayed notification.
		/// </summary>
		/// <param name="notificationId">The ID of the notification to dismiss.</param>
		/// <exception cref="InvalidOperationException"><see cref="Initialize"/> has not been called.</exception>
		public void DismissNotification(int notificationId)
		{
			if (!Initialized)
			{
				throw new InvalidOperationException("Must call Initialize() first.");
			}

			_notificationService?.DismissNotification(notificationId);
		}

		/// <summary>
		/// Dismisses all displayed notifications.
		/// </summary>
		/// <exception cref="InvalidOperationException"><see cref="Initialize"/> has not been called.</exception>
		public void DismissAllNotifications()
		{
			if (!Initialized)
			{
				throw new InvalidOperationException("Must call Initialize() first.");
			}

			_notificationService?.DismissAllNotifications();
		}
		
		/// <summary>
		/// Sets the local notifications that should be sent on start
		/// </summary>
		protected virtual void SetInitialLocalNotifications()
		{
			_notificationService?.SetInitialLocalNotifications();
		}
		
		/// <summary>
		/// Sets the local notifications that should be sent when the player is saved
		/// </summary>
		protected virtual void SetOnSaveLocalNotifications()
		{	
			_notificationService?.SetOnSaveLocalNotifications();
		}
	}
}
