#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using BigfootSdk.Core.Idle;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Gameplay modifiers manager to handle the inspector way of view.
    /// </summary>
    public partial class GameplayModifiersManager
    {
        /// <summary>
        /// Color for all the Bonus multiplier properties
        /// </summary>
        private Color bonusMultiplierColor = new Color(.33f, .89f, 1f);

        /// <summary>
        /// Color for all the Time multiplier properties
        /// </summary>
        private Color timeMultiplierColor = new Color(.7f, .56f, .53f);

        /// <summary>
        /// Color for all the Cost multiplier properties
        /// </summary>
        private Color costMultiplierColor = new Color(.37f, .37f, .20f);


        // Summary of all modifiers in the game with his current values
        // When a new modifier it's added to the game, you should add a new dict with the reference to those values in the same way as bellow

        #region Summary

        /// <summary>
        /// Summary for AllGeneratorBonusMultiplier type
        /// </summary>
        [ShowInInspector, TabGroup("Summary"), TableList(IsReadOnly = true, AlwaysExpanded = true), ShowIf("AllGeneratorBonusMultiplierIsNotNull"), DictionaryDrawerSettings(IsReadOnly = true), GUIColor("bonusMultiplierColor")]
        private Dictionary<string, double> AllGeneratorBonusMultiplierSummary
        {
            get
            {
                if (GeneratorModifiersModel.Instance == null)
                {
                    return null;
                }

                return GeneratorModifiersModel.Instance.AllGeneratorBonusMultiplierByGroup;
            }
        }

        /// <summary>
        /// Null check to know if we should show this summary in the inspector
        /// </summary>
        private bool AllGeneratorBonusMultiplierIsNotNull()
        {
            return !(GeneratorModifiersModel.Instance == null || GeneratorModifiersModel.Instance != null && GeneratorModifiersModel.Instance.AllGeneratorBonusMultiplierByGroup == null);
        }

        /// <summary>
        /// Summary for SpecificGeneratorsBonusMultipliers type
        /// </summary>
        [ShowInInspector, TabGroup("Summary"), TableList(IsReadOnly = true, AlwaysExpanded = true), ShowIf("SpecificGeneratorsBonusMultipliersIsNotNull"), DictionaryDrawerSettings(IsReadOnly = true, KeyLabel = "Generator name"), PropertySpace(SpaceAfter = 15), GUIColor("bonusMultiplierColor")]
        private Dictionary<string, Dictionary<string, double>> SpecificGeneratorsBonusMultipliersSummary
        {
            get
            {
                if (GeneratorModifiersModel.Instance == null)
                {
                    return null;
                }

                return GeneratorModifiersModel.Instance.SpecificGeneratorsBonusMultipliersByGroup;
            }
        }

        /// <summary>
        /// Null check to know if we should show this summary in the inspector
        /// </summary>
        private bool SpecificGeneratorsBonusMultipliersIsNotNull()
        {
            return !(GeneratorModifiersModel.Instance == null || GeneratorModifiersModel.Instance != null && GeneratorModifiersModel.Instance.SpecificGeneratorsBonusMultipliersByGroup == null);
        }

        /// <summary>
        /// Summary for AllGeneratorTimeMultiplier type
        /// </summary>
        [ShowInInspector, TabGroup("Summary"), TableList(IsReadOnly = true, AlwaysExpanded = true), ShowIf("AllGeneratorTimeMultiplierIsNotNull"), DictionaryDrawerSettings(IsReadOnly = true), GUIColor("timeMultiplierColor")]
        private Dictionary<string, double> AllGeneratorTimeMultiplierSummary
        {
            get
            {
                if (GeneratorModifiersModel.Instance == null)
                {
                    return null;
                }

                return GeneratorModifiersModel.Instance.AllGeneratorTimeMultiplierByGroup;
            }
        }

        /// <summary>
        /// Null check to know if we should show this summary in the inspector
        /// </summary>
        private bool AllGeneratorTimeMultiplierIsNotNull()
        {
            return !(GeneratorModifiersModel.Instance == null || GeneratorModifiersModel.Instance != null && GeneratorModifiersModel.Instance.AllGeneratorTimeMultiplierByGroup == null);
        }

        /// <summary>
        /// Summary for SpecificGeneratorsTimeMultipliers type
        /// </summary>
        [ShowInInspector, TabGroup("Summary"), TableList(IsReadOnly = true, AlwaysExpanded = true), ShowIf("SpecificGeneratorsTimeMultipliersIsNotNull"), DictionaryDrawerSettings(IsReadOnly = true, KeyLabel = "Generator name"), PropertySpace(SpaceAfter = 15), GUIColor("timeMultiplierColor")]
        private Dictionary<string, Dictionary<string, double>> SpecificGeneratorsTimeMultipliersSummary
        {
            get
            {
                if (GeneratorModifiersModel.Instance == null)
                {
                    return null;
                }

                return GeneratorModifiersModel.Instance.SpecificGeneratorsTimeMultipliersByGroup;
            }
        }

        /// <summary>
        /// Null check to know if we should show this summary in the inspector
        /// </summary>
        private bool SpecificGeneratorsTimeMultipliersIsNotNull()
        {
            return !(GeneratorModifiersModel.Instance == null || GeneratorModifiersModel.Instance != null && GeneratorModifiersModel.Instance.SpecificGeneratorsTimeMultipliersByGroup == null);
        }


        /// <summary>
        /// Summary for AllGeneratorCostMultiplier type
        /// </summary>
        [ShowInInspector, TabGroup("Summary"), TableList(IsReadOnly = true, AlwaysExpanded = true), ShowIf("AllGeneratorCostMultiplierIsNotNull"), DictionaryDrawerSettings(IsReadOnly = true), GUIColor("costMultiplierColor")]
        private Dictionary<string, double> AllGeneratorCostMultiplierSummary
        {
            get
            {
                if (GeneratorModifiersModel.Instance == null)
                {
                    return null;
                }

                return GeneratorModifiersModel.Instance.AllGeneratorCostMultiplierByGroup;
            }
        }

        /// <summary>
        /// Null check to know if we should show this summary in the inspector
        /// </summary>
        private bool AllGeneratorCostMultiplierIsNotNull()
        {
            return !(GeneratorModifiersModel.Instance == null || GeneratorModifiersModel.Instance != null && GeneratorModifiersModel.Instance.AllGeneratorCostMultiplierByGroup == null);
        }

        /// <summary>
        /// Summary for SpecificGeneratorsCostMultipliers type
        /// </summary>
        [ShowInInspector, TabGroup("Summary"), TableList(IsReadOnly = true, AlwaysExpanded = true), ShowIf("SpecificGeneratorsCostMultipliersIsNotNull"), DictionaryDrawerSettings(IsReadOnly = true, KeyLabel = "Generator name"), PropertySpace(SpaceAfter = 15), GUIColor("costMultiplierColor")]
        private Dictionary<string, Dictionary<string, double>> SpecificGeneratorsCostMultipliersSummary
        {
            get
            {
                if (GeneratorModifiersModel.Instance == null)
                {
                    return null;
                }

                return GeneratorModifiersModel.Instance.SpecificGeneratorsCostMultipliersByGroup;
            }
        }

        /// <summary>
        /// Null check to know if we should show this summary in the inspector
        /// </summary>
        private bool SpecificGeneratorsCostMultipliersIsNotNull()
        {
            return !(GeneratorModifiersModel.Instance == null || GeneratorModifiersModel.Instance != null && GeneratorModifiersModel.Instance.SpecificGeneratorsCostMultipliersByGroup == null);
        }

        #endregion

        // List of the individual modifiers applied in the game

        #region Detail applied bonuses

        /// <summary>
        /// List of SpecificGeneratorBonusMultiplier modifier type
        /// </summary>
        [ShowInInspector, TabGroup("Applied Bonuses"), TableList(IsReadOnly = true, AlwaysExpanded = true), GUIColor("bonusMultiplierColor")]
        private List<AppliedModifierInspectorCell> specificGeneratorBonusMultipliers
        {
            get { return GetGPMApplied(typeof(SpecificGeneratorBonusMultiplier)); }
            set { specificGeneratorBonusMultipliers = value; }
        }

        /// <summary>
        /// List of AllGeneratorBonusMultiplier modifier type
        /// </summary>
        [ShowInInspector, TabGroup("Applied Bonuses"), TableList(IsReadOnly = true, AlwaysExpanded = true), PropertySpace(SpaceAfter = 15), GUIColor("bonusMultiplierColor")]
        private List<AppliedModifierInspectorCell> AllGeneratorBonusMultiplier
        {
            get { return GetGPMApplied(typeof(AllGeneratorBonusMultiplier)); }
            set { AllGeneratorBonusMultiplier = value; }
        }

        /// <summary>
        /// List of SpecificGeneratorTimeMultiplier modifier type
        /// </summary>
        [ShowInInspector, TabGroup("Applied Bonuses"), TableList(IsReadOnly = true, AlwaysExpanded = true), GUIColor("timeMultiplierColor")]
        private List<AppliedModifierInspectorCell> SpecificGeneratorTimeMultiplier
        {
            get { return GetGPMApplied(typeof(SpecificGeneratorTimeMultiplier)); }
            set { SpecificGeneratorTimeMultiplier = value; }
        }

        /// <summary>
        /// List of AllGeneratorTimeMultiplier modifier type
        /// </summary>
        [ShowInInspector, TabGroup("Applied Bonuses"), TableList(IsReadOnly = true, AlwaysExpanded = true), PropertySpace(SpaceAfter = 15), GUIColor("timeMultiplierColor")]
        private List<AppliedModifierInspectorCell> AllGeneratorTimeMultiplier
        {
            get { return GetGPMApplied(typeof(AllGeneratorTimeMultiplier)); }
            set { AllGeneratorTimeMultiplier = value; }
        }

        /// <summary>
        /// List of SpecificGeneratorCostMultiplier modifier type
        /// </summary>
        [ShowInInspector, TabGroup("Applied Bonuses"), TableList(IsReadOnly = true, AlwaysExpanded = true), GUIColor("costMultiplierColor")]
        private List<AppliedModifierInspectorCell> SpecificGeneratorCostMultiplier
        {
            get { return GetGPMApplied(typeof(SpecificGeneratorCostMultiplier)); }
            set { SpecificGeneratorCostMultiplier = value; }
        }

        /// <summary>
        /// List of AllGeneratorCostMultiplier modifier type
        /// </summary>
        [ShowInInspector, TabGroup("Applied Bonuses"), TableList(IsReadOnly = true, AlwaysExpanded = true), PropertySpace(SpaceAfter = 15), GUIColor("costMultiplierColor")]
        private List<AppliedModifierInspectorCell> AllGeneratorCostMultiplier
        {
            get { return GetGPMApplied(typeof(AllGeneratorCostMultiplier)); }
            set { AllGeneratorCostMultiplier = value; }
        }

        /// <summary>
        /// Get the applied modifiers in the game
        /// </summary>
        private List<AppliedModifierInspectorCell> GetGPMApplied(Type modType)
        {
            List<AppliedModifierInspectorCell> result = new List<AppliedModifierInspectorCell>();

            foreach (GameplayModifierModel modifier in AppliedModifiers)
            {
                if (modifier.GetType() == modType)
                {
                    result.Add(new AppliedModifierInspectorCell(modifier));
                }
            }

            return result;
        }

        /// <summary>
        /// Class used to show the content of the applied modifiers in the inspector
        /// </summary>
        private class AppliedModifierInspectorCell
        {
            /// <summary>
            /// Class constructor
            /// </summary>
            public AppliedModifierInspectorCell(GameplayModifierModel modifierModel)
            {
                model = modifierModel;
            }

            private GameplayModifierModel model;

            /// <summary>
            /// The group of the current modifier
            /// </summary>
            [ShowInInspector, ReadOnly]
            private string group
            {
                get { return model == null ? "Null modifier model" : model.Group; }
            }

            /// <summary>
            /// The value of the current modifier
            /// </summary>
            [ShowInInspector, ReadOnly]
            private string value
            {
                get { return model == null ? "Null modifier model" : model.GetValue().ToString(); }
            }

            /// <summary>
            /// Button to remove the current modifier
            /// </summary>
            [ShowInInspector, Button("Remove", ButtonStyle.CompactBox), EnableIf("CanRemove")]
            private void Action()
            {
                if (model == null)
                {
                    return;
                }

                Instance.RemoveGameplayModifier(model);
            }

            /// <summary>
            /// Used to check if the model can be used to be removed
            /// </summary>
            private bool CanRemove()
            {
                return model != null;
            }
        }

        #endregion
    }
}
#endif