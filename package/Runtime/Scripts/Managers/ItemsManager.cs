﻿using System;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Helpers;
using BigfootSdk.Sprites;
using UnityEngine;
using BigfootSdk.LTE;
using Sirenix.OdinInspector;

namespace BigfootSdk.Backend
{
    /// <summary>
    /// Manager in charge of everything related to the Items
    /// </summary>
    public class ItemsManager : LoadableSingleton<ItemsManager>
    {
        /// <summary>
        /// The items.
        /// </summary>
        [TableList(IsReadOnly = true, DefaultMinColumnWidth = 100, MinScrollViewHeight = 600, AlwaysExpanded = true), TabGroup("Complete Collection")]
        public List<BaseItemModel> Items = new List<BaseItemModel>();

#if UNITY_EDITOR
        /// <summary>
        /// Bundle items list. Created to show in the inspector all the bundle items. This list get updated after the main list its assigned
        /// </summary>
        //[ListDrawerSettings(IsReadOnly = true), TabGroup("Bundles")]
        [TableList(IsReadOnly = true, DefaultMinColumnWidth = 100, MinScrollViewHeight = 600, AlwaysExpanded = true), TabGroup("Bundles")]
        public List<BundleItemModel> BundleItems = new List<BundleItemModel>();

        /// <summary>
        /// Bundle items list. Created to show in the inspector all the droptable items. This list get updated after the main list its assigned
        /// </summary>
        [TableList(IsReadOnly = true, DefaultMinColumnWidth = 100, MinScrollViewHeight = 600, AlwaysExpanded = true), TabGroup("Droptable")]
        public List<DroptableItemModel> DroptableItems = new List<DroptableItemModel>();

        /// <summary>
        /// Bundle items list. Created to show in the inspector all the gameplay modifier items. This list get updated after the main list its assigned
        /// </summary>
        [TableList(IsReadOnly = true, DefaultMinColumnWidth = 100, MinScrollViewHeight = 600, AlwaysExpanded = true), TabGroup("Gameplay Modifiers")]
        public List<GameplayModifierItemModel> GameplayModifierItems = new List<GameplayModifierItemModel>();

        /// <summary>
        /// Bundle items list. Created to show in the inspector all the items. This list get updated after the main list its assigned
        /// </summary>
        [TableList(IsReadOnly = true, DefaultMinColumnWidth = 100, MinScrollViewHeight = 600, AlwaysExpanded = true), TabGroup("Base Items")]
        public List<BaseItemModel> BaseItems = new List<BaseItemModel>();
#endif
        /// <summary>
        /// Callback used after the list of items gets updated
        /// </summary>
        public static Action<List<BaseItemModel>> OnItemListUpdated;

        /// <summary>
        /// Initializes the Items
        /// </summary>
        public override void StartLoading()
        {
            LogHelper.LogSdk("Initializing Items...", LogHelper.DEPENDENCY_MANAGER_TAG);

            UpdateItemList();

            LogHelper.LogSdk("Items Finished Loading", LogHelper.DEPENDENCY_MANAGER_TAG);

            FinishedLoading(true);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            GameModeManager.OnGameModeChanged += GameModeChanged;
#if UNITY_EDITOR
            OnItemListUpdated += UpdateListForBundleItemType;
            OnItemListUpdated += UpdateListForDroptableItemType;
            OnItemListUpdated += UpdateListForGameplayModifierItemType;
            OnItemListUpdated += UpdateListForBaseItemType;
            DependencyManager.OnEverythingLoaded += OnDependenciesLoaded;
#endif
        }

        void OnDisable()
        {
            GameModeManager.OnGameModeChanged -= GameModeChanged;
#if UNITY_EDITOR
            OnItemListUpdated -= UpdateListForBundleItemType;
            OnItemListUpdated -= UpdateListForDroptableItemType;
            OnItemListUpdated -= UpdateListForGameplayModifierItemType;
            OnItemListUpdated -= UpdateListForBaseItemType;
            DependencyManager.OnEverythingLoaded -= OnDependenciesLoaded;
#endif
        }

#if UNITY_EDITOR

        /// <summary>
        /// Used to serialize all the values from the Data field in each base item so it can be shown in the inspector
        /// </summary>
        private void OnDependenciesLoaded()
        {
            if (Items == null)
            {
                return;
            }

            foreach (BaseItemModel baseItemModel in Items)
            {
                if (baseItemModel == null)
                {
                    continue;
                }

                baseItemModel.SerializeRawDataForInspector();
            }
        }

        /// <summary>
        /// Update the content of the bundle item list
        /// </summary>
        /// <param name="items">item list</param>
        private void UpdateListForBundleItemType(List<BaseItemModel> items)
        {
            BundleItems = GetAllItemsOfType<BundleItemModel>();
        }

        /// <summary>
        /// Update the content of the droptable item list
        /// </summary>
        /// <param name="items">item list</param>
        private void UpdateListForDroptableItemType(List<BaseItemModel> items)
        {
            DroptableItems = GetAllItemsOfType<DroptableItemModel>();
        }

        /// <summary>
        /// Update the content of the gameplay modifier item list
        /// </summary>
        /// <param name="items">item list</param>
        private void UpdateListForGameplayModifierItemType(List<BaseItemModel> items)
        {
            GameplayModifierItems = GetAllItemsOfType<GameplayModifierItemModel>();
        }

        /// <summary>
        /// Update the content of the droptable item list
        /// </summary>
        /// <param name="items">item list</param>
        private void UpdateListForBaseItemType(List<BaseItemModel> items)
        {
            BaseItems = new List<BaseItemModel>();
            List<BaseItemModel> auxItems = GetAllItemsOfType<BaseItemModel>();
            foreach (BaseItemModel baseItemModel in auxItems)
            {
                if (string.Equals(baseItemModel.Type, "Item"))
                {
                    BaseItems.Add(baseItemModel);
                }
            }
        }
#endif

        /// <summary>
        /// Update the list items
        /// </summary>
        public void UpdateItemList()
        {
            Items = JsonHelper.DeserializeObjectList<BaseItemModel>(TitleManager.Instance.GetFromTitleData(LimitedTimeEventsManager.Instance.GetConfigurationMapping("items")));
            OnItemListUpdated?.Invoke(Items);
        }

        /// <summary>
        /// When the game mode changes, update the Items
        /// </summary>
        /// <param name="newGameMode">The new game mode</param>
        void GameModeChanged(string newGameMode, bool isRestart)
        {
            UpdateItemList();
        }

        /// <summary>
        /// Gets all items.
        /// </summary>
        /// <returns>The all items.</returns>
        public List<BaseItemModel> GetAllItems()
        {
            return Items;
        }

        /// <summary>
        /// Gets all of the items of a certain type
        /// </summary>
        /// <returns>All of the items of a certain type.</returns>
        /// <typeparam name="T">The type of the items</typeparam>
        public List<T> GetAllItemsOfType<T>() where T : BaseItemModel
        {
            return Items.OfType<T>().ToList();
        }

        /// <summary>
        /// Get all of the items of a certain class
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public List<BaseItemModel> GetAllItemsOfClass(string className)
        {
            return Items.Where(item => item.Class == className).ToList();
        }

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <returns>The item.</returns>
        /// <param name="itemName">Item name.</param>
        public BaseItemModel GetItem(string itemName)
        {
            BaseItemModel item = Items.FirstOrDefault(i => i.ItemName == itemName);

            if(item != null)
            {
                return item;
            }

            LogHelper.LogWarning(string.Format("Item with name {0} was not found", itemName));
            return null;
        }

        /// <summary>
        /// Get the sprite key of an item
        /// </summary>
        /// <param name="itemName">The item we want to use</param>
        /// <returns>The sprite key</returns>
        public string GetItemSpriteKey(string itemName)
        {
            BaseItemModel item = Items.FirstOrDefault(i => i.ItemName == itemName);

            if(item != null)
            {
                return item.SpriteKey;
            }

            LogHelper.LogWarning(string.Format("Item with name {0} was not found", itemName));
            return "";
        }

        /// <summary>
        /// Get the sprite for an item
        /// </summary>
        /// <param name="itemName">The item we want to use</param>
        /// <param name="callback">The callback for when we get the sprite</param>
        /// <param name="changeInEvent">Should this item be different in events?</param>
        public void GetItemSprite(string itemName, Action<Sprite> callback, bool changeInEvent = false)
        {
            var spriteKey = GetItemSpriteKey(itemName);
            SpriteManager.Instance.GetSprite(spriteKey, callback, changeInEvent);
        }
    }
}