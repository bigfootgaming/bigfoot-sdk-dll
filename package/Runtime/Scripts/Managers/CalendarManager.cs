﻿using System;
using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.Notifications;
using UnityEngine;

namespace BigfootSdk.Calendar
{
    public class CalendarManager : LoadableSingleton<CalendarManager>
    {
        // String Key used to save Calendar Progress.
        private readonly string CALENDAR_PROGRESSION_KEY = "CalendarProgression";
        
        // Events
        public static Action<CalendarProgressionModel> OnCalendarCompletedEvent;
        public static Action<int> OnCalendarDayClaimed;
        public static Action<int> OnCourseRewardClaimed;

        // Current Active Day
        public int ActiveDay;
        
        // Private fields
        private List<CalendarCourseModel> _calendarCourses;
        private CalendarsModel _calendarsConfig;
        private int _currentCalendar;
        private int _calendarTotalDays;
        private PlayerManager _playerManager;
        private CalendarProgressionModel _calendarProgression;

        private bool _isCalendarAsigned;
        
        protected override void OnEnable()
        {
            base.OnEnable();
           OnCalendarCompletedEvent += OnCalendarCompleted;
        }

        private void OnDisable()
        {
            OnCalendarCompletedEvent -= OnCalendarCompleted;
        }

        public override void StartLoading()
        {
            // Cached Reference to Player Manager
            _playerManager = PlayerManager.Instance;
            
            // Load Calendar Configs.
            _calendarsConfig = ConfigHelper.LoadConfig<CalendarsModel>("calendar");
            
            // Load Player Progression
            if (LoadCalendarProgression())
            {
                // Initialize Calendar
                InitCalendar();    
            }
            
            FinishedLoading(true);
        }

        /// <summary>
        /// Loads Calendar Data
        /// </summary>
        private void InitCalendar()
        {
            // Initialize wich calendar is ongoing
            SelectActiveCalendar();

            if (_currentCalendar == -1)
            {
                SetNoCalendar();
                return;
            }

            _isCalendarAsigned = true;
            
            // Load Calendar Courses
            _calendarCourses = JsonHelper.DesarializeObject<List<CalendarCourseModel>>(_calendarsConfig.Calendars[_currentCalendar].CalendarCourses["Courses"].ToString());

            // Calculate Calendar Total Days
            _calendarTotalDays = GetCalendarTotalDays();
            
            // Get Current Active Day
            ActiveDay = GetActiveDay();
        }

        /// <summary>
        /// Set no calendar assigned
        /// </summary>
        private void SetNoCalendar()
        {
            _calendarCourses = null;
            _calendarTotalDays = 0;
            _isCalendarAsigned = false;
        }

        /// <summary>
        /// Returns if a calendar is available
        /// </summary>
        /// <returns></returns>
        public bool IsCalendarAvailable()
        {
            return _isCalendarAsigned;
        }

        /// <summary>
        /// Returns True if Course Completion Reward Has been Claimed
        /// </summary>
        /// <param name="courseID"></param>
        /// <returns></returns>
        public bool IsCourseClaimed(int courseID)
        {
            return _calendarProgression.ClaimedCourses.ContainsKey(courseID);
        }

        /// <summary>
        /// Returns true is course completion reward can be claimed
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsCourseClaimeable(CalendarCourseModel model)
        {
            if (model.HasCompletionRewards && !IsCourseClaimed(model.Id))
            {
                // Find Course Index
                int courseIndex = 0;
                for (int i = 0; i < _calendarCourses.Count; i++)
                {
                    if (_calendarCourses[i].Id == model.Id)
                    {
                        courseIndex = i;
                        break;
                    }
                }

                // Calculate how many days were claimed for this calendar course
                int courseClaimedDays = 0;
                int from = model.CourseLenght * courseIndex;
                int to = from + model.CourseLenght - 1;
                
                for (int a = from; a < to; a++)
                {
                    if (_calendarProgression.ClaimedDays.ContainsKey(a))
                        if (_calendarProgression.ClaimedDays[a] == true)
                            courseClaimedDays++;

                    if (courseClaimedDays >= model.MinClaimForBonus)
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns true if the calendar course is passed
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsCourseCompleted(CalendarCourseModel model)
        {
            if (model.HasCompletionRewards)
            {
                if (IsCourseClaimed(model.Id))
                    return true;
                
                // Find Course Index
                int courseIndex = 0;
                for (int i = 0; i < _calendarCourses.Count; i++)
                {
                    if (_calendarCourses[i].Id == model.Id)
                    {
                        courseIndex = i;
                        break;
                    }
                }
                
                // Get Course Last Day
                int firstCourseDay = model.CourseLenght * courseIndex;
                int lastCourseDay = firstCourseDay + model.CourseLenght;
                
                // If Actual Day is greather than last course day, the course is missed
                if (ActiveDay > lastCourseDay)
                    return true;
            }
            
            return false;
        }

        /// <summary>
        /// Persists CompletedCalendar Key, and load next available
        /// </summary>
        /// <param name="obj"></param>
        private void OnCalendarCompleted(CalendarProgressionModel obj)
        {
            // Persist this calendar is completed
            _calendarProgression.CompletedCalendars.Add(_calendarProgression.AssignedCalendar);
            
            // Clean control fields
            _calendarProgression.ClaimedDays.Clear();
            _calendarProgression.ClaimedCourses.Clear();
            
            // Save user Progress
            SaveCalendarProgress();
            
            // Initializes new calendar
            InitCalendar();
        }

        /// <summary>
        /// Returns True is reward has being claimed
        /// </summary>
        /// <param name="dayIndex"></param>
        /// <returns></returns>
        public bool IsDayClaimed(int dayIndex)
        {
            if (_calendarProgression.ClaimedDays.ContainsKey(dayIndex))
                return _calendarProgression.ClaimedDays[dayIndex];

            return false;
        }

        /// <summary>
        /// Returns True if the day passed without being claimed
        /// </summary>
        /// <param name="dayIndex"></param>
        /// <returns></returns>
        public bool IsDayMissed(int dayIndex)
        {
            if (dayIndex < ActiveDay)
            {
                if (_calendarProgression.ClaimedDays.ContainsKey(dayIndex))
                {
                    return !_calendarProgression.ClaimedDays[dayIndex];
                }
                else
                {
                    _calendarProgression.ClaimedDays.Add(dayIndex,false);
                    IsCalendarCompleted();
                }
                
                return true;
            }

            return false;
            
        }
        
        /// <summary>
        /// Returns wich day is active today
        /// </summary>
        private int GetActiveDay()
        {
            int result = 0;

            foreach (KeyValuePair<int,bool> claimedDay in _calendarProgression.ClaimedDays)
            {
                // Search for the last Claimed Day on dictionart
                if (claimedDay.Value == true)
                    result = claimedDay.Key;
            }

            if (_calendarProgression.StartingDate < TimeHelper.GetTimeInServerDateTime())
            {
                TimeSpan nextReward = GetTimeUntilNextReward();

                if (nextReward.TotalSeconds <= 0)
                {
                    if (_calendarsConfig.CanMissDays)
                    {
                        result += (nextReward.Days * -1) + 1;
                    }
                    else if (IsDayClaimed(ActiveDay))
                    {
                      result++;
                    }    
                }
            }
            return result;
        }

        /// <summary>
        /// Returns time left until next claim
        /// </summary>
        /// <returns></returns>
        public TimeSpan GetTimeUntilNextReward()
        {
            // Get When is NextClaim, Based on Last:
            DateTime serverTime = TimeHelper.GetTimeInServerDateTime();

            // Create a date with same day at 01 am
            DateTime lastTime = _calendarProgression.LastClaimedDay != DateTime.MinValue ? _calendarProgression.LastClaimedDay : _calendarProgression.StartingDate;
            DateTime nextClaim = new DateTime(lastTime.Year,lastTime.Month,lastTime.Day,1, 0, 0);

            // And increase it to the next Day
            nextClaim = nextClaim.AddDays(1);

            return nextClaim - serverTime;
        }

        /// <summary>
        /// Load Calendar progression for the player
        /// </summary>
        private bool LoadCalendarProgression()
        {
            string json = _playerManager.GetFromPlayerData(CALENDAR_PROGRESSION_KEY, GameModeManager.CurrentGameMode);
         
            if (!string.IsNullOrEmpty(json) && json != "null")
            {
                _calendarProgression = JsonHelper.DesarializeObject<CalendarProgressionModel>(json);
            }
            else
            {
                _calendarProgression = new CalendarProgressionModel();
                
                SelectActiveCalendar();

                if (_currentCalendar == -1)
                    return false;
                
                InitializeCalendarProgression();
            }

            return true;
        }

        /// <summary>
        /// Initialize Calendar Progression by default
        /// </summary>
        private void InitializeCalendarProgression()
        {
            // Set Selected Calendar
            _calendarProgression.AssignedCalendar = _calendarsConfig.Calendars[_currentCalendar].CalendarName; 
            
            // Last Claimed Day to min time to be able to claim today first reward
            _calendarProgression.LastClaimedDay = DateTime.MinValue;
            
            // Initialize an empty dictionary of claimed days
            _calendarProgression.ClaimedDays = new Dictionary<int, bool>();
            
            // Initialize an empty dictionary of claimed courses
            _calendarProgression.ClaimedCourses = new Dictionary<int, bool>();
            
            // Initializaze an empty list of completed Calendars
            _calendarProgression.CompletedCalendars = new List<string>();
            
            // Set Starting Date for calendar
            _calendarProgression.StartingDate = DateTime.Today;
        }

        /// <summary>
        /// Persists Calendar into playerData
        /// </summary>
        private void SaveCalendarProgress()
        {
            string progressJson = JsonHelper.SerializeObject(_calendarProgression);

            Dictionary<string, DataUpdateModel> data = new Dictionary<string, DataUpdateModel>
            {
                {CALENDAR_PROGRESSION_KEY, new DataUpdateModel(progressJson)},
            };
            
            _playerManager.SetPlayerData(data,GameModeManager.CurrentGameMode);
        }
      
        /// <summary>
        /// Returns User Active Calendar
        /// </summary>
        /// <returns></returns>
        public List<CalendarCourseModel> GetActiveCalendarCourses()
        {
            return _calendarCourses;
        }

        /// <summary>
        /// Returns wich course of the calendar is active today
        /// </summary>
        /// <returns></returns>
        public CalendarCourseModel GetActiveCourse()
        {
            int activeCalendar = Mathf.FloorToInt(GetActiveDay() / _calendarCourses[0].CourseLenght);
            return _calendarCourses[activeCalendar];
        }
        
        /// <summary>
        /// Check if user is already completing a calendar
        /// or assign a new one
        /// </summary>
        private void SelectActiveCalendar()
        {
            // Assign first one
            _currentCalendar = -1;
            
            // If the user does not have a calendar assigned search for one
            if (String.IsNullOrEmpty(_calendarProgression.AssignedCalendar))
            {
                _currentCalendar = SearchForAvailableCalendar();
            }
            else
            {
                // Get current Assigned Calendar
                CalendarModel assignedCalendar = _calendarsConfig.Calendars.Find(x => x.CalendarName == _calendarProgression.AssignedCalendar);

                if (assignedCalendar != null)
                {
                    // If the Calendar is already completed search a new one
                    if (_calendarProgression.CompletedCalendars.Contains(assignedCalendar.CalendarName))
                    {
                        _currentCalendar = SearchForAvailableCalendar(_calendarProgression.CompletedCalendars);
                    }
                    else
                    {
                        // If the calendar is stil not completed, just assign it
                        _currentCalendar = _calendarsConfig.Calendars.IndexOf(assignedCalendar);
                    }
                }
                else
                {
                    // If no calendar assigned Search for a new one
                    _currentCalendar = SearchForAvailableCalendar(_calendarProgression.CompletedCalendars);
                }
            }
        }

        
        /// <summary>
        /// Check if a calendar has been completed
        /// </summary>
        /// <returns></returns>
        private bool IsCalendarCompleted()
        {
            if (_calendarProgression.ClaimedDays.Count == _calendarTotalDays)
            {
                OnCalendarCompletedEvent?.Invoke(_calendarProgression);
                return true;
            }
                
            
            return false;
        }

        /// <summary>
        /// Counts for calendar total days
        /// </summary>
        private int GetCalendarTotalDays()
        {
            int result = 0;
            foreach (CalendarCourseModel courseModel in _calendarCourses)
            {
                result += courseModel.BaseRewards.Count;
            }

            return result;
        }

        /// <summary>
        /// Search for the first Calendar within prerequisites
        /// </summary>
        /// <returns></returns>
        private int SearchForAvailableCalendar(List<string> calendarsToExclude = null)
        {
            int result = -1;
            for (int i = 0; i < _calendarsConfig.Calendars.Count; i++)
            {
                if (PrerequisiteHelper.CheckPrerequisites(_calendarsConfig.Calendars[i].Prerequisites))
                {
                    // If we have a list of calendar to exlude from the list
                    if (calendarsToExclude != null)
                    {
                        // Check is passed calendar is on the list
                        if (!calendarsToExclude.Contains(_calendarsConfig.Calendars[i].CalendarName))
                        {
                            // if is not on the list, assign it and search no more
                            result = i;
                            break;
                        }
                    }
                    else // If we dont have an exclude list, assign first one and break
                    {
                        result = i;
                        break;
                    }
                }
            }

            return result;
        }
        
        /// <summary>
        /// Check if Calendar prerequisites are met
        /// </summary>
        /// <returns></returns>
        public bool IsCalendarUnlocked()
        {
            foreach (IPrerequisite prerequisite in _calendarsConfig.UnlockPrerequisites)
            {
                if (!prerequisite.Check())
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Persist that a reward for a given day was claimed
        /// </summary>
        /// <param name="dayIndex"></param>
        public void RewardClaimed(int dayIndex)
        {
            // Send Analytics Event
            var data = new Dictionary<string, object>()
            {
                {"calendarId", _calendarCourses[_currentCalendar].Id},
                {"dayClaimed", dayIndex}
            };
            
            AnalyticsManager.Instance.TrackCustomEvent("CalendarRewardClaimed", data);
            
            if (!_calendarProgression.ClaimedDays.ContainsKey(dayIndex))
            {
                _calendarProgression.ClaimedDays.Add(dayIndex, true);
                IsCalendarCompleted();
            }
            
            _calendarProgression.LastClaimedDay = TimeHelper.GetTimeInServerDateTime();
            
            SaveCalendarProgress();
            
            OnCalendarDayClaimed?.Invoke(dayIndex);
            
            // Schedule Notification for next reward Claim
            if (_calendarsConfig.SendLocalNotifications && _isCalendarAsigned)
                LocalNotificationsManager.Instance.SendNotification("calendar_notification", GetTimeUntilNextReward().TotalSeconds);
        }

        /// <summary>
        /// Persist that a reward for a given course was claimed
        /// </summary>
        /// <param name="courseID"></param>
        public void CourseRewardClaimed(int courseID)
        {
            _calendarProgression.ClaimedCourses.Add(courseID, true);
            
            SaveCalendarProgress();

            OnCourseRewardClaimed(courseID);
        }
    }
}
 