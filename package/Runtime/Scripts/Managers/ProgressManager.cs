﻿using System;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Manager in charge of everything related to the progress, including levels, worlds, unlocked content, etc
	/// </summary>
	public class ProgressManager : LoadableSingleton<ProgressManager>
	{
		/// <summary>
		/// List of the worlds
		/// </summary>
		[SerializeField]
		List<WorldModel> worlds = new List<WorldModel> ();

		/// <summary>
		/// List of the progress of the worlds
		/// </summary>
		[SerializeField]
		List<WorldProgressModel> worldsProgress = new List<WorldProgressModel> ();

		/// <summary>
		/// The progress service interface.
		/// </summary>
		private IProgressService _progressService;

		/// <summary>
		/// Gets the progress service.
		/// </summary>
		/// <value>The progress service.</value>
		private IProgressService ProgressService {
			get { 
				if (_progressService == null) {
                    #if BACKEND_AWS || BACKEND_FIREBASE
                    _progressService = new AWSProgressService();
                    #elif BACKEND_GS
                    _progressService = new GSProgressService ();
					#endif
				}
				return _progressService;
			}
		}

		/// <summary>
		/// Initializes the progress
		/// </summary>
		public override void StartLoading ()
		{
			LogHelper.LogSdk ("Initializing Progress...", LogHelper.DEPENDENCY_MANAGER_TAG);

            // Populate the levels
            ProgressService.TitleInitialized();

            // Populate the level progress
            ProgressService.PlayerInitialized();
            			
			LogHelper.LogSdk ("Progress Finished Loading: True", LogHelper.DEPENDENCY_MANAGER_TAG);

			FinishedLoading (true);
		}

		#region Worlds

		/// <summary>
		/// Sets the worlds.
		/// </summary>
		/// <param name="worlds">Worlds.</param>
		public void SetWorlds (List<WorldModel> worlds)
		{
			this.worlds = worlds;
		}

		/// <summary>
		/// Gets a world.
		/// </summary>
		/// <returns>The world.</returns>
		/// <param name="worldId">World identifier.</param>
		public WorldModel GetWorld (int worldId)
		{
			return worlds.Where (world => world.WorldId == worldId).FirstOrDefault ();
		}

		/// <summary>
		/// Gets all worlds.
		/// </summary>
		/// <returns>All the worlds.</returns>
		public List<WorldModel> GetAllWorlds ()
		{
			return worlds;
		}

		#endregion


		#region World Progress

		/// <summary>
		/// Sets the worlds progress.
		/// </summary>
		/// <param name="worlds">Worlds.</param>
		public void SetWorldsProgress (List<WorldProgressModel> worlds)
		{
			this.worldsProgress = worlds;
		}

		/// <summary>
		/// Gets the world's progress.
		/// </summary>
		/// <returns>The world progress.</returns>
		/// <param name="worldId">World identifier.</param>
		public WorldProgressModel GetWorldProgress (int worldId)
		{
			return worldsProgress.Where (world => world.WorldId == worldId).FirstOrDefault ();
		}

		/// <summary>
		/// Gets all world's progress.
		/// </summary>
		/// <returns>All the worlds progress.</returns>
		public List<WorldProgressModel> GetAllWorldsProgress ()
		{
			return worldsProgress;
		}

		/// <summary>
		/// Determines whether a world is locked, based on its prerequisites
		/// </summary>
		/// <returns><c>true</c> if this world is locked otherwise, <c>false</c>.</returns>
		/// <param name="worldId">World identifier.</param>
		public bool IsWorldLocked (int worldId)
		{
			WorldModel world = GetWorld (worldId);
			if (world != null)
				return PrerequisiteHelper.CheckPrerequisites (world.Prereqs);

			return true;
		}

		#endregion


		#region Levels

		/// <summary>
		/// Gets the level.
		/// </summary>
		/// <returns>The level.</returns>
		/// <param name="worldId">World identifier.</param>
		/// <param name="levelId">Level identifier.</param>
		public LevelModel GetLevel (int worldId, int levelId)
		{
			List<LevelModel> levels = GetAllLevelsForWorld (worldId);
			if (levels != null) {
				return levels.Where (level => level.LevelId == levelId).FirstOrDefault ();
			}
			return null;
		}

		/// <summary>
		/// Gets all levels for world.
		/// </summary>
		/// <returns>All the levels for the world.</returns>
		/// <param name="worldId">World identifier.</param>
		public List<LevelModel> GetAllLevelsForWorld (int worldId)
		{
			var selectedWorld = worlds.Where (world => world.WorldId == worldId).FirstOrDefault ();
			if (selectedWorld != null) {
				return selectedWorld.Levels;
			}
			return null;
		}

		#endregion

		#region LevelsProgress

		/// <summary>
		/// Gets the level progress.
		/// </summary>
		/// <returns>The level progress.</returns>
		/// <param name="worldId">World identifier.</param>
		/// <param name="levelId">Level identifier.</param>
		public LevelProgressModel GetLevelProgress (int worldId, int levelId)
		{
			List<LevelProgressModel> levels = GetAllLevelsProgressForWorld (worldId);
			LevelProgressModel levelProgress = levels.Where (level => level.LevelId == levelId).FirstOrDefault ();
			if (levelProgress == null) {
				levelProgress = new LevelProgressModel ();
				levelProgress.WorldId = worldId;
				levelProgress.LevelId = levelId;

				levels.Add (levelProgress);
			}
			return levelProgress;
		}

		/// <summary>
		/// Gets all levels progress for the world.
		/// </summary>
		/// <returns>All the levels progress for the world.</returns>
		/// <param name="worldId">World identifier.</param>
		public List<LevelProgressModel> GetAllLevelsProgressForWorld (int worldId)
		{
			var selectedWorld = worldsProgress.Where (world => world.WorldId == worldId).FirstOrDefault ();
			if (selectedWorld != null) {
				return selectedWorld.LevelsProgress;
			} else {
				WorldProgressModel newWorldProgress = new WorldProgressModel ();
				newWorldProgress.WorldId = worldId;
				newWorldProgress.CurrentLevel = 0;
				worldsProgress.Add (newWorldProgress);

				return newWorldProgress.LevelsProgress;
			}
		}

		/// <summary>
		/// Determines whether a level is locked, based on its prerequisites
		/// </summary>
		/// <returns><c>true</c> if this level is locked; otherwise, <c>false</c>.</returns>
		/// <param name="worldId">World identifier.</param>
		/// <param name="levelId">Level identifier.</param>
		public bool IsLevelLocked (int worldId, int levelId)
		{
			LevelModel level = GetLevel (worldId, levelId);
			if (level != null)
				return PrerequisiteHelper.CheckPrerequisites (level.Prereqs);

			return true;
		}

		/// <summary>
		/// Sets the level progress.
		/// </summary>
		/// <param name="worldId">World identifier.</param>
		/// <param name="levelId">Level identifier.</param>
		/// <param name="score">Score.</param>
		public void SetLevelProgress (int worldId, int levelId, int score)
		{
			LevelProgressModel levelProgress = GetLevelProgress (worldId, levelId);
			if (levelProgress != null) {
				if (score > levelProgress.Score) {
					// Save it locally
					levelProgress.Score = score;

					// Persist it in the backend
                    PlayerManager.Instance.SetPlayerData ("progress", new DataUpdateModel(JsonHelper.SerializeObject (worldsProgress)), GameModeManager.CurrentGameMode);
				}
			}
		}

		#endregion
	}
}
