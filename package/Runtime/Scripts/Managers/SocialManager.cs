﻿using System;
using BigfootSdk.Helpers;
using UnityEngine;

namespace BigfootSdk
{
    public class SocialManager : LoadableSingleton<SocialManager>
    {
        public static Action OnSocialLogout;
#if UNITY_ANDROID && GOOGLE_PLAY_GAMES
        protected string _authCode;

        public string AuthCode
        {
            get => _authCode;
        }
#endif
        
#if UNITY_IOS && APPLE_ID
        //Apple Id Vars

        public string Nonce => _nonce;
        protected string _nonce;
        public byte[] IdentityToken => _identityToken;
        protected byte[] _identityToken;
        public byte[] AuthorizationCode => _authorizationCode;
        protected byte[] _authorizationCode;
#endif
        

        protected bool _authenticated;

        public bool Authenticated
        {
            get => _authenticated;
        }

        public override void StartLoading()
        {
#if !UNITY_EDITOR
            Initialize();
            if (ShouldAuthenticate())
            {
                Authenticate(FinishAuthenticate);
            }
            else
            {
                FinishAuthenticate(false);
            }
#else

            FinishAuthenticate(false);
#endif
        }
        
        protected virtual void Initialize()
        {
        
        }

        
        protected virtual bool ShouldAuthenticate()
        {
            return true;
        }

        public virtual void Authenticate(Action<bool> callback)
        {

        }

        public virtual void Reauthenticate(Action<bool> callback)
        {
            
        }

        protected virtual void FinishAuthenticate(bool success)
        {
            _authenticated = success;
            LogHelper.LogSdk ($"Social Manager authenticated: {success}", LogHelper.DEPENDENCY_MANAGER_TAG);
            FinishedLoading(true);
        }
        
        public virtual void SignOut(Action<bool> callback) {
            if (_authenticated)
            {
                _authenticated = false;
            }
        }

        public virtual string GetEmail()
        {
            return "";
        }

        public virtual string GetUserId()
        {
            return "";
        }
    }
}