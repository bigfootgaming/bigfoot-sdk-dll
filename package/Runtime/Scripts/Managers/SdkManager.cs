﻿using System;
using BigfootSdk.Backend;
using BigfootSdk.Extensions;
using UnityEngine;
using Zenject;

namespace BigfootSdk
{
	/// <summary>
	/// Manager in charge of the configuration of the sdk
	/// </summary>
	public class SdkManager : Singleton<SdkManager>
	{
		/// <summary>
		/// Configuration file to use
		/// </summary>
		[Inject]
		[HideInInspector]
		public SdkConfiguration Configuration;

		public override void Awake()
		{
			base.Awake();
			TaskExtension.Init();
		}

		private void Update()
		{
			TaskExtension.Update();
		}

		public void Reset()
		{
			TaskExtension.Reset();
		}
	}
}