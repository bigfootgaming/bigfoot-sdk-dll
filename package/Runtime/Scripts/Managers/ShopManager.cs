﻿using System.Collections.Generic;
using System;
using UnityEngine;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using BigfootSdk.Analytics;
using BigfootSdk.LTE;
using Zenject;
using System.Threading;
using System.Threading.Tasks;
using BigfootSdk.Extensions;

namespace BigfootSdk.Shop
{
    public class ShopManager : LoadableSingleton<ShopManager>
    {
        /// <summary>
        /// Connection to store failed probably because of internet connection or user is not logged in with an account
        /// </summary>
        public static Action OnConnectionToStoreFailed;
        
        /// <summary>
        /// Purchase Start Event
        /// </summary>
        public static Action<string> OnPurchaseStart;
        
        /// <summary>
        /// Purchase Success Event
        /// </summary>
        public static Action<string, TransactionResultModel, object> OnPurchaseSuccess;

        /// <summary>
        /// Purchase Fail Event
        /// </summary>
        public static Action<string, string> OnPurchaseFail;
        
        /// <summary>
        /// Shop refreshed event
        /// </summary>
        public static Action OnShopRefreshed;

        /// <summary>
        /// The shop config.
        /// </summary>
        protected ShopModel _shopModel;

        [SerializeField]
        /// <summary>
        /// the current available shop config.
        /// </summary>
        protected ShopModel _enabledShop;

        [SerializeField]
        /// <summary>
        /// The current stock for this player.
        /// </summary>
        protected List<PlayerShopItemModel> _playerShopItemModels;

        /// <summary>
        /// The purchase callback sucess.
        /// </summary>
        protected Action<TransactionResultModel> _purchaseCallbackSuccess;

        /// <summary>
        /// The purchase callback fail.
        /// </summary>
        protected Action<string> _purchaseCallbackFail;

        /// <summary>
        /// The shop item to purchase.
        /// </summary>
        protected ShopItemModel _shopItemToPurchase;

        /// <summary>
        /// Reference to the PlayerManager
        /// </summary>
        protected PlayerManager _playerManager;

        /// <summary>
        /// Reference to the AnalyticsManager
        /// </summary>
        protected AnalyticsManager _analyticsManager;

        /// <summary>
        /// Reference to the ItemsManager
        /// </summary>
        protected ItemsManager _itemsManager;

        /// <summary>
        /// Cached origin for this purchase
        /// </summary>
        protected string _purchaseOrigin = "";

        /// <summary>
        /// task timer to update the shop
        /// </summary>
        private Task _timerTask;

        /// <summary>
        /// cancellation token for the current timer
        /// </summary>
        private CancellationTokenSource _tokenSource;

        protected List<PendingPurchaseModel> _pendingValidatedPurchases = new List<PendingPurchaseModel>();
        
        /// <summary>
        /// Gets the iap service.
        /// </summary>
        /// <value>The iap service.</value>
        [Inject]
        public IIapService IapService { get; }

        /// <summary>
        /// Gets the iap validation service.
        /// </summary>
        /// <value>The iap validation service.</value>
        [Inject]
        private IIapValidationService IapValidationService { get; }

        protected override void OnEnable()
        {
            base.OnEnable();
            GameModeManager.OnGameModeChanged += GameModeChanged;
        }

        protected virtual void OnDisable()
        {
            GameModeManager.OnGameModeChanged -= GameModeChanged;
        }

        /// <summary>
        /// When the game mode changes, update the config
        /// </summary>
        /// <param name="newGameMode">The new game mode</param>
        protected virtual void GameModeChanged(string newGameMode, bool isRestart)
        {
            // Update the config
            InitializeModels();
            
            UpdateEnabledShop();
        }
        
        /// <summary>
        /// Ititializes the ShopManager
        /// </summary>
        public override void StartLoading()
        {
            // Cache the managers
            _playerManager = PlayerManager.Instance;
            _analyticsManager = AnalyticsManager.Instance;
            _itemsManager = ItemsManager.Instance;

            InitializeModels();

            LogHelper.LogSdk("Starting store connection", LogHelper.DEPENDENCY_MANAGER_TAG);

            if (IapService != null)
            {
                IapService.Connect((connected) =>
                {
                    UpdateEnabledShop();
                    LogHelper.LogSdk(string.Format("Store connection: {0}", connected), LogHelper.DEPENDENCY_MANAGER_TAG);
                    FinishedLoading(true);
                });
            }
            else
            {
                FinishedLoading(true);
            }
        }
        
        public virtual void ConnectionToStoreFailed()
        {
            OnConnectionToStoreFailed?.Invoke();
        }

        /// <summary>
        /// Initializes models.
        /// </summary>
        protected virtual void InitializeModels()
        {
            // Grab the config
            _shopModel = ConfigHelper.LoadConfig<ShopModel>(LimitedTimeEventsManager.Instance.GetConfigurationMapping("shop"));
            _enabledShop = new ShopModel();
            _enabledShop.ShopItems = new List<ShopItemModel>();
            _enabledShop.ShopSections = new List<ShopSectionModel>();
            
            string playerShop = PlayerManager.Instance.GetFromPlayerData("shop_stock", GameModeManager.CurrentGameMode);
            if (!string.IsNullOrEmpty(playerShop))
            {
                _playerShopItemModels = JsonHelper.DesarializeObject<List<PlayerShopItemModel>>(playerShop);
            }
            else
            {
                _playerShopItemModels = new List<PlayerShopItemModel>();
            }
        }

        protected virtual void UpdateTimers()
        {
            long shorterTimer = long.MaxValue;
            bool hasTimer = false;
            foreach (var shopItem in _enabledShop.ShopItems)
            {
                if (shopItem.Timer != null)
                {
                    if (shopItem.Timer.UpdateTimer(shopItem.Id))
                    {
                        RemovePlayerShopItem(shopItem.Id);
                    }

                    long remainingTime = shopItem.Timer.GetRemainingTime();
                    if (shorterTimer > remainingTime && remainingTime > 0)
                    {
                        shorterTimer = remainingTime;
                        hasTimer = true;
                    }
                }
            }

            foreach (var shopSection in _enabledShop.ShopSections)
            {
                if (shopSection.Timer != null)
                {
                    if (shopSection.Timer.UpdateTimer(shopSection.Id))
                    {
                        List<ShopItemModel> toReset = new List<ShopItemModel>();
                        foreach (var p in _playerShopItemModels)
                        {
                            ShopItemModel shopItemModel = GetShopItemModel(p.Id);
                            if (shopItemModel != null && shopItemModel.SectionId == shopSection.Id)
                            {
                                toReset.Add(shopItemModel);
                            }
                        }

                        foreach (var sim in toReset)
                            RemovePlayerShopItem(sim.Id);
                    }

                    long remainingTime = shopSection.Timer.GetRemainingTime();
                    if (shorterTimer > remainingTime)
                    {
                        shorterTimer = remainingTime;
                        hasTimer = true;
                    }
                }
            }

            if (!hasTimer)
                shorterTimer = 0;
            
            
            if (shorterTimer > 0)
            {
                try
                {
                    int miliseconds = Convert.ToInt32(shorterTimer * 1000);
                
                    if (_tokenSource != null)
                    {
                        _tokenSource.Cancel();
                    }

                    _tokenSource = new CancellationTokenSource();
                    _timerTask = Task.Delay(miliseconds, _tokenSource.Token);
                    _timerTask.ContinueWithOnMainThread(task =>
                    {
                        if (_timerTask.IsCompleted && !_timerTask.IsCanceled && !_timerTask.IsFaulted)
                        {
                            _tokenSource.Dispose();
                            _tokenSource = null;
                            UpdateEnabledShop();
                        }
                    });
                }
                catch (Exception e)
                {
                    LogHelper.LogWarning($"Update timer task exception: {e.Message}");
                    throw;
                }
            }
        }

        /// <summary>
        /// Updates the enabled shop
        /// </summary>
        public virtual void UpdateEnabledShop()
        {
            _enabledShop?.ShopItems?.Clear();
            _enabledShop?.ShopSections?.Clear();

            if (_shopModel != null && _shopModel.ShopItems != null)
            {
                foreach (ShopItemModel shopItemModel in _shopModel.ShopItems)
                {
                    if (shopItemModel != null && PrerequisiteHelper.CheckPrerequisites(shopItemModel.Prereqs) &&
                        (!IsSubscription(shopItemModel.Id) || CanSubscribe(shopItemModel.Id)))
                    {
                        _enabledShop?.ShopItems?.Add(shopItemModel);
                    }
                }
            }

            if (_shopModel != null && _shopModel.ShopSections != null)
            {
                foreach (ShopSectionModel shopSectionModel in _shopModel.ShopSections)
                {
                    if (shopSectionModel != null && PrerequisiteHelper.CheckPrerequisites(shopSectionModel.Prereqs))
                    {
                        _enabledShop?.ShopSections?.Add(shopSectionModel);
                    }
                }
            }
            UpdateTimers();
            OnShopRefreshed?.Invoke();
        }
        

        /// <summary>
        /// Gets the shop sections for a particular placement
        /// </summary>
        /// <param name="placement">The placement</param>
        /// <returns>The shop sections</returns>
        public virtual List<ShopSectionModel> GetShopSectionModelsForPlacement(string placement)
        {
            List<ShopSectionModel> sections = new List<ShopSectionModel>();
            foreach (var section in _enabledShop.ShopSections)
            {
                if (section.Placement == placement)
                {
                    sections.Add(section);
                }
            }
            return sections;
        }

        /// <summary>
        /// Gets all of the items for a particular section
        /// </summary>
        /// <param name="sectionId">The id of the section</param>
        /// <returns>The items for the section</returns>
        public virtual List<ShopItemModel> GetAllShopItemsForSection(string sectionId)
        {
            List<ShopItemModel> items = new List<ShopItemModel>();
            foreach (var item in _enabledShop.ShopItems)
            {
                if (item.SectionId == sectionId)
                {
                    items.Add(item);
                }
            }
            return items;
        }
        
        /// <summary>
        /// Gets the shop section config.
        /// </summary>
        /// <returns>The shop item config.</returns>
        /// <param name="shopSectionId">Shop section identifier.</param>
        public virtual ShopSectionModel GetShopItemSection(string shopSectionId)
        {
            foreach (var section in _shopModel.ShopSections)
            {
                if (section.Id == shopSectionId)
                {
                    return section;
                }
            }
            return null;
        }
        
        /// <summary>
        /// Gets all of the items for a particular section
        /// </summary>
        /// <returns>The items for the section</returns>
        public virtual List<ShopItemModel> GetAllShopItems()
        {
            return _shopModel.ShopItems;
        }

        /// <summary>
        /// Gets the shop item config.
        /// </summary>
        /// <returns>The shop item config.</returns>
        /// <param name="shopItemId">Shop item identifier.</param>
        public virtual ShopItemModel GetShopItemModel(string shopItemId)
        {
            foreach (var item in _shopModel.ShopItems)
            {
                if (item.Id == shopItemId)
                {
                    return item;
                }
            }
            return null;
        }
        
        /// <summary>
        /// Gets the player shop item.
        /// </summary>
        /// <returns>The player shop item.</returns>
        /// <param name="shopItemId">Shop item identifier.</param>
        public virtual PlayerShopItemModel GetPlayerShopItemModel(string shopItemId)
        {
            foreach (var playerShopItem in _playerShopItemModels)
            {
                if (playerShopItem.Id == shopItemId)
                {
                    return playerShopItem;
                }
            }
            return null;
        }

        /// <summary>
        /// Add an item to the stock of the player
        /// </summary>
        /// <param name="shopItemId">Shop item identifier.</param>
        /// <param name="amount">The stock amount.</param>
        /// <returns></returns>
        public virtual PlayerShopItemModel AddPlayerShopItemModel(string shopItemId, int amount)
        {
            PlayerShopItemModel playerShopItemModel = new PlayerShopItemModel() {Id = shopItemId, Amount = amount};
            _playerShopItemModels.Add(playerShopItemModel);
            PersistPlayerShopItems();
            return playerShopItemModel;
        }

        /// <summary>
        /// Update player shop item model (stock)
        /// </summary>
        /// <param name="shopItemId"></param>
        public virtual void UpdatePlayerShopItemModel(string shopItemId)
        {
            PlayerShopItemModel playerShopItemModel = GetPlayerShopItemModel(shopItemId);
            if (playerShopItemModel != null)
            {
                playerShopItemModel.Amount--;
                PersistPlayerShopItems();
            }
            else
            {
                LogHelper.LogError(string.Format("Player has no item {0} in player items", shopItemId), Environment.StackTrace);
            }

            
        }

        /// <summary>
        /// remove an item from the player shop
        /// </summary>
        /// <param name="playerShopItemModel"></param>
        public virtual void RemovePlayerShopItem(PlayerShopItemModel playerShopItemModel)
        {
            if (_playerShopItemModels.Contains(playerShopItemModel))
            {
                _playerShopItemModels.Remove(playerShopItemModel);
            }

            PersistPlayerShopItems();
        }
        
        /// <summary>
        /// remove an item from the player shop
        /// </summary>
        /// <param name="playerShopItemId"></param>
        public virtual void RemovePlayerShopItem(string playerShopItemId)
        {
            PlayerShopItemModel playerShopItemModel = GetPlayerShopItemModel(playerShopItemId);
            RemovePlayerShopItem(playerShopItemModel);
        }

        /// <summary>
        /// Resets an item from the player shop
        /// </summary>
        /// <param name="playerShopItemModel"></param>
        public virtual PlayerShopItemModel ResetPlayerShopItem(string shopItemId, int amount)
        {
            PlayerShopItemModel playerShopItemModel = GetPlayerShopItemModel(shopItemId);
            if (playerShopItemModel != null)
            {
                if (_playerShopItemModels.Contains(playerShopItemModel))
                {
                    _playerShopItemModels.Remove(playerShopItemModel);
                }
                PlayerShopItemModel newPlayerShopItemModel = new PlayerShopItemModel() {Id = shopItemId, Amount = amount};
                _playerShopItemModels.Add(newPlayerShopItemModel);
                PersistPlayerShopItems();
                return newPlayerShopItemModel;
            }

            return null;
        }

        /// <summary>
        /// Persist the player shop
        /// </summary>
        void PersistPlayerShopItems()
        {
            PlayerManager.Instance.SetPlayerData("shop_stock", new DataUpdateModel(JsonHelper.SerializeObject(_playerShopItemModels)), GameModeManager.CurrentGameMode);
        }

        /// <summary>
        /// Purchases the shop item.
        /// </summary>
        /// <param name="model">Model.</param>
        /// <param name="origin">Where was this purchase made.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        public virtual void PurchaseShopItem(ShopItemModel model, string origin = "", Action<TransactionResultModel> successCallback = null, Action<string> failureCallback = null)
        {
            _shopItemToPurchase = model;
            
            OnPurchaseStart?.Invoke(model.Id);
            
            // Grab the item
            BaseItemModel itemToPurchase = _itemsManager.GetItem(_shopItemToPurchase.ItemName);

            if (itemToPurchase != null)
            {
                if (_shopItemToPurchase.Cost != null && _shopItemToPurchase.Cost.IsIAP())
                {
                    string productSubscribed;
                    if (_shopItemToPurchase.Cost is RealMoneyCost rm && rm.ProductType == "Subscription" && HasExistingSubscriptionForGroup(model.Id, model.ItemName, out productSubscribed))
                    {
                        if (productSubscribed != model.Id)
                            UpdateSubscription(productSubscribed, model.Id, origin, successCallback, failureCallback);
                    }
                    else
                    {
                        PurchaseIAP(_shopItemToPurchase, origin, successCallback, failureCallback);
                    }
                }
                else
                {
                    ItemTransaction transaction = new ItemTransaction(itemToPurchase, 1, _shopItemToPurchase.Cost);
                    
                    TransactionResultModel result = transaction.ApplyTransaction(origin);
                    if (string.IsNullOrEmpty(result.Error))
                    {
                        _playerManager.SetCounter(string.Format(CountersConstants.ITEM_PURCHASED, _shopItemToPurchase.Id), 1, GameModeManager.CurrentGameMode, true);
                        OnPurchaseSuccess?.Invoke(_shopItemToPurchase.Id, result, null);
                        successCallback?.Invoke(result);
                        UpdateShop();
                    }
                    else
                    {
                        string errorMessage = string.Format("ShopManager.cs :: don't have enough currency for item {0}", _shopItemToPurchase.Id);
                        LogHelper.LogError(errorMessage, Environment.StackTrace);
                        OnPurchaseFail?.Invoke(_shopItemToPurchase.Id, errorMessage);
                        failureCallback?.Invoke(errorMessage);
                    }
                }
            }
            else
            {
                string errorMessage = string.Format("ShopManager.cs :: item {0} does not exist", _shopItemToPurchase.Id);
                LogHelper.LogError(errorMessage, Environment.StackTrace);
                OnPurchaseFail?.Invoke(_shopItemToPurchase.Id, errorMessage);
                failureCallback?.Invoke(errorMessage);
            }
        }


        /// <summary>
        /// Purchases the iap.
        /// </summary>
        /// <param name="shopItemToPurchase">Iap shopItem.</param>
        /// <param name="origin">Where was this purchase made.</param>
        /// <param name="callbackSuccess">Callback success.</param>
        /// <param name="callbackFail">Callback fail.</param>
        public virtual void PurchaseIAP(ShopItemModel shopItemToPurchase, string origin, Action<TransactionResultModel> callbackSuccess = null, Action<string> callbackFail = null)
        {
            LogHelper.LogSdk(string.Format("Starting puchase. IapId: {0}", shopItemToPurchase.Id), LogHelper.SHOP_TAG);

            // Cache the callbacks
            _purchaseCallbackSuccess = callbackSuccess;
            _purchaseCallbackFail = callbackFail;
            _purchaseOrigin = origin;

#if UNITY_EDITOR
            // Simulate a successfull call
            ValidationCallback(null, new IapValidationResultModel() {ItemId = shopItemToPurchase.Id});
#else
            if (IapService != null)
            {
                if (IapService.IsConnected())
                {
                    // Call the service
                    if (Application.platform == RuntimePlatform.Android)
                        IapService.PurchaseItemOnAndroid(shopItemToPurchase.Id, AndroidValidation, OnIapPurchaseFail);
                    else if (Application.platform == RuntimePlatform.IPhonePlayer)
                        IapService.PurchaseItemOnIos(shopItemToPurchase.Id, IosValidation, OnIapPurchaseFail);
        }
                else
                {
                    string errorMessage = "ShopManager.cs :: Trying to purchase the item but IAP service is not connected";
                    LogHelper.LogError(errorMessage, Environment.StackTrace);
                    OnIapPurchaseFail(shopItemToPurchase.Id, errorMessage);
                }
            }
            else
            {
                string errorMessage = "ShopManager.cs :: Trying to purchase IAP, but no IAPService is set";
                LogHelper.LogError(errorMessage, Environment.StackTrace);
                OnIapPurchaseFail(shopItemToPurchase.Id, errorMessage);
            }
#endif
        }

        /// <summary>
        /// Android validation.
        /// </summary>
        /// <param name="signature">Signature.</param>
        /// <param name="purchaseData">Purchase data.</param>
        public virtual void AndroidValidation (string shopItemId, string signature, string purchaseData, object purchase)
        {
            LogHelper.LogSdk("Purchase successful. Starting Google validation", LogHelper.SHOP_TAG);
            IapValidationService.GooglePlayPurchaseValidation(shopItemId, signature, purchaseData, purchase, ValidationCallback);
        }


        /// <summary>
        /// Ios validation.
        /// </summary>
        /// <param name="receipt">Receipt.</param>
        public virtual void IosValidation(string shopItemId, string receipt, object purchase)
        {
            LogHelper.LogSdk("Purchase successful. Starting iOS validation", LogHelper.SHOP_TAG);
            IapValidationService.IosPurchaseValidation(shopItemId, receipt, purchase, ValidationCallback);
        }
        
        /*public virtual void PendingAndroidValidation(string shopItemId, string signature, string purchaseData, object purchase)
        {
            LogHelper.LogSdk("Purchase successful. Starting Google validation", LogHelper.SHOP_TAG);
            IapValidationService.GooglePlayPurchaseValidation(shopItemId, signature, purchaseData, purchase, PendingValidationCallback);
        }
        
        public virtual void PendingIosValidation(string shopItemId, string receipt, object purchase)
        {
            LogHelper.LogSdk("Purchase successful. Starting iOS validation", LogHelper.SHOP_TAG);
            IapValidationService.IosPurchaseValidation(shopItemId, receipt, purchase, PendingValidationCallback);
        }*/

        /// <summary>
        /// Validation success callback
        /// </summary>
        /// <param name="result">Iap validation result.</param>
        protected virtual void ValidationCallback(object purchase, IapValidationResultModel result)
        {
            if (string.IsNullOrEmpty(result.Error) && !string.IsNullOrEmpty(result.ItemId))
            {
                LogHelper.LogSdk("Validation succesful", LogHelper.SHOP_TAG);

                _shopItemToPurchase = GetShopItemModel(result.ItemId);
                
                BaseItemModel itemToPurchase = _itemsManager.GetItem(_shopItemToPurchase.ItemName);
                
                ItemTransaction transaction = new ItemTransaction(itemToPurchase);

                TransactionResultModel transactionResultModel = transaction.ApplyTransaction("PurchaseShopItem");
                if (IsSubscription(_shopItemToPurchase.Id))
                    transactionResultModel = HandleSubscription(itemToPurchase.ItemName);

                IapService?.PurchaseCompleted(_shopItemToPurchase.Id);
                
                _playerManager.SetCounter(string.Format(CountersConstants.ITEM_PURCHASED, _shopItemToPurchase.Id), 1, GameModeConstants.MAIN_GAME, true);
                
                if(_shopItemToPurchase.Cost != null && _shopItemToPurchase.Cost is RealMoneyCost rm)
                {
                    _playerManager.SetCounter(CountersConstants.REAL_MONEY_SPENT, rm.RealMoneyValue, GameModeConstants.MAIN_GAME,true);  
                    _playerManager.SetFlag(FlagsConstants.IS_SPENDER, true, GameModeConstants.MAIN_GAME);

                    // Track this purchase
                    _analyticsManager.TrackIapPurchase(itemToPurchase, rm, _purchaseOrigin);
                }
                
                OnPurchaseSuccess?.Invoke(_shopItemToPurchase.Id, transactionResultModel, purchase);
                if (_purchaseCallbackSuccess == null)
                {
                    PendingPurchaseModel pendingPurchaseModel = new PendingPurchaseModel()
                        {ShopItemId = _shopItemToPurchase.Id, ResultModel = transactionResultModel, Purchase = purchase};
                    _pendingValidatedPurchases.Add(pendingPurchaseModel);
                }
                else
                {
                    _purchaseCallbackSuccess.Invoke(transactionResultModel);
                }
                UpdateShop();
                
                _analyticsManager.TrackCustomProperty(FlagsConstants.IS_SPENDER, "Yes");
            }
            else
            {
                string errorMessage = $"ShopManager.cs :: Item: {result.ItemId}, Validation error: {result.Error}";

                LogHelper.LogError(errorMessage, Environment.StackTrace);

                OnPurchaseFail?.Invoke(result.ItemId, result.Error);
                _purchaseCallbackFail?.Invoke(result.Error);
            }
        }
        
        
        /// <summary>
        /// Validation success callback
        /// </summary>
        /// <param name="result">Iap validation result.</param>
        protected virtual void PendingValidationCallback(object purchase, IapValidationResultModel result)
        {
            if (string.IsNullOrEmpty(result.Error) && !string.IsNullOrEmpty(result.ItemId))
            {
                LogHelper.LogSdk("Pending Validation succesful", LogHelper.SHOP_TAG);

                if (_shopItemToPurchase == null)
                {
                    _shopItemToPurchase = GetShopItemModel(result.ItemId);
                }
                BaseItemModel itemToPurchase = _itemsManager.GetItem(_shopItemToPurchase.ItemName);
                
                ItemTransaction transaction = new ItemTransaction(itemToPurchase);

                TransactionResultModel transactionResultModel = transaction.ApplyTransaction("PurchaseShopItem");
                if (IsSubscription(_shopItemToPurchase.Id))
                    transactionResultModel = HandleSubscription(itemToPurchase.ItemName);

                IapService?.PurchaseCompleted(_shopItemToPurchase.Id);
                
                _playerManager.SetCounter(string.Format(CountersConstants.ITEM_PURCHASED, _shopItemToPurchase.Id), 1, GameModeConstants.MAIN_GAME, true);
                
                if(_shopItemToPurchase.Cost != null && _shopItemToPurchase.Cost is RealMoneyCost rm)
                {
                    _playerManager.SetCounter(CountersConstants.REAL_MONEY_SPENT, rm.RealMoneyValue, GameModeConstants.MAIN_GAME,true);  
                    _playerManager.SetFlag(FlagsConstants.IS_SPENDER, true, GameModeConstants.MAIN_GAME);

                    // Track this purchase
                    _analyticsManager.TrackIapPurchase(itemToPurchase, rm, _purchaseOrigin);
                }
                OnPurchaseSuccess?.Invoke(_shopItemToPurchase.Id, transactionResultModel, purchase);
                
                UpdateShop();
                
                _analyticsManager.TrackCustomProperty(FlagsConstants.IS_SPENDER, "Yes");
            }
            else
            {
                string errorMessage = $"ShopManager.cs :: Item: {result.ItemId},Pending Validation error: {result.Error}";

                LogHelper.LogError(errorMessage, Environment.StackTrace);
                OnPurchaseFail?.Invoke(_shopItemToPurchase.Id, result.Error);
                _purchaseCallbackFail?.Invoke(result.Error);
            }
        }


        public PendingPurchaseModel GetPendingPurchase(string shopItemId)
        {
            foreach (var pending in _pendingValidatedPurchases)
            {
                if (pending.ShopItemId == shopItemId)
                    return pending;
            }
            return null;
        }

        public List<PendingPurchaseModel> GetPendingPurchases()
        {
            return _pendingValidatedPurchases;
        }

        public void ClearPendingPurchase(PendingPurchaseModel pendingPurchaseModel)
        {
            if (_pendingValidatedPurchases.Contains(pendingPurchaseModel))
                _pendingValidatedPurchases.Remove(pendingPurchaseModel);
        }

        public void ClearPendingPurchases()
        {
            _pendingValidatedPurchases.Clear();
        }

        private void UpdateShop()
        {
            var playerShopItemModel = GetPlayerShopItemModel(_shopItemToPurchase.Id);
            if (playerShopItemModel != null)
            {
                UpdatePlayerShopItemModel(playerShopItemModel.Id);
            }

            UpdateEnabledShop();
        }

        /// <summary>
        /// Gets the localized price for an IAP
        /// </summary>
        /// <param name="skuId">The SKU for the iap</param>
        /// <returns>The price in the local currency of the player</returns>
        public virtual string GetLocalizedPrice(string skuId)
        {
            return IapService?.GetLocalizedPrice(skuId);
        }

        /// <summary>
        /// If in iOS, tries to restore previous purchases made
        /// </summary>
        public virtual void RestorePurchases()
        {
            #if UNITY_IOS
                LogHelper.LogSdk("Trying to restore purchases...", LogHelper.SHOP_TAG);
                IapService?.RestorePurchases(IosValidation);
            #endif
        }

        public bool IsSubscription(string shopItemId)
        {
            var shopItem = GetShopItemModel(shopItemId);
            return shopItem is { Cost: RealMoneyCost { ProductType: "Subscription" } };
        }

        public object GetSubscriptionInfo(string productId)
        {
            return IapService?.GetSubscriptionInfo(productId);
        }

        public bool IsSubscriptionActive(string productId)
        {
            if (IapService != null)
                return IapService.IsSubscriptionActive(productId);
            return false;
        }
        
        public bool IsSubscriptionAutoRenewing(string productId)
        {
            if (IapService != null)
                return IapService.IsSubscriptionAutoRenewing(productId);
            return false;
        }

        public long GetSubscriptionRemainingTime(string productId)
        {
            if (IapService != null)
                return IapService.GetSubscriptionRemainingTime(productId);
            return 0;
        }
        
        public DateTime GetSubscriptionExpirationDate(string productId)
        {
            if (IapService != null)
                return IapService.GetSubscriptionExpirationDate(productId);
            return TimeHelper.GetTimeInServerDateTime();
        }

        public void UpdateSubscription(string oldProductId, string newProductId, string origin, Action<TransactionResultModel> successCallback = null, Action<string> failureCallback = null)
        {
            _purchaseCallbackSuccess = successCallback;
            _purchaseCallbackFail = failureCallback;
            _purchaseOrigin = origin;
#if UNITY_EDITOR
            ValidationCallback(null, new IapValidationResultModel() {ItemId = newProductId});
#else
            if (IapService != null)
            {
                if (IapService.IsConnected())
                {
                    if (Application.platform == RuntimePlatform.Android)
                        IapService?.UpdateSubscriptionOnAndroid(oldProductId, newProductId, AndroidValidation, OnIapPurchaseFail);
                    else if (Application.platform == RuntimePlatform.IPhonePlayer)
                        IapService?.UpdateSubscriptionOnIos(newProductId, IosValidation, OnIapPurchaseFail);
                }
                else
                {
                    string errorMessage = "ShopManager.cs :: Trying to purchase the item but IAP service is not connected";
                    LogHelper.LogError(errorMessage, Environment.StackTrace);
                    OnIapPurchaseFail(newProductId, errorMessage);
                }
            }
            else
            {
                string errorMessage = "ShopManager.cs :: Trying to purchase IAP, but no IAPService is set";
                LogHelper.LogError(errorMessage, Environment.StackTrace);
                OnIapPurchaseFail(newProductId, errorMessage);
            }
#endif
        }

        protected void OnIapPurchaseFail(string shopItemId, string error)
        {
            OnPurchaseFail?.Invoke(shopItemId, error);
            _purchaseCallbackFail?.Invoke(error);
        }

        public bool CanSubscribe(string productId)
        {
            if (IapService != null)
                return IapService.CanSubscribe(productId);
            return false;
        }

        protected bool HasExistingSubscriptionForGroup (string productId, string itemName, out string subscribedProduct)
        {
            bool result = false;
            subscribedProduct = null;
            if (CanSubscribe(productId))
            {
                result = HasExistingSubscriptionForGroup(itemName, out subscribedProduct);
            }
            return result;
        }

        public bool HasExistingSubscriptionForGroup(string itemName, out string subscribedProduct)
        {
            subscribedProduct = null;
            List<ShopItemModel> subscriptionGroup = GetSubscriptionsByGroup(itemName);
            foreach (var s in subscriptionGroup)
            {
                if (IapService != null && IapService.IsConnected() && !CanSubscribe(s.Id))
                {
                    subscribedProduct = s.Id;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns all the subscriptions that grant the same reward but with different billing period.
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public List<ShopItemModel> GetSubscriptionsByGroup(string itemName)
        {
            List<ShopItemModel> items = new List<ShopItemModel>();
            foreach (var shopItemModel in _shopModel.ShopItems)
            {
                if (shopItemModel.Cost is RealMoneyCost rm && rm.ProductType == "Subscription" && shopItemModel.ItemName == itemName)
                    items.Add(shopItemModel);
            }

            return items;
        }
        

        List<string> GetSubscriptionsGroups()
        {
            List<string> result = new List<string>();
            foreach (var shopItemModel in _shopModel.ShopItems)
            {
                if (shopItemModel.Cost is RealMoneyCost rm && rm.ProductType == "Subscription" && !result.Contains(shopItemModel.ItemName))
                    result.Add(shopItemModel.ItemName);
            }
            return result;
        }

        public virtual bool ShouldGiveSubscriptionReward(string itemName)
        {
            if (ItemsManager.Instance.GetItem(itemName) is SubscriptionItemModel subscriptionItem)
            {
                return subscriptionItem.ShouldGiveRewards();
            }

            return false;
        }

        public virtual Dictionary<string, TransactionResultModel> GetSubscriptionRewards()
        {
            Dictionary<string, TransactionResultModel> result = new Dictionary<string, TransactionResultModel>();
            var groups = GetSubscriptionsGroups();
            foreach (var g in groups)
            {
                TransactionResultModel trm = HandleSubscription(g);
                if (trm.Currencies.Count > 0 || trm.Items.Count > 0)
                    result.Add(g, trm);
            }

            return result;
        }

        public virtual TransactionResultModel HandleSubscription(string itemName)
        {
            if (ItemsManager.Instance.GetItem(itemName) is SubscriptionItemModel subscriptionItem)
            {
                return subscriptionItem.HandleSubscription();
            }
            else
            {
                LogHelper.LogSdk(itemName +" is not a subscription item", LogHelper.SHOP_TAG);
                return new TransactionResultModel();
            }
        }
    }
}
