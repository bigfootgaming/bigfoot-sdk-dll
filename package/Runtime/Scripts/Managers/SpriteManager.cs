﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.U2D;
using BigfootSdk.Helpers;
using BigfootSdk.LTE;

namespace BigfootSdk.Sprites
{
    /// <summary>
    /// Sprite manager. Manages sprites and sprite atlases through Adressables.
    /// </summary>
    public class SpriteManager : LoadableSingleton<SpriteManager>
    {
        /// <summary>
        /// The default sprite key. Used when a requested sprite is missing.
        /// </summary>
        public string DefaultSpriteKey;

        protected Dictionary<string, SpriteModel> _spriteModels = new Dictionary<string, SpriteModel>();
        
        /// <summary>
        /// On Enable, subscribe to SpriteAtlasManager.atlasRequested
        /// </summary>
        protected override void OnEnable()
        {
            base.OnEnable();
            SpriteAtlasManager.atlasRequested += SpriteAtlasRequested;
        }

        /// <summary>
        /// On Disable, unsubscribe to SpriteAtlasManager.atlasRequested
        /// </summary>
        private void OnDisable()
        {
            SpriteAtlasManager.atlasRequested -= SpriteAtlasRequested;
        }

        public override void StartLoading()
        {
            LoadSpriteModels();
        }

        /// <summary>
        /// Sprite atlas requested event.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="callback">Callback.</param>
        void SpriteAtlasRequested (string key, Action<SpriteAtlas> callback)
        {
            LogHelper.LogSdk(string.Format("Sprite atlas requested. Key: {0}", key), LogHelper.SPRITE_MANAGER_TAG);
            AddressablesHelper.LoadAssetAsync(key, callback);
        }

        /// <summary>
        /// Gets the sprite.
        /// </summary>
        /// <param name="spriteKey">Sprite key.</param>
        /// <param name="callback">Callback.</param>
        /// <param name="changeInEvent">If this sprite is different during an event</param>
        public virtual async Task GetMappedSprite(string spriteKey, Action<Sprite> callback, bool changeInEvent = false)
        {
            if (changeInEvent && GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
            {
                spriteKey = string.Format("{0}_{1}", LimitedTimeEventsManager.Instance.ActiveEvent.Name, spriteKey);
            }
            
            if (_spriteModels.ContainsKey(spriteKey))
            {
                var spriteModel = _spriteModels[spriteKey];
                if (!string.IsNullOrEmpty(spriteModel.SpriteAtlasKey))
                {
                    await GetSpriteFromSpriteAtlas(spriteModel.SpriteAtlasKey, spriteModel.SpriteKey, callback, false);
                }
                else
                {
                    await GetSprite(spriteModel.SpriteKey, callback, false);
                }
            }
            else
            {
                string errorMessage = string.Format("SpriteManager.cs :: Missing mapped sprite {0}", spriteKey);
                LogHelper.LogError(errorMessage, Environment.StackTrace);
                await GetDefaultSprite(callback);
            }
        }

        /// <summary>
        /// Gets the sprite.
        /// </summary>
        /// <param name="spriteKey">Sprite key.</param>
        /// <param name="callback">Callback.</param>
        /// <param name="changeInEvent">If this sprite is different during an event</param>
        public virtual async Task GetSprite (string spriteKey, Action<Sprite> callback, bool changeInEvent = false)
        {
            if (changeInEvent && GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
            {
                spriteKey = string.Format("{0}_{1}", LimitedTimeEventsManager.Instance.ActiveEvent.Name, spriteKey);
            }
            try
            {
                var resultingSprite = await AddressablesHelper.LoadAssetAsync<Sprite>(spriteKey);
                if (resultingSprite == null)
                {
                    string errorMessage = string.Format("SpriteManager.cs :: Missing sprite {0}", spriteKey);
                    LogHelper.LogError(errorMessage, Environment.StackTrace);
                    await GetDefaultSprite(callback);
                    return;
                }

                callback(resultingSprite);
            }
            catch (Exception e)
            {
                string errorMessage = string.Format("SpriteManager.cs :: Missing sprite {0}", spriteKey);
                LogHelper.LogError(errorMessage, Environment.StackTrace);
                GetDefaultSprite(callback);
            }
        }

        /// <summary>
        /// Gets the sprite from sprite atlas.
        /// </summary>
        /// <param name="spriteAtlasKey">Sprite atlas key.</param>
        /// <param name="spriteKey">Sprite name.</param>
        /// <param name="callback">Callback.</param>
        /// <param name="changeInEvent">If this sprite is different during an event</param>
        public virtual async Task GetSpriteFromSpriteAtlas (string spriteAtlasKey, string spriteKey, Action<Sprite> callback, bool changeInEvent = false)
        {
            if (changeInEvent && GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
            {
                spriteKey = string.Format("{0}_{1}", LimitedTimeEventsManager.Instance.ActiveEvent.Name, spriteKey);
            }

            try
            {
                var resultingSpriteAtlas = await AddressablesHelper.LoadAssetAsync<SpriteAtlas>(spriteAtlasKey);
                if (resultingSpriteAtlas == null)
                {
                    string errorMessage = string.Format("SpriteManager.cs :: Missing sprite {0}[{1}]", spriteAtlasKey, spriteKey);
                    LogHelper.LogError(errorMessage, Environment.StackTrace);
                    GetDefaultSprite(callback);
                }
                else
                {
                    callback(resultingSpriteAtlas.GetSprite(spriteKey));
                }
            }
            catch (Exception e)
            {
                string errorMessage = string.Format("SpriteManager.cs :: Missing sprite {0}[{1}]", spriteAtlasKey, spriteKey);
                LogHelper.LogError(errorMessage, Environment.StackTrace);
                GetDefaultSprite(callback);
            }
           
        }

        /// <summary>
        /// Gets the default sprite.
        /// </summary>
        /// <param name="callback">Callback.</param>
        async Task GetDefaultSprite (Action<Sprite> callback)
        {
            await AddressablesHelper.LoadAssetAsync(DefaultSpriteKey, callback);
        }

        /// <summary>
        /// Releases the sprite. This should be used only with sprites that are not included in sprite atlases.
        /// </summary>
        /// <param name="sprite">Sprite.</param>
        public void ReleaseSprite (Sprite sprite)
        {
            Addressables.Release(sprite);
        }

        public void ReleaseSpriteAtlas(string key)
        {
            AddressablesHelper.Release(key);
        }

        /// <summary>
        /// Load Addressable Sprites.csv File
        /// </summary>
        void LoadSpriteModels()
        {
            AddressablesHelper.LoadAssetAsync<TextAsset>("Sprites", (textAsset) =>
            {
                if (textAsset != null)
                {
                    ParseSpriteModels(textAsset.text);
                }

                FinishedLoading(true);
            });
        }
        
        public void ParseSpriteModels(string result)
        {
            string[] lines = Regex.Split (result, "\n|\r|\r\n" );
            foreach (var line in lines)
            {
                string[] values = Regex.Split ( line, "," );
                if (!string.IsNullOrEmpty(values[0]) && !_spriteModels.ContainsKey(values[0]))
                {
                    if(values.Length == 3)
                        _spriteModels.Add(values[0], new SpriteModel(){SpriteKey = values[1], SpriteAtlasKey = values[2]});
                    else if (values.Length == 2)
                        _spriteModels.Add(values[0], new SpriteModel(){SpriteKey = values[1]});
                }
            }
        }
    }

    [Serializable]
    public class SpriteModel
    {
        /// <summary>
        /// The key for the sprite
        /// </summary>
        public string SpriteKey;

        /// <summary>
        /// The key for the sprite atlas
        /// </summary>
        public string SpriteAtlasKey;
    }
}
