﻿using System;
using System.Collections.Generic;
using BigfootSdk.Backend;
using Sirenix.OdinInspector;

namespace BigfootSdk
{
    /// <summary>
    /// Manager to handle all of the In-game Notifications
    /// </summary>
    public class InGameNotificationManager : LoadableSingleton<InGameNotificationManager>
    {
        /// <summary>
        /// Event to show notificatin
        /// </summary>
        public static Action<string> OnShowNotification;
        
        /// <summary>
        /// Event to hide notification
        /// </summary>
        public static Action<string> OnHideNotification;
        
        /// <summary>
        /// Ref count of all the active notifications
        /// </summary>
        [ShowInInspector, DictionaryDrawerSettings(IsReadOnly = true)]
        private Dictionary<string, int> _activeNotifications = new Dictionary<string, int>();
        
        public override void StartLoading()
        {
            // Check if we have any initial notifications to show
            CheckInitialNotifications();
            
            FinishedLoading(true);
        }

        /// <summary>
        /// Class to check on start to see if we should show any notification on startup
        /// </summary>
        protected virtual void CheckInitialNotifications()
        {
            
        }

        /// <summary>
        /// Clears a notification
        /// </summary>
        /// <param name="notificationId">The id of the notification</param>
        /// <returns>If a notification was clear or not</returns>
        public virtual bool ClearNotification(string notificationId)
        {
            // If we don't have an active notification with that id, return
            if (!_activeNotifications.ContainsKey(notificationId))
            {
                return false;
            }
            
            // If we only have one ref count, remove it, and hide the notification
            if(_activeNotifications[notificationId] == 1)
            {
                _activeNotifications.Remove(notificationId);
                OnHideNotification?.Invoke(notificationId);
                return true;
            }

            // If we have more than one, just decrement by one
            if (_activeNotifications[notificationId] > 1)
            {
                _activeNotifications[notificationId]--;
                return true;
            }

            return false;
        }
        
        /// <summary>
        /// Shows a notification
        /// </summary>
        /// <param name="notificationId"> The id of the notification</param>
        public virtual void ShowNotification(string notificationId)
        {
            // Add/Increment the ref count
            if (!_activeNotifications.ContainsKey(notificationId))
            {
                _activeNotifications.Add(notificationId, 1);
            }
            else
            {
                _activeNotifications[notificationId]++;
            }
            
            OnShowNotification?.Invoke(notificationId);
        }

        /// <summary>
        /// Checks if there is any active notification
        /// </summary>
        /// <param name="notificationId">The notification id</param>
        /// <returns>True if there is an active notification</returns>
        public virtual bool HasActiveNotification(string notificationId)
        {
            return _activeNotifications.ContainsKey(notificationId) && _activeNotifications[notificationId] > 0;
        }
    }
}

