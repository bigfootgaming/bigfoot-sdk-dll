﻿using System;
using System.Collections.Generic;
using BigfootSdk.Analytics;
using BigfootSdk.Backend;
using BigfootSdk.SceneManagement;

namespace BigfootSdk
{
	public delegate void GameModeDelagate(string gameMode, bool isRestart);
    /// <summary>
    /// Manager to handle everything relate to the different game modes
    /// </summary>
    public static class GameModeManager
	{
		/// <summary>
		/// Event for when the GameMode is starting to change
		/// </summary>
		public static Action<string> OnGameModeStartedChanging;
		
		/// <summary>
		/// Event for when the GameMode is changed
		/// </summary>
		public static GameModeDelagate OnGameModeChanged;
		
		/// <summary>
		/// Hold the current game mode
		/// </summary>
		private static string _currentGameMode = GameModeConstants.MAIN_GAME;
		public static string CurrentGameMode => _currentGameMode;

		/// <summary>
		/// Change to the other GameMode
		/// </summary>
		/// <param name="sceneToLoad">The scene to load</param>
		public static void ChangeGameMode(string sceneToLoad = "", bool isRestart = false)
		{
			if(_currentGameMode == GameModeConstants.MAIN_GAME)
				ChangeToLimitedTimeEvent(sceneToLoad);
			else if (_currentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
				ChangeToMainGame(sceneToLoad, isRestart);
		}
		
		/// <summary>
		/// Change to the LimitedTimeEvent GameMode
		/// </summary>
		/// <param name="sceneToLoad">The scene to load</param>
		public static void ChangeToLimitedTimeEvent(string sceneToLoad = "")
		{
			// Make sure we persist the data before switching
			PlayerManager.Instance.SaveLocalFile();
			
			// Send the event that we started changing modes
			OnGameModeStartedChanging?.Invoke(GameModeConstants.LIMITED_TIME_EVENT);
			
			// Change the game mode
			_currentGameMode = GameModeConstants.LIMITED_TIME_EVENT;
			
			// Remove all of the modifiers
			GameplayModifiersManager.Instance.RemoveAllGameplayModifiers();
			
			// Send the event
			OnGameModeChanged?.Invoke(GameModeConstants.LIMITED_TIME_EVENT, false);
			
			// Track the event
			AnalyticsManager.Instance.TrackCustomEvent("GameModeChanged", new Dictionary<string, object>{{"NewGameMode", GameModeConstants.LIMITED_TIME_EVENT}});
			
			// Change the scene
			if (!string.IsNullOrEmpty(sceneToLoad))
			{
				BigfootSceneManager.LoadScene(sceneToLoad);
			}
		}
		
		/// <summary>
		/// Change to the MainGame GameMode
		/// </summary>
		/// <param name="sceneToLoad">The scene to load</param>
		public static void ChangeToMainGame(string sceneToLoad = "", bool isRestart = false)
		{
			// Make sure we persist the data before switching
			PlayerManager.Instance.SaveLocalFile();
			
			// Send the event that we started changing modes
			OnGameModeStartedChanging?.Invoke(GameModeConstants.MAIN_GAME);
			
			// Change the game mode
			_currentGameMode = GameModeConstants.MAIN_GAME;
			
			// Remove all of the modifiers
			GameplayModifiersManager.Instance.RemoveAllGameplayModifiers();
			
			// Send the event
			OnGameModeChanged?.Invoke(GameModeConstants.MAIN_GAME, isRestart);

			// Track the event
			AnalyticsManager.Instance.TrackCustomEvent("GameModeChanged", new Dictionary<string, object>{{"NewGameMode", GameModeConstants.MAIN_GAME}});

			// Change the scene
			if (!string.IsNullOrEmpty(sceneToLoad))
			{
				BigfootSceneManager.LoadScene(sceneToLoad);
			}
		}
		
		/// <summary>
		/// Reload the main game quickly
		/// </summary>
		/// <param name="sceneToLoad">The scene to load</param>
		public static void HotReload(string sceneToLoad = "")
		{
			PlayerManager.Instance.LoadLocalPlayer();
			
			// Send the event that we started changing modes
			OnGameModeStartedChanging?.Invoke(GameModeConstants.MAIN_GAME);
			
			// Change the game mode
			_currentGameMode = GameModeConstants.MAIN_GAME;
			
			// Remove all of the modifiers
			GameplayModifiersManager.Instance.RemoveAllGameplayModifiers();
			
			// Send the event
			OnGameModeChanged?.Invoke(GameModeConstants.MAIN_GAME, true);

			// Change the scene
			if (!string.IsNullOrEmpty(sceneToLoad))
			{
				BigfootSceneManager.LoadScene(sceneToLoad);
			}
		}

	}
}