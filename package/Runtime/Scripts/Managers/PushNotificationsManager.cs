﻿using System;

namespace BigfootSdk.Notifications
{
    public class PushNotificationsManager : ILoadable
    {
#pragma warning disable 0649
        private IPushNotificationsService _service;
#pragma warning restore 0649

        public static Action<object> OnPushNotificationOpened;
        
        public override void StartLoading()
        {
#if FIREBASE_PUSH_NOTIFICATIONS
            _service = new FirebasePushNotificactionsService();
#endif
            _service?.Init();
            FinishedLoading(true);
        }

        private void OnDestroy()
        {
            Reset();
        }

        void Reset()
        {
            _service?.Reset();
        }
    }

}

