﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Globalization;
using System.Threading.Tasks;
using BigfootSdk.Analytics;
using BigfootSdk.Extensions;
using BigfootSdk.Helpers;
using BigfootSdk.LTE;
using CodeStage.AntiCheat.ObscuredTypes;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Manager in charge of everything related to the player, including currencies, id, player data, etc
	/// </summary>
	public partial class PlayerManager : Singleton<PlayerManager>
	{
		public static Action OnSimultaneousSessionsDetected;
		/// <summary>
        /// Event for when the user time travels
        /// </summary>
        public static Action<int> OnTimeTravelApplied;
		
		/// <summary>
		/// Event for when the player id is set from authentication
		/// </summary>
		public static Action<string> OnPlayerIdSet;

		/// <summary>
		/// Event for when the player data is loaded.
		/// </summary>
		public static Action OnPlayerLoaded;

		/// <summary>
		/// Event for when an update comes in from server.
		/// </summary>
		public static Action OnPlayerUpdated;

        /// <summary>
        /// Event for when new items are added
        /// </summary>
        public static Action<List<PlayerItemModel>> OnItemsAdded;

        /// <summary>
        /// Event for when an item was removed
        /// </summary>
        public static Action OnItemsRemoved;

        /// <summary>
        /// Event for when we detect the player has cheated
        /// </summary>
        public static Action OnPlayerHasCheated;
        
        /// <summary>
        /// Event for when save data to local storange fails.
        /// </summary>
        public static Action OnSavePlayerDataFailed;
        
        public static Action<long> OnServerSaveStarted;
        
        public static Action<long> OnServerSaveCompleted;
        
        /// <summary>
        /// The PlayerModel
        /// </summary>
		protected PlayerModel Player = new PlayerModel ();

        /// <summary>
        /// The file that stores player data locally.
        /// </summary>
        protected ES3File _playerFile;

        /// <summary>
        /// extra save method to be called on quit.
        /// </summary>
        protected  Action _saveMethod;

		/// <summary>
		/// The player service interface.
		/// </summary>
		protected IPlayerService _playerService;

		/// <summary>
		/// is true if the player data was deleted.
		/// </summary>
		protected bool _playerDataDeleted = false;

		protected bool _localPlayerDataDeleted = false;

		/// <summary>
		/// list of save times
		/// </summary>
		protected List<ObscuredLong> _saves;
		
		/// <summary>
		/// The file that stores save times locally.
		/// </summary>
		protected ES3File _savesFile;

		/// <summary>
		/// if true, we dectected the player has cheated.
		/// </summary>
		protected bool _cheated;

		/// <summary>
		/// returns _cheated;
		/// </summary>
		public bool Cheated => _cheated;

		/// <summary>
		/// Returns true if the IS_OLD_PLAYER flags doesn't exist
		/// </summary>
		public bool FirstOpen
		{
			get
			{
				return (!GetPlayerInfoModel(GameModeConstants.MAIN_GAME).Flags.ContainsKey(FlagsConstants.IS_OLD_PLAYER));
			}
		}

		/// <summary>
		/// returns true if the player data is loaded
		/// </summary>
		protected bool _isLoaded;

		/// <summary>
		/// returns _isLoaded
		/// </summary>
		public bool IsLoaded => _isLoaded;

		protected string _localPlayerId;

		public string LocalPlayerId => _localPlayerId;

		protected string _serverPlayerId;

		public string ServerPlayerId => _serverPlayerId;

		protected IPlayerService PlayerService {
			get { 
				if (_playerService == null) {
                    #if BACKEND_AWS
                        _playerService = new AWSPlayerService();
                    #elif BACKEND_GS
                        _playerService = new GSPlayerService ();
					#elif BACKEND_FIREBASE_DATABASE
                        _playerService = new FirebaseDatabasePlayerService();
					#elif BACKEND_FIREBASE_FIRESTORE
                        _playerService = new FirebaseFirestorePlayerService();
					#endif
					
				}
				return _playerService;
			}
		}

		protected string _customFilePath;

		protected float _secondsToSaveLocal;

		
		public bool SyncAvailable { set; protected get; }
		
		public bool SaveAvailable { set; protected get; }

		/// <summary>
		/// Initializes the player from the local backend, filling in the PlayerModel in the PlayerManager
		/// </summary>
		public virtual void LoadLocalPlayer()
		{
			LogHelper.LogSdk ("Loading Local Player...", LogHelper.DEPENDENCY_MANAGER_TAG);
			_secondsToSaveLocal = SdkManager.Instance.Configuration.BackendConfiguration
				.SecondsBetweenPlayerDataLocalSaves;
			_customFilePath = PlayerPrefs.GetString("PROGRESS_FILE_TO_LOAD", null);
			LoadLocalFile(_customFilePath);
			_isLoaded = true;
			LogHelper.LogSdk ("Local Player Finished Loading...", LogHelper.DEPENDENCY_MANAGER_TAG);
			OnPlayerLoaded?.Invoke();

			Invoke("SaveLocalFile", _secondsToSaveLocal);
		}

		public virtual void LoadPlayerItemsModifiers()
		{
			// Apply the items owned by the player that are GameplayModifiers	
			ApplyItemsOfGameplayModifierType();
		}

		public virtual void LoadServerPlayer(Action<bool> callback)
		{
			SaveAvailable = true;
			SyncAvailable = true;
			LogHelper.LogSdk ("Loading Server player...", LogHelper.DEPENDENCY_MANAGER_TAG);
			if (!string.IsNullOrEmpty(Player.PlayerId))
				PlayerService?.SetUserId(Player.PlayerId);
			BackendConfiguration backendConfiguration = SdkManager.Instance.Configuration.BackendConfiguration;
			if (backendConfiguration.SkipServerPlayerDataInFirstSessions &&
			    BackendManager.Instance.UseLocalPlayerData)
			{
				LogHelper.LogSdk ("Server player skipped during first sessions");
				callback?.Invoke(true);
			}
			else
			{
				PlayerService?.InitializePlayer (!backendConfiguration.AllowOfflineLoad, (success) => 
				{
					if (success && string.IsNullOrEmpty(_customFilePath))
					{
						OnPlayerLoaded?.Invoke();
					}
					LogHelper.LogSdk ("Server player Finished Loading: "+success, LogHelper.DEPENDENCY_MANAGER_TAG);
				
					callback?.Invoke(success);
					ClearSaves();
				});
			}
		}
		
		protected virtual void OnEnable()
		{
			GameModeManager.OnGameModeChanged += GameModeChanged;
		}

		protected virtual void OnDisable()
		{
			GameModeManager.OnGameModeChanged -= GameModeChanged;
		}

		/// <summary>
		/// Called when the game mode changed
		/// </summary>
		protected virtual void GameModeChanged(string newGameMode, bool isRestart)
		{
			// Apply the items owned by the player that are GameplayModifiers	
			ApplyItemsOfGameplayModifierType();
		}

		/// <summary>
		/// Loads player data from local storage.
		/// </summary>
		void LoadLocalFile(string customFileName)
		{
			if (!string.IsNullOrEmpty(customFileName))
			{
				PlayerPrefs.SetString("PROGRESS_FILE_TO_LOAD", null);
				ProgressFileHelper.LoadFileAsJson(customFileName, LoadLocalFileFromJson, LoadLocalFileInternal);
			}
			else
			{
				LoadLocalFileInternal();
			}
		}

		private void LoadLocalFileInternal()
		{
			_localPlayerDataDeleted = false;
			//player data
			try
			{
				_playerFile = new ES3File("player");
			}
			catch (Exception)
			{
				_playerFile = new ES3File("player", false);
			}

			if (string.IsNullOrEmpty(Player.PlayerId))
			{
				ObscuredString defaultString = "";
				Player.PlayerId = _playerFile.Load("Id", defaultString);
			}
			_localPlayerId = Player.PlayerId;
			LogHelper.LogSdk($"Local player id: {_localPlayerId}");
			ObscuredLong defaultLong = long.MinValue;
			Player.LastSave = _playerFile.Load("LastSave", defaultLong);
			Player.PlayerInfoModels = _playerFile.Load("PlayerInfoModels", new Dictionary<string, PlayerInfoModel>()
			{
				{"MainGame", new PlayerInfoModel()}
			});
			string playerinfo = JsonHelper.SerializeObject(Player.PlayerInfoModels);
			LogHelper.LogSdk(playerinfo, LogHelper.PLAYER_MANAGER_TAG);
			LoadLastSaves();
		}
		
		private void LoadLastSaves()
		{
			//last saves
			try
			{
				_savesFile = new ES3File("lastSaves");
			}
			catch (Exception)
			{
				_savesFile = new ES3File("lastSaves", false);
			}

			_saves = _savesFile.Load("lastSaves", new List<ObscuredLong>());
		}

		private void LoadLocalFileFromJson(string json)
		{
			LogHelper.LogSdk(json, LogHelper.PLAYER_MANAGER_TAG);
			SerializablePlayerModel spm = JsonHelper.DesarializeObject<SerializablePlayerModel>(json);
			_playerFile = new ES3File("player", false);
			Player = spm.ToPlayerModel();
			LoadLastSaves();
		}

		/// <summary>
		/// returns true if the user has cheat through the clock device during offline sessions
		/// </summary>
		/// <returns></returns>
		public bool HasCheated()
		{
			//saves should be in order, if not the user has cheated
			_cheated = false;
			if (!BackendManager.Instance.IsOffline)
			{
				if (_saves.Count > 1)
				{
					for (int i = 0; i < _saves.Count - 1; i++)
					{
						if (_saves[i + 1] < _saves[i])
						{
							if (SdkManager.Instance.Configuration.DebugSdkEvents)
								LogSaves();
							_cheated = true;
							OnPlayerHasCheated?.Invoke();
							break;
						}
					}
				}
				ClearSaves();
			}
			return _cheated;
		}

		void LogSaves()
		{
			string log = "";
			foreach (var s in _saves)
				log += s + " \n";
			LogHelper.LogSdk(log);
		}

		/// <summary>
		/// Clear the saves list
		/// </summary>
		void ClearSaves()
		{
			_saves.Clear();
			_savesFile.Save<List<ObscuredLong>>("lastSaves", _saves);
		}

		/// <summary>
		/// Sets a method to be called just before the player data is persisted locally and remotely.
		/// </summary>
		/// <param name="method"></param>
		public void SetSaveMethod(Action method)
		{
			_saveMethod = method;
		}

		public virtual void OnApplicationQuit()
		{
			SavePlayerData(false, (result) =>
			{
				BackendManager.Instance.Reset(null);
			});
		}

		internal void Reset()
		{
			SyncAvailable = false;
			PlayerService?.Reset();
		}


		protected virtual void OnApplicationFocus(bool focus)
		{
			if (!focus)
			{
				SavePlayerData(false);
			}
		}

		public void SavePlayerData(bool withtimeout, Action<bool> callback = null)
		{
			if (IsLoaded)
			{
				if (!_playerDataDeleted && SaveAvailable)
				{
					SaveLocalFile();
					SyncWithServer(withtimeout, callback);
				}
				else
				{
					ResetLocalFile();
					callback?.Invoke(true);
				}
			}
		}

		public void SavePlayerDataAsync(bool withTimeout, Action<bool> callback = null)
		{
			Task.Run(() => {
				SavePlayerData(withTimeout);
			}).ContinueWithOnMainThread(task => { callback?.Invoke(true); });
		}

		public void SyncWithServer(bool withTimeout, Action<bool> callback = null)
		{
			if (SyncAvailable && Application.internetReachability != NetworkReachability.NotReachable)
				PlayerService?.SetPlayerData(Player, withTimeout, callback);
		}


		/// <summary>
		/// Initializes counters, timers and flags
		/// </summary>
		public void InitializePlayerInfo()
		{
			// Get the PlayerInfoModel
			PlayerInfoModel playerInfo = GetPlayerInfoModel(GameModeConstants.MAIN_GAME);
			
			// Initialize the counters
			if (playerInfo.Counters != null && playerInfo.Counters.Count == 0)
			{
				// If counters is not defined, this is the installing session
				AnalyticsManager.Instance.TrackCustomProperty("InstallVersion", Application.version);
				AnalyticsManager.Instance.TrackCustomProperty(CountersConstants.DAYS_ACTIVE, "1");
				
				playerInfo.Counters.Add(CountersConstants.SESSIONS_AMOUNT, 0);
				playerInfo.Counters.Add(CountersConstants.DAYS_ACTIVE, 1);
			}

			// Initialize the flags
			if (playerInfo.Flags != null && playerInfo.Flags.Count == 0)
			{
				playerInfo.Flags.Add(FlagsConstants.IS_SPENDER, false);
			}
			
			// Initialize the timers
			if (playerInfo.Timers != null && playerInfo.Timers.Count == 0)
			{
				playerInfo.Timers.Add(TimersConstants.FIRST_LOGIN, TimeHelper.GetTimeInServer());
				playerInfo.Timers.Add(TimersConstants.LAST_ACTIVE_DATE, TimeHelper.GetTimeInServer());
			}

			// Track properties & sessions
			SetCounter(CountersConstants.SESSIONS_AMOUNT, 1, GameModeConstants.MAIN_GAME, true);
			AnalyticsManager.Instance.TrackCustomProperty(CountersConstants.SESSIONS_AMOUNT, GetCounter(CountersConstants.SESSIONS_AMOUNT, GameModeConstants.MAIN_GAME).ToString());
			
			// Track days retained
			long firstLoginTimer = GetTimer(TimersConstants.FIRST_LOGIN, GameModeConstants.MAIN_GAME);
			int daysRetained = (int)(DateTime.Now - TimeHelper.FromUnixTime(firstLoginTimer)).TotalDays;
			
			AnalyticsManager.Instance.TrackCustomProperty("DaysRetained", daysRetained.ToString());


			DateTime lastActiveDay = TimeHelper.FromUnixTime(GetTimer(TimersConstants.LAST_ACTIVE_DATE, GameModeConstants.MAIN_GAME));
			long today = TimeHelper.GetTimeInServer();
			DateTime todaDateTime = TimeHelper.FromUnixTime(today);
			long days = (long)(todaDateTime - lastActiveDay).TotalDays;
			if (days > 0)
			{
				//set days active
				SetCounter(CountersConstants.DAYS_ACTIVE, 1, GameModeConstants.MAIN_GAME, true);
				AnalyticsManager.Instance.TrackCustomProperty(CountersConstants.DAYS_ACTIVE, GetCounter(CountersConstants.DAYS_ACTIVE, GameModeConstants.MAIN_GAME).ToString());
				
				//set last day active
				SetTimer(TimersConstants.LAST_ACTIVE_DATE, today, GameModeConstants.MAIN_GAME);
				AnalyticsManager.Instance.TrackCustomProperty(TimersConstants.LAST_ACTIVE_DATE,
					todaDateTime.Date.ToString("MM/dd/yyyy"));
			}
			
			RegisterDeviceId();
		}
		
		protected virtual void RegisterDeviceId()
		{
			SetPlayerData("DeviceId", new DataUpdateModel(SystemInfo.deviceUniqueIdentifier),
				GameModeConstants.MAIN_GAME);
		}

		/// <summary>
		/// Sets the player as and old player
		/// </summary>
		public void SetOldPlayerFlag()
		{
			SetFlag(FlagsConstants.IS_OLD_PLAYER, true, GameModeConstants.MAIN_GAME);
		}
		
		/// <summary>
		/// Save the current player data into the local file.
		/// </summary>
		public void SaveLocalFile()
		{
			CancelInvoke();
			if (!_localPlayerDataDeleted)
			{
				_saveMethod?.Invoke();
				LogHelper.LogSdk("Saving local file", LogHelper.PLAYER_MANAGER_TAG);
				if (_playerFile != null)
				{
					_playerFile.Save<ObscuredString>("Id", Player.PlayerId);
					_playerFile.Save<ObscuredLong>("LastSave", Player.LastSave);
					_playerFile.Save<Dictionary<string, PlayerInfoModel>>("PlayerInfoModels", Player.PlayerInfoModels);
				}
				if (_savesFile != null)
				{
					_savesFile.Save<List<ObscuredLong>>("lastSaves", _saves);
				}
				SyncLocalFiles();
				Invoke("SaveLocalFile", _secondsToSaveLocal);
			}
		}

		/// <summary>
		/// Reset the local file content to default values.
		/// </summary>
		void ResetLocalFile()
		{
			if (_playerFile != null)
			{
				ObscuredString defaultStringValue = "";
				_playerFile.Save<ObscuredString>("Id", defaultStringValue);
				ObscuredLong defaultLongValue = long.MinValue;
				_playerFile.Save<ObscuredLong>("LastSave", defaultLongValue);
				_playerFile.Save<Dictionary<string, PlayerInfoModel>>("PlayerInfoModels", new Dictionary<string, PlayerInfoModel>()
				{
					{"MainGame", new PlayerInfoModel()}
				});
				SyncLocalFiles();
			}
		}

		public void DeleteLocalFile()
		{
			ResetLocalFile();
			_localPlayerId = "";
			_localPlayerDataDeleted = true;
			LogHelper.LogSdk("Local player deleted");
		}

		public void EnableLocalFile()
		{
			_localPlayerDataDeleted = false;
		}


		/// <summary>
		/// Synchronises player and saves data with files in storage.
		/// </summary>
		void SyncLocalFiles()
		{
			try 
			{
				_playerFile.Sync();
				_savesFile.Sync();
			} 
			catch(Exception e) 
			{
				OnSavePlayerDataFailed?.Invoke();
				LogHelper.LogError("Failed to save data to storage: " + e.Message, Environment.StackTrace);
			}
		}
		
		/// <summary>
		/// Generates a traditional game save file in json.
		/// </summary>
		public virtual void GenerateProgressFile(string fileName)
		{
			_saveMethod?.Invoke();
			SerializablePlayerModel playerModel = new SerializablePlayerModel(Player);
			string jsonContent = JsonHelper.SerializeObject(playerModel);
			ProgressFileHelper.SaveJsonAsFile(fileName, jsonContent);
		}

		/// <summary>
		/// Sets a savefile to be loaded in the next execution of the game and quits.
		/// </summary>
		public virtual void LoadProgressFile(string fileName)
		{
			PlayerPrefs.SetString("PROGRESS_FILE_TO_LOAD", fileName);
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif

		}

		#region PlayerData

		/// <summary>
		/// Get the PlayerId
		/// </summary>
		/// <returns>The PlayerId</returns>
		public string GetPlayerId()
		{
			if (Player != null)
				return Player.PlayerId;

			return "";
		}
		
		
		public void SetServerPlayerId(string playerId)
		{
			LogHelper.LogSdk($"Server player id: {playerId}");
			_serverPlayerId = playerId;
		}

		public void OverridePlayerId()
		{
			Player.PlayerId = ServerPlayerId;
		}

		/// <summary>
		/// Sets the PlayerId
		/// </summary>
		public void SetPlayerId()
		{
			_localPlayerId = Player.PlayerId;
			_serverPlayerId = Player.PlayerId;
			LogHelper.LogSdk($"player id event: {Player.PlayerId}");
			PlayerService?.SetUserId(Player.PlayerId);
			OnPlayerIdSet?.Invoke(Player.PlayerId);
		}

		/// <summary>
		/// Get the current PlayerModel
		/// </summary>
		/// <returns>The PlayerModel</returns>
		public PlayerModel GetPlayerModel()
		{
			return Player;
		}

		internal void SetPlayerModel(PlayerModel model, bool update = false)
		{
			if (string.IsNullOrEmpty(_customFilePath))
			{
				Player = model;
				if (update)
					OnPlayerUpdated?.Invoke();
			}
			else
				LogHelper.LogWarning("Server player data wasn't set because a custom player data was loaded before");
		}

		public void DeletePlayerData(Action callback = null)
		{
			_playerDataDeleted = true;
			PlayerService?.DeletePlayerData(callback);
		}
		

		/// <summary>
		/// Returns the PlayerInfoModel
		/// </summary>
		/// <param name="gameMode">The game mode.</param>
		/// <returns>The PlayerInfoModel</returns>
		public PlayerInfoModel GetPlayerInfoModel(string gameMode)
		{
			if (Player.PlayerInfoModels == null)
				return null;
			
			if(gameMode == GameModeConstants.MAIN_GAME && Player.PlayerInfoModels.ContainsKey(GameModeConstants.MAIN_GAME))
				return Player.PlayerInfoModels[GameModeConstants.MAIN_GAME];
			
			if(gameMode == GameModeConstants.LIMITED_TIME_EVENT)
			{
				var activeEvent = LimitedTimeEventsManager.Instance.ActiveEvent;
				if (activeEvent != null)
				{
					if (!Player.PlayerInfoModels.ContainsKey(activeEvent.Id))
					{
						var eventInfoModel = new PlayerInfoModel();
						eventInfoModel.Counters.Add("EventNumber", activeEvent.Number);
						Player.PlayerInfoModels.Add(activeEvent.Id, eventInfoModel);
					}

					return Player.PlayerInfoModels[activeEvent.Id];
				}
			}

			return null;
		}

		/// <summary>
		/// Return all of the player's info models
		/// </summary>
		/// <returns>The info models</returns>
		public Dictionary<string, PlayerInfoModel> GetAllPlayerInfoModels()
		{
			return Player?.PlayerInfoModels;
		}

		/// <summary>
		/// Sets the player data.
		/// </summary>
		/// <param name="key">Key.</param>
		/// <param name="value">Value.</param>
		/// <param name="gameMode">The game mode.</param>
		/// <param name="callback">A callback to know the result of the update</param>
        public void SetPlayerData (string key, DataUpdateModel value, string gameMode, Action<bool> callback = null)
		{
            Dictionary<string, DataUpdateModel> data = new Dictionary<string, DataUpdateModel>
            {
                { key, value }
            };

            SetPlayerData (data, gameMode, callback);
		}

        /// <summary>
        /// Sets the player data.
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="gameMode">The game mode.</param>
        /// <param name="callback">A callback to know the result of the update</param>
        public void SetPlayerData (Dictionary<string, DataUpdateModel> data, string gameMode, Action<bool> callback = null)
        {
			// Make sure we save it locally first
            SetPlayerDataLocal (data, gameMode);
            callback?.Invoke(true);
        }

        /// <summary>
        /// Sets the player data locally
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <param name="updateFile">Update local file</param>
        void SetPlayerDataLocal (Dictionary<string, DataUpdateModel> data, string gameMode, bool updateFile = false)
        {
	        // Get the PlayerInfoModel
	        PutDataInPlayerData(data, gameMode);
	        UpdateLastSave(updateFile);
		}

        void UpdateLastSave(bool updateFile)
        {
	        Player.LastSave = TimeHelper.GetTimeInServer();
	        _saves.Add(Player.LastSave);
	        if (updateFile)
	        {
		        _playerFile.Save<ObscuredLong>("LastSave", Player.LastSave);
		        _playerFile.Save<Dictionary<string, PlayerInfoModel>>("PlayerInfoModels", Player.PlayerInfoModels);
	        }
        }

        /// <summary>
        /// Stores the data in Player.Data
        /// </summary>
        /// <param name="data">The data to add</param>
        /// <param name="gameMode">The game mode.</param>
        /// <returns>The newly added data</returns>
        Dictionary<string, string> PutDataInPlayerData(Dictionary<string, DataUpdateModel> data, string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
	        Dictionary<string, string> result = new Dictionary<string, string>();

	        foreach (var pair in data) {
		        LogHelper.LogSdk (string.Format ("Saving {0}: {1}...", pair.Key, pair.Value), LogHelper.PLAYER_MANAGER_TAG);

		        if (playerInfo.Data.ContainsKey (pair.Key))
		        {
			        if(pair.Value.IsUpdate)
			        {
				        long.TryParse(playerInfo.Data[pair.Key].ToString(),NumberStyles.None, CultureInfo.InvariantCulture, out long savedValue);
				        long.TryParse(pair.Value.Value,NumberStyles.None, CultureInfo.InvariantCulture, out long delta);
				        playerInfo.Data[pair.Key] = (savedValue + delta).ToString();
			        }
			        else
			        {
				        playerInfo.Data[pair.Key] = pair.Value.Value;
			        }
		        }
		        else
			        playerInfo.Data.Add (pair.Key, pair.Value.Value);

		        result.Add(pair.Key, playerInfo.Data[pair.Key].ToString());
	        }
	        return result;
        }

        /// <summary>
        /// Removes from PlayerData locally
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <param name="updateFile">Update local file.</param>
        public void RemovePlayerDataLocal(Dictionary<string, DataUpdateModel> data, string gameMode, bool updateFile = false)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
            foreach (var pair in data)
            {
                LogHelper.LogSdk(string.Format("Removing {0}...", pair.Key), LogHelper.PLAYER_MANAGER_TAG);

                playerInfo.Data.Remove(pair.Key);
            }
            UpdateLastSave(updateFile);
        }

        /// <summary>
        /// Removes from PlayerData locally
        /// </summary>
        /// <param name="keyToRemove">Data.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <param name="updateFile">Update local file.</param>
        public void RemovePlayerDataLocal(string keyToRemove, string gameMode, bool updateFile = false)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
            LogHelper.LogSdk(string.Format("Removing {0}...", keyToRemove), LogHelper.PLAYER_MANAGER_TAG);

            playerInfo.Data.Remove(keyToRemove);
            UpdateLastSave(updateFile);
        }
        

        /// <summary>
        /// Gets a value from player data.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <returns>the value requested.</returns>
        public string GetFromPlayerData(string key, string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
            if(Player != null && playerInfo.Data != null && playerInfo.Data.ContainsKey(key))
            {
                if(playerInfo.Data[key] != null)
                {
                    return playerInfo.Data[key].ToString();
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }
        
        public void DeleteData(string key, string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);

	        if (playerInfo?.Data != null && playerInfo.Data.ContainsKey(key))
	        {
		        playerInfo.Data.Remove(key);
	        }
        }

        /// <summary>
        /// Sets a counter
        /// </summary>
        /// <param name="key">The key of the counter</param>
        /// <param name="value">The value of the counter</param>
        /// <param name="increment">Should the actual value be incremented?</param>
        /// <param name="gameMode">The game mode.</param>
        public void SetCounter(string key, int value, string gameMode, bool increment = false)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);

	        if (playerInfo != null && playerInfo.Counters != null)
	        {
		        if (playerInfo.Counters.ContainsKey(key))
		        {
			        if (increment)
				        playerInfo.Counters[key] += value;
			        else
				        playerInfo.Counters[key] = value;
		        }
		        else
		        {
			        playerInfo.Counters.Add(key, value);
		        }
	        }
	        UpdateLastSave(false);
        }

        /// <summary>
        /// Sets a counter for a specific event
        /// </summary>
        /// <param name="key">The key of the counter</param>
        /// <param name="value">The value of the counter</param>
        /// <param name="increment">Should the actual value be incremented?</param>
        /// <param name="eventId">The id of the event.</param>
        public void SetCounterForSpecificEvent(string key, int value, string eventId, bool increment = false)
        {
	        // Get the PlayerInfoModel
	        if (Player != null && Player.PlayerInfoModels != null && !Player.PlayerInfoModels.ContainsKey(eventId))
	        {
		        return;
	        }
	        
	        var playerInfo = Player.PlayerInfoModels[eventId];
	        
	        if (playerInfo.Counters.ContainsKey(key))
	        {
		        if (increment)
			        playerInfo.Counters[key] += value;
		        else
			        playerInfo.Counters[key] = value;
	        }
	        else
	        {
		        playerInfo.Counters.Add(key, value);
	        }
	        UpdateLastSave(false);
        }

        /// <summary>
        /// Gets the value of a counter
        /// </summary>
        /// <param name="key">The key of the counter</param>
        /// <param name="defaultValue">The default value of the counter</param>
        /// <param name="gameMode">The game mode.</param>
        /// <returns>The saved value or the default</returns>
        public int GetCounter(string key, string gameMode, int defaultValue = 0)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);

	        if (playerInfo?.Counters != null)
	        {
		        return playerInfo.Counters.ContainsKey(key) ?  (int)playerInfo.Counters[key] :  defaultValue;
	        }
	        return defaultValue;
        }

        /// <summary>
        /// Gets the value of a counter, for a specific event
        /// </summary>
        /// <param name="key">The key of the counter</param>
        /// <param name="eventId">The id of the event</param>
        /// <param name="defaultValue">The default value of the counter</param>
        /// <returns>The saved or the default</returns>
        public int GetCounterForSpecificEvent(string key, string eventId, int defaultValue = 0)
        {
	        if (!Player.PlayerInfoModels.ContainsKey(eventId))
	        {
		        return defaultValue;
	        }
	        
	        var playerInfo = Player.PlayerInfoModels[eventId];

	        if (playerInfo?.Counters != null)
	        {
		        return playerInfo.Counters.ContainsKey(key) ? (int) playerInfo.Counters[key] : defaultValue;
	        }

	        return defaultValue;
        }
        
        public void DeleteCounter(string key, string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);

	        if (playerInfo?.Counters != null && playerInfo.Counters.ContainsKey(key))
	        {
		        playerInfo.Counters.Remove(key);
	        }
        }
        
        /// <summary>
        /// Sets a timer
        /// </summary>
        /// <param name="key">The key of the timer</param>
        /// <param name="value">The value of the timer</param>
        /// <param name="gameMode">The game mode.</param>
		/// <param name="increment">Should the actual value be incremented?</param>
        public void SetTimer(string key, long value, string gameMode, bool increment = false)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);

	        if (playerInfo?.Timers != null)
	        {
		        if (playerInfo.Timers.ContainsKey(key))
		        {
			        if (increment)
				        playerInfo.Timers[key] += value;
			        else
				        playerInfo.Timers[key] = value;
		        }
		        else
		        {
			        playerInfo.Timers.Add(key, value);
		        }
	        }
	        UpdateLastSave(false);
        }
        
        /// <summary>
        /// Gets the value of a timer
        /// </summary>
        /// <param name="key">The key of the timer</param>
        /// <param name="gameMode">The game mode.</param>
        /// <param name="defaultValue">The default value of the timer</param>
        /// <returns>The saved value or the default</returns>
        public long GetTimer(string key, string gameMode, long defaultValue = 0)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);

	        if (playerInfo?.Timers != null)
	        {
		        return playerInfo.Timers.ContainsKey(key) ? (long) playerInfo.Timers[key] : defaultValue;
	        }

	        return defaultValue;
        }
        
        /// <summary>
        /// Deletes a timer
        /// </summary>
        /// <param name="key">Timer to delete</param>
        /// <param name="gameMode">The current game mode</param>
        public void DeleteTimer(string key, string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);

	        if (playerInfo?.Timers != null && playerInfo.Timers.ContainsKey(key))
	        {
		        playerInfo.Timers.Remove(key);
	        }
        }
        
        /// <summary>
        /// Sets a flag
        /// </summary>
        /// <param name="key">The key of the flag</param>
        /// <param name="value">The value of the flag</param>
        /// <param name="gameMode">The game mode.</param>
        public void SetFlag(string key, bool value, string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);

	        if (playerInfo?.Flags != null)
	        {
		        if (playerInfo.Flags.ContainsKey(key))
		        {
			        playerInfo.Flags[key] = value;
		        }
		        else
		        {
			        playerInfo.Flags.Add(key, value);
		        }
	        }
	        UpdateLastSave(false);
        }
        
        /// <summary>
        /// Sets a flag for a specific event
        /// </summary>
        /// <param name="key">The key of the flag</param>
        /// <param name="value">The value of the flag</param>
        /// <param name="eventId">The event id.</param>
        public void SetFlagForSpecificEvent(string key, bool value, string eventId)
        {
	        if (!Player.PlayerInfoModels.ContainsKey(eventId))
	        {
		        return;
	        }
	        
	        var playerInfo = Player.PlayerInfoModels[eventId];

	        if (playerInfo?.Flags != null)
	        {
		        if (playerInfo.Flags.ContainsKey(key))
		        {
			        playerInfo.Flags[key] = value;
		        }
		        else
		        {
			        playerInfo.Flags.Add(key, value);
		        }
	        }
	        UpdateLastSave(false);
        }
        
        /// <summary>
        /// Gets the value of a flag
        /// </summary>
        /// <param name="key">The key of the flag</param>
        /// <param name="gameMode">The game mode.</param>
        /// <param name="defaultValue">The default value of the counter</param>
        /// <returns>The saved value or the default</returns>
        public bool GetFlag(string key, string gameMode, bool defaultValue = false)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);

	        if (playerInfo?.Flags != null)
	        {
		        return playerInfo.Flags.ContainsKey(key) ? (bool) playerInfo.Flags[key] : defaultValue;
	        }

	        return defaultValue;
        }
        
        /// <summary>
        /// Gets the value of a flag, for a specific event
        /// </summary>
        /// <param name="key">The key of the flag</param>
        /// <param name="eventId">The id of the event</param>
        /// <param name="defaultValue">The default value of the flag</param>
        /// <returns>The saved or the default</returns>
        public bool GetFlagForSpecificEvent(string key, string eventId, bool defaultValue = false)
        {
	        if (!Player.PlayerInfoModels.ContainsKey(eventId))
	        {
		        return defaultValue;
	        }
	        
	        var playerInfo = Player.PlayerInfoModels[eventId];

	        if (playerInfo?.Flags != null)
	        {
		        return playerInfo.Flags.ContainsKey(key) ? (bool) playerInfo.Flags[key] : defaultValue;
	        }

	        return defaultValue;
        }
        
        /// <summary>
        /// Deletes a flag
        /// </summary>
        /// <param name="key">Flag to delete</param>
        /// <param name="gameMode">The current game mode</param>
        public void DeleteFlag(string key, string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);

	        if (playerInfo?.Flags != null && playerInfo.Flags.ContainsKey(key))
	        {
		        playerInfo.Flags.Remove(key);
	        }
        }

        /// <summary>
        /// Adds to the player's time travel.
        /// </summary>
        /// <param name="delta">Delta in seconds.</param>
        public void AddTimeTravel(int delta)
        {
            // Update the timer for the user
            SetTimer(TimersConstants.TIME_TRAVEL, delta, GameModeConstants.MAIN_GAME, true);

            // Send the event that we time traveled
            OnTimeTravelApplied?.Invoke(delta);
        }

        #endregion

        #region Currency

        /// <summary>
        /// Change the currency
        /// </summary>
        /// <param name="key">Currency Key.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <param name="persistInFile">Should the file be persisted?</param>
        public void ChangeCurrency(string key, int amount, string gameMode, bool persistInFile = false)
        {
	        // If we are going to change the hard currency, make it always do it from the main game
	        if (key == "hard")
	        {
		        gameMode = GameModeConstants.MAIN_GAME;
	        }
	        
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
	        if (playerInfo.Currencies.ContainsKey(key))
	        {
		        playerInfo.Currencies[key] += amount;
	        }
	        else
	        {
		        playerInfo.Currencies.Add(key, amount);
	        }
	        
	        PlayerEvents.CurrencyBalanceChanged(key, amount);
	        
	        AnalyticsManager.Instance.TrackCustomProperty(key, playerInfo.Currencies[key].ToString());

	        UpdateLastSave(persistInFile);
        }

        /// <summary>
		/// Gets the currency balance of a specific currency
		/// </summary>
		/// <param name="key">Currency Key</param>
        /// <param name="gameMode">The game mode.</param>
		/// <returns>The current balance.</returns>
        public int GetCurrencyBalance (string key, string gameMode)
		{
			// If we are going to change the hard currency, make it always do it from the main game
			if (key == "hard")
			{
				gameMode = GameModeConstants.MAIN_GAME;
			}
			
			// Get the PlayerInfoModel
			PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
			
			if (playerInfo.Currencies != null && playerInfo.Currencies.ContainsKey (key))
				return playerInfo.Currencies [key];

			return 0;
		}

        #endregion

        #region Items
        
        /// <summary>
        /// Returns all of the items
        /// </summary>
        /// <param name="gameMode">The game mode.</param>
        /// <returns>The player items</returns>
        public List<PlayerItemModel> GetPlayerItems(string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
	        return playerInfo.Items;
        }

        /// <summary>
        /// Returns all of the items that match a certain name
        /// </summary>
        /// <param name="itemName">Item name.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <returns>The player items by name.</returns>
        public List<PlayerItemModel> GetPlayerItemsByName(string itemName, string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
            return playerInfo.Items.Where(it => it.Name == itemName).ToList();
        }

        /// <summary>
        /// Gets the player item by identifier.
        /// </summary>
        /// <param name="itemId">Item identifier.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <returns>The player item by identifier.</returns>
        public PlayerItemModel GetPlayerItemById(string itemId, string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
            return playerInfo.Items.FirstOrDefault(it => it.Id == itemId);
        }
		
		/// <summary>
		/// Gets the player items by type.
		/// </summary>
		/// <param name="type">Item type.</param>
		/// <param name="gameMode">The game mode.</param>
		/// <returns>The player item by type.</returns>
		public List<PlayerItemModel> GetPlayerItemsByType(string type, string gameMode) 
		{
			// Get the PlayerInfoModel
			PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
			
			return playerInfo.Items.Where(item => item.Type == type).ToList();
		}
        
        /// <summary>
        /// Gets the player items by class.
        /// </summary>
        /// <param name="className">Item class.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <returns>The player item by class.</returns>
        public List<PlayerItemModel> GetPlayerItemsByClass(string className, string gameMode) 
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
            return playerInfo.Items.Where(item => item.Class == className).ToList();
        }

        /// <summary>
        /// Adds the items
        /// </summary>
        /// <param name="items">Items.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <param name="persistInFile">Should the file be persisted?</param>
        public virtual void AddItems(List<PlayerItemModel> items, string gameMode, bool persistInFile = false)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
            // Add all of the items
            foreach (PlayerItemModel i in items)
            {
                // Get the item model to check if it is stackable
                BaseItemModel baseItem = ItemsManager.Instance.GetItem(i.Name);
                if (baseItem != null)
                {
	                if (baseItem.IsStackable)
	                {
		                // Try to get an existing item in playerData
		                PlayerItemModel exitingItem = playerInfo.Items.FirstOrDefault(x => x.Name == i.Name);
		                if (exitingItem != null)
			                // Increment the amount remaining
			                exitingItem.AmountRemaining += i.AmountRemaining;
		                else
			                // Add a new item
			                playerInfo.Items.Add(i);
	                } 
	                else
		                // Add a new item
		                playerInfo.Items.Add(i);
                }
            }
            
            UpdateLastSave(persistInFile);

            // Send the appropriate event
            OnItemsAdded?.Invoke(items);
        }

        /// <summary>
        /// Adds the item locally.
        /// </summary>
        /// <param name="item">Item.</param>
        /// <param name="gameMode">The game mode.</param>
        public void AddItem(PlayerItemModel item, string gameMode)
        {
	        var newItem = new PlayerItemModel()
	        {
		        Id = item.Id,
		        Name = item.Name,
		        Type = item.Type,
		        Class = item.Class,
		        AmountRemaining = item.AmountRemaining
	        };
	        var items = new List<PlayerItemModel>() { newItem };
	        
	        // Add all of the items locally
	        AddItems(items, gameMode);
        }

        /// <summary>
        /// Consumes the items locally.
        /// </summary>
        /// <param name="itemsToConsume">Items to consume.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <param name="persistInFile">Should the file be persisted?</param>
        public void ConsumeItems(List<PlayerItemModel> itemsToConsume, string gameMode, bool persistInFile = false)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
	        foreach (PlayerItemModel itc in itemsToConsume)
	        {
		        PlayerItemModel pim = playerInfo.Items.FirstOrDefault(x => x.Id == itc.Id);
		        if (pim != null)
		        {
			        pim.AmountRemaining -= itc.AmountRemaining;
			        
			        if (pim.AmountRemaining == 0)
				        playerInfo.Items.Remove(pim);
		        }
	        }

	        UpdateLastSave(persistInFile);
	        
	        OnItemsRemoved?.Invoke();
        }

        /// <summary>
        /// Gets the balance of a specific Item
        /// </summary>
        /// <param name="itemName">Item name</param>
        /// <param name="gameMode">The game mode.</param>
        /// <returns>The Item balance.</returns>

        public int GetItemBalance(string itemName, string gameMode)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
            int balance = 0;
            if (playerInfo.Items != null)
            {
                foreach (PlayerItemModel pim in playerInfo.Items)
                {
                    if (pim.Name == itemName)
                        balance += pim.AmountRemaining;
                }
            }

            return balance;
        }

        /// <summary>
        /// If the user has an item
        /// </summary>
        /// <param name="itemName">Item name.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <returns><c>true</c>, if it has an item, <c>false</c> otherwise.</returns>
    
        public bool HasItem(string itemName, string gameMode)
        {
            return GetItemBalance(itemName, gameMode) > 0;
        }

        /// <summary>
        /// Updates the CustomData on this item
        /// </summary>
        /// <param name="itemId">Items Id.</param>
        /// <param name="data">The new data.</param>
        /// <param name="gameMode">The game mode.</param>
        /// <param name="persistInFile">Should the file be persisted?</param>
        public void UpdateItemCustomData(string itemId, Dictionary<string, object> data, string gameMode, bool persistInFile = false)
        {
	        // Get the PlayerInfoModel
	        PlayerInfoModel playerInfo = GetPlayerInfoModel(gameMode);
	        
	        PlayerItemModel item = GetPlayerItemById(itemId, gameMode);

	        item.Data = data;

	        UpdateLastSave(persistInFile);
        }

        #endregion
		
        #region Apply Items of Modifier Type

        /// <summary>
        /// Apply all the modifiers for GameplayModifierItems 
        /// </summary>
        private void ApplyItemsOfGameplayModifierType()
        {
            // Grab all of the GameplayModifiersItems that the player has and apply them
            List<PlayerItemModel> playerItems = GetPlayerItemsByType("GameplayModifierItemModel", GameModeManager.CurrentGameMode);
            
            // Get all modifiers to be applied from items
            List<GameplayModifierItemModel> modifierItemModels = GetModifiersItemsFromPlayerItems(playerItems);
            
            // Apply the modifiers
            foreach (GameplayModifierItemModel itemModel in modifierItemModels)
            {
	            GameplayModifiersManager.Instance.RemoveGameplayModifiers(itemModel.Modifiers);
	            GameplayModifiersManager.Instance.ApplyGameplayModifiers(itemModel.Modifiers);
            }
        }

        /// <summary>
        /// Gets the modifiers from player items.
        /// </summary>
        /// <returns>The modifiers from player items.</returns>
        /// <param name="playerItems">Player items.</param>
        public List<GameplayModifierItemModel> GetModifiersItemsFromPlayerItems(List<PlayerItemModel> playerItems)
        {
            // Create the result
            List<GameplayModifierItemModel> result = new List<GameplayModifierItemModel>();

            if (playerItems == null)
            {
                return result;
            }

            // Iterate through all of the items, and get the originals from the ItemManager
            foreach (PlayerItemModel playerItem in playerItems)
            {
                if (ItemsManager.Instance.GetItem(playerItem.Name) is GameplayModifierItemModel gameplayModifierItem)
                {
	                for(int i = 0; i < playerItem.AmountRemaining; i++)
						result.Add(gameplayModifierItem);
                }
            }

            return result;
        }

        #endregion
    }
}
