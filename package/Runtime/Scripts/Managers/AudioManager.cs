﻿using BigfootSdk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace BigfootSdk.Audio
{
    public enum AudioBusEnum
    {
        SFX,
        MUSIC
    }



    /// <summary>
    /// Manages audio.
    /// </summary>
    public class AudioManager : Singleton<AudioManager>
    {


        /// <summary>
        /// If <see langword="true"/> the music bus is on.
        /// </summary>
        [SerializeField]
        private bool _musicBusOn;

        [Range(0,1f)]
        public float MusicBusVolume = 1f;

        /// <summary>
        /// If <see langword="true"/> the sfx bus is on.
        /// </summary>
        [SerializeField]
        private bool _sfxBusOn;

        [Range(0, 1f)]
        public float SfxBusVolume = 1f;

        /// <summary>
        /// The playlists.
        /// </summary>
        private List<Playlist> _playlists = new List<Playlist>();

        /// <summary>
        /// The current music.
        /// </summary>
        private AudioElement _currentMusic;

        /// <summary>
        /// The play playlist coroutine.
        /// </summary>
        Coroutine _playPlaylistCo;

        /// <summary>
        /// The index of the playlist.
        /// </summary>
        int _playlistIndex;

        private List<AudioGroup> _audioGroups = new List<AudioGroup>();

        // Adds an AudioGroup reference
        public bool AddAudioGroup(AudioGroup ag)
        {
            bool value = true;

            if (!_audioGroups.Contains(ag))
                _audioGroups.Add(ag);

            return value;
        }

        // Removes and AudioGroup reference
        public bool RemoveAudioGroup(AudioGroup ag)
        {
            bool value = true;

            if (_audioGroups.Contains(ag))
                _audioGroups.Remove(ag);

            return value;
        }


        /// <summary>
        /// Retrieves all audio elements matching name of all audioGroups active
        /// </summary>
        /// <param name="name">name of the audio</param>
        /// <returns></returns>
        public List<AudioElement> GetAudioElements(string name)
        {
            List<AudioElement> result = new List<AudioElement>();

            foreach (var item in _audioGroups)
            {
                var audiosFoundInGroup = item.FindAudios(name);
                if (audiosFoundInGroup != null)
                    result.AddRange(audiosFoundInGroup);
            }

            return result;
        }

        /// <summary>
        /// Gets a value indicating whether the music bus is on.
        /// </summary>
        /// <value><c>true</c> if music bus is on; otherwise, <c>false</c>.</value>
        public bool MusicBusOn
        {
            get
            {
                return _musicBusOn;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the sfx bus is on.
        /// </summary>
        /// <value><c>true</c> if sfx bus is on; otherwise, <c>false</c>.</value>
        public bool SfxBusOn
        {
            get
            {
                return _sfxBusOn;
            }
        }

        private void OnEnable()
        {
            _musicBusOn = (PlayerPrefs.GetInt("MusicBusOn", 1) == 1);
            _sfxBusOn = (PlayerPrefs.GetInt("SfxBusOn", 1) == 1);
        }

        /// <summary>
        /// Loads the playlist.
        /// </summary>
        /// <param name="playlist">Playlist.</param>
        public void LoadPlaylist (Playlist playlist)
        {
            if (!_playlists.Contains(playlist))
                _playlists.Add(playlist);
        }

        /// <summary>
        /// Unloads the playlist.
        /// </summary>
        /// <param name="playlist">Playlist.</param>
        public void UnloadPlaylist (Playlist playlist)
        {
            if (_playlists.Contains(playlist))
                _playlists.Remove(playlist);
        }

        /// <summary>
        /// Play the playlist.
        /// </summary>
        /// <param name="playlistName">Playlist name.</param>
        public void PlayPlaylist (string playlistName)
        {
            Playlist playlist = _playlists.Where(x => x.Name == playlistName).FirstOrDefault();
            if (playlist != null)
            {
                if (_playPlaylistCo != null)
                    StopPlaylist();

                _playlistIndex = 0;
                if (playlist.Elements.Count > 0)
                    PlayNextItemOnPlaylist(playlist);
            }
        }

        /// <summary>
        /// Stops the playlist.
        /// </summary>
        public void StopPlaylist ()
        {
            if(_currentMusic != null)
                _currentMusic.StopAudio();
            if(_playPlaylistCo != null)
                StopCoroutine(_playPlaylistCo);
        }

        /// <summary>
        /// Play the next item on playlist.
        /// </summary>
        /// <param name="playlist">Playlist.</param>
        void PlayNextItemOnPlaylist(Playlist playlist)
        {
            _playPlaylistCo = StartCoroutine(PlayPlaylistCo(playlist));
        }

        /// <summary>
        /// Play the playlist.
        /// </summary>
        /// <returns>The playlist coroutine.</returns>
        /// <param name="playlist">Playlist.</param>
        IEnumerator PlayPlaylistCo(Playlist playlist)
        {
            _currentMusic = playlist.Elements[_playlistIndex];
            _currentMusic.PlayAudio(false);

            // Get duration to wait for next item
            float duration = playlist.Elements[_playlistIndex].UpdateDuration();
            yield return new WaitForSeconds(duration);
            _playlistIndex++;

            // Reset cycle
            if (_playlistIndex >= playlist.Elements.Count)
                _playlistIndex = 0;

            PlayNextItemOnPlaylist(playlist);
        }

        /// <summary>
        /// Play the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="loop">If set to <c>true</c> the audio will loop.</param>
        /// <param name="groupId">Group identifier.</param>
        public void Play (string audioName, bool loop, int groupId = 0)
        {
            AudioEvents.Play(audioName, loop, groupId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="loop">If set to <c>true</c> the audio will loop.</param>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="crossfadeTime">Crossfade time.</param>
        public void PlayWithCrossfade(string audioName, bool loop, int groupId, float crossfadeTime)
        {
            AudioEvents.PlayWithCrossfade(audioName, loop, groupId, crossfadeTime);
        }

        /// <summary>
        /// Play the specified audio with a custom volume.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="volume">Custom volume play.</param>
        /// <param name="loop">If set to <c>true</c> the audio will loop.</param>
        /// <param name="groupId">Group identifier.</param>
        public void PlayCustomVolume(string audioName, float volume, bool loop, int groupId = 0)
        {
            AudioEvents.PlayCustomVolume(audioName, volume, loop, groupId);
        }

        /// <summary>
        /// Play the specified audio with volume set by its distance to target.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="target">Target which distance fill affect volume.</param>
        /// <param name="loop">If set to <c>true</c> the audio will loop.</param>
        /// <param name="groupId">Group identifier.</param>
        public void PlayTargetVolume(string audioName, AudioTarget3D target, bool loop, int groupId = 0)
        {
            AudioEvents.PlayTargetVolume(audioName, target, loop, groupId);
        }

        /// <summary>
        /// Pause the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        public void Pause (string audioName, int groupId = 0)
        {
            AudioEvents.Pause(audioName, groupId);
        }

        /// <summary>
        /// Unpause the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        public void Unpause(string audioName, int groupId = 0)
        {
            AudioEvents.Unpause(audioName, groupId);
        }

        /// <summary>
        /// Stop the specified audio.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        public void Stop(string audioName, int groupId = 0)
        {
            AudioEvents.Stop(audioName, groupId);
        }

        /// <summary>
        /// Fade's a sound volume to a specific target.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="targetVolume">Volume to reach.</param>
        /// <param name="fadeTime">Time to reach volume values.</param>
        public void Fade(string audioName, int groupId, float targetVolume, float fadeTime = 0.5f)
        {
            AudioEvents.Fade(audioName, groupId, targetVolume, fadeTime);
        }

        /// <summary>
        /// Plays a sound using FadeIN, the sound will reach its volume preset value.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="fadeTime">Time to reach volume values.</param>
        /// <param name="loop">Should it loop?</param>
        public void PlayWithFadeIn(string audioName, int groupId, float fadeTime = 0.5f, bool loop = true)
        {
            AudioEvents.PlayWithFadeIn(audioName, groupId, fadeTime, loop);
        }

        /// <summary>
        /// Stops a sound usind FadeOut, the sound will reach volume value of 0 and will stop.
        /// </summary>
        /// <param name="audioName">Audio name.</param>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="fadeTime">Time to reach volume values.</param>
        public void StopWithFadeOut(string audioName, int groupId, float fadeTime = 0.5f)
        {
            AudioEvents.StopWithFadeOut(audioName, groupId, fadeTime);
        }

        /// <summary>
        /// Mutes the specified bus.
        /// </summary>
        /// <param name="bus">Bus.</param>
        /// <param name="save">Save will save setting on player prefs.</param>
        public void MuteBus(AudioBusEnum bus, bool save = true)
        {
            if (bus == AudioBusEnum.MUSIC)
            {
                _musicBusOn = false;
                if(save)
                    PlayerPrefs.SetInt("MusicBusOn", 0);
            }

            else
            {
                _sfxBusOn = false;
                if (save)
                    PlayerPrefs.SetInt("SfxBusOn", 0);
            }
                
            AudioEvents.MuteBus(bus);
        }

        /// <summary>
        /// Unutes the specified bus.
        /// </summary>
        /// <param name="bus">Bus.</param>
        /// <param name="save">Save will save setting on player prefs.</param>
        public void UnmuteBus(AudioBusEnum bus, bool save = true)
        {
            if (bus == AudioBusEnum.MUSIC)
            {
                _musicBusOn = true;
                if (save)
                    PlayerPrefs.SetInt("MusicBusOn", 1);
            }
            else
            {
                _sfxBusOn = true;
                if (save)
                    PlayerPrefs.SetInt("SfxBusOn", 1);
            }
                
            AudioEvents.UnmuteBus(bus);
        }

    }
}