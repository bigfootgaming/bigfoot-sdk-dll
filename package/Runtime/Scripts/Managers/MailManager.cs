using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk 
{
    public class MailManager : LoadableSingleton<MailManager>
    {
        protected List<MailModel> _mails;
        protected List<PlayerMailModel> _playerMails;
        protected int _giftId;
        public List<MailModel> Mails => _mails;
        public List<PlayerMailModel> PlayerMails => _playerMails;
        public int GiftId => _giftId;

        public override void StartLoading()
        {
            _mails = ConfigHelper.LoadConfig<List<MailModel>>("mails");
            string playerMailString = PlayerManager.Instance.GetFromPlayerData("mails", GameModeConstants.MAIN_GAME);
            if (!string.IsNullOrEmpty(playerMailString))
            {
                _playerMails = JsonHelper.DesarializeObject<List<PlayerMailModel>>(playerMailString);
            }
            else
            {
                _playerMails = new List<PlayerMailModel>();
            }

            ClearOldMails();

            _giftId = PlayerManager.Instance.GetCounter(GetGiftCounterKey(), GameModeConstants.MAIN_GAME,
                GetGiftCounterDefaultValue());

            FinishedLoading(true);
        }

        protected virtual void ClearOldMails()
        {
            for (int i = 0; i < _playerMails.Count; )
            {
                MailModel mailModel = GetMailModel(_playerMails[i].Id);
                if (mailModel == null)
                {
                    _playerMails.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
        }

        protected virtual string GetGiftCounterKey()
        {
            return "gift";
        }

        protected virtual int GetGiftCounterDefaultValue()
        {
            return -1;
        }

        public virtual bool HasIndividualGift()
        {
            return _giftId != GetGiftCounterDefaultValue() && IsValidGift();
        }

        public virtual bool IsValidGift()
        {
            MailModel giftModel = GetMailModel(_giftId);
            return giftModel != null && giftModel.Individual;
        }

        public virtual MailModel GetMailModel(int id)
        {
            return _mails?.FirstOrDefault(x => x.Id == id);
        }
        
        public virtual PlayerMailModel GetPlayerMailModel(int id)
        {
            return _playerMails.FirstOrDefault(x => x.Id == id);
        }

        public virtual void MailOpened(int id)
        {
            PlayerMailModel playerGift = new PlayerMailModel()
            {
                Id = id,
                Time = TimeHelper.GetTimeInServer()
            };
            _playerMails.Add(playerGift);
            PlayerManager.Instance.SetPlayerData("mails", new DataUpdateModel(JsonHelper.SerializeObject(_playerMails)),
                GameModeConstants.MAIN_GAME);
        }

        public virtual TransactionResultModel ClaimGift(int id)
        {
            MailModel giftModel = GetMailModel(id);
            if (giftModel != null && giftModel.Reward != null)
            {
                SetGiftClaimed(giftModel);
                return ClaimGift(giftModel.Reward);
            }

            return null;
        }

        protected virtual TransactionResultModel ClaimGift(ITransaction reward)
        {
            return reward.ApplyTransaction(GetGiftCounterKey());
        }

        protected virtual void SetGiftClaimed(MailModel gift)
        {
            MailOpened(gift.Id);
            
            if (gift.Individual)
            {
                _giftId = GetGiftCounterDefaultValue();
                PlayerManager.Instance.SetCounter(GetGiftCounterKey(), _giftId, GameModeConstants.MAIN_GAME);
            }
        }

        public virtual List<MailModel> GetActiveMails()
        {
            List<MailModel> result = new List<MailModel>();
            foreach (MailModel mm in _mails)
            {
                if (mm.Reward == null)
                    result.Add(mm);
                else
                {
                    if (!mm.Individual || mm.Individual && mm.Id == _giftId)
                    {
                        result.Add(mm);
                    }
                }
            }
            result.Sort(delegate(MailModel m1, MailModel m2)
            {
                if (m1.Individual)
                    return -1;
                if (m2.Individual)
                    return 1;
                return m1.Id - m2.Id;
            });
            return result;
        }
    }
}
