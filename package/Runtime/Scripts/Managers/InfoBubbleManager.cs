﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BigfootSdk
{
    /// <summary>
    /// Manager to handle InfoBubbles on screen
    /// </summary>
    public class InfoBubbleManager : Singleton<InfoBubbleManager>
    {
        /// <summary>
        /// The pool to use
        /// </summary>
        public ObjectPool Pool;

        /// <summary>
        /// List of instantiated bubbles
        /// </summary>
        protected List<InfoBubbleDialog> _bubbles = new List<InfoBubbleDialog>();
    
        /// <summary>
        /// Trigger an info bubble
        /// </summary>
        /// <param name="prefabName">The prefab's name for the bubble</param>
        /// <param name="id">An id</param>
        /// <param name="position">The position to spawn it</param>
        /// <param name="text">The text to use</param>
        /// <returns>The spawned info bubble</returns>
        public virtual GameObject TriggerInfoBubble(string prefabName, string id, Transform parent, string text, Vector2 offset)
        {
            // Make sure we dont have this bubble already showing
            if (_bubbles.All(bubble => bubble.BubbleModel.Id != id))
            {
                // Get an object from the pool
                var infoBubble = Pool.GetObjectForType(prefabName, parent);

                // Add it to the instantiated bubbles list
                var infoBubbleModel = new InfoBubbleModel() {Id = id, GameObject = infoBubble};
                if (infoBubble != null)
                {
                    // Show it
                    var infoBubbleDialog = infoBubble.GetComponent<InfoBubbleDialog>();
                    infoBubbleDialog.TriggerNotification(infoBubbleModel, offset, text);
                    _bubbles.Add(infoBubbleDialog);
                }

                return infoBubble;
            }

            return null;
        }
        
        /// <summary>
       /// Force hides an info bubble
       /// </summary>
       /// <param name="id">The bubble's id to hide</param>
       /// <param name="withAnimation">If we want to hide it with animation or just disable it</param>
       public virtual void ForceHideInfoBubble(string id, bool withAnimation = false)
       {
           var bubble = _bubbles.FirstOrDefault(b => b.BubbleModel.Id == id);
           if (bubble != null)
           {
               if(withAnimation)
                    bubble.HideNotification();
               else
               {
                   bubble.transform.localScale = Vector3.zero;
                   RemoveFromShowing(bubble);   
               }
           }
       }
       
        /// <summary>
        /// Removes the info bubble from the showing list
        /// </summary>
        /// <param name="bubble"></param>
        public virtual void RemoveFromShowing(InfoBubbleDialog bubble)
        {
            Pool.PoolObject(bubble.BubbleModel.GameObject);
            _bubbles.Remove(bubble);
        }
    }
}
