﻿using System;
using BigfootSdk.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections.Generic;
using BigfootSdk.ErrorReport;
using UnityEngine.AddressableAssets;
using UnityEngine.U2D;
using BigfootSdk.Helpers;
using Object = UnityEngine.Object;

namespace BigfootSdk.SceneManagement
{
    /// <summary>
    /// Bigfoot scene manager. Add some custom behavior to the UnityEngine.SceneManagement.SceneManager class
    /// </summary>
    public static class BigfootSceneManager
    {
        /// <summary>
        /// Operation type.
        /// </summary>
        enum OperationType
        {
            Addressables,
            UnusedAssets,
            Scene
        }
        
        /// <summary>
        /// List of addressables to release. The key must be equal to asset name.
        /// </summary>
        static List<string> _addressablesToRelease = new List<string>();

        /// <summary>
        /// The references to the addressables
        /// </summary>
        static Dictionary<string, object> _addressablesReferences = new Dictionary<string, object>();

        /// <summary>
        /// The current async operation.
        /// </summary>
        static OperationType _currentOperation;

        /// <summary>
        /// The amount of async operations during scene load.
        /// </summary>
        static float _asyncOperations;

        /// <summary>
        /// The last scene used
        /// </summary>
        public static string LastScene;

        /// <summary>
        /// The percentage complete of addressables async operations.
        /// </summary>
        static float _addressablesLoadPercentage;

        /// <summary>
        /// The unload unused assets operation.
        /// </summary>
        static AsyncOperation _unloadUnusedAssetsOperation;

        /// <summary>
        /// The load scene operation.
        /// </summary>
        static AsyncOperation _loadSceneOperation;

        /// <summary>
        /// The initial time of the scene load
        /// </summary>
        static float _initialTime;
        
        /// <summary>
        /// The name of the previous scene name
        /// </summary>
        static string _prevSceneName;
        
        /// <summary>
        /// Flag to know if we just need the addressables to finish to change scene
        /// </summary>
        static bool _waitingForAddressables = false;

        /// <summary>
        /// The amount of addressables loaded
        /// </summary>
        static int _addressablesLoaded = 0;

        /// <summary>
        /// The amount of addressables that we need to load
        /// </summary>
        static int _addressablesNeeded = 0;

        /// <summary>
        /// Do we want to unload unused assets before a new scene load?
        /// </summary>
        static bool _unloadUnusedAssets;

        /// <summary>
        /// The name of the scene to load
        /// </summary>
        static string _sceneToLoad;

        /// <summary>
        /// The load scene mode
        /// </summary>
        private static LoadSceneMode _mode;
        
        /// <summary>
        /// Reset the initial values
        /// </summary>
        static void ResetInitialValues()
        {
            _waitingForAddressables = false;
            _addressablesLoaded = 0;
            _addressablesNeeded = 0;
        }
        
        /// <summary>
        /// Gets the percentage loaded.
        /// </summary>
        /// <value>The percentage loaded.</value>
        public static float PercentageLoaded
        {
            get
            {
                if (_currentOperation == OperationType.Addressables)
                    return _addressablesLoadPercentage / _asyncOperations;
                else if (_currentOperation == OperationType.UnusedAssets)
                    return (1f / _asyncOperations) + (_unloadUnusedAssetsOperation.progress / _asyncOperations);
                else
                    return (1f / _asyncOperations) * (_asyncOperations - 1) + (_loadSceneOperation.progress / _asyncOperations);
            }
        }
        
        /// Loads the scene.
        /// </summary>
        /// <param name="sceneName">Scene name.</param>
        /// <param name="unloadUnsedAssets">If <see langword="true"/> unused resources will be unloaded.</param>
        public static void LoadScene(string sceneName, bool unloadUnsedAssets = false)
        {
            LoadScene(sceneName, LoadSceneMode.Single, unloadUnsedAssets);
        }
        
        public static void LoadScene (string sceneName, LoadSceneMode mode, bool unloadUnsedAssets = false)
        {
            _mode = mode;
            SceneEvents.StartLoadingScene(sceneName);

            _initialTime = Time.time;
            _prevSceneName = GetActiveScene().name;
            LastScene = _prevSceneName;
            _sceneToLoad = sceneName;
            _unloadUnusedAssets = unloadUnsedAssets;
            
            if (unloadUnsedAssets)
                _asyncOperations = 3;
            else
                _asyncOperations = 2;

            ReleaseAddressables();

            _waitingForAddressables = true;
            
            CheckIfAllAddressablesFinished();
        }

        /// <summary>
        /// Finish the scene load.
        /// </summary>
        /// <param name="sceneName">Scene name.</param>
        /// <param name="prevSceneName">Previous scene name.</param>
        /// <param name="initialTime">Initial time.</param>
        static void FinishSceneLoad (string sceneName, string prevSceneName, float initialTime)
        {
            _loadSceneOperation = SceneManager.LoadSceneAsync(sceneName, _mode);
            _currentOperation = OperationType.Scene;

            _loadSceneOperation.completed += (aOp) =>
            {
                SceneEvents.SceneLoaded(sceneName);

                float operationTime = Time.time - initialTime;
                AnalyticsManager.Instance.TrackSceneLoadEvent(operationTime, prevSceneName, sceneName);

                ResetInitialValues();
            };
        }

        /// <summary>
        /// Gets the active scene.
        /// </summary>
        /// <returns>The active scene.</returns>
        public static Scene GetActiveScene()
        {
            return SceneManager.GetActiveScene();
        }

        /// <summary>
        /// Adds the addressable key to release.
        /// </summary>
        /// <param name="key">Addressable key.</param>
        public static void AddAddressableToRelease(string key)
        {
            if (!_addressablesToRelease.Contains(key))
                _addressablesToRelease.Add(key);
        }

        /// <summary>
        /// Loads the addressable.
        /// </summary>
        /// <param name="key">addressable key.</param>
        /// <param name="tries">recursive tries to avoid infinite loop.</param>
        public static void LoadAddressable<T>(string key, int tries = 0) where T : Object
        {
            _currentOperation = OperationType.Addressables;
            _addressablesNeeded++;

            AddressablesHelper.LoadAssetAsync<T>(key, (loadResult) =>
            {
                if (loadResult == null)
                {
                    string text = string.Format("Failed to load addressable: {0}", loadResult.name);
                    LogHelper.LogError(text, Environment.StackTrace);
                    if (tries > 0)
                    {
                        LoadAddressable<T>(key, --tries);
                        return;
                    }
                }
                else
                {
                    _addressablesReferences.Add(key, loadResult);
                }
                _addressablesLoaded++;

                _addressablesLoadPercentage = (float)_addressablesLoaded / (float)_addressablesNeeded;

                CheckIfAllAddressablesFinished();
            });
        }

        /// <summary>
        /// Checks if we can switch scene
        /// </summary>
        static void CheckIfAllAddressablesFinished()
        {
            if (_addressablesLoaded == _addressablesNeeded && _waitingForAddressables)
            {
                if (_unloadUnusedAssets)
                {
                    _unloadUnusedAssetsOperation = Resources.UnloadUnusedAssets();
                    _currentOperation = OperationType.UnusedAssets;

                    _unloadUnusedAssetsOperation.completed += (unload) => {
                        FinishSceneLoad(_sceneToLoad, _prevSceneName, _initialTime);
                    };
                }
                else
                {
                    FinishSceneLoad(_sceneToLoad, _prevSceneName, _initialTime);
                }
            }
        }

        /// <summary>
        /// Releases the addressables.
        /// </summary>
        static void ReleaseAddressables ()
        {
            foreach (string key in _addressablesToRelease)
            {
                if (_addressablesReferences.ContainsKey(key))
                {
                    Addressables.Release(_addressablesReferences[key]);
                    _addressablesReferences.Remove(key);
                }
                else
                {
                    LogHelper.LogWarning(string.Format("Addressable key {0} doesn't exist in references", key));
                }
            }
        }
    }
}

