﻿using System;
using System.Collections;
using System.Collections.Generic;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using UnityEngine;
using CodeStage.AdvancedFPSCounter;
using Zenject;

namespace BigfootSdk.Analytics
{
    /// <summary>
    /// Manager in charge of analytics
    /// </summary>
    public class AnalyticsManager : LoadableSingleton<AnalyticsManager>
    {
        /// <summary>
        /// Gets the analytics services.
        /// </summary>
        /// <value>The analytics services.</value>
        [Inject]
        public List<IAnalyticsService> AnalyticsServices = new List<IAnalyticsService>();

        /// <summary>
        /// Session id.
        /// </summary>
        public string SessionId => _sessionId;
        
        /// <summary>
        /// Session id.
        /// </summary>
        private string _sessionId;

        /// <summary>
        /// Configuration for the Analytics Manager
        /// </summary>
        private AnalyticsConfiguration _analyticsConfiguration;

        /// <summary>
        /// The moment in seconds when the app goes to background.
        /// </summary>
        private float _goToBackgroundTime;

        /// <summary>
        /// The amount of seconds in background.
        /// </summary>
        private float _totalBackgroundTime = 0;

        private List<AnalyticsEventModel> _pendingEvents = new List<AnalyticsEventModel>();

        /// <summary>
        /// Initializes the analytics backend.
        /// </summary>
        public override void StartLoading ()
		{
			LogHelper.LogSdk ("Starting Analytics...", LogHelper.DEPENDENCY_MANAGER_TAG);
            
            // Assign the configuration file
            _analyticsConfiguration = SdkManager.Instance.Configuration.AnalyticsConfiguration;
            if (_analyticsConfiguration == null)
            {
                LogHelper.LogWarning("Need to assign a AnalyticsConfiguration to the SDKConfiguration.");
                return;
            }
            
            int initialized = 0;
            int servicesAmount = AnalyticsServices.Count;
            
            // Create a unique session id
            _sessionId = Guid.NewGuid().ToString();
            string userId = PlayerManager.Instance.GetPlayerId();
            LogHelper.LogSdk($"Analytics services: {servicesAmount}", LogHelper.DEPENDENCY_MANAGER_TAG);
            if (servicesAmount > 0)
            {
                for (int i = 0; i < servicesAmount; i++)
                {
                    if (!string.IsNullOrEmpty(userId))
                        AnalyticsServices[i].SetUserId(userId);
                    AnalyticsServices[i].InitializeAnalytics(() =>
                    {
                        initialized++;
                        LogHelper.LogSdk($"Analytics services initialized: {initialized}", LogHelper.DEPENDENCY_MANAGER_TAG);
                        if (initialized == servicesAmount)
                        {
                            LogHelper.LogSdk("Analytics Finished Loading", LogHelper.DEPENDENCY_MANAGER_TAG);
                            FinishedLoading(true);
                            SendPendingEvents();
                            TrackSessionStartEvent();
                            
                            // Start dispatching events
                            StartCoroutine("DispatchEvents");
                            
                            // Try to start the FPS Events
                            TryStartFpsEvents();
                        }
                    });
                }
            }
            else
            {
                LogHelper.LogSdk("No analytics service set up");
                FinishedLoading(true);
            }
        }

        internal void Reset()
        {
            foreach (var analyticsService in AnalyticsServices)
            {
                analyticsService.Reset();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            PlayerManager.OnPlayerIdSet += SetUserId;
        }

        private void OnDisable()
        {
            PlayerManager.OnPlayerIdSet -= SetUserId;
        }

        /// <summary>
        /// Sets the user Id for all the services.
        /// </summary>
        /// <param name="userId">The id of the user</param>
        void SetUserId(string userId)
        {
            foreach (var analyticsService in AnalyticsServices)
            {
                analyticsService.SetUserId(userId);
            }
        }

        void SendPendingEvents()
        {
            LogHelper.LogSdk("sending pending events", LogHelper.DEPENDENCY_MANAGER_TAG);
            foreach (var e in _pendingEvents)
                TrackCustomEvent(e.Name, e.Data, true);
            _pendingEvents.Clear();
            LogHelper.LogSdk("sent pending events", LogHelper.DEPENDENCY_MANAGER_TAG);
        }

        /// <summary>
        /// Starts the fps events
        /// </summary>
        void TryStartFpsEvents()
        {
            // If in the EventsToIgnore, ignore sending this event
            if (_analyticsConfiguration.EventsToIgnore != null && _analyticsConfiguration.EventsToIgnore.Contains("HealthAVGFPS") && _analyticsConfiguration.EventsToIgnore.Contains("HealthMemStatus"))
            {
                foreach (var analyticsService in AnalyticsServices)
                {
                    analyticsService.EventIgnored("HealthAVGFPS", null);
                    analyticsService.EventIgnored("HealthMemStatus", null);
                }
                return;
            }
            
            // Start the coroutine that sends the Health events
            if(AFPSCounter.Instance != null)
                StartCoroutine(TrackHealthEvent());
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
            
            TrackSessionEndEvent();
        }

        protected virtual void TrackSessionEndEvent()
        {
            foreach (var analyticsService in AnalyticsServices)
            {
                analyticsService.TrackSessionEndEvent();
            }
        }

        private void OnApplicationPause(bool paused)
        {
            if (paused)
            {
                _goToBackgroundTime = Time.realtimeSinceStartup;
            }
            else
            {
                _totalBackgroundTime += (Time.realtimeSinceStartup - _goToBackgroundTime);
            }
        }

        /// <summary>
        /// Tells Analytics Services to Dispatch Events
        /// </summary>
        /// <returns></returns>
        IEnumerator DispatchEvents()
        {
            while (true)
            {
                foreach (var analyticsService in AnalyticsServices)
                {
                    analyticsService.DispatchEvents();
                }
                yield return new WaitForSeconds(_analyticsConfiguration.DispatchEventsPeriod);
            }
        }

        /// <summary>
        /// Tracks session start event
        /// </summary>
        /// <returns></returns>
        protected virtual void TrackSessionStartEvent()
        {
            foreach (var analyticsService in AnalyticsServices)
            {
                analyticsService.TrackSessionStartEvent();
            }
        }

        /// <summary>
        /// Tells Analytics Services to Dispatch Events
        /// </summary>
        /// <returns></returns>
        IEnumerator TrackHealthEvent()
        {
            var fpsManager = AFPSCounter.Instance;
            
            var fpsData = new Dictionary<string, object>()
            {
                {"AverageFPS", 0},
                {"MinFPS", 0},
                {"MaxFPS", 0},
                {"SessionTime", 0},
                {"ForegroundTime", 0},
                {"SceneName", ""}
            };

            var memData = new Dictionary<string, object>()
            {
                {"MemAlloc", 0},
                {"MemTotal", 0},
                {"SessionTime", 0},
                {"ForegroundTime", 0}
            };

            while (true)
            {
                fpsData["AverageFPS"] = fpsManager.fpsCounter.LastAverageValue;
                fpsData["MinFPS"] = fpsManager.fpsCounter.LastMinimumValue;
                fpsData["MaxFPS"] = fpsManager.fpsCounter.LastMaximumValue;
                fpsData["SessionTime"] = Time.realtimeSinceStartup;
                fpsData["ForegroundTime"] = Time.realtimeSinceStartup - _totalBackgroundTime;
                fpsData["SceneName"] = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;

                memData["MemAlloc"] = fpsManager.memoryCounter.LastAllocatedValue;
                memData["MemTotal"] = fpsManager.memoryCounter.LastTotalValue;
                memData["SessionTime"] = Time.realtimeSinceStartup;
                memData["ForegroundTime"] = Time.realtimeSinceStartup - _totalBackgroundTime;

                foreach (var analyticsService in AnalyticsServices)
                {
                    analyticsService.TrackCustomEvent("HealthAVGFPS", fpsData);
                    analyticsService.TrackCustomEvent("HealthMemStatus", memData);
                }
                yield return new WaitForSeconds(_analyticsConfiguration.HealthDispatchEventsPeriod);
            }
        }


        /// <summary>
        /// Tracks a transaction event.
        /// </summary>
        /// <param name="data">Data.</param>
        public virtual void TrackTransactionEvent (TransactionEventModel data)
        {
            foreach (var analyticsService in AnalyticsServices)
            {
                analyticsService.TrackTransactionEvent(data);
            }
        }

        /// <summary>
        /// Tracks a custom event.
        /// </summary>
        /// <param name="eventName">Event name.</param>
        /// <param name="data">Data.</param>
        public virtual void TrackCustomEvent (string eventName, Dictionary<string, object> data, bool isPeding = false)
        {
            if (IsLoaded && _pendingEvents.Count == 0 || isPeding)
            {
                // If in the EventsToIgnore, ignore sending this event
                if (_analyticsConfiguration.EventsToIgnore != null && _analyticsConfiguration.EventsToIgnore.Contains(eventName))
                {
                    foreach (var analyticsService in AnalyticsServices)
                    {
                        analyticsService.EventIgnored(eventName, data);
                    }
                    return;
                }

                // If in the EventsToRename, rename the event
                if (_analyticsConfiguration.EventsToRename != null && _analyticsConfiguration.EventsToRename.ContainsKey(eventName))
                {
                    eventName = _analyticsConfiguration.EventsToRename[eventName];
                }
            
                // If this event needs to be discriminated for LTEs
                if (_analyticsConfiguration.EventsForLTE != null && _analyticsConfiguration.EventsForLTE.Contains(eventName))
                {
                    if(GameModeManager.CurrentGameMode == GameModeConstants.LIMITED_TIME_EVENT)
                        eventName += "_LTE";
                }
            
                foreach (var analyticsService in AnalyticsServices)
                {
                    analyticsService.TrackCustomEvent(eventName, data);
                }
            }
            else
            {
                _pendingEvents.Add(new AnalyticsEventModel() {Name = eventName, Data = data} );
            }
        }
        
        /// <summary>
        /// Tracks a custom property.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        /// <param name="propertyValue">Property value.</param>
        public virtual void TrackCustomProperty (string propertyName, string propertyValue)
        {
            foreach (var analyticsService in AnalyticsServices)
            {
                analyticsService.TrackCustomProperty(propertyName, propertyValue);
            }
        }
        
        /// <summary>
        /// Tracks an IAP Purchase
        /// </summary>
        /// <param name="itemModel">The item that was purchased</param>
        /// <param name="cost">The real money cost of this IAP</param>
        /// <param name="origin">The origin</param>
        public virtual void TrackIapPurchase (BaseItemModel itemModel, RealMoneyCost cost, string origin = "")
        {
            foreach (var analyticsService in AnalyticsServices)
            {
                analyticsService.TrackIapPurchase(itemModel, cost, origin);
            }
        }

        /// <summary>
        /// Tracks the health load time event.
        /// </summary>
        /// <param name="loadTime">Load time.</param>
        /// <param name="currentScene">Current loaded scene.</param>
        public virtual void TrackDependencyLoadEvent (float loadTime, string currentScene)
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("LoadTime", loadTime);
            data.Add("CurrentScene", currentScene);

            TrackCustomEvent("DependencyLoad", data);
        }

        /// <summary>
        /// Tracks the scene load time event.
        /// </summary>
        /// <param name="loadTime">Load time.</param>
        /// <param name="prevScene">Previous scene.</param>
        /// <param name="newScene">New scene.</param>
        public virtual void TrackSceneLoadEvent (float loadTime, string prevScene = null, string newScene = null)
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("LoadTime", loadTime);
            
            if (prevScene != null)
                data.Add("PreviousScene", prevScene);
            
            if (newScene != null)
                data.Add("NewScene", newScene);

            TrackCustomEvent("SceneLoadTime", data);
        }

        /// <summary>
        /// Tracks the UII nteraction event.
        /// </summary>
        /// <param name="action">Action.</param>
        /// <param name="location">Location.</param>
        /// <param name="elementName">Element name.</param>
        /// <param name="elementType">Element type.</param>
        public virtual void TrackUIInteractionEvent (string action, string location, string elementName, string elementType)
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("UIAction", action);
            data.Add("UILocation", location);
            data.Add("UIName", elementName);
            data.Add("UIType", elementType);
            
            TrackCustomEvent("UIInteraction", data); 
        }

        /// <summary>
        /// Tracks the tutorial event.
        /// </summary>
        /// <param name="tutorialId">Tutorial identifier.</param>
        /// <param name="stageName">Stage name.</param>
        /// <param name="result">Result.</param>
        public virtual void TrackTutorialEvent (string tutorialId, string stageName, int index, string result)
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("TutorialId", tutorialId);
            data.Add("StageName", stageName);
            data.Add("Index", index);
            data.Add("Result", result);
            
            TrackCustomEvent("TutorialAction", data);
        }

        /// <summary>
        /// Tracks the tutorial started event
        /// </summary>
        public virtual void TrackTutorialStartedEvent()
        {
            TrackCustomEvent("TutorialStarted", null);
        }

        /// <summary>
        /// Tracks the tutorial ended event
        /// </summary>
        public virtual void TrackTutorialEndedEvent()
        {
            TrackCustomEvent("TutorialEnded", null);
        }

        /// <summary>
        /// Tracks when a user completes a milestone
        /// </summary>
        public virtual void TrackMilestoneEvent(int index)
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("Index", index);
            TrackCustomEvent("TutorialCheckpoint", data);
        }
    }

    internal class AnalyticsEventModel
    {
        public string Name;
        public Dictionary<string, object> Data;
    }
}
