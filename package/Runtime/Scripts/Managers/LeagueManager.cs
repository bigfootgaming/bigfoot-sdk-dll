﻿using System.Collections;
using System.Collections.Generic;
using BigfootSdk;
using UnityEngine;

namespace  BigfootSdk.Backend
{
    public class LeagueManager : LoadableSingleton<LeagueManager>
    {
        /// <summary>
        /// The league service interface.
        /// </summary>
        private ILeagueService _leagueService;

        public ILeagueService LeagueService{
            get { 
                if (_leagueService == null) {
#if BACKEND_FIREBASE_FIRESTORE
                    _leagueService = new FirebaseFirestoreLeagueService();
#endif
                }
                return _leagueService;
            }
        }

        public override void StartLoading()
        {
#if BACKEND_FIREBASE_FIRESTORE
            LeagueService.InitLeagues();
#endif
        }
    }
}

