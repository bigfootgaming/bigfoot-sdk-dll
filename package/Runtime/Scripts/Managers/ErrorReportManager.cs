﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using BigfootSdk.Backend;
using BigfootSdk.Helpers;

namespace BigfootSdk.ErrorReport
{
    /// <summary>
    /// Manager in charge of error reports.
    /// </summary>
    public class ErrorReportManager : LoadableSingleton<ErrorReportManager>
    {
        /// <summary>
        /// The error report service.
        /// </summary>
#pragma warning disable 0649
        private IErrorReportService _service;
#pragma warning restore 0649

        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <value>The service.</value>
        public IErrorReportService Service
        {
            get
            {
                if (_service == null)
                {
#if FIREBASE_CRASHLYTICS
                    _service = new FirebaseErrorReportService();    
#endif
                }
                return _service;
            } 
        }

        // Start is called before the first frame update
        public override void StartLoading()
        {
            LogHelper.LogSdk ("Initializing Error Report...", LogHelper.DEPENDENCY_MANAGER_TAG);
            if (Service != null)
            {
                Service.Init(() =>
                {
                    string playerId = PlayerManager.Instance.GetPlayerId();
                    if (!string.IsNullOrEmpty(playerId))
                    {
                        Service?.SetUserId(playerId);
                    }
                    else
                    {
                        PlayerManager.OnPlayerIdSet += SetPlayerId;
                    }

                    FinishedLoading(true);
                });
            }
            else
            {
                FinishedLoading(true);
            }
        }

        void SetPlayerId(string id)
        {
            Service?.SetUserId(PlayerManager.Instance.GetPlayerId());
            PlayerManager.OnPlayerIdSet -= SetPlayerId;
        }

        /// <summary>
        /// Log the specified text. Useful to add information logs before a crash or non-fatal error.
        /// </summary>
        /// <param name="text">Text.</param>
        public void Log (string text)
        {
            if (Service != null)
                Service.Log(text);
        }

        /// <summary>
        /// Log the specified exception.
        /// </summary>
        /// <param name="e">E.</param>
        public void LogException (Exception e)
        {
            if (Service != null)
                Service.LogException(e);
        }

        /// <summary>
        /// Log the specified exception.
        /// </summary>
        /// <param name="exceptionMessage">Exception message.</param>
        /// <param name="stackTrace">Exception stack trace.</param>
        public void LogException (string exceptionMessage, string stackTrace)
        {
            Exception e = new Exception(exceptionMessage);
            var stackTraceField = typeof(Exception).GetField("_stackTraceString", BindingFlags.Instance | BindingFlags.NonPublic);
            stackTraceField.SetValue(e, string.IsNullOrEmpty(stackTrace) ? Environment.StackTrace : stackTrace);
            LogException(e);
        }
    }
}

