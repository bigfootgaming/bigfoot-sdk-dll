﻿using System;
using System.Collections.Generic;
using System.Linq;
using BigfootSdk.Analytics;
using BigfootSdk.Helpers;
using BigfootSdk.LTE;
namespace BigfootSdk.Backend
{
	/// <summary>
	/// Manager in charge of everything related to the Tasks
	/// </summary>
	public abstract class TasksManager : LoadableSingleton<TasksManager>
	{
		/// <summary>
		/// Event for when a task is completed
		/// </summary>
		public static Action<ITaskModel> OnTaskCompleted;
		/// <summary>
		/// Event for when the active tasks changed
		/// </summary>
		public static Action OnActiveTasksChanged;
		/// <summary>
		/// Event for when the amount of a task changes
		/// </summary>
		public static Action<int> OnTaskAmountChanged;
		/// <summary>
		/// Event for when a task is collected
		/// </summary>
		public static Action<ITaskModel> OnTaskCollected;
		/// <summary>
		/// The Tasks Model
		/// </summary>
		public TasksModel TasksModel => _tasksModel;
		/// <summary>
		/// The active Tasks
		/// </summary>
		public List<ITaskModel> ActiveTasks => _activeTasks;
		/// <summary>
		/// The Progression Service
		/// </summary>
		public ITaskProgressionService ProgressionService => _progressionService;
		/// <summary>
		/// The Tasks Model
		/// </summary>
		protected TasksModel _tasksModel;
		/// <summary>
		/// The active Tasks
		/// </summary>
		protected List<ITaskModel> _activeTasks;
		/// <summary>
		/// The Progression Service
		/// </summary>
		protected ITaskProgressionService _progressionService;
		/// <summary>
		/// Analytics Manager
		/// </summary>
		protected AnalyticsManager _analyticsManager;
		/// <summary>
		/// Flag to know if we are changing modes
		/// </summary>
		protected bool _isChangingGamemode;
        		
        protected override void OnEnable()
        {
        	base.OnEnable();
        	GameModeManager.OnGameModeStartedChanging += OnGameModeStartedChanging;
        }
        
        void OnDisable()
        {
        	GameModeManager.OnGameModeStartedChanging -= OnGameModeStartedChanging;
        }

        void OnGameModeStartedChanging(string gameMode)
        {
        	_isChangingGamemode = true;
        }

        /// <summary>
        /// Initializes the Tasks
        /// </summary>
        public override void StartLoading()
        {
            LogHelper.LogSdk("Initializing Tasks...", LogHelper.DEPENDENCY_MANAGER_TAG);
            // Cache the analytics manager
            _analyticsManager = AnalyticsManager.Instance;
            // Initialize the progression service
            InitializeProgressionService();
            // Fetch the config from the server
            _tasksModel = ConfigHelper.LoadConfig<TasksModel>(LimitedTimeEventsManager.Instance.GetConfigurationMapping("tasks"));
            // Grab the active tasks
            var activeTasksJson = PlayerManager.Instance.GetFromPlayerData("active_tasks", GameModeManager.CurrentGameMode);
            if (string.IsNullOrEmpty(activeTasksJson))
            {
	            // If it's empty, get new tasks from the progression service
	            _activeTasks = _progressionService.GetNewTasks(_tasksModel.SimultaneousTasks);
	            // Initialize them
	            foreach (var task in _activeTasks)
	            {
		            task.InitializeTaskModel();
		            var data = new Dictionary<string, object>()
		            {
			            {"result", "STARTED"},
			            {"taskId", task.TaskId}
		            };
		            _analyticsManager.TrackCustomEvent("TaskAction", data);
	            }
	            // Save the progress
	            SaveTaskProgress();
            }
            else
            {
	            // Parse the active tasks
	            _activeTasks = JsonHelper.DesarializeObject<List<ITaskModel>>(activeTasksJson);
	            // Check if we need to add more tasks
	            CheckIfCanAddMoreTasks();
            }
            LogHelper.LogSdk("Tasks Finished Loading", LogHelper.DEPENDENCY_MANAGER_TAG);
            FinishedLoading(true);
        }
		// Initializes the Progression service
		protected abstract void InitializeProgressionService();
		/// <summary>
		/// Saves the Tasks progress
		/// </summary>
        protected virtual void SaveTaskProgress()
        {
	        PlayerManager.Instance.SetPlayerData("active_tasks", new DataUpdateModel(JsonHelper.SerializeObject(ActiveTasks)), GameModeManager.CurrentGameMode);
        }
		/// <summary>
		/// Updates the progress of a task
		/// </summary>
		/// <param name="task">The Task to update the progress</param>
		/// <param name="delta">The progress made for this task</param>
        public virtual void UpdateProgress(ITaskModel task, double delta)
        {
	        // If the task is already completed, ignore updating it's progress
	        if (task.IsComplete() || _isChangingGamemode)  
		        return;
	        // Update the progress
	        task.UpdateProgress(delta);
	        // Notify it's change
	        OnTaskAmountChanged?.Invoke(task.TaskId);
	        // Save it's progress
	        SaveTaskProgress();
        }
		/// <summary>
		/// Checks if can add more Tasks
		/// </summary>
		/// <returns></returns>
        public virtual List<ITaskModel> CheckIfCanAddMoreTasks()
        {
	        // Get the current amount of tasks active
	        int currentAmountOfTasks = _activeTasks.Count;
	        // If we have room to add more
	        if (currentAmountOfTasks < _tasksModel.SimultaneousTasks)
	        {
		        // Grab new tasks from the ProgressionService
		        var newTasks = _progressionService.GetNewTasks(_tasksModel.SimultaneousTasks - _activeTasks.Count);
		        // Initialize the new tasks
		        foreach (var t in newTasks)
		        {
			        t.InitializeTaskModel();
			        var data = new Dictionary<string, object>()
			        {
				        {"result", "STARTED"},
				        {"taskId", t.TaskId}
			        };
			        _analyticsManager.TrackCustomEvent("TaskAction", data);
		        }
		        // Add them to the active tasks
		        _activeTasks.AddRange(newTasks);
		        // Save the progress
		        SaveTaskProgress();
		        // Send the active tasks changed event
		        if(currentAmountOfTasks != _activeTasks.Count)
					OnActiveTasksChanged?.Invoke();
		        // Return the new added tasks
		        return newTasks;
	        }
	        return null;
        }
		/// <summary>
		/// Collects the reward of a given Task
		/// </summary>
		/// <param name="task">The task to collect</param>
		/// <returns>The reward</returns>
        public virtual TransactionResultModel CollectCompletedTaskReward(ITaskModel task)
        {
	        // If the task is not completed, ignore the collect
	        if (!task.IsComplete())
		        return null;
	        // Send the appropiate event
	        OnTaskCollected?.Invoke(task);
	        var data = new Dictionary<string, object>()
	        {
		        {"result", "COLLECTED"},
		        {"taskId", task.TaskId}
	        };
	        _analyticsManager.TrackCustomEvent("TaskAction", data);
	        // Apply the transaction and return the TransactionResultModel
	        return task.Reward != null ? task.Reward.ApplyTransaction("TaskReward") : new TransactionResultModel();
        }
		/// <summary>
		/// Complete the task
		/// </summary>
		/// <param name="task">The task to complete</param>
        public virtual void CompleteTask(ITaskModel task)
        {
	        var data = new Dictionary<string, object>()
	        {
		        {"result", "COMPLETED"},
		        {"taskId", task.TaskId}
	        };
	        _analyticsManager.TrackCustomEvent("TaskAction", data);
	        // Send the event
	        OnTaskCompleted?.Invoke(task);
        }
		/// <summary>
		/// Removes the completed task from the active list
		/// </summary>
		/// <param name="task">The task to remove from the active list</param>
        public virtual void RemoveCompletedTaskFromActive(ITaskModel task)
        {
	        // If the task is not completed, don't remove it
	        if (!task.IsComplete())
		        return;
	        // Remove the task from the active list
	        var taskToRemove = _activeTasks.FirstOrDefault(t => task.TaskId == t.TaskId);
	        // If we found the task, remove it
	        if (taskToRemove != null)
	        {
		        _activeTasks.Remove(taskToRemove);
	        }
	        // Send the event
	        OnActiveTasksChanged?.Invoke();
	        // Save the progress
	        SaveTaskProgress();
        }
	}
}