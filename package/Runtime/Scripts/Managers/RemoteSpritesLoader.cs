using BigfootSdk.Backend;
using BigfootSdk.Helpers;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace BigfootSdk.Sprites
{
    public class RemoteSpritesLoader : ILoadable
    {
        public bool LoadOnFirstLaunch;
        
        public override void StartLoading()
        {

            if (LoadOnFirstLaunch)
            {
                LoadRemoteSprites();
            }
            else
            {
                //should wait player manager completely loaded
                if (PlayerManager.Instance.FirstOpen)
                    FinishedLoading(true);
                else
                    LoadRemoteSprites();
            }
        }


        void LoadRemoteSprites()
        {
            AddressablesHelper.LoadAssetAsync<TextAsset>("RemoteSprites",(loadResult) =>
            {
                if (loadResult == null)
                {
                    LogHelper.LogWarning("Failed to load remote sprites");
                    FinishedLoading(true);
                }
                else
                {
                    SpriteManager.Instance.ParseSpriteModels(loadResult.text);
                    FinishedLoading(true);
                }
            });
        }
    }
}