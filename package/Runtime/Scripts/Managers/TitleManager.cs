﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using BigfootSdk.Helpers;
using BigfootSdk.Panels;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;
using Zenject;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Manager in charge of everything related to the title, including progress, game & config data
	/// </summary>
	public class TitleManager : Singleton<TitleManager>
	{
		public static Action<bool> OnTitleDataUpdateChecked;
		
		/// <summary>
		/// The TitleModel
		/// </summary>
        [HideInInspector]
		public TitleModel TitleData = new TitleModel ();

		/// <summary>
		/// Key for local title data version
		/// </summary>
		public const string LocalTitleDataVersionKey = "title_data_version";
		
		
		/// <summary>
		/// Only local data, always server data, or smart (production build will automatically use smart)
		/// </summary>
		public TitleDataLoadMode LoadMode = TitleDataLoadMode.Smart;

		public enum TitleDataLoadMode
		{
			OnlyLocal, //only loads title data from resources or from local folder
			AlwaysServer, //always downloads the corresponding title data version from server
			Smart //checks local version and server version, and decide what to use
		};

		/// <summary>
		/// Local title data saved.
		/// </summary>
		private ES3File _titleDataFile;

		[Inject]
		/// <summary>
		/// Gets the title service.
		/// </summary>
		/// <value>The title service.</value>
		public ITitleService TitleService { get; }

		private bool _usingServerTitleData;

		public bool UsingServerTitleData => _usingServerTitleData;

		/// <summary>
		/// Load title data saved locally.
		/// </summary>
		public virtual void LoadLocalTitleData()
		{
			//config
			_titleDataFile = GetSavedTitleData();
			Dictionary<string, object> titleData = new Dictionary<string, object>();

			if (SdkManager.Instance.Configuration.BackendConfiguration.SkipServerTitleDataInFirstSessions &&
			    PlayerPrefs.GetInt("BF-use-local-title-data", 0) == 0)
			{
				//built in first sessions title data
				TextAsset textAsset = Resources.Load<TextAsset>("InitTitleData");
				if (textAsset != null)
				{
					titleData = JsonHelper.DesarializeObject<Dictionary<string, object>>(textAsset.text);
				}
				LogHelper.LogSdk ("Loading built-in init title data", LogHelper.TITLE_MANAGER_TAG);
			}
			else
			{
				//built in complete title data
				TextAsset textAsset = Resources.Load<TextAsset>("TitleData");
				if (textAsset != null)
				{
					titleData = JsonHelper.DesarializeObject<Dictionary<string, object>>(textAsset.text);
				}

				int builtInTitleDataVersion = 0;
				if (titleData.Count > 0 && titleData.ContainsKey(LocalTitleDataVersionKey))
					int.TryParse(titleData[LocalTitleDataVersionKey].ToString(), NumberStyles.None, CultureInfo.InvariantCulture,  out builtInTitleDataVersion);
			
				//saved title data
				Dictionary<string, object> latestSavedTitleData = _titleDataFile.Load("Data", new Dictionary<string, object>());
				int latestSavedTitleDataVersion = 0;
				if (latestSavedTitleData.Count > 0 && latestSavedTitleData.ContainsKey(LocalTitleDataVersionKey))
					int.TryParse(latestSavedTitleData[LocalTitleDataVersionKey].ToString(),NumberStyles.None, CultureInfo.InvariantCulture, out latestSavedTitleDataVersion);
			
				//if the built in title data is newer than last saved title data, we have to use the built in one
				if (latestSavedTitleDataVersion.CompareTo(builtInTitleDataVersion) > 0 && LoadMode != TitleDataLoadMode.OnlyLocal)
				{
					titleData = latestSavedTitleData;
					LogHelper.LogSdk ("Loading last-saved title data. Version: " + latestSavedTitleDataVersion, LogHelper.TITLE_MANAGER_TAG);
				}
				else
				{
					LogHelper.LogSdk ("Loading built-in main title data", LogHelper.TITLE_MANAGER_TAG);
				}
			}
			

			foreach (KeyValuePair<string, object> kvp in titleData)
			{
				string key = kvp.Key;
				string value = kvp.Value.ToString();
				TitleData.Data[key] = Regex.Replace(value, @"\t|\n|\r", "").Trim('"');
			}
			
			//server time
			string localValue = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
			TitleData.Data["server_time"] = Regex.Replace(localValue, @"\t|\n|\r", "").Trim('"');
			BackendManager.Instance.IsOffline = true;
		}
		
		/// <summary>
		/// Load local title data file.
		/// </summary>
		/// <returns></returns>
		ES3File GetSavedTitleData()
		{
			try
			{
				return new ES3File("titleData");
			}
			catch (Exception)
			{
				return new ES3File("titleData", false);
			}
		}

		/// <summary>
		/// Update title data.
		/// </summary>
		/// <param name="data"></param>
		public void UpdateTitleData(Dictionary<string, object> data, bool additive)
		{
			LogHelper.LogSdk("Updating title data", LogHelper.TITLE_MANAGER_TAG);
			
			string servertime = GetFromTitleData("server_time");

			if (!additive)
			{
				TitleData.Data.Clear();
			}

			foreach (KeyValuePair<string, object> pair in data)
			{
				TitleData.Data[pair.Key] = Regex.Replace(pair.Value.ToString(), @"\t|\n|\r", "").Trim('"');
			}

			TitleData.Data["server_time"] = servertime;

			if (!additive)
			{
				SaveLatestTitleData();
			}
		}

		/// <summary>
		/// Update key value pair
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		public void UpdateTitleDataKeyValuePair(string key, string value)
		{
			if (TitleData.Data.ContainsKey(key) && !string.IsNullOrEmpty(value))
			{
				string oldValue = TitleData.Data[key];
				string newValue = Regex.Replace(value, @"\t|\n|\r", "").Trim('"');
				TitleData.Data[key] = newValue;
				LogHelper.LogSdk(string.Format("Updating Title Data. Key: {0}, OldValue: {1}, NewValue; {2}", key, oldValue, newValue), LogHelper.TITLE_MANAGER_TAG);
			}
		}

		/// <summary>
		/// Update local title data.
		/// </summary>
		void SaveLatestTitleData()
		{
			_titleDataFile.Save<Dictionary<string, object>>("Data", Instance.TitleData.Data);
			_titleDataFile.Sync();
		}

		/// <summary>
		/// Load server title data.
		/// </summary>
		/// <returns></returns>
		public void LoadServerTitleData(Action<bool> callback)
		{
			LogHelper.LogSdk ("Loading server title data...", LogHelper.TITLE_MANAGER_TAG);
			//if not allow offline change title load mode
			_usingServerTitleData = false;
			BackendConfiguration config = SdkManager.Instance.Configuration.BackendConfiguration;
			if (!config.AllowOfflineLoad && LoadMode == TitleDataLoadMode.OnlyLocal && config.ForceAlwaysServer)
				LoadMode = TitleDataLoadMode.AlwaysServer;
			#if ENVIRONMENT_PROD
			LoadMode = TitleDataLoadMode.Smart;
			#endif
			if (config.SkipServerTitleDataInFirstSessions && BackendManager.Instance.UseLocalTitleData)
			{
				BackendManager.Instance.IsOffline = false;
				LogHelper.LogSdk("Server title skipped during first sessions");
				callback?.Invoke(true);
			}
			else
			{
				TitleService.InitializeTitle ((success) =>
				{
					_usingServerTitleData = true;
					LogHelper.LogSdk ("Server Title Finished Loading: " + success, LogHelper.TITLE_MANAGER_TAG);
					callback?.Invoke(success);
				});
			}
		}

		/// <summary>
		/// Check de app version
		/// </summary>
		/// <param name="minVersionString"></param>
		/// <param name="callback"></param>
		public void CheckAppVersion(string minVersionString, Action<string> callback)
		{
			string currentVersionString = Application.version;
			LogHelper.LogSdk($"min version: {minVersionString}, current version: {currentVersionString}");
			if (!string.IsNullOrEmpty(minVersionString))
			{
				var minVersion = new Version(minVersionString);
				var currentVersion = new Version(currentVersionString);

				var result = minVersion.CompareTo(currentVersion);
				if (result > 0)
				{
					LogHelper.LogSdk ("Server Title Finished Loading: Force update is needed!", LogHelper.TITLE_MANAGER_TAG);
					ShowForceUpdate();
				}
				else
				{
					callback?.Invoke(currentVersionString);
				}
			}
			else
			{
				callback?.Invoke(currentVersionString);
			}
		}

		public void ShowForceUpdate()
		{
			PanelsEvents.ShowPanel("ForceUpdate", PanelBehavior.KeepActive, null, null, 10);
		}

		public void GetConfigFromUrl(string file, bool additive, Action callback)
		{
			TitleService?.GetConfigFromUrl(file, additive, callback);
		}

		public void CheckTitleDataUpdate()
		{
			TitleService?.CheckForUpdates();
		}

		internal void Reset()
		{
			TitleService?.Reset();
		}

		#region TitleData

		/// <summary>
		/// Gets a value from title data.
		/// </summary>
		/// <returns>the value requested.</returns>
		/// <param name="key">Key.</param>
		public string GetFromTitleData (string key)
		{
			if (TitleData != null && TitleData.Data != null && TitleData.Data.ContainsKey (key)) {
				return TitleData.Data [key].ToString();
			}
			return "";
		}
		#endregion
	}
}
