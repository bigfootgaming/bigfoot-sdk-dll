using System.Threading.Tasks;
using Zenject;

namespace BigfootSdk.Backend
{
    public class AddressablesManager : LoadableSingleton<AddressablesManager>
    {
        [Inject]
        public IAddressablesService Service { get; }
        
        public override void StartLoading()
        {
            if(Service != null)
                Service.InitializeAddressables(FinishedLoading);
            else
                FinishedLoading(true);
        }
    }
}
