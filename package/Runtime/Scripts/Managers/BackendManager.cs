﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using BigfootSdk.Analytics;
using BigfootSdk.ErrorReport;
using BigfootSdk.Extensions;
using BigfootSdk.Helpers;
using UnityEngine.Networking;
using UnityEngine;

namespace BigfootSdk.Backend
{
	/// <summary>
	/// Manager in charge of backend initialization, availablity, etc
	/// </summary>
	public class BackendManager : LoadableSingleton<BackendManager>
	{
		public static Action<string> OnBackendTimeoutReached;
		
		public static Action<string, string> OnBackendConnectionFailed;
		
		public static Action<string> OnBackendConnectionWarning;

		public static Action<string> OnBackendConnectionWorking;

		private List<string> _backendCalls = new List<string>();

		private Action _resetCallback;

		private bool _pendingReset;

		
		/// <summary>
		/// The backend service interface.
		/// </summary>
		private IBackendService _backendService;

		/// <summary>
		/// The callback for when we finished loading
		/// </summary>
		private Action<bool> _loadingCallback;

		/// <summary>
		/// The SDK configuration
		/// </summary>
		private SdkConfiguration _sdkConfiguration;

		public bool IsOffline;

		public bool IsServerTimeSync = false;

		public bool UseLocalTitleData { get; private set; }
		
		public bool UseLocalPlayerData { get; private set; }
		
		private List<TimeoutData> _timeouts = new List<TimeoutData>();

		private bool _restartTimeouts;

		/// <summary>
		/// Gets the backend service.
		/// </summary>
		/// <value>The backend service.</value>
		private IBackendService BackendService {
			get { 
				if (_backendService == null) {
                    #if BACKEND_AWS
                        _backendService = new AWSBackendService(this);
                    #elif BACKEND_GS
                        _backendService = new GSBackendService ();
					#elif BACKEND_FIREBASE
						_backendService = new FirebaseBackendService();
					#endif
					
				}
				return _backendService;
			}
		}

		/// <summary>
		/// Initializes the backend
		/// </summary>
		public override void StartLoading ()
		{
			LogHelper.LogSdk ("Starting Backend...", LogHelper.DEPENDENCY_MANAGER_TAG);
			
			// Cache the sdk configuration
			_sdkConfiguration = SdkManager.Instance.Configuration;
			
			UseLocalTitleData = PlayerPrefs.GetInt("BF-use-local-title-data", 0) == 0;
			
			UseLocalPlayerData = PlayerPrefs.GetInt("BF-use-local-player-data", 0) == 0;
			
			// Cache the callback
			_loadingCallback = FinishedLoading;
			if (IsBackendAvailable ())
				GetServerTime();
			else
				ListenBackendAvailable (FinishedLoading);
		}

		public void SetFirstSessionForTitleDataComplete()
		{
			PlayerPrefs.SetInt("BF-use-local-title-data", 1);
			UseLocalTitleData = false;
		}
		
		public void SetFirstSessionForPlayerDataComplete()
		{
			PlayerPrefs.SetInt("BF-use-local-player-data", 1);
			UseLocalPlayerData = false;
		}

		public void ResetFirstSessionForTitleDataComplete()
		{
			PlayerPrefs.SetInt("BF-use-local-title-data", 0);
			UseLocalTitleData = true;
		}
		
		public void ResetFirstSessionForPlayerDataComplete()
		{
			PlayerPrefs.SetInt("BF-use-local-player-data", 0);
			UseLocalPlayerData = true;
		}

		/// <summary>
		/// Checks if the backend is available to be used.
		/// </summary>
		/// <returns><c>true</c> if the backend is available; otherwise, <c>false</c>.</returns>
		public bool IsBackendAvailable ()
		{
			bool available = BackendService.IsBackendAvailable ();
			return available;
		}

		/// <summary>
		/// Listens for the event that the backend becomes available. Besides the optional callback,
		/// it will call BackendEvents.OnBackendAvailable
		/// </summary>
		/// <param name="callback">Callback</param>
		public void ListenBackendAvailable (Action<bool> callback = null)
		{
			LogHelper.LogSdk ("Listening for when the backend is available...", LogHelper.DEPENDENCY_MANAGER_TAG);

			BackendService.ListenBackendAvailable (GetServerTime);
		}
		
		/// <summary>
		/// Get server time
		/// </summary>
		protected virtual void GetServerTime()
		{
			ExecuteCloudCode("getServerTime", new Dictionary<string, object>(), (result) =>
			{
				string serverValue = result["value"].ToString();
				TitleManager.Instance.TitleData.Data["server_time"] = Regex.Replace(serverValue, @"\t|\n|\r", "").Trim('"');
				IsOffline = false;
				Instance.IsServerTimeSync = true;
				BackendFinishedLoading();
			}, () =>
			{
				Instance.IsServerTimeSync = false;
				BackendFinishedLoading();
			});
		}

		/// <summary>
		/// Backend finished loading.
		/// </summary>
		void BackendFinishedLoading()
		{
			LogHelper.LogSdk (string.Format ("Backend Finished Loading"), LogHelper.DEPENDENCY_MANAGER_TAG);
            _loadingCallback(true);
		}


		public void SetBackendBusy(string origin)
		{
			if (!_backendCalls.Contains(origin))
			{
				_backendCalls.Add(origin);
				LogHelper.LogSdk ($"Add Backend call {origin}", LogHelper.DEPENDENCY_MANAGER_TAG);
			}
		}

		public void SetBackendNotBusy(string origin)
		{
			if (_backendCalls.Contains(origin))
			{
				LogHelper.LogSdk ($"Remove Backend call {origin}", LogHelper.DEPENDENCY_MANAGER_TAG);
				_backendCalls.Remove(origin);
			}
			if (_backendCalls.Count == 0 && _pendingReset)
			{
				Reset(_resetCallback);
			}
		}

		public void Reset(Action callback)
		{
			if (_backendCalls.Count == 0)
			{
				ResetTimeouts();
				SdkManager.Instance.Reset();
				AdsManager.Instance.Reset();
				AnalyticsManager.Instance.Reset();
				PlayerManager.Instance.Reset();
				AuthenticationManager.Instance.Reset();
				TitleManager.Instance.Reset();
				BackendService?.Reset();
				MemoryHelper.TryCleanMemory(true, true, "ResetBackendManager");
				_resetCallback = null;
				_pendingReset = false;
				LogHelper.LogSdk (string.Format ("Backend reset done"), LogHelper.DEPENDENCY_MANAGER_TAG);
				callback?.Invoke();
			}
			else
			{
				LogHelper.LogSdk (string.Format ("Backend reset pending"), LogHelper.DEPENDENCY_MANAGER_TAG);
				PlayerManager.Instance.SyncAvailable = false;
				_pendingReset = true;
				_resetCallback = callback;
			}
		}

		/// <summary>
        /// Executes the cloud code.
        /// </summary>
        /// <param name="method">Method.</param>
        /// <param name="data">Data.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
		public void ExecuteCloudCode (string method, Dictionary<string, string> data = null, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
		{
			BackendService.ExecuteCloudCode (method, data, successCallback, failureCallback);
		}
        
        /// <summary>
        /// Executes the cloud code. The data values have no need to be serialized.
        /// </summary>
        /// <param name="method">Method.</param>
        /// <param name="data">Data.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        public void ExecuteCloudCode (string method, Dictionary<string, object> data = null, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
        {
	        BackendService.ExecuteCloudCode (method, data, successCallback, failureCallback);
        }
        
        /// <summary>
        /// Executes the web request.
        /// </summary>
        /// <param name="method">Method name.</param>
        /// <param name="data">Data.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        public void ExecuteWebRequest(string method, Dictionary<string, string> data = null, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
        {
            string jsonData = "{}";
            if (data != null)
                jsonData = JsonHelper.SerializeObject(data);
            StartCoroutine(WebRequest(method, jsonData, successCallback, failureCallback));
        }

        /// <summary>
        /// Executes the web request.
        /// </summary>
        /// <param name="method">Method name.</param>
        /// <param name="data">Data.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        public void ExecuteWebRequest(string method, string data = null, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
        {
            StartCoroutine(WebRequest(method, data, successCallback, failureCallback));
        }

        /// <summary>
        /// Webs the request.
        /// </summary>
        /// <returns>The request.</returns>
        /// <param name="method">Method name.</param>
        /// <param name="data">Data.</param>
        /// <param name="successCallback">Success callback.</param>
        /// <param name="failureCallback">Failure callback.</param>
        IEnumerator WebRequest(string method, string data = null, Action<Dictionary<string, object>> successCallback = null, Action failureCallback = null)
        {
            string url = string.Format("{0}/{1}", SdkManager.Instance.Configuration.BackendConfiguration.AwsBackendConfiguration.EndpointURL, method);
            using (UnityWebRequest request = UnityWebRequest.Put(url, data))
            {
                request.method = UnityWebRequest.kHttpVerbPOST;
                request.SetRequestHeader("Content-Type", "application/json");
                request.SetRequestHeader("Accept", "application/json");
                if(_sdkConfiguration.BackendConfiguration.AwsBackendConfiguration != null)
					request.SetRequestHeader("app_name", _sdkConfiguration.BackendConfiguration.AwsBackendConfiguration.AppName);
                request.certificateHandler = new BigfootCertificateModel();
                
                yield return request.SendWebRequest();

                if (!request.isNetworkError)
                {
                    string responseString = request.downloadHandler.text;
                    var response = JsonHelper.DesarializeObject<AWSLambdaResponse>(responseString);
                    if (response.statusCode == 201)
                        successCallback?.Invoke(response.data);
                    else
                    {

                        string errorMessage = string.Format("WebRequestError::statusCode: {0}, message: {1}", response.statusCode, response.errorMessage);
                        LogHelper.LogError(errorMessage, Environment.StackTrace);
                        failureCallback?.Invoke();
                    }
                }
                else
                {
                    string errorMessage = string.Format("WebRequestError::{0}", request.error);
                    LogHelper.LogError(errorMessage, Environment.StackTrace);
                    failureCallback?.Invoke();
                }
            }
        }

		/// <summary>
		/// Creates a timeout tasks
		/// </summary>
		/// <param name="timeout">amount of milliseconds</param>
		/// <param name="cancellationToken">token to cancel tasks</param>
		/// <param name="warningFraction">if this is greater than 0 a warning task will be created with timeout equal to the original amount divided by this value</param>
		/// <param name="callback">action callback</param>
        public void SetBackendTimeout(string origin, int timeout, CancellationTokenSource cancellationTokenSource, 
			int warningFraction, Action<bool> callback, Action<CancellationTokenSource> refreshCTS)
		{

			Task timeOutTask = Task.Delay(timeout, cancellationTokenSource.Token);
			_timeouts.Add(new TimeoutData()
			{
				Id = timeOutTask.Id, Origin = origin, Time = timeout, Fraction = warningFraction,
				Source = cancellationTokenSource, Callback = callback, RefreshCTS = refreshCTS
			});
	        LogHelper.LogSdk("Starting timeout task. Origin: "+ origin, LogHelper.BACKEND_TIMEOUT_TAG);
	        timeOutTask.ContinueWithOnMainThread(task =>
	        {
		        if (task.IsCompleted && !task.IsCanceled && !task.IsFaulted)
		        {
			        LogHelper.LogSdk("Timeout reached. Origin: "+ origin, LogHelper.BACKEND_TIMEOUT_TAG);
			        ErrorReportManager.Instance.Log("Timeout reached. Origin: "+ origin);
			        if (SdkManager.Instance.Configuration.BackendConfiguration.AllowOfflineLoad)
				        callback?.Invoke(true);
			        OnBackendTimeoutReached?.Invoke(origin);
			        RemoveTimeout(timeOutTask.Id);
		        }
		        if (task.IsCanceled)
		        {
			        LogHelper.LogSdk("Timeout canceled. Origin: "+ origin , LogHelper.BACKEND_TIMEOUT_TAG);
			        RemoveTimeout(timeOutTask.Id);
		        }
	        });
	        if (warningFraction > 0)
	        {
		        Task warningTask = Task.Delay(timeout/warningFraction, cancellationTokenSource.Token);
		        warningTask.ContinueWithOnMainThread(task =>
		        {
			        if (task.IsCompleted && !task.IsCanceled && !task.IsFaulted)
						OnBackendConnectionWarning?.Invoke(origin);
		        });
	        }
        }

		void ResetTimeouts()
		{
			for (int i = 0; i < _timeouts.Count; i++)
			{
				if (!_timeouts[i].Source.IsCancellationRequested)
				{
					_timeouts[i].Source.Cancel();
				}
			}
				
			LogHelper.LogSdk("Timeouts cancelled: "+_timeouts.Count, LogHelper.BACKEND_TIMEOUT_TAG);
		}



		void RemoveTimeout(int id)
		{
			TimeoutData t = _timeouts.Find(x => x.Id == id);
			if (t != null)
			{
				_timeouts.Remove(t);
			}
		}

#if !UNITY_EDITOR
		private void OnApplicationFocus(bool hasFocus)
		{
			LogHelper.LogSdk($"_restartTimeouts: {_restartTimeouts}", LogHelper.BACKEND_TIMEOUT_TAG);
			if (!hasFocus)
			{
				ResetTimeouts();	
				_restartTimeouts = true;
				LogHelper.LogSdk($"_restartTimeouts: {_restartTimeouts}", LogHelper.BACKEND_TIMEOUT_TAG);
			}
			else if (_restartTimeouts)
			{
				int count = 0;
				int timeoutCount = _timeouts.Count;
				LogHelper.LogSdk("Restarting timeouts: "+_timeouts.Count, LogHelper.BACKEND_TIMEOUT_TAG);
				while (count < timeoutCount)
				{
					_timeouts[0].Source = new CancellationTokenSource();
					_timeouts[0].RefreshCTS(_timeouts[0].Source);

					SetBackendTimeout(_timeouts[0].Origin, _timeouts[0].Time, _timeouts[0].Source,
						_timeouts[0].Fraction, _timeouts[0].Callback, _timeouts[0].RefreshCTS);
					_timeouts.RemoveAt(0);
					count++;
				}
				_restartTimeouts = false;
				LogHelper.LogSdk($"_restartTimeouts: {_restartTimeouts}", LogHelper.BACKEND_TIMEOUT_TAG);
			}

		}
#endif

		class TimeoutData
		{
			public int Id;
			public string Origin;
			public CancellationTokenSource Source;
			public int Time;
			public int Fraction;
			public Action<bool> Callback;
			public Action<CancellationTokenSource> RefreshCTS;
		}
	}
	
}


