﻿#if BACKEND_FIREBASE_FIRESTORE
using System;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    public class ObscuredStringDictionaryConverter : FirestoreConverter<Dictionary<string, ObscuredString>>
    {
        public override object ToFirestore(Dictionary<string, ObscuredString> value)
        {
            var dic = new Dictionary<string, string>();
            foreach (KeyValuePair<string, ObscuredString> p in value)
            {
                dic.Add(p.Key, p.Value);
            }
            return dic;
        }

        public override Dictionary<string, ObscuredString> FromFirestore(object value)
        {
            if (value == null) {
                throw new ArgumentNullException("value"); 
            }
            else
            {
                var dictionary = (IDictionary<string, object>)value;
                var result = new Dictionary<string, ObscuredString>();
                foreach (KeyValuePair<string, object> p in dictionary)
                {
                    result.Add(p.Key, p.Value.ToString());
                }
                return result;
            }
        }
    }
}


#endif
