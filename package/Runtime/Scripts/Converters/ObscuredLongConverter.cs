﻿#if BACKEND_FIREBASE_FIRESTORE
using System;
using System.Globalization;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    public class ObscuredLongConverter : FirestoreConverter<ObscuredLong>
    {
        public override object ToFirestore(ObscuredLong value)
        {
            long result = value;
            return result;
        }

        public override ObscuredLong FromFirestore(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value"); // Shouldn't happen
            }
            else 
            {
                return long.Parse(value.ToString(), CultureInfo.InvariantCulture);
            } 
        }
    }
}

#endif