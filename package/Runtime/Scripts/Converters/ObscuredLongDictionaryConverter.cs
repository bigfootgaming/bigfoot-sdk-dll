﻿#if BACKEND_FIREBASE_FIRESTORE
using System;
using System.Collections.Generic;
using System.Globalization;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    public class ObscuredLongDictionaryConverter : FirestoreConverter<Dictionary<string, ObscuredLong>>
    {
        public override object ToFirestore(Dictionary<string, ObscuredLong> value)
        {
            var dic = new Dictionary<string, object>();
            foreach (KeyValuePair<string, ObscuredLong> p in value)
            {
                long longValue = p.Value;
                dic.Add(p.Key, longValue);
            }
            return dic;
        }

        public override Dictionary<string, ObscuredLong> FromFirestore(object value)
        {
            if (value == null) {
                throw new ArgumentNullException("value"); 
            }
            else
            {
                var dictionary = (IDictionary<string, object>)value;
                var result = new Dictionary<string, ObscuredLong>();
                foreach (KeyValuePair<string, object> p in dictionary)
                    result.Add(p.Key, long.Parse(p.Value.ToString(), CultureInfo.InvariantCulture));
                return result;
            }
        }
    }
    
}

#endif