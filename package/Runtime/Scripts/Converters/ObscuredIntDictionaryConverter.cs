﻿#if BACKEND_FIREBASE_FIRESTORE
using System;
using System.Collections.Generic;
using System.Globalization;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    public class ObscuredIntDictionaryConverter : FirestoreConverter<Dictionary<string, ObscuredInt>>
    {
        public override object ToFirestore(Dictionary<string, ObscuredInt> value)
        {
            var dic = new Dictionary<string, object>();
            foreach (KeyValuePair<string, ObscuredInt> p in value)
            {
                int intValue = p.Value;
                dic.Add(p.Key, intValue);
            }
        
            return dic;
        }

        public override Dictionary<string, ObscuredInt> FromFirestore(object value)
        {
            if (value == null) {
                throw new ArgumentNullException("value"); 
            }
            else
            {
                var dictionary = (IDictionary<string, object>)value;
                var result = new Dictionary<string, ObscuredInt>();
                foreach (KeyValuePair<string, object> p in dictionary)
                    result.Add(p.Key, int.Parse(p.Value.ToString(), CultureInfo.InvariantCulture));
                return result;
            }
        }
    }
}

#endif
