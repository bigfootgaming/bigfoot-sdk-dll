﻿#if BACKEND_FIREBASE_FIRESTORE
using System;
using System.Collections.Generic;
using System.Globalization;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    public class ObscuredBoolDictionaryConverter : FirestoreConverter<Dictionary<string, ObscuredBool>>
    {
        public override object ToFirestore(Dictionary<string, ObscuredBool> value)
        {
            var dic = new Dictionary<string, object>();
            foreach (KeyValuePair<string, ObscuredBool> p in value)
            {
                bool boolValue = p.Value;
                dic.Add(p.Key, boolValue);
            }
            return dic;
        }

        public override Dictionary<string, ObscuredBool> FromFirestore(object value)
        {
            if (value == null) {
                throw new ArgumentNullException("value"); 
            }
            else
            {
                var dictionary = (IDictionary<string, object>)value;
                var result = new Dictionary<string, ObscuredBool>();
                foreach (KeyValuePair<string, object> p in dictionary)
                    result.Add(p.Key, bool.Parse(p.Value.ToString()));
                return result;
            }
        }
    }
}

#endif