﻿#if BACKEND_FIREBASE_FIRESTORE
using System;
using System.Globalization;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    public class ObscuredIntConverter : FirestoreConverter<ObscuredInt>
    {
        public override object ToFirestore(ObscuredInt value)
        {
            int result = value;
            return result;
        }

        public override ObscuredInt FromFirestore(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value"); // Shouldn't happen
            }
            else
            {
                return int.Parse(value.ToString(), CultureInfo.InvariantCulture);
            }
        }
    }
}

#endif