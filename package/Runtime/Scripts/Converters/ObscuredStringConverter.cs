﻿#if BACKEND_FIREBASE_FIRESTORE
using System;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Firestore;

namespace BigfootSdk.Backend
{
    public class ObscuredStringConverter : FirestoreConverter<ObscuredString>
    {
        public override object ToFirestore(ObscuredString value)
        {
            return value.ToString();
        }

        public override ObscuredString FromFirestore(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value"); // Shouldn't happen
            }
            else 
            {
                return value.ToString();
            }
        }
    }
}
#endif