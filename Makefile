version := $(shell grep version $(dll)/package/package.json | cut -c 15- | rev | cut -c 3- | rev)
current_dir = $(shell pwd)
branch = "upm-1.12"

info:
	@echo =============================
	@echo Bigfoot SDK Root: $(sdk)
	@echo Bigfoot DLL Root: $(current_dir)
	@echo Current Version: $(version)
	@echo =============================

info_branch:
	@echo =============================
	@echo Branch Name: $(branch)
	@echo =============================

copy: info
	@echo Coping Documentation...
	@cp -R $(sdk)/docs $(current_dir)
	@echo  Coping Documentation Done!
	@echo Coping SDK Files...
	@rm -R -f $(current_dir)/package/Editor
	@rm -R -f $(current_dir)/package/Runtime
	@rm -R -f $(current_dir)/package/Samples
	@cp -R $(sdk)/Assets/Bigfoot-sdk/Editor $(current_dir)/package
	@cp -R $(sdk)/Assets/Bigfoot-sdk/Runtime $(current_dir)/package
	@cp -R $(sdk)/Assets/Bigfoot-sdk/Samples $(current_dir)/package
	@echo Coping SDK Files Done!
	@echo Coping Helper Scripts...
	@cp -R $(sdk)/OpenCygwinTerminal.bat $(current_dir)/
	@echo Coping Helper Scripts Done!

commit:
	@echo Committing...
	@git add -A && git commit -m "Pushed new version" && git push
	@echo Committing Done

push: info_branch
	@echo Pushing to server...
	@git subtree push --prefix package origin $(branch)
	@echo Pushing to server Done!

copy_push: copy commit push
	@echo All done!
