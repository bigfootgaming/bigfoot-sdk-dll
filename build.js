// Grabbing parameters for the build process
var args = process.argv.slice(2);
var verbose = args[0];
var ftp;

connectToFTP();
buildResult = buildDll();
if(buildResult == true)
{
    uploadDllToFTP();
}

function connectToFTP()
{
    // Create the ftp object
    console.log("Connecting to FTP...")
    const jsftp = require("jsftp");
    ftp = new jsftp({
        host: "bigfootgaming.net",
        port: 21, // defaults to 21
        user: "martinamat@bigfootgaming.net",
        pass: "breakinglab123" // defaults to "@anonymous"
        });

    if(ftp.err == undefined)
        console.log("Connected to FTP!")
    else
        console.log("Connection to FTP failed: " + ftp.err)
}

function buildDll() {
    // Start to build the DLL
    console.log("Starting to build DLL...")
    const { spawnSync } = require('child_process');
    dll = spawnSync('msbuild', ['Bigfoot-sdk/Bigfoot-sdk.csproj']);

    // See the result of the build
    if(dll['status'] == 0)
    { 
        console.log("DLL built...");
        return true;
    }
    else
    {
        console.log("DLL build failed");
        return false;
    }
}

function uploadDllToFTP()
{
    // Upload the DLL to the FTP
    console.log("Starting to upload DLL...");
     var fs = require("fs");

    fs.readFile("Bigfoot-sdk/bin/Debug/Bigfoot-sdk.dll", function(err, buffer) {
     if(err) {
          console.log("DLL Read failed...");
          console.log(err);
     }
     else {
         ftp.put(buffer, "sdk/releases/Bigfoot-sdk.dll", function(err) {
             if (err) {
                 console.log("DLL Upload failed...");
                 console.log(err);
             }
             else {
                 console.log("DLL Uploaded to FTP!");
                 uploadPbdToFTP();
             }
         });
     }
 });
}

function uploadPbdToFTP()
{
    // Upload the DLL to the FTP
    console.log("Starting to upload PBD...");
     var fs = require("fs");

    fs.readFile("Bigfoot-sdk/bin/Debug/Bigfoot-sdk.pdb", function(err, buffer) {
     if(err) {
          console.log("PDB Read failed...");
          console.log(err);
     }
     else {
         ftp.put(buffer, "sdk/releases/Bigfoot-sdk.pdb", function(err) {
             if (err) {
                 console.log("PDB Upload failed...");
                 console.log(err);
             }
             else {
                 console.log("PDB Uploaded to FTP!");
                 createDocumentation();
                 beginUploadDocumentationToFTP();
             }
         });
     }
 });
}

function createDocumentation()
{
    // Create the documentation
    console.log("Starting to create documentation...")
    var doxygen = require('doxygen');

    // //Uncomment if you need to create the config again
    // var userOptions = {
    //     OUTPUT_DIRECTORY: "docs",
    //     INPUT: "./",
    //     RECURSIVE: "YES",
    //     FILE_PATTERNS: ["*.cs"],
    //     QUIET: "YES"
    // };
     
    // doxygen.createConfig(userOptions);

    doxygen.run();
    console.log("documentation created!")
}

function beginUploadDocumentationToFTP()
{
    // Upload the Documentation to the FTP
    console.log("Starting to upload documentation...")
    
    var recursive = require("recursive-readdir");
 
    recursive("docs/html/", function (err, files) {
        uploadDocumentationToFTP(files)
    });
}

function uploadDocumentationToFTP(files)
{
    var fileName = __dirname + "/" + files[0];
    var fs = require("fs");

    fs.readFile(fileName, function(err, buffer) {
     if(err) {
          console.log("Docs upload failed...");
          console.log(err);
     }
     else {
         ftp.put(buffer, files[0], function(err) {
             if (!err) {
                process.stdout.write(".")
            }
            else
                console.log(err);
            if (files.length === 1) 
            { 
                console.log("")
                console.log("documentation uploaded!")
                return;
            } 
            uploadDocumentationToFTP(files.slice(1)); // Call again with the rest of the items.
         });
     }
 });
}


    // ftp.put(fileName, files[0], err => {
    //     if (!err) {
    //         process.stdout.write(".")
    //     }
    //     else
    //     console.log(err);
    //     if (files.length === 1) 
    //     { 
    //         console.log("")
    //         console.log("documentation uploaded!")
    //         return;
    //     } 
    //     uploadDocumentationToFTP(files.slice(1)); // Call again with the rest of the items.
    // });
// }