.. _doc_initialize_scene:

Initialize Scene
====================

This scene is going to be used as the entry point of your game.

.. warning:: Make sure this scene is only loaded once per session to avoid multiple issues.\

To get started using it in your project, after you install the SDK, go to the ``Package Manager``, find the entry for the ``Bigfoot Sdk``, and make sure you import from the samples the ``Initialize Scene``. Once imported, you can copy it and move it to your Scenes folder.

.. image:: images/initialize_1.png
  :width: 600
  :align: center
