.. _doc_create_repository:

Create Repository
=================

Contact ``Gaston`` to create a new Bitbucket repository under the Bigfoot's organization, so the whole team can have access to it.

Make sure you add the following :download:`.gitignore <downloads/.gitignore>` file to the repository.
