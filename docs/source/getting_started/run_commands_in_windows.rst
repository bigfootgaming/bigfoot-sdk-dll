.. _doc_win_users:

Run Commands In Windows
=======================

.. note:: If you are OSX user, skip this step.

Install cygwin
---------------------

In order to run scripts and commands in next sections you need to install `cygwin <https://cygwin.com/install.html>`_.

When you're asked for packages to install, search and select ``make`` and ``python3``.

.. figure:: images/run_commands_in_windows_1.png
  :width: 700px
  :align: center

  How to add make package.

Then click `Next` and finish the installation.

Open a terminal
---------------

You can run ``OpenCygwinTerminal.bat`` (located in the root directory) to open a terminal.