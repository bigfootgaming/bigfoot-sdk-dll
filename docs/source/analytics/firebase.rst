.. _doc_firebase_analytics:

Firebase
=========

Firebase as a backend
---------------------

If you are using ``Firebase`` as the backend, you need to go to the Unity Package Manager, and add ``Google Analytics for Firebase``.

Once in Unity, go to the folder ``Assets/Configuration`` and select the ``Analytics Configuration`` file. Go to the tab Firebase Analytics, and enable it.

That should be enough to have ``Firebase Analytics`` working inside your project.

Using another backend
---------------------

If you are not using ``Firebase`` as the backend, you first need to create the application in Firebase, download the different environment files, and copy them over to the Unity project. Once that's done, you can follow the steps above.

For a guide on how to setup Firebase, see Firebase Backend [[todo]].

.. note: You don't need to import the different unity packages for analytics to work, only the one mentioned above.
