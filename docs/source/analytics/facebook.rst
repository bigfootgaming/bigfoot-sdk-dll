.. _doc_facebook_analytics:

Facebook
=========

The first step is to into the project the ``Facebook SDK``, that can be found in the `Facebook SDK Unity page <https://developers.facebook.com/docs/unity/>`_.

Once it's imported, go to Facebook -> Edit Settings inside Unity. Make sure you set the correct app name, and Facebook App Id (if you don't have it, ask ``Gaston`` for it.

After Facebook is setup, go to the folder ``Assets/Configuration`` and select the ``Analytics Configuration`` file. Go to the tab ``Facebook Analytics``, and enable it.

That should be enough to have ``Facebook Analytics`` working inside your project.

Android Debug key
-----------------

To validate that the requests are coming from your application, Facebook needs to know the ``Android Debug Key(ADK)`` of your application, and have it set in the Facebook Application.

You need to set one ADK for each keystore you use, and one for the Play Store, if you are using their signing.

To do this.... [[todo]]


Common Issues
-------------

- Error on startup on Android

  If you are getting an error when you open the app in an Android device, it might be due to ``IL2CPP`` stripping some of the files that Facebook needs to run.

  To avoid this, go to Project Settings -> Publishing Settings, and enable the option ``User Proguard File``. That will create a file in ``Assets/Plugins/Android/proguard-user.txt``. Open that file, and copy the following:

  .. code-block:: none

    -keep class com.facebook.internal.* {*;}
    -keep class com.facebook.** {*;}
