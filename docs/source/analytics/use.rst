.. _doc_use_analytics:

Use
====

There are a ton of events that are tracked automatically. To see a full list, check out the :ref:`doc_default_events_analytics`

If you want to track a custom event, you need to:

.. code-block::

  var analyticsManager = AnalyticsManager.Instance;
  var eventData = new Dictionary<string, object>();
  eventData.Add("remaining_lives", 2)
  analyticsManager.TrackCustomEvent("LostLife", eventData);


For Garbage Collection purposes, if you are sending an event frequently, make sure you recicle the dictionary, and not create a new one every single time you call ``TrackCustomEvent``.

.. code-block::

  AnalyticsManager _analyticsManager;
  Dictionary<string, object>() _eventData = new Dictionary<string, object>();

  void Start()
  {
      _analyticsManager = AnalyticsManager.Instance;
  }

  void FrequentlyCalledMethod()
  {
      _eventData.Clear();
      _eventData.Add("remaining_lives", 2)
      _analyticsManager.TrackCustomEvent("LostLife", _eventData);
  }
