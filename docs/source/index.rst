Welcome to Bigfoot SDK's documentation!
=======================================

Welcome to the website for the Bigfoot’s SDK documentation.

If it’s your first time using the SDK, head over to the :ref:`doc_create_repository` section, where you will learn how to setup the SDK for the first time use.

.. toctree::
   :maxdepth: 1
   :caption: General

   general/faq
   general/troubleshooting
   general/changelog

.. toctree::
   :maxdepth: 1
   :caption: Getting Started

   getting_started/run_commands_in_windows
   getting_started/create_repository
   getting_started/create_unity_project
   getting_started/download_sdk
   getting_started/balance_sheet
   getting_started/initialize_scene
   getting_started/dependency_manager

.. toctree::
   :maxdepth: 1
   :titlesonly:
   :caption: Backend

   backend/setup
   backend/services

.. toctree::
  :maxdepth: 1
  :titlesonly:
  :caption: Analytics

  analytics/setup
  analytics/services
  analytics/use
  analytics/default_events

.. toctree::
   :maxdepth: 1
   :titlesonly:
   :caption: Features

   features/remote/index
   features/force_update
   features/rate_us
   features/sprites/index
   features/prerequisites/index
   features/firebase_ab_test_rc
   features/ui_particles
   features/shop/index
   features/mail_system

.. toctree::
    :maxdepth: 1
    :titlesonly:
    :caption: Build the SDK

    sdk/make_changes
    sdk/documentation

.. toctree::
    :maxdepth: 1
    :titlesonly:
    :caption: Admin Tool

    admin/index
    admin/make_changes
