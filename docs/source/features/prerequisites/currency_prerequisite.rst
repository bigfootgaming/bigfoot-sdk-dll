.. _doc_currency_prerequisites:

Currency
========

This prerequisite can be used to check if a user has a certain amount of a currency in a given game mode.

.. note:: Hard currency (``hard``) will always be checked in the main game, since it's shared between the Main Game and the Limited Time Events.

Examples
^^^^^^^^

Evaluates to true if we exactly 0 ``hard`` currency

.. code-block::

  {"$type": "BigfootSdk.CurrencyPrereq, BigfootSdk", "Currency": "hard", "Amount": 0, "Comparison": "eq"}

Evaluates to true if we have over 100 of the currency ``potions`` in the current active event we are in.

.. code-block::

  {"$type": "BigfootSdk.CurrencyPrereq, BigfootSdk", "Currency": "potions", "Amount": 100, "Comparison": "gt", "GameMode" : "LimitedTimeEvent"}

Parameters
^^^^^^^^^^

================= ============= ============== ==============
Parameter         Type          Default        Description
================= ============= ============== ==============
Currency *        string                       They key of the currency
Amount *          int           0              The amount to check against
Comparison        string        eq             Comparison operator to use
GameMode          string        NONE           The game mode to check this in
================= ============= ============== ==============
