.. _doc_start_timer_prerequisites:

Start Timer
===========

This prerequisite can be used to check if a certain timer exists or has started before or after a given time (in unix time stamp).

Examples
^^^^^^^^
Evaluates to true if the timer 'stacked_ads_timer' exists.

.. code-block::

  {"$type":"BigfootSdk.StartTimerPrereq", "Key":"stacked_ads_timer", "Amount" : 0, "Comparison" : "gt" }


Evaluates to true if the timer 'stacked_ads_timer' started before the 30th of July of 2021 at 00:00:00.

.. code-block::

  {"$type":"BigfootSdk.StartTimerPrereq", "Key":"stacked_ads_timer", "Amount" : 1627614000, "Comparison" : "lt" }


Parameters
^^^^^^^^^^

================= ============= ============== ==============
Parameter         Type          Default        Description
================= ============= ============== ==============
Key *             string                       They key of the timer
Amount *          int           0              The amount to check against
Comparison        string        eq             Comparison operator to use
GameMode          string        NONE           The game mode to check this in
================= ============= ============== ==============
