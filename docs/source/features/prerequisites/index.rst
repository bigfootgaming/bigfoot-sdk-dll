.. _doc_prerequisites:

Prerequisites
=============

``Prerequisites`` are used throughout the SDK to model the different conditions a certain feature has to meet in order for it to be valid.

For example, if we want to show an offer only to users who are spenders in the game, we would add a :ref:`Flag Prerequisite <doc_flag_prerequisites>` to that offer, with the key set to ``IsSpender`` and it will only show the offer if the prerequisite is evaluated to true.

Custom prerequisites
--------------------

If you need to create a custom prerequisite in your game, just extend the class ``IPrerequisite`` and override the method ``Check()``.

.. note:: If you create a custom prerequisite, in the ``$type`` you need to use the following format ``[ClassName], Assembly-CSharp``.

.. toctree::
   :maxdepth: 1
   :caption: List of Prerequisites

   counter_prerequisite
   currency_prerequisite
   flag_prerequisite
   game_mode_prerequisite
   idle_level_prerequisite
   last_scene_loaded_prerequisite
   start_timer_prerequisite
   time_prerequisite
   timer_prerequisite
   tutorial_finished_prerequisite
