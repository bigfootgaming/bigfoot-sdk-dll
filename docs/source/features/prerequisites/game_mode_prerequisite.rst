.. _doc_game_mode_prerequisites:

Game Mode
================

This prerequisite can be used to check in which game mode we are currently in.

Examples
^^^^^^^^

Evaluates to true if the user is in the Main Game.

.. code-block::

  {"$type":"BigfootSdk.GameModePrereq", "Comparison":"eq", "GameMode" : "MainGame"}


Evaluates to true if the user is playing in a Limited Time Event.

.. code-block::

  {"$type":"BigfootSdk.GameModePrereq", "Comparison":"eq", "GameMode" : "LimitedTimeEvent"}

Parameters
^^^^^^^^^^

================= ============= ============== ==============
Parameter         Type          Default        Description
================= ============= ============== ==============
GameMode *        string                       They game mode we want to check
Comparison        string        eq             Comparison operator to use
================= ============= ============== ==============
