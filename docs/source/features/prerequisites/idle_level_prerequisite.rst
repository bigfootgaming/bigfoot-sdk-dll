.. _doc_idle_level_prerequisites:

Idle Level
================

This prerequisite can be used to check if the level of the user in the idle core.

.. warning: This works only for the Main Game.

Examples
^^^^^^^^

Evaluates to true if the user is level 11 or above in the current prestige in the idle core.

.. code-block::

  {"$type":"BigfootSdk.IdleLevelPrereq", "Comparison":"gt", "Level" : "10", "CheckMax": "false"}


Evaluates to true if the user ever reached level 11 or above in the idle core.

.. code-block::

  {"$type":"BigfootSdk.IdleLevelPrereq", "Comparison":"gt", "Level" : "10", "CheckMax": "true"}

Parameters
^^^^^^^^^^

================= ============= ============== ==============
Parameter         Type          Default        Description
================= ============= ============== ==============
Level *           int                          The level we want to check against
Comparison        string        eq             Comparison operator to use
CheckMax          bool          true           Should we check the max level achieved, or the one in the current prestige
================= ============= ============== ==============
