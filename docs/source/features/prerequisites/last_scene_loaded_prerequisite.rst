.. _doc_last_scene_loaded_prerequisites:

Last Scene Loaded
==================

This prerequisite can be used to check which was the last scene loaded. For example, in Idle games, it's common to have 2 scenes: Initialize & Idle.
If you want to show something when the user opens the game, you can check to see if the last loaded scene is not the Idle one.

Examples
^^^^^^^^

Evaluates to true if the last scene loaded by the user is not Idle.

.. code-block::

  {"$type":"BigfootSdk.LastSceneLoadedPrereq", "Comparison":"ne", "SceneName" : "Idle"}

Parameters
^^^^^^^^^^

================= ============= ============== ==============
Parameter         Type          Default        Description
================= ============= ============== ==============
SceneName *       string                       They name of the scene
Comparison        string        eq             Comparison operator to use
================= ============= ============== ==============
