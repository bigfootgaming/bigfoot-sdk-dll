.. _doc_timer_prerequisites:

Timer
========

This prerequisite can be used to check if a certain time has passed since an event happened that was tracked with a timer.

Examples
^^^^^^^^

Evaluates to true if more than a day has passed since the last time we saw an offer popup with id 'legendary_event_pack' in a Limited Time Event.

.. code-block::

  {"$type":"BigfootSdk.TimerPrereq", "Key":"legendary_event_pack_PopupLastShown", "Amount" : 86400, "Comparison" : "gt", "GameMode" : "LimitedTimeEvent" }


Parameters
^^^^^^^^^^

================= ============= ============== ==============
Parameter         Type          Default        Description
================= ============= ============== ==============
Key *             string                       They key of the timer
Amount *          int           0              The amount to check against
Comparison        string        eq             Comparison operator to use
GameMode          string        NONE           The game mode to check this in
================= ============= ============== ==============
