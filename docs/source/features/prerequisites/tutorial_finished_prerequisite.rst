.. _doc_tutorial_finished_prerequisites:

Tutorial Finished
==================

This prerequisite can be used to check if a tutorial is finished.

Examples
^^^^^^^^

Evaluates to true if we exactly 0 'hard' currency

.. code-block::

  {"$type":"BigfootSdk.TutorialFinishedPrereq", "IsFinished": true, "TutorialId" : "FTUE"}

Parameters
^^^^^^^^^^

================= ============= ============== ==============
Parameter         Type          Default        Description
================= ============= ============== ==============
TutorialId *      string                       They id of the tutorial
IsFinised         boolean       true           The value to check against
================= ============= ============== ==============
