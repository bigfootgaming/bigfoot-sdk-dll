.. _doc_time_prerequisites:

Time
========

This prerequisite can be used to compare the time in the server to a specific one. This is useful to set the start/end date of offers, or limited time events.

Examples
^^^^^^^^

Evaluates to true if the current date in the server is greater or equals to 2017-01-30 13:00:00.

.. code-block::

  {"$type":"BigfootSdk.TimePrereq", "Comparison":"lt", "Date" : "2017-01-30 13:00:00"}


Parameters
^^^^^^^^^^

================= ============= ============== ==============
Parameter         Type          Default        Description
================= ============= ============== ==============
Date *            string                       Date with format "YYYY-MM-DD hh:mm:ss" in UTC.
Comparison        string        eq             Comparison operator to use
================= ============= ============== ==============
