.. _doc_flag_prerequisites:

Flag
========

This prerequisite can be used to check if a user has a certain flag in a given game mode.

Examples
^^^^^^^^

Evaluates to true if the user has a flag 'IsSpender' with the value true, in the current game mode that we are in.

.. code-block::

  {"$type": "BigfootSdk.FlagPrereq, BigfootSdk", "Key": "IsSpender", "Value": true, "Comparison": "eq"}

Evaluates to true if the user has a flag 'FirstTimeInEvent' with the value true, in the current active event we are in.

.. code-block::

  {"$type": "BigfootSdk.FlagPrereq, BigfootSdk", "Key": "FirstTimeInEvent", "Value": true, "Comparison": "eq", "GameMode" : "LimitedTimeEvent"}

Parameters
^^^^^^^^^^

================= ============= ============== ==============
Parameter         Type          Default        Description
================= ============= ============== ==============
Key *             string                       They key of the flag
Value *           bool          false          The value to check against
Comparison        string        eq             Comparison operator to use
GameMode          string        NONE           The game mode to check this in
================= ============= ============== ==============
