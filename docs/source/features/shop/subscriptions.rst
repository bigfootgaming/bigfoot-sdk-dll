.. _doc_subscriptions:

Subscriptions
=======================================

In this guide we will learn how to use the store subscriptions.

The App Store and Google Play support subscriptions and the Bigfoot SDK uses UNITY IAP for this feature. Once you have the store subscriptions created you must add the configuration needed to your config spreadsheet.

First, you have to add as many shop items to ``$shop_items`` tab as subscriptions you added to the stores. Add the real money costs, the item that represents the subscription content, and everything you need in your case. In the next example you can see three subscription periods for the same subscription content. 

.. image:: images/subscriptions_1.png
  :width: 700
  :align: center

.. note:: For now use only one subscription period for iOS.

For the subscription content you have to add the item to ``items`` tab as a ``SubscriptionItemModel``. If this is the first time you use subscriptions you probably have to add some columns the the ``items`` tab, needed by this new model. ``Frequency`` column supports the following values: ``Daily``, ``Weekly``, ``Monthly``. It is used to know how often the subscription reward is given to the user. You can add your own frequency and override the HandleSubscription method to support it. ``FirstTimeItems`` column is a list of ``DropableModel`` and it is used to give rewards only for the first time the user subscribes. ``Items`` tab is used to give rewards every time the user has to receive them according to the frequency. If you want a different behavior you have to override the HandleSubscription method.

In your code, to purchase a subscription, you have to do the same you do for other shop items.


.. code-block::

  ShopManager.Instance.PurchaseShopItem(item, "Subscription", PurchaseSuccessful, PurchaseFailure);

Check the ``ShopManager`` class. You will see several API method to get useful data from subscriptions.

.. note:: Editor, iOS tests and Google tests behave in different ways, so take care when you test this feature. Ask Martin if you have any doubt or if there is anything missing in this guide.