.. _doc_shop_timers:

Shop Timers
=======================================

In this guide we will learn how to use the shop timers.

In the SDK we saveral types of timers for the shop.

Shop Timer
-----------------------------------

This is the basic timer. It only has a ``Duration`` (in seconds). It can be restarted on login, when the placement is enabled when the previous loop of the timer ended.

Daytime Shop Timer
-----------------------------------

It extends the basic timer. It also has a ``StartingTime``, that refers to the starting time of the day of this timer. So, when the placement is enabled the remainging time is calculated based on these values. For example, if ``StartingTime`` is ``02:00:00`` and ``Duration`` is ``28800``, the timer will start at ``02:00:00``, ``10:00:00`` and ``18:00:00``.

Action Shop Timer
-----------------------------------

This is an abstract class that extends the basic timer, but the timer starts on specific events.

Purchase Item Shop Timer
-----------------------------------

It extends the Action Shop Timer. It also has an ``ItemId``. The timer starts only if the user purchases this item.

Purchase Section Shop Timer
-----------------------------------

It extends the Action Shop Timer. It also has an ``SectionId``. The timer starts only if the user purchases an item of this section.