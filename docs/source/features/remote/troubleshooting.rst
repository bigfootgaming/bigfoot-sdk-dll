.. _doc_remote_troubleshooting:

Troubleshooting
=================

- I'm getting an error saying libcurl doesn't support gs://

  This is because you are trying to load a remote addressable with Firebase, but you haven't setup correctly the Asset Bundle Provider.

- When I try to update remote content, it's not being updated in the Build
  
  Make sure you are waiting for ``AddressablesManager`` to finish loading before loading ANY Addressable, remote or not.
  Also, check if you have setup the catalog to be remote.

  If issues persist, you can add the ADDRESSABLES_LOG_ALL compiler flag to your project, and debug a build to see what is happing. You should be seeing the local catalog hash, then the remote one, and if they don't match, thats when an update is triggered.

  If you have done all of the above, and still cannot get it to work, contact ``Gaston``.