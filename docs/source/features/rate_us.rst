.. _doc_rate_us:

Rate Us
========

The Rate Us system allows you to prompt the user to give us a rate in the corresponding store for the platform.

The first thing you need to do, is make sure that in the tab ``TitleData`` of the Balance Sheet, you have the ``android_url`` and ``ios_url`` set so the manager can know where to redirect the user for the actual rate.

.. note:: If you don't know what values to include in those fields, ask ``Gaston``.

.. image:: images/rate_us_1.png
  :width: 700
  :align: center

For this to work, you need to add the ``RateUsManager`` to your dependencies to be loaded. It needs to be loaded after the ``PlayerManager``.

The manager will keep track of the status of the player regarding the Rate Us feature. In a counter ``RateUs`` it will hold the status:

- ``RATED`` = 1
- ``DONT_ASK_AGAIN`` = 2
- ``NOT_ASKED_YET`` = 3

You can also use the class ``RateUsDialog``, or extend as needed, for the actual behaviour.
