.. _doc_sprite_config:

Sprite Config
=============

1. Open the initialize scene where DependencyManager is located.
2. Add SpriteLoader.cs as a new gameObject child of DependencyManager.
3. Inside DependencyManager you'll find ``Things To Load``. Add SpriteLoader as a new reference.
4. Add SpriteManager.cs as a new gameObject child of Singletons parent container if it's already created.
5. Go to ``Assets/Configurations`` and we need to create 2 files here: ``Sprites.csv`` and ``SpriteLoader_Initialize.txt``. These files should be setted as Addressables and short their names as "Sprites" and "SpriteLoader_Initialize" respectively. The "Initialize" on SpriteLoader_Initialize refers to the name of our current scene name.


Sprites.csv
^^^^^^^^^^^^
Contains the ID needed to find the sprite, the SPRITENAME itself, and the Atlas in which is located. [ID,SPRITENAME,ATLASNAME].
In the following example both id's will be using the same sprite and are located inside QuestInfo SpriteAtlas.
Example:
quest_info_unlock_generator_01,quest_info_unlock_machine,QuestInfo
quest_info_unlock_generator_02,quest_info_unlock_machine,QuestInfo


SpriteLoader_Initialize.txt
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Contains names of SpriteAtlas and Sprites.
In the following example the first item is an atlas as it has type after the comma "," and the second one is just a sprite and does not need to specify the type.
Example:
Generators,SpriteAtlas
PurchaseIcon

Note: when creating spriteAtlasses, we should set them as addressables and short their names to call them easily with the previous 2 files.
Example after creating Addressable for Generators:
from "Assets/Art/Atlas/Generators.spriteatlas"
to "Generators"
