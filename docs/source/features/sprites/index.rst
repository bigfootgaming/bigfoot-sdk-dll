.. _doc_sprites:

Sprites
=============

``Sprites`` are used mostly as SpriteAtlases (addressables) for flexibility on sizes and compression.

.. toctree::
   :maxdepth: 1
   :caption: Sprites

   sprite_config
   sprite_compression
