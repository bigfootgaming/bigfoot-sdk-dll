.. _doc_load_server_data:

Load Server Data On First Load
=======================================

.. note:: This feature is required before going to Test Markets

In our games, we have the configuration for the game both locally (in ``Resources/TitleData.json``), and in our server. One thing we might want to do, is to use the local configuration on the first ever run of the game, so users can get in the game faster.

The ``BackendManager`` has bool called ``LoadServerDataOnFirstOpen`` to do just this. If you set this variable to ``false``, no data will be downloaded on the first session.

Now, if you set that to false, we need a way to tell the SDK that the user is no longer a new one. If you are using the ``TutorialsManager``, the SDK will do this on it's own once a user makes some progress in the tutorial.
If for some reason you don't have a tutorial, or need a different behaviour, you can just call the following:

.. code-block::

  PlayerManager.Instance.SetOldPlayerFlag();

You can also check if it's the users first open or not with the following property in the ``PlayerManager``:

.. code-block::

  PlayerManager.Instance.FirstOpen
