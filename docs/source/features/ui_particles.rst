.. _doc_ui_particles:

UI Particles
================

To use particles in the UI, we are using the package called `Particle Effect For UGUI <https://github.com/mob-sakai/ParticleEffectForUGUI>`_. This package will allow you to sort particles, mask them, and scale them as elements of the UI.

To install it, you have to add the following url to the ``manifest.json`` file of your project, and then follow the instructions on the package's Github page.

.. code-block::

  "com.coffee.ui-particle": "https://github.com/mob-sakai/ParticleEffectForUGUI.git"
