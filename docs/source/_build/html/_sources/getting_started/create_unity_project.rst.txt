Create Unity Project
====================

Once you have the cloned repository, create a new Unity project using the version `2019.4 LTS <https://unity.com/releases/2019-lts>`_.

If for any reason you need a different version, please contact ``Gaston`` or ``Martin`` before doing so. It's important to keep consistency across our portfolio.
