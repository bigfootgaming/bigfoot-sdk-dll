.. _doc_setup_backend:

Setup
=======================

To setup the backend in your game, go to the folder ``Assets/Configuration`` (create it if it doesn't exist).

Inside that folder, create an ``Backend Configuration`` file (right click -> Create -> Configurations -> Backend -> Main). Once it's created, make sure you assign it to the corresponding ``SDK Configuration`` file for that environment.

Once you have it created and assigned to the appropriate environment, make sure you set up the parameters as needed.

=============================== ============================
Parameter                       Description
=============================== ============================
Seconds Between Local Saves     The delay between each of the Player Data local saves
=============================== ============================
