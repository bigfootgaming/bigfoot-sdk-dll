.. _doc_counter_prerequisites:

Counter
========

This prerequisite can be used to check if a user has a certain amount of a counter in a given game mode.

Examples
^^^^^^^^

Evaluates to true if we have a counter with the key 'rateus_popup_shown' with a value greater of equal than 1, in the current game mode that we are in.

.. code-block::

  {"$type": "BigfootSdk.CounterPrereq, BigfootSdk", "Key": "rateus_popup_shown", "Amount": 1, "Comparison": "ge"}

Evaluates to true if we have a counter with the key 'tasks_claimed' with a value less than 5, in the current active event we are in.

.. code-block::

  {"$type": "BigfootSdk.CounterPrereq, BigfootSdk", "Key": "tasks_claimed", "Amount": 5, "Comparison": "lt", "GameMode" : "LimitedTimeEvent"}


Parameters
^^^^^^^^^^

================= ============= ============== ==============
Parameter         Type          Default        Description
================= ============= ============== ==============
Key *             string                       They key of the counter
Amount *          int           0              The amount to check against
Comparison        string        eq             Comparison operator to use
GameMode          string        NONE           The game mode to check this in
================= ============= ============== ==============
