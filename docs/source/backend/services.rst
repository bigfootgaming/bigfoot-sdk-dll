.. _doc_backends:

Backend Services
=======================

Here is a list of all the backend services currently available to use in the SDK. If you are not sure which one you should use, please consult with ``Gaston``.

.. warning:: Only one Backend Service can be used at the same time in a game.

.. toctree::
   :maxdepth: 1
   :caption: Backend Services

   firebase
