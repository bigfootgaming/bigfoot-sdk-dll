.. _doc_admin_search:

Search Players
================

.. warning:: Keep in mind that if you change a player's information in ``prod``, you are changing the information for a live player, and there is no turning back.

Use this feature to search for players in the game, see their information, and make changes to any values that you wish.

.. note:: Make sure you are in the correct environment! You can check this in the buttons on the left side ``(Dev/Staging/Prod)``.

Here you can change any information about the player, as well as open their information in Firebase, or just delete the player.

If you want to edit any existing value, you can double click on it, and the field will become editable.

.. image:: images/search_1.png
  :width: 400
  :align: center
