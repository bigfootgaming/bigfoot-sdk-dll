.. _doc_admin:

Getting Started
================

The Admin Tool is a web application where, depending on your access level, can:

.. toctree::
   :maxdepth: 1
   :caption: Features

   Push Configurations <push>
   Download Configurations <download>
   Search, Edit & Delete Players <search_players>
   Copy Players Data <copy_players>
   Change Game Settings <settings>


If you don't have a user for the tool, or need increased access, please contact ``Gaston``.

Link to the `Admin Tool <https://bigfoot-admin.herokuapp.com/>`_.
