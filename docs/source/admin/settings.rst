.. _doc_admin_settings:

Settings
================

In the settings you can change the id of the Google Spreadsheet where the balance is taken from.

.. image:: images/settings_2.png
  :width: 600
  :align: center

Keep in mind that this is an array of ids, so if you put more than one, it will merge the contents of all of the spreadsheets.

.. note:: To get the id of a Google Sheet, you need to copy the part of the URL after the ``../d/`` and before the ``/edit...``.

  .. image:: images/settings_1.png
    :width: 600
    :align: center
