.. _doc_admin_push:

Push Configuration
===================

.. warning:: When pushing a configuration, please make sure you are in the correct environment! You can check this both in the buttons on the left side ``(Dev/Staging/Prod)``, and also in the text of the button.

Pushing a configuration will take the Google Spreadsheet that is referenced in the Settings, parse all of it's information, and push it to Firebase.

.. image:: images/push_1.png
  :width: 700
  :align: center
