.. _doc_admin_make_changes:

Make Changes
==============

To deploy a new version of the Admin Tool to Heroku, you need to follow the below steps:

1. Make sure you have the `Heroku CLI <https://devcenter.heroku.com/articles/heroku-command-line>`_ installed.
2. Make sure you are logged in, by calling

.. code-block::

  heroku login

3. Open the Terminal and navigate to the root of the ``BigfootAdmin`` repository (where the .git folder is located).
4. Run the following command if this is the first time you do this:

.. code-block::

  heroku git:remote -a bigfoot-admin  

5. After you have all of the changes committed and pushed in a branch, push that branch to deploy it as follows:

.. code-block::

  git push heroku [BRANCH_NAME]:master

After you execute that command, you should start seeing some logs from Heroku, until the application is deployed.

.. image:: images/heroku_1.png
  :width: 700
  :align: center

.. note:: If you need to see the logs of the application running live, you can open the Terminal and run

  .. code-block::

    heroku logs --tail
