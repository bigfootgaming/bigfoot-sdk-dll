.. _doc_admin_download:

Download Configuration
=======================

Downloading a configuration will take the Google Spreadsheet that is referenced in the Settings, parse all of it's information, and export it locally as a json file.

This is useful if you want to test some changes locally in the Unity Editor, before pushing them for the rest of the team.

.. image:: images/download_1.png
  :width: 700
  :align: center
