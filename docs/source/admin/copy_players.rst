.. _doc_admin_copy:

Copy Players
================

.. warning:: This will erase all of the information of the player in the ``To`` field. Be careful if doing this in production.

You can also copy all of the information from one player (the top one), to another (the bottom one).

.. image:: images/copy_1.png
  :width: 600
  :align: center
