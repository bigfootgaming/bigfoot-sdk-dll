.. _doc_documentation:

Documentation
==============

Every commit made to the SDK, should update it's corresponding documentation, or it won't be approved. The documentation is at the root of the SDK's repository, under ``/docs``.

The documentation used for the ``Bigfoot SDK`` is powered by `ReadTheDocs <https://readthedocs.org>`_, and uses a combination of reStructuredText & Sphinx as it's markup syntax and parser.

Every time a commit is made to master, ReadTheDocs triggers a hook and the documentation is built automatically.

Helpful link
- `reST Cheat Sheet <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_

First Time Setup
----------------

Before committing changes in the documentation, you need to able to build it and test it locally, to make sure it compiles and it's formatted as intended.

Open up the Terminal, and run the following pip3 commands in order.

.. note:: If the pip3 command is not found, you need to have `Python <https://www.python.org/downloads/>`_ installed in your machine.

.. code-block::

  pip3 install -U sphinx
  pip3 install sphinx_rtd_theme

Build Locally
----------------

After you installed Sphinx, go to the ``docs`` folder in the Terminal, and run

.. code-block::

  make html

If no errors where found, you will see the output of the built documentation in ``docs/build/html`` (This folder is ignored by the version control)

If you open ``docs/build/html/index.html`` in your browser, you can see the modified local version. If everything looks good, go ahead with the commit.

Add New Page / Section
-----------------------

If the Section doesn't exist:

- Duplicate an existing category.
- Change the name of the folder.
- Open ``index.rst``, and add a new toctree (Table of contents tree) in the file. You can copy one of the existing ones, and change the ``Caption``.
- Empty the contents of the ``images/`` folder if there is anything in there.
- Delete all of the pages except one, and do all of the steps below except the first one.

If the Section already exists, and you want to create a new page under it:

- Duplicate one of the existing pages in the same directory.
- Change the name of the file.
- Open the new file, and change the first line to ``.. _doc_[page_name]:``.
- Change the title of the page (the line above the ===========). This title is what will show up on the left side nav bar.
- Open ``index.rst``, locate the section, and add a new entry in the toctree ``[section_name]/[page_name]``.
- Build and make sure everything is working properly.

Add Images
----------

The first thing you need to do to add an image, is to put it in the correct folder. Inside the section you are working on, go to the ``/images`` folder (or create it if it doesn't exist).

Take a screenshot of the thing you want to show, put it there, and name it accordingly ([name_of_page]_#)

After you have done that, in your code, put the following:

.. note:: Play around with the width until you get something that looks good. Don't upload huge images or things that can't even be read.

.. code-block:: none

    .. image:: images/[name_of_page]_#.png
      :width: 700
      :align: center
