.. _doc_make_changes:

Make Changes
==============

If you want to make a change in the code, fix a bug, add a feature, or touch up a documentation page, you've come to the right place.

The first thing you need to do, is download both the `Bigfoot SDK <https://bitbucket.org/bigfootgaming/bigfoot-sdk/src/master/>`_ and the `Bigfoot SDK DLL <https://bitbucket.org/bigfootgaming/bigfoot-sdk-dll/src/master/>`_

Before doing any work on the SDK, make sure you pull the latest from master, and make a branch to work on your changes.

.. note:: Everything inside ``Assets/Bigfoot-sdk`` will be copied over to the actual package. Keep this in mind when you make changes.

When you have everything working locally, you can create a PR to ``Gaston`` or ``Martin`` with those changes.

Once it's approved, go to the SDK DLL repository, and also make sure you don't have any pulls pending. (On this repository you don't need to create a new branch)

Before doing anything, open ```package/package.json``, and increment by one the version number (the last number)

Push Changes
------------

The last step is to copy over the changes from the SDK to the DLL projects, and push the changes. The documentation will get built on it's own, and the SDK will be available to be pulled from the projects.

Open a Terminal windows inside the DLL project (at the root), and call the following command:

.. note:: In OSX, if you drag the folder to the Terminal, it will copy it's path. This is handy to get the paths to the SDK and DLL.

- Only copy the files from the SDK to the DLL (so you can see in git if everything is correct)

.. code-block::

  make sdk=<PATH_TO_SDK_ROOT> copy

- Only push the pending changes that you have in the DLL repository (``branch`` is optional)

.. code-block::

  make [branch=<BRANCH_NAME>] push

- Copy the changes, and push them (``branch`` is optional)

.. code-block::

  make sdk=<PATH_TO_SDK_ROOT> [branch=<BRANCH_NAME>] copy_push
